<!DOCTYPE html><!-- HTML 5 -->

<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<title>
<?php // bloginfo('name'); if(is_home() || is_front_page()) { echo ' - '; bloginfo('description'); } else { wp_title(); } ?>
<?php if(is_home() || is_front_page()) { echo "Enterprise Agility Blog - People10" ; } else { wp_title(); echo " - People10" ;  }  ?>
</title>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29156147-1']);
  _gaq.push(['_setDomainName', 'people10.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>
#event {
	width:555px;
	height:115px;
}
#advertisement {
	margin-top:15px;
	margin-bottom:-18px;
	width:900px;
}
</style>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" >
<div id="header">
  <div id="logo">
    <?php 

				$options = get_option('themezee_options');

				if ( isset($options['themeZee_logo']) and $options['themeZee_logo'] <> "" ) { ?>
    <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($options['themeZee_logo']); ?>" alt="Logo" style="width:250px;" /></a>
    <?php } else { ?>
    <a href="<?php echo home_url(); ?>/">
    <h1>
      <?php bloginfo('name'); ?>
    </h1>
    </a>
    <?php } ?>
  </div>
  <!--  <div id="navi">
    <?php 

/*					// Get Top Navigation out of Theme Options

					wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'echo' => true, 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));*/

				?>
  </div>
--> 
</div>
<div class="clear"></div>
<?php if( get_header_image() != '' ) : ?>
<div id="custom_header"> <img src="<?php echo get_header_image(); ?>" /> </div>
<?php endif; ?>
<div style="position:absolute; margin-top:-128px; margin-left: 345px;"> <a href="http://www.people10.com/resources/white-papers/Building-scalable-web-applications-in-weeks-not-in-months"><img id="event" src="http://www.people10.com/blog/images/Top-Banner.jpg" width="555" height="150" /> 
  
  <!-- <object width="555" height="115"
classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
codebase="http://fpdownload.macromedia.com/
pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0">
    <param name="SRC" value="http://www.people10.com/blog/wp-content/themes/zeecorporate/Blog_Banner_Animated.swf">
    <embed src="http://www.people10.com/blog/wp-content/themes/zeecorporate/Blog_Banner_Animated.swf" width="555" height="115"> </embed>
  </object>--> 
  </a> </div>

<div style="width:900px"> <a href="http://www.people10.com/registration.php?DocId=11" target="_blank"> <img id="advertisement" src="http://www.people10.com/blog/images/Nav-content.jpg"/> </a> </div>
<!-- start of syam's code --> 

<!-- end of syam's code -->

<div id="wrap">
