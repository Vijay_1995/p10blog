<!DOCTYPE html><!-- HTML 5 -->

<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<title>
<?php // bloginfo('name'); if(is_home() || is_front_page()) { echo ' - '; bloginfo('description'); } else { wp_title(); } ?>
<?php if(is_home() || is_front_page()) { echo "Enterprise Agility Blog - People10" ; } else { wp_title(); echo " - People10" ;  }  ?>
</title>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29156147-1']);
  _gaq.push(['_setDomainName', 'people10.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<style>

</style>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" >
<div id="header">
  <div id="logo">
    <?php 
				$options = get_option('themezee_options');

				if ( isset($options['themeZee_logo']) and $options['themeZee_logo'] <> "" ) { ?>
    <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($options['themeZee_logo']); ?>" alt="Logo" style="width:250px;" /></a>
    <?php } else { ?>
    <a href="<?php echo home_url(); ?>/">
    <h1>
      <?php bloginfo('name'); ?>
    </h1>
    </a>
    <?php } ?>
  </div>
  <!--  <div id="navi">
    <?php 

/*					// Get Top Navigation out of Theme Options

					wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'echo' => true, 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));*/

				?>
  </div>
--> 
</div>
<div class="clear"></div>
<?php if( get_header_image() != '' ) : ?>
<div id="custom_header"> <img src="<?php echo get_header_image(); ?>" /> </div>
<?php endif; ?>
<div style="position:absolute; margin-top:-128px; margin-left: 345px;"> <a href="http://www.people10.com/resources/white-papers/Building-scalable-web-applications-in-weeks-not-in-months"><img id="event" src="http://www.people10.com/blog/images/Top-Banner.jpg" width="555" height="150" /> 
  
  <!-- <object width="555" height="115"
classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
codebase="http://fpdownload.macromedia.com/
pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0">
    <param name="SRC" value="http://www.people10.com/blog/wp-content/themes/zeecorporate/Blog_Banner_Animated.swf">
    <embed src="http://www.people10.com/blog/wp-content/themes/zeecorporate/Blog_Banner_Animated.swf" width="555" height="115"> </embed>
  </object>--> 
  </a> </div>
<div id="wrapper1">
  <div id="head_container">
    <div id="block-om_maximenu-om-maximenu-15" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-web" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1261514393">
          <div id="om-menu-web-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-web" class="om-menu">
              <li id="om-leaf-om-u1-1261514393-1" class="om-leaf first last leaf-web active"> <span class="om-link  link-web active active">Web<span class="om-subtitle">Development</span></span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-89 first last">
                          <div class="content">
                            <div id="main-container">
                              <div>
                                <div class="block-menu-region" style="width: 435px;"> <a href="http://www.people10.com/Web-Development/Custom-Application-Development" class="om-autoscroll block-title"><span>CUSTOM APPLICATION DEVELOPMENT</span></a>
                                  <p class="paragraph_context"><a href="http://www.people10.com/Web-Development/Custom-Application-Development" class="om-autoscroll">Discover how our expertise in agile product engineering, UX and choice of rapid development technologies help you achieve state-of-the-art, modern web applications guaranteed for faster time to market, enchanting user experience and great performance</a></p>
                                </div>
                                <div id="block5" class="block-menu-region" style="width: 435px; padding-left: 11px;"> <a href="http://www.people10.com/Web-Development/Legacy-Modernization" class="om-autoscroll block-title"><span>LEGACY MODERNIZATION</span></a>
                                  <p class="paragraph_context"> <a href="http://www.people10.com/Web-Development/Legacy-Modernization" class="om-autoscroll"> Learn how our application modernization techniques help you give a facelift to your dated IT systems in a cost effective way resulting in lower TCO, higher maintainability, lesser risk and giving the much needed boost and agility for your business to run ahead of your competition </a> </p>
                                </div>
                              </div>
                              <div class="two-column-downnload-block">
                                <div style="width: 435px; float: left; border-right: solid 1px #ccc;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/TotalCostOfOwnership189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;"><span>Total cost of ownership is lower in agile development</span></div>
                                  </div>
                                  <div style="margin-right: 10px;"><a href="http://www.people10.com/lead-capture?DocId=24" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                                <div style="width: 435px; float: left;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/AgileVendorChecklist189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;"><span>Agile outsourcing vendor selection checklist</span></div>
                                  </div>
                                  <div style="margin-left: 10px; "><a href="http://www.people10.com/lead-capture?DocId=31" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-13" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-mobile" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-393703193">
          <div id="om-menu-mobile-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-mobile" class="om-menu">
              <li id="om-leaf-om-u1-393703193-1" class="om-leaf first last leaf-mobile active"> <span class="om-link  link-mobile active active">Mobile<span class="om-subtitle">Apps</span></span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-90 first last">
                          <div class="content">
                            <div id="main-container">
                              <div class="block-menu-region" style="width: 285px;"> <a href="http://www.people10.com/Mobile-Apps/Mobile-Application-Development" class="om-autoscroll  block-title">Mobile App Development </a>
                                <p class="paragraph_context"><a href="http://www.people10.com/Mobile-Apps/Mobile-Application-Development" class="om-autoscroll">Discover how we build awesome user experience and mobile accessibility for enterprises and consumer portals in this age where every internet based activity is expected out of a mobile device</a></p>
                              </div>
                              <div class="block-menu-region" style="width: 285px; padding-left: 10px;"> <a href="http://www.people10.com/Mobile-Apps/Mobile-Application-Testing" class="om-autoscroll block-title">Mobile App Testing</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Mobile-Apps/Mobile-Application-Testing" class="om-autoscroll">Learn about our expertise in validation and testing of mobile apps to ensure cross-platform compatibility, performance, stability, security, compliance and seamless user experience</a></p>
                              </div>
                              <div id="block5" class="block-menu-region" style="width:285px; padding-left: 10px;"> <a href="http://www.people10.com/Mobile-Apps/Mobile-UX-Design" class="om-autoscroll block-title">Mobile UX Design</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Mobile-Apps/Mobile-UX-Design" class="om-autoscroll">Find out how we can delivery amazing user experiences for multi-device or native OS through continuous design, wire-framing, prototyping, and modern responsive UX frameworks</a></p>
                              </div>
                              <div class="two-column-downnload-block">
                                <div style="width: 435px; float: left; border-right: solid 1px #ccc;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/MobileArchitecture189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Mobile application development architecture</div>
                                  </div>
                                  <div style="margin-right: 10px;"><a href="http://www.people10.com/lead-capture?DocId=47" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                                <div style="width: 435px; float: left;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/HospitalManagementSolution189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Mobile hospital management solution</div>
                                  </div>
                                  <div style="margin-left: 10px; "><a href="http://www.people10.com/lead-capture?DocId=48" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-12" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-cloud" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1073975659">
          <div id="om-menu-cloud-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-cloud" class="om-menu">
              <li id="om-leaf-om-u1-1073975659-1" class="om-leaf first last leaf-cloud active"> <span class="om-link  link-cloud active active">Cloud<span class="om-subtitle">Enablement</span></span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-92 first last">
                          <div class="content">
                            <div id="main-container">
                              <div>
                                <div class="block-menu-region" style="width: 435px"> <a href="http://www.people10.com/Cloud-Enablement/Cloud-Consulting" class="om-autoscroll block-title">Cloud Consulting</a>
                                  <p class="paragraph_context"><a href="http://www.people10.com/Cloud-Enablement/Cloud-Consulting" class="om-autoscroll">Discover how we can help assess your company’s readiness for the cloud, define cloud adoption roadmap, the end to end optimal cloud architecture and deployment options for stand-alone applications as well as inter-dependent application clusters</a></p>
                                </div>
                                <div id="block5" class="block-menu-region" style="width: 435px; padding-left: 10px;"> <a href="http://www.people10.com/Cloud-Enablement/Cloud-Application-Development" class="om-autoscroll block-title">Cloud Application Development</a>
                                  <p  class="paragraph_context"><a href="http://www.people10.com/Cloud-Enablement/Cloud-Application-Development" class="om-autoscroll">Learn about our success stories on greenfield SaaS development and SaaS enablement of existing products with the popular PaaS platforms providing multi-tenancy, scalability and security using agile product engineering and continuous delivery</a></p>
                                </div>
                              </div>
                              <div class="two-column-downnload-block">
                                <div style="width: 435px; float: left; border-right: solid 1px #ccc;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/SAASTransformation189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Software as a service transformation handbook</div>
                                  </div>
                                  <div style="margin-right: 10px;"><a href="http://www.people10.com/lead-capture?DocId=37" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                                <div style="width: 435px; float: left;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/CloudProductDevelopment189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Cloud product development</div>
                                  </div>
                                  <div style="margin-left: 10px; "><a href="http://www.people10.com/lead-capture?DocId=38" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-14" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-data" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1124536330">
          <div id="om-menu-data-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-data" class="om-menu">
              <li id="om-leaf-om-u1-1124536330-1" class="om-leaf first last leaf-data active"> <span class="om-link  link-data active active">Data<span class="om-subtitle">Intelligence </span></span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-91 first last">
                          <div class="content">
                            <div id="main-container">
                              <div class="block-menu-region" style="width: 285px;"> <a href="http://www.people10.com/Data-Intelligence/Data-Management-and-Search" class="om-autoscroll  block-title">Data Management and Search</a>
                                <p class="paragraph_context"><a href="http://www.people10.com/Data-Intelligence/Data-Management-and-Search" class="om-autoscroll">Unleash the power of data and gain unquestionable advantage through people10’s data strategy and governance, data architecture and integration, master data and metadata management, data cleansing services</a></p>
                              </div>
                              <div class="block-menu-region" style="width: 285px; padding-left: 10px; "> <a href="http://www.people10.com/Data-Intelligence/Analytics-and-Business-Intelligence" class="om-autoscroll block-title">Analytics and Business Intelligence</a>
                                <p class="paragraph_context"><a href="http://www.people10.com/Data-Intelligence/Analytics-and-Business-Intelligence" class="om-autoscroll">Discover how we can help your business make meaningful decisions based on structured as well as social intelligence and of big data analytics while making use of advanced techniques of search, in memory analytics, mobile BI, self-service BI and agile BI </a></p>
                              </div>
                              <div id="block5" class="block-menu-region" style="width: 285px; padding-left: 10px;"> <a href="http://www.people10.com/Data-Intelligence/Enterprise-Content-Management" class="om-autoscroll  block-title">Enterprise Content Management</a>
                                <p class="paragraph_context"><a href="http://www.people10.com/Data-Intelligence/Enterprise-Content-Management" class="om-autoscroll">Learn about People10’s content management services that solve your information discovery issues with modern CMS platforms that incorporate efficient management of workflows, documents, notifications, search and digital assets with utmost ease of use </a></p>
                              </div>
                              <div class="two-column-downnload-block">
                                <div style="width: 435px; float: left; border-right: solid 1px #ccc;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/SoftwareUXDesign189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Software user experience handbook</div>
                                  </div>
                                  <div style="margin-right: 10px;"><a href="http://www.people10.com/lead-capture?DocId=44" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                                <div style="width: 435px; float: left;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/SharepointDevelopment189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">SharePoint product development for a leading ISV</div>
                                  </div>
                                  <div style="margin-left: 10px; "><a href="http://www.people10.com/lead-capture?DocId=15" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-16" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-agile" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1215440839">
          <div id="om-menu-agile-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-agile" class="om-menu">
              <li id="om-leaf-om-u1-1215440839-1" class="om-leaf first last leaf-agile active"> <span class="om-link  link-agile active active">Agile<span class="om-subtitle">Consulting</span></span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-87 first last">
                          <div class="content">
                            <div id="main-container">
                              <div class="block-menu-region"> <a href="http://www.people10.com/Agile-Consulting/Agile-Assessment" class="om-autoscroll block-title">Agile Assessment</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Agile-Consulting/Agile-Assessment" class="om-autoscroll">Explore how People10’s appreciative enquiry can help you benchmark your current level of agility, identify the gaps and chart out a clear roadmap to embrace agility to gain benefits </a></p>
                              </div>
                              <div class="block-menu-region" style="padding-left: 10px;"> <a href="http://www.people10.com/Agile-Consulting/Agile-Transformation" class="om-autoscroll block-title">Agile<br>
                                Transformation</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Agile-Consulting/Agile-Transformation" class="om-autoscroll">Discover how People10 can transform your team or the whole IT organization in a systematic way to achieve agility through quick wins and radical changes in your culture, processes, tools and techniques </a></p>
                              </div>
                              <div class="block-menu-region" style="padding-left: 10px;"> <a href="http://www.people10.com/Agile-Consulting/Agile-Coaching-and-Training" class="om-autoscroll block-title">Agile Coaching &amp; Training</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Agile-Consulting/Agile-Coaching-and-Training" class="om-autoscroll">Read about our custom designed workshop based approach to help you improve specific agile practices in product management, estimations and scrum execution practice </a></p>
                              </div>
                              <div class="block-menu-region" style="padding-left: 10px;"> <a href="http://www.people10.com/Agile-Consulting/Product-Quality-Benchmarking" class="om-autoscroll block-title">Product Quality Benchmarking</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Agile-Consulting/Product-Quality-Benchmarking" class="om-autoscroll">Learn how we help you assess technical debts in code quality, complexity, coverage, code standards and enable you to recover stability through agile engineering practices </a></p>
                              </div>
                              <div id="block5" class="block-menu-region" style="padding-left: 10px;"> <a href="http://www.people10.com/Agile-Consulting/Agile-Governance" class="om-autoscroll block-title">Agile Governance</a>
                                <p  class="paragraph_context"><a href="http://www.people10.com/Agile-Consulting/Agile-Governance" class="om-autoscroll">See how we help small and large enterprises get a 360 degree grip on their agile initiatives through defining the right agile contracts, roles, steering, metrics and agile PMO. </a></p>
                              </div>
                              <div class="two-column-downnload-block">
                                <div style="width: 435px; float: left; border-right: solid 1px #ccc;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/FourQuadrantsEnterpriseAgility189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Four quadrants(c) for total enterprise agility</div>
                                  </div>
                                  <div style="margin-right: 10px;"><a href="http://www.people10.com/lead-capture?DocId=8" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                                <div style="width: 435px; float: left;">
                                  <div style="float: left; padding: 5px;"><img src="http://www.people10.com/sites/default/files/imce/AgileAssessmentforUK189x100.png" style="height: 40px; width: 100px; margin-left: 20px;"></div>
                                  <div style="float: left; padding: 5px;">
                                    <div class="menu-block-download-title" style="width: 215px; float: left;">Agile assessment for a leading UK bank</div>
                                  </div>
                                  <div style="margin-left: 10px; "><a href="http://www.people10.com/lead-capture?DocId=11" class="menu-block-download-link om-autoscroll"><img src="http://www.people10.com/sites/default/files/imce/Download@2x.png"></a></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-17" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-approach" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1092860156">
          <div id="om-menu-approach-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-approach" class="om-menu">
              <li id="om-leaf-om-u1-1092860156-1" class="om-leaf first last leaf-approach active"> <span class="om-link  link-approach active active">Approach</span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-menu block-menu-id-menu-how-we-do-links first last">
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Approach</span>
                                <ul class="menu">
                                  <li class="leaf first"><a href="http://www.people10.com/Approach/Four-Quadrants-Total-Agile" title="Four Quadrants© and Total Agile©" class="om-autoscroll">Four Quadrants© and Total Agile©</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Approach/Agile-Product-Engineering" title="Agile Product Engineering " class="om-autoscroll">Agile Product Engineering</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Approach/Quality-Speed-via-Continuous-Delivery" title="Quality &amp; Speed via Continuous Delivery" class="om-autoscroll">Quality &amp; Speed via Continuous Delivery</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Approach/Scrum-Delivery" title="Scrum Delivery" class="om-autoscroll">Scrum Delivery</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Approach/Distributed-Agile" title="Distributed Agile" class="om-autoscroll">Distributed Agile</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Approach/Engagement-Models" title="Engagement Models" class="om-autoscroll">Engagement Models</a></li>
                                  <li class="leaf last"><a href="http://www.people10.com/Approach/Technology-Stack" title="Technology" class="om-autoscroll">Technology Stack</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-18" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-company" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-2140693620">
          <div id="om-menu-company-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-company" class="om-menu">
              <li id="om-leaf-om-u1-2140693620-1" class="om-leaf first last leaf-company active"> <span class="om-link  link-company active active">Company</span>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-menu block-menu-id-menu-who-we-are first">
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Company</span>
                                <ul class="menu">
                                  <li class="leaf first"><a href="http://www.people10.com/Company/About" title="About" class="om-autoscroll">About</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/Vision" title="Vision" class="om-autoscroll">Vision</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/People10-Philosophy" title="Philosophy" class="om-autoscroll">Philosophy</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/Leadership-team" title="Leadership Team" class="om-autoscroll">Leadership Team</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/Customers" title="Customers" class="om-autoscroll">Customers</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/News" title="News" class="om-autoscroll">News</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/Company/Alliances" title="Alliances" class="om-autoscroll">Alliances</a></li>
                                  <li class="leaf last"><a href="http://www.people10.com/Company/Locations" title="Locations" class="om-autoscroll">Locations</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block --> 
                        
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
  </div>
</div>
<div style="width:900px"> <a href="http://www.people10.com/lead-capture?DocId=11" target="_blank"> <img id="advertisement" src="http://www.people10.com/blog/images/Nav-content.jpg"/> </a> </div>
<!-- start of syam's code --> 

<!-- end of syam's code -->

<div id="wrap">
