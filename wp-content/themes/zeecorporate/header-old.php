<!DOCTYPE html><!-- HTML 5 -->

<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<title>
<?php bloginfo('name'); if(is_home() || is_front_page()) { echo ' - '; bloginfo('description'); } else { wp_title(); } ?>
</title>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-29156147-1']);

  _gaq.push(['_setDomainName', 'people10.com']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

</script>
<style>
#event {
	width:555px;
	height:115px;
}
#advertisement {
	margin-top:15px;
	margin-bottom:-18px;
	width:900px;
}

</style>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" >
<div id="header">
  <div id="logo">
    <?php 

				$options = get_option('themezee_options');

				if ( isset($options['themeZee_logo']) and $options['themeZee_logo'] <> "" ) { ?>
    <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($options['themeZee_logo']); ?>" alt="Logo" style="width:250px;" /></a>
    <?php } else { ?>
    <a href="<?php echo home_url(); ?>/">
    <h1>
      <?php bloginfo('name'); ?>
    </h1>
    </a>
    <?php } ?>
  </div>
  <!--  <div id="navi">
    <?php 

/*					// Get Top Navigation out of Theme Options

					wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'echo' => true, 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));*/

				?>
  </div>
--> 
</div>
<div class="clear"></div>
<?php if( get_header_image() != '' ) : ?>
<div id="custom_header"> <img src="<?php echo get_header_image(); ?>" /> </div>
<?php endif; ?>
<div style="position:absolute; margin-top:-128px; margin-left: 345px;"> <a href="http://www.people10.com/resources/webinars/agile-contracting-best-practices"> 
  
  <!--<img id="event" src="http://www.people10.com/images/ten/Four-Quadrants-Home-Page-Banner.jpg" />--> 
  <img id="event" src="http://www.people10.com/blog/images/Top-banner-Agile-contracts-webinar.jpg" width="555" height="150" /> </a> </div>
<div id="header_new">
  <div id="head_container">
    <div id="block-om_maximenu-om-maximenu-1" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-what-we-do" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-477440581">
          <div id="om-menu-what-we-do-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-what-we-do" class="om-menu">
              <li id="om-leaf-om-u1-477440581-1" class="om-leaf first last leaf-what-we-do active-trail"> <a class="om-link  link-what-we-do om-autoscroll" href="http://www.people10.com/" style="font-size: 80%;">People10 Home</a><!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-1" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-what-we-do" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-477440581">
          <div id="om-menu-what-we-do-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-what-we-do" class="om-menu">
              <li id="om-leaf-om-u1-477440581-1" class="om-leaf first last leaf-what-we-do active-trail"> <a class="om-link  link-what-we-do om-autoscroll" href="http://www.people10.com/what-we-do" style="font-size: 80%;">What we do</a>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-47 first">
                          <h3 class="title"><a href="http://www.people10.com/what-we-do" title="Rethink Agility" class="om-autoscroll">Rethink Agility</a></h3>
                          <div class="content"></div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-applications-links">
                          <h3 class="title">Applications</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                                <ul class="menu">
                                  <li class="expanded first last"><a href="http://www.people10.com/what-we-do/applications/agile-application-lifecycle-management" title="Applications" class="om-autoscroll">Applications</a>
                                    <ul class="menu">
                                      <li class="leaf first"><a href="http://www.people10.com/what-we-do/applications/custom-application-development" title="Custom Application Development" class="om-autoscroll">Custom Application Development</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/what-we-do/applications/application-management" title="Application Management" class="om-autoscroll">Application Management</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/what-we-do/applications/application-modernization" title="Application Modernization" class="om-autoscroll">Application Modernization</a></li>
                                      <li class="leaf last"><a href="http://www.people10.com/what-we-do/applications/application-integration" title="Application Integration" class="om-autoscroll">Application Integration</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-it-consulting-links">
                          <h3 class="title">IT Consulting</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                                <ul class="menu">
                                  <li class="expanded first last nolink-li"><span class="nolink">IT Consulting</span>
                                    <ul class="menu">
                                      <li class="leaf first"><a href="http://www.people10.com/what-we-do/it-consulting/agile-transformation" title="Agile Transformation" class="om-autoscroll">Agile Transformation</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/what-we-do/it-consulting/architectural-review" title="Architectural Review" class="om-autoscroll">Architectural Review</a></li>
                                      <li class="leaf last"><a href="http://www.people10.com/what-we-do/it-consulting/software-quality-benchmarking" title="Software Quality Benchmarking" class="om-autoscroll">Software Quality Benchmarking</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-enterprise-services-links">
                          <h3 class="title">Information Management &amp; Analytics</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last active-trail nolink-li"><span class="nolink">What we do</span>
                                <ul class="menu">
                                  <li class="expanded first last active-trail"><a href="http://www.people10.com/what-we-do/information-management-and-analytics" title="Information Management and Analytics" class="om-autoscroll">Information Management and Analytics</a>
                                    <ul class="menu">
                                      <li class="leaf first"><a href="http://www.people10.com/what-we-do/information-management-analytics/data-integration" title="Data Integration" class="om-autoscroll">Data Integration</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/what-we-do/information-management-analytics/business-intelligence" title="Business Intelligence" class="om-autoscroll">Business Intelligence</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/what-we-do/information-management-analytics/content-management" title="Content Management" class="om-autoscroll">Content Management</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-technology-links last" style="width:150px;">
                          <h3 class="title">Technology</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                                <ul class="menu">
                                  <li class="expanded first last nolink-li"><span class="nolink">Technology</span>
                                    <ul class="menu">
                                      <li class="leaf first last"><a href="http://www.people10.com/what-we-do/technology/technical-stack" title="Technical Stack" class="om-autoscroll">Technical Stack</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-2" class="clear-block block block-om_maximenu visibility_css">
      <div class="content">
        <div id="om-maximenu-industries" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1882247154">
          <div id="om-menu-industries-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-industries" class="om-menu">
              <li id="om-leaf-om-u1-1882247154-1" class="om-leaf first last leaf-industries"> <a class="om-link  link-industries om-autoscroll" href="http://www.people10.com/industries/financial-services" style="font-size: 80%;">Industries</a>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-menu block-menu-id-menu-domains-links first last">
                          <h3 class="title">Industries</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Industries</span>
                                <ul class="menu">
                                  <li class="leaf first"><a href="http://www.people10.com/industries/financial-services" title="Financial Services" class="om-autoscroll">Financial Services</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/industries/healthcare" title="Healthcare" class="om-autoscroll">Healthcare</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/industries/retail" title="Retail" class="om-autoscroll">Retail</a></li>
                                  <li class="leaf last"><a href="http://www.people10.com/industries/ebusiness" title="eBusiness" class="om-autoscroll">eBusiness</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-3" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-how-we-do" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-961004352">
          <div id="om-menu-how-we-do-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-how-we-do" class="om-menu">
              <li id="om-leaf-om-u1-961004352-1" class="om-leaf first last leaf-how-we-do"> <a class="om-link  link-how-we-do om-autoscroll" href="http://www.people10.com/how-we-do" style="font-size: 80%;">How we do</a>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-block block-block-id-48 first">
                          <h3 class="title"><a href="http://www.people10.com/how-we-do" title="Early Business Value" class="om-autoscroll">Early Business Value</a></h3>
                          <div class="content"></div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-how-we-do-links last">
                          <h3 class="title">How we do</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">How we do</span>
                                <ul class="menu">
                                  <li class="leaf first"><a href="http://www.people10.com/how-we-do/agile-offshore" title="Agile at Offshore" class="om-autoscroll">Agile at Offshore</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/how-we-do/people-and-ecosystem" title="People and Ecosystem" class="om-autoscroll">People and Ecosystem</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/how-we-do/continuous-delivery" title="Continuous Delivery" class="om-autoscroll">Continuous Delivery</a></li>
                                  <li class="leaf last"><a href="http://www.people10.com/how-we-do/engagement-models" title="Engagement Models" class="om-autoscroll">Engagement Models</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-4" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-who-we-are" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-929686616">
          <div id="om-menu-who-we-are-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-who-we-are" class="om-menu">
              <li id="om-leaf-om-u1-929686616-1" class="om-leaf first last leaf-who-we-are"> <a class="om-link  link-who-we-are om-autoscroll" href="http://www.people10.com/who-we-are/company/about" style="font-size: 80%;">Who we are</a>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-menu block-menu-id-menu-who-we-are first">
                          <h3 class="title">Company</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Who we are</span>
                                <ul class="menu">
                                  <li class="expanded first last nolink-li"><span class="nolink">Company</span>
                                    <ul class="menu">
                                      <li class="leaf first"><a href="http://www.people10.com/who-we-are/company/about" title="About us" class="om-autoscroll">About</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/company/vision" title="Vision" class="om-autoscroll">Vision</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/company/people10-philosophy" title="Philosophy" class="om-autoscroll">Philosophy</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/company/leadership-team" title="Leadership Team" class="om-autoscroll">Leadership Team</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/company/news" class="om-autoscroll">News</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/company/alliances" title="Alliances" class="om-autoscroll">Alliances</a></li>
                                      <li class="leaf last"><a href="http://www.people10.com/who-we-are/company/locations" title="Locations" class="om-autoscroll">Locations</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-customers-links">
                          <h3 class="title">Customers</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Who we are</span>
                                <ul class="menu">
                                  <li class="expanded first last"><a href="http://www.people10.com/who-we-are/customers" title="Customers" class="om-autoscroll">Customers</a>
                                    <ul class="menu">
                                      <li class="leaf first last"><a href="http://www.people10.com/resources/customer-stories" title="Customer Stories" class="om-autoscroll">Customer Stories</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        
                        <div class="block block-menu block-menu-id-menu-work-with-us-links last">
                          <h3 class="title">Work with Us</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Who we are</span>
                                <ul class="menu">
                                  <li class="expanded first last nolink-li"><span class="nolink">Work with us</span>
                                    <ul class="menu">
                                      <li class="leaf first"><a href="http://www.people10.com/who-we-are/work-us/tech-fanatics" title="Tech Fanatics" class="om-autoscroll">Tech Fanatics</a></li>
                                      <li class="leaf"><a href="http://www.people10.com/who-we-are/work-us/meet-p10ers" title="Meet P10ers" class="om-autoscroll">Meet P10ers</a></li>
                                      <li class="leaf last"><a href="http://www.people10.com/who-we-are/work-us/careers" title="Careers" class="om-autoscroll">Careers</a></li>
                                    </ul>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-5" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-resources" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1854642798">
          <div id="om-menu-resources-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-resources" class="om-menu">
              <li id="om-leaf-om-u1-1854642798-1" class="om-leaf first last leaf-resources"> <a class="om-link  link-resources om-autoscroll" href="http://www.people10.com/resources/white-papers" style="font-size: 80%;">Resources</a>
                <div class="om-maximenu-content om-maximenu-content-nofade closed">
                  <div class="om-maximenu-top">
                    <div class="om-maximenu-top-left"></div>
                    <div class="om-maximenu-top-right"></div>
                  </div>
                  <!-- /.om-maximenu-top -->
                  <div class="om-maximenu-middle">
                    <div class="om-maximenu-middle-left">
                      <div class="om-maximenu-middle-right">
                        <div class="block block-menu block-menu-id-menu-knowledge-center-links first last">
                          <h3 class="title">Resources</h3>
                          <div class="content">
                            <ul class="menu">
                              <li class="expanded first last nolink-li"><span class="nolink">Resources</span>
                                <ul class="menu">
                                  <li class="leaf first"><a href="http://www.people10.com/resources/brochures" title="Brochures" class="om-autoscroll">Brochures</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/resources/customer-stories" title="Customer Stories" class="om-autoscroll">Customer Stories</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/resources/events" title="Events" class="om-autoscroll">Events</a></li>
                                  <li class="leaf"><a href="http://www.people10.com/resources/presentations" title="Presentations" class="om-autoscroll">Presentations</a></li>
                                  <li class="leaf last"><a href="http://www.people10.com/resources/white-papers" title="White Papers" class="om-autoscroll">White Papers</a></li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- /.block -->
                        <div class="om-clearfix"></div>
                      </div>
                      <!-- /.om-maximenu-middle-right --> 
                    </div>
                    <!-- /.om-maximenu-middle-left --> 
                  </div>
                  <!-- /.om-maximenu-middle -->
                  <div class="om-maximenu-bottom">
                    <div class="om-maximenu-bottom-left"></div>
                    <div class="om-maximenu-bottom-right"></div>
                  </div>
                  <!-- /.om-maximenu-bottom -->
                  <div class="om-maximenu-arrow"></div>
                  
                  <!-- /.om-maximenu-open --> 
                </div>
                <!-- /.om-maximenu-content --> 
                
              </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
    <div id="block-om_maximenu-om-maximenu-8" class="clear-block block block-om_maximenu ">
      <div class="content">
        <div id="om-maximenu-contact-us" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-1519711211">
          <div id="om-menu-contact-us-ul-wrapper" class="om-menu-ul-wrapper">
            <ul id="om-menu-contact-us" class="om-menu">
              <li id="om-leaf-om-u1-1519711211-1" class="om-leaf first last leaf-contact-us"> <a class="om-link  link-contact-us om-autoscroll" href="http://www.people10.com/contact-us" style="font-size: 80%;">Contact us</a> </li>
            </ul>
            <!-- /#om-menu-[menu name] --> 
          </div>
          <!-- /.om-menu-ul-wrapper --> 
          
        </div>
        <!-- /#om-maximenu-[menu name] --> 
      </div>
    </div>
  </div>
</div>
<div style="width:900px">

<a href="http://www.people10.com/registration.php?DocId=11" target="_blank">
<img id="advertisement" src="http://www.people10.com/blog/images/Nav-content.jpg"/>
</a>
</div>
<!-- start of syam's code --> 

<!-- end of syam's code -->

<div id="wrap">
