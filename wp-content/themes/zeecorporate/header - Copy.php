<!DOCTYPE html><!-- HTML 5 -->

<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<title>
<?php bloginfo('name'); if(is_home() || is_front_page()) { echo ' - '; bloginfo('description'); } else { wp_title(); } ?>
</title>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
<script type="text/javascript">

  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-29156147-1']);

  _gaq.push(['_setDomainName', 'people10.com']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

</script>
<style>
#event {
	width:555px;
	height:115px;
}
</style>
</head>
<body <?php body_class(); ?>



<div id="wrapper">
<div id="header">
  <div id="logo">
    <?php 

				$options = get_option('themezee_options');

				if ( isset($options['themeZee_logo']) and $options['themeZee_logo'] <> "" ) { ?>
    <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($options['themeZee_logo']); ?>" alt="Logo" /></a>
    <?php } else { ?>
    <a href="<?php echo home_url(); ?>/">
    <h1>
      <?php bloginfo('name'); ?>
    </h1>
    </a>
    <?php } ?>
  </div>
  <div id="navi">
    <?php 

					// Get Top Navigation out of Theme Options

					wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'echo' => true, 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));

				?>
  </div>
  <div id="block-om_maximenu-om-maximenu-1" class="clear-block block block-om_maximenu" style="font-size:1.3em; display:none">
    <div class="content">
      <div id="om-maximenu-what-we-do" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-row om-maximenu-block-down code-om-u1-477440581">
        <div id="om-menu-what-we-do-ul-wrapper" class="om-menu-ul-wrapper">
          <ul id="om-menu-what-we-do" class="om-menu">
            <li id="om-leaf-om-u1-477440581-1" class="om-leaf first last leaf-what-we-do active-trail">
              <div class="om-maximenu-content om-maximenu-content-nofade closed">
                <div class="om-maximenu-top">
                  <div class="om-maximenu-top-left"></div>
                  <div class="om-maximenu-top-right"></div>
                </div>
                <!-- /.om-maximenu-top -->
                <div class="om-maximenu-middle">
                  <div class="om-maximenu-middle-left">
                    <div class="om-maximenu-middle-right"> 
                      
                      <!-- /.block -->
                      <div class="block block-menu block-menu-id-menu-applications-links">
                        <h3 class="title">Applications</h3>
                        <div class="content">
                          <ul class="menu">
                            <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                              <ul class="menu">
                                <li class="expanded first last"><a href="/what-we-do/applications/agile-application-lifecycle-management" title="Applications" class="om-autoscroll" style="text-decoration: none;">Applications</a>
                                  <ul class="menu">
                                    <li class="leaf first"><a href="/what-we-do/applications/custom-application-development" title="Custom Application Development" class="om-autoscroll">Custom Application Development</a></li>
                                    <li class="leaf"><a href="/what-we-do/applications/application-management" title="Application Management" class="om-autoscroll">Application Management</a></li>
                                    <li class="leaf"><a href="/what-we-do/applications/application-modernization" title="Application Modernization" class="om-autoscroll">Application Modernization</a></li>
                                    <li class="leaf last"><a href="/what-we-do/applications/application-integration" title="Application Integration" class="om-autoscroll">Application Integration</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.block -->
                      <div class="block block-menu block-menu-id-menu-it-consulting-links">
                        <h3 class="title">IT Consulting</h3>
                        <div class="content">
                          <ul class="menu">
                            <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                              <ul class="menu">
                                <li class="expanded first last nolink-li"><span class="nolink">IT Consulting</span>
                                  <ul class="menu">
                                    <li class="leaf first"><a href="/what-we-do/it-consulting/agile-transformation" title="Agile Transformation" class="om-autoscroll">Agile Transformation</a></li>
                                    <li class="leaf"><a href="/what-we-do/it-consulting/architectural-review" title="Architectural Review" class="om-autoscroll">Architectural Review</a></li>
                                    <li class="leaf last"><a href="/what-we-do/it-consulting/software-quality-benchmarking" title="Software Quality Benchmarking" class="om-autoscroll">Software Quality Benchmarking</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.block -->
                      <div class="block block-menu block-menu-id-menu-enterprise-services-links">
                        <h3 class="title">Information Management &amp; Analytics</h3>
                        <div class="content">
                          <ul class="menu">
                            <li class="expanded first last active-trail nolink-li"><span class="nolink">What we do</span>
                              <ul class="menu">
                                <li class="expanded first last active-trail"><a href="/what-we-do/information-management-and-analytics" title="Information Management and Analytics" class="om-autoscroll" style="text-decoration: none;">Information Management and Analytics</a>
                                  <ul class="menu">
                                    <li class="leaf first"><a href="/what-we-do/information-management-analytics/data-integration" title="Data Integration" class="om-autoscroll">Data Integration</a></li>
                                    <li class="leaf"><a href="/what-we-do/information-management-analytics/business-intelligence" title="Business Intelligence" class="om-autoscroll">Business Intelligence</a></li>
                                    <li class="leaf"><a href="/what-we-do/information-management-analytics/content-management" title="Content Management" class="om-autoscroll">Content Management</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.block -->
                      <div class="block block-menu block-menu-id-menu-technology-links last">
                        <h3 class="title">Technology</h3>
                        <div class="content">
                          <ul class="menu">
                            <li class="expanded first last nolink-li"><span class="nolink">What we do</span>
                              <ul class="menu">
                                <li class="expanded first last nolink-li"><span class="nolink">Technology</span>
                                  <ul class="menu">
                                    <li class="leaf first last"><a href="/what-we-do/technology/technical-stack" title="Technical Stack" class="om-autoscroll">Technical Stack</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.block -->
                      <div class="om-clearfix"></div>
                    </div>
                    <!-- /.om-maximenu-middle-right --> 
                  </div>
                  <!-- /.om-maximenu-middle-left --> 
                </div>
                <!-- /.om-maximenu-middle -->
                <div class="om-maximenu-bottom">
                  <div class="om-maximenu-bottom-left"></div>
                  <div class="om-maximenu-bottom-right"></div>
                </div>
                <!-- /.om-maximenu-bottom -->
                <div class="om-maximenu-arrow"></div>
                
                <!-- /.om-maximenu-open --> 
              </div>
              <!-- /.om-maximenu-content --> 
              
            </li>
          </ul>
          <!-- /#om-menu-[menu name] --> 
        </div>
        <!-- /.om-menu-ul-wrapper --> 
        
      </div>
      <!-- /#om-maximenu-[menu name] --> 
    </div>
  </div>
  <div class="om-maximenu-content om-maximenu-content-nofade closed"  style="display:block;position:absolute;">
    <div class="om-maximenu-top">
      <div class="om-maximenu-top-left"></div>
      <div class="om-maximenu-top-right"></div>
    </div>
    <!-- /.om-maximenu-top -->
    <div class="om-maximenu-middle"  style="width: 139px;margin-left: 121px; background: url(images/mega_menu_bg.jpg) repeat-x left bottom #fff; border-left:solid 1px #007aa9; border-right: solid 1px #007aa9; border-bottom:solid 1px #007aa9;    background: none repeat scroll 0 0 #E7E7E7;border-top-left-radius: 10px;    border-top-right-radius: 10px;">
      <div class="om-maximenu-middle-left">
        <div class="om-maximenu-middle-right">
          <div class="block block-menu block-menu-id-menu-domains-links first last">
            <h3 class="title" style="font-size:1em;color:#003466;font-weight:bold;">Industries</h3>
            <div class="content">
              <ul class="menu">
                <li class="expanded first last nolink-li"><span class="nolink" style="font-size:1em;color:#003466;font-weight:bold;">Industries</span>
                  <ul class="menu">
                    <li class="leaf first"><a title="Financial Services" href="/industries/financial-services" class="om-autoscroll">Financial Services</a></li>
                    <li class="leaf"><a title="Healthcare" href="/industries/healthcare" class="om-autoscroll">Healthcare</a></li>
                    <li class="leaf"><a title="Retail" href="/industries/retail" class="om-autoscroll">Retail</a></li>
                    <li class="leaf last"><a title="eBusiness" href="/industries/ebusiness" class="om-autoscroll">eBusiness</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <!-- /.block -->
          <div class="om-clearfix"></div>
        </div>
        <!-- /.om-maximenu-middle-right --> 
      </div>
      <!-- /.om-maximenu-middle-left --> 
    </div>
    <!-- /.om-maximenu-middle -->
    <div class="om-maximenu-bottom">
      <div class="om-maximenu-bottom-left"></div>
      <div class="om-maximenu-bottom-right"></div>
    </div>
    <!-- /.om-maximenu-bottom -->
    <div class="om-maximenu-arrow"></div>
  </div>
  <div class="om-maximenu-content om-maximenu-content-nofade closed" style="display:none;" >
    <div class="om-maximenu-top">
      <div class="om-maximenu-top-left"></div>
      <div class="om-maximenu-top-right"></div>
    </div>
    <!-- /.om-maximenu-top -->
    <div class="om-maximenu-middle">
      <div class="om-maximenu-middle-left">
        <div class="om-maximenu-middle-right">
          <div class="block block-block block-block-id-48 first">
            <h3 class="title"><a title="Early Business Value" href="/how-we-do" class="om-autoscroll">Early Business Value</a></h3>
            <div class="content"></div>
          </div>
          <!-- /.block -->
          
          <div class="block block-menu block-menu-id-menu-how-we-do-links last" >
            <div class="content">
              <ul class="menu">
                <li class="expanded first last nolink-li">
                  <ul class="menu">
                    <li class="leaf first"><a title="Agile at Offshore" href="/how-we-do/agile-offshore" class="om-autoscroll">Agile at Offshore</a></li>
                    <li class="leaf"><a title="People and Ecosystem" href="/how-we-do/people-and-ecosystem" class="om-autoscroll">People and Ecosystem</a></li>
                    <li class="leaf"><a title="Continuous Delivery" href="/how-we-do/continuous-delivery" class="om-autoscroll">Continuous Delivery</a></li>
                    <li class="leaf last"><a title="Engagement Models" href="/how-we-do/engagement-models" class="om-autoscroll">Engagement Models</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <!-- /.block -->
          <div class="om-clearfix"></div>
        </div>
        <!-- /.om-maximenu-middle-right --> 
      </div>
      <!-- /.om-maximenu-middle-left --> 
    </div>
    <!-- /.om-maximenu-middle -->
    <div class="om-maximenu-bottom">
      <div class="om-maximenu-bottom-left"></div>
      <div class="om-maximenu-bottom-right"></div>
    </div>
    <!-- /.om-maximenu-bottom -->
    <div class="om-maximenu-arrow"></div>
  </div>
</div>
<div class="clear"></div>
<?php if( get_header_image() != '' ) : ?>
<div id="custom_header"> <img src="<?php echo get_header_image(); ?>" /> </div>
<?php endif; ?>
<div style="position:absolute; margin-top:-286px; margin-left: 345px;"> <a href="http://www.people10.com/resources/events/advanced-scrum-master-boot-camp"> 
  
  <!--<img id="event" src="http://www.people10.com/images/ten/Four-Quadrants-Home-Page-Banner.jpg" />--> 
  <img id="event" src="http://www.people10.com/blog/images/Advanced-Scrum-Master-Boot-Camp1.jpg" width="555" height="150" /> </a> </div>
<!-- start of syam's code --> 

<!-- end of syam's code -->

<div id="wrap">
