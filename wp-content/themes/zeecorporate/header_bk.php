<!DOCTYPE html><!-- HTML 5 -->
<html <?php language_attributes(); ?>>

<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	<title><?php bloginfo('name'); if(is_home() || is_front_page()) { echo ' - '; bloginfo('description'); } else { wp_title(); } ?></title>

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29156147-1']);
  _gaq.push(['_setDomainName', 'people10.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<style>
	#event{width:555px;height:115px;}
</style>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">

	<div id="header">

			<div id="logo">
				<?php 
				$options = get_option('themezee_options');
				if ( isset($options['themeZee_logo']) and $options['themeZee_logo'] <> "" ) { ?>
					<a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($options['themeZee_logo']); ?>" alt="Logo" /></a>
				<?php } else { ?>
					<a href="<?php echo home_url(); ?>/"><h1><?php bloginfo('name'); ?></h1></a>
				<?php } ?>
			</div>
			<div id="navi">
				<?php 
					// Get Top Navigation out of Theme Options
					wp_nav_menu(array('theme_location' => 'main_navi', 'container' => false, 'echo' => true, 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'depth' => 0));
				?>
			</div>
	</div>
	<div class="clear"></div>
	
	<?php if( get_header_image() != '' ) : ?>
	<div id="custom_header">
		<img src="<?php echo get_header_image(); ?>" />
	</div>
	<?php endif; ?>
	
	<div style="position:absolute; margin-top:-170px; margin-left: 345px;">
		<a href="http://people10.com/business-value-of-agile.htm?type=5">
			<img id="event" src="http://www.people10.com/images/ten/Four-Quadrants-Home-Page-Banner.jpg" />
		</a>
	</div>

	<div id="wrap">