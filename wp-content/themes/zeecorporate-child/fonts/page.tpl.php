<?php
ob_start("");
session_start();
//unset($_SESSION['is_pop_up_closed']) ;
if ($node->type == 'webinar') {/* check if it's a page with right side bar */
    include 'webinar.tpl.php'; /*load a page-rightsidebar.tpl.php */
    return; }

if ($node->type == 'event') {/* check if it's a page with right side bar */
    include 'event.tpl.php'; /*load a page-rightsidebar.tpl.php */
    return; }

if ($node->type == 'whitepaper') {/* check if it's a page with right side bar */
    include 'whitepaper.tpl.php'; /*load a page-rightsidebar.tpl.php */
    return; }
	
if ($node->type == 'casestudies') {/* check if it's a page with right side bar */
    include 'casestudies.tpl.php'; /*load a page-rightsidebar.tpl.php */
    return; }
if ($node->type == 'presentation') {/* check if it's a page with right side bar */
    include 'presentation.tpl.php'; /*load a page-rightsidebar.tpl.php */
    return; }

if ($node->type == 'brochures') {
    include 'brochures.tpl.php'; 
    return; }
	
	
	$url1 = $_SERVER['REQUEST_URI'];
	$url1 = explode('/', $url1);
	$url1 = $url1[1];
	$url2=$_SERVER['REQUEST_URI']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php print $scripts; ?>
<meta name="robots" content="noindex">
<link rel="icon" href="http://beta.people10.com/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://beta.people10.com/favicon.ico" type="image/x-icon" />
<meta property="og:image" content="http://www.people10.com/sites/all/themes/people10/people10logo-200.png" />
<?php
if ($url1=="beta" || $url1=="production1" || $url1=="admin" || $url1=="comment" || $url1=="filter" || $url1=="logout"  || $url1=="node"  || $url1=="search"  || $url1=="user" ) {
?>
<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
<?php
}
?>
<script type="text/javascript">
   var GB_ROOT_DIR = "<?php print $GLOBALS['base_url']; ?>/greybox/";
</script>
<script type="text/javascript" src="<?php print base_path(); ?>greybox/AJS.js"></script>
<script type="text/javascript" src="<?php print base_path(); ?>greybox/AJS_fx.js"></script>
<script type="text/javascript" src="<?php print base_path(); ?>greybox/gb_scripts.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print base_path(); ?>greybox/gb_styles.css" media="all" />

<?php if($is_front) {  ?>

<link rel="stylesheet" type="text/css" href="<?php print base_path(); ?>elastislide/css/elastislide.css" />
<link rel="stylesheet" type="text/css" href="<?php print base_path(); ?>elastislide/css/custom.css" />
<script src="<?php print base_path(); ?>elastislide/js/modernizr.custom.17475.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script src="<?php print base_path(); ?>jquery-slider/js/bjqs-1.3.min.js"></script> 
<script type="text/javascript" src="<?php print base_path(); ?>elastislide/js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="<?php print base_path(); ?>elastislide/js/jquery.elastislide.js"></script> 
<?php }  ?>

<script type="text/javascript">
	GB_myShowm = function(caption, url, /* optional */ height, width, callback_fn) {
	var options = {
	caption: caption,
	height: height || 520,
	width: width || 485,
	fullscreen: false,
	show_loading: false,
	callback_fn: callback_fn
	}
	var win = new GB_Window(options);
	return win.show(url);
	}
	
	GB_myShowm_video = function(caption, url, /* optional */ height, width, callback_fn) {
	var options = {
	caption: caption,
	height: height || 450,
	width: width || 650,
	fullscreen: false,
	show_loading: false,
	callback_fn: callback_fn
	}
	var win = new GB_Window(options);
	return win.show(url);
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php print $head_title; ?></title>
<?php
$nodeurl = url('node/'. $node->nid);
?>
<link rel="canonical" href="http://beta.people10.com<?php echo $nodeurl ; ?>"/>
<?php print $head; ?><?php print $styles; ?>
<!-- contact end-->

</head>
<body>
<div id="main_container"> <!--start of main_container--> 
  <!-- <div class="header_ribbon_div">
    <div style="width:1024px; margin:0 auto ;">
      <?php   print $header_ribbon; ?>
    </div>
  </div>-->
  <div id="top_links">
    <?php include 'includes/top_left_links.php'; ?>
    <?php include 'includes/top_right_links.php'; ?>
    <div class="clearall"></div>
  </div>
  <?php include 'includes/header.php'; ?>
  <?php include 'includes/animation.php'; ?>
  <?php include 'includes/one_col.php'; ?>
  <?php include 'includes/three_col.php'; ?>
  <?php include 'includes/two_col.php'; ?>
  <?php if($left && $right) { ?>
  <?php  // include 'includes/showcase.php'; ?>
  <?php include 'includes/left_right.php'; ?>
  <?php }elseif($right && $rightsecond) { ?>
  <?php include 'includes/showcase.php'; ?>
  <?php include 'includes/right_or_rightsecond .php'; ?>
  <?php } elseif($left || $right) { ?>
  <?php include 'includes/showcase.php'; ?>
  <?php include 'includes/left_or_right.php'; ?>
  <?php } else { ?>
  <?php include 'includes/content_only.php'; ?>
  <?php } ?>
  <div id="footer">
    <div id="footer_container">
      <?php include 'includes/footer_regions.php'; ?>
      <?php print $footer; ?> <?php print $footer_message; ?> </div>
  </div>
</div>
<style>
/* this is slider css */


#banner-slide {
	width: 100%;
	height: 325px;
	overflow: hidden;
	position: relative;
}
li.active-marker a {
	font-weight:bold;
	background: url(/sites/default/files/imce/banner-bullets-hover1.png) no-repeat center;
	cursor: default;
}
ul.bjqs {
	position:relative;
	list-style:none;
	padding:0;
	margin:0;
	overflow:hidden;
	display:none;
}
li.bjqs-slide {
	position:absolute;
	display:none;
}
ul.bjqs-controls {
	list-style:none;
	margin:0;
	padding:0;
	z-index:9999;
}
ul.bjqs-controls.v-centered li a {
	position:absolute;
}
ul.bjqs-controls.v-centered li.bjqs-next a {
	right:0;
	position: absolute;
	background: url(/sites/default/files/imce/bannerarrow-right1.png) no-repeat center;
	background-size: 36px 48px;
	width: 34px;
	height: 60px;
	overflow: hidden;
	text-indent: -9999px;
}
ul.bjqs-controls.v-centered li.bjqs-next a:hover {
	background:  url(/sites/default/files/imce/bannerarrow-right-hover1.png) no-repeat center;
}
ul.bjqs-controls.v-centered li.bjqs-prev a:hover {
	background:  url(/sites/default/files/imce/bannerarrow-left-hover1.png) no-repeat center;
}
ul.bjqs-controls.v-centered li.bjqs-prev a {
	position: absolute;
	background: url(/sites/default/files/imce/bannerarrow-left1.png) no-repeat center;
	background-size: 36px 48px;
	width: 34px;
	height: 60px;
	overflow: hidden;
	text-indent: -9999px;
}
ol.bjqs-markers {
	position: absolute;
	bottom: 0px;
	left: 36%;
	z-index: 6;
}
ol.bjqs-markers.h-centered {
	text-align: center;
}
ol.bjqs-markers li {
	float: left;
	margin: 0 0 0 11px;
	list-style:none;
	bottom: 0px;
	left: 46%;
	z-index: 6;
}
ol.bjqs-markers li a {
	float: left;
	width: 16px;
	height: 16px;
	overflow: hidden;
	text-align: center;
	color: #fff;
	font-size: 1.2em;
	line-height: 20px;
	padding-top: 20px;
	background: url(/sites/default/files/imce/banner-bullets1.png) no-repeat center;
}
ol.bjqs-markers li.active-marker a {
	background: url(/sites/default/files/imce/banner-bullets-hover1.png) no-repeat center;
}
ol.bjqs-markers li a:hover {
	background: url(/sites/default/files/imce/banner-bullets-hover1.png) no-repeat center;
	cursor: default;
}
p.bjqs-caption {
	display:block;
	width:96%;
	margin:0;
	padding:2%;
	position:absolute;
	bottom:0;
}
#block-block-10 {
	margin: 0 auto;
	width: 1024px;

}
/* this is slider css ---end */

/* this is 12 box layout css */
#vertical div.wrap:hover {
	top: -100px;
}

#one_col{
	background: #1ABC9C;
	margin-top: -20px;
}
 #block-block-141{
	width: 1024px;
	margin: 0 auto;
	background: #1ABC9C;
	margin-bottom: 2px;
}
#vertical h1 {
	text-align: center;
	font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
	font-size: 36px;
	font-weight: bold;
	background: #1ABC9C;
	color: #ECF0F1;
	text-shadow: none;
	border: 0;
	padding: 10px 10px 5px;
	margin-bottom: 0px;
	text-transform: lowercase;
}
.clear {
	clear: both;
}
 
#vertical div.element {
	margin: 1px;
	float: left;
	width: 254px;
	height: 100px;
	position: relative;
	overflow: hidden;
	font-family: "Myriad_Pro_Condensed", "Arial Narrow", "arial", "helvetica", "Sans-Serif";
	font-size: 20px;
	font-weight: bold;
	background: #FFF;
	 
}
.wrap {
	display: block;
	height: 100px;
	width: 253px;
	position: absolute;
	top: 2px;
	left: 0px;
}
.init {
	display: block;
	height: 100px;
	width:253px;
}
.short {
	display: block;
	height: 100px;
	width: 253px;
	background: #2C3E50;
	position: relative;
}
h4.twelve-block-hover-title { 
position: absolute;
font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
font-size: 16px;
color: #ECF0F1;
text-shadow: none;
font-weight: bold;
padding: 5px 10px;
}

.twelve-blocks-div-left {
	float: left;
	width: 30%;
	height: 100px;
	margin-top: 25px;
	text-align: left;
}
.twelve-blocks-div-hover-left {
	float: left;
	width: 30%;
	height: 100px;
	text-align: left;
	padding-left: 0;
	margin-top:2px;
}

.twelve-blocks-div-right {
	float: right;
	width: 70%;
	height: 100px;
	font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
	font-size: 20px;
	color: #2C3E50;
	text-shadow: none;
	padding-top: 30px;
	font-weight: bold;
}

.twelve-blocks-div-hover-right{
	float: left;
	width: 70%;
	height: 70px;
	font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
	font-size: 16px;
	color: #ECF0F1;
	text-shadow: none;
	font-weight: bold;
 }
.twelve-blocks-image-default {
	width:65%;
	padding-left:17px;
}

div.twelve-sub-desc {
font-size: 15px;
font-weight: normal;
 padding-right:10px;
}
div.twelve-sub-more-link {
	position: absolute;
	right: 8px;
	bottom: 8px;
}
a.twelve-block-a-link {
	color:#ECF0F1 ;
	cursor:pointer;
}

 
/* featured insights code */
.featured-insight-thumbnail {
	float: left;
	width: 85px;
}
.featured-insight-desc {
	float: left;
	font-size: 16px;
	width: 200px;
	padding-left: 10px;
}
.featured-insight-wrapper {
	width: auto;
	display: inline-block;
	padding: 10px;
	position: relative;
	padding-bottom: 15px;
	border-bottom: dotted 1px #CCC;
	padding-top: 15px;
}
.featured-insight-download-button {
	float: right;
	position: absolute;
	right: 0;
	top: 30px;
}

/* end of featured insight code */

/* spotlight code */
div.spotlight-wrapper:hover {
top: -276px;
}
.spotlight-wrapper {
	display: block;
	height: 276px;
	width: 312px;
	position: absolute;
}
.spotlight-init {
	display: block;
	height: 276px;
	width:312px;
}
.spotlight-short {
	display: block;
	height: 276px;
	background: #2C3E50;
	position: relative;
	margin-top: 64px;
	margin-left: 7px;
}
/* this is 12 box layout css --end*/
.view-news {
 margin-top: -10px;
padding: 5px;
}
.view-news div.view-content , .view-news div.view-content {
	font-size:16px;
}
.view-news div.view-content div.views-row {
	border-bottom: dotted 1px #CCC;
	font-size: 16px;
	padding: 10px 5px;
}
.view-news div.view-content div.views-row div.views-field-teaser{
	margin-top: -10px;
	margin-bottom: -15px;
}
.view-blog-aggregator div.view-content div.item-list ul li.views-row-3{
  border:none;	
}
.view-news div.view-content div.views-row-3 {
	border:none;
}

/* view blog agrregator code */

.view-blog-aggregator div.view-content div.item-list ul {
	margin: 0;
	padding: 5px;
}

.view-blog-aggregator div.view-content div.item-list ul li {
	padding: 10px 5px;
	list-style: none;
	border-bottom: dotted 1px #CCC;
	font-size: 16px;
	color: #2C3E50;
	margin: 0;
}

.view-blog-aggregator div.view-content div.item-list ul li div span a , .view-news div.view-content div.views-row  div.views-field-title span a{
	color:#ECF0F1;
}
div.view-blog-aggregator {
	margin-top:-10px;
}

/*------------------------------------------------------------ */

#twtr-widget-1 .twtr-new-results, #twtr-widget-1 .twtr-results-inner, #twtr-widget-1 .twtr-timeline {
	background: #ECF0F1 !important ;
}
.twtr-widget p {
	font-family: "Myriad_Web","Sans-Serif" !important; 
	font-size:14px !important;
}
.twtr-widget .twtr-tweet-wrap {
	padding: 6px 8px;
	overflow: hidden;
	zoom: 1;
	border : none !important;
}

.twtr-widget .twtr-tweet { 
border-bottom: 1px dotted #ccc;
overflow: hidden;
zoom: 1;
}
div.twitter_profile_widget
{
	margin-top:-5px !important;
}

/* spotlight hover code */
.spotlight-hover-title { 
	font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
	font-size: 29px;
	color: #ECF0F1;
	text-shadow: none;
	font-weight: bold;
	padding: 5px 15px;
	padding-top: 30px;
}
.spotlight-hover-desc {
	font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
	font-size: 16px;
	color: #ECF0F1;
	text-shadow: none;
	font-weight: normal;
	padding: 5px 15px;
}
.spotlight-hover-readmore { 
	position: absolute;
	right: 0px;
	bottom: 15px;
}

/* new code for two coumns */

#two_columns {
background: #3498DB;
width: 1024px;
margin: 0 auto;
}

#two_columns h2 , #three_columns_two h2 {
border-bottom: 1px solid #CCC;
padding: 5px;
color: #ECF0F1;
text-shadow: none;
font-size: 20px;
margin-top: 15px;
font-family: "Myriad_Pro_Condensed","Arial Narrow","arial","helvetica","Sans-Serif";
text-transform: uppercase;
font-weight: normal;
}

#two_coumns a {
	color:#ECF0F1 ; 
}

#two_coumns a:hover {
color: #ECF0F1;
text-decoration: none;
}


/* new css code for header and animation */

div#total_header {
background: #2C3E50;
height: 62px;
 margin: 0 auto;	
}
div#total_animation {
	background: #FFF;
	margin: 0 auto;
}
div#total_three_columns{
	background: #ECF0F1;
}
div#total_three_columns_two {
	background: #3498DB;
}
div#total_two_columns{
	background: #3498DB;
}
#header {
background: #2C3E50;
height: 62px;
width: 1024px;
margin: 0 auto;
}


.p10-about-us-text {
	font-size: 16px;
	color: lightgray;
	font-weight: normal;
	font-family: "Myriad_Web","Arial Narrow","arial","helvetica","Sans-Serif";
}


/*a.more_actions_button {
	text-align: right;
	text-decoration: none;
	border-right-width: 2px;
	border-right-color: #fff;
	display: block;
	float: right;
	position: relative;
	font-family: "Myriad_Pro_Condensed", "Arial Narrow", "arial", "helvetica", "Sans-Serif";
	font-size: 15px;
	text-transform: uppercase;
	color: #fff;
	height: 29px;
	max-height: 32px;
	line-height: 32px;
	padding: 0 16px;
	background: #2C3E50;
	border-radius: 2px;
	font-weight: normal;
}*/


a.download_button {
	float: right;
	width: 32px;
	overflow: hidden;
	text-align: center;
	color: #FFF;
	font-size: 1.2em;
	line-height: 20px;
	padding-top: 20px;
	background: url(/sites/default/files/imce/Download@2x.png) no-repeat center;
	background-size: 36px;
	text-indent: -9998px;
}

a.read_more_button_icon {
	background: url(/sites/default/files/imce/ReadMore@2x.png) no-repeat center;
	background-size: 32px;
	width: 32px;
	padding: 0px 0px 10px 10px;
	
}

a.more_actions_button
{
	/*text-align: right;
	text-decoration: none;
	border-right-width: 2px;
	border-right-color: #FFF;
	display: block;
	float: right;
	position: relative;	*/
	
	text-align: right;
	text-decoration: none;
	border-right-width: 2px;
	border-right-color: #FFF;
	display: block;
	float: right;
	position: absolute;
	right: 20px;
}
a.more_actions_button:hover {
background-color: none;
}
#two_col_1 {
width: 490px;
float: left;
margin: 0 10px;
}

#two_col_2 {
width: 490px;
float: left;
margin: 0 10px;
}

div.two_col_separator {
	height: 400px;
	float: left;
	border-right: solid 1px #CCC;
	margin-top: 20px;
}

a.reach_us_download {
text-decoration: none;
	border-right-width: 2px;
	border-right-color: #fff;
	display: block;
	float: right;
	position: relative;
	font-family: "Myriad_Pro_Condensed", "Arial Narrow", "arial", "helvetica", "Sans-Serif";
	font-size: 15px;
	text-transform: uppercase;
	color: #fff;
	height: 29px;
	max-height: 32px;
	line-height: 32px;
	padding: 0 16px;
	background: #2C3E50;
	border-radius: 2px;
	font-weight: normal;
	margin-bottom: 5px;
}
a.reach_us_download:hover {
	background: #2980B9;
	color: #fff;
}

 </style>
 <script type="text/javascript">
			$( '#carousel' ).elastislide();
			
			jQuery(document).ready(function($) {
          
          $('#banner-slide').bjqs({
          /*  animtype      : 'slide',*/
            height        : 325,
            width         : 1024,
            responsive    : true,
            randomstart   : false,
			animspeed : 3000,
			animduration : 1000,
			centermarkers : true
          });
		  
		  // email subscription
		  
		  $("#edit-subscribe-submit").click(function () {
            var email = $("#edit-subscribe-email").val();

            if (email == "") {
                 $("#news_error").html("Please Enter Your Email...!");
			    $("#edit-subscribe-email").addClass("subscribe_error");
                return false;
            }
			else
			$("#news_error").html("");
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) {
				 $("#edit-subscribe-email").addClass("subscribe_error");
               // $("#news_error").html("Email is Invalid...!");
                return false;
            }
			else
			$("#news_error").html("");
			$("#edit-subscribe-email").removeClass("subscribe_error");


            var dataString = "email=" + email;

            //  alert (dataString);

            $.ajax({
                type: "POST",
                url: "http://beta.people10.com/subscribe_news.php",
                data: dataString,
                cache: false,
                success: function (msg) {
					if(msg =="true")                     
					{
					//$("#news_error").hide();
					$("#edit-subscribe-email").removeClass("subscribe_error");
					$("#edit-subscribe-email").val('');
                    $("#news_error").html('<h3 style="color:#007AA9; font-size:13px;"> Email Address ' + email + ' Sucessfully Subscribed with us.</h3>');						
					}
					else if(msg == "false"){
						$("#edit-subscribe-email").addClass("subscribe_error");
						$("#news_error").html(email + " already Subscribed With Us.");
						
					 
				}
                }
            });
	

            return false;
        });

          //  12 blocks hover script
		  /*$(function(){
			$("#vertical div.wrap").hover(function(){
				$(this).stop().animate({top:"-100px"},{queue:false,duration:50});
			}, function() {
				$(this).stop().animate({top:"0px"},{queue:false,duration:50});
			});
		});*/
		  
        });
		</script>

<?php print $closure; ?>
</body>
</html>
