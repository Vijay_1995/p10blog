<?php
/**
 * 	Display search form
 *
 * @package neal
 */

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <span class="screen-reader-text"><?php esc_html_e( 'Search for:', 'neal' ) ?></span>
    <input type="search" class="search-field"
        placeholder="<?php esc_attr_e( 'Search here', 'neal' ) ?>"
        value="<?php echo get_search_query() ?>" name="s"
        title="<?php esc_attr_e( 'Search for:', 'neal' ) ?>" />
    <p class="search-description"><?php esc_html_e( 'Begin typing your search above and press return to search. Press Esc to cancel.', 'neal' ); ?></p>
    <input type="submit" class="search-submit"
        value="<?php esc_attr_e( 'Search', 'neal' ) ?>" />
</form>
