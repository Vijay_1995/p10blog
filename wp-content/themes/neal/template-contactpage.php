<?php
/**
 * Template name: Contactpage
 *
 * @package neal
 */

get_header(); 
	
	/**
	 * Functions hooked in to neal-contact-page action
	 *
     * @hooked neal_contact_page_frontend    - 10
	 * @hooked neal_contact_page_backend     - 20
	*/
	do_action( 'neal-contact-page' );

get_footer();