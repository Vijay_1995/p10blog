<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package neal
 */

if ( ! is_active_sidebar( 'sidebar-widget' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-lg-3 col-md-3 col-sm-12 col-xs-12" role="complementary">
	<?php dynamic_sidebar( 'sidebar-widget' ); ?>
</aside><!-- #secondary -->