<?php
/**
 * Template used to display single post gallery.
 *
 * @package neal
 */

	// get theme customizer data
	$blog = get_option( 'neal_blog' );
	
	// get post meta data
	$gallery_img_id 	= get_post_meta( $post->ID, 'neal_gallery_images_id', true );
	$gallery_img_id_e 	= explode( ',', $gallery_img_id );

?>

<div class="entry-gallery owl-carousel">
	<?php foreach ( $gallery_img_id_e as $img_id ) : ?>
		<a href="<?php echo esc_url( wp_get_attachment_url( $img_id ) ); ?>">
			<?php echo wp_get_attachment_image( $img_id, 'neal-post-single-gallery-image' ); ?>
		</a>
	<?php endforeach; ?>	 
</div>