<?php
/**
 * Template used to display post video.
 *
 * @package neal
 */

?>

<div class="entry-video">
    <a href="<?php echo esc_url( get_the_permalink() ); ?>">
        <?php neal_post_thumbnail( neal_get_custom_image_size() ); ?>
        <span class="post-icon post-icon-video"><i class="ion-play"></i></span>
        <?php neal_post_thumbnail_liked(); ?>
    </a>
</div>