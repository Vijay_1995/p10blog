<?php
/**
 * Template used to display post audio.
 *
 * @package neal
 */

?>

<div class="entry-audio">
	<a href="<?php echo esc_url( get_the_permalink() ); ?>">
        <?php neal_post_thumbnail( neal_get_custom_image_size() ); ?>
        <span class="post-icon post-icon-audio"><i class="ion-music-note"></i></span>
        <?php neal_post_thumbnail_liked(); ?>
    </a>
</div>