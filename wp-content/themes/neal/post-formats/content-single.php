<?php
/**
 * Template used to display post content.
 *
 * @package neal
 */

// get post meta data
$items    = neal_get_post_options_data();
$img_size = 'neal-post-single-image';

if ( $items['post_layouts'] == 3 ) {
	$img_size = 'full';
}

?>

<?php if ( $items['featured_image'] ) : ?>
	<?php if ( $items['parallax_image'] ) : ?>
		<div class="parallax-image" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)"></div>
	<?php else: ?>
		<div class="entry-image">
			<?php neal_post_thumbnail( $img_size ); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>