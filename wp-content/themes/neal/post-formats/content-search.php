<?php
/**
 * Template part for displaying results in search pages.
 *
 * @package neal
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<span class="search-post-type">
		<?php echo get_post_type(); ?>
	</span>

	<div class="entry-header">
		<h2 class="entry-title">
			<a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a>
		</h2>
	</div>

	<div class="entry-content">
		<?php neal_custom_post_content( 'p_excerpt', 25 ); ?>
	</div>
</article>