<?php
/**
 * Template used to display post link.
 *
 * @package neal
 */

// get post meta data
$link_description 	= get_post_meta( $post->ID, 'neal_link_description', true );
$link_title 		= get_post_meta( $post->ID, 'neal_link_title', true );
$link_url 			= get_post_meta( $post->ID, 'neal_link_url', true );

?>

<div class="entry-link">
	<?php neal_post_thumbnail( neal_get_custom_image_size() ); ?>
	<div class="overlay-wrap">
		<div class="overlay">
			<div class="overlay-inner">
				<div class="overlay-content">
					<?php if ( esc_html( $link_description ) ) { ?>
						<p><?php echo esc_html( $link_description ); ?></p>
					<?php  } ?>
					<small><a href="<?php echo esc_url( $link_url ); ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i><?php echo esc_html( $link_title ); ?></a></small>
				</div>
			</div>
		</div>
	</div>
</div>