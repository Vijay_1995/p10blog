<?php
/**
 * Template used to display single post audio.
 *
 * @package neal
 */

// get post meta data
$audio_type 	= get_post_meta( $post->ID, 'neal_audio_type', true );
$audio_embed 	= get_post_meta( $post->ID, 'neal_audio_embed', true );
$audio_self_mp3 = get_post_meta( $post->ID, 'neal_audio_self_mp3', true );
$audio_self_ogg = get_post_meta( $post->ID, 'neal_audio_self_ogg', true );

$items = neal_get_post_options_data();

if ( $items['post_layouts'] != 3 && $items['post_layouts'] != 4 && $items['post_layouts'] != 5 ) {
	neal_post_thumbnail( 'neal-post-single-image' );
}

?>

<?php if ( $audio_type == 'embed-code' ) : ?>
	<div class="entry-audio">
		<?php echo ''. $audio_embed; ?>
	</div>
<?php else: ?>

	<div class="entry-audio">
		<?php echo do_shortcode( '[audio mp3="'.esc_url( $audio_self_mp3 ).'" ogg="'.esc_url( $audio_self_ogg ).'"]' ); ?>
	</div>
			
<?php endif; ?>