<?php
/**
 * Template used to display video content.
 *
 * @package neal
 */

// get post meta data
$video_type     = get_post_meta( $post->ID, 'neal_video_type', true );
$video_embed    = get_post_meta( $post->ID, 'neal_video_embed', true );
$video_self_mp4 = get_post_meta( $post->ID, 'neal_video_self_mp4', true );
$video_self_ogv = get_post_meta( $post->ID, 'neal_video_self_ogv', true );

if ( $video_type == 'embed-code' ) : ?>
    <div class="entry-video">
        <?php echo ''. $video_embed; ?>
    </div>
<?php else: ?>
    <div class="entry-video">
        <?php echo do_shortcode( '[video mp4="'.esc_url( $video_self_mp4 ).'" ogv="'.esc_url( $video_self_ogv ).'" width="1116" height="700"]' ); ?>
    </div>
<?php endif; ?>