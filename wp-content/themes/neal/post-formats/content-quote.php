<?php
/**
 * Template used to display post quote.
 *
 * @package neal
 */
	
// get post meta data
$quote_content 	= get_post_meta( $post->ID, 'neal_quote_content', true );
$quote_title 	= get_post_meta( $post->ID, 'neal_quote_title', true );

?>

<div class="entry-quote">
	<?php neal_post_thumbnail( neal_get_custom_image_size() ); ?>
	<div class="overlay-wrap">
		<div class="overlay">
			<div class="overlay-content">
				<h4><?php echo esc_html( $quote_content ); ?></h4>
				<small><?php echo esc_html( $quote_title ); ?></small>
			</div>
		</div>
	</div>
</div>