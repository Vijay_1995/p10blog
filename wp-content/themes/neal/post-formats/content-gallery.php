<?php
/**
 * Template used to display post gallery.
 *
 * @package neal
 */

// get post meta data
$gallery_img_id 	= get_post_meta( $post->ID, 'neal_gallery_images_id', true );
$gallery_img_id_e 	= explode( ',', $gallery_img_id );

?>

<div class="entry-gallery">
	<a href="<?php echo esc_url( get_the_permalink() ); ?>">
        <?php neal_post_thumbnail( neal_get_custom_image_size() ); ?>
        <span class="post-icon post-icon-gallery"><i class="fa fa-camera"></i></span>
        <?php neal_post_thumbnail_liked(); ?>
    </a> 
</div>