<?php
/**
 * Template used to display post content.
 *
 * @package neal
 */

?>

<div class="entry-image">
	<a href="<?php echo esc_url( get_the_permalink() ); ?>">
		<?php 
			neal_post_thumbnail( neal_get_custom_image_size() );
			neal_post_thumbnail_liked();
		?>
	</a>
</div>