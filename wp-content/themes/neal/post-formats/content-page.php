<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package neal
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('col-lg-10 col-md-10 col-sm-10 col-xs-12 centered'); ?>>
	<?php if ( ! get_post_meta( get_the_ID(), 'neal_hide_page_title', true ) && get_post_meta( get_the_ID(), 'neal_hide_page_header', true ) ) : ?>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
	<?php endif; ?>

	<?php
	/**
	 * Functions hooked in to neal_page add_action
	 *
	 * @hooked neal_page_content		- 10
	 */
	do_action( 'neal_page' );
	?>
</article><!-- #post-## -->
