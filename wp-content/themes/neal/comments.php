<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package neal
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

// get post meta data
$items   		= neal_get_post_options_data();
$cl_classes 	= 'col-lg-8';
$form_classes 	= 'col-lg-6 col-md-7 col-sm-8';

if ( $items['post_layouts'] == 2 || $items['post_layouts'] == 3 ) {
	$cl_classes   = 'col-lg-10';
	$form_classes = 'col-lg-11 col-md-12 col-sm-12';
}

$comment_type = neal_get_option( 'comments_comment-type' );

if ( $comment_type == 'disqus' ) : ?>
<div class="container">
		<div class="row">
<div id="comments" class="comment-wrapper section centered col-xs-10" data-type="<?php echo esc_attr($comment_type);?>" data-id="<?php echo esc_attr(neal_get_option( 'comments_comment-type-disqus-shortname' )); ?>">
    <div id="disqus_thread"></div>
</div>

</div>
</div>

<?php elseif ( $comment_type == 'facebook' ) : ?>

	<div class="container">
		<div class="row">
<div id="comments" class="comment-wrapper section centered col-xs-10" data-type="<?php echo esc_attr($comment_type);?>" data-id="<?php echo esc_attr(neal_get_option( 'comments_comment-type-facebook-appid' )); ?>">
    <div class="fb-comments" data-href="<?php echo esc_url( get_the_permalink() ); ?>" data-num-posts="10" data-width="100%"></div>
</div>

</div>

</div>

<?php else: ?>

<div class="post-comments">
	<div id="comments" class="comments-area">

		<?php if ( have_comments() ) : ?>
			<h2 class="comments-title">
				<?php
					$comments_number = get_comments_number();
					if ( 1 == $comments_number ) {
						echo sprintf( '%s %s', intval( $comments_number ), esc_html__( 'Comment', 'neal' ) );
					} else {
						echo sprintf( '%s %s', intval( $comments_number ), esc_html__( 'Comments', 'neal' ) );
					}
				?>
			</h2>

			<div class="comments-inner">
				<div class="container">
					<div class="row">
						<ol class="comment-list <?php echo esc_attr( $cl_classes ); ?>">
						<?php
							wp_list_comments( 'callback=neal_comments' );
						?>
						</ol><!-- .comment-list -->
					</div>
				</div>
			</div>

			<div class="comments-links">
				<?php
					paginate_comments_links(
						array(
							'prev_text' => '<span class="fa fa-angle-left"></span>' . '<span class="screen-reader-text">' . esc_html__( 'Previous', 'neal' ) . '</span>',
							'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next', 'neal' ) . '</span>' . '</span><span class="fa fa-angle-right"></span>',
						)
					);
				?>
			</div>

		<?php endif; // Check for have_comments(). ?>

		<?php
			// If comments are closed and there are comments, let's leave a little note, shall we?
			if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
			<p class="no-comments"><?php echo esc_html( neal_get_option('comments_comment-texts-closed') ); ?></p>
		<?php endif; ?>

		<div class="container">
			<div class="row">
				<?php
					comment_form( array( 
						'label_submit'	=> esc_html__( 'Post A Comment', 'neal' ),
						'class_form' 	=> 'comment-write centered ' .$form_classes. ' col-xs-12 comment-form' 
					) );
				?>
			</div>
		</div>

	</div><!-- .comments-area -->
</div>

<?php endif; ?>