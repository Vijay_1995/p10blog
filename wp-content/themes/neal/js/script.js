jQuery(function( $ ) {
	"use strict";

	// define main variables
	var $window   = $( window ),
    	$document = $( document ),
    	$body 	  = $('body');

	$('.search-btn').on( 'click', function () {
		$('.search-wrap').addClass('open');
		$('.search-wrap .search-field').focus();
		
		// close mobile menu
		nealCloseMobileMenu();
		return false;
	});

	$('.search-close').on( 'click', function () {
		$('.search-wrap').removeClass('open');
		$('.search-wrap .search-field').focusout();
		return false;
	});

	$document.keyup( function( e ) {
		// search wrap close
  		if ( e.keyCode === 27 ) {
  			$('.search-wrap').removeClass('open');
  			$('.search-wrap .search-field').blur();
  		}
	});

	// Wordpress Default Gallery LightBox
	$('.gallery').magnificPopup({
		delegate: 'a[href*="wp-content/uploads"]',
		type: 'image',
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		zoom: {
			enabled: true,
			duration: 300
		},
		gallery: {
			enabled: true,
			tCounter: '<span class="mfp-counter">%curr%/%total%</span>'
		},
		image: {
			verticalFit: true
		}

	});

	// Wordpress Defult Image LightBox 
	var wpImgLightbox = $('img[class*="wp-image"]').parent('a[href*="wp-content/uploads"]');
	$( wpImgLightbox ).magnificPopup({
		type: 'image',
		midClick: true,
		closeOnContentClick: true,
		fixedContentPos: true,
		image: {
			verticalFit: true,
			titleSrc: function ( item ) {
				return '<strong>' + item.el.find('img').attr('alt') + '</strong>';
			}
		},
		zoom: {
			enabled: true,
			duration: 300
		}
	});

	// next & prev buttons
	$body.on( 'mouseover', '.owl-carousel', function() {
		$( this ).find( '.owl-nav .owl-prev, .owl-nav .owl-next' ).stop().fadeIn();
	}).on( 'mouseout', function () {
		$( this ).find( '.owl-nav .owl-prev, .owl-nav .owl-next' ).stop().fadeOut();
	} );

	// carousel posts
	var postCarousel   	 = $('.post-carousel'),
		postCarouselCol	 = postCarousel.data('carousel-columns'),
		postCarouselNav  = postCarousel.data('carousel-nav'),
		postCarouselDots = postCarousel.data('carousel-dots'),
		postCarouselLoop = postCarousel.data('carousel-loop'),
		postCarouselAuto = postCarousel.data('carousel-autoplay'),
		postCarouselAIn  = postCarousel.data('carousel-animate-in'),
		postCarouselAOut = postCarousel.data('carousel-animate-out');

	postCarousel.find('.post-carousel-inner').waitForImages( function() {
		postCarousel.find('.post-carousel-inner').owlCarousel({
			items: postCarouselCol,
		    loop: postCarouselLoop,
		    nav: postCarouselNav,
		    navText: [ '<i class="ion-ios-arrow-thin-left"></i>', '<i class="ion-ios-arrow-thin-right"></i>' ],
		    dots: postCarouselDots,
		    autoplay: postCarouselAuto,
		    autoplayHoverPause: true,
		    animateIn: postCarouselAIn,
		    animateOut: postCarouselAOut,
		    autoHeight: true,
		});
	});

	var dotCount = 1;
	postCarousel.find('.owl-dots .owl-dot').each( function () {
		if ( dotCount != 10 ) {
			$( 'span', this ).text( "0" + dotCount );
		} else {
			$( 'span', this ).text( dotCount );
		}
		
		dotCount++;
	});

	// slider posts
	var sliderPosts = $('.slider-posts'),	
		carColumns  = sliderPosts.data('carousel-columns'),
		carNav		= sliderPosts.data('carousel-nav'),
		carDots		= sliderPosts.data('carousel-dots'),
		carLoop		= sliderPosts.data('carousel-loop'),
		carAutoPlay	= sliderPosts.data('carousel-autoplay');

	sliderPosts.waitForImages( function() {
		sliderPosts.find('.posts').owlCarousel({
			items: carColumns,
		    loop: carLoop,
		    nav: carNav,
		    navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ],
		    dots: carDots,
		    autoplay: carAutoPlay,
		    autoplayHoverPause: true,
		    responsiveClass: true,
		    responsive: {
		        0: {
		            items: 1,
		            nav: false,
		        },
		        500: {
		            items: 2,
		            nav: false,
		        },
		        1000 : {
		        	items: 3,
		        	nav: carNav,
		        },
		        1200: {
		            items: carColumns,
		           	nav: carNav,
		        }
		    }
		});
	});

	// instagram carousel
	if ( $( '.instagram-footer .instagram-list .list' ).hasClass('owl-carousel') ) {
		var iCarCol = $('.instagram-footer .instagram-list').data('carousel-columns'),
			iCarNav = $('.instagram-footer .instagram-list').data('carousel-nav'),
			iCarLoop = $('.instagram-footer .instagram-list').data('carousel-loop');

		$('.instagram-footer .instagram-list .list').owlCarousel({
			items: iCarCol,
		    loop: iCarLoop,
		    nav: iCarNav,
		    dots: false,
		    navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ],
		    responsiveClass: true,
		    responsive:{
		        0: {
		            items: 1,
		            nav: false,
		        },
		        400 : {
		        	items: 2,
		            nav: false,
		        },
		        600: {
		            items: 3,
		            nav: false,
		        },
		        1000: {
		            items: iCarCol,
		           	nav: iCarNav,
		        }
		    }
		});

		// instagram fixed title
		var insta  		= $('.instagram-footer'),
			instaItem 	= insta.find('.insta'),
			itemWidth  	= instaItem.width();

		if ( insta.find('.title').hasClass('fixed') ) {
			insta.find('.title.fixed').css({
				width : itemWidth + 'px',
				height: itemWidth + 'px',
			});
		}
	}

	// blog single gallery
	var entryGallery = $('.single .entry-gallery');
	entryGallery.waitForImages( function() {

		entryGallery.find('a').magnificPopup({
			gallery: {
				enabled: true
			},
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			fixedContentPos: true,
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			gallery: {
				enabled: true,
				tCounter: '<span class="mfp-counter">%curr%/%total%</span>'
			},
			image: {
				verticalFit: true
			}
		});

		entryGallery.owlCarousel({
			items: 1,
			loop: true,
			autoHeight: true,
			dots: false,
			nav: true,
			controlsClass: 'gallery-controls center-width',
		    dotsClass: 'gallery-pagination',
		    dotClass: 'gallery-pagination-item',
			navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ]
		});

	});

	// reponsive videos
	$('.entry-video').fitVids();

	// Sticky Header
	var headerArea = $(".site-header .header-area"),
    	headerHeight = headerArea.innerHeight(),
    	treshold = 0,
    	lastScroll = 0;

   	if ( headerArea.data( 'sticky' ) ) {
		$document.on( 'scroll', function ( e ) {
			var currentScroll = $document.scrollTop(),
				diff 	  	  = currentScroll - lastScroll;

	    	// normalize treshold range
	    	treshold = ( treshold + diff > headerHeight ) ? headerHeight : treshold + diff;
	    	treshold = ( treshold < 0 ) ? 0 : treshold;

	    	if ( treshold <= 0 ) {
	    		if ( currentScroll < 1 ) {
	    			headerArea
		        	.removeClass('sticky');
		        	$body.removeClass('sticky-header');
		    	}
	    	} else {
	    		if ( currentScroll > headerHeight ) {
	    			headerArea
	        		.addClass('sticky');
	        		$body.addClass('sticky-header');
		    	}
	    	}
	    	
	    	headerArea.css( 'top', ( - treshold ) + 'px' );

	    	if ( treshold >= headerHeight ) {
	    		$('.nav-menu li > ul.sub-menu, .mega-dropdown-submenu, .mega-content').css( 'visibility', 'hidden' );
	    	} else {
	    		$('.nav-menu li > ul.sub-menu, .mega-dropdown-submenu, .mega-content').css( 'visibility', '' );
	    	}

	    	lastScroll = currentScroll;
		});
	}

	// Google Map
	$('.google-map').each( function () {
		var geocoderObj;
		var mapData = {
			location: $( this ).data('location'),
			zoom: $( this ).data('zoom'),
			mapType: $( this ).data('map-type'),
			markerTitle: $( this ).data('title'),
			typeControl: $( this ).data('type-control'),
			nav: $(this).data('nav'),
			markerIcon: $( this ).data('marker-icon'),
		}

		if ( mapData.mapType === 'ROADMAP' ) {
			mapData.mapType = google.maps.MapTypeId.ROADMAP;
		} else {
			mapData.mapType = google.maps.MapTypeId.SATELLITE;
		}

		if ( parseInt( mapData.typeControl, 10 ) === 1 ) {
			mapData.typeControl = true;
		} else {
			mapData.typeControl = false;
		}

		if ( parseInt( mapData.nav, 10 ) === 1 ) {
			mapData.nav = true;
		} else {
			mapData.nav = false;
		}

		geocoderObj = new google.maps.Geocoder();

		geocoderObj.geocode( { 'address': mapData.location }, function( results, status ) {
			if ( status == google.maps.GeocoderStatus.OK ) {
				var mapSettings = {
					zoom: mapData.zoom,
					mapTypeId: mapData.mapType,
					streetViewControl: false,
					scrollwheel: false,
					mapTypeControl: mapData.typeControl,
					panControl: mapData.nav,
					zoomControl: mapData.nav,
					mapTypeControlOptions: {
				      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				    }
				}

				var map = new google.maps.Map( $('.google-map')[0], mapSettings );

				map.setCenter( results[0].geometry.location );

				// Marker
				var marker = new google.maps.Marker({
					map: map,
					icon: mapData.markerIcon,
				  	position: results[0].geometry.location,
				});

			}
		});

	});

	// hamburger menu
	$('.mobile-nav-open').on( 'click', function () {
		if ( ! $( this ).hasClass('open') ) {
			$body.css( 'overflow', 'hidden' );
		} else {
			$body.css( 'overflow', '' );
		}

		var scrollTop = $window.scrollTop();
		
		// header
		$('.site-header .header-area').css( {
			'margin-top': scrollTop + 'px',
			'position' : 'relative',
		} );

		$( this ).toggleClass('is-active')
		setTimeout( function() { 
			$body.toggleClass('toggle-mobile-menu');
			$('.mobile-nav-close').addClass('open');
		}, 380 );
	});

	// mobile nav close button
	function nealCloseMobileMenu() {
		$body.removeClass('toggle-mobile-menu');
		setTimeout( function() { 
			$('.mobile-nav-open').removeClass('is-active');
			$body.css( 'overflow', '' );

			// header
			$('.site-header .header-area').css( {
				'margin-top': '',
				'position' : ''
			} );
		}, 350 );
	}

	$('.mobile-nav-close, .menu-background').on( 'click', function () {
		// close mobile menu
		nealCloseMobileMenu();
		return false;
	});

	// mobile navigation
	$('.mobile-nav .menu li a').on( 'click', function () {
		$('.mobile-nav .menu > li > ul.sub-menu').stop().slideUp();
		$( this ).next('.sub-menu').stop().slideToggle();

		if ( $( this ).parent().hasClass('menu-item-has-children') ) {
			return false;
		}
	});

	// mobile nav header
    var mtreshold   = 0,
    	mlastScroll = 0;
		
	$('.mobile-nav .menu-scroll .menu-content').on( 'scroll', function ( e ) {
		var currentScroll = $('.mobile-nav .menu-scroll .menu-content').scrollTop(),
			diff 	  	  = currentScroll - lastScroll,
			mHeaderHeight = $('.mobile-nav .mobile-nav-header').height();

	    mtreshold = ( mtreshold + diff > mHeaderHeight ) ? mHeaderHeight : mtreshold + diff;
	    mtreshold = ( mtreshold < 0 ) ? 0 : mtreshold;
	    	
	    $('.mobile-nav-header,.mobile-nav .menu-scroll-container').css( 'margin-top', ( - mtreshold ) + 'px' );

	    lastScroll = currentScroll;
	});
	
	// sub menu overflow
	$('.nav-menu li.hasmenu .sub-menu').each( function (index) {
		var element = $( this ),
			offset  = element.offset(),
			width   = element.width();

		if ( element.length ) {
			var right = ( $window.width() - (offset.left + element.outerWidth()) + width ),
				left  = ( offset.left + width );

			if ( left > $window.width() || right > $window.width() ) {
				if ( element.css('left') == '0px' ) {
					element.css({
						'right' : 0,
					   	'left'  : 'auto'
					});
				} else if ( element.css('right') == '0px' ) {
					element.css({
						'left' : 0,
					   	'right'  : 'auto'
					});
				}

				if ( element.css('left') == width + 'px' ) {
					element.parent().parent().not('.nav-menu > li.hasmenu > .sub-menu').css({
						'left' : '-100%',
					   	'right'  : 'auto'
					}).addClass('left');

					element.css({
						'left' : '-100%',
					   	'right'  : 'auto'
					}).addClass('left');
				}
			}
		}
	});

	// if empty remove the element
	$('.entry-header, .container-header, .page-header').each( function () {
		if ( $( this ).text().trim() === '' ) {
        	$( this ).remove();
		}
	});

	// entry audio set height
	$('.entry-media .entry-audio iframe').attr( 'height', 100 );

	// post like
	var ajaxStart = 0;
	$('.post-like').on( 'click', function () {
		var thisP		= $( this ),
			postID 		= thisP.data('post-id'),
			countLikes 	= parseInt( thisP.find('span').html() );

		if ( ! ajaxStart ) {
			ajaxStart = 1;
			if ( ! thisP.hasClass('liked') ) {
				countLikes++;
			} else {
				countLikes--;
			}

			thisP.toggleClass('liked');
			thisP.toggleClass('bounceIn');

			thisP.find('span').text( countLikes );
			countLikes = 0;

			$.post( ajaxurl, {
				action: 'neal_post_like',
				post_id: postID,
		    }, function( res ) {
		    	thisP.find('span').html( res );
		    	ajaxStart = 0;
		    });
		 }


		return false;
	});

	// featured post parallax image
	$window.scroll( function () {
		var scroll = $window.scrollTop(),
			featuredPost = $('.featured-post'),
			featuredPostPrlx = featuredPost.data('parallax');

		if ( featuredPostPrlx ) {
			if ( scroll < featuredPost.innerHeight() ) {
				featuredPost.css({
					'background-position' : 'center calc(50% + ' + ( scroll * .5 ) + 'px)'
				});
			}
		}		
	});

	// Sticky Sidebar
	function nealStickySidebar() {
		var singlePSticky 	= $(".single .widget-area"),
			blogPSticky		= $(".archive:not(.woocommerce) .content-area, .archive:not(.woocommerce) .widget-area"),
			marginTop       = 60;

		if ( $body.hasClass('sticky-header-active') ) {
			marginTop = 130;
		}

		// single posts
		if ( $('.single .site-main').hasClass('sticky-sidebar')  ) {
			singlePSticky.theiaStickySidebar({
      			additionalMarginTop: marginTop
    		});
		}

		// blog pages
		if ( $body.hasClass('sticky-sidebar-blog') ) {
			blogPSticky.theiaStickySidebar({
      			additionalMarginTop: marginTop
    		});
		}

		// homepage
		for ( var i = 1; i <= 3; i ++ ) {
			var widget = $('.site-content .sidebar-area-' + i );
			if ( widget.data('sticky') ) {
				widget.theiaStickySidebar({
      				additionalMarginTop: marginTop
    			});

			}
		}
	}

	// fixed footer
	function nealFixedFooter() {
		if ( $body.hasClass('fixed-footer') ) {
			var siteFooter   = $('.site-footer'),
				footerHeight = siteFooter.innerHeight() + ( ! siteFooter.hasClass('footer-3') ? 100 : 0 );

			$('.site-content').css( 'margin-bottom', footerHeight + 'px' );
		}
	}

	// mega menu
	function nealMegaMenu() {
		var megaContent  = $('.nav-menu li.full-width'),
			megaWidth	 = $window.width(),
			megaBoxedPos = 0;

		// boxed layout
		if ( $body.hasClass('neal-boxed-layout') ) {
			megaWidth 	 = $('.full-site-content').width();
			megaBoxedPos = (  $window.width() - megaWidth ) / 2;
		}

		megaContent.each( function () {
			var megaOffset  	= $( this ).offset(),
				megaOffsetLeft  = megaOffset.left - megaBoxedPos;

			$( this ).find('.mega-content,.mega-dropdown-submenu').css( {
				width : megaWidth + 'px',
				left  : '-' + megaOffsetLeft + 'px',
				overflow : 'hidden'
			} );
		});
	}

	$window.resize( function() {
		nealStickySidebar();
		nealFixedFooter();
		nealMegaMenu();
	});

	$window.load( function() {
		nealStickySidebar();
		nealFixedFooter();
		nealMegaMenu();

		$('.site-header').css( 'overflow', 'visible' );
	});

	// mega menu tabs
	var megaPosts 			= $('.mega-posts'),
		megaPostsTabMenu	= megaPosts.find('ul.post-tab li'), 
		megaPostsTabContent = megaPosts.find('.post-content .tabs-post-content');

	megaPostsTabContent.first().show();
	
	megaPostsTabMenu.on( 'click', function () {
		var index = $( this ).index('.mega-posts ul.post-tab li');

		if ( ! $( this ).hasClass('active') ) {
			megaPostsTabMenu.removeClass('active');
			$( this ).addClass('active');

			megaPostsTabContent.hide().eq( index ).show();
		}
	});

	// product photoswipe setup
	var pSwipeItems;
	$('#slider').each( function() {
    var $pic     = $(this),
        getItems = function() {
            var getItems = [];
            $pic.find('a').each(function() {
                var $href   = $(this).attr('href'),
                    $size   = $(this).data('size').split('x'),
                    $width  = $size[0],
                    $height = $size[1];
 
                var item = {
                    src : $href,
                    w   : $width,
                    h   : $height
                }
 
                getItems.push(item);
            });
            return getItems;
        }
 
    	pSwipeItems = getItems();
	});

	var $pswp = $('.pswp')[0];
	$('#slider').on('click', 'a', function(event) {
	    event.preventDefault();
	     
	    var $index = $(this).index( '#slider a' );
	    var options = {
	        index: $index,
	        bgOpacity: 1,
	        showHideOpacity: true,
	   		zoomEl: false
	    }
	     
	    // Initialize PhotoSwipe
	    var lightBox = new PhotoSwipe( $pswp, PhotoSwipeUI_Default, pSwipeItems, options );
	    lightBox.init();
	});

	// product carousel setup
	 var isVCarousel,
	 	 carouselItem;

	 if ( $( '.product-slider' ).hasClass( 'vertical-carousel' ) ) {
	 	$( '.woocommerce .vertical-carousel li:first-child' ).addClass('active-item');
	 	isVCarousel = 1;
	 } else {
	 	isVCarousel = 0;
	 }


	var $productSlider = $("#slider"),
		$productCarousel = $("#carousel"),
		flag = false,
		duration = 300;

	$productSlider.waitForImages( function() {
		$productSlider.owlCarousel({
				rtl: false,
				items: 1,
				margin: 10,
				autoHeight: true,
				nav: true,
				dots: true,
				controlsClass: 'p-slider-controls center-width',
			    navContainerClass: 'p-slider-nav',
			    navClass: ['p-slider-prev','p-slider-next'],
			    dotsClass: 'p-slider-pagination',
			    dotClass: 'p-slider-pagination-item',
				navText: ['','']
		}).on('changed.owl.carousel', function ( e ) {
			if ( !flag ) {
				flag = true;
				$productCarousel.trigger('to.owl.carousel', [ e.item.index, duration, true ]);
				flag = false;

				var $items = $productCarousel.find('.owl-item');
	  			$items.removeClass('active-item');
	  			$items.eq( e.item.index ).addClass('active-item');

	  			if ( isVCarousel ) {
	  				var vItems = $('.vertical-carousel').find('li');
	  				vItems.removeClass('active-item');
	  				vItems.eq( e.item.index ).addClass('active-item');
	  			}
			}
		});
	});

	if ( !isVCarousel ) {

		$productCarousel.waitForImages( function() {
			$productCarousel.owlCarousel({
				rtl: false,
				margin: 20,
				items: 4,
				nav: true,
				onInitialized: function ( event ) {
					var $items = $( event.target ).find('.owl-item');
	  				$items.removeClass('active-item');
	  				$items.eq( event.item.index ).addClass('active-item');
	  			},
			}).on('click', '.owl-item', function ( event ) {
				$productSlider.trigger('to.owl.carousel', [ $(this).index(), duration, true] );

				var $items = $productCarousel.find('.owl-item');
	  			$items.removeClass('active-item');
	  			$( this ).addClass('active-item');

	  			return false;

			}).on('changed.owl.carousel', function (e) {
				if ( ! flag ) {
					flag = true;		
					$productSlider.trigger('to.owl.carousel', [ e.item.index, duration, true ]);
					flag = false;

					var $items = $productCarousel.find('.owl-item');
	  				$items.removeClass('active-item');
	  				$items.eq( e.item.index ).addClass('active-item');
				}
			});
		});

	} else {
		$productCarousel.on( 'click', 'li', function () {
			var itemIndex = $( this ).index();
			$productSlider.trigger( 'to.owl.carousel', [ itemIndex, duration, true ] );

			return false;
		});
	}

	// product quantity
	var qtyInput = $('.woocommerce .quantity'),
		qtyInputInStock = qtyInput.find('input.qty').attr('max') ? qtyInput.find('input.qty').attr('max') : 99999999;

	// minus
	$document.on( 'click', '.woocommerce .quantity .minus', function () {
		var qtyVal = parseInt( $( this ).next('.qty').val() );
		if ( qtyVal > 1 ) {
			$( this ).next('.qty').val( qtyVal - 1 );
			$('input[name="update_cart"]').removeAttr('disabled');
		}
	} );

	// plus
	$document.on( 'click', '.woocommerce .quantity .plus', function (){
		var qtyVal = parseInt( $( this ).prev('.qty').val() );
		if ( qtyVal < qtyInputInStock ) {
			$( this ).prev('.qty').val( qtyVal + 1 );
			$('input[name="update_cart"]').removeAttr('disabled');
		}
	} );

	// Grid & List View in Shop
	var toolBarLink = $( '.tool-bar .mode-view li a' ),
		showEl = $( '.grid-view, .list-view' ),
		viewData;
	toolBarLink.on( 'click', function () {
		viewData = $(this).parent().attr('class');
		if ( ! $( this ).hasClass( 'active' ) ) {
			toolBarLink.removeClass( 'active' );
			$( this ).addClass( 'active' );
			showEl.stop().slideUp( 500 );
			$( '#' + viewData ).stop().slideDown( 600 );

			$body.removeClass( 'shop-view-grid shop-view-list' ).addClass( 'shop-view-' + viewData );
		}

		return false;
	} );
	

	// parallax featured image on blog single
	$window.scroll( function () {
		var scroll = $window.scrollTop(),
			featuredImage = $('.single-post .entry-media .parallax-image, .single-post .post-full-media.parallax-image')

			if ( scroll < featuredImage.innerHeight() ) {
				featuredImage.css({
					'background-position' : 'center calc(50% + ' + ( scroll * .5 ) + 'px)'
				});
			}
			
	});

	// widget gallery
	var widgetGallery = $('.widget_media_gallery .gallery'),
		galleryCol 	  = 1,
		count         = 1;

	if ( widgetGallery.length ) {
		while( true ) {
			if ( widgetGallery.hasClass('gallery-columns-' + count ) ) {
				galleryCol = count;
				break;
			}

			count ++;
		}

		widgetGallery.addClass('owl-carousel').waitForImages( function() {
			widgetGallery.owlCarousel({
				items: galleryCol,
				loop: false,
				autoHeight:true,
				dots: false,
				nav: true,
				navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ],
			});
		});
	}

	// widget posts
	var widgetPosts 	= $('.widget_displayposts .widget-posts');

	widgetPosts.each( function () {
		var wThis 			= $( this ),
			widgetCarLoop	= wThis.data('loop'),
			widgetCarNav	= wThis.data('nav'),
			widgetCarDots	= wThis.data('dots');

		if ( wThis.hasClass('owl-carousel') ) {
			wThis.waitForImages( function() {
				wThis.owlCarousel({
					items: 1,
					loop: widgetCarLoop,
					autoHeight:true,
					dots: widgetCarDots,
					nav: widgetCarNav,
					navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ],
				});
			});
		}
	});

	// portfolio items
	var $items = $('.portfolio-items');
	$items.imagesLoaded( function () {
		$( this.elements ).isotope({
			itemSelector: '.portfolio-item',
			layoutMode: 'fitRows'
		});
	} );

	// portfolio filter
	$('.portfolio-filter .filter li').on( 'click', function ( e ) {
		e.preventDefault();

		var $this = $( this ),
			selector = $this.attr( 'data-filter' );

		if ( $this.hasClass( 'active' ) ) {
			return;
		}

		$this.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );
		$this.closest( '.portfolio-filter' ).next( '.portfolio-items' ).isotope( {
			filter: selector
		} );
		
	});

	// load more button pagination portfolio
	var nextPage = 1;
	$('.portfolio-load-more-btn').on( 'click', function () {
		var btnThis  	= $( this ),
			$portfolio  = $('.portfolio-items'),
			maxNumPages = btnThis.data('max-num-pages'),
			postCat     = btnThis.data('category');

		$.post( ajaxurl, {
			action: 'neal_portfolio_load_more',
			postCat: postCat,
			nextPage: nextPage
	    }, function ( resp ) {
	    	if ( resp.html ) {
	    		 var $items = $( resp.html );
	    		$items.imagesLoaded( function() {
	    			 $portfolio.append( $items ).isotope( 'appended', $items );
	    		});

	    		nextPage++;
	    		if ( nextPage == maxNumPages ) {
	    			btnThis.hide();
	    		}
	    	}
	    });

		return false;
	});

	// load more posts on homepage
	$('.homepage-post-load-more').on( 'click', function () {
		var btnThis  	 = $( this ),
			postItem  	 = btnThis.data('post-item'),
			nextPage     = btnThis.data('next-page'),
			postsPerPage = btnThis.data('posts-per-page'),
			maxNumPages  = btnThis.data('max-num-pages'),
			postOrderBy  = btnThis.data('post-orderby'),
			postNotIn  	 = btnThis.data('post-not-in'),
			featuredPost = btnThis.data('featured-post'),
			postCategory = btnThis.data('post-category'),
			postTag 	 = btnThis.data('post-tag'),
			postIds		 = btnThis.data('post-ids'),
			metaCategory = btnThis.data('meta-category'),
			metaDate 	 = btnThis.data('meta-date'),
			metaAuthor   = btnThis.data('meta-author'),
			postExcerpt	 = btnThis.data('post-excerpt'),
			readMore     = btnThis.data('read-more');

		$.post( ajaxurl, {
			action: 'neal_homepage_post_load_more',
			postItem: postItem,
			postsPerPage: postsPerPage,
			postOrderBy: postOrderBy,
			postNotIn: postNotIn,
			featuredPost: featuredPost,
			postCategory: postCategory,
			postTag: postTag,
			postIds: postIds,
			nextPage: nextPage,
			metaCategory: metaCategory,
			metaDate: metaDate,
			metaAuthor: metaAuthor,
			postExcerpt: postExcerpt,
			readMore: readMore
	    }, function ( resp ) {
	    	if ( resp.html ) {
	    		
	    		if ( btnThis.closest('.container').find('.posts').hasClass( 'row' ) ) {
	    			btnThis.closest('.container').find('.posts').append( resp.html );
	    		} else {
	    			btnThis.closest('.container').find('.posts > .row').append( resp.html );
	    		}

	    		nextPage++;
	    		btnThis.data('next-page', nextPage);

	    		if ( nextPage == maxNumPages ) {
	    			btnThis.hide();
	    			nextPage = 1;
	    		}
	    	}
	    });

		return false;
	});

	if ( $body.hasClass('navigation-ajax') ) {

		// shop ajax navigation
		$( '.woocommerce' ).on( 'click', '.navigation .next', function ( e ) {
			e.preventDefault();
			var $button = $( this ),
					$nav = $button.closest( '.navigation' ),
					$products = $nav.prev( '.products-wrap' ).find('.products'),
					url = $button.attr( 'href' );

			$.get( 
				url,
				function ( response ) {
					var $primary    = $( response ).find( '#primary' ),
						$_products  = $primary.find('.products').children(),
						$pagination = $primary.find('.pagination');

						$products.append( $_products );
						$nav.html( $pagination.html() );

						window.history.pushState( null, '', url );
					}
				);
		});

		// posts ajax navigation
		$body.on( 'click', '.post-pagination .navigation .next', function ( e ) {
			e.preventDefault();
			var $button	= $( this ),
				$nav 	= $button.closest( '.post-pagination' ),
				$posts 	= $nav.closest('.blog-posts,.search-posts'),
				url 	= $button.attr( 'href' );

			$.get(
				url,
				function ( response ) {
					var $primary	= $( response ).find( '#primary' ),
						$_posts 	= $primary.find( '.blog-posts,.search-posts' ).children().not('.post-pagination,.row'),
						$pagination = $primary.find( '.post-pagination' );

						$( $_posts ).insertBefore('.blog-posts .post-pagination,.search-posts .row');
						$nav.html( $pagination.html() );

						window.history.pushState( null, '', url );
					}
				);
		});
	}

	// infinite scroll
	if ( $body.hasClass('infinite-scroll') && $('.products-wrap').length ) {
		var ajaxStart = 0;
		$window.scroll( function () {
			var products = $('.products-wrap');

			if ( ! products.length ) {
				return;
			}

			if ( $window.scrollTop() + $window.height() >= products.offset().top ) {
	        	var $button		= $( '.navigation' ).find('.next'),
					$nav 		= $button.closest( '.navigation' ),
					$products 	= $nav.prev( '.products-wrap' ).find('.products'),
					url 		= $button.attr( 'href' );

				if ( ! ajaxStart && $button.length ) {
					ajaxStart = 1;
					$.get(
						url,
						function ( response ) {
							var $primary	= $( response ).find( '#primary' ),
								$_products	= $primary.find( '.products' ).children(),
								$pagination = $primary.find( '.pagination' );

							if ( $pagination.length ) {
								$products.append( $_products );
								$nav.html( $pagination.html() );

								// Re-select because DOM has been changed
								$button = $nav.find( '.next' );
							} else {
								$nav.html( '' );
								$button.length = 0;
							}

							window.history.pushState( null, '', url );

							ajaxStart = 0;
						}
					);
			    }
			}
	    });
	}

    // infinity load
    if ( $body.hasClass('infinite-scroll') && $('.blog-posts').length ) {
		var ajaxStart = 0;
		$window.scroll( function () {
			var posts = $('.blog-posts');

			if ( $window.scrollTop() + $window.height() >= posts.offset().top ) {
	            var $button	= $( '.navigation' ).find('.next'),
					$nav 	= $button.closest( '.navigation' ),
					$posts 	= $nav.closest('.blog-posts'),
					url 	= $button.attr( 'href' );


				if ( ! ajaxStart && $button.length ) {
					ajaxStart = 1;
					$.get(
						url,
						function ( response ) {
							var $primary	= $( response ).find( '#primary' ),
								$_posts 	= $primary.find( '.blog-posts' ).children().not('.post-pagination'),
								$pagination = $primary.find( '.post-pagination' );

							if ( $pagination.length ) {
								$( $_posts ).insertBefore('.blog-posts .post-pagination')
								$nav.html( $pagination.html() );

								// Re-select because DOM has been changed
								$button = $nav.find( '.next' );
							} else {
								$nav.html( '' );
								$button.length = 0;
							}

							window.history.pushState( null, '', url );
							ajaxStart = 0;
							
							}
						);
			    }
			}
	    });
	}

	// shop ordering
	$('.woocommerce form.woocommerce-ordering select').select2({
		minimumResultsForSearch: Infinity
	});

	// post comment
	var commentType = $('#comments').attr('data-type');
	var commentId   = $('#comments').attr('data-id');

	if ( commentType == 'disqus' ){
		if ( $('#disqus-script').length ) {
			DISQUS.reset({
				reload: true
			});
		} else {
			$('#disqus-script').remove();
			(function() {
				var dsq = document.createElement('script');
				dsq.id  = 'disqus-script';
				dsq.type = 'text/javascript';
				dsq.async = true;
				dsq.src = '//' + commentId + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();
		}
	} else if ( commentType == 'facebook' ) {
		if ( $('#facebook-jssdk').length ) {
			FB.XFBML.parse();
		} else if ( commentType  == 'facebook' ) {
			var appid = commentId ? '&appId=' + commentId : '';
			(function(d, s, id) {
	         	var js, fjs = d.getElementsByTagName(s)[0];
	            if (d.getElementById(id)) return;
	            js = d.createElement(s); js.id = id;
	            js.src = "//connect.facebook.net/" + nealScript.lang + "/sdk.js#xfbml=1&version=v2.8" + appid;
	            fjs.parentNode.insertBefore(js, fjs);
	        }(document, 'script', 'facebook-jssdk'));
		}
	}

	// widget categories dropdown
	$('.widget.widget_categories #cat').select2();
});