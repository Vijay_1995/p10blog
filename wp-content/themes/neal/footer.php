<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package neal
 */

?>

    <?php 
    /**
     * Functions hooked into neal_before_content action
     *
     * @hooked neal_close_content_container   - 10
     */
    do_action( 'neal_after_content' ); ?>

	<div class="clear"></div>

</div><!-- #content -->


	<?php
	do_action( 'neal_before_footer'); ?>

	<footer id="colophon" class="site-footer footer-<?php echo esc_attr( neal_get_option('footer_general-footer-layout') ); ?>" role="contentinfo">

		<?php 
		/**
		 * Functions hooked into neal_footer action
		 *
		 * @hooked neal_footer_instagram	- 10
		 * @hooked neal_credit	        - 20
		 */
		do_action( 'neal_footer' ); ?>

	</footer><!-- #colophon .site-footer -->

	<?php do_action( 'neal_after_footer'); ?>

	</div><!-- #full-site-content -->

</div><!-- #page -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="<?php esc_attr_e( 'Close (Esc)', 'neal' ); ?>"></button>

                <button class="pswp__button pswp__button--share" title="<?php esc_attr_e( 'Share', 'neal' ); ?>"></button>

                <button class="pswp__button pswp__button--fs" title="<?php esc_attr_e( 'Toggle fullscreen', 'neal' ); ?>"></button>
                
                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="<?php esc_attr_e( 'Previous (arrow left)', 'neal' ); ?>">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="<?php esc_attr_e( 'Next (arrow right)', 'neal' ); ?>">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<?php wp_footer(); ?>

</body>
</html>