<?php
/**
 * Neal engine room
 *
 * @package neal
 */

/**
 * Assign the neal version to a var
 */
$theme           = wp_get_theme( 'neal' );
$neal_version    = $theme['Version'];

// category image size
add_image_size( 'neal-cat-image', 564, 312, true );

// carousel post image size
add_image_size( 'neal-carousel-post', 1300, 600, true );

// blog page image size
add_image_size( 'neal-blog-img-full-size', 1020, 680, true );

// homepage posts image sizes
add_image_size( 'neal-full-posts-image', 1080, 1350, true );
add_image_size( 'neal-posts-list-image', 920, 600, true );
add_image_size( 'neal-slider-posts-image', 477, 655, true );

// widget post image size
add_image_size( 'neal-widget-posts-image', 500, 350, true );

// portfolio image sizes
add_image_size( 'neal-portfolio-item-image', 450, 450, true );
add_image_size( 'neal-portfolio-single-image', 1140, 740, true );

// post single image sizes
add_image_size( 'neal-post-single-image', 1116, 750, true );
add_image_size( 'neal-post-single-gallery-image', 870, 557, true );

// mega menu post image size
add_image_size( 'neal-mega-posts-image', 400, 260, true );

/*
***************************************************************
* #Initialize all the things.
***************************************************************
*/

// Main Class
require_once( get_template_directory() . '/inc/class-neal.php');

// Widgets Init Classes
require_once( get_template_directory() . '/inc/neal-widgets-init-classes.php');

// Customizer
require_once( get_template_directory() . '/inc/customizer/class-neal-customizer.php');

// Customizer Default Values
require_once( get_template_directory() . '/inc/customizer/class-neal-customizer-defaults.php');

// Dynamic CSS - generated via theme customzier
require_once( get_template_directory() .'/inc/customizer/css/dynamic-css.php');

// Google Fonts
require_once( get_template_directory() .'/inc/google-fonts/google-fonts.php');

// Functions
require_once( get_template_directory() . '/inc/neal-functions.php');

// Template Hooks
require_once( get_template_directory() . '/inc/neal-template-hooks.php');

// Template Functions
require_once( get_template_directory() . '/inc/neal-template-functions.php');

// MetaBoxes
require_once( get_template_directory() . '/inc/metaboxes/class-neal-metaboxes.php');

// MetaBoxes Functions
require_once( get_template_directory() . '/inc/metaboxes/neal-metaboxes-functions.php');

if ( neal_is_woocommerce_activated() ) {

	// WooCommerce
	require_once( get_template_directory() . '/inc/woocommerce/class-neal-woocommerce.php');

    require_once( get_template_directory() . '/inc/woocommerce/neal-woocommerce-template-functions.php');

	require_once( get_template_directory() . '/inc/woocommerce/neal-woocommerce-template-hooks.php');
}

// TGM_Plugin_Activation class.
require_once( get_template_directory() . '/inc/tgm-plugin-activation/class-tgm-plugin-activation.php');