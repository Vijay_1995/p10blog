<?php
/**
 * The template for displaying search results pages.
 *
 * @package neal
 */

get_header(); ?>
	
<div id="primary" class="content-area col-xs-12">
	<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>
			<h1 class="search-query"><?php printf( esc_html__( 'Search Results for: %s', 'neal' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			<div class="row">
				<div class="search-posts col-col-lg-10 col-md-10 col-sm-10 col-xs-12 centered">
					<?php 
						do_action( 'neal_loop_before' );

						while( have_posts() ) : the_post();

							get_template_part( 'post-formats/content', 'search' );

						endwhile;
					?>
							
					<div class="row">
						<div class="post-pagination col-xs-12">
							<?php
								/**
								 * Functions hooked in to neal_paging_nav action
								 *
								 * @hooked neal_paging_nav - 10
								 */
								do_action( 'neal_loop_after' );
							?>
						</div>
					</div>
				</div>
			</div>

		<?php else : ?>		

			<h1 class="search-query"><?php echo esc_html__( 'No search results found. Try Again:', 'neal' ); ?></h1>
			<p class="text-center"><?php echo esc_html__( 'It seems we can"t find what you"re looking for. Perhaps searching can help.', 'neal' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</main><!-- #main -->
</div><!-- #primary -->


<?php 
get_footer();