<?php
/**
 * Template for displaying all single posts
 *
 * @package neal
 */

global $post;

// get post meta data
$items 		 = neal_get_post_options_data();
$post_layout = $items['post_layouts'];

get_header();

get_template_part( 'template-parts/single-post/layout', $post_layout );
	
get_footer();