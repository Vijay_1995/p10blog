<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package neal
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php neal_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<div id="page" class="site">

		<div class="mobile-nav">
			<div class="mobile-nav-header">
				<div class="hamburger mobile-nav-btn mobile-nav-close hamburger--collapse is-active">
					<div class="hamburger-box">
						<div class="hamburger-inner"></div>
					</div>
				</div>
				<?php 
					// shop icon
					neal_header_shop_cart(); 

					// search icon
					neal_header_search_icon();
				?>
			</div>
			<div class="container">
				<div class="menu-scroll-container">
					<div class="menu-scroll">
						<div class="menu-content">
						<?php
							if ( has_nav_menu( 'mobile-menu' ) ) {
								wp_nav_menu( array( 
									'theme_location' => 'mobile-menu', 
									'menu_class' => 'menu' 
								) );
							}
						?>
						</div>
					</div>
				</div>

				<?php neal_social_media(); ?>
			</div>
		</div>

		<div class="menu-background"></div>

		<div class="full-site-content">

			<?php do_action( 'neal_before_header' ); ?>

			<header id="masthead" class="site-header header-<?php echo esc_attr( neal_get_option('header_general-header-layout') ); ?>" role="banner">
				<div class="header-area" data-sticky="<?php echo esc_attr( neal_get_option('header_general-header-sticky') ) ? 1 : 0;?>">
					<?php do_action( 'neal_header' ); ?>
				</div>
				<!-- HEADER TOP END -->

			</header><!-- #masthead -->

			<?php
			/**
			 * Functions hooked into neal_after_header action
			 *
			 * @hooked neal_social_media	- 10
			 */
			do_action( 'neal_after_header' ); ?>
	

			<div id="content" class="site-content">
				<?php 
				/**
				 * Functions hooked into neal_before_content action
				 * @hooked neal_page_header	            - 10
				 * @hooked neal_open_content_container	- 20
				 */
				do_action( 'neal_before_content' ); ?>
