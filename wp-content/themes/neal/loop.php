<?php
/**
 * The loop template file.
 *
 * Included on pages like index.php, archive.php and search.php to display a loop of posts
 * Learn more: http://codex.wordpress.org/The_Loop
 *
 * @package neal
 */

// get theme customizer data
$blog = get_option( 'neal_blog' );

$is_layout_list = false;
$post_left_class = '';
$post_right_class = '';

if ( neal_get_option( 'blog_layout-columns' ) == 'list' ) {
	$is_layout_list = true;
	$post_left_class = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
	$post_right_class = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
}

do_action( 'neal_loop_before' );

while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="article-inner">

			<?php echo esc_html( $is_layout_list ) ? '<div class="row">' : ''; ?>

			<div class="post-left <?php echo esc_attr( $post_left_class ); ?>">

				<?php if ( has_post_thumbnail() ) : 
				 ?>
					<!-- Post Media -->
					<div class="entry-media">
						<?php neal_get_template_part( 'post-formats/content', get_post_format(), neal_blog_thumbnail_sizes() ); ?>
					</div>
					<!-- Post Media End -->
				<?php endif; ?>
			</div>

			<div class="post-right <?php echo esc_attr( $post_right_class ); ?>">

				<!-- Post Header -->
				<div class="entry-header">
					<?php
						// post categories
						if ( $blog['categories-label'] ) {
							neal_post_categories(); 
						}
						
						// post date
						if ( $blog['date-label'] ) { 
							neal_post_date( $blog['date-icon'] );
						} 

						// post author
						if ( $blog['author-label'] ) {
							neal_post_author( $blog['author-icon'] );
										
						}

						// post title
						if ( $blog['title-label'] ) {
							neal_post_title();
						}
					?>
				</div>
				<!-- Post Header End -->
				<div class="entry-content"><?php neal_custom_post_content( 'p_excerpt', esc_html( $blog['description-excerpt_length'] ) ); ?></div>
				<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span>
				</a>

			</div>

			<?php echo esc_html( $is_layout_list ) ? '</div>' : ''; ?>

			<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="article-link"></a>
		</div>
	</article>

<?php endwhile; ?>


<div class="post-pagination col-xs-12">
	<?php
	/**
	 * Functions hooked in to neal_paging_nav action
	 *
	 * @hooked neal_paging_nav - 10
	 */
	do_action( 'neal_loop_after' );
	?>
</div>