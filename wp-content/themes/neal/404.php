<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package neal
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="error-404 not-found">
			<div class="page-content col-xs-10">
				<header class="content-header">
					<h1 class="content-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'neal' ); ?></h1>
				</header><!-- .page-header -->

				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'neal' ); ?></p>

				<?php get_search_form(); ?>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();