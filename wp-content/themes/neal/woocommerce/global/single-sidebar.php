<?php
/**
 * Single Sidebar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php if ( is_active_sidebar('woocommerce-widget-2') ) : ?>

	<aside id="secondary" class="widget-area col-lg-3 col-md-3 col-sm-4 col-xs-12">
		<?php dynamic_sidebar('woocommerce-widget-2'); ?>
	</aside> <!-- #secondary -->

<?php endif; ?>
