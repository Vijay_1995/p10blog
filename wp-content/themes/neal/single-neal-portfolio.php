<?php
/**
 * Template for displaying all single portfolio
 *
 * @package neal
 */

get_header();

?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="container">
			<div class="row">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); 
			?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('col-lg-10 col-md-10 col-sm-10 col-xs-12 centered'); ?>>
					
					<?php if ( has_post_thumbnail() ) :  ?>
						<!-- Post Media -->
						<div class="entry-media">
							<?php neal_post_thumbnail( 'neal-portfolio-single-image' ); ?>
						</div>
						<!-- Post Media End -->
					<?php endif; ?>

					<?php

						// categories
						neal_get_portfolio_categories( true );

						// title
						echo '<h1 class="entry-title" itemprop="name headline">'. get_the_title() .'</h1>';
					?>

					<!-- Post Content -->
					<div class="entry-content" itemprop="articleBody">
						<?php 
							the_content();
							
							// pagination
							wp_link_pages( array(
								'before' 		=> '<div class="page-links">' . esc_html__( 'Pages:', 'neal' ),
								'after'  		=> '</div>',
								'link_before'	=> '<span>',
								'link_after'    => '</span>'
							) );
						?>
					</div>
					<!-- Post Content End -->
				</article>

			<?php
				endwhile;

				// navigation
				neal_post_nav( 
					esc_html__( 'Next Project', 'neal' ), 
					esc_html__( 'Previous Project', 'neal' )
				);
			?>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
	
get_footer();