<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Homepage
 *
 * @package neal
 */

get_header();

// dynamic components list
neal_homepage_compo_list();

?>

	<?php
			/**
			 * Functions hooked in to neal_homepage action
			 * @hooked neal_caoursel_posts	    - 10
			 * @hooked neal_full_featured_post	- 20
			 * @hooked neal_masonry_posts	    - 30
			 * @hooked neal_2_grid_posts	    - 40
			 * @hooked neal_adv_block			- 50
			 * @hooked neal_full_posts			- 60
			 * @hooked neal_list_posts			- 70
			 * @hooked neal_adv_block_2 		- 80
			 * @hooked neal_slider_posts		- 90
			 * @hooked neal_3_grid_posts		- 100
			 * @hooked neal_adv_block_3			- 110
			 * @hooked neal_newsletter			- 120
			 */
			do_action( 'neal-homepage' ); 
	?>

<?php
			
get_footer();