<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package neal
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main<?php echo neal_is_content_page() ? ' single' : ''; ?>" role="main">
		<div class="container">
			<div class="row">
			<?php while ( have_posts() ) : the_post();

				  	do_action( 'neal_page_before' );

					get_template_part( 'post-formats/content', 'page' );

					/**
					 * Functions hooked in to neal_page_after action
					 *
					 * @hooked neal_display_comments - 10
					 */
					 do_action( 'neal_page_after' );

				  endwhile; // End of the loop. ?>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
