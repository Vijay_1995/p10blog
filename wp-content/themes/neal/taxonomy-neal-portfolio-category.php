<?php
/**
 * The template for displaying Portfolio category page
 *
 * @package neal
 */

get_header(); 

// get current category
$current_cat = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$portfolio = new WP_Query( array(
	'post_type' 				=> 'neal-portfolio',
	'posts_per_page' 			=> intval( get_option('neal-portfolio_posts_per_page') ),
	'neal-portfolio-category' 	=> $current_cat->slug

) );

$portfolio_categories = get_terms( array(
    'taxonomy'		=> 'neal-portfolio-category',
    'hide_empty'	=> false,
) );

?>

<div id="primary" class="content-area col-xs-12">
	<main id="main" class="site-main" role="main">
		<div class="portfolio-filter">
			<ul class="filter">
				<li data-filter="*" class="active"><?php esc_html_e( 'All', 'neal'); ?></li>
				<?php foreach( $portfolio_categories as $category ) : ?>
					<li data-filter=".neal-portfolio-category-<?php echo esc_attr( $category->slug ); ?>"><?php echo esc_html( $category->name ); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="portfolio-items row">

<?php

if ( $portfolio->have_posts() ) :

	// Start the Loop.
	while ( $portfolio->have_posts() ) : $portfolio->the_post(); 
?>
	<div id="post-<?php the_ID(); ?>" <?php post_class('portfolio-item col-lg-4'); ?>>
		<?php if ( has_post_thumbnail() ) : 
			 ?>
			<!-- Post Media -->
			<div class="entry-media">
				<div class="entry-image">
					<a href="<?php echo esc_url( get_the_permalink() ); ?>">
						<?php neal_post_thumbnail( 'neal-portfolio-item-image' ); ?>
						<span class="portfolio-link"><i class="ion-ios-plus"></i></span>
					</a>
				</div>
			</div>
			<!-- Post Media End -->
		<?php endif; ?>
		<?php neal_post_title(); ?>

		<?php neal_get_portfolio_categories( true ); ?>
	</div>

<?php
	endwhile;
	wp_reset_postdata();
endif;

?>

</div>

<?php if ( $portfolio->max_num_pages > 1 ) : ?> 
	<div class="portfolio-load-more">
		<a href="#" class="portfolio-load-more-btn" data-max-num-pages="<?php echo esc_attr( $portfolio->max_num_pages ); ?>" data-category="<?php echo esc_attr( $current_cat->slug ); ?>"><?php esc_html_e( 'Load More', 'neal' ); ?></a>
	</div>
<?php endif; ?>

</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();