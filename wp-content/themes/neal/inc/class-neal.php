<?php
/**
 * Neal Main Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Main' ) ) :

	/**
	 * The main Neal class
	 */
	class Neal_Main {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'after_setup_theme',     array( $this, 'setup' ) );
			add_action( 'widgets_init',          array( $this, 'widgets_init' ) );
			add_action( 'wp_enqueue_scripts',    array( $this, 'scripts' ),       10 );
			add_action( 'admin_init',			 array( $this, 'theme_add_editor_styles' ) );
			add_action( 'wp_enqueue_scripts',    array( $this, 'child_scripts' ), 30 ); // After WooCommerce.
			add_filter( 'body_class',            array( $this, 'body_classes' ) );
			add_filter( 'nav_menu_css_class',    array( $this, 'nav_menu_classes' ), 10 , 2 );
			// add blog post classes
			add_filter( 'post_class', 			 array( $this, 'post_class' ), 30, 4 );
			add_filter( 'wp_page_menu_args',     array( $this, 'page_menu_args' ) );
			add_action( 'wp_enqueue_scripts',	 array( $this, 'enqueue_google_fonts' ) );
			add_action( 'wp_enqueue_scripts', 	 array( $this, 'get_custom_fonts' ) );

			add_action( 'tgmpa_register',		 array( $this, 'register_plugins' ) );

			// post like
			add_action( 'wp_ajax_neal_post_like', 			array( $this, 'post_like' ) );
			add_action( 'wp_ajax_nopriv_neal_post_like',	array( $this, 'post_like' ) );

			// load more posts on homepage
			add_action( 'wp_ajax_neal_homepage_post_load_more',			array( $this, 'homepage_post_load_more' ) );
			add_action( 'wp_ajax_nopriv_neal_homepage_post_load_more',	array( $this, 'homepage_post_load_more' ) );

			// jetpack comment subscription form
			add_filter( 'jetpack_comment_subscription_form', array( $this, 'comment_sub' ) );
		}

		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		public function setup() {

			/**
			 * Set the content width based on the theme's design and stylesheet.
			 */
			if ( ! isset( $content_width ) ) {
				$content_width = 900;
			}


			/*
			 * Load Localisation files.
			 *
			 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
			 */

			// Loads wp-content/languages/themes/neal.mo.
			load_theme_textdomain( 'neal', trailingslashit( WP_LANG_DIR ) . 'themes/' );

			// Loads wp-content/themes/child-theme-name/languages/en_EN.mo.
			load_theme_textdomain( 'neal', get_stylesheet_directory() . '/languages' );

			// Loads wp-content/themes/neal/languages/en_EN.mo.
			load_theme_textdomain( 'neal', get_template_directory() . '/languages' );

			/**
			 * Add default posts and comments RSS feed links to head.
			 */
			add_theme_support( 'automatic-feed-links' );

			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
			 */
			add_theme_support( 'post-thumbnails' );


			// This theme uses wp_nav_menu() in two locations.
			register_nav_menus( array(
				'primary'		=> esc_html__( 'Primary Menu', 'neal' ),
				'secondary'		=> esc_html__( 'Secondary Menu', 'neal' ),
				'mobile-menu'	=> esc_html__( 'Mobile Menu', 'neal' ),
				'footer-menu'	=> esc_html__( 'Footer Menu', 'neal' )
			) );


			/*
			 * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'widgets',
			) );


			// Declare WooCommerce support.
			add_theme_support( 'woocommerce' );

			// Declare support for title theme feature.
			add_theme_support( 'title-tag' );

			// Add post formats
			add_theme_support( 'post-formats', array( 
											   		'gallery',  
													'video',
													'audio',
													'link',
													'quote'
											   ) );
		}

		/**
		 * Register widget area.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		public function widgets_init() {

			// define sidebars
			$sidebars = array(
				'homepage-widget-1'  	=> esc_html__( 'Homepage Widgets 1', 'neal' ),
				'homepage-widget-2'  	=> esc_html__( 'Homepage Widgets 2', 'neal' ),
				'homepage-widget-3'  	=> esc_html__( 'Homepage Widgets 3', 'neal' ),
				'sidebar-widget'     	=> esc_html__( 'Sidebar Widgets', 'neal' ),
				'footer-widget-1'    	=> esc_html__( 'Footer Widgets 1', 'neal' ),
				'footer-widget-2'    	=> esc_html__( 'Footer Widgets 2', 'neal' ),
				'footer-widget-3'    	=> esc_html__( 'Footer Widgets 3', 'neal' ),
				'footer-widget-4'    	=> esc_html__( 'Footer Widgets 4', 'neal' ),
				'woocommerce-widget' 	=> esc_html__( 'WooCommerce Widgets', 'neal' ),
				'woocommerce-widget-2' 	=> esc_html__( 'WooCommerce Widgets 2 (Product single)', 'neal' ),
			);

			// register sidebars
			foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
				register_sidebar( array(
					'name'          => $sidebar_name,
					'id'            => $sidebar_id,
					'description'   => sprintf( esc_html__( 'Widget area for %s', 'neal' ), $sidebar_name ),
					'before_widget' => '<aside id="%1$s" class="widget %2$s">',
					'after_widget'  => '</aside>',
					'before_title'  => '<h3 class="widget-title">',
					'after_title'   => '</h3>',
				) );

			}
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function scripts() {
			$version = wp_get_theme()->get('Version');

			// get theme customizer data
			$contact = get_option( 'neal_contact' );
			
			// detect protocol
			$protocol 		 = is_ssl() ? 'https' : 'http';
			$google_maps_url = $protocol .'://maps.googleapis.com/maps/api/js?key='. $contact['google-map-api_key'] .'&callback=initMap"type="text/javascript';

			/**
			 * Styles
			 */
			wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );
			wp_register_style( 'ionicons', get_template_directory_uri() . '/css/ionicons.min.css', array(), '2.0.1' );
			wp_register_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '' );
			wp_register_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.min.css', array(), '' );
			wp_register_style( 'photoswipe', get_template_directory_uri() . '/css/photoswipe.min.css', array(), '' );
			wp_register_style( 'hamburgers', get_template_directory_uri() . '/css/hamburgers.min.css', array(), '' );
			wp_register_style( 'animate', get_template_directory_uri() . '/css/animate.min.css', array(), '3.5.2' );
			wp_register_style( 'select2', get_template_directory_uri() . '/css/select2.min.css', array(), '4.0.6' );

			// Main theme stylesheet file
			wp_register_style( 'neal-style', get_template_directory_uri() . '/style.css', array( 
					'font-awesome',
					'ionicons',
					'owl-carousel',
					'magnific-popup',
					'photoswipe',
					'hamburgers',
					'animate',
					'select2' ), 
				$version );

			wp_enqueue_style( 'neal-style' );
			
			/**
			 * Scripts
			 */

			wp_register_script( 'neal-google-maps', esc_url( $google_maps_url ), null, false, true );

			// load Google Maps JavaScript API v3
			if ( is_page_template( 'template-contactpage.php' ) ) {
				wp_enqueue_script( 'neal-google-maps' );
			}

			wp_register_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '2.2.0', true );
			wp_register_script( 'jquery-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array(), '', true );
			wp_register_script( 'jquery-magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(),'', true );

			wp_register_script( 'photoswipe', get_template_directory_uri() . '/js/photoswipe.min.js', array(), '', true );

			wp_register_script( 'photoswipe-ui-default', get_template_directory_uri() . '/js/photoswipe-ui-default.min.js', array(), '', true );

			if ( is_singular( 'product' ) ) {
				wp_enqueue_script( 'photoswipe' );
				wp_enqueue_script( 'photoswipe-ui-default' );
			}

			wp_register_script( 'jquery.waitforimages', get_template_directory_uri() . '/js/jquery.waitforimages.min.js', array(),'', true );
			wp_register_script( 'isotope-pkgd', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '', true );
			wp_register_script( 'select2', get_template_directory_uri() . '/js/select2.min.js', array(), '', true );
			wp_register_script( 'resize-sensor', get_template_directory_uri() . '/js/ResizeSensor.min.js', array(), '', true );
			wp_register_script( 'theia-sticky-sidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.min.js', array(), '', true );

			wp_register_script( 'neal-script', get_template_directory_uri() . '/js/script.min.js', array(
					'jquery',
					'owl-carousel',
					'jquery-fitvids',
					'jquery-magnific-popup',
					'jquery.waitforimages',
					'imagesloaded',
					'isotope-pkgd',
					'select2',
					'resize-sensor',
					'theia-sticky-sidebar',
			), $version, true );

			wp_enqueue_script( 'neal-script' );

			wp_localize_script( 'neal-script', 'ajaxurl', admin_url( "admin-ajax.php" ) );

			$neal_localize_main_js 			= array();
			$neal_localize_main_js['lang']  = get_locale();

			wp_localize_script( 'neal-script', 'nealScript', $neal_localize_main_js );

			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

		}

		/**
		 * Registers an editor stylesheet for the theme.
		 */
		public function theme_add_editor_styles() {
		    add_editor_style( get_template_directory_uri() .'/css/custom-editor-style.css' );
		}

		/**
		 * Enqueue child theme stylesheet.
		 * A separate function is required as the child theme css needs to be enqueued _after_ the parent theme
		 * primary css and the separate WooCommerce css.
		 *
		 * @since  1.0.0
		 */
		public function child_scripts() {
			if ( is_child_theme() ) {
				wp_enqueue_style( 'neal-child-style', get_stylesheet_uri(), '' );
			}
		}

		/**
		 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
		 *
		 * @param array $args Configuration arguments.
		 * @return array
		 */
		public function page_menu_args( $args ) {
			$args['show_home'] = true;
			return $args;
		}

		/**
		 * Adds custom classes to the array of body classes.
		 *
		 * @param array $classes Classes for the body element.
		 * @return array
		 */
		public function body_classes( $classes ) {

			global $post;

			// Adds a class of group-blog to blogs with more than 1 published author.
			if ( is_multi_author() ) {
				$classes[] = 'group-blog';
			}

			/**
			 * What is this?!
			 * Take the blue pill, close this file and forget you saw the following code.
			 * Or take the red pill, filter neal_make_me_cute and see how deep the rabbit hole goes...
			 */
			$cute = apply_filters( 'neal_make_me_cute', false );

			if ( true === $cute ) {
				$classes[] = 'neal-cute';
			}

			// If our main sidebar doesn't contain widgets, adjust the layout to be full-width.
			if ( ! is_active_sidebar( 'sidebar-widget' ) ) {
				$classes[] = 'neal-full-width-content';
			}

			// If sticky header
			if ( neal_get_option( 'header_general-header-sticky' ) ) {
				$classes[] = 'sticky-header-active';
			}

			// If fixed footer
			if ( neal_get_option( 'footer_general-footer-fixed' ) ) {
				$classes[] = 'fixed-footer';
			}

			// Full featured post
			if ( is_page_template( 'template-homepage.php' ) ) {
				$full_f_post = get_post_meta( $post->ID, 'neal_full_featured_post_checkbox', true );
				if ( $full_f_post ) {
					$classes[] = 'full-featured-post';
				}
			}

			// Carousel posts
			if ( is_page_template( 'template-homepage.php' ) ) {
				$carousel_posts 	  = get_post_meta( $post->ID, 'neal_carousel_posts_checkbox', true );
				$carousel_posts_items = get_post_meta( $post->ID, 'neal_carousel_posts_items', true );
				
				if ( $carousel_posts && $carousel_posts_items['full_height'] ) {
					$classes[] = 'carousel-posts-full-height';
				}
			}

			// Masonry posts
			if ( is_page_template( 'template-homepage.php' ) ) {
				$masonry_posts 		 = get_post_meta( $post->ID, 'neal_masonry_posts_checkbox', true );
				$masonry_posts_items = get_post_meta( $post->ID, 'neal_masonry_posts_items', true );
				
				if ( $masonry_posts && $masonry_posts_items['full_header'] ) {
					$classes[] = 'masonry-posts-full-header';
				}
			}

			// Single post layouts
			if ( is_singular( 'post' ) ) {
				// get post meta data
				$items 		 = neal_get_post_options_data();
				$post_layout = $items['post_layouts'];

				$classes[] = 'post-layout-' . $post_layout;

				if ( has_post_thumbnail() ) {
					$classes[] = 'has-post-thumbnail';
				}
			}

			// Layout width
			if ( neal_get_option( 'body_layout-width') == 'boxed' ) {
				$classes[] = 'neal-boxed-layout';
			}

			// Load more navigation ajax
			if ( neal_get_option( 'pagination_navigation-pag-type' ) == 'load-more' ) {
				$classes[] = 'navigation-ajax';
			}

			// Infinite scroll navigation ajax
			if ( neal_get_option( 'pagination_navigation-pag-type' ) == 'infinite-scroll' ) {
				$classes[] = 'navigation-ajax infinite-scroll';
			}

			// Sticky sidebar on blog pages
			if ( neal_get_option( 'blog_sidebar-sticky' ) ) {
				$classes[] = 'sticky-sidebar-blog';
			}

			return $classes;
		}

		/**
		 * Adds custom classes to the array of nav menu classes.
		 *
		 * @param array $classes
		 * @return array
		 */
		public function nav_menu_classes( $classes ) {

			// add active item to menu items
			$active_classes = array(
				'current-menu-item',
				'current-menu-parent',
			);
			$is_active = array_intersect( $classes, $active_classes );

			if ( ! empty( $is_active ) && neal_get_option( 'menu_menu-items-styl-active-item' ) ) {
        		$classes[] = 'active';
    		}
   			
   			return $classes;
		}

		/**
		 * Add grid column classes for blog posts
		 *
		 * @since 1.0.0
		 * @param array  $classes
		 * @param string $class
		 * @param string $post_id
		 * @return array
		 */
		public function post_class( $classes, $class = '', $post_id = '' ) {

			if ( ! $post_id || get_post_type( $post_id ) !== 'post' || is_single( $post_id ) ) {
				return $classes;
			}

			if ( $class ) {
				return $classes;
			}

			$xs_num = 12;

			if ( neal_get_option('blog_layout-columns') == 'classic' || neal_get_option('blog_layout-columns') == 'list' ) {
				$lg_md_sm_num = ( 12 / 1 );
			} else {
				$lg_md_sm_num = ( 12 / intval( neal_get_option('blog_layout-columns') ) );
			}

			$columns = 'col-lg-' . $lg_md_sm_num .' col-md-' . $lg_md_sm_num .' col-sm-'. $lg_md_sm_num . ' col-xs-' . $xs_num;

			if ( ! is_search() ) {
   				$classes[] = $columns;
   			}

   			return $classes;
		}

		/**
		 * TGM Plugin activation
		 */
		function register_plugins() {

			/*
		     * Array of plugin arrays. Required keys are name and slug.
		     * If the source is NOT from the .org repo, then source is also required.
		    */

		    $plugins = array(
		    	array(
		            'name'               => 'Neal Extension',
		            'slug'               => 'neal-extension',
		            'source'             => get_template_directory() . '/plugins/neal-extension.zip',
		            'required'           => true
		        ),
		    	array(
		            'name'               => 'Neal Demo Importer',
		            'slug'               => 'neal-demo-importer',
		            'source'             => get_template_directory() . '/plugins/neal-demo-importer.zip',
		            'required'           => false
		        ),
		        array(
		            'name'               => 'WooCommerce',
		            'slug'               => 'woocommerce',
		            'required'           => false
		        ),
		        array(
		            'name'               => 'Jetpack',
		            'slug'               => 'jetpack',
		            'required'           => false
		        ),
		        array(
		            'name'               => 'Envato Market',
		            'slug'               => 'envato-market',
		            'source'             => get_template_directory() . '/plugins/envato-market.zip',
		            'required'           => false
		        )
		    );

			tgmpa( $plugins );

		}

		/**
		 * Get Google fonts
		 *
		 * @param string $font
		 */
		public function get_google_fonts( $font ) {
			// get data from theme customizer
			$typography = get_option( 'neal_typography' );

			// detect protocol
			$protocol = is_ssl() ? 'https' : 'http';

			// subsets
			$subsets = array();
			if ( $typography['subset-label'] === true ) {

				if ( $typography['subset-latin'] === true ) {
					$subsets[] = 'latin';
					$subsets[] = 'latin-ext';
				}

				if ( $typography['subset-cyrillic'] === true ) {
					$subsets[] = 'cyrillic';
					$subsets[] = 'cyrillic-ext';
				}

				if ( $typography['subset-greek'] === true ) {
					$subsets[] = 'greek';
					$subsets[] = 'greek-ext';
				}

				if ( $typography['subset-vietnamese'] === true ) {
					$subsets[] = 'vietnamese';
				}

				if ( $typography['subset-arabic'] === true ) {
					$subsets[] = 'arabic';
				}

				if ( $typography['subset-hindi'] === true ) {
					$subsets[] = 'hindi';
				}


				$subsets = '&subset='. implode( ",", $subsets );

			} else {
				$subsets = '';
			}

			$font_url = '';


			// get font url
			if ( 'off' !== _x( 'on', 'Google font: on or off', 'neal' ) ) {
				$font_url = $protocol .'://fonts.googleapis.com/css?family='. esc_html($font) .':100,200,300,400,500,600,700,800,900'.$subsets;
			}

			$font = str_replace( '+', '_', esc_html( $font ) );

			// register & enqueue
			wp_register_style( "neal_enqueue_$font", $font_url, array(), '1.0.0' );
			wp_enqueue_style( "neal_enqueue_$font" );
		}

		public function get_custom_fonts( $font ) {

			$custom_css = '';
			$get_custom_fonts = get_option( 'neal-custom-fonts' );

			if ( $get_custom_fonts ) {
				foreach ( $get_custom_fonts as $fontt ) {
					if ( $fontt['name'] == $font ) {
						$custom_css .= "
			                @font-face {
			                	font-family: {$fontt['name']};
			                	src: url({$fontt['url']});
			                }";
					}
				}

				wp_enqueue_style(
					'custom-fonts',
					get_template_directory_uri() . '/css/custom-fonts.css'
				);
			        

			    wp_add_inline_style( 'custom-fonts', $custom_css );
			}
		}

		/**
		 * Enqueue google fonts
		 */
		public function enqueue_google_fonts() {
			// Get 'font-family' Select values from theme customizer

			$body = get_option('neal_body');
			$logo = get_option('neal_logo');
			$menu = get_option('neal_menu');
			$blog_single = get_option('neal_blog_single');
			$typography = get_option('neal_typography');
			

			// load font only once
			$unique_fonts = array_unique( array(
				$body['general-font-family'],
				$logo['logo-text-font-family'],
				$menu['menu-items-font-family'],
				$blog_single['content-font-family'],
				$typography['paragraph-font-family'],
				$typography['h1-font-family'],
				$typography['h2-font-family'],
				$typography['h3-font-family'],
				$typography['h4-font-family'],
				$typography['h5-font-family'],
				$typography['h6-font-family']
			) );

			// websafe fonts
			$websafe = array(
				'Arial',
				'Georgia',
				'Tahoma',
				'Times New Roman',
				'Trebuchet MS',
				'Verdana'
			);

			// custom fonts
			$custom_fonts = array();
			$get_custom_fonts = get_option( 'neal-custom-fonts' );
			
			if ( $get_custom_fonts ) {
				foreach ( $get_custom_fonts as $font ) {
					$custom_fonts[] = $font['name'];
				}
			}

			// enqueue
			if ( ! empty( $unique_fonts ) ) {
				foreach( $unique_fonts as $unique_font ) {
					if ( ! in_array( $unique_font, $websafe ) && ! in_array( $unique_font, $custom_fonts )  ) {
						$this->get_google_fonts( $unique_font );
					}

					if ( in_array( $unique_font, $custom_fonts ) ) {
						$this->get_custom_fonts( $unique_font );
					}
				}
			}
		}

		/**
		 * Post like with ajax
		 */
		public function post_like() {
			$post_id 	= $_POST['post_id'];
			$get_ip 	= neal_get_ip();
			$get_ips 	= get_post_meta( $post_id, 'neal_post_like_user_ips', true );
			$list_ips 	= $get_ips ? $get_ips : array();
			$likes 		= get_post_meta( $post_id, 'neal_post_like', true );

			// is not liked
			if ( ! in_array( $get_ip, $get_ips ) ) {
				$list_ips[] = wp_filter_nohtml_kses( $get_ip );
				update_post_meta( $post_id, 'neal_post_like_user_ips', $list_ips );
				update_post_meta( $post_id, 'neal_post_like', ++$likes );
			
			} else { // is liked
				if ( ( $key = array_search( $get_ip, $get_ips ) ) !== false ) {
					 unset( $get_ips[ $key ] );
				}
				update_post_meta( $post_id, 'neal_post_like_user_ips', $get_ips );
				update_post_meta( $post_id, 'neal_post_like', --$likes );
			}
			
			wp_send_json( $likes );
			wp_die();
		}

		/**
		 * Homepage load more with ajax
		 */
		public function homepage_post_load_more() {
			$post_item      = esc_html( $_POST['postItem'] );
			$post_amount   	= intval( $_POST['postsPerPage'] );
			$post_orderby   = esc_html( $_POST['postOrderBy'] );
			$post_not_in	= explode( ',', $_POST['postNotIn'] );
			$post_category	= explode( ',', $_POST['postCategory'] );
			$post_tag		= explode( ',', $_POST['postTag'] );
			$post_ids		= explode( ',', $_POST['postIds'] );
			$post_offset 	= $post_amount * $_POST['nextPage'];
			$meta_category	= esc_html( $_POST['metaCategory'] );
			$meta_date		= esc_html( $_POST['metaDate'] );
			$meta_author	= esc_html( $_POST['metaAuthor'] );
			$post_excerpt	= intval( $_POST['postExcerpt'] );
			$read_more	    = esc_html( $_POST['readMore'] );
			$image_size		= 'neal-full-posts-image';

			// post classes
			$c_class = 4;
			if ( $post_item == '2_grid_posts' ) {
				$c_class = 6;
			} elseif ( $post_item == 'list_posts' ) {
				$c_class = 12;
				$image_size = 'neal-posts-list-image';
			}

			$p_classes = 'col-lg-'.$c_class.' col-md-'.$c_class.' col-sm-'.$c_class.' col-xs-12';

			$output = '';

			$args = array(
				'post_type'			=> 'post',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> $post_amount,
				'offset'			=> $post_offset,
				'order'   			=> $post_orderby,
				'post__not_in' 		=> get_option( 'sticky_posts' )
			);

			// random posts
			if ( $post_orderby == 'random' ) {
				$not_in = array_map( 'intval', $post_not_in );
				$args['post__not_in'] = $not_in;

				unset( $args['order'] );
				$args['orderby'] = 'rand';
			}

			// featured posts
			if ( isset( $_POST['featuredPost'] ) ) {
				$args['meta_key'] = 'neal_post_options_featured';
				$args['meta_value'] = 'yes';
			}

			// category
			if ( isset( $_POST['postCategory'] ) ) {
				$categories = array_map( 'intval', $post_category );
				$args['cat'] = $categories;
			}

			// tag
			if ( isset( $_POST['postTag'] ) ) {
				$tags = array_map( 'intval', $post_tag );
				$args['tag__in'] = $tags;
			}

			// post id
			if ( isset( $_POST['postIds'] ) ) {
				$args['post__in'] = $post_ids;
			}

			$wp_query = new WP_Query( $args );

			if ( $wp_query->have_posts() ) :
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
					$post_classes = implode( ' ', get_post_class( $p_classes ) );
					$output .= '<article id="post-'.get_the_ID().'" class="'.$post_classes.'">';
						$output .= '<div class="article-inner">';

						if ( $post_item == 'list_posts' ) {
							$output .= '<div class="row">';
								$output .= '<div class="post-left col-lg-6 col-md-6 col-sm-6 col-xs-12">';
						}
						
						// entry media
						if ( has_post_thumbnail() ) :
							$output .= '<div class="entry-media">';
											ob_start();
											neal_get_template_part( 'post-formats/content', get_post_format(), $image_size, $post_item ); 
											$output .= ob_get_clean();
							$output .= '</div>';
						endif;

						if ( $post_item == 'list_posts' ) {
								$output .= '</div>';
						}

						if ( $post_item == 'list_posts' ) {
								$output .= '<div class="post-right col-lg-6 col-md-6 col-sm-6 col-xs-12">';
						}
						
						// entry header
						$output .= '<div class="entry-header">';
							// category
							if ( $meta_category ) {
								$output .= neal_post_categories( false );
							}

							// date
							if ( $meta_date ) {
								$output .= neal_post_date( false, false );
							}

							// author
							if ( $meta_author ) {
								$output .= neal_post_author( false, false );
							}

							// title
							$output .= neal_post_title( false );

						$output .= '</div>';

						// post content
						if ( $post_excerpt != 0 ) {
							$output .= '<div class="entry-content">'. neal_custom_post_content( 'p_excerpt', $post_excerpt, false ) .'</div>';
						}

						// read more
						if ( $read_more ) {
							$output .= '<a href="' .esc_url( get_the_permalink() ) .'" class="read-more"><span>'. esc_html__( 'Read More', 'neal' ) .'</span></a>';
						}

						if ( $post_item == 'list_posts' ) {
								$output .= '</div>';
						}

						$output .= '<a href="'.esc_url( get_the_permalink() ).'" class="article-link"></a>';
						$output .= '</div>';
					$output .= '</article>';
				endwhile;
				wp_reset_postdata();
			endif;


			$html_temp = array(
				'html' 	=> $output
			);

			wp_send_json( $html_temp );
			wp_die();
		}

		/**
		 * Subscriptions form
		 */
		public function comment_sub( $args ) {
			$output = '<div class="comment-form-subscriptions">';
				$output .= $args;
			$output .= '</div>';

			return $output;
		}

	} //end Neal_Main class

endif;

return new Neal_Main();