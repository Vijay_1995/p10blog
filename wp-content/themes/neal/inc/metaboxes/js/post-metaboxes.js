jQuery(document).ready(function( $ ) {
	"use strict";

	var postFormat 		  = $("input[name=post_format]"),
		checkedPostFormat = $("input[name=post_format]:checked"),
		metaBoxes	      = $('div[id*=neal_pf]');

	metaBoxes.hide();
	var currentFormat = checkedPostFormat.val();
	$('#neal_pf_'+ currentFormat +'_post').show();

	var sFormat;
	postFormat.change(function () {
		sFormat = $( this ).val();
		metaBoxes.hide();

		$('#neal_pf_'+ sFormat +'_post').show();
	});


	// Gallery
	var galleryImagesIdE = $('#neal-gallery-images-id'),
		galleryImagesSrcE = $('#neal-gallery-images-src');

	if ( galleryImagesSrcE.val() !== '' ) {
		var galleryImagesIdVal  = galleryImagesIdE.val().split(','),
			galleryImagesSrcVal = galleryImagesSrcE.val().split(',');
		$.each( galleryImagesSrcVal, function( index ) {
			// load image
	        $( '.gallery-images' )
	        .append( '<li class="image" data-attachment_id="'+ galleryImagesIdVal[index] +'">' +
	        				'<img src="'+ galleryImagesSrcVal[index] +'" />' +
	        				'<a href="#" class="gallery-img-delete" title=""></a>' +
	        			'</li>');
		});
	}


	$( '.gallery-images-container .gallery-images ' ).sortable({
		items: 'li.image',
		cursor: 'move',
		scrollSensitivity: 40,
		forcePlaceholderSize: true,
		forceHelperSize: false,
		helper: 'clone',
		opacity: 0.65,
		start: function( event, ui ) {
			ui.item.css( 'background-color', '#f6f6f6' );
		},
		stop: function( event, ui ) {
			ui.item.removeAttr( 'style' );
		},
		update: function() {
			var galleryImagesIdV,
				galleryImagesSrcV;

			$( '.gallery-images-container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
				var imagesId  = $( this ).attr( 'data-attachment_id' ),
					imagesSrc = $( this ).find( 'img' ).attr( 'src' );

				galleryImagesIdV = galleryImagesIdV ? galleryImagesIdV + ',' + imagesId : imagesId;
				galleryImagesSrcV = galleryImagesSrcV ? galleryImagesSrcV + ',' + imagesSrc : imagesSrc;
			});

			galleryImagesIdE.val( galleryImagesIdV );
       		galleryImagesSrcE.val( galleryImagesSrcV );
		}
	});


	// Remove gallery images
	$( '.gallery-images-container .gallery-images' ).on( 'click', 'a.gallery-img-delete', function() {
		$( this ).closest( 'li.image' ).remove();

		var galleryImagesIdV,
			galleryImagesSrcV;

		$( '.gallery-images-container' ).find( 'ul li.image' ).css( 'cursor', 'default' ).each( function() {
			var imagesId = $( this ).attr( 'data-attachment_id' );
			var imagesSrc = $( this ).find( 'img' ).attr( 'src' );

			galleryImagesIdV = galleryImagesIdV ? galleryImagesIdV + ',' + imagesId : imagesId;
			galleryImagesSrcV = galleryImagesSrcV ? galleryImagesSrcV + ',' + imagesSrc : imagesSrc;
		});

		galleryImagesIdE.val( galleryImagesIdV );
       	galleryImagesSrcE.val( galleryImagesSrcV );

		return false;
	});

	// Video & Audio select type
	function nealSelectType( media ) {
		var selectType   = $( '.neal-select-' + media + '-type' ),
			selectedType = selectType.val();

		$( '.neal-' + media + '-' + selectedType ).show();
			
		selectType.change( function() {
			var type = $(this).val();
			$( '.neal-' + media ).hide();
			$( '.neal-' + media + '-' + type ).show();
		});
	}

	nealSelectType( 'video' ); // video
	nealSelectType( 'audio' ); // audio

	// post option customizer post
	$('#neal-post-options-customize-post').on( 'click', function () {
		$( this ).closest('.neal-post-options-customize-post')
		.find('.neal-post-options-customize-post-inner').slideToggle();
	});

	// post options dropdown
	$('.neal-post-options-dropdown-btn').on( 'click', function () {
		$( this ).closest('.neal-form-field').find('.neal-post-options-dropdown').slideToggle()
	});

});