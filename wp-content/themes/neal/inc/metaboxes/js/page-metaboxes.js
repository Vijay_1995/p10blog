jQuery(document).ready(function( $ ) {
	"use strict";

	var metaBoxes 		= $( '#neal_homepage_page' ),
		hPageMain 		= $( '.neal-homepage-settings' ),
		hPageItem 		= $( '.neal-homepage-item'),
		hPageHeader 	= hPageItem.find( '.neal-homepage-header' ),
		hPageItemInner	= hPageItem.find( '.neal-homepage-item-inner' ),
		checkbox 		= hPageHeader.find('input');

	hPageMain.sortable({
		connectWith: '.neal-homepage-settings',
    	handle: '.neal-homepage-header > h4',
    	axis: 'y',
		cursor: 'move',
		update: function() {
			var compoList = '',
				compoOrder = '';

			hPageMain.find( 'li' ).css( 'cursor', 'default' ).each( function() {;
				var liId = $(this).attr('id');
				compoList = compoList ? compoList + ',' + liId : liId;
				if ( $('.switch input[type="checkbox"]', this ).is(':checked') ) {
					compoOrder = compoOrder ? compoOrder + ',' + liId : liId;
				}
			});

			$( 'input#neal-homepage-compo-list' ).val( compoList );
			$( 'input#neal-homepage-compo-order' ).val( compoOrder );
		}
	});
   	
   	hPageMain.disableSelection();

    var componentListInput = $( 'input#neal-homepage-compo-list' ).val(),
    	componentList,
    	newComponentList = [],
    	componentsInput = [];


    if ( componentListInput ) {
    	componentList = componentListInput.split(',')

	    for ( var i = 0; i < componentList.length; i ++ ) {
	    	var liHtml = $( '#' + componentList[i] ).html(),
	    		li = '<li class="neal-homepage-item" id="'+ componentList[i] +'">' + liHtml + '</li>';

	    	newComponentList.push( li );
	    }

	    hPageMain.html( newComponentList );

	} else {

		$( '.neal-homepage-settings li' ).each(function () {
			componentsInput.push( $(this).attr('id') );
		});

		componentsInput = componentsInput.join( ',' );

		$( 'input#neal-homepage-compo-list' ).val( componentsInput );

	}

	metaBoxes.hide();
	hPageItemInner.hide();

	var val = $( '#page_template' ).val();
	if ( val == 'template-homepage.php' ) {
		metaBoxes.show();
	} else {
		metaBoxes.hide();
	}

	$( '#page_template' ).change(function() {
		var val = $( this ).val();
		if ( val == 'template-homepage.php' ) {
			metaBoxes.show();
		} else {
			metaBoxes.hide();
		}
	});

	$('body').on('change', '.neal-homepage-header input', function() {
		var inputThis = $( this );
		var components = [];
		var index = inputThis.index( '.neal-homepage-header input' );

		if ( inputThis.is(':checked') ) {
			inputThis.val(1);
			$('.neal-homepage-item-inner').eq( index ).stop().slideDown();
		} else {
			inputThis.val(0);
			$('.neal-homepage-item-inner').eq( index ).stop().slideUp();
			var getId = hPageMain.find('li').eq( index ).attr('id');
		}

		inputThis.parents( '.neal-homepage-settings' ).find( 'li' ).each( function () {
        	if ( $( '.switch > input', this ).is(':checked') ) {
        		var liId = $( this ).attr('id');
        		components.push( liId );
        	}
        });

      	components = components.join( ',' );

		$( 'input#neal-homepage-compo-order' ).val( components );
	});

	$('.item-toggle-btn').on( 'click', function () {
		var index = $( this ).index('.item-toggle-btn');
		$('.neal-homepage-item-inner').eq( index ).stop().slideToggle();
	});

	var cat_id = $('.neal-homepage-item-inner #neal_carousel_posts_category').data('cat-id');
	$( '.neal-homepage-item-inner #neal_carousel_posts_category option' ).each( function () {
		if ( $( this ).val() == cat_id ) {
			$( this ).attr( 'selected', 'selected' );
		}
	});

	// show categories & tags
	$('.neal_posts_source').change( function () {
		var form = $( this ).closest('.neal-form'),
			val	 = $( this ).val();
		form.find('.neal_posts_source_category,.neal_posts_source_tag').hide();
		form.find('.neal_posts_source_' + val ).show();
	});

	// show animations
	$('input#neal_carousel_posts_animate').on( 'click', function () {
		var form = $( this ).closest('.neal-form');
		form.find('.neal-list-animations').toggleClass('open');
	});

	// show display advertisement
	$('.neal-display-post-inner-adv').on( 'click', function () {
		var form = $( this ).closest('.neal-form');
		form.find('.display-adv').toggleClass('open');
	});
});