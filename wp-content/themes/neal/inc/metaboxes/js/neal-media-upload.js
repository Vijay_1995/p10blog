jQuery(document).ready(function( $ ) {
	"use strict";

	// gallery images upload
	var galleryImagesIdE  = $('#neal-gallery-images-id'),
		galleryImagesSrcE = $('#neal-gallery-images-src');

	$( '.neal-add-gallery-images a' ).on( 'click', function ( event ) {
		// Prevents default behaviour
        event.preventDefault();

        var mediaFrame = wp.media({
	        title: nealUploader.mediaTitle,
	        multiple: true,
	        library: {
	            type: 'image'
	        },
	        button: {
	            text: nealUploader.mediaButton
	        }
	    });

	    mediaFrame.on('select', function() {
        	var selection = mediaFrame.state().get( 'selection' ),
				galleryImagesIdV  = galleryImagesIdE.val(),
				galleryImagesSrcV = galleryImagesSrcE.val();

			selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					var attachmentImage = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
					galleryImagesIdV  	 = galleryImagesIdV ? galleryImagesIdV + ',' + attachment.id : attachment.id;
					galleryImagesSrcV    = galleryImagesSrcV ? galleryImagesSrcV + ',' + attachmentImage : attachmentImage;
					  
					// load image
        			$( '.gallery-images' )
        			.append( '<li class="image" data-attachment_id="' + attachment.id + '">' +
        					'<img src="'+ attachmentImage +'" />' +
        					'<a href="#" class="gallery-img-delete" title=""></a>' +
        				'</li>');


				}

				// set id
       			galleryImagesIdE.val( galleryImagesIdV );
       			galleryImagesSrcE.val( galleryImagesSrcV );
			});


    	});

        // open popup
        mediaFrame.open();

		return false;
	});

	// widget avatar image upload
	$('body').on( 'click', '.neal-widget-avatar-upload', function () {
		var btnThis = $( this );
		var bannerImg = btnThis.next( 'input#neal-widget-avatar' );
		
		// Prevents default behaviour
		event.preventDefault();

        var mediaFrame = wp.media({
	        title: nealUploader.mediaTitle,
	        multiple: false,
	        library: {
	            type: 'image'
	        },
	        button: {
	            text: nealUploader.mediaButton
	        }
	    });

	    mediaFrame.on('select', function() {
	    	var selection = mediaFrame.state().get( 'selection' );

	    	selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					var attachmentImage = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
					btnThis.prev( '.neal-widget-avatar' )
					.html( '<img src="' + attachmentImage + '">' );
				}

				bannerImg.val( attachment.id );
			});
	    });

	    // open popup
        mediaFrame.open();
	});

	// advertisement image upload
	$('body').on( 'click', '.neal-adv-image-upload', function ( event ) {
		var btnThis = $( this );
		var advImg = btnThis.next( 'input#neal-adv-image' );
		
		// Prevents default behaviour
		event.preventDefault();

        var mediaFrame = wp.media({
	        title: nealUploader.mediaTitle,
	        multiple: false,
	        library: {
	            type: 'image'
	        },
	        button: {
	            text: nealUploader.mediaButton
	        }
	    });

	    mediaFrame.on('select', function() {
	    	var selection = mediaFrame.state().get( 'selection' );

	    	selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					var attachmentImage = attachment.sizes && attachment.sizes.full ? attachment.sizes.full.url : attachment.url;
					btnThis.prev( '.adv-image-preview' )
					.html( '<img src="' + attachmentImage + '">' );
				}

				advImg.val( attachment.id );
			});
	    });

	    // open popup
        mediaFrame.open();
	});

	// avatar image remove
	$('body').on( 'click', '.neal-avatar-image-remove', function ( event ) {
		$( this ).prev().prev( '.image-prew' ).find('img').remove();
		$( this ).next( 'input#neal-avatar-img' ).val('');
	});

	// Video & Audio Upload
	function nealVideoAudioUpload( type ) {

		// video | audio upload button
		$( '.neal-' + type + '-upload-btn' ).on( 'click', function ( event ) {
			var btnThis = $( this );
			// Prevents default behaviour
	        event.preventDefault();

	         var mediaFrame = wp.media({
		        title: nealUploader.mediaTitle,
		        library: {
		            type: type
		        },
		        button: {
		            text: nealUploader.mediaButton
		        }
		    });

	        mediaFrame.on('select', function(){
	            var mediaAttachment = mediaFrame.state().get('selection').first().toJSON();
	            btnThis.prev( '#neal-' + type + '-self-hosted' ).val( mediaAttachment.url );
	        	
	        	btnThis.prev().prev( '.neal-' + type + '-prew' ).attr( 'src', mediaAttachment.url );
	        });

	        // open popup
	        mediaFrame.open();
		});
	}

	nealVideoAudioUpload( 'video' ); // video
	nealVideoAudioUpload( 'audio' ); // audio

});