<?php
/**
 * Metaboxes Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_MetaBoxes' ) ) :

	/**
	 * The Metaboxes class
	 */
	class Neal_MetaBoxes {

		/**
		 * Setup class.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
			add_action( 'admin_enqueue_scripts', 		array( $this, 'admin_enqueue_scripts' ) );

			// post formats
			add_action( 'add_meta_boxes',				array( $this, 'add_gallery_post' ) );
			add_action( 'add_meta_boxes',				array( $this, 'add_video_post' ) );
			add_action( 'add_meta_boxes',				array( $this, 'add_audio_post' ) );
			add_action( 'add_meta_boxes',				array( $this, 'add_link_post' ) );
			add_action( 'add_meta_boxes',				array( $this, 'add_quote_post' ) );

			add_action( 'add_meta_boxes',				array( $this, 'add_post_options' ) );
			add_action( 'add_meta_boxes',				array( $this, 'add_product_options' ) );

			// Homepage settings
			add_action( 'add_meta_boxes',				array( $this, 'add_homepage_settings' ) );

			// Display settings
			add_action( 'add_meta_boxes',				array( $this, 'add_display_settings_page' ) );
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function admin_enqueue_scripts( $hook ) {
			global $post;

			// custom text labels in Wp Media Uploader - ready for translation
		    $neal_localize_uploader = array(
		    	'mediaTitle' 	=> esc_html__( 'Add Media', 'neal' ),
		    	'mediaButton' 	=> esc_html__( 'Add Media', 'neal' )
		    );

		    // styles
			wp_register_style( 'neal-metabox-css', get_template_directory_uri() .'/inc/metaboxes/css/ui.css' );

			// scripts
			wp_register_script( 'neal-media-upload-js', get_template_directory_uri() .'/inc/metaboxes/js/neal-media-upload.js', array(), false, true );
			wp_enqueue_script( 'neal-media-upload-js' );
			wp_localize_script( 'neal-media-upload-js', 'nealUploader', $neal_localize_uploader );

			wp_register_script( 'neal-post-metabox-js', get_template_directory_uri() .'/inc/metaboxes/js/post-metaboxes.js', array(), false, true );
			wp_localize_script( 'neal-post-metabox-js', 'nealUploader', $neal_localize_uploader );

			wp_register_script( 'neal-page-metabox-js', get_template_directory_uri() .'/inc/metaboxes/js/page-metaboxes.js', array(), false, true );

			wp_enqueue_script( 'jquery-ui-sortable' );

			// enqueue scripts & styles
	    	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {

	    		wp_enqueue_style( 'neal-metabox-css' );
	    		
		        if ( 'post' === $post->post_type ) {  
		            wp_enqueue_script( 'neal-post-metabox-js' );
		        } elseif ( 'page' === $post->post_type ) {
	        		wp_enqueue_script( 'neal-page-metabox-js' );
	        	}
		    }
		}
		

		/**
		 * Post Format: Gallery
		 *
		 * @since 1.0.0
		 */
		public function add_gallery_post() {
			add_meta_box( 
		    	'neal_pf_gallery_post', 
		    	esc_html__( 'Post Format: Gallery', 'neal' ),
		    	'neal_gallery_post_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Post Format: Video
		 *
		 * @since 1.0.0
		 */
		public function add_video_post() {
			add_meta_box( 
		    	'neal_pf_video_post', 
		    	esc_html__( 'Post Format: Video', 'neal' ),
		    	'neal_video_post_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Post Format: Audio
		 *
		 * @since 1.0.0
		 */
		public function add_audio_post() {
			add_meta_box( 
		    	'neal_pf_audio_post', 
		    	esc_html__( 'Post Format: Audio', 'neal' ),
		    	'neal_audio_post_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Post Format: Link
		 *
		 * @since 1.0.0
		 */
		public function add_link_post() {
			add_meta_box( 
		    	'neal_pf_link_post', 
		    	esc_html__( 'Post Format: Link', 'neal' ),
		    	'neal_link_post_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Post Format: Quote
		 *
		 * @since 1.0.0
		 */
		public function add_quote_post() {
			add_meta_box( 
		    	'neal_pf_quote_post', 
		    	esc_html__( 'Post Format: Quote', 'neal' ),
		    	'neal_quote_post_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Post Options
		 *
		 * @since 1.0.0
		 */
		public function add_post_options() {
			add_meta_box( 
		    	'neal_post_options', 
		    	esc_html__( 'Post Options', 'neal' ),
		    	'neal_post_options_function', 
		    	'post', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Product Options
		 *
		 * @since 1.0.0
		 */
		public function add_product_options() {
			add_meta_box( 
		    	'neal_prdouct_options', 
		    	esc_html__( 'Product Options', 'neal' ),
		    	'neal_product_options_function', 
		    	'product', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Homepage Settings
		 *
		 * @since 1.0.0
		 */
		public function add_homepage_settings() {
			add_meta_box( 
		    	'neal_homepage_page', 
		    	esc_html__( 'Homepage Settings', 'neal' ),
		    	'neal_homepage_page_function', 
		    	'page', 
		    	'normal', 
		    	'high' 
		    );
		}

		/**
		 * Display Settings
		 *
		 * @since 1.0.0
		 */
		public function add_display_settings_page() {
			add_meta_box( 
		    	'neal_display_settings_page', 
		    	esc_html__( 'Display Settings', 'neal' ),
		    	'neal_display_settings_page_function', 
		    	'page', 
		    	'normal', 
		    	'high' 
		    );
		}

	} //end Neal_MetaBoxes class

endif;

return new Neal_MetaBoxes();