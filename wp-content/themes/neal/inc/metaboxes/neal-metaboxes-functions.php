<?php
/**
 * Metaboxes functions.
 *
 * @package neal
 */


/**
 * Post Format: Gallery (Display Function)
 *
 * @since  1.0.0
 */
function neal_gallery_post_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'gallery_post_nonce' );

	// get post meta data
	$images_id  = get_post_meta( $post->ID, 'neal_gallery_images_id', true );
	$images_src = get_post_meta( $post->ID, 'neal_gallery_images_src', true );

	$html = '<div class="gallery-images-container">
				<ul class="gallery-images">
				</ul>

				<input type="hidden" name="neal_gallery_images_id" id="neal-gallery-images-id" value="'. esc_attr( $images_id ) .'" />
				<input type="hidden" name="neal_gallery_images_src" id="neal-gallery-images-src" value="'. esc_attr( $images_src ) .'" />

			</div>';
	$html .= '<p class="neal-add-gallery-images">
				<a href="#">'. esc_html__( 'Add Image to Gallery', 'neal' ) .'</a>
			</p>';

	echo ''. $html;
}

/**
 * Post Format: Gallery (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_gallery_post_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'gallery_post_nonce' ) ) {

		// gallery images id
		if ( isset( $_POST['neal_gallery_images_id'] ) ) {
			$valid_neal_gallery_img_ids = wp_filter_nohtml_kses( $_POST['neal_gallery_images_id'] );
			update_post_meta( $post_id, 'neal_gallery_images_id', $valid_neal_gallery_img_ids );
		}

		// gallery images src
		if ( isset( $_POST['neal_gallery_images_src'] ) ) {
			$valid_neal_gallery_imgs_src = esc_url_raw( $_POST['neal_gallery_images_src'] );
			update_post_meta( $post_id, 'neal_gallery_images_src', $valid_neal_gallery_imgs_src );
		}
	}
}

/**
 * Post Format: Video (Display Function)
 *
 * @since  1.0.0
 */
function neal_video_post_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'video_post_nonce' );

	// get post meta data
	$video_type  	= get_post_meta( $post->ID, 'neal_video_type', true );
	$video_embed 	= get_post_meta( $post->ID, 'neal_video_embed', true );
	$video_self_mp4 = get_post_meta( $post->ID, 'neal_video_self_mp4', true );
	$video_self_ogv = get_post_meta( $post->ID, 'neal_video_self_ogv', true );

	// selected option
	$selected_embed = ( ( isset ( $video_type ) ) ? selected( $video_type, 'embed-code', false ) : '' );
	$selected_self_hosted = ( ( isset ( $video_type ) ) ? selected( $video_type, 'self-hosted', false ) : '' );

	$html = '<div class="neal-form">';

		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-select-video-type">'. esc_html__( 'Select Video Type: ', 'neal' ) .'</label>';
			$html .= '</div>';
			
			$html .= '<div class="neal-form-right-col">';
				$html .= '<select class="neal-select-video-type" name="neal_video_type" id="neal-select-video-type">';
					$html .= '<option value="embed-code" '.$selected_embed.'>'.esc_html__( 'Embed Code', 'neal' ).'</option>';
					$html .= '<option value="self-hosted" '.$selected_self_hosted.'>'.esc_html__( 'Self-Hosted', 'neal' ).'</option>';
				$html .= '</select>';
			$html .= '</div>';
		$html .= '</div>';

		// embed code
		$html .= '<div class="neal-video-embed-code neal-video">';
			$html .= '<div class="neal-form-field">';
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal-video-embed">'. esc_html__( 'Enter Video Embed code: ', 'neal' ) .'</label>';
				$html .= '</div>';
				$html .= '<div class="neal-form-right-col">';
					$html .= '<textarea type="text" name="neal_video_embed" id="neal-video-embed" rows="8">'. esc_textarea( $video_embed ) .'</textarea>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';

		// self hosted
		$html .= '<div class="neal-video-self-hosted neal-video">';

			$html .= '<div class="neal-form-field">';
				// mp4
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal_video_embed">'. esc_html__( 'Upload Self-hosted Video (mp4): ', 'neal' ) .'</label>';
				$html .= '</div>';

				$html .= '<div class="neal-form-right-col">';

					$html .= '<video class="neal-video-prew" controls>
	  							<source src="'.esc_url( $video_self_mp4 ).'" type="video/mp4">
							  </video>';
					$html .= '<input type="hidden" name="neal_video_self_mp4" id="neal-video-self-hosted" value="'.esc_url( $video_self_mp4 ).'">';
					$html .= '<input type="button" class="neal-video-upload-btn button button-primary" value="'.esc_attr__( 'Upload Video', 'neal' ).'">';
				$html .= '</div>';
			$html .= '</div>';

			$html .= '<div class="neal-form-field">';
				// ogv
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal-video-self-hosted">'. esc_html__( 'Upload Self-hosted Video (ogv): ', 'neal' ) .'</label>';
				$html .= '</div>';

				$html .= '<div class="neal-form-right-col">';
					$html .= '<input type="text" name="neal_video_self_ogv" id="neal-video-self-hosted" value="'.esc_url( $video_self_ogv ).'">';
					$html .= '<input type="button" class="neal-video-upload-btn button button-primary" value="'.esc_attr__( 'Upload Video', 'neal' ).'">';
				$html .= '</div>';
			$html .= '</div>';

		$html .= '</div>';
	$html .= '</div>';

	echo ''. $html;
}

/**
 * Post Format: Video (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_video_post_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'video_post_nonce' ) ) {

		// video type
		if ( isset( $_POST['neal_video_type'] ) ) {

			$valid_neal_video_type = wp_filter_nohtml_kses( $_POST['neal_video_type'] );
			update_post_meta( $post_id, 'neal_video_type', $valid_neal_video_type );
		}

		// video embed code
		if ( isset( $_POST['neal_video_embed'] ) ) {
			// allowed html for ifame embed
			$allowed_html = array(
				'iframe' => array(
						    	'src'                   => array(),
						        'width'                 => array(),
						        'height'                => array(),
						        'frameborder'           => array(),
						        'allowfullscreen'       => array(),
						        'mozallowfullscreen'    => array(),
						        'webkitallowfullscreen' => array()
						    )
			);

			$valid_neal_video_embed = wp_kses( $_POST['neal_video_embed'], $allowed_html );
			update_post_meta( $post_id, 'neal_video_embed', $valid_neal_video_embed );
		}

		// video self hosted mp4
		if ( isset( $_POST['neal_video_self_mp4'] ) ) {
			$valid_neal_video_self_mp4 = esc_url_raw( $_POST['neal_video_self_mp4'] );
			update_post_meta( $post_id, 'neal_video_self_mp4', $valid_neal_video_self_mp4 );
		}

		// video self hosted ogv
		if ( isset( $_POST['neal_video_self_ogv'] ) ) {
			$valid_neal_video_self_ogv = esc_url_raw( $_POST['neal_video_self_ogv'] );
			update_post_meta( $post_id, 'neal_video_self_ogv', $valid_neal_video_self_ogv );
		}
	}
}

/**
 * Post Format: Audio (Display Function)
 *
 * @since  1.0.0
 */
function neal_audio_post_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'audio_post_nonce' );

	// get post meta data
	$audio_type  	= get_post_meta( $post->ID, 'neal_audio_type', true );
	$audio_embed 	= get_post_meta( $post->ID, 'neal_audio_embed', true );
	$audio_self_mp3 = get_post_meta( $post->ID, 'neal_audio_self_mp3', true );
	$audio_self_ogg = get_post_meta( $post->ID, 'neal_audio_self_ogg', true );

	// selected option
	$selected_embed = ( ( isset ( $audio_type ) ) ? selected( $audio_type, 'embed-code', false ) : '' );
	$selected_self_hosted = ( ( isset ( $audio_type ) ) ? selected( $audio_type, 'self-hosted', false ) : '' );

	$html = '<div class="neal-form">';

		$html .= '<div class="neal-form-field">';

			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-select-audio-type">'. esc_html__( 'Select Audio Type: ', 'neal' ) .'</label>';
			$html .= '</div>';
				
			$html .= '<div class="neal-form-right-col">';
				$html .= '<select class="neal-select-audio-type" name="neal_audio_type" id="neal-select-audio-type">';
					$html .= '<option value="embed-code" '.$selected_embed.'>'.esc_html__( 'Embed Code', 'neal' ).'</option>';
					$html .= '<option value="self-hosted" '.$selected_self_hosted.'>'.esc_html__( 'Self-Hosted', 'neal' ).'</option>';
				$html .= '</select>';
			$html .= '</div>';
		$html .= '</div>';

		// embed code
		$html .= '<div class="neal-audio-embed-code neal-audio">';
			$html .= '<div class="neal-form-field">';
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal-audio-embed">'. esc_html__( 'Enter Audio Embed code: ', 'neal' ) .'</label>';
				$html .= '</div>';
				$html .= '<div class="neal-form-right-col">';
					$html .= '<textarea type="text" name="neal_audio_embed" id="neal-audio-embed" rows="8">'. esc_textarea( $audio_embed ) .'</textarea>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';

		// self hosted
		$html .= '<div class="neal-audio-self-hosted neal-audio">';
			$html .= '<div class="neal-form-field">';
				// mp4
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal_audio_embed">'. esc_html__( 'Upload Self-hosted Audio (mp3): ', 'neal' ) .'</label>';
				$html .= '</div>';

				$html .= '<div class="neal-form-right-col">';

					$html .= '<audio class="neal-audio-prew" controls>
	  							<source src="'.esc_url( $audio_self_mp3 ).'" type="audio/mpeg">
							  </audio>';
					$html .= '<input type="hidden" name="neal_audio_self_mp3" id="neal-audio-self-hosted" value="'.esc_url( $audio_self_mp3 ).'">';
					$html .= '<input type="button" class="neal-audio-upload-btn button button-primary" value="'.esc_attr__( 'Upload Audio', 'neal' ).'">';
				$html .= '</div>';
			$html .= '</div>';

			$html .= '<div class="neal-form-field">';
				// ogg
				$html .= '<div class="neal-form-left-col">';
					$html .= '<label for="neal-audio-self-hosted">'. esc_html__( 'Upload Self-hosted Audio (ogg): ', 'neal' ) .'</label>';
				$html .= '</div>';
				
				$html .= '<div class="neal-form-right-col">';
					$html .= '<input type="text" name="neal_audio_self_ogg" id="neal-audio-self-hosted" value="'.esc_url( $audio_self_ogg ).'">';
					$html .= '<input type="button" class="neal-audio-upload-btn button button-primary" value="'.esc_attr__( 'Upload Audio', 'neal' ).'">';
				$html .= '</div>';
			$html .= '</div>';

		$html .= '</div>';
	$html .= '</div>';

	echo ''. $html;
}

/**
 * Post Format: Audio (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_audio_post_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'audio_post_nonce' ) ) {

		// audio type
		if ( isset( $_POST['neal_audio_type'] ) ) {

			$valid_neal_audio_type = wp_filter_nohtml_kses( $_POST['neal_audio_type'] );
			update_post_meta( $post_id, 'neal_audio_type', $valid_neal_audio_type );
		}

		// audio embed code
		if ( isset( $_POST['neal_audio_embed'] ) ) {
			// allowed html for ifame embed
			$allowed_html = array(
				'iframe' => array(
						    	'src'			=> array(),
								'width'			=> array(),
								'height'		=> array(),
								'frameborder' 	=> array(),
								'scrolling'		=> array()
						    )
			);

			$valid_neal_audio_embed = wp_kses( $_POST['neal_audio_embed'], $allowed_html );
			update_post_meta( $post_id, 'neal_audio_embed', $valid_neal_audio_embed );
		}

		// audio self hosted mp3
		if ( isset( $_POST['neal_audio_self_mp3'] ) ) {
			$valid_neal_audio_self_mp3 = esc_url_raw( $_POST['neal_audio_self_mp3'] );
			update_post_meta( $post_id, 'neal_audio_self_mp3', $valid_neal_audio_self_mp3 );
		}

		// audio self hosted ogg
		if ( isset( $_POST['neal_audio_self_ogg'] ) ) {
			$valid_neal_audio_self_ogg = esc_url_raw( $_POST['neal_audio_self_ogg'] );
			update_post_meta( $post_id, 'neal_audio_self_ogg', $valid_neal_audio_self_ogg );
		}
	}
}


/**
 * Post Format: Link (Display Function)
 *
 * @since  1.0.0
 */
function neal_link_post_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'link_post_nonce' );

	// get post meta data
	$link_description 	= get_post_meta( $post->ID, 'neal_link_description', true );
	$link_title 		= get_post_meta( $post->ID, 'neal_link_title', true );
	$link_url 			= get_post_meta( $post->ID, 'neal_link_url', true );

	$html = '<div class="neal-form">';
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-link-description" class="link-desc-label">'. esc_html__( 'Enter Link Description: ', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<textarea type="text" name="neal_link_description" id="neal-link-description" rows="8">'. esc_textarea( $link_description ) .'</textarea>';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-link-title" class="link_title_label">'. esc_html__( 'Enter Link Title: ', 'neal' ) .'</label>';
			$html .= '</div>';
			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="text" name="neal_link_title" id="neal-link-title" value="'. esc_attr( $link_title ) .'" />';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-link-url" class="link_url_label">'. esc_html__( 'Enter Link URL: ', 'neal' ) .'</label>';
			$html .= '</div>';
			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="text" name="neal_link_url" id="neal-link-url" value="'. esc_attr( $link_url ) .'" />';
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';

	echo ''. $html;
}

/**
 * Post Format: Link (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_link_post_function( $post_id ) {
	if ( neal_user_can_save( $post_id, 'link_post_nonce' ) ) {

		// link description
		if ( isset( $_POST['neal_link_description'] ) ) {

			// strip all html tag
			$valid_neal_link_description = wp_filter_nohtml_kses( $_POST['neal_link_description'] );

			// save or update value
			update_post_meta( $post_id, 'neal_link_description', $valid_neal_link_description );
		}

		// link title
		if ( isset( $_POST['neal_link_title'] ) ) {

			// strip all html tag
			$valid_neal_link_title = wp_filter_nohtml_kses( $_POST['neal_link_title'] );

			// save or update value
			update_post_meta( $post_id, 'neal_link_title', $valid_neal_link_title );
		}

		// link url
		if ( isset( $_POST['neal_link_url'] ) ) {

			// sanitizing url
			$valid_neal_link_url = esc_url_raw( $_POST['neal_link_url'] );

			// save or update value
			update_post_meta( $post_id, 'neal_link_url', $valid_neal_link_url );
		}
	}

}

/**
 * Post Format: Quote (Display Function)
 *
 * @since  1.0.0
 */
function neal_quote_post_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'quote_post_nonce' );

	// get post meta data
	$quote_content 	= get_post_meta( $post->ID, 'neal_quote_content', true );
	$quote_title 	= get_post_meta( $post->ID, 'neal_quote_title', true );

	$html = '<div class="neal-form">';
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-quote-content" class="quote_content_label">'. esc_html__( 'Enter Quote: ', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<textarea type="text" name="neal_quote_content" id="neal-quote-content" rows="8">'. esc_textarea( $quote_content ) .'</textarea>';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal-quote-title" class="quote_title_label">'. esc_html__( 'Enter Quote Author: ', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="text" name="neal_quote_title" id="neal-quote-title" value="'. esc_attr( $quote_title ) .'" />';
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';

	echo ''. $html;
}

/**
 * Post Format: Quote (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_quote_post_function( $post_id ) {
	if ( neal_user_can_save( $post_id, 'quote_post_nonce' ) ) {

		// quote content
		if ( isset( $_POST['neal_quote_content'] ) ) {

			// strip all html tag
			$valid_neal_quote_content = wp_filter_nohtml_kses( $_POST['neal_quote_content'] );

			// save or update value
			update_post_meta( $post_id, 'neal_quote_content', $valid_neal_quote_content );
		}

		// quote title
		if ( isset( $_POST['neal_quote_title'] ) ) {

			// strip all html tag
			$valid_neal_quote_content = wp_filter_nohtml_kses( $_POST['neal_quote_title'] );

			// save or update value
			update_post_meta( $post_id, 'neal_quote_title', $valid_neal_quote_content );
		}
	}
}

/**
 * Post Options
 *
 * @since  1.0.0
 */
function neal_post_options_function( $post ) {
	// security check
	wp_nonce_field( 'neal_nonce', 'post_options_nonce' );

	// get post meta data
	$featured_post  = get_post_meta( $post->ID, 'neal_post_options_featured', true );
	$items 			= get_post_meta( $post->ID, 'neal_post_options_items', true );

	// get theme customizer data
	$blog_single = get_option( 'neal_blog_single' );

	$defaults = array(
		'customize_post'        => 0,
		'post_layouts'			=> $blog_single['general-post-layout'],
		'parallax_image'		=> $blog_single['general-post-parallax'],
		'sidebar'				=> $blog_single['sidebar-label'],
		'sidebar_align'			=> $blog_single['sidebar-align'],
		'sidebar_sticky'		=> $blog_single['sidebar-sticky'],
		'featured_image'		=> $blog_single['featured-img-label'],
		'title_pos'				=> $blog_single['title-pos'],
		'title_align'			=> $blog_single['title-align'],
		'meta'					=> $blog_single['meta-label'],
		'meta_pos'				=> $blog_single['meta-pos'],
		'meta_category'			=> $blog_single['meta-category'],
		'meta_date'				=> $blog_single['meta-date'],
		'meta_author'			=> $blog_single['meta-author'],
		'meta_align'			=> $blog_single['meta-align'],
		'header_social_media' 	=> false,
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_customize_post = ( ( isset ( $customize_post) ) ? checked( $customize_post, 1, false ) : '' );

	$checked_parallax = ( ( isset ( $parallax_image ) ) ? checked( $parallax_image, 1, false ) : '' );

	$checked_feat_post = ( ( isset ( $featured_post ) ) ? checked( $featured_post, 'yes', false ) : '' );
	$checked_feat_img = ( ( isset ( $featured_image ) ) ? checked( $featured_image, 1, false ) : '' );

	$checked_sidebar 	= ( ( isset ( $sidebar ) ) ? checked( $sidebar, 1, false ) : '' );
	$checked_sidebar_s 	= ( ( isset ( $sidebar_sticky ) ) ? checked( $sidebar_sticky, 1, false ) : '' );

	$checked_meta 			= ( ( isset ( $meta ) ) ? checked( $meta, 1, false ) : '' );
	$checked_meta_category  = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 1, false ) : '' );
	$checked_meta_date  	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 1, false ) : '' );
	$checked_meta_author  	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 1, false ) : '' );

	$checked_header_social_media = ( ( isset ( $header_social_media ) ) ? checked( $header_social_media, 1, false ) : '' );

	// selected option
	$select_p_layout_1 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 1, false ) : '' );
	$select_p_layout_2 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 2, false ) : '' );
	$select_p_layout_3 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 3, false ) : '' );
	$select_p_layout_4 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 4, false ) : '' );
	$select_p_layout_5 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 5, false ) : '' );
	$select_p_layout_6 = ( ( isset ( $post_layouts ) ) ? selected( $post_layouts, 6, false ) : '' );

	$select_s_align_right = ( ( isset ( $sidebar_align ) ) ? selected( $sidebar_align, 'sidebar-right', false ) : '' );
	$select_s_align_left    = ( ( isset ( $sidebar_align ) ) ? selected( $sidebar_align, 'sidebar-left', false ) : '' );

	$select_t_pos_below    = ( ( isset ( $title_pos ) ) ? selected( $title_pos, 'below', false ) : '' );
	$select_t_pos_above    = ( ( isset ( $title_pos ) ) ? selected( $title_pos, 'above', false ) : '' );
	$select_t_pos_in_media = ( ( isset ( $title_pos ) ) ? selected( $title_pos, 'in-media', false ) : '' );
	$select_t_align_left    = ( ( isset ( $meta_align ) ) ? selected( $title_align, 'left', false ) : '' );
	$select_t_align_center    = ( ( isset ( $meta_align ) ) ? selected( $title_align, 'center', false ) : '' );
	$select_t_align_right = ( ( isset ( $meta_align ) ) ? selected( $title_align, 'right', false ) : '' );

	$select_m_pos_below    = ( ( isset ( $meta_pos ) ) ? selected( $meta_pos, 'below', false ) : '' );
	$select_m_pos_above    = ( ( isset ( $meta_pos ) ) ? selected( $meta_pos, 'above', false ) : '' );
	$select_m_pos_in_media = ( ( isset ( $meta_pos ) ) ? selected( $meta_pos, 'in-media', false ) : '' );
	$select_m_align_left    = ( ( isset ( $meta_align ) ) ? selected( $meta_align, 'left', false ) : '' );
	$select_m_align_center    = ( ( isset ( $meta_align ) ) ? selected( $meta_align, 'center', false ) : '' );
	$select_m_align_right = ( ( isset ( $meta_align ) ) ? selected( $meta_align, 'right', false ) : '' );

	$html = '<div class="neal-form">';
		$html .= '<div class="neal-form-field">';
			$html .= '<div style="margin-bottom:10px;">';
				$html .= '<label for="neal-post-options-featured">';
					$html .= '<input type="checkbox" name="neal_post_options_featured" id="neal-post-options-featured" value="yes" '.$checked_feat_post.'>';
					$html .= esc_html__( 'Featured Post', 'neal' );
				$html .= '</label>';
			$html .= '</div>';

			// post layout
			$html .= '<div class="neal-post-options-customize-post">';
				$html .= '<div class="neal-form-heading">';
					$html .= '<label for="neal-post-options-customize-post">';
						$html .= '<h4>';
							$html .= '<input type="checkbox" name="neal_post_options_customize_post" id="neal-post-options-customize-post" value="1" '.$checked_customize_post.'>';
							$html .= esc_html( 'Customize Post', 'neal' );
						$html .= '</h4>';
					$html .= '</label>';
				$html .= '</div>';

			$html .= '<div class="neal-post-options-customize-post-inner '.( $customize_post ? 'open' : '').'">';
				// post layouts
				$html .= '<div style="margin-bottom:10px;">';
					$html .= '<label for="neal-post-options-post-layouts">';
						$html .= esc_html__( 'Post Layouts', 'neal' );
						$html .= '<select id="neal-post-options-post-layouts" name="neal_post_options_post_layouts">';
							$html .= '<option value="1" '.$select_p_layout_1.'>'.esc_html__( 'Layout 1', 'neal' ).'</option>';
							$html .= '<option value="2" '.$select_p_layout_2.'>'.esc_html__( 'Layout 2', 'neal' ).'</option>';
							$html .= '<option value="3" '.$select_p_layout_3.'>'.esc_html__( 'Layout 3', 'neal' ).'</option>';
							$html .= '<option value="4" '.$select_p_layout_4.'>'.esc_html__( 'Layout 4', 'neal' ).'</option>';
							$html .= '<option value="5" '.$select_p_layout_5.'>'.esc_html__( 'Layout 5', 'neal' ).'</option>';
							$html .= '<option value="6" '.$select_p_layout_6.'>'.esc_html__( 'Layout 6', 'neal' ).'</option>';
						$html .= '</select>';
					$html .= '</label>';
				$html .= '</div>';

				// parallax
				$html .= '<div style="margin-bottom:10px;">';
					$html .= '<label for="neal-post-options-parallax">';
						$html .= '<input type="checkbox" name="neal_post_options_parallax" id="neal-post-options-parallax" value="1" '.$checked_parallax.'>';
						$html .= esc_html__( 'Parallax Image', 'neal' );
					$html .= '</label>';
				$html .= '</div>';

				// sidebar
				$html .= '<div class="neal-form-field" style="margin-bottom:10px;">';
					$html .= '<label for="neal-post-options-sidebar">';
						$html .= '<input type="checkbox" class="neal-post-options-dropdown-btn" name="neal_post_options_sidebar" id="neal-post-options-sidebar" value="1" '.$checked_sidebar.'>';
						$html .= esc_html__( 'Sidebar', 'neal' );
					$html .= '</label>';

					// dropdown
					$html .= '<div class="neal-post-options-dropdown '.( $sidebar ? 'open' : '').'">';

						// sidebar align
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-sidebar-align">';
								$html .= esc_html__( 'Align', 'neal' );
								$html .= '<select id="neal-post-options-sidebar-align" name="neal_post_options_sidebar_align">';
											$html .= '<option value="sidebar-right" '.$select_s_align_right.'>'.esc_html__( 'Right', 'neal' ).'</option>';
											$html .= '<option value="sidebar-left" '.$select_s_align_left.'>'.esc_html__( 'Left', 'neal' ).'</option>';
								$html .= '</select>';
							$html .= '</label>';
						$html .= '</div>';

						// sidebar sticky
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-sidebar-sticky">';
								$html .= '<input type="checkbox" name="neal_post_options_sidebar_sticky" id="neal-post-options-sidebar-sticky" value="1" '.$checked_sidebar_s.'>';
								$html .= esc_html__( 'Sticky', 'neal' );
							$html .= '</label>';
						$html .= '</div>';

					$html .= '</div>';

				$html .= '</div>';

				// featured image
				$html .= '<div style="margin-bottom:10px;">';
					$html .= '<label for="neal-post-options-featured-img">';
						$html .= '<input type="checkbox" name="neal_post_options_featured_img" id="neal-post-options-featured-img" value="1" '.$checked_feat_img.'>';
						$html .= esc_html__( 'Featured Image', 'neal' );
					$html .= '</label>';
				$html .= '</div>';

				// title
				$html .= '<div class="neal-form-field" style="margin-bottom:10px;">';
					$html .= '<label>';
						$html .= esc_html__( 'Title', 'neal' );
					$html .= '</label>';

					// dropdown
					$html .= '<div class="neal-post-options-dropdown open">';

						// title position
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-title-pos">';
								$html .= esc_html__( 'Position', 'neal' );
								$html .= '<select id="neal-post-options-title-pos" name="neal_post_options_title_pos">';
											$html .= '<option value="below" '.$select_t_pos_below.'>'.esc_html__( 'Below Media', 'neal' ).'</option>';
											$html .= '<option value="above" '.$select_t_pos_above.'>'.esc_html__( 'Above Media', 'neal' ).'</option>';
											$html .= '<option value="in-media" '.$select_t_pos_in_media.'>'.esc_html__( 'In Media', 'neal' ).'</option>';
								$html .= '</select>';
							$html .= '</label>';
						$html .= '</div>';

						// title align
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-title-align">';
								$html .= esc_html__( 'Align', 'neal' );
								$html .= '<select id="neal-post-options-title-align" name="neal_post_options_title_align">';
											$html .= '<option value="left" '.$select_t_align_left.'>'.esc_html__( 'Left', 'neal' ).'</option>';
											$html .= '<option value="center" '.$select_t_align_center.'>'.esc_html__( 'Center', 'neal' ).'</option>';
											$html .= '<option value="right" '.$select_t_align_right.'>'.esc_html__( 'Right', 'neal' ).'</option>';
								$html .= '</select>';
							$html .= '</label>';
						$html .= '</div>';

					$html .= '</div>';

				$html .= '</div>';

				// meta
				$html .= '<div class="neal-form-field" style="margin-bottom:10px;">';
					$html .= '<label for="neal-post-options-meta">';
						$html .= '<input type="checkbox" class="neal-post-options-dropdown-btn" name="neal_post_options_meta" id="neal-post-options-meta" value="1" '.$checked_meta.'>';
						$html .= esc_html__( 'Meta', 'neal' );
					$html .= '</label>';

					// dropdown
					$html .= '<div class="neal-post-options-dropdown '.( $meta ? 'open' : '').'">';

						// meta position
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-meta-pos">';
								$html .= esc_html__( 'Position', 'neal' );
								$html .= '<select id="neal-post-options-meta-pos" name="neal_post_options_meta_pos">';
											$html .= '<option value="below" '.$select_m_pos_below.'>'.esc_html__( 'Below Media', 'neal' ).'</option>';
											$html .= '<option value="above" '.$select_m_pos_above.'>'.esc_html__( 'Above Media', 'neal' ).'</option>';
											$html .= '<option value="in-media" '.$select_m_pos_in_media.'>'.esc_html__( 'In Media', 'neal' ).'</option>';
								$html .= '</select>';
							$html .= '</label>';
						$html .= '</div>';

						// meta category
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-meta-category">';
								$html .= '<input type="checkbox" name="neal_post_options_meta_category" id="neal-post-options-meta-category" value="1" '.$checked_meta_category.'>';
								$html .= esc_html__( 'Category', 'neal' );
							$html .= '</label>';
						$html .= '</div>';

						// meta date
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-meta-date">';
								$html .= '<input type="checkbox" name="neal_post_options_meta_date" id="neal-post-options-meta-date" value="1" '.$checked_meta_date.'>';
								$html .= esc_html__( 'Date', 'neal' );
							$html .= '</label>';
						$html .= '</div>';

						// meta author
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-meta-author">';
								$html .= '<input type="checkbox" name="neal_post_options_meta_author" id="neal-post-options-meta-author" value="1" '.$checked_meta_author.'>';
								$html .= esc_html__( 'Author', 'neal' );
							$html .= '</label>';
						$html .= '</div>';

						// meta align
						$html .= '<div style="margin-bottom:10px;">';
							$html .= '<label for="neal-post-options-meta-align">';
								$html .= esc_html__( 'Align', 'neal' );
								$html .= '<select id="neal-post-options-meta-align" name="neal_post_options_meta_align">';
											$html .= '<option value="left" '.$select_m_align_left.'>'.esc_html__( 'Left', 'neal' ).'</option>';
											$html .= '<option value="center" '.$select_m_align_center.'>'.esc_html__( 'Center', 'neal' ).'</option>';
											$html .= '<option value="right" '.$select_m_align_right.'>'.esc_html__( 'Right', 'neal' ).'</option>';
								$html .= '</select>';
							$html .= '</label>';
						$html .= '</div>';

					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

			// header
			$html .= '<div class="neal-form-heading">';
				$html .= '<h4>'.esc_html( 'Header', 'neal' ).'</h4>';
			$html .= '</div>';
			$html .= '<div style="margin-bottom:10px;">';
				$html .= '<label for="neal-post-options-header-social-media">';
					$html .= '<input type="checkbox" name="neal_post_options_header_social_media" id="neal-post-options-header-social-media" value="1" '.$checked_header_social_media.'>';
					$html .= esc_html__( 'Hide Header Social Media', 'neal' );
				$html .= '</label>';
			$html .= '</div>';

		$html .= '</div>';

	$html .= '</div>';

	echo ''. $html;
}

/**
 * Post Options (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_post_options_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'post_options_nonce' ) ) {

		$compo_items = array();

		// featured post
		if ( isset( $_POST['neal_post_options_featured'] ) ) {
			update_post_meta( $post_id, 'neal_post_options_featured', 'yes' );
		} else {
			update_post_meta( $post_id, 'neal_post_options_featured', '' );
		}

		if ( isset( $_POST['neal_post_options_customize_post'] ) ) {

			$compo_items['customize_post'] = 1;

			// post layouts
			if ( isset( $_POST['neal_post_options_post_layouts'] ) ) {
				// strip all html tag
				$valid_post_layout = intval( $_POST['neal_post_options_post_layouts'] );
				$compo_items['post_layouts'] = $valid_post_layout;
			}

			// parallax
			if ( isset( $_POST['neal_post_options_parallax'] ) ) {
				$compo_items['parallax_image'] = 1;
			} else {
				$compo_items['parallax_image'] = 0;	
			}

			// hide featured image
			if ( isset( $_POST['neal_post_options_featured_img'] ) ) {
				$compo_items['featured_image'] = 1;
			} else {
				$compo_items['featured_image'] = 0;
			}

			// sidebar
			if ( isset( $_POST['neal_post_options_sidebar'] ) ) {
				$compo_items['sidebar'] = 1;

				// sidebar align
				if ( isset( $_POST['neal_post_options_sidebar_align'] ) ) {
					$sidebar_align = wp_filter_nohtml_kses( $_POST['neal_post_options_sidebar_align'] );
					$compo_items['sidebar_align'] = $sidebar_align;
				}

				// sidebar sticky
				if ( isset( $_POST['neal_post_options_sidebar_sticky'] ) ) {
					$compo_items['sidebar_sticky'] = 1;
				} else {
					$compo_items['sidebar_sticky'] = 0;
				}

			} else {
				$compo_items['sidebar'] = 0;
			}

			// title
				// title position
				if ( isset( $_POST['neal_post_options_title_pos'] ) ) {
					$title_pos = wp_filter_nohtml_kses( $_POST['neal_post_options_title_pos'] );
					$compo_items['title_pos'] = $title_pos;
				}

				// title align
				if ( isset( $_POST['neal_post_options_title_align'] ) ) {
					$title_align = wp_filter_nohtml_kses( $_POST['neal_post_options_title_align'] );
					$compo_items['title_align'] = $title_align;
				}

			// meta
			if ( isset( $_POST['neal_post_options_meta'] ) ) {
				$compo_items['meta'] = 1;

				// meta position
				if ( isset( $_POST['neal_post_options_meta_pos'] ) ) {
					$meta_pos = wp_filter_nohtml_kses( $_POST['neal_post_options_meta_pos'] );
					$compo_items['meta_pos'] = $meta_pos;
				}

				// meta category
				if ( isset( $_POST['neal_post_options_meta_category'] ) ) {
					$compo_items['meta_category'] = 1;
				} else {
					$compo_items['meta_category'] = 0;
				}

				// meta date
				if ( isset( $_POST['neal_post_options_meta_date'] ) ) {
					$compo_items['meta_date'] = 1;
				} else {
					$compo_items['meta_date'] = 0;	
				}

				// meta author
				if ( isset( $_POST['neal_post_options_meta_author'] ) ) {
					$compo_items['meta_author'] = 1;
				} else {
					$compo_items['meta_author'] = 0;
				}

				// meta align
				if ( isset( $_POST['neal_post_options_meta_align'] ) ) {
					$meta_align = wp_filter_nohtml_kses( $_POST['neal_post_options_meta_align'] );
					$compo_items['meta_align'] = $meta_align;
				}

			} else {
				$compo_items['meta'] = 0;
			}
		} else {
			$compo_items['customize_post'] = 0;
		}

		// hide header social media
		if ( isset( $_POST['neal_post_options_header_social_media'] ) ) {
			$compo_items['header_social_media'] = 1;
		} else {
			$compo_items['header_social_media'] = 0;
		}

		// save or update values
		update_post_meta( $post_id, 'neal_post_options_items', $compo_items );
	}
}

/**
 * Product Options
 *
 * @since  1.0.0
 */
function neal_product_options_function( $post ) {
	// security check
	wp_nonce_field( 'neal_nonce', 'product_options_nonce' );

	// get post meta data
	$header_social_media = get_post_meta( $post->ID, 'neal_product_options_header_social_media', true );

	// checked
	$checked_header_social_media = ( ( isset ( $header_social_media ) ) ? checked( $header_social_media, 1, false ) : '' );

	$html = '<div class="neal-form">';
		$html .= '<div class="neal-form-field">';
			$html .= '<div style="margin-bottom:10px;">';
				$html .= '<label for="neal-product-options-header-social-media">';
					$html .= '<input type="checkbox" name="neal_product_options_header_social_media" id="neal-product-options-header-social-media" value="1" '.$checked_header_social_media.'>';
					$html .= esc_html__( 'Hide Header Social Media', 'neal' );
				$html .= '</label>';
			$html .= '</div>';

		$html .= '</div>';

	$html .= '</div>';

	echo ''. $html;
}

/**
 * Product Options (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_product_options_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'product_options_nonce' ) ) {

		// hide header social media
		if ( isset( $_POST['neal_product_options_header_social_media'] ) ) {
			update_post_meta( $post_id, 'neal_product_options_header_social_media', 1 );
		} else {
			update_post_meta( $post_id, 'neal_product_options_header_social_media', '' );	
		}
	}
}

/**
 * Homepage Settings (Display Function)
 *
 * @since  1.0.0
 */
function neal_homepage_page_function( $post ) {
	// security check
	wp_nonce_field( 'neal_nonce', 'homepage_page_nonce' );

	// get post meta data
	$compo_list  = get_post_meta( $post->ID, 'neal_homepage_compo_list', true );
	$compo_order = get_post_meta( $post->ID, 'neal_homepage_compo_order', true );

	$html = '<ul class="neal-homepage-settings">';

		// carousel posts
		$html .= neal_homepage_carousel_posts( $post );

		// full featured post
		$html .= neal_homepage_full_featured_post( $post );

		// masonry posts
		$html .= neal_homepage_masonry_posts( $post );

		// 2 grid posts
		$html .= neal_homepage_2_grid_posts( $post );

		// adv block
		$html .= neal_homepage_adv_block_1( $post );

		// full posts
		$html .= neal_homepage_full_posts( $post );

		// list posts
		$html .= neal_homepage_list_posts( $post );

		// adv block 2
		$html .= neal_homepage_adv_block_2( $post );

		// slider posts
		$html .= neal_homepage_slider_posts( $post );

		// 3 grid posts
		$html .= neal_homepage_3_grid_posts( $post );

		// adv block 3
		$html .= neal_homepage_adv_block_3( $post );

		// newsletter
		$html .= neal_homepage_newsletter( $post );

	$html .= '</ul>';

	$html .= '<input type="hidden" id="neal-homepage-compo-list" name="neal_homepage_compo_list" value="'.esc_attr($compo_list).'">';
	$html .= '<input type="hidden" id="neal-homepage-compo-order" name="neal_homepage_compo_order" value="'.esc_attr($compo_order).'">';

	echo ''. $html;
	
}

// carousel posts
function neal_homepage_carousel_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_carousel_posts_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_carousel_posts_items', true );

	$defaults = array(
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 10,
		'car_columns'	=> 1,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 0,
		'read_more'		=> '',
		'width'			=> 'center-width',
		'full_height'	=> 0,
		'layouts'		=> 1,
		'content_position' => 'in-media',
		'nav'			=> 0,
		'dots'			=> 1,
		'loop'			=> 0,
		'autoplay'		=> 0,
		'animate'		=> 0,
		'animate_in'	=> '',
		'animate_out'	=> '',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button  = ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_f_height 	= ( ( isset ( $full_height ) ) ? checked( $full_height, 1, false ) : '' );
	$checked_nav 	    = ( ( isset ( $nav ) ) ? checked( $nav, 1, false ) : '' );
	$checked_dots 	    = ( ( isset ( $dots ) ) ? checked( $dots, 1, false ) : '' );
	$checked_loop 	    = ( ( isset ( $loop ) ) ? checked( $loop, 1, false ) : '' );
	$checked_autoplay 	= ( ( isset ( $autoplay ) ) ? checked( $autoplay, 1, false ) : '' );
	$checked_animate 	= ( ( isset ( $animate ) ) ? checked( $animate, 1, false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc      = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc     = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random   = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );
	$select_w_c_width	= ( ( isset ( $width ) ) ? selected( $width, 'center-width', false ) : '' );
	$select_w_boxed     = ( ( isset ( $width ) ) ? selected( $width, 'boxed', false ) : '' );
	$select_w_full   	= ( ( isset ( $width ) ) ? selected( $width, 'full', false ) : '' );
	$select_layouts_1 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 1, false ) : '' );
	$select_layouts_2 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 2, false ) : '' );
	$select_layouts_3 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 3, false ) : '' );
	$select_c_pos_i_media 	= ( ( isset ( $content_position ) ) ? selected( $content_position, 'in-media', false ) : '' );
	$select_c_pos_a_media 	= ( ( isset ( $content_position ) ) ? selected( $content_position, 'above', false ) : '' );
	$select_c_pos_b_media 	= ( ( isset ( $content_position ) ) ? selected( $content_position, 'below', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_carousel_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_carousel_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Carousel Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_carousel_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';


							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_carousel_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_carousel_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_carousel_posts_amount" id="neal_carousel_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_car_columns">'.esc_html__( 'Carousel Columns:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_carousel_posts_car_columns" id="neal_carousel_posts_car_columns" value="'.esc_attr($car_columns).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_carousel_posts_sort_order" name="neal_carousel_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_carousel_posts_post_ids" id="neal_carousel_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_carousel_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_carousel_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_carousel_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_carousel_posts_post_excerpt" id="neal_carousel_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_carousel_posts_read_more" id="neal_carousel_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_width">'.esc_html__( 'Width:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_carousel_posts_width" name="neal_carousel_posts_width">';
								$html .= '<option value="center-width" '.$select_w_c_width.'>'.esc_html__( 'Center Width', 'neal' ).'</option>';
								$html .= '<option value="boxed" '.$select_w_boxed.'>'.esc_html__( 'Boxed', 'neal' ).'</option>';
								$html .= '<option value="full" '.$select_w_full.'>'.esc_html__( 'Full', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';

					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_full_height">'.esc_html__( 'Full Height:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_full_height" name="neal_carousel_posts_full_height" value="1" '.$checked_f_height.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';

					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_layouts">'.esc_html__( 'Layouts:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_carousel_posts_layouts" name="neal_carousel_posts_layouts">';
								$html .= '<option value="1" '.$select_layouts_1.'>'.esc_html__( 'Layout 1', 'neal' ).'</option>';
								$html .= '<option value="2" '.$select_layouts_2.'>'.esc_html__( 'Layout 2', 'neal' ).'</option>';
								$html .= '<option value="3" '.$select_layouts_3.'>'.esc_html__( 'Layout 3', 'neal' ).'</option>';
							$html .= '</select>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_content_position">'.esc_html__( 'Content Position:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_carousel_posts_content_position" name="neal_carousel_posts_content_position">';
								$html .= '<option value="in-media" '.$select_c_pos_i_media.'>'.esc_html__( 'In Media', 'neal' ).'</option>';
								$html .= '<option value="above" '.$select_c_pos_a_media.'>'.esc_html__( 'Above Media', 'neal' ).'</option>';
								$html .= '<option value="below" '.$select_c_pos_b_media.'>'.esc_html__( 'Below Media', 'neal' ).'</option>';
							$html .= '</select>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_nav">'.esc_html__( 'Carousel Nav:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_nav" name="neal_carousel_posts_nav" value="1" '.$checked_nav.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_dots">'.esc_html__( 'Carousel Dots:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_dots" name="neal_carousel_posts_dots" value="1" '.$checked_dots.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_loop">'.esc_html__( 'Carousel Loop:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_loop" name="neal_carousel_posts_loop" value="1" '.$checked_loop.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_autoplay">'.esc_html__( 'Carousel Autoplay:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_autoplay" name="neal_carousel_posts_autoplay" value="1" '.$checked_autoplay.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_animate">'.esc_html__( 'Carousel Animate:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_carousel_posts_animate" name="neal_carousel_posts_animate" value="1" '.$checked_animate.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';

							// list animations
							$html .= '<div class="neal-list-animations '.( $animate ? "open" : '' ).'">';
								$html .= neal_get_css_animations( array( 
											'name'			=> 'neal_carousel_posts',
											'id'			=> 'neal_carousel_posts',
											'an_in_val' 	=> $animate_in,
											'an_out_val'	=> $animate_out,
											'an_in_label'	=> esc_html__( 'Animations In', 'neal' ),
											'an_out_label'	=> esc_html__( 'Animations Out', 'neal' ),
										) );
							$html .= '</div>';

						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// full featured post
function neal_homepage_full_featured_post( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_full_featured_post_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_full_featured_post_items', true );

	$defaults = array(
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'read_more'		=> 'yes',
		'parallax'		=> 1,
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_parallax 	= ( ( isset ( $parallax ) ) ? checked( $parallax, 1, false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_full_featured_post">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_full_featured_post_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Full Featured Post', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_full_featured_post_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_full_featured_post_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_full_featured_post_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_featured_post_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_full_featured_post_post_excerpt" id="neal_full_featured_post_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_featured_post_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_full_featured_post_read_more" id="neal_full_featured_post_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_featured_post_parallax">'.esc_html__( 'Parallax Effect:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="checkbox" name="neal_full_featured_post_parallax" id="neal_full_featured_post_parallax" value="1" '.$checked_parallax.'>';
						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// masonry posts
function neal_homepage_masonry_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_masonry_posts_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_masonry_posts_items', true );

	$defaults = array(
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'layouts'		=> 1,
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'width'			=> 0,
		'full_header'	=> 1,
		'read_more'		=> 'yes',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_width 		= ( ( isset ( $width ) ) ? checked( $width, 1, false ) : '' );
	$checked_f_header 	= ( ( isset ( $full_header ) ) ? checked( $full_header, 1, false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc 		= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc 	= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random 	= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );
	$select_layouts_1 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 1, false ) : '' );
	$select_layouts_2 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 2, false ) : '' );
	$select_layouts_3 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 3, false ) : '' );
	$select_layouts_4 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 4, false ) : '' );
	$select_layouts_5 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 5, false ) : '' );
	$select_layouts_6 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 6, false ) : '' );
	$select_layouts_7 	= ( ( isset ( $layouts ) ) ? selected( $layouts, 7, false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_masonry_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_masonry_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Masonry Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_masonry_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_masonry_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_masonry_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_masonry_posts_sort_order" name="neal_masonry_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_layouts">'.esc_html__( 'Layouts:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_masonry_posts_layouts" name="neal_masonry_posts_layouts">';
								$html .= '<option value="1" '.$select_layouts_1.'>'.esc_html__( 'Layout 1', 'neal' ).'</option>';
								$html .= '<option value="2" '.$select_layouts_2.'>'.esc_html__( 'Layout 2', 'neal' ).'</option>';
								$html .= '<option value="3" '.$select_layouts_3.'>'.esc_html__( 'Layout 3', 'neal' ).'</option>';
								$html .= '<option value="4" '.$select_layouts_4.'>'.esc_html__( 'Layout 4', 'neal' ).'</option>';
								$html .= '<option value="5" '.$select_layouts_5.'>'.esc_html__( 'Layout 5', 'neal' ).'</option>';
								$html .= '<option value="6" '.$select_layouts_6.'>'.esc_html__( 'Layout 6', 'neal' ).'</option>';
								$html .= '<option value="7" '.$select_layouts_7.'>'.esc_html__( 'Layout 7', 'neal' ).'</option>';
							$html .= '</select>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_masonry_posts_post_ids" id="neal_3masonry_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_masonry_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_masonry_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_masonry_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_masonry_posts_post_excerpt" id="neal_masonry_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_width">'.esc_html__( 'Width:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_masonry_posts_width" name="neal_masonry_posts_width" value="1" '.$checked_width.'>'.esc_html__( 'Boxed', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_full_header">'.esc_html__( 'Full Header:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_masonry_posts_full_header" name="neal_masonry_posts_full_header" value="1" '.$checked_f_header.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_masonry_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_masonry_posts_read_more" id="neal_masonry_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// 2 grid posts
function neal_homepage_2_grid_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_2_grid_posts_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_2_grid_posts_items', true );

	$defaults = array(
		'title'			=> '',
		'title_fl'		=> 'yes',
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 2,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'read_more'		=> 'yes',
		'load_more'		=> '',
		'hover_like'	=> 'yes',
		'display_adv'	=> '',
		'adv_img'		=> '',
		'adv_url'		=> '',
		'adv_pos'		=> 'bottom',
		'adv_js_code'	=> '',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_title_fl 	 = ( ( isset ( $title_fl ) ) ? checked( $title_fl, 'yes', false ) : '' );
	$checked_m_category  = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	 = ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	 = ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	 = ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_lm_button 	= ( ( isset ( $load_more ) ) ? checked( $load_more, 'yes', false ) : '' );
	$checked_hover_like  = ( ( isset ( $hover_like ) ) ? checked( $hover_like, 'yes', false ) : '' );
	$checked_display_adv = ( ( isset ( $display_adv ) ) ? checked( $display_adv, 'yes', false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc 	    = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc     = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random   = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );
	$select_adv_pos_top    = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'top', false ) : '' );
	$select_adv_pos_bottom = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'bottom', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_2_grid_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_2_grid_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( '2 Grid Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_2_grid_posts_title" id="neal_2_grid_posts_title" value="'.esc_attr($title).'">';
							$html .= '<p class="form-description">'.esc_html__( 'Choose a title for the section of posts that describes them best. Leaving this field blank will not display any title.', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_amount">'.esc_html__( 'Title First Letter:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label for="neal_2_grid_posts_title_fl"><input type="checkbox" name="neal_2_grid_posts_title_fl" id="neal_2_grid_posts_title_fl" value="yes" '.$checked_title_fl.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_2_grid_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_2_grid_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_2_grid_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_2_grid_posts_amount" id="neal_2_grid_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_2_grid_posts_sort_order" name="neal_2_grid_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_2_grid_posts_post_ids" id="neal_2_grid_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_2_grid_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_2_grid_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_2_grid_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_2_grid_posts_post_excerpt" id="neal_2_grid_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_2_grid_posts_read_more" id="neal_2_grid_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_load_more">'.esc_html__( 'Load More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_2_grid_posts_load_more" id="neal_2_grid_posts_load_more" value="yes" '.$checked_lm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_hover_like">'.esc_html__( 'Hover Like:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_2_grid_posts_hover_like" name="neal_2_grid_posts_hover_like" value="yes" '.$checked_hover_like.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_2_grid_posts_display_adv">'.esc_html__( 'Display Advertisement:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';

							$html .= '<label><input type="checkbox" id="neal_2_grid_posts_display_adv" name="neal_2_grid_posts_display_adv" class="neal-display-post-inner-adv" value="yes" '.$checked_display_adv.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';

							$html .= '<div class="display-adv hidden '.( $display_adv ? "open" : '' ).'">';
								$html .= '<div class="adv-image-preview">';
									$html .= wp_get_attachment_image( $adv_img, 'full' );
								$html .= '</div>';

								$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
								$html .= '<input type="hidden" id="neal-adv-image" name="neal_2_grid_posts_adv_img" value="'.esc_attr($adv_img).'">';

								$html .= '<label>'.esc_html__( 'Adv Position', 'neal' ).'
								   <select name="neal_2_grid_posts_adv_pos">';
									$html .= '<option value="top" '.$select_adv_pos_top.'>'.esc_html__( 'Top', 'neal' ).'</option>';
									$html .= '<option value="bottom" '.$select_adv_pos_bottom.'>'.esc_html__( 'Bottom', 'neal' ).'</option>';
								$html .= '</select></label>';

								$html .= '<label>'.esc_html__( 'Adv Url', 'neal' ).'<input type="text" name="neal_2_grid_posts_adv_url" value="'.esc_attr($adv_url).'"></label>';

								$html .= '<label>'.esc_html__( 'JS Code', 'neal' ).'<textarea rows="10" name="neal_2_grid_posts_adv_js_code" id="neal_2_grid_posts_adv_js_code"></textarea></label>';
							$html .= '</div>';

						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// full posts
function neal_homepage_full_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_full_posts_checkbox', true );
	
	$items = get_post_meta( $post->ID, 'neal_full_posts_items', true );

	$defaults = array(
		'title'			=> '',
		'title_fl'		=> 'yes',
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 3,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'read_more'		=> 'yes',
		'load_more'		=> '',
		'hover_like'	=> 'yes',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_title_fl 	= ( ( isset ( $title_fl ) ) ? checked( $title_fl, 'yes', false ) : '' );
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_lm_button 	= ( ( isset ( $load_more ) ) ? checked( $load_more, 'yes', false ) : '' );
	$checked_hover_like = ( ( isset ( $hover_like ) ) ? checked( $hover_like, 'yes', false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_full_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_full_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Full Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_full_posts_title" id="neal_full_posts_title" value="'.esc_attr($title).'">';
							$html .= '<p class="form-description">'.esc_html__( 'Choose a title for the section of posts that describes them best. Leaving this field blank will not display any title.', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_amount">'.esc_html__( 'Title First Letter:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label for="neal_full_posts_title_fl"><input type="checkbox" name="neal_full_posts_title_fl" id="neal_full_posts_title_fl" value="yes" '.$checked_title_fl.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_full_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_full_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_full_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_full_posts_amount" id="neal_full_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_full_posts_sort_order" name="neal_full_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_full_posts_post_ids" id="neal_full_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_full_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_full_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_full_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_full_posts_post_excerpt" id="neal_full_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_full_posts_read_more" id="neal_full_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_load_more">'.esc_html__( 'Load More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_full_posts_load_more" id="neal_full_posts_load_more" value="yes" '.$checked_lm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_full_posts_hover_like">'.esc_html__( 'Hover Like:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_full_posts_hover_like" name="neal_full_posts_hover_like" value="yes" '.$checked_hover_like.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';


				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// list posts
function neal_homepage_list_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_list_posts_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_list_posts_items', true );

	$defaults = array(
		'title'			=> '',
		'title_fl'		=> 'yes',
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 3,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'read_more'		=> 'yes',
		'load_more'		=> '',
		'hover_like'	=> 'yes',
		'display_adv'	=> '',
		'adv_img'		=> '',
		'adv_url'		=> '',
		'adv_pos'		=> 'bottom',
		'adv_js_code'	=> '',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_title_fl 	= ( ( isset ( $title_fl ) ) ? checked( $title_fl, 'yes', false ) : '' );
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_lm_button 	= ( ( isset ( $load_more ) ) ? checked( $load_more, 'yes', false ) : '' );
	$checked_hover_like = ( ( isset ( $hover_like ) ) ? checked( $hover_like, 'yes', false ) : '' );
	$checked_display_adv = ( ( isset ( $display_adv ) ) ? checked( $display_adv, 'yes', false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );
	$select_adv_pos_top    = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'top', false ) : '' );
	$select_adv_pos_bottom = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'bottom', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_list_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_list_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'List Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_list_posts_title" id="neal_list_posts_title" value="'.esc_attr($title).'">';
							$html .= '<p class="form-description">'.esc_html__( 'Choose a title for the section of posts that describes them best. Leaving this field blank will not display any title.', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_amount">'.esc_html__( 'Title First Letter:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label for="neal_list_posts_title_fl"><input type="checkbox" name="neal_list_posts_title_fl" id="neal_list_posts_title_fl" value="yes" '.$checked_title_fl.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_list_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_list_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_list_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_list_posts_amount" id="neal_list_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_list_posts_sort_order" name="neal_list_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_list_posts_post_ids" id="neal_list_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_list_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_list_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_list_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_list_posts_post_excerpt" id="neal_list_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_list_posts_read_more" id="neal_list_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_load_more">'.esc_html__( 'Load More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_list_posts_load_more" id="neal_list_posts_load_more" value="yes" '.$checked_lm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_hover_like">'.esc_html__( 'Hover Like:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_list_posts_hover_like" name="neal_list_posts_hover_like" value="yes" '.$checked_hover_like.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_list_posts_display_adv">'.esc_html__( 'Display Advertisement:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';

							$html .= '<label><input type="checkbox" id="neal_list_posts_display_adv" name="neal_list_posts_display_adv" class="neal-display-post-inner-adv" value="yes" '.$checked_display_adv.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';

							$html .= '<div class="display-adv hidden '.( $display_adv ? "open" : '' ).'">';
								$html .= '<div class="adv-image-preview">';
									$html .= wp_get_attachment_image( $adv_img, 'full' );
								$html .= '</div>';

								$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
								$html .= '<input type="hidden" id="neal-adv-image" name="neal_list_posts_adv_img" value="'.esc_attr($adv_img).'">';

								$html .= '<label>'.esc_html__( 'Adv Position', 'neal' ).'
								   <select name="neal_list_posts_adv_pos">';
									$html .= '<option value="top" '.$select_adv_pos_top.'>'.esc_html__( 'Top', 'neal' ).'</option>';
									$html .= '<option value="bottom" '.$select_adv_pos_bottom.'>'.esc_html__( 'Bottom', 'neal' ).'</option>';
								$html .= '</select></label>';

								$html .= '<label>'.esc_html__( 'Adv Url', 'neal' ).'<input type="text" name="neal_list_posts_adv_url" value="'.esc_attr($adv_url).'"></label>';

								$html .= '<label>'.esc_html__( 'JS Code', 'neal' ).'<textarea rows="10" name="neal_list_posts_adv_js_code" id="neal_list_posts_adv_js_code"></textarea></label>';
							$html .= '</div>';

						$html .= '</div>';
					$html .= '</div>';


				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// slider
function neal_homepage_slider_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_slider_posts_checkbox', true );

	$items 		= get_post_meta( $post->ID, 'neal_slider_posts_items', true );

	$defaults = array(
		'title'			=> '',
		'title_fl'		=> '',
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 8,
		'car_columns'	=> 4,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 0,
		'read_more'		=> '',
		'nav'			=> 0,
		'dots'			=> 1,
		'loop'			=> 1,
		'autoplay'		=> 0,
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_title_fl 	= ( ( isset ( $title_fl ) ) ? checked( $title_fl, 'yes', false ) : '' );
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_nav		= ( ( isset ( $nav ) ) ? checked( $nav, 1, false ) : '' );
	$checked_dots		= ( ( isset ( $dots ) ) ? checked( $dots, 1, false ) : '' );
	$checked_loop 		= ( ( isset ( $loop ) ) ? checked( $loop, 1, false ) : '' );
	$checked_autoplay 	= ( ( isset ( $autoplay ) ) ? checked( $autoplay, 1, false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );
	$select_so_asc 		= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc 	= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random 	= ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_slider_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_slider_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Slider Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_slider_posts_title" id="neal_slider_posts_title" value="'.esc_attr($title).'">';
							$html .= '<p class="form-description">'.esc_html__( 'Choose a title for the section of posts that describes them best. Leaving this field blank will not display any title.', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_amount">'.esc_html__( 'Title First Letter:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label for="neal_slider_posts_title_fl"><input type="checkbox" name="neal_slider_posts_title_fl" id="neal_slider_posts_title_fl" value="yes" '.$checked_title_fl.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_slider_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_slider_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_slider_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_slider_posts_amount" id="neal_slider_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_hover_like">'.esc_html__( 'Carousel Columns:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_slider_posts_car_columns" id="neal_slider_posts_car_columns" value="'.esc_attr($car_columns).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_slider_posts_sort_order" name="neal_slider_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_slider_posts_post_ids" id="neal_slider_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_slider_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_slider_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_slider_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_slider_posts_post_excerpt" id="neal_slider_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_slider_posts_read_more" id="neal_slider_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_nav">'.esc_html__( 'Carousel Nav:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_slider_posts_nav" name="neal_slider_posts_nav" value="1" '.$checked_nav.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_dots">'.esc_html__( 'Carousel Dots:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_slider_posts_dots" name="neal_slider_posts_dots" value="1" '.$checked_dots.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_loop">'.esc_html__( 'Carousel Loop:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_slider_posts_loop" name="neal_slider_posts_loop" value="1" '.$checked_loop.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_slider_posts_autoplay">'.esc_html__( 'Carousel Autoplay:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_slider_posts_autoplay" name="neal_slider_posts_autoplay" value="1" '.$checked_autoplay.'>'.esc_html__( 'Yes / No', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// 3 grid posts
function neal_homepage_3_grid_posts( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_3_grid_posts_checkbox', true );

	$items = get_post_meta( $post->ID, 'neal_3_grid_posts_items', true );

	$defaults = array(
		'title'			=> '',
		'title_fl'		=> 'yes',
		'sort_by'		=> 'recent',
		'category'		=> '',
		'tag'			=> '',
		'amount'		=> 3,
		'sort_order'	=> 'desc',
		'post_ids'		=> '',
		'meta_category'	=> 'yes',
		'meta_date'		=> 'yes',
		'meta_author'	=> '',
		'post_excerpt'	=> 20,
		'read_more'		=> 'yes',
		'load_more'		=> '',
		'hover_like'	=> 'yes',
		'display_adv'	=> '',
		'adv_img'		=> '',
		'adv_url'		=> '',
		'adv_pos'		=> 'bottom',
		'adv_js_code'	=> '',
	);

	$items = wp_parse_args( $items, $defaults );
	extract( $items );

	// checked
	$checked_title_fl 	= ( ( isset ( $title_fl ) ) ? checked( $title_fl, 'yes', false ) : '' );
	$checked_m_category = ( ( isset ( $meta_category ) ) ? checked( $meta_category, 'yes', false ) : '' );
	$checked_m_date 	= ( ( isset ( $meta_date ) ) ? checked( $meta_date, 'yes', false ) : '' );
	$checked_m_author 	= ( ( isset ( $meta_author ) ) ? checked( $meta_author, 'yes', false ) : '' );
	$checked_rm_button 	= ( ( isset ( $read_more ) ) ? checked( $read_more, 'yes', false ) : '' );
	$checked_lm_button 	= ( ( isset ( $load_more ) ) ? checked( $load_more, 'yes', false ) : '' );
	$checked_hover_like = ( ( isset ( $hover_like ) ) ? checked( $hover_like, 'yes', false ) : '' );
	$checked_display_adv = ( ( isset ( $display_adv ) ) ? checked( $display_adv, 'yes', false ) : '' );

	// selected option
	$select_sb_recent   = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'recent', false ) : '' );
	$select_sb_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );
	$select_sb_category = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'category', false ) : '' );
	$select_sb_tag 		= ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'tag', false ) : '' );	
	$select_so_asc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'asc', false ) : '' );
	$select_so_desc = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'desc', false ) : '' );
	$select_so_random = ( ( isset ( $sort_order ) ) ? selected( $sort_order, 'random', false ) : '' );
	$select_adv_pos_top    = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'top', false ) : '' );
	$select_adv_pos_bottom = ( ( isset ( $adv_pos ) ) ? selected( $adv_pos, 'bottom', false ) : '' );

	$html = '<li class="neal-homepage-item" id="neal_3_grid_posts">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_3_grid_posts_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( '3 Grid Posts', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';
					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_3_grid_posts_title" id="neal_3_grid_posts_title" value="'.esc_attr($title).'">';
							$html .= '<p class="form-description">'.esc_html__( 'Choose a title for the section of posts that describes them best. Leaving this field blank will not display any title.', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_amount">'.esc_html__( 'Title First Letter:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label for="neal_3_grid_posts_title_fl"><input type="checkbox" name="neal_3_grid_posts_title_fl" id="neal_3_grid_posts_title_fl" value="yes" '.$checked_title_fl.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_carousel_posts_sort_by">'.esc_html__( 'Posts Source:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select class="neal_posts_source" name="neal_3_grid_posts_sort_by">';
								$html .= '<option value="recent" '.$select_sb_recent.'>'.esc_html__( 'Recent Posts', 'neal' ).'</option>';
								$html .= '<option value="featured" '.$select_sb_featured.'>'.esc_html__( 'Featured Posts', 'neal' ).'</option>';
								$html .= '<option value="category" '.$select_sb_category.'>'.esc_html__( 'Category', 'neal' ).'</option>';
								$html .= '<option value="tag" '.$select_sb_tag.'>'.esc_html__( 'Tag', 'neal' ).'</option>';
							$html .= '</select>';

							// categories
							$html .= '<select multiple="multiple" class="neal_posts_source_category '.( $sort_by == 'category' ? "open" : "" ).'" name="neal_3_grid_posts_category[]">';
								$html .= neal_get_categories_option( 0, $category );
							$html .= '</select>';

							// tags
							$html .= '<select multiple="multiple" class="neal_posts_source_tag '.( $sort_by == 'tag' ? "open" : "" ).'" name="neal_3_grid_posts_tag[]">';
								$html .= neal_get_tags( $tag );
							$html .= '</select>';

						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_amount">'.esc_html__( 'Posts Amount:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_3_grid_posts_amount" id="neal_3_grid_posts_amount" value="'.esc_attr($amount).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_sort_order">'.esc_html__( 'Sort Order:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<select id="neal_3_grid_posts_sort_order" name="neal_3_grid_posts_sort_order">';
								$html .= '<option value="asc" '.$select_so_asc.'>'.esc_html__( 'Ascending (A > Z)', 'neal' ).'</option>';
								$html .= '<option value="desc" '.$select_so_desc.'>'.esc_html__( 'Descending (Z > A)', 'neal' ).'</option>';
								$html .= '<option value="random" '.$select_so_random.'>'.esc_html__( 'Random', 'neal' ).'</option>';
							$html .= '</select>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_post_ids">'.esc_html__( 'Post IDs:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_3_grid_posts_post_ids" id="neal_3_grid_posts_post_ids" value="'.esc_attr($post_ids).'">';
							$html .= '<p class="form-description">'.esc_html__( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ).'</p>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label>'.esc_html__( 'Post Meta:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_3_grid_posts_meta_category" value="yes" '.$checked_m_category.'>'.esc_html__( 'Category', 'neal' ).'</label>';
							$html .= '<label style="margin-right:20px;"><input type="checkbox" name="neal_3_grid_posts_meta_date" value="yes" '.$checked_m_date.'>'.esc_html__( 'Date', 'neal' ).'</label>';
							$html .= '<label><input type="checkbox" name="neal_3_grid_posts_meta_author" value="yes" '.$checked_m_author.'>'.esc_html__( 'Author', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_post_excerpt">'.esc_html__( 'Post Excerpt:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="number" min="0" name="neal_3_grid_posts_post_excerpt" id="neal_3_grid_posts_post_excerpt" value="'.esc_attr($post_excerpt).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_read_more">'.esc_html__( 'Read More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_3_grid_posts_read_more" id="neal_3_grid_posts_read_more" value="yes" '.$checked_rm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_load_more">'.esc_html__( 'Load More Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" name="neal_3_grid_posts_load_more" id="neal_3_grid_posts_load_more" value="yes" '.$checked_lm_button.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_hover_like">'.esc_html__( 'Hover Like:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<label><input type="checkbox" id="neal_3_grid_posts_hover_like" name="neal_3_grid_posts_hover_like" value="yes" '.$checked_hover_like.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_3_grid_posts_display_adv">'.esc_html__( 'Display Advertisement:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';

							$html .= '<label><input type="checkbox" id="neal_3_grid_posts_display_adv" name="neal_3_grid_posts_display_adv" class="neal-display-post-inner-adv" value="yes" '.$checked_display_adv.'>'.esc_html__( 'Show / Hide', 'neal' ).'</label>';

							$html .= '<div class="display-adv hidden '.( $display_adv ? "open" : '' ).'">';
								$html .= '<div class="adv-image-preview">';
									$html .= wp_get_attachment_image( $adv_img, 'full' );
								$html .= '</div>';

								$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
								$html .= '<input type="hidden" id="neal-adv-image" name="neal_3_grid_posts_adv_img" value="'.esc_attr($adv_img).'">';

								$html .= '<label>'.esc_html__( 'Adv Position', 'neal' ).'
								   <select name="neal_3_grid_posts_adv_pos">';
									$html .= '<option value="top" '.$select_adv_pos_top.'>'.esc_html__( 'Top', 'neal' ).'</option>';
									$html .= '<option value="bottom" '.$select_adv_pos_bottom.'>'.esc_html__( 'Bottom', 'neal' ).'</option>';
								$html .= '</select></label>';

								$html .= '<label>'.esc_html__( 'Adv Url', 'neal' ).'<input type="text" name="neal_3_grid_posts_adv_url" value="'.esc_attr($adv_url).'"></label>';

								$html .= '<label>'.esc_html__( 'JS Code', 'neal' ).'<textarea rows="10" name="neal_3_grid_posts_adv_js_code" id="neal_3_grid_posts_adv_js_code"></textarea></label>';
							$html .= '</div>';

						$html .= '</div>';
					$html .= '</div>';


				$html .= '</div>';
			$html .= '</div>';

	$html .= '</li>';

	return $html;
}

// newsletter
function neal_homepage_newsletter( $post ) {

	// get post meta data
	$checkbox 	 = get_post_meta( $post->ID, 'neal_newsletter_checkbox', true );

	$title 		 = get_post_meta( $post->ID, 'neal_newsletter_title', true );
	$desc 		 = get_post_meta( $post->ID, 'neal_newsletter_desc', true );
	$placeholder = get_post_meta( $post->ID, 'neal_newsletter_placeholder', true );
	$button 	 = get_post_meta( $post->ID, 'neal_newsletter_button', true );

	$html = '<li class="neal-homepage-item" id="neal_newsletter">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_newsletter_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Newsletter', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_newsletter_title">'.esc_html__( 'Title:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_newsletter_title" id="neal_newsletter_title" value="'.esc_attr($title).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_newsletter_desc">'.esc_html__( 'Description:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_newsletter_desc" id="neal_newsletter_desc" value="'.esc_attr($desc).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_newsletter_placeholder">'.esc_html__( 'Subscribe Placeholder:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_newsletter_placeholder" id="neal_newsletter_placeholder" value="'.esc_attr($placeholder).'">';
						$html .= '</div>';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_newsletter_button">'.esc_html__( 'Subscribe Button:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_newsletter_button" id="neal_newsletter_button" value="'.esc_attr($button).'">';
						$html .= '</div>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';
	$html .= '</li>';

	return $html;
}

// advertisement block
function neal_homepage_adv_block_1( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_adv_block_1_checkbox', true );

	$adv_img 	= get_post_meta( $post->ID, 'neal_adv_block_1_img', true );
	$adv_url 	= get_post_meta( $post->ID, 'neal_adv_block_1_url', true );
	$js_code 	= get_post_meta( $post->ID, 'neal_adv_block_1_js_code', true );

	$html = '<li class="neal-homepage-item" id="neal_adv_block_1">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_adv_block_1_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Adv Block', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_1_img">'.esc_html__( 'Upload Adv Image:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<div class="adv-image-preview">';
								$html .= wp_get_attachment_image( $adv_img, 'full' );
							$html .= '</div>';
							$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
							$html .= '<input type="hidden" id="neal-adv-image" name="neal_adv_block_1_img" value="'.esc_attr($adv_img).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_1_url">'.esc_html__( 'Adv Url:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_adv_block_1_url" id="neal_adv_block_1_url" value="'.esc_attr($adv_url).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_1_js_code">'.esc_html__( 'Adv JS Code:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<textarea rows="10" name="neal_adv_block_1_js_code" id="neal_adv_block_1_js_code">'.$js_code.'</textarea>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';
	$html .= '</li>';

	return $html;
}

// advertisement block 2
function neal_homepage_adv_block_2( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_adv_block_2_checkbox', true );

	$adv_img 	= get_post_meta( $post->ID, 'neal_adv_block_2_img', true );
	$adv_url 	= get_post_meta( $post->ID, 'neal_adv_block_2_url', true );
	$js_code 	= get_post_meta( $post->ID, 'neal_adv_block_2_js_code', true );

	$html = '<li class="neal-homepage-item" id="neal_adv_block_2">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_adv_block_2_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Adv Block 2', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_2_img">'.esc_html__( 'Upload Adv Image:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<div class="adv-image-preview">';
								$html .= wp_get_attachment_image( $adv_img, 'full' );
							$html .= '</div>';
							$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
							$html .= '<input type="hidden" id="neal-adv-image" name="neal_adv_block_2_img" value="'.esc_attr($adv_img).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_2_url">'.esc_html__( 'Adv Url:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_adv_block_2_url" id="neal_adv_block_2_url" value="'.esc_attr($adv_url).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_2_js_code">'.esc_html__( 'Adv JS Code:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<textarea rows="10" name="neal_adv_block_2_js_code" id="neal_adv_block_2_js_code">'.$js_code.'</textarea>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';
	$html .= '</li>';

	return $html;
}

// advertisement block 3
function neal_homepage_adv_block_3( $post ) {

	// get post meta data
	$checkbox 	= get_post_meta( $post->ID, 'neal_adv_block_3_checkbox', true );

	$adv_img 	= get_post_meta( $post->ID, 'neal_adv_block_3_img', true );
	$adv_url 	= get_post_meta( $post->ID, 'neal_adv_block_3_url', true );
	$js_code 	= get_post_meta( $post->ID, 'neal_adv_block_3_js_code', true );

	$html = '<li class="neal-homepage-item" id="neal_adv_block_3">';
			$html .= '<div class="neal-homepage-header">
						<label class="switch">
							<input type="checkbox" name="neal_adv_block_3_checkbox" value="'.esc_attr($checkbox).'" '.($checkbox == 1 ? 'checked' : '').'>
							<div class="neal-slider round"></div>
						</label>
						<h4 class="item-toggle-btn">'.esc_html__( 'Adv Block 3', 'neal' ).'</h4>
					  </div>';
			$html .= '<div class="neal-homepage-item-inner">';

				$html .= '<div class="neal-form">';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_3_img">'.esc_html__( 'Upload Adv Image:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<div class="adv-image-preview">';
								$html .= wp_get_attachment_image( $adv_img, 'full' );
							$html .= '</div>';
							$html .= '<a href="#" class="button-primary neal-adv-image-upload">'.esc_html__( 'Add Media', 'neal' ).'</a>';
							$html .= '<input type="hidden" id="neal-adv-image" name="neal_adv_block_3_img" value="'.esc_attr($adv_img).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_3_url">'.esc_html__( 'Adv Url:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<input type="text" name="neal_adv_block_3_url" id="neal_adv_block_3_url" value="'.esc_attr($adv_url).'">';
					$html .= '</div>';

					$html .= '<div class="neal-form-field">';
						$html .= '<div class="neal-form-left-col">';
							$html .= '<label for="neal_adv_block_3_js_code">'.esc_html__( 'Adv JS Code:', 'neal' ).'</label>';
						$html .= '</div>';

						$html .= '<div class="neal-form-right-col">';
							$html .= '<textarea rows="10" name="neal_adv_block_3_js_code" id="neal_adv_block_3_js_code">'.$js_code.'</textarea>';
					$html .= '</div>';

				$html .= '</div>';
			$html .= '</div>';
	$html .= '</li>';

	return $html;
}

/**
 * Homepage Settings (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_homepage_page_function( $post_id ) {
	if ( neal_user_can_save( $post_id, 'homepage_page_nonce' ) ) {

		// homepage components list
		$valid_homepage_compo_list = esc_html( $_POST['neal_homepage_compo_list'] );

		// save or update value
		update_post_meta( $post_id, 'neal_homepage_compo_list', $valid_homepage_compo_list );

		// homepage components order
		$valid_homepage_compo_order = esc_html( $_POST['neal_homepage_compo_order'] );

		// save or update value
		update_post_meta( $post_id, 'neal_homepage_compo_order', $valid_homepage_compo_order );

		/* Checkboxes */

			$checkboxes = array(
				'neal_carousel_posts_checkbox',
				'neal_full_featured_post_checkbox',
				'neal_masonry_posts_checkbox',
				'neal_2_grid_posts_checkbox',
				'neal_full_posts_checkbox',
				'neal_list_posts_checkbox',
				'neal_slider_posts_checkbox',
				'neal_3_grid_posts_checkbox',
				'neal_newsletter_checkbox',
				'neal_adv_block_1_checkbox',
				'neal_adv_block_2_checkbox',
				'neal_adv_block_3_checkbox',
			);

			foreach ( $checkboxes as $checkbox ) {
				// valid value
				$valid_checkbox = intval( $_POST[ $checkbox ] );

				// save or update value
				update_post_meta( $post_id, $checkbox, $valid_checkbox );
			}

		// newsletter title
		if ( isset( $_POST['neal_newsletter_title'] ) ) {
			
			// strip all html tag
			$valid_title = wp_filter_nohtml_kses( $_POST['neal_newsletter_title'] );

			// save or update value
			update_post_meta( $post_id, 'neal_newsletter_title', $valid_title );
		}

		// newsletter description
		if ( isset( $_POST['neal_newsletter_desc'] ) ) {
			
			// strip all html tag
			$valid_desc = wp_filter_nohtml_kses( $_POST['neal_newsletter_desc'] );

			// save or update value
			update_post_meta( $post_id, 'neal_newsletter_desc', $valid_desc );
		}

		// newsletter placeholder
		if ( isset( $_POST['neal_newsletter_placeholder'] ) ) {
			
			// strip all html tag
			$valid_place = wp_filter_nohtml_kses( $_POST['neal_newsletter_placeholder'] );

			// save or update value
			update_post_meta( $post_id, 'neal_newsletter_placeholder', $valid_place );
		}

		// newsletter button
		if ( isset( $_POST['neal_newsletter_button'] ) ) {
			
			// strip all html tag
			$valid_button = wp_filter_nohtml_kses( $_POST['neal_newsletter_button'] );

			// save or update value
			update_post_meta( $post_id, 'neal_newsletter_button', $valid_button );
		}

		// full advertisements
		for ( $i = 1; $i <= 3; $i++ ) {

			// adv image
			$valid_adv_img =  intval( $_POST['neal_adv_block_'.$i.'_img'] );
			update_post_meta( $post_id, 'neal_adv_block_'.$i.'_img', $valid_adv_img );

			// adv url
			$valid_adv_url =  esc_url_raw( $_POST['neal_adv_block_'.$i.'_url'] );
			update_post_meta( $post_id, 'neal_adv_block_'.$i.'_url', $valid_adv_url );

			// js code
			$valid_js_code =  $_POST['neal_adv_block_'.$i.'_js_code'];
			update_post_meta( $post_id, 'neal_adv_block_'.$i.'_js_code', $valid_js_code );
		}

		// homepage posts
		$posts = array(
			'carousel_posts',
			'full_featured_post',
			'masonry_posts',
			'2_grid_posts',
			'full_posts',
			'list_posts',
			'slider_posts',
			'3_grid_posts'
		);

		$compo_items = array();

		foreach ( $posts as $post ) {
			// title
			if ( isset( $_POST['neal_' .$post. '_title'] ) ) {
				// strip all html tag
				$valid_title = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_title'] );

				$compo_items['title'] = $valid_title;

			}

			// title first letter
			if ( isset( $_POST['neal_' .$post. '_title_fl'] ) ) {
				$compo_items['title_fl'] = 'yes';
			} else {
				$compo_items['title_fl'] = '';
			}

			// sort by
			if ( isset( $_POST['neal_' .$post. '_sort_by'] ) ) {
				
				// strip all html tag
				$valid_sort_by = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_sort_by'] );

				$compo_items['sort_by'] = $valid_sort_by;

				// categories
				if ( isset( $_POST['neal_' .$post. '_category'] ) ) {
					$categories = array_map( 'intval', wp_unslash( $_POST['neal_' .$post. '_category'] ) );
					$compo_items['category'] = $categories;
				}

				// tags
				if ( isset( $_POST['neal_' .$post. '_tag'] ) ) {
					$tags = array_map( 'intval', wp_unslash( $_POST['neal_' .$post. '_tag'] ) );
					$compo_items['tag'] = $tags;
				}
			}

			// posts amount
			if ( isset( $_POST['neal_' .$post. '_amount'] ) ) {
				
				// strip all html tag
				$valid_posts_amount = intval( $_POST['neal_' .$post. '_amount'] );
				$compo_items['amount'] = $valid_posts_amount;
			}

			// carousel columns
			if ( $post == 'slider_posts' || $post == 'carousel_posts' ) {
				// strip all html tag
				$valid_car_columns = intval( $_POST['neal_' .$post. '_car_columns'] );
				$compo_items['car_columns'] = $valid_car_columns;
			}

			// sort order
			if ( isset( $_POST['neal_' .$post. '_sort_order'] ) ) {
				
				// strip all html tag
				$valid_sort_order = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_sort_order'] );
				$compo_items['sort_order'] = $valid_sort_order;
			}

			// post ids
			if ( isset( $_POST['neal_' .$post. '_post_ids'] ) ) {
				
				// strip all html tag
				$valid_post_ids= wp_filter_nohtml_kses( $_POST['neal_' .$post. '_post_ids'] );
				$compo_items['post_ids'] = $valid_post_ids;
			}

			// post meta: category
			if ( isset( $_POST['neal_' .$post. '_meta_category'] ) ) {
				$compo_items['meta_category'] = 'yes';
			} else {
				$compo_items['meta_category'] = '';
			}

			// post meta: date
			if ( isset( $_POST['neal_' .$post. '_meta_date'] ) ) {
				$compo_items['meta_date'] = 'yes';
			} else {
				$compo_items['meta_date'] = '';
			}

			// post meta: author
			if ( isset( $_POST['neal_' .$post. '_meta_author'] ) ) {
				$compo_items['meta_author'] = 'yes';
			} else {
				$compo_items['meta_author'] = '';
			}

			// post excerpt
			if ( isset( $_POST['neal_' .$post. '_post_excerpt'] ) ) {
				// strip all html tag
				$valid_post_excerpt = intval( $_POST['neal_' .$post. '_post_excerpt'] );
				$compo_items['post_excerpt'] = $valid_post_excerpt;
			}

			// read more button
			if ( isset( $_POST['neal_' .$post. '_read_more'] ) ) {
				$compo_items['read_more'] = 'yes';
			} else {
				$compo_items['read_more'] = '';
			}

			// read more button
			if ( isset( $_POST['neal_' .$post. '_load_more'] ) ) {
				$compo_items['load_more'] = 'yes';
			} else {
				$compo_items['load_more'] = '';
			}

			// hover like
			if ( isset( $_POST['neal_' .$post. '_hover_like'] ) ) {
				$compo_items['hover_like'] = 'yes';
			} else {
				$compo_items['hover_like'] = '';
			}

			// display advertisement
			if ( isset( $_POST['neal_' .$post. '_display_adv'] ) ) {
				$compo_items['display_adv'] = 'yes';
			} else {
				$compo_items['display_adv'] = '';
			}

			// display advertisement: image
			if ( isset( $_POST['neal_' .$post. '_adv_img'] ) ) {
				// strip all html tag
				$valid_adv_img = intval( $_POST['neal_' .$post. '_adv_img'] );
				$compo_items['adv_img'] = $valid_adv_img;
			}

			// display advertisement: position
			if ( isset( $_POST['neal_' .$post. '_adv_pos'] ) ) {
				// strip all html tag
				$valid_adv_pos = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_adv_pos'] );
				$compo_items['adv_pos'] = $valid_adv_pos;
			}

			// display advertisement: url
			if ( isset( $_POST['neal_' .$post. '_adv_url'] ) ) {
				// strip all html tag
				$valid_adv_url = esc_url_raw( $_POST['neal_' .$post. '_adv_url'] );
				$compo_items['adv_url'] = $valid_adv_url;
			}

			// display advertisement: js code
			if ( isset( $_POST['neal_' .$post. '_adv_js_code'] ) ) {
				// strip all html tag
				$valid_adv_js_code = json_decode( $_POST['neal_' .$post. '_adv_js_code'] );
				$compo_items['adv_js_code'] = $valid_adv_js_code;
			}

			// carousel nav
			if ( $post == 'slider_posts' || $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_nav'] ) ) {
					$compo_items['nav'] = 1;
				} else {
					$compo_items['nav'] = 0;
				}
			}

			// carousel dots
			if ( $post == 'slider_posts' || $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_dots'] ) ) {
					$compo_items['dots'] = 1;
				} else {
					$compo_items['dots'] = 0;
				}
			}

			// carousel loop
			if ( $post == 'slider_posts' || $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_loop'] ) ) {
					$compo_items['loop'] = 1;
				} else {
					$compo_items['loop'] = 0;
				}
			}

			// carousel autoplay
			if ( $post == 'slider_posts' || $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_autoplay'] ) ) {
					$compo_items['autoplay'] = 1;
				} else {
					$compo_items['autoplay'] = 0;
				}
			}

			// full featured post parallax
			if ( $post == 'full_featured_post' ) {
				if ( isset( $_POST['neal_' .$post. '_parallax'] ) ) {
					$compo_items['parallax'] = 1;
				} else {
					$compo_items['parallax'] = 0;
				}
			}

			// masonry posts width
			if ( $post == 'masonry_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_width'] ) ) {
					$compo_items['width'] = 1;
				} else {
					$compo_items['width'] = 0;
				}
			}

			// carousel posts width
			if ( $post == 'carousel_posts' || $post == 'masonry_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_width'] ) ) {
					// width
					$valid_width = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_width'] );
					$compo_items['width'] = $valid_width;
				}
			}

			// carousel posts full-height
			if ( $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_full_height'] ) ) {
					$compo_items['full_height'] = 1;
				} else {
					$compo_items['full_height'] = 0;
				}
			}

			// masonry posts layout
			if ( $post == 'masonry_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_full_header'] ) ) {
					$compo_items['full_header'] = 1;
				} else {
					$compo_items['full_header'] = 0;
				}
			}

			// carousel & masonry posts layouts
			if ( $post == 'carousel_posts' || $post == 'masonry_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_layouts'] ) ) {
					// layout
					$valid_layouts = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_layouts'] );
					$compo_items['layouts'] = $valid_layouts;
				}
			}

			// carousel posts content position
			if ( $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_content_position'] ) ) {
					// layout
					$valid_position = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_content_position'] );
					$compo_items['content_position'] = $valid_position;
				}
			}

			// carousel posts animations
			if ( $post == 'carousel_posts' ) {
				if ( isset( $_POST['neal_' .$post. '_animate'] ) ) {
					// animate
					$compo_items['animate'] = 1;

					// animate in
					$valid_animate_in = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_animate_in'] );
					$compo_items['animate_in'] = $valid_animate_in;

					// animate out
					$valid_animate_out = wp_filter_nohtml_kses( $_POST['neal_' .$post. '_animate_out'] );
					$compo_items['animate_out'] = $valid_animate_out;
				} else {
					// animate
					$compo_items['animate'] = 0;
				}
			}

			// save or update values
			update_post_meta( $post_id, 'neal_' .$post. '_items', $compo_items );
		}

	}
}


/**
 * Display Settings (Display Function)
 *
 * @since  1.0.0
 */
function neal_display_settings_page_function( $post ) {

	// security check
	wp_nonce_field( 'neal_nonce', 'display_settings_page_nonce' );

	// get post meta data
	$hide_header_social_media = get_post_meta( $post->ID, 'neal_hide_header_social_media', true );
	$hide_page_header 		  = get_post_meta( $post->ID, 'neal_hide_page_header', true );
	$hide_breadcrumbs         = get_post_meta( $post->ID, 'neal_hide_breadcrumbs', true );
	$hide_page_title          = get_post_meta( $post->ID, 'neal_hide_page_title', true );


	// checked
	$checked_hide_header_social_media = ( ( isset ( $hide_header_social_media ) ) ? checked( $hide_header_social_media, 1, false ) : '' );
	$checked_hide_page_header = ( ( isset ( $hide_page_header ) ) ? checked( $hide_page_header, "yes", false ) : '' );
	$checked_breadcrumbs = ( ( isset ( $hide_breadcrumbs ) ) ? checked( $hide_breadcrumbs, "yes", false ) : '' );
	$checked_hide_page_title = ( ( isset ( $hide_page_title ) ) ? checked( $hide_page_title, "yes", false ) : '' );

	$html = '<div class="neal-form">';

		$html .= '<div class="neal-form-heading">';
			$html .= '<h4>'.esc_html( 'Header', 'neal' ).'</h4>';
		$html .= '</div>';

		// hide social media
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal_hide_header_social_media">'. esc_html__( 'Hide Social Media', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="checkbox" id="neal_hide_header_social_media" name="neal_hide_header_social_media" value="1" '.$checked_hide_header_social_media.'/>';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="neal-form-heading">';
			$html .= '<h4>'.esc_html( 'Page Header', 'neal' ).'</h4>';
		$html .= '</div>';

		// hide page header
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal_hide_page_header">'. esc_html__( 'Hide Page Header', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="checkbox" id="neal_hide_page_header" name="neal_hide_page_header" value="yes" '.$checked_hide_page_header.'/>';
			$html .= '</div>';
		$html .= '</div>';

		// hide breadcrumbs
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal_hide_breadcrumbs">'. esc_html__( 'Hide Breadcrumbs', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="checkbox" id="neal_hide_breadcrumbs" name="neal_hide_breadcrumbs" value="yes" '.$checked_breadcrumbs.' />';
			$html .= '</div>';
		$html .= '</div>';

		// hide page title
		$html .= '<div class="neal-form-field">';
			$html .= '<div class="neal-form-left-col">';
				$html .= '<label for="neal_hide_page_title">'. esc_html__( 'Hide Page Title', 'neal' ) .'</label>';
			$html .= '</div>';

			$html .= '<div class="neal-form-right-col">';
				$html .= '<input type="checkbox" id="neal_hide_page_title" name="neal_hide_page_title" value="yes" '.$checked_hide_page_title.' />';
			$html .= '</div>';
		$html .= '</div>';

	$html .= '</div>';

	echo ''. $html;
}

/**
 * Display Settings (Save | Update Function)
 *
 * @since  1.0.0
 */
function neal_save_display_settings_page_function( $post_id ) {

	if ( neal_user_can_save( $post_id, 'display_settings_page_nonce' ) ) {

		// hide header social media
		if ( isset( $_POST['neal_hide_header_social_media'] ) ) {
			update_post_meta( $post_id, 'neal_hide_header_social_media', 1 );
		} else {
			update_post_meta( $post_id, 'neal_hide_header_social_media', '' );
		}

		// hide page header
		if ( isset( $_POST['neal_hide_page_header'] ) ) {
			update_post_meta( $post_id, 'neal_hide_page_header', 'yes' );
		} else {
			update_post_meta( $post_id, 'neal_hide_page_header', '' );
		}

		// hide breadcrumbs
		if ( isset( $_POST['neal_hide_breadcrumbs'] ) ) {
			update_post_meta( $post_id, 'neal_hide_breadcrumbs', 'yes' );
		} else {
			update_post_meta( $post_id, 'neal_hide_breadcrumbs', '' );
		}

		// hide page title
		if ( isset( $_POST['neal_hide_page_title'] ) ) {
			update_post_meta( $post_id, 'neal_hide_page_title', 'yes' );
		} else {
			update_post_meta( $post_id, 'neal_hide_page_title', '' );
		}

	}
}


/**
 * Determines whether or not the current user has the ability to save meta data associated with this post.
 * @author      Tom Mcfarlin
 * @param		int		$post_id	The ID of the post being save
 * @param		bool				Whether or not the user has the ability to save this post.
 * @link https://tommcfarlin.com/save-custom-post-meta-refactored/
 *
*/
function neal_user_can_save( $post_id, $nonce ) {
	
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ $nonce ] ) && wp_verify_nonce( $_POST[ $nonce ], 'neal_nonce' ) );
    
    // Return true if the user is able to save; otherwise, false.
    return ! ( $is_autosave || $is_revision ) && $is_valid_nonce;
} // end user_can_save


/* Save || Update Actions */

// post formats
add_action( 'save_post', 'neal_save_audio_post_function' ); // audio
add_action( 'save_post', 'neal_save_video_post_function' ); // video
add_action( 'save_post', 'neal_save_gallery_post_function' ); // gallery
add_action( 'save_post', 'neal_save_link_post_function' ); // link
add_action( 'save_post', 'neal_save_quote_post_function' ); // quote

add_action( 'save_post', 'neal_save_post_options_function' ); // post options
add_action( 'save_post', 'neal_save_product_options_function' ); // product options

// homepage settings
add_action( 'save_post', 'neal_save_homepage_page_function' );

// display settings
add_action( 'save_post', 'neal_save_display_settings_page_function' );