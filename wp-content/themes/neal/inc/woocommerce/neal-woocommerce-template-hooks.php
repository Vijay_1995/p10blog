<?php
/**
 * Neal WooCommerce hooks
 *
 * @package neal
 */


/**
 * Products
 */

// remove add to cart link
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );


/**
 * Layout
 *
 * @see  woocommerce_breadcrumb()
 * @see  neal_shop_messages()
 */

add_action( 'neal_content_top',  'woocommerce_breadcrumb',	10 );
add_action( 'neal_content_top',	'neal_shop_messages',	15 );


remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',		20 );
add_action( 'woocommerce_before_single_product', 'woocommerce_breadcrumb', 10 );

// result text remove
if ( ! neal_get_option( 'shop_result-t-label' ) ) {
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
}

// ordering remove
if ( ! neal_get_option( 'shop_ordering-label' ) ) {
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
}

/**
 * Others
 */

add_filter( 'woocommerce_product_reviews_tab_title', 'neal_wc_reviews_tab_link_text', 999, 2 );

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description' );

// product categories description
add_action( 'woocommerce_after_subcategory_title', 'neal_product_categories_description', 12 );