<?php
/**
 * WooCommerce Template Functions.
 *
 * @package neal
 */

if ( ! function_exists( 'neal_shop_messages' ) ) {
	/**
	 * neal shop messages
	 *
	 * @since   1.0.0
	 * @uses    neal_do_shortcode
	 */
	function neal_shop_messages() {
		if ( ! is_checkout() ) {
			echo wp_kses_post( neal_do_shortcode( 'woocommerce_messages' ) );
		}
	}
}

if ( ! function_exists( 'neal_wc_reviews_tab_link_text' ) ) {
	/**
	 * Change the "Reviews" tab link text for single products
	 *
	 * @since 1.0.0
	 */
	function neal_wc_reviews_tab_link_text( $text, $tab_key ) {
	 	global $product;

	 	$allowed_html = array(
	 		'span' => array()
	 	);

	    return sprintf( wp_kses( __( 'Reviews <span>%d</span>', 'neal' ), $allowed_html ), $product->get_review_count() );
	}
}

if ( ! function_exists( 'neal_product_categories_description' ) ) {
	/**
	 * Add description to Product categories
	 *
	 * @since 1.0.0
	 */
	function neal_product_categories_description( $category ) {
		$cat_id 	 = $category->term_id; // category id
		$prod_term   = get_term( $cat_id, 'product_cat' ); // product category
		$description = $prod_term->description; // category description

		echo '<span class="desc">'.esc_html( $description ).'</span>';
	}
}

if ( ! function_exists( 'neal_cross_sell_display' ) ) {
	/**
	 * Custom Cross Sells
	 *
	 * @since   1.0.0
	 */
	function neal_cross_sell_display() {
		$total   = 4;
		$columns = 4;
   		woocommerce_cross_sell_display( apply_filters('woocommerce_cross_sells_total', $total ), $columns );
	}
}