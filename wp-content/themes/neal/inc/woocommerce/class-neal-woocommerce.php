<?php
/**
 * Neal WooCommerce Class
 *
 * @package  neal
 * @author   TheSpan
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_WooCommerce' ) ) :

	/**
	 * The Neal WooCommerce Integration class
	 */
	class Neal_WooCommerce {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_filter( 'loop_shop_columns',							array( $this, 'loop_columns' ) );
			add_filter( 'body_class', 									array( $this, 'woocommerce_body_class' ) );
			add_action( 'wp_enqueue_scripts', 							array( $this, 'woocommerce_scripts' ),	20 );
			add_filter( 'woocommerce_enqueue_styles',					'__return_empty_array' );
			add_filter( 'woocommerce_output_related_products_args',		array( $this, 'related_products_args' ) );
			add_filter( 'woocommerce_product_thumbnails_columns',		array( $this, 'thumbnail_columns' ) );
			add_filter( 'loop_shop_per_page',							array( $this, 'products_per_page' ) );
			add_filter( 'post_class', 									array( $this, 'product_class' ), 30, 3 );
			
			// add to cart fragments
			add_filter( 'woocommerce_add_to_cart_fragments',			array( $this, 'add_to_cart_fragments' ) );
			add_action( 'wp_ajax_neal_get_mini_cart_fragments',		array( $this, 'get_mini_cart_fragments' ) );

			add_action( 'wp_ajax_nopriv_neal_get_mini_cart_fragments',	array( $this, 'get_mini_cart_fragments' ) );

			// remove shop page title
			add_filter( 'woocommerce_show_page_title', '__return_false' );

			// remove product link
			remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

			// remove badges
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
			remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );

			// add product thumbnail
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail' );
			add_action( 'woocommerce_before_shop_loop_item_title',		array( $this, 'product_content_thumbnail' ) );

			// remove product meta
			remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
			remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
			remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

			// change position product meta
			$this->product_meta_position();

			// add product buttons
			add_action( 'woocommerce_after_shop_loop_item', 			array( $this, 'product_buttons' ), 6 );

			// add shop toolbar
			add_action( 'woocommerce_before_shop_loop', 				array( $this, 'shop_toolbar' ), 5 );

			// show except in shop view list
			add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 5 );

			// change length of description
			add_filter( 'woocommerce_short_description', 				array( $this, 'short_description' ) );
			add_filter( 'woocommerce_short_description', 'wpautop' );

			// remove sidebar
			remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar' );
			add_action( 'woocommerce_sidebar',							array( $this, 'get_sidebar' ) );

			// comments avatar size
			add_filter( 'woocommerce_review_gravatar_size', array( $this, 'change_avatar_size' ) );

		}


		/**
		 * Default loop columns on product archives
		 *
		 * @return integer products per row
		 * @since  1.0.0
		 */
		public function loop_columns() {
			$columns = intval( neal_get_option( 'shop_layout-columns' ) );

			return apply_filters( 'neal_loop_columns', $columns ); // products per row
		}

		/**
		 * Add 'woocommerce-active' class to the body tag
		 *
		 * @param  array $classes css classes applied to the body tag.
		 * @return array $classes modified to include 'woocommerce-active' class
		 */
		public function woocommerce_body_class( $classes ) {
			
			if ( neal_is_woocommerce_activated() ) {
				$classes[] = 'woocommerce-active';
			}

			return $classes;
		}

		/**
		 * WooCommerce specific scripts & stylesheets
		 *
		 * @since 1.0.0
		 */
		public function woocommerce_scripts() {
			$version = wp_get_theme()->get('Version');

			wp_enqueue_style( 'neal-woocommerce-style', get_template_directory_uri() . '/css/woocommerce/woocommerce.css', $version );

			wp_register_script( 'neal-sticky-payment', get_template_directory_uri() . '/js/woocommerce/checkout.min.js', 'jquery', $version, true );

			if ( is_checkout() ) {
				wp_enqueue_script( 'neal-sticky-payment' );
			}
		}

		/**
		 * Related Products Args
		 *
		 * @param  array $args related products args.
		 * @since 1.0.0
		 * @return  array $args related products args
		 */
		public function related_products_args( $args ) {
			$args = apply_filters( 'neal_related_products_args', array(
				'posts_per_page' => 4,
				'columns'        => 4,
			) );

			return $args;
		}

		/**
		 * Product gallery thumnail columns
		 *
		 * @return integer number of columns
		 * @since  1.0.0
		 */
		public function thumbnail_columns() {
			return intval( apply_filters( 'neal_product_thumbnail_columns', 4 ) );
		}

		/**
		 * Products per page
		 *
		 * @return integer number of products
		 * @since  1.0.0
		 */
		public function products_per_page() {
			return intval( apply_filters( 'neal_products_per_page', 9 ) );
		}

		/**
		 * Add grid column classes for product
		 *
		 * @since 1.0.0
		 * @param array  $classes
		 * @param string $class
		 * @param string $post_id
		 * @return array
		 */
		public function product_class( $classes, $class = '', $post_id = '' ) {
			global $woocommerce_loop;

			if ( ! $post_id || get_post_type( $post_id ) !== 'product' || is_single( $post_id ) ) {
				return $classes;
			}

			$classes[] = 'product-item';
			$sm_num = 6;
			$xs_num = 6;

			$lg_md_num = ( 12 / $woocommerce_loop['columns'] );

			$columns = 'col-lg-' . $lg_md_num .' col-md-' . $lg_md_num .' col-sm-'. $sm_num . ' col-xs-' . $xs_num;

			if ( ! is_search() ) {
				$classes[] = $columns;
			} else {
				if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
					$classes[] = $columns;
				}
			}

			return $classes;
		}

		/**
		 * Add to Cart fragments
		 *
		 * @param  array $fragments Fragments to refresh via AJAX
		 * @return array 			Fragments to refresh via AJAX
		 */
		public function add_to_cart_fragments( $fragments ) {

			ob_start(); // start
			get_template_part( 'inc/woocommerce/neal-woocommerce-mini-cart' ); 
			$mini_cart_contents = ob_get_clean(); // end
			
			$fragments['div.mini-cart-wrap'] = '<div class="mini-cart-wrap">'.$mini_cart_contents.'</div>';

			$fragments['span.cart-value'] = '<span class="cart-value">' .WC()->cart->cart_contents_count .'</span>';
			
			return $fragments;

		}

		/**
		 * Get Mini Cart fragments via AJAX
		 *
		 * @since 1.0.0
		 */
		public function get_mini_cart_fragments() {

			ob_start(); // start
			get_template_part( 'inc/woocommerce/neal-woocommerce-mini-cart' ); 
			$mini_cart_contents = ob_get_clean(); // end
			
			$fragments = array();
			
			$fragments['miniCartContent'] = $mini_cart_contents;
			$fragments['miniCartCount'] = WC()->cart->cart_contents_count;
			
			echo json_encode( $fragments );

			die();

		}

		/**
		 * Product Content Thumbnail
		 *
		 * @since  1.0.0
		 * @return string
		 */
		function product_content_thumbnail( $image_size = false ) {

			echo '<a href="' . esc_url( get_the_permalink() ) . '">';

			if ( ! $image_size ) {
				$image_size = 'shop_catalog';
			}

			// product badges
			$this->product_badges();

			// product image
			echo woocommerce_get_product_thumbnail( $image_size );

			echo '</a>';

		}

		/**
		 * Display badges
		 *
		 * @since 1.0.0
		 */
		function product_badges() {
			global $product;

			$html = '';

			if ( $product->is_on_sale() ) {
				if ( neal_get_option( 'product_sale-type' ) != 'sale' ) {
					$percentage = 0;

					if ( $product->get_type() == 'variable' ) {
						$available_variations = $product->get_available_variations();
						$max_percentage       = 0;

						for ( $i = 0; $i < count( $available_variations ); $i ++ ) {
							$variation_id     = $available_variations[$i]['variation_id'];
							$variable_product = new WC_Product_Variation( $variation_id );
							$regular_price    = $variable_product->get_regular_price();
							$sales_price      = $variable_product->get_sale_price();
							$percentage       = $regular_price ? round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ) ) : 0;

							if ( $percentage > $max_percentage ) {
								$max_percentage = $percentage;
							}
						}

					} elseif ( $product->get_type() == 'simple' ) {
						$percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
					}

					if ( $percentage ) {
						$html .= '<span class="on-sale ribbon">' . sprintf( esc_html__('-%s off', 'neal' ), $percentage . '%' ) . '</span>';
					}

				} else {
					$html .= '<span class="on-sale ribbon">' . esc_html__( 'Sale!', 'neal' ) . '</span>';
				}
			} else if ( $product->get_stock_status() == 'outofstock' ) {
				$outofstock = '';
				if ( ! $outofstock ) {
					$outofstock = esc_html__( 'Out Of Stock', 'neal' );
				}
				$html .= '<span class="out-of-stock ribbon">' . esc_html( $outofstock ) . '</span>';
			}

			echo ''. $html;
			
		}

		/**
		 * Product rating
		 *
		 * @since  1.0.0
		 */
		function product_rating() {
			woocommerce_template_loop_rating();
		}

		/**
		 * Product title
		 *
		 * @since  1.0.0
		 */
		function product_title() {
			printf( '<h4><a href="%s">%s</a></h4>', esc_url( get_the_permalink() ), get_the_title() );
		}

		/**
		 * Product prices
		 *
		 * @since  1.0.0
		 */
		function product_prices() {
			woocommerce_template_loop_price();
		}

		/**
		 * Product meta position
		 *
		 * @since  1.0.0
		 */
		public function product_meta_position() {

			// action priority
			$rating = intval( neal_get_option('product_star-rating-pos') ) * 10;
			$title = intval( neal_get_option('product_title-pos') ) * 10;
			$prices = intval( neal_get_option('product_prices-pos') ) * 10;

			// rating
			add_action( 'woocommerce_after_shop_loop_item_title', array( $this, 'product_rating' ), $rating );

			// title
			add_action( 'woocommerce_after_shop_loop_item_title', array( $this, 'product_title' ), $title );

			// prices
			add_action( 'woocommerce_after_shop_loop_item_title', array( $this, 'product_prices' ), $prices );
		}

		/**
		 * Display product buttons
		 *
		 * @since 1.0.0
		 */
		public function product_buttons() {
			
			if ( function_exists( 'woocommerce_template_loop_add_to_cart' ) ) {
				woocommerce_template_loop_add_to_cart();
			}
		}

		/**
		 * Display a tool bar on top of product archive
		 *
		 * @since 1.0.0
		 */
		function shop_toolbar() {
			if ( ! neal_get_option( 'shop_tbar-label' ) ) {
				return;
			}

			$html = '';

			$html .= '<div class="tool-bar">';
				$html .= '<ul class="mode-view">';
					$html .= '<li class="grid">';
						$html .= '<a href="#" class="active"><i class="fa ' .neal_get_option('shop_tbar-grid-icon') . '" aria-hidden="true"></i></a>';
					$html .= '</li>';
						
					$html .= '<li class="list">';
						$html .= '<a href="#"><i class="fa ' . neal_get_option('shop_tbar-list-icon') . '" aria-hidden="true"></i></a>';
					$html .= '</li>';
				$html .= '</ul>';
			$html .= '</div>';

			echo ''. $html;
		}

		/**
		 * Change length of product description
		 *
		 * @since  1.0.0
		 * @return string
		 */
		function short_description( $desc ) {
			if ( empty( $desc ) ) {
				return $desc;
			}

			if ( neal_is_shop_pages() ) {
				$length = 20;

				return neal_custom_excerpt( $desc, $length );
			}

			return $desc;
		}

		/**
		 * Shop sidebar
		 *
		 * @since  1.0.0
		 */
		public function get_sidebar() {

			// check sidebar label and shop pages
			if ( neal_get_option( 'shop_sidebar-label' ) && neal_is_shop_pages() ) {
				woocommerce_get_sidebar();
			}

			// check sidebar label and shop single
			if ( neal_get_option( 'shop_single_sidebar-label' ) && is_singular('product') ) {
				wc_get_template( 'global/single-sidebar.php' );
			}

		}

		public function change_avatar_size() {
			return neal_get_option( 'comments_author-img-size' );
		}
	}
	
endif;

return new Neal_WooCommerce();