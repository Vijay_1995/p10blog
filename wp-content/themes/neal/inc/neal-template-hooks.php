<?php
/**
 * Neal hooks
 *
 * @package neal
 */

/**
 * General
 *
 * @see  neal_get_sidebar()
 */
add_action( 'neal_sidebar',	'neal_get_sidebar',		10 );

/**
 * Layout
 *
 * @see  neal_open_content_container()
 * @see  neal_close_content_container()
 */
add_action( 'neal_before_content', 'neal_page_header', 10 );
add_action( 'neal_before_content', 'neal_open_content_container',	20 );
add_action( 'neal_after_content', 'neal_close_content_container',	10 );


/**
 * Header
 *
 * @see  neal_page_header()
 */
add_action( 'neal_header',	'neal_header_layouts',   10 );
add_action( 'neal_after_header',	'neal_header_social_media',  10 );


/**
 * Footer
 *
 * @see  neal_footer_layouts()
 */
add_action( 'neal_footer',	'neal_footer_layouts',   10 );


/**
 * Posts
 *
 * @see  neal_post_header()
 * @see  neal_post_meta()
 * @see  neal_post_content()
 * @see  neal_paging_nav()
 * @see  neal_single_post_header()
 * @see  neal_display_comments()
 */
add_action( 'neal_loop_post',         'neal_post_header',      10 );
add_action( 'neal_loop_post',         'neal_post_content',     20 );
add_action( 'neal_loop_post',         'neal_post_meta',        30 );
add_action( 'neal_loop_after',        'neal_paging_nav',       10 );
add_action( 'neal_single_post',       'neal_post_header',      10 );
add_action( 'neal_single_post',       'neal_post_meta',        20 );
add_action( 'neal_single_post',       'neal_post_content',     30 );

add_action( 'neal_single_post_after', 'neal_blog_single_subscribe',	10 );
add_action( 'neal_single_post_after', 'neal_author_box', 			20 );
add_action( 'neal_single_post_after', 'neal_related_posts', 		30 );
add_action( 'neal_single_post_after', 'neal_display_comments', 		40 );
add_action( 'neal_single_post_after', 'neal_post_nav',         		50 );

if ( neal_get_option( 'blog_single_related-posts-pos' ) == 'below-comments' ) {
	// remove actions
	remove_action( 'neal_single_post_after', 'neal_related_posts', 		30 );
	remove_action( 'neal_single_post_after', 'neal_post_nav',         	50 );

	// add actions
	add_action( 'neal_single_post_after', 'neal_post_nav',         	30 );
	add_action( 'neal_single_post_after', 'neal_related_posts', 	50 );
}


/**
 * Pages
 *
 * @see  neal_page_content()
 * @see  neal_display_comments()
 */

add_action( 'neal_page',       'neal_page_content',     10 );
add_action( 'neal_page_after', 'neal_display_comments', 10 );

/**
 * Contact Page
 *
 * @see  neal_contact_page_frontend()
 * @see  neal_contact_page_backend()
 */
add_action( 'neal-contact-page', 'neal_contact_page_frontend',     10 );
add_action( 'neal-contact-page', 'neal_contact_page_backend',      20 );