<?php
/**
 * Neal Widgets Init Classes
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	
	/**
	 * Neal Posts By Category class
	 */
	class Neal_Posts_By_Category extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'postsByCategory', // Base ID
				esc_html__( 'Posts By Category', 'neal' ), // Name
				array( 'description' => esc_html__( 'Display Posts by category', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )
			echo ''. $args['before_title'] . $title . $args['after_title'];
		?>
			<h1>widget posts</h1>
		<?php

			echo ''. $args['after_widget'];
		}

		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title'		 => esc_html__( 'Recent Posts', 'neal' ),
					'image_show' => true,
					'image_size' => 'thumbnail',
					'sort_by' 	 => 'recent',
					'showposts'  => 5,
					'postdate' 	 => true,
				) );

			if ( isset ( $instance ) ) {
				$title = $instance[ 'title' ];
				$image_show = $instance[ 'image_show' ];
				$image_size = $instance[ 'image_size' ];
				$sort_by = $instance[ 'sort_by' ];
				$showposts = $instance[ 'showposts' ];
				$postdate = $instance[ 'postdate' ];
			}
			else {
				$title = esc_html__( 'New title', 'neal' );
				$image_show = '';
				$image_size = '';
				$sort_by = '';
				$showposts = '';
				$postdate = '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>"><?php esc_html_e( 'Image Show?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_show' ) ); ?>" type="checkbox" <?php checked( $image_show ); ?>  />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>"><?php esc_html_e( 'Image Size:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_size' ) ); ?>">
					<option value="thumbnail" <?php selected( $image_size, 'thumbnail' ); ?>><?php esc_html_e( 'Small', 'neal' ); ?></option>
					<option value="medium" <?php selected( $image_size, 'medium' ); ?>><?php esc_html_e( 'Medium', 'neal' ); ?></option>
					<option value="full" <?php selected( $image_size, 'full' ); ?>><?php esc_html_e( 'Full', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>"><?php esc_html_e( 'Sort By:', 'neal' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sort_by' ) ); ?>">
					<?php echo neal_get_categories(); ?>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'showposts' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'neal' ); ?></label> 
				<input class="tiny-text" id="<?php echo esc_attr( $this->get_field_id( 'showposts' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'showposts' ) ); ?>" type="number" min="1" value="<?php echo intval( $showposts ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>"><?php esc_html_e( 'Display post date?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postdate' ) ); ?>" type="checkbox" <?php checked( $instance['postdate'] ); ?>  />
			</p>
		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['image_show'] = ( ! empty( $new_instance['image_show'] ) ) ? (bool) $new_instance['image_show'] : false;

			$instance['image_size'] = ( ! empty( $new_instance['image_size'] ) ) ? strip_tags( $new_instance['image_size'] ) : '';

			$instance['sort_by'] = ( ! empty( $new_instance['sort_by'] ) ) ? strip_tags( $new_instance['sort_by'] ) : '';

			$instance['showposts'] = ( ! empty( $new_instance['showposts'] ) ) ? strip_tags( $new_instance['showposts'] ) : '';

			$instance['postdate'] = ( ! empty( $new_instance['postdate'] ) ) ? (bool) $new_instance['postdate'] : false;

			return $instance;
		}
	}

	/**
	 * Neal Recent Posts class
	 */
	class Neal_Recent_Img_Posts extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'recentImgPosts', // Base ID
				esc_html__( 'Recent Posts With Images', 'neal' ), // Name
				array( 'description' => esc_html__( 'Display Posts with images', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )
			echo ''. $args['before_title'] . $title . $args['after_title'];
		?>
			<ul>
				<?php

					$params = array(
						'post_type'			=> 'post',
						'post_status' 		=> 'publish',
						'posts_per_page' 	=> esc_html( $instance['showposts'] ),
					);
					
					// featured posts
					if ( $instance['sort_by'] == 'featured' ) {
						$params['meta_key']   = 'neal_post_options_featured';
						$params['meta_value'] = 'yes';
					}

					$wp_query = new WP_Query( $params );

					if ( $wp_query->have_posts() ) : 
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<li>
						<?php if ( $instance['image_show'] ) : ?>
							<div class="post-thumb">
								<a href="<?php esc_url( the_permalink() ); ?>">
									<?php
										the_post_thumbnail( $instance['image_size'] ); 
									?>
								</a>
							</div>
						<?php endif; ?>
						<div class="post-meta">
							<h3 class="post-title">
								<a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?>
								</a>
							</h3>
							<?php if ( $instance['postdate'] ) : ?>
								<span class="post-date"><?php the_time( get_option('date_format') ); ?></span>
							<?php endif; ?>
						</div>
					</li>

				<?php 
				endwhile;
				wp_reset_postdata();
				endif;
				?>
			</ul>
		<?php

			echo ''. $args['after_widget'];
		}

		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title'		 => esc_html__( 'Recent Posts', 'neal' ),
					'image_show' => true,
					'image_size' => 'thumbnail',
					'sort_by' 	 => 'recent',
					'showposts'  => 5,
					'postdate' 	 => true,
				) );

			if ( isset ( $instance ) ) {
				$title = $instance[ 'title' ];
				$image_show = $instance[ 'image_show' ];
				$image_size = $instance[ 'image_size' ];
				$sort_by = $instance[ 'sort_by' ];
				$showposts = $instance[ 'showposts' ];
				$postdate = $instance[ 'postdate' ];
			}
			else {
				$title = esc_html__( 'New title', 'neal' );
				$image_show = '';
				$image_size = '';
				$sort_by = '';
				$showposts = '';
				$postdate = '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>"><?php esc_html_e( 'Image Show?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_show' ) ); ?>" type="checkbox" <?php checked( $image_show ); ?>  />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>"><?php esc_html_e( 'Image Size:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_size' ) ); ?>">
					<option value="thumbnail" <?php selected( $image_size, 'thumbnail' ); ?>><?php esc_html_e( 'Small', 'neal' ); ?></option>
					<option value="medium" <?php selected( $image_size, 'medium' ); ?>><?php esc_html_e( 'Medium', 'neal' ); ?></option>
					<option value="full" <?php selected( $image_size, 'full' ); ?>><?php esc_html_e( 'Full', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>"><?php esc_html_e( 'Sort By:', 'neal' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sort_by' ) ); ?>">
					<option value="recent" <?php selected( $sort_by, 'recent' ); ?>><?php esc_html_e( 'Recent', 'neal' ); ?></option>
					<option value="featured" <?php selected( $sort_by, 'featured' ); ?>><?php esc_html_e( 'Featured', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'showposts' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'neal' ); ?></label> 
				<input class="tiny-text" id="<?php echo esc_attr( $this->get_field_id( 'showposts' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'showposts' ) ); ?>" type="number" min="1" value="<?php echo intval( $showposts ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>"><?php esc_html_e( 'Display post date?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postdate' ) ); ?>" type="checkbox" <?php checked( $instance['postdate'] ); ?>  />
			</p>
		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['image_show'] = ( ! empty( $new_instance['image_show'] ) ) ? (bool) $new_instance['image_show'] : false;

			$instance['image_size'] = ( ! empty( $new_instance['image_size'] ) ) ? strip_tags( $new_instance['image_size'] ) : '';

			$instance['sort_by'] = ( ! empty( $new_instance['sort_by'] ) ) ? strip_tags( $new_instance['sort_by'] ) : '';

			$instance['showposts'] = ( ! empty( $new_instance['showposts'] ) ) ? strip_tags( $new_instance['showposts'] ) : '';

			$instance['postdate'] = ( ! empty( $new_instance['postdate'] ) ) ? (bool) $new_instance['postdate'] : false;

			return $instance;
		}
	}

	/**
	 * Neal Recent Avatar Comments class
	 */
	class Neal_Recent_Avatar_Comments extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'recentAvatarComments', // Base ID
				esc_html__( 'Recent Comments With Avatar', 'neal' ), // Name
				array( 'description' => esc_html__( 'Display Comments with Avatar', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )
			echo ''. $args['before_title'] . $title . $args['after_title'];
		?>
			<ul>
				<?php
					$comments_query = new WP_Comment_Query();
					$comments = $comments_query->query( array( 
						'status' => 'approve', 
						'number' => intval( $instance['numbers'] )
					) );

					if ( $comments ) : foreach ( $comments as $comment ) :
				?>
					<li>
						<div class="author-avatar">
							<?php echo get_avatar( $comment->comment_author_email, $instance['avatar_size'] ); ?>
						</div>
						<div class="author-content">
							<span><?php echo get_comment_author( $comment->comment_ID ); ?></span> <?php esc_html_e( 'on', 'neal' ); ?> <a href="<?php echo esc_url( get_the_permalink( $comment->comment_post_ID ) ); ?>"><?php echo get_the_title( $comment->comment_post_ID ); ?></a>
						</div>
					</li>

				<?php 
				endforeach;
				endif;
				?>
			</ul>
		<?php

			echo ''. $args['after_widget'];
		}

		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title'		  => esc_html__( 'Recent Comments', 'neal' ),
					'avatar_size' => 50,
					'numbers'     => 5,
					'postdate' 	  => true,
				) );

			if ( isset ( $instance ) ) {
				$title       = $instance[ 'title' ];
				$avatar_size = $instance[ 'avatar_size' ];
				$numbers     = $instance[ 'numbers' ];
				$postdate    = $instance[ 'postdate' ];
			}
			else {
				$title       = esc_html__( 'New title', 'neal' );
				$avatar_size = '';
				$numbers     = '';
				$postdate    = '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'avatar_size' ) ); ?>"><?php esc_html_e( 'Avatar Size:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'avatar_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'avatar_size' ) ); ?>">
					<option value="50" <?php selected( $avatar_size, 50 ); ?>><?php esc_html_e( 'Small', 'neal' ); ?></option>
					<option value="70" <?php selected( $avatar_size, 70 ); ?>><?php esc_html_e( 'Medium', 'neal' ); ?></option>
					<option value="100" <?php selected( $avatar_size, 100 ); ?>><?php esc_html_e( 'Large', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'numbers' ) ); ?>"><?php esc_html_e( 'Number of comments to show:', 'neal' ); ?></label> 
				<input class="tiny-text" id="<?php echo esc_attr( $this->get_field_id( 'numbers' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'numbers' ) ); ?>" type="number" min="1" value="<?php echo intval( $numbers ); ?>" />
			</p>
		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['avatar_size'] = ( ! empty( $new_instance['avatar_size'] ) ) ? intval( $new_instance['avatar_size'] ) : '';

			$instance['numbers'] = ( ! empty( $new_instance['numbers'] ) ) ? intval( $new_instance['numbers'] ) : '';

			return $instance;
		}
	}

	/**
	 * Neal Display posts class
	 */
	class Neal_Display_Posts extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'displayPosts', // Base ID
				esc_html__( 'Display Posts', 'neal' ), // Name
				array( 'description' => esc_html__( 'Display Posts', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )
			echo ''. $args['before_title'] . $title . $args['after_title'];

			$extra_class = '';
			// posts carousel
			$js_data = '';

			if ( $instance['posts_carousel'] ) {
				$extra_class .= ' owl-carousel';
				$js_data .= ' data-loop='.esc_attr($instance['posts_carousel_loop']);
				$js_data .= ' data-nav='.esc_attr($instance['posts_carousel_nav']);
				$js_data .= ' data-dots='.esc_attr($instance['posts_carousel_dots']);
			}

			// mini posts
			if ( $instance['image_size'] == 'thumbnail' ) {
				$extra_class .= ' mini-posts';
			}

		?>
			<div class="widget-posts<?php echo esc_attr( $extra_class ); ?>"<?php echo esc_html( $js_data ); ?>>
				<?php

					$params = array(
						'post_type'			=> 'post',
						'post_status' 		=> 'publish',
						'posts_per_page' 	=> intval( $instance['posts_amount'] ),
						'order'   			=> esc_html( $instance['posts_sort_order'] ),
					);

					// random posts
					if ( $instance['posts_sort_order'] == 'random' ) {
						unset( $params['order'] );
						$params['orderby'] = 'rand';
					}
					
					// featured posts
					if ( $instance['posts_source'] == 'featured' ) {
						$params['meta_key']   = 'neal_post_options_featured';
						$params['meta_value'] = 'yes';
					}

					// category
					if ( $instance['posts_source'] == 'category' ) {
						$categories = array_map( 'intval', $instance['posts_category'] );
						$params['cat'] = $categories;
					}

					// tag
					if ( $instance['posts_source'] == 'tag' ) {
						$tags = array_map( 'intval', $instance['posts_tag'] );
						$params['tag__in'] = $tags;
					}

					// post ids
					if ( $instance['posts_ids'] ) {
						unset( $params['cat'] ); // remove cat
						unset( $params['tag__in'] ); // remove tag

						$ids = explode( ',', esc_html( $instance['posts_ids'] ) );
						$params['post__in'] = $ids;
					}

					$wp_query = new WP_Query( $params );

					if ( $wp_query->have_posts() ) : 
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<div class="post-item">
							<?php if ( has_post_thumbnail() && $instance['image_show'] ) : ?>
								<!-- Post Media -->
								<div class="entry-media">
									<?php neal_get_template_part( 'post-formats/content', get_post_format(), $instance['image_size'] ); ?>
								</div>
								<!-- Post Media End -->
							<?php endif; ?>
						<div class="post-meta">
							<?php
								
								// category
								if ( $instance['posts_meta_category'] ) {
									neal_post_categories();
								}

								// date
								if ( $instance['posts_meta_date'] ) {
									neal_post_date();
								}
							?>
							<h3 class="post-title">
								<a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?>
								</a>
							</h3>
						</div>
					</div>
				<?php 
				endwhile;
				wp_reset_postdata();
				endif;
				?>
			</div>
		<?php

			echo ''. $args['after_widget'];
		}

		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title'				  => '',
					'posts_source'		  => '',
					'posts_category'      => '',
					'posts_tag'			  => '',
					'posts_amount'		  => 3,
					'posts_sort_order' 	  => '',
					'posts_ids'			  => '',
					'posts_meta_category' => '',
					'posts_meta_date'	  => '',
					'image_show' 		  => true,
					'image_size' 		  => 'thumbnail',
					'posts_carousel'	  => false,
					'posts_carousel_loop' => 0,
					'posts_carousel_nav'  => 0,
					'posts_carousel_dots' => 0,
				) );

			if ( isset ( $instance ) ) {
				$title 				 = $instance['title'];
				$posts_source 		 = $instance['posts_source'];
				$posts_category 	 = $instance['posts_category'];
				$posts_tag 	 		 = $instance['posts_tag'];
				$posts_amount 		 = $instance['posts_amount'];
				$posts_sort_order 	 = $instance['posts_sort_order'];
				$posts_ids 			 = $instance['posts_ids'];
				$posts_meta_category = $instance['posts_meta_category'];
				$posts_meta_date 	 = $instance['posts_meta_date'];
				$image_show 		 = $instance['image_show'];
				$image_size 		 = $instance['image_size'];
				$posts_carousel		 = $instance['posts_carousel'];
				$posts_carousel_loop = $instance['posts_carousel_loop'];
				$posts_carousel_nav  = $instance['posts_carousel_nav'];
				$posts_carousel_dots = $instance['posts_carousel_dots'];
			}
			else {
				$title 				 = esc_html__( 'New title', 'neal' );
				$posts_source    	 = '';
				$posts_category      = '';
				$posts_tag           = '';
				$posts_amount        = '';
				$posts_sort_order 	 = '';
				$posts_ids 			 = '';
				$posts_meta_category = '';
				$posts_meta_date 	 = '';
				$image_show 		 = '';
				$image_size 		 = '';
				$posts_carousel		 = '';
				$posts_carousel_loop = '';
				$posts_carousel_nav  = '';
				$posts_carousel_dots = '';

			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'posts_source' ) ); ?>"><?php esc_html_e( 'Posts Source:', 'neal' ); ?></label> 
				<select class="neal-widget-posts-source" id="<?php echo esc_attr( $this->get_field_id( 'posts_source' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_source' ) ); ?>">
					<option value="recent" <?php selected( $posts_source, 'recent' ); ?>><?php esc_html_e( 'Recent', 'neal' ); ?></option>
					<option value="featured" <?php selected( $posts_source, 'featured' ); ?>><?php esc_html_e( 'Featured', 'neal' ); ?></option>
					<option value="category" <?php selected( $posts_source, 'category' ); ?>><?php esc_html_e( 'Category', 'neal' ); ?></option>
					<option value="tag" <?php selected( $posts_source, 'tag' ); ?>><?php esc_html_e( 'Tag', 'neal' ); ?></option>
				</select>

				<select multiple="multiple" class="neal-widget-posts-source-category <?php echo esc_attr( $posts_source ) == 'category' ? "open" : ''; ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_category' ) ); ?>[]">
					<?php echo neal_get_categories_option( 0, $posts_category ); ?>
				</select>

				<select multiple="multiple" class="neal-widget-posts-source-tag <?php echo esc_attr( $posts_source ) == 'tag' ? "open" : ''; ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_tag' ) ); ?>[]">
					<?php echo neal_get_tags( $posts_tag ); ?>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'posts_amount' ) ); ?>"><?php esc_html_e( 'Posts Amount:', 'neal' ); ?></label> 
				<input class="tiny-text" id="<?php echo esc_attr( $this->get_field_id( 'posts_amount' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_amount' ) ); ?>" type="number" min="1" value="<?php echo intval( $posts_amount ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'posts_sort_order' ) ); ?>"><?php esc_html_e( 'Sort Order:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'posts_sort_order' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_sort_order' ) ); ?>">
					<option value="asc" <?php selected( $posts_sort_order, 'asc' ); ?>><?php esc_html_e( 'Ascending (A > Z)', 'neal' ); ?></option>
					<option value="desc" <?php selected( $posts_sort_order, 'desc' ); ?>><?php esc_html_e( 'Descending (Z > A)', 'neal' ); ?></option>
					<option value="random" <?php selected( $posts_sort_order, 'random' ); ?>><?php esc_html_e( 'Random', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'posts_ids' ) ); ?>"><?php esc_html_e( 'Posts IDs:', 'neal' ); ?></label> 
				<input class="text" id="<?php echo esc_attr( $this->get_field_id( 'posts_ids' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_ids' ) ); ?>" type="text" value="<?php echo esc_attr( $posts_ids ); ?>" />
				<p class="neal-form-description"><?php esc_html_e( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post\'s ID (Example: 55,22,33)', 'neal' ); ?></p>
			</p>

			<p>
				<label"><?php esc_html_e( 'Post Meta:', 'neal' ); ?></label> 
				<label><input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'posts_meta_category' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_meta_category' ) ); ?>" type="checkbox" <?php checked( $posts_meta_category ); ?>  /><?php esc_html_e( 'Category', 'neal' ); ?></label>
				<label><input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'posts_meta_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_meta_date' ) ); ?>" type="checkbox" <?php checked( $posts_meta_date ); ?>  /><?php esc_html_e( 'Date', 'neal' ); ?></label>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>"><?php esc_html_e( 'Image Show?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'image_show' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_show' ) ); ?>" type="checkbox" <?php checked( $image_show ); ?>  />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>"><?php esc_html_e( 'Image Size:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'image_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image_size' ) ); ?>">
					<option value="thumbnail" <?php selected( $image_size, 'thumbnail' ); ?>><?php esc_html_e( 'Small', 'neal' ); ?></option>
					<option value="medium" <?php selected( $image_size, 'medium' ); ?>><?php esc_html_e( 'Medium', 'neal' ); ?></option>
					<option value="neal-widget-posts-image" <?php selected( $image_size, 'neal-widget-posts-image' ); ?>><?php esc_html_e( 'Full', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'posts_carousel' ) ); ?>"><?php esc_html_e( 'Posts Carousel?:', 'neal' ); ?></label> 
				<input class="checkbox show-posts-carousel" id="<?php echo esc_attr( $this->get_field_id( 'posts_carousel' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_carousel' ) ); ?>" type="checkbox" <?php checked( $posts_carousel ); ?>  />
				<div class="widget-posts-carousel-settings <?php echo esc_html( $posts_carousel ) ? "open" : ''; ?>">
					<p>
						<label for="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_loop' ) ); ?>"><?php esc_html_e( 'Carousel Loop:', 'neal' ); ?></label> 
						<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_loop' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_carousel_loop' ) ); ?>" type="checkbox" <?php checked( $posts_carousel_loop ); ?>  />
					</p>

					<p>
						<label for="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_nav' ) ); ?>"><?php esc_html_e( 'Carousel Nav:', 'neal' ); ?></label> 
						<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_nav' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_carousel_nav' ) ); ?>" type="checkbox" <?php checked( $posts_carousel_nav ); ?>  />
					</p>

					<p>
						<label for="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_dots' ) ); ?>"><?php esc_html_e( 'Carousel Dots:', 'neal' ); ?></label> 
						<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'posts_carousel_dots' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'posts_carousel_dots' ) ); ?>" type="checkbox" <?php checked( $posts_carousel_dots ); ?>  />
					</p>
				</div>
			</p>
		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['posts_source'] = ( ! empty( $new_instance['posts_source'] ) ) ? strip_tags( $new_instance['posts_source'] ) : '';

			// category
			$categories = array_map( 'intval', wp_unslash( $new_instance['posts_category'] ) );
			$instance['posts_category'] = isset( $categories ) ? $categories : array();

			// tag
			$tags = array_map( 'intval', wp_unslash( $new_instance['posts_tag'] ) );
			$instance['posts_tag'] = isset( $tags ) ? $tags : array();

			$instance['posts_amount'] = ( ! empty( $new_instance['posts_amount'] ) ) ? intval( $new_instance['posts_amount'] ) : '';

			$instance['posts_sort_order'] = ( ! empty( $new_instance['posts_sort_order'] ) ) ? strip_tags( $new_instance['posts_sort_order'] ) : '';

			$instance['posts_ids'] = ( ! empty( $new_instance['posts_ids'] ) ) ? strip_tags( $new_instance['posts_ids'] ) : '';

			$instance['posts_meta_category'] = ( ! empty( $new_instance['posts_meta_category'] ) ) ? (bool) $new_instance['posts_meta_category'] : false;

			$instance['posts_meta_date'] = ( ! empty( $new_instance['posts_meta_date'] ) ) ? (bool) $new_instance['posts_meta_date'] : false;

			$instance['image_show'] = ( ! empty( $new_instance['image_show'] ) ) ? (bool) $new_instance['image_show'] : false;

			$instance['image_size'] = ( ! empty( $new_instance['image_size'] ) ) ? strip_tags( $new_instance['image_size'] ) : '';

			$instance['posts_carousel'] = ( ! empty( $new_instance['posts_carousel'] ) ) ? (bool) $new_instance['posts_carousel'] : false;
			$instance['posts_carousel_loop'] = ( ! empty( $new_instance['posts_carousel_loop'] ) ) ? (bool) $new_instance['posts_carousel_loop'] : 0;
			$instance['posts_carousel_nav'] = ( ! empty( $new_instance['posts_carousel_nav'] ) ) ? (bool) $new_instance['posts_carousel_nav'] : 0;
			$instance['posts_carousel_dots'] = ( ! empty( $new_instance['posts_carousel_dots'] ) ) ? (bool) $new_instance['posts_carousel_dots'] : 0;


			return $instance;
		}
	}

	/**
	 * Neal Categories image class
	 */
	class Neal_Categories_Image extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'categoriesImage', // Base ID
				esc_html__( 'Categories Image', 'neal' ), // Name
				array( 'description' => esc_html__( 'Display Categories with image', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )
			echo ''. $args['before_title'] . $title . $args['after_title'];
		?>
			<ul>
				<?php

					$params = array(
						'orderby' => 'name',
						'parent' => 0,
					);

					$counter = 0;

					$categories = get_categories( $params );
					foreach ( $categories as $category ) :
						$t_id = $category->term_id;
						$term_meta = get_option( "taxonomy_$t_id" );
						$featured = isset( $term_meta['neal_featured_category'] ) ? $term_meta['neal_featured_category'] : '';

						$img_id = isset( $term_meta['neal_cat_thumbnail_id'] ) ? $term_meta['neal_cat_thumbnail_id'] : '';

						$sort_by = true;
						if ( $instance['sort_by'] == 'featured' ) {
							$sort_by = $featured;
						}

						if ( $img_id && $counter < $instance['category_limit'] && $sort_by ) :

							$counter++;
					?>
						<li>
							<a href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>">
								<?php echo wp_get_attachment_image( $img_id, 'neal-cat-image' ); ?>
								<div class="category-title">
									<div class="cat-title">
										<h3><?php echo esc_html( $category->name ); ?></h3>
									</div>
								</div>
							</a>
						</li>

					<?php 
					endif;
					endforeach;	?>
			</ul>
		<?php
			echo ''. $args['after_widget'];
		}

		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title'		=> '',
					'sort_by'	=> 'latest',
					'category_limit' => 5,
					 ) );

			if ( isset ( $instance ) ) {
				$title = $instance[ 'title' ];
				$sort_by = $instance[ 'sort_by' ];
				$category_limit = $instance[ 'category_limit' ];
			}
			else {
				$title = esc_html__( 'New title', 'neal' );
				$sort_by = $instance[ 'sort_by' ];
				$category_limit = '';
			}

			$selected_sort_by_latest = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'latest', false ) : '' );
			$selected_sort_by_featured = ( ( isset ( $sort_by ) ) ? selected( $sort_by, 'featured', false ) : '' );

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>"><?php esc_html_e( 'Sort By:', 'neal' ); ?></label>
				<select id="<?php echo esc_attr( $this->get_field_id( 'sort_by' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sort_by' ) ); ?>">
					<option value="latest" <?php echo esc_html( $selected_sort_by_latest ); ?>><?php esc_html_e( 'Latest Categories', 'neal' ); ?></option>
					<option value="featured" <?php echo esc_html( $selected_sort_by_featured ); ?>><?php esc_html_e( 'Featured Categories', 'neal' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'category_limit' ) ); ?>"><?php esc_html_e( 'Number of categories to show:', 'neal' ); ?></label> 
				<input class="tiny-text" id="<?php echo esc_attr( $this->get_field_id( 'category_limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'category_limit' ) ); ?>" type="number" min="1" value="<?php echo intval( $category_limit ); ?>" />
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['sort_by'] = ( ! empty( $new_instance['sort_by'] ) ) ? strip_tags( $new_instance['sort_by'] ) : '';

			$instance['category_limit'] = ( ! empty( $new_instance['category_limit'] ) ) ? strip_tags( $new_instance['category_limit'] ) : '';

			return $instance;
		}
	}

	/**
	 * Neal About Me Widget class
	 */
	class Neal_About_Me extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'widgets_init', array( $this, 'aboutme_load_widget' ) );
			
			parent::__construct(
				'aboutMe', // Base ID
				esc_html__( 'About Me', 'neal' ), // Name
				array( 'description' => esc_html__( 'A Footer Widget', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating widget front-end
		 */
		public function widget( $args, $instance ) {
			// get theme customizer data 
			$copyright  = get_option( 'neal_socialcopy' );

			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )

			$avatar = wp_get_attachment_image_src( $instance['avatar'], $instance['avatar_size'] );

			echo '<div class="about-me">';
				echo '<img src="' . $avatar[0] . ' " alt>';
				echo ''. $args['before_title'] . $title . $args['after_title'];

				echo '<p>' . $instance['about_text'] . '</p>';

				// socials
				if ( $instance['social_icons'] ) {
					neal_social_icons();
				}									

			echo '</div>';

			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' 		=> esc_html__( 'About Me', 'neal' ),
					'avatar' 		=> '',
					'avatar_size' 	=> 'full',
					'about_text' 	=> esc_html__( 'About Text', 'neal' ),
					'social_icons' 	=> esc_html__( 'Social Icons', 'neal' ),
				) );

			if ( isset ( $instance ) ) {
				$title = $instance['title'];
				$avatar = $instance['avatar'];
				$avatar_size = $instance['avatar_size'];
				$about_text = $instance['about_text'];
				$social_icons = $instance['social_icons'];
			}
			else {
				$title 			= esc_html__( 'New title', 'neal' );
				$avatar 	 	= '';
				$avatar_size 	= '';
				$about_text  	= '';
				$social_icons 	= '';
			}

			// Widget admin form
			?>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for=""><?php esc_html_e( 'Avatar:', 'neal' ); ?></label> 
				<div>
					<div class="neal-widget-avatar" style="text-align:center;margin-bottom: 10px;">
						<?php echo wp_get_attachment_image( esc_html( $avatar) ); ?>
					</div>
					<input type="button" class="button button-primary neal-widget-avatar-upload"  value="<?php esc_attr_e( 'Upload Image', 'neal' ); ?>">
					<input type="hidden"  id="neal-widget-avatar" name="<?php echo esc_attr( $this->get_field_name( 'avatar' ) ); ?>" value="<?php echo esc_attr( $avatar ); ?>">
				</div>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'avatar_size' ) ); ?>"><?php esc_html_e( 'Avatar Size:', 'neal' ); ?></label> 
				<select  id="<?php echo esc_attr( $this->get_field_id( 'avatar_size' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'avatar_size' ) ); ?>">
					<option value="thumbnail" <?php selected( $avatar_size, 'thumbnail' ); ?>><?php esc_html_e( 'Small', 'neal' ); ?></option>
					<option value="medium" <?php selected( $avatar_size, 'medium' ); ?>><?php esc_html_e( 'Medium', 'neal' ); ?></option>
					<option value="full" <?php selected( $avatar_size, 'full' ); ?>><?php esc_html_e( 'Original', 'neal' ); ?></option>
				</select>
			</p>

			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'about_text' ) ); ?>"><?php esc_html_e( 'About Text:', 'neal' ); ?></label> 
			<textarea rows="8" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'about_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'about_text' ) ); ?>"><?php echo esc_html( $about_text ); ?></textarea>
			<em><?php esc_html_e( 'Allowed HTML Tags: a, br, em, strong', 'neal' ); ?></em>
			</p>

			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'social_icons' ) ); ?>"><?php esc_html_e( 'Social Icons:', 'neal' ); ?></label>
			<input type="checkbox" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'social_icons' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'social_icons' ) ); ?>" <?php checked( $social_icons, 'on' ); ?>>
			</p>
		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$allow = array(
			    'a' => array(
			        'href' => array(),
			        'title' => array()
			    ),
			    'br' => array(),
			    'em' => array(),
			    'strong' => array(),
			);

			$instance['avatar'] = ( ! empty( $new_instance['avatar'] ) ) ? intval( $new_instance['avatar'] ) : '';

			$instance['avatar_size'] = ( ! empty( $new_instance['avatar_size'] ) ) ? strip_tags( $new_instance['avatar_size'] ) : '';
			$instance['about_text'] = ( ! empty( $new_instance['about_text'] ) ) ? wp_kses( $new_instance['about_text'], $allow ) : '';
			$instance['social_icons'] = ( ! empty( $new_instance['social_icons'] ) ) ? strip_tags( $new_instance['social_icons'] ) : '';

			return $instance;
		}

	}

	/**
	 * Neal Instagram Widget class
	 */
	class Neal_Instagram extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'widgets_init', array( $this, 'instagram_load_widget' ) );
			
			parent::__construct(
				'instagram', // Base ID
				esc_html__( 'Instagram', 'neal' ), // Name
				array( 'description' => esc_html__( 'A Footer Widget', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {

			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )

			echo '<div class="neal-instgaram">';
				echo ''. $args['before_title'] . $title . $args['after_title'];

			echo '</div>';

			if ( function_exists('neal_instagram') ) {
				echo neal_instagram( $instance['username'], $instance['number'], false, '', false, false, 'column-' .esc_html( $instance['columns'] ) );
			}


			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' 		=> esc_html__( 'Instagram', 'neal' ),
					'username' 		=> '',
					'number' 		=> '',
					'columns'		=> 3
			 	) );

			if ( isset ( $instance ) ) {
				$title 			= $instance[ 'title' ];
				$username 		= $instance[ 'username' ];
				$number 		= $instance[ 'number' ];
				$columns 		= $instance[ 'columns' ];
			}
			else {
				$title 			= esc_html__( 'New title', 'neal' );
				$username 		= '';
				$number 		= '';
				$columns		= '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Username:', 'neal' ); ?></label> 
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" value="<?php echo esc_attr( $username ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number:', 'neal' ); ?></label>
				<input type="number" class="widefat" min="1" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" value="<?php echo esc_attr( $number ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>"><?php esc_html_e( 'Columns:', 'neal' ); ?></label>
				<input type="number" class="widefat" min="1" id="<?php echo esc_attr( $this->get_field_id( 'columns' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'columns' ) ); ?>" value="<?php echo esc_attr( $columns ); ?>">
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['username'] = ( ! empty( $new_instance['username'] ) ) ? strip_tags( $new_instance['username'] ) : '';

			$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? intval( $new_instance['number'] ) : '';

			$instance['columns'] = ( ! empty( $new_instance['columns'] ) ) ? intval( $new_instance['columns'] ) : '';

			return $instance;
		}

		/**
		 * Register and load the widget
		 */
		public function instagram_load_widget() {
			register_widget( $this );
		}

	}

	/**
	 * Neal Social Widget class
	 */
	class Neal_Social extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'widgets_init', array( $this, 'social_load_widget' ) );
			
			parent::__construct(
				'social', // Base ID
				esc_html__( 'Social', 'neal' ), // Name
				array( 'description' => esc_html__( 'A Footer Widget', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			// get theme customizer data 
			$copyright  = get_option( 'neal_socialcopy' );

			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )

			echo '<div class="social">';

				// socials
				neal_social_icons();

			echo '</div>';

			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' => esc_html__( 'Social', 'neal' ),
				) );

			if ( isset ( $instance ) ) {
				$title = $instance[ 'title' ];
			}
			else {
				$title = esc_html__( 'New title', 'neal' );
			}

			// Widget admin form
			?>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			return $instance;
		}

		/**
		 * Register and load the widget
		 */
		public function social_load_widget() {
			register_widget( $this );
		}

	}

	/**
	 * Neal Facebook Widget class
	 */
	class Neal_Facebook extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */

		public function __construct() {
			add_action( 'widgets_init', 	  array( $this, 'facebook_load_widget' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_script' ) );
			
			parent::__construct(
				'facebook', // Base ID
				esc_html__( 'Facebook', 'neal' ), // Name
				array( 'description' => esc_html__( 'A Footer Widget', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )

			echo ''. $args['before_title'] . $title . $args['after_title'];

			$small_header = esc_html( $instance['small_header'] ) ? 'true' : 'false';
			$hide_cover   = esc_html( $instance['hide_cover'] ) ? 'true' : 'false';
		?>
			<div id="fb-root"></div>
			<div class="fb-page" data-href="<?php echo esc_attr( $instance['fb_page_url'] ); ?>" data-width="<?php echo esc_attr( $instance['widget_width'] ); ?>" data-small-header="<?php echo esc_attr( $small_header ); ?>" data-adapt-container-width="true" data-hide-cover="<?php echo esc_attr( $hide_cover ); ?>" data-show-facepile="true"></div>

		<?php

			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' 		=> esc_html__( 'Facebook', 'neal' ),
					'fb_page_url' 	=> '',
					'widget_width'	=> 245,
					'small_header' 	=> false,
					'hide_cover' 	=> true
			 	) );

			if ( isset ( $instance ) ) {
				$title 			= $instance[ 'title' ];
				$fb_page_url 	= $instance[ 'fb_page_url' ];
				$widget_width 	= $instance[ 'widget_width' ];
				$small_header 	= $instance[ 'small_header' ];
				$hide_cover 	= $instance[ 'hide_cover' ];
			}
			else {
				$title 			= esc_html__( 'New title', 'neal' );
				$fb_page_url 	= '';
				$widget_width	= '';
				$small_header	= '';
				$hide_cover		= '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'fb_page_url' ) ); ?>"><?php esc_html_e( 'Facebook Page URL:', 'neal' ); ?></label> 
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'fb_page_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'fb_page_url' ) ); ?>" value="<?php echo esc_attr( $fb_page_url ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'widget_width' ) ); ?>"><?php esc_html_e( 'Widget Width:', 'neal' ); ?></label> 
				<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'widget_width' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'widget_width' ) ); ?>" value="<?php echo esc_attr( $widget_width ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'small_header' ) ); ?>"><?php esc_html_e( 'Small Header?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'small_header' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'small_header' ) ); ?>" type="checkbox" <?php checked( $small_header ); ?>  />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'hide_cover' ) ); ?>"><?php esc_html_e( 'Hide Cover?:', 'neal' ); ?></label> 
				<input class="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'hide_cover' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_cover' ) ); ?>" type="checkbox" <?php checked( $hide_cover ); ?>  />
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['fb_page_url'] = ( ! empty( $new_instance['fb_page_url'] ) ) ? strip_tags( $new_instance['fb_page_url'] ) : '';

			$instance['widget_width'] = ( ! empty( $new_instance['widget_width'] ) ) ? intval( $new_instance['widget_width'] ) : '';

			$instance['small_header'] = ( ! empty( $new_instance['small_header'] ) ) ? (bool) $new_instance['small_header'] : false;

			$instance['hide_cover'] = ( ! empty( $new_instance['hide_cover'] ) ) ? (bool) $new_instance['hide_cover'] : false;

			return $instance;
		}

		/**
		 * Register and load the widget
		 */
		public function facebook_load_widget() {
			register_widget( $this );
		}

		/**
		 * Enqueue scripts
		 *
		 */
		public function enqueue_script() {
			if ( is_active_widget( false, false, $this->id_base, true ) ) {
				wp_add_inline_script( 'neal-script', '(function(d, s, id) {
			  									  	var js, fjs = d.getElementsByTagName(s)[0];
			  										if (d.getElementById(id)) return;
			  										js = d.createElement(s); js.id = id;
			  										js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
			  										fjs.parentNode.insertBefore(js, fjs);
													}(document, "script", "facebook-jssdk"));' );
			}
		}

	}

	/**
	 * Neal Advertisement Widget class
	 */
	class Neal_Adv extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'neal_adv', // Base ID
				esc_html__( 'Advertisement', 'neal' ), // Name
				array( 'description' => esc_html__( 'Custom Advertisements', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {

			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];
			if ( ! empty( $title ) )

			echo ''. $args['before_title'] . $title . $args['after_title'];

		?>

			<?php echo json_decode( $instance['adv_js_code'] ); ?>

		<?php

			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' 		=> '',
					'adv_js_code' 	=> '',
			 	) );

			if ( isset ( $instance ) ) {
				$title 			= $instance[ 'title' ];
				$adv_js_code 	= $instance[ 'adv_js_code' ];
			}
			else {
				$title 			= esc_html__( 'New title', 'neal' );
				$adv_js_code	= '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'adv_js_code' ) ); ?>"><?php esc_html_e( 'Adv JS Code:', 'neal' ); ?></label>
				<textarea rows="8" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'adv_js_code' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'adv_js_code' ) ); ?>"><?php echo json_decode( $adv_js_code ); ?></textarea>
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			$instance['adv_js_code'] = ( ! empty( $new_instance['adv_js_code'] ) ) ? json_encode( $new_instance['adv_js_code'] ) : '';

			return $instance;
		}

	}

	/**
	 * Neal Custom Subscription Widget class
	 */
	class Neal_Custom_Subscription extends WP_Widget {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			
			parent::__construct(
				'neal_custom_subscription', // Base ID
				esc_html__( 'Custom Subscription', 'neal' ), // Name
				array( 'description' => esc_html__( 'Neal Custom Subscription', 'neal' ), ) // Args
			);

		}

		/**
		 * Creating Contact Us widget front-end
		 */
		public function widget( $args, $instance ) {

			$title = apply_filters( 'widget_title', $instance['title'] );
			// before and after widget arguments are defined by themes
			echo ''. $args['before_widget'];

			if ( shortcode_exists( 'jetpack_subscription_form' ) ) {
				echo do_shortcode( '[jetpack_subscription_form title="'.esc_attr( $instance['title'] ).'" subscribe_text="'.esc_attr( $instance['subscribe_text'] ).'" subscribe_placeholder="'.esc_attr( $instance['subscribe_place'] ).'" subscribe_button="'.esc_attr( $instance['subscribe_button'] ).'" ]' );
			}
			
			echo ''. $args['after_widget'];
		}
 
		/**
		 * Widget Backend
		 */
		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, 
				array( 
					'title' 			=> esc_html__( 'Let\'s be Friends!', 'neal' ),
					'subscribe_text'	=> esc_html__( 'Sign up to our newsletter to get exclusive content and DIYs!', 'neal' ),
					'subscribe_place'	=> esc_html__( 'Enter Your Email Here', 'neal' ),
					'subscribe_button'	=> esc_html__( 'Sign Up', 'neal' ),
			 	) );

			if ( isset ( $instance ) ) {
				$title 				= $instance['title'];
				$subscribe_text		= $instance['subscribe_text'];
				$subscribe_place	= $instance['subscribe_place'];
				$subscribe_button	= $instance['subscribe_button'];
			} else {
				$title 				= '';
				$subscribe_text		= '';
				$subscribe_place	= '';
				$subscribe_button	= '';
			}

			// Widget admin form
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'subscribe_text' ) ); ?>"><?php esc_html_e( 'Subscribe Text:', 'neal' ); ?></label>
				<textarea rows="8" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subscribe_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subscribe_text' ) ); ?>"><?php echo esc_html( $subscribe_text ); ?></textarea>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'subscribe_place' ) ); ?>"><?php esc_html_e( 'Subscribe Placeholder:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subscribe_place' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subscribe_place' ) ); ?>" type="text" value="<?php echo esc_attr( $subscribe_place ); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'subscribe_button' ) ); ?>"><?php esc_html_e( 'Subscribe Button:', 'neal' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'subscribe_button' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subscribe_button' ) ); ?>" type="text" value="<?php echo esc_attr( $subscribe_button ); ?>" />
			</p>

		<?php 
		}

		/**
		 * Updating widget replacing old instances with new
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['subscribe_text'] = ( ! empty( $new_instance['subscribe_text'] ) ) ? strip_tags( $new_instance['subscribe_text'] ) : '';
			$instance['subscribe_place'] = ( ! empty( $new_instance['subscribe_place'] ) ) ? strip_tags( $new_instance['subscribe_place'] ) : '';
			$instance['subscribe_button'] = ( ! empty( $new_instance['subscribe_button'] ) ) ? strip_tags( $new_instance['subscribe_button'] ) : '';

			return $instance;
		}

	}

	// Register widgets
	function register_widgets() {
		register_widget( 'Neal_Posts_By_Category' );
	    register_widget( 'Neal_Recent_Img_Posts' );
	    register_widget( 'Neal_Recent_Avatar_Comments' );
	    register_widget( 'Neal_Display_Posts' );
	    register_widget( 'Neal_Categories_Image' );
	    register_widget( 'Neal_About_Me' );
	    register_widget( 'Neal_Instagram' );
	    register_widget( 'Neal_Social' );
	    register_widget( 'Neal_Facebook' );
	    register_widget( 'Neal_Adv' );
	    register_widget( 'Neal_Custom_Subscription' );
	}

	add_action( 'widgets_init', 'register_widgets' );