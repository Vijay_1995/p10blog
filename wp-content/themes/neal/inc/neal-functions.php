<?php
/**
 * Neal functions.
 *
 * @package neal
 */

/**
 * Custom query on homepage posts
 *
 * @since 1.0
 */
function neal_homepage_post_query( $amount = '', $sort_order = '', $sort_by = '', $category = '', $tag = '', $post_ids = '' ) {
	$args = array(
		'post_type'			=> 'post',
		'post_status' 		=> 'publish',
		'posts_per_page' 	=> intval( $amount ),
		'order'   			=> esc_html( $sort_order ),
		'post__not_in' 		=> get_option( 'sticky_posts' )
	);

	// random posts
	if ( $sort_order == 'random' ) {
		unset( $args['order'] );
		$args['orderby'] = 'rand';
	}

	// featured posts
	if ( $sort_by == 'featured' ) {
		$args['meta_key'] = 'neal_post_options_featured';
		$args['meta_value'] = 'yes';
	}

	// category
	if ( $sort_by == 'category' ) {
		if ( is_array( $category ) ) {
			$categories = array_map( 'intval', $category );
			$args['cat'] = $categories;
		}
	}

	// tag
	if ( $sort_by == 'tag' ) {
		if ( is_array( $tag ) ) {
			$tags = array_map( 'intval', $tag );
			$args['tag__in'] = $tags;
		}
	}

	// post ids
	if ( $post_ids ) {
		$ids = explode( ',', esc_html( $post_ids ) );
		$args['post__in'] = $ids;
	}

	$neal_query = new WP_Query( $args );

	return $neal_query;
}

/**
 * Masonry posts data
 *
 * @since 1.0
 */
function neal_masonry_posts_data( $args_number = '', $args2_number = '' ) {

	// get post meta data
	$items = get_post_meta( get_the_ID(), 'neal_masonry_posts_items', true );
	extract( $items );

	$args = array(
		'post_type'			=> 'post',
		'post_status' 		=> 'publish',
		'posts_per_page' 	=> $args_number,
		'post__not_in' 		=> get_option( 'sticky_posts' )
	);

	$args2 = array();

	// random posts
	if ( $sort_order == 'random' ) {
		unset( $args['order'] );
		$args['orderby'] = 'rand';
	}

	// featured posts
	if ( $sort_by == 'featured' ) {
		$args['meta_key'] = 'neal_post_options_featured';
		$args['meta_value'] = 'yes';
	}

	// category
	if ( $sort_by == 'category' ) {
		$categories = array_map( 'intval', $category );
		$args['cat'] = $categories;
	}

	// tag
	if ( $sort_by == 'tag' ) {
		$tags = array_map( 'intval', $tag );
		$args['tag__in'] = $tags;
	}

	// post ids
	if ( $post_ids ) {
		$ids = explode( ',', esc_html( $post_ids ) );
		$args['post__in'] = $ids;
	}

	$post_query = new WP_Query( $args );

	$data = array(
		'query'	=> $post_query,
		'items' => $items,
	);

	if ( $args2_number ) {
		$args2 = array_merge( $args, $args2 );
		$args2['posts_per_page']	= $args2_number;
		$args2['offset']			= $args_number;
		$post_query2 = new WP_Query( $args2 );
		
		$data['query2'] = $post_query2;
	}

	return $data;
}

/**
 * Get on post options data
 *
 * @since 1.0
 */
function neal_get_post_options_data() {
	// get theme customizer data
	$blog_single = get_option( 'neal_blog_single' );

	// get post meta data
	$items = get_post_meta( get_the_ID(), 'neal_post_options_items', true );
	$defaults = array(
		'post_layouts'			=> $blog_single['general-post-layout'],
		'parallax_image'		=> $blog_single['general-post-parallax'],
		'sidebar'				=> $blog_single['sidebar-label'],
		'sidebar_align'			=> $blog_single['sidebar-align'],
		'sidebar_sticky'		=> $blog_single['sidebar-sticky'],
		'featured_image'		=> $blog_single['featured-img-label'],
		'title_pos'				=> $blog_single['title-pos'],
		'title_align'			=> $blog_single['title-align'],
		'meta'					=> $blog_single['meta-label'],
		'meta_pos'				=> $blog_single['meta-pos'],
		'meta_category'			=> $blog_single['meta-category'],
		'meta_date'				=> $blog_single['meta-date'],
		'meta_author'			=> $blog_single['meta-author'],
		'meta_align'			=> $blog_single['meta-align'],
		'header_social_media' 	=> false,
	);

	$items = wp_parse_args( $items, $defaults );

	return $items;
}

/**
 * Load more on homepage
 *
 * @since 1.0
 */
function neal_homepage_load_more( $post_item, $args = array() ) {
	$defaults = array();

	$args = wp_parse_args( $args, $defaults );

	extract( $args );

	if ( $load_more && $max_num_pages > 1 ) :

		$load_more_data = array();

		// post item
		$load_more_data[] = sprintf( 'data-post-item="%s"', $post_item );

		// next page
		$load_more_data[] = sprintf( 'data-next-page="1"' );

		// posts per page
		$load_more_data[] = sprintf( 'data-posts-per-page="%s"', $posts_per_page );

		// max_num_pages
		$load_more_data[] = sprintf( 'data-max-num-pages="%s"', $max_num_pages );

		// sort order
		$load_more_data[] = sprintf( 'data-post-orderby="%s"', $sort_order );

		// post not in
		if ( $sort_order == 'random' ) {
			$load_more_data[] = sprintf( 'data-post-not-in="%s"', implode( ',', $post_not_in ) );
		}

		// featured posts
		if ( $sort_by == 'featured' ) {
			$load_more_data[] = sprintf( 'data-featured-post="true"' );
		}

		// category
		if ( $sort_by == 'category' ) {
			$load_more_data[] = sprintf( 'data-post-category="%s"', implode( ',', $category ) );
		}

		// tag
		if ( $sort_by == 'tag' ) {
			$load_more_data[] = sprintf( 'data-post-tag="%s"', implode( ',', $tag ) );
		}

		// post ids
		if ( $post_ids ) {
			$ids = explode( ',', esc_html( $post_ids ) );
			$load_more_data[] = sprintf( 'data-post-ids="%s"', implode( ',', $ids ) );
		}

		// meta category
		$load_more_data[] = sprintf( 'data-meta-category="%s"', $meta_category );

		// meta date
		$load_more_data[] = sprintf( 'data-meta-date="%s"', $meta_date );

		// meta author
		$load_more_data[] = sprintf( 'data-meta-author="%s"', $meta_author );

		// post excerpt
		$load_more_data[] = sprintf( 'data-post-excerpt="%s"', $post_excerpt );

		// read more
		$load_more_data[] = sprintf( 'data-read-more="%s"', $read_more );


		echo '<a href="#" class="homepage-post-load-more" '. implode( ' ', $load_more_data ) .'>' . esc_html__( 'Load More', 'neal' ) . '</a>';
	endif;
}

/**
 * Get portfolio categories
 *
 * @since 1.0
 */
function neal_get_portfolio_categories( $echo = false ) {
	global $post;

	$tax   = 'neal-portfolio-category';
	$terms = wp_get_post_terms(
		$post->ID,
		$tax,
		array(
			'fields' => 'all'
		)
	);

	$html = '<div class="portfolio-categories">';
		foreach ( $terms as $term ) {
			$html .= '<a href="' . esc_url( get_term_link( $term->slug, $tax ) ) . '">' . $term->name . '</a>';
		}
	$html .= '</div>';

	if ( $echo ) {
		echo ''. $html;
	} else {
		return $html;
	}
}

/**
 * Get first post ID
 *
 * @since 1.0
 */
function neal_get_first_post_ID() {
    global $post;

    $loop = get_posts( 'numberposts=1' );
    $first_ID = $loop[0]->ID;

    return $first_ID;
}

/**
 * Display Social media with icons
 *
 * @since 1.0
 */
function neal_social_icons() {
	// get theme customizer data 
	$copyright  = get_option( 'neal_socialcopy' );

	// socials
	if ( $copyright['general-social-label'] ) {
		echo '<div class="social-icons">';

		// social icon 1
		if ( trim( $copyright['general-social-url1' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url1' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon1' ]).'"></i></span></a>';
		}

		// social icon 2
		if ( trim( $copyright['general-social-url2' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url2' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon2' ]).'"></i></span></a>';
		}

		// social icon 3
		if ( trim( $copyright['general-social-url3' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url3' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon3']).'"></i></span></a>';
		}

		// social icon 4
		if ( trim( $copyright['general-social-url4' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url4' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon4']).'"></i></span></a>';
		}

		// social icon 5
		if ( trim( $copyright['general-social-url5' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url5' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon5']).'"></i></span></a>';
		}

		// social icon 6
		if ( trim( $copyright['general-social-url6' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url6' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon6']).'"></i></span></a>';
		}

		// social icon 7
		if ( trim( $copyright['general-social-url7' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url1' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon7']).'"></i></span></a>';
		}

		// social icon 8
		if ( trim( $copyright['general-social-url8' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url8' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon8']).'"></i></span></a>';
		}

		// social icon 9
		if ( trim( $copyright['general-social-url9' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url9' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon9']).'"></i></span></a>';
		}

		// social icon 10
		if ( trim( $copyright['general-social-url10' ] ) !== '' ) {
			echo '<a href="'.esc_url($copyright['general-social-url10' ]).'" target="_blank"><span><i class="fa fa-'.esc_attr($copyright['general-social-icon10']).'"></i></span></a>';
		}
														
		echo '</div>';
	}
}

/**
 * Display Social media
 *
 * @since 1.0
 */
function neal_social_media() {
	// get theme customizer data 
	$copyright  = get_option( 'neal_socialcopy' );

	$socials = array();
	for ( $i = 0;  $i < 10;  $i++ ) {
		if ( isset( $copyright['general-social-icon'.$i ] ) ) {
			$socials[] = $copyright['general-social-icon'.$i ];
		}
	}

	$social_media = array(
		'facebook'		=> 'Facebook',
		'twitter'		=> 'Twitter',
		'instagram'		=> 'Instagram',
		'google-plus'	=> 'Google',
		'pinterest'		=> 'Pinterest',
		'linkedin'		=> 'Linkedin',
		'skype'			=> 'Skype',
		'youtube'		=> 'Youtube',
		'vk'			=> 'Vkontakte',
		'github'		=> 'Github',
		'soundcloud'	=> 'Soundcloud',
		'behance'		=> 'Behance'

	);

	$socials = wp_parse_args( $socials, $social_media );

	// socials
	if ( $copyright['general-social-label'] ) {
		echo '<div class="socials-wrap">';
			echo '<div class="socials">';
				echo '<ul class="list-socials">';

					// social icon 1
					if ( trim( $copyright['general-social-url1' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url1' ]).'" class="'.esc_attr($copyright['general-social-icon1' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon1' ])].'</a></li>';
					}

					// social icon 2
					if ( trim( $copyright['general-social-url2' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url2' ]).'" class="'.esc_attr($copyright['general-social-icon2' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon2' ])].'</a></li>';
					}

					// social icon 3
					if ( trim( $copyright['general-social-url3' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url3' ]).'" class="'.esc_attr($copyright['general-social-icon3' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon3' ])].'</a></li>';
					}

					// social icon 4
					if ( trim( $copyright['general-social-url4' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url4' ]).'" class="'.esc_attr($copyright['general-social-icon4' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon4' ])].'</a></li>';
					}

					// social icon 5
					if ( trim( $copyright['general-social-url5' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url5' ]).'" class="'.esc_attr($copyright['general-social-icon5' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon5' ])].'</a></li>';
					}

					// social icon 6
					if ( trim( $copyright['general-social-url6' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url6' ]).'" class="'.esc_html($copyright['general-social-icon6' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon6' ])].'</a></li>';
					}

					// social icon 7
					if ( trim( $copyright['general-social-url7' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url7' ]).'" class="'.esc_attr($copyright['general-social-icon7' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon7' ])].'</a></li>';
					}

					// social icon 8
					if ( trim( $copyright['general-social-url8' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url8' ]).'" class="'.esc_attr($copyright['general-social-icon8' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon8' ])].'</a></li>';
					}

					// social icon 9
					if ( trim( $copyright['general-social-url9' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url9' ]).'" class="'.esc_attr($copyright['general-social-icon9' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon9' ])].'</a></li>';
					}

					// social icon 10
					if ( trim( $copyright['general-social-url10' ] ) !== '' ) {
						echo '<li><a href="'.esc_url($copyright['general-social-url10' ]).'" class="'.esc_attr($copyright['general-social-icon10' ]).'" target="_blank">'.$social_media[esc_html($copyright['general-social-icon10' ])].'</a></li>';
					}

				echo '</ul>';
			echo '</div>';
		echo '</div>';
	}
}

/**
 * Display numeric pagination
 *
 * @since 1.0
 * @return void
 */
function neal_numeric_pagination( $max_num = '', $paged = '', $prev = '', $next = '' ) {
	global $wp_query;

	if ( $max_num ) {
		$total = $max_num;
	} else {
		$total = $wp_query->max_num_pages;
	}

	if ( $total < 2 ) {
		return;
	}

	if ( $paged ) {
		$current = max( 1, $paged );
	} else {
		$current = max( 1, get_query_var( 'paged' ) );
	}

	?>
	<nav class="navigation pagination numeric-navigation">
		<?php
		$big  = 999999999;
		$args = array(
			'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'total'     => $total,
			'current'   => $current,
			'prev_text' => $prev,
			'next_text' => $next,
			'type'      => 'plain',
		);

		echo paginate_links( $args );
		?>
	</nav>
	<?php
}

if ( ! function_exists( 'neal_post_title' ) ) {
	/**
	 * Post title
	 */
	function neal_post_title( $echo = true ) {
		$output = '<h1 class="entry-title" itemprop="name headline"><a href="'. esc_url(get_the_permalink()) .'" rel="bookmark">'. get_the_title() .'</a></h1>';

		if ( $echo ) {
			echo ''. $output;
		} else {
			return $output;
		}
	}
}

if ( ! function_exists( 'neal_post_categories' ) ) {
	/**
	 * Post categories
	 */
	function neal_post_categories( $echo = true ) {
		$output = '<div class="meta-categories">';
			$output .= '<span class="post-category">';
			  	/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( esc_html__( ' ', 'neal' ) );

				if ( $categories_list ) :
					$output .= wp_kses_post( $categories_list );
				endif; // End if categories.

			$output .= '</span>';
		$output .= '</div>';

		if ( $echo ) {
			echo ''. $output;
		} else {
			return $output;
		}
	}
}

if ( ! function_exists( 'neal_get_post_categories' ) ) {
	/**
	 * Get post categories
	 */
	function neal_get_post_categories() {
		$html = '<div class="meta-categories">';
			$html .= '<span class="post-category">';
			  	/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( esc_html__( ' ', 'neal' ) );

				if ( $categories_list ) :
					$html .= wp_kses_post( $categories_list );
				endif; // End if categories.

			$html .= '</span>';
		$html .= '</div>';

		return $html;
	}
}

if ( ! function_exists( 'neal_post_author' ) ) {
	/**
	 * Post author
	 */
	function neal_post_author( $icon = false, $echo = true ) {
		$output = '<div class="meta-author">';
			$output .= '<span class="post-author">';
				if ( $icon ) {
					if ( $icon == 'none' && is_single() ) {
						$output .= '<i class="fa fa-user"></i>';
					} elseif ( $icon != 'none' ) {
						$output .= '<i class="fa '. esc_attr( $icon ) .'"></i>';
					}
				}

				$output .= get_the_author_posts_link();
			$output .= '</span>';
		$output .= '</div>';

		if ( $echo ) {
			echo ''. $output;
		} else {
			return $output;
		}
	}
}

if ( ! function_exists( 'neal_post_date' ) ) {
	/**
	 * Post date
	 */
	function neal_post_date( $icon = false, $echo = true ) {
		$output = '<div class="meta-date">';
			$output .= '<span class="post-date">';
				if ( $icon ) {
					if ( $icon == 'none' && is_single() ) {
						$output .= '<i class="fa fa-user"></i>';
					} elseif ( $icon != 'none' ) {
						$output .= '<i class="fa '. esc_attr( $icon ) .'"></i>';
					}
				}
				
				$output .= get_the_time( get_option('date_format') );
			$output .= '</span>';
		$output .= '</div>';

		if ( $echo ) {
			echo ''. $output;
		} else {
			return $output;
		}
	}
}

if ( ! function_exists( 'neal_get_post_date' ) ) {
	/**
	 * Get post date
	 */
	function neal_get_post_date( $icon = false ) {
		$html = '<div class="meta-date">';
			$html .= '<span class="post-date">';
				if ( $icon == 'none' && is_single() ) {
					$html .= '<i class="fa fa-clock-o"></i>';
				} elseif ( $icon && $icon != 'none' ) {
					$html .= '<i class="fa '. esc_attr( $icon ) .'"></i>';
				}
				
				$html .= get_the_time( get_option('date_format') );
			$html .= '</span>';
		$html .= '</div>';

		return $html;
	}
}

if ( ! function_exists( 'neal_post_tags' ) ) {
	/**
	 * Post tags
	 */
	function neal_post_tags() {

		$tags = get_the_tags();

		if ( $tags ) {
			$html = '<div class="tags-links">';

			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link( $tag->term_id );
				$html .= '<a href="'.$tag_link.'" rel="tag">';
				$html .= $tag->name;
				$html .= '</a>';
			}
			
			$html .= '</div>';
			echo ''. $html;
		}
	}
}

if ( ! function_exists( 'neal_strip_shortcodes_excerpt' ) ) {
	/**
	 * Custom strip shortcodes
	 *
	 * @param string $content Post Content
	 */
	function neal_strip_shortcodes_excerpt( $content = '' ) {

	    $raw_excerpt = $content;

	    if ( $content == '' ) {
	        $content = get_the_content('');
	        $content = do_shortcode( $content );
	        $content = apply_filters( 'the_content', $content );
	        $content = str_replace( ']]>', ']]>', $content );
	        $content = wp_trim_words( $content );
	    }

	    return apply_filters('wp_trim_excerpt', $content, $raw_excerpt );
	}

}

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt'  );
add_filter( 'get_the_excerpt', 'neal_strip_shortcodes_excerpt'  );

if ( ! function_exists( 'neal_custom_post_content' ) ) {
	/**
	 * Custom Post Content
	 *
	 * @param string $type Post Content type
	 * @param integer $length Content length 
	 */
	function neal_custom_post_content( $type, $length, $echo = true ) {
		if ( $length == 0 ) {
			return;
		}

		$output = '';
		
		if ( $type == 'p_excerpt' ) {
			$output .= neal_custom_excerpt( get_the_excerpt(), $length, true );
		} else {
			$output .= get_the_content('');
		}

		if ( $echo ) {
			echo ''. $output;
		} else {
			return $output;
		}
	}
}

if ( ! function_exists( 'neal_custom_excerpt' ) ) {
	/**
	 * Custom Excerpt
	 *
	 * @param integer $limit Content length
	 */
	function neal_custom_excerpt( $content, $limit, $points = false ) {
	  $excerpt = explode( ' ', $content, esc_html( $limit ) );
	  $point = '';

	  if ( count( $excerpt ) >= $limit ) {

	    array_pop( $excerpt );

	    if ( $points ) {
	    	$point = '...';
	    }

	    $excerpt = implode( " ", $excerpt ).$point;

	  } else {
	    $excerpt = implode( " ", $excerpt );
	  }	

	  $excerpt = preg_replace( '/`[[^]]*]`/', '', $excerpt );
	  
	  return $excerpt;
	}
}

function neal_get_ip() {
	if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}

	$ip = filter_var( $ip, FILTER_VALIDATE_IP );
	$ip = ( $ip === false ) ? '0.0.0.0' : $ip;
	return $ip;
}
 
if ( ! function_exists( 'neal_sharing_icon_links' ) ) {
	/**
	 * Social sharing
	 *
	 * @since  1.0.0
	 */
	function neal_sharing_icon_links( $wrap ) {
		global $post;

		$socials = array();

		if ( is_singular( 'post' ) ) {
			// get theme customizer data
			$blog_single = get_option( 'neal_blog_single' );

			if ( ! $blog_single['sharing-label'] ) {
				return;
			}

			$socials = array(
				'facebook'	=> $blog_single['sharing-fb'],
				'twitter' 	=> $blog_single['sharing-tw'],
				'google-p' 	=> $blog_single['sharing-google-p'],
				'linkedin' 	=> $blog_single['sharing-linke'],
				'pinterest' => $blog_single['sharing-pint']
			);

		} elseif ( is_singular( 'product' ) ) {
			// get theme customizer data
			$shop_single = get_option( 'neal_shop_single' );
			
			$socials = array(
				'facebook'	=> $shop_single['sharing-fb'],
				'twitter' 	=> $shop_single['sharing-tw'],
				'google-p' 	=> $shop_single['sharing-google-p'],
				'linkedin' 	=> $shop_single['sharing-linke'],
				'pinterest' => $shop_single['sharing-pint']
			);
		}

		$html = ( $wrap === true || $wrap === '' ) ? '<div class="social-share">' : '';

		if ( $socials ) {

			if ( is_singular( 'post' ) ) {
				// post like
				$get_ip   = neal_get_ip();
				$get_ips  = get_post_meta( $post->ID, 'neal_post_like_user_ips', true );
				$likes    = get_post_meta( $post->ID, 'neal_post_like', true ) ? get_post_meta( $post->ID, 'neal_post_like', true ) : 0;
				$is_liked = is_array( $get_ips ) && in_array( $get_ip, $get_ips ) ? 'liked animated bounceIn' : '';

				$html .= '<a class="post-like '.esc_attr($is_liked).'" href="#" data-post-id="'.esc_attr($post->ID).'"><i class="fa fa-heart"></i><span>'.esc_html($likes).'</span></a>';
			}

			// facebook
			if ( $socials['facebook'] ) {
				$facebook_url = 'https://www.facebook.com/sharer/sharer.php?u='. esc_url(get_the_permalink());
				$html .= '<a class="facebook" href="'. esc_url( $facebook_url ) .'" target="_blank" data-url="'.esc_url(get_the_permalink()).'"><i class="fa fa-facebook"></i>'.esc_html__('Facebook', 'neal').'</a>';
			}

			// twitter
			if ( $socials['twitter'] ) {
				$twitter_url = 'https://twitter.com/share?'. esc_url(get_permalink()) .'&amp;text='. get_the_title();
				$html .= '<a class="twitter" href="'. esc_url( $twitter_url ) .'" target="_blank" data-url="'.esc_url(get_the_permalink()).'"><i class="fa fa-twitter"></i>'.esc_html__('Twitter', 'neal').'</a>';
			}

			// google plus
			if ( $socials['google-p'] ) {
				$google_plus_url = 'https://plus.google.com/share?url='. esc_url(get_permalink());
				$html .= '<a class="google-p" href="'. esc_url( $google_plus_url ) .'" target="_blank" data-url="'.esc_url(get_the_permalink()).'"><i class="fa fa-google-plus"></i>'.esc_html__('Google', 'neal').'</a>';
			}
				
			// linkedin
			if ( $socials['linkedin'] ) {
				$linkedin_url = 'http://www.linkedin.com/shareArticle?url='. esc_url(get_permalink()) .'&amp;title='. get_the_title();
				$html .= '<a class="linkedin" href="'. esc_url( $linkedin_url ) .'" target="_blank" data-url="'.esc_url(get_the_permalink()).'"><i class="fa fa-linkedin"></i>'.esc_html__('Linkedin', 'neal').'</a>';
			}
				
			// pinterest
			if ( $socials['pinterest'] ) {
				$pinterest_url = 'https://pinterest.com/pin/create/bookmarklet/?url='. esc_url(get_permalink()) .'&amp;description='. get_the_title() .'&amp;media='. esc_url(wp_get_attachment_url( get_post_thumbnail_id($post->ID)) );
					$html .= '<a class="pinterest" href="'. esc_url( $pinterest_url ) .'" target="_blank" data-url="'.esc_url(get_the_permalink()).'"><i class="fa fa-pinterest"></i>'.esc_html__('Pinterest', 'neal').'</a>';
			}
		}

		$html .= ( $wrap === true || $wrap === '' ) ? '</div>' : '';

		echo ''. $html;
	}
}

add_action( 'woocommerce_after_single_product_summary', 'neal_sharing_icon_links', 5 );

if ( ! function_exists('neal_comment_custom_form') ) {
	/**
	 * Comments form & textarea for comment message
	 *
	 * @since  1.0.0
	 */
	function neal_comment_custom_form( $arg ) {

		if ( is_user_logged_in()  ) {
			$arg['comment_field'] = '<div class="row">
										<p class="comment-form-comment col-lg-12">
											<textarea name="comment" id="comment" class="pers-message" rows="8" placeholder="'. esc_attr__( 'Your comment', 'neal' ) .'"></textarea>
										</p>
									</div>';

		} else {
			$arg['comment_field'] = '';
		}

		return $arg;
	}
}

add_filter( 'comment_form_defaults', 'neal_comment_custom_form' );

if ( ! function_exists('neal_comment_custom_fields') ) {
	/**
	 * Comment form fields - author, email, url
	 *
	 * @since  1.0.0
	 */
	function neal_comment_custom_fields( $defaults ) {

		// required fields
		$req 		= get_option('require_name_email');
		$aria_req 	= ( $req ) ? ' aria-required="true"' : '';

		$fields = array(
			'comment_form_before' => '<div class="row">',
			
			'comment_field' => '<p class="comment-form-comment col-lg-12">
									<textarea name="comment" id="comment" class="pers-message" rows="8" placeholder="'. esc_attr__( 'Your comment', 'neal' ) .'"></textarea>
								</p>',

			'author' => '<p class="comment-form-author col-lg-6">
							<input type="text" name="author" id="author" class="pers-name" placeholder="'. esc_attr__( 'Name', 'neal' ) .''. ( $req ? '*' : '' ).'" '. $aria_req .'>
						</p>',
			
			'email' => '<p class="comment-form-email col-lg-6">
							<input type="text" name="email" id="email" class="pers-email" placeholder="' . esc_attr__( 'Email', 'neal' ) . ''. ( $req ? '*' : '' ).'" '. $aria_req .'>
						</p>',
			
			'url' => '<p class="comment-form-url col-lg-12">
					 	<input type="text" name="url" id="url" placeholder="'. esc_attr__( 'Website', 'neal' ) .'">
					  </p>',
			'comment_form_after' => '</div>',
		);

		return $fields;
	}
}

add_filter( 'comment_form_default_fields', 'neal_comment_custom_fields' );

if ( ! function_exists( 'neal_comments' ) ) {
	/**
	 * Comments
	 *
	 * @param array $comment the comment array.
	 * @param array $args the comment args.
	 * @param int   $depth the comment depth.
	 * @since  1.0.0
	 */
	function neal_comments( $comment, $args, $depth ) {
		global $post;

		// get avatar size from theme customizer
		$custom_comments = get_option( 'neal_comments' );

		// normal comment
		if ( get_comment_type() == 'comment' ) { 

			$html  = '<li id="comment-'. get_comment_ID() . '" ' . comment_class( '', null, null, false ) .'>';

			$avatar_size = intval( $custom_comments['author-img-size'] );

			$html .= '<div class="comment-body">';
				// author image
				$html .= '<div class="comment-author-img">'. get_avatar( $comment, $avatar_size ) .'</div>';

				$html .= '<div class="comment-content-wrap">';
					$html .= '<div class="comment-meta">';

						$html .= '<strong class="user">'. get_comment_author_link() .'</strong>';
						$html .= ( $comment->user_id === $post->post_author ) ? '<span class="post-author">' . esc_html__( 'Post author', 'neal' ) . '</span>' : '';
						
						if ( trim( get_edit_comment_link() ) !== '' ) {
							$html .= '<span><a href="'. esc_url(get_edit_comment_link()) .'" target="_blank">'. esc_html__( ' [Edit] ', 'neal' ) .'</a></span>';
						}

						$html .= '<span class="reply">'. get_comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'], "reply_text" => esc_html__( 'Reply', 'neal' ) ) ) ).'</span>';

						$html .= '<span class="date">'. get_comment_date() . esc_html__( ' at ', 'neal' ) . get_comment_time() .'&nbsp;</span>';

					$html .= '</div>';

				echo ''. $html;

						echo '<div class="comment-content">';
							comment_text();
						echo '</div>';
				echo '</div>';

			if ( $comment->comment_approved == '0' ) {
         		echo '<em class="comment-awaiting-moderation">'.$custom_comments['comment-texts-moderation'].'</em>';
         	}

			echo '</div>';

		} // pingback comment
		elseif ( get_comment_type() == 'pingback' || get_comment_type() == 'trackback' ) {
			$html  = '<li id="comment-'. get_comment_ID() . '"' . comment_class( '', null, null, false ) .'>';
			
				$html .= '<div class="comment-body">';

					$html .= '<div class="comment-content-wrap">';

						$html .= '<p class="pingback">'. esc_html__( 'Pingback:&nbsp;', 'neal' ) . get_comment_author_link() .'</p>';
						$html .= '<span><a href="'. esc_url(get_edit_comment_link()) .'" target="_blank">'. esc_html__( 'Edit this', 'neal' ) .'</a></span>';

					$html .= '</div>';
				$html .= '</div>';

			echo ''. $html;
		}
	}
}

/**
 * Call a shortcode function by tag name.
 *
 * @param string $tag     The shortcode whose function to call.
 * @param array  $atts    The attributes to pass to the shortcode function. Optional.
 * @param array  $content The shortcode's content. Default is null (none).
 *
 * @return string|bool False on failure, the result of the shortcode on success.
 * @since  1.0.0
 */
function neal_do_shortcode( $tag, array $atts = array(), $content = null ) {
	global $shortcode_tags;

	if ( ! isset( $shortcode_tags[ $tag ] ) ) {
		return false;
	}

	return call_user_func( $shortcode_tags[ $tag ], $atts, $content, $tag );
}

if ( ! function_exists( 'neal_get_meta_tab_categories' ) ) {
	/**
	 * Get categories of tabs product
	 *
	 * @return array 	Product Categories
	 * @since  1.0.0
	 */
	function neal_get_meta_tab_categories() {
		global $post;

		// get meta data
		$cat_list = get_post_meta( $post->ID, 'neal_tab_cat_list', true );
		$cat_list_exp = explode( ",", $cat_list  );
		return $cat_list_exp;
	}
}

if ( ! function_exists( 'neal_header_logo' ) ) {
	/**
	 * Header Logo
	 *
	 * @since  1.0.0
	 */
	function neal_header_logo() {

		if ( neal_get_option('logo_img-label') ) { ?>
			<a href="<?php echo esc_url( home_url( '/' ) ) ?>" rel="home" class="logo">
				<img src="<?php echo esc_url( neal_get_option('logo_img') ); ?>" class="logo-1" alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" style="max-height: <?php echo esc_html( neal_get_option('logo_img-spac-max-height') ); ?>px">
				<img src="<?php echo esc_url( neal_get_option('logo_img-2') ); ?>" class="logo-2" alt="<?php esc_attr( bloginfo( 'name' ) ); ?>" style="max-height: <?php echo esc_html( neal_get_option('logo_img-spac-max-height') ); ?>px">
			</a>
		<?php } else { ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo neal_get_option('logo_text'); ?></a></h1>
			<?php if ( '' != get_bloginfo( 'description' ) ) { ?>
				<?php if ( neal_get_option('logo_tagline-label') ) : ?>
					<p class="site-description"><?php echo bloginfo( 'description' ); ?></p>
					<?php endif; ?>
			<?php } ?>
		<?php 
		}
	}
}

if ( ! function_exists( 'neal_get_terms_list' ) ) {
	/**
	 * Get list all terms with parents
	 *
	 * @param string $taxonomy
	 * @param integer $term_id
	 * @param string $separator
	 * @param string $html_item HTML element exp: option
	 * @param string $html_item_attr HTML element attribute exp: value
	 * @param string $attr_value HTML element value of attribute exp: term_id
	 *
	 * @since  1.0.0
	 * @return  string
	 */
	function neal_get_terms_list( $taxonomy = 'category', $term_id, $separator = '', $html_item = '', $html_item_attr = '', $attr_value = '' ){

		$args = array(
			'taxonomy'     	=> $taxonomy,
		    'orderby'      	=> 'name',
		    'show_count'   	=> 0,
		    'pad_counts'   	=> 0,
		    'hierarchical' 	=> 1,
		    'hide_empty'   	=> 0,
		    'parent'		=> $term_id,
		);

	    $output = '';

	    $item_tpl = '<'. $html_item . ' ' .'>%s</'. $html_item . '>';

	    $item_tpl_attr = '<'. $html_item . ' ' .'%s="%s">%s</'. $html_item . '>';
	 
	    $terms = get_terms( $args );

	    if ( count( $terms ) > 0 ) {
	        foreach ( $terms as $term ) {

	        	if ( $term->parent > 0 ) {
	        		 $separator .= '&nbsp;&nbsp;&nbsp;'; 
	        	}

	        	$array_term = (array) $term;

	        	// with attribute
	        	if ( $html_item_attr ) {
	        		$output .= sprintf( $item_tpl_attr, $html_item_attr, $array_term[ $attr_value ], $separator . $term->name );

	        	} elseif ( $html_item ) { // without attribute
	        		$output .= sprintf( $item_tpl, $separator . $term->name );
	        	 } else { // without HTML
	        		$output .= $separator . $term->name;
	        	}

	            $output .=  neal_get_terms_list( $taxonomy, $term->term_id, $separator, $html_item, $html_item_attr, $attr_value );
	        }
	    }

	    return $output;
	}
}

/**
 * Get All categories
 */
function neal_get_categories() {

	return neal_get_terms_list( 'category', 0, '', 'option', 'value', 'term_id' );
}

/**
 * Get All categories with options for select elements
 */
function neal_get_categories_option( $term_id = 0, $selected_values = array(), $separator = '' ) {
	$args = array(
			'taxonomy'     	=> 'category',
		    'orderby'      	=> 'name',
		    'show_count'   	=> 0,
		    'pad_counts'   	=> 0,
		    'hierarchical' 	=> 1,
		    'hide_empty'   	=> 0,
		    'parent'		=> $term_id,
		);

	    $output = '';
	 
	    $terms = get_terms( $args );

	    if ( count( $terms ) > 0 ) {
	        foreach ( $terms as $term ) {

	        	if ( $term->parent > 0 ) {
	        		 $separator .= '&nbsp;&nbsp;&nbsp;'; 
	        	}

	        	$array_term = (array) $term;

	        	if ( @in_array( $term->term_id, $selected_values ) ) {
	        		$output .= sprintf( '<option value="%s" selected>%s%s</option>', $term->term_id, $separator, $term->name );
	        	} else {
	        		$output .= sprintf( '<option value="%s">%s%s</option>', $term->term_id, $separator, $term->name );
	        	}

	            $output .=  neal_get_categories_option( $term->term_id, $selected_values, $separator );
	        }
	    }

	    return $output;
}

/**
 * Get All tags
 */
function neal_get_tags( $selected_values = array() ) {
	$tags = get_tags();
	$html = '';
	foreach ( $tags as $tag ) {
		if ( @in_array( $tag->term_id, $selected_values ) ) {
			$html .= '<option value="'.$tag->term_id.'" selected>' . $tag->name . '</option>';
		} else {
			$html .= '<option value="'.$tag->term_id.'">' . $tag->name . '</option>';
		}
	}
	
	return $html;
}

if ( ! function_exists( 'neal_homepage_compo_list' ) ) {
	/**
	 * Homepage components list
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_homepage_compo_list() {
		global $post;

		// get post meta data
		$options = get_post_meta( $post->ID, 'neal_homepage_compo_order', true );
		$components = array();

		if ( isset( $options ) && ! empty( $options ) ) {
					
			// Remove all neal-homepage actions
			remove_all_actions( 'neal-homepage' );

			$priority = 1; // priority
			$components = explode( ',', $options );
			foreach ( $components as $item ) {
				// Add homepage action
				add_action( 'neal-homepage', $item, $priority * 10 );
				$priority++; // add priority value
			}
		}
	}
}

/**
 * Check blog pages
 *
 * @return bool
 */
function neal_is_blog_pages() {
	if ( ! is_front_page() && is_home() || is_category() || is_tag() || is_author() || is_date() ) {
		return true;
	}
	
	return false;
}

/**
 * Check shop pages
 *
 * @return bool
 */
function neal_is_shop_pages() {

	if ( function_exists( 'is_shop' ) && ( is_shop() || is_product_category() || is_product_tag() ) ) {
		return true;
	} 
	
	return false;
}

/**
 * Check content pages
 *
 * @return bool
 */
function neal_is_content_page() {

	// if page is not content  
	if ( neal_is_woocommerce_activated() && ( is_cart() || is_checkout() || is_account_page() ) ) {
		return;
	}

	if ( is_page() ) {
		return true;
	}
}

if ( ! function_exists( 'neal_has_page_header' ) ) {
	/**
	 * Check if current page has page header
	 *
	 * @return bool
	 */
	function neal_has_page_header() {

		if ( is_front_page() && ! is_home() ) {
			return false;
		} elseif ( is_page_template( 'template-contactpage.php' ) ) {
			return false;
		} elseif ( is_single() ) {
			return false;
		} elseif ( is_page() && get_post_meta( get_the_ID(), 'neal_hide_page_header', true ) ) {
			return false;
		} elseif ( is_home() ) {
			$posts_page_id = get_option( 'page_for_posts' );

			if ( $posts_page_id ) {
				return true;
			}
		} elseif ( is_404() ) { 
			return false;
		} elseif ( function_exists( 'WC' ) ) {
			if ( is_shop() && get_post_meta( wc_get_page_id('shop'), 'neal_hide_page_header', true )  ) {
				return false;
			} elseif ( is_cart() && get_post_meta( wc_get_page_id('cart'), 'neal_hide_page_header', true ) 
				|| is_checkout() && get_post_meta( wc_get_page_id('checkout'), 'neal_hide_page_header', true ) 
				|| is_account_page() && get_post_meta( wc_get_page_id('myaccount'), 'neal_hide_page_header', true ) ) {
				return false;
			}
		}

		return true;

	}
}

/**
 * Filter to archive title and add page title for singular pages
 *
 * @param string $title
 * @return string
 */
function neal_the_archive_title( $title ) {
	if ( function_exists( 'is_shop' ) && is_shop() ) {
		$title = get_the_title( get_option( 'woocommerce_shop_page_id' ) );
	} elseif ( function_exists( 'is_checkout' ) && is_checkout() ) {
		$title = get_the_title( get_option( 'woocommerce_checkout_page_id' ) );
	} elseif ( function_exists( 'is_cart' ) && is_cart() ) {
		$title = get_the_title( get_option( 'woocommerce_cart_page_id' ) );
	} elseif ( function_exists( 'is_account_page' ) && is_account_page() ) {
		$title = get_the_title( get_option( 'woocommerce_myaccount_page_id' ) );
	} elseif ( is_tax() ) {
		$title = single_term_title( '', false );
	} elseif ( is_home() ) {
		$title = 'page' == get_option( 'show_on_front' ) ? get_the_title( get_option( 'page_for_posts' ) ) : esc_html__( 'Blog', 'neal' );
	}

	$title_exp = explode( ':', $title );

	if ( count( $title_exp ) > 1 ) {
		$title = '<span>' . trim( $title_exp[0] ) .':</span>'. trim( $title_exp[1] );
	}

	return $title;
}

add_filter( 'get_the_archive_title', 'neal_the_archive_title' );


/**
 * Display breadcrumbs
 */
function neal_display_breadcrumbs() {
	neal_breadcrumbs();
}

if ( ! function_exists( 'neal_breadcrumbs' ) ) {
	/**
	 * Custom breadcrumbs for posts, pages, tags, archive pages etc.
	 *
	 * @param array | string $args
	 * @since 1.0.0
	 */
	function neal_breadcrumbs( $args = '' ) {
		global $post;

		$args = wp_parse_args( $args, array(
			'separator'         => '<i class="fa fa-angle-right"></i>',
			'taxonomy'          => 'category',
			'display_last_item' => true,
			'show_on_front'     => true,
			'labels'            => array(
				'home'      => esc_html__( 'Home', 'neal' ),
				'archive'   => esc_html__( 'Archives', 'neal' ),
				'blog'      => esc_html__( 'Blog', 'neal' ),
				'search'    => esc_html__( 'Search results for', 'neal' ),
				'not_found' => esc_html__( 'Not Found', 'neal' ),
				'author'    => esc_html__( 'Author:', 'neal' ),
				'day'       => esc_html__( 'Daily:', 'neal' ),
				'month'     => esc_html__( 'Monthly:', 'neal' ),
				'year'      => esc_html__( 'Yearly:', 'neal' ),
			),
		) );
	       
	      
	    $html = '<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';

	    $html_tpl = '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
      		<a itemprop="item" href="%s"><span itemprop="name">%s</span>
      		</a><meta itemprop="position" content="%s" /></li>';

      	$html_text_tpl = '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
      		<span itemprop="name">%s</span>
      		<meta itemprop="position" content="%s" />
      		</li>';
	           
	    // Home
	    $html .= sprintf( $html_tpl, esc_url( get_home_url() ), $args['labels']['home'], 1 );
	    $html .= '<li class="separator">' . $args['separator'] . '</li>';

	    // Front page
	    if ( is_front_page() ) {
	    	$html .= sprintf( $html_text_tpl, get_the_archive_title(), 2 );
	    } 
	    // Blog page
	    elseif ( is_home() && ! is_front_page() ) {
	    	$html .= sprintf( $html_text_tpl, get_the_title( get_option( 'page_for_posts', true ) ), 2 );

	    } 
	    // Custom post type
	    elseif ( is_archive() && is_tax() && ! is_category() && ! is_tag() ) {
	    	$post_type = get_post_type();
	              
	        if ( $post_type != 'post' ) {
	        	$post_type_object = get_post_type_object( $post_type );
	            $post_type_archive = get_post_type_archive_link( $post_type );
	              
	            $html .= sprintf( $html_tpl, esc_url( $post_type_archive ), $post_type_object->labels->name, 2 );
	            $html .= '<li class="separator">' . $args['separator'] . '</li>';
	              
	        }
	              
	       $tax_name = get_queried_object()->name;
	       $html .= sprintf( $html_text_tpl,  $tax_name, 2 );
	              
	   	} 
	   	// Single
	   	elseif ( is_single() ) {
	    	// get post category info
	        $category = get_the_category();
	        $output = '';
	        $count = 2;
	        
	        if ( ! empty( $category ) ) {

	        	// get last category
	            $last_category = end( $category );

	       		// get parent categories
	        	$get_cat_parents = rtrim( get_category_parents( $last_category->term_id, false, ',', true ), ',' );
	            $cat_parents = explode( ',', $get_cat_parents );

	            foreach ( $cat_parents as $parents ) {
	            	$term 		= get_category_by_slug( $parents );
	            	$link 		= get_category_link( $term->term_id );
	            	$output    .= sprintf( $html_tpl, esc_url( $link ), $term->name, $count );
	                $output    .= '<li class="separator">' . $args['separator'] . '</li>';
	                $count ++;
	            }
	             
	        }
	         
	        $html .= $output;
	        $html .= sprintf( $html_text_tpl, get_the_title(), $count );

	    } 
	    // Category
	    elseif ( is_category() ) {
	   		$category = get_category( get_query_var('cat'), false );
	        $output = '';
	        $count = 2;
	        
	        if ( $category->parent != 0 ) {
	        	
	            // get parent categories
	        	$get_cat_parents = rtrim( get_category_parents( $category->parent, false, ',', true ), ',' );
	            $cat_parents = explode( ',', $get_cat_parents );

	            foreach ( $cat_parents as $parents ) {
	            	$term 		= get_category_by_slug( $parents );
	            	$link 		= get_category_link( $term->term_id );
	            	$output    .= sprintf( $html_tpl, esc_url( $link ), $term->name, $count );
	                $output    .= '<li class="separator">' . $args['separator'] . '</li>';

	                $count ++;
	            }  
	        }
	               
	        $html .= $output;
	        $html .= sprintf( $html_text_tpl, single_cat_title( '', false ), $count );
	               
	    } 
	    // Pages
	    elseif ( is_page() ) {
	    	$count = 2;

	    	if ( $post->post_parent ) {
	        	$parents = get_post_ancestors( $post->ID ); // get page parents
	            $parents = array_reverse( $parents );
	                   
	            if ( ! isset( $output ) ) {
	           		$output = null;
	            }

	            foreach ( $parents as $parent ) {
	            	$output .= sprintf( $html_tpl, esc_url( get_permalink( $parent ) ), get_the_title( $parent ), $count );
	                $output .= '<li class="separator"> ' . $args['separator'] . ' </li>';

	                $count ++;
	            }
	                   
	           	$html .= $output;
	                   
	            $html .= sprintf( $html_text_tpl, get_the_title(), $count );
	                   
	        } else {
	        	$html .= sprintf( $html_text_tpl, get_the_title(), $count );
	        }
	               
	    } 
	    // Tags
	    elseif ( is_tag() ) {
	        $tag_id = get_query_var('tag_id'); // get tag id
	        $terms 	= get_terms( 'post_tag', 'include=' . $tag_id );
	               
	        // display the tag name
	        $html .= sprintf( $html_text_tpl, $terms[0]->name, 2 );
	           
	    } 
	    // Day archive
	    elseif ( is_day() ) {
	    	$html .= sprintf(
				$html_text_tpl,
				sprintf( esc_html__( '%s %s', 'neal' ), $args['labels']['day'], get_the_date() ),
				2
			);
	    } 
	    // Month archive
	    elseif ( is_month() ) {
	    	$html .= sprintf(
				$html_text_tpl,
				sprintf( esc_html__( '%s %s', 'neal' ), $args['labels']['month'], get_the_date( 'F Y' ) ),
				2
			);
	   	} 
	   	// Year archive
	   	elseif ( is_year() ) {
	    	$html .= sprintf(
				$html_text_tpl,
				sprintf( esc_html__( '%s %s', 'neal' ), $args['labels']['year'], get_the_date( 'Y' ) ),
				2
			);
	    } 
	    // Author
	    elseif ( is_author() ) {
	    	$html .= sprintf( $html_text_tpl, $args['labels']['author'] . ' ' . get_the_author(), 2 );
	    } 
	    // Search
	    elseif ( is_search() ) {
	    	$html .= sprintf( $html_text_tpl, $args['labels']['search'] . ' ' . get_search_query() , 2 );  
	    } 
	    // 404
	    elseif ( is_404() ) {
	    	$html .= sprintf( $html_text_tpl, $args['labels']['not_found'], 2 );
	    }
	       
	    $html .= '</ul>';

	    echo ''. $html;
	}
}

if ( ! function_exists( 'neal_get_option' ) ) {
	/**
	 * This is a shorthand function for getting setting value from customizer
	 *
	 * @param string
	 */
	function neal_get_option( $id ) {
		$prefix = 'neal_';
		$option = explode( '_',  $id );
		$option_id = $prefix . $option[0];
		$value = $option[1];

		if ( count( $option ) > 2 ) {
			$option_id = $prefix . $option[0] .'_'. $option[1];
			$value = $option[2];
		}

		$options = get_option( $option_id );

		return $options[ $value ];
	}
}

if ( ! function_exists( 'neal_get_layout' ) ) {
	/**
	 * Get layout
	 *
	 * @return string $layout
	 */
	function neal_get_layout() {
		$layout = '';

		if ( neal_is_blog_pages() ) {
			if ( ! neal_get_option( 'blog_sidebar-label' ) ) {
				$layout = 'no-sidebar';
			}
		} elseif ( is_singular( 'product' ) ) {
			$layout = '';
			if ( ! neal_get_option( 'shop_single_sidebar-label' ) ) {
				$layout = 'no-sidebar';
			}
		} elseif ( neal_is_shop_pages() ) {
			$layout = '';
			if ( ! neal_get_option( 'shop_sidebar-label' ) ) {
				$layout = 'no-sidebar';
			}

		} elseif ( is_404() ) {
			$layout = 'no-sidebar';
		} elseif ( is_singular( 'post' ) ) {
			// get post meta data
			$items  = neal_get_post_options_data();
			$layout = '';
			if ( ! $items['sidebar'] ) {
				$layout = 'no-sidebar';
			}
		} elseif ( is_page() ) {
			$layout = 'no-sidebar';
		}

		return $layout;
	}
}

if ( ! function_exists( 'neal_get_content_columns' ) ) {
	/**
	 * Get content columns
	 *
	 * @param string $layout
	 * @return string $classes
	 */
	function neal_get_content_columns( $layout = null ) {
		$layout = $layout ? $layout : neal_get_layout();
		$classes = array( 'col-lg-9', 'col-md-9', 'col-sm-12', 'col-xs-12' );

		if ( $layout == 'no-sidebar' ) {
			$classes = array( 'col-xs-12' );
		} elseif ( $layout == 'boxed-no-sidebar' ) {
			$classes = array( 'centered col-sm-10 col-xs-12' );
		}

		// no-sidebar on shop single
		if ( is_singular('product') && ! neal_get_option( 'shop_single_sidebar-label' ) ) {
			$classes = array( 'col-sm-10 col-xs-12 centered' );
		}

		return $classes;
	}
}

if ( ! function_exists( 'neal_content_columns' ) ) {
	/**
	 * Display content columns
	 *
	 * @since 1.0.0
	 */
	function neal_content_columns( $layout = null ) {
		echo implode( ' ', neal_get_content_columns( $layout ) );
	}
}

function neal_get_template_part( $slug, $format, $image_size, $post_type = '' ) {
	global $neal_custom_img_size, $h_post_type;

	$h_post_type = $post_type;
	$neal_custom_img_size = $image_size;

	get_template_part( $slug, $format );
}

function neal_get_custom_image_size() {
	global $neal_custom_img_size;
	return $neal_custom_img_size;
}

if ( ! function_exists( 'neal_blog_thumbnail_sizes' ) ) {
	/**
	 * Set blog thumbnails size
	 *
	 * @since 1.0.0
	 */
	function neal_blog_thumbnail_sizes() {

		$img_size = 'full';

		// full image in blog classic
		if ( neal_get_option( 'blog_layout-columns' ) == 'classic' ) {
			$img_size = 'neal-blog-img-full-size';
		} elseif ( neal_get_option( 'blog_layout-columns' ) == 'list' ) {
			$img_size = 'neal-posts-list-image';
		} else {
			$img_size = 'neal-full-posts-image';
		}

		return $img_size;
	}
}

function neal_post_thumbnail_liked() {
	global $h_post_type;

	if ( ! $h_post_type ) {
		return;
	}

	// get post meta data
	$home_id 	= get_option( 'page_on_front' );
	$items 		= get_post_meta( $home_id, 'neal_'. $h_post_type .'_items', true );

	if ( ! $items['hover_like'] ) {
		return;
	}
	
	$post_id  = get_the_ID();
	$get_ip   = neal_get_ip();
	$get_ips  = get_post_meta( $post_id, 'neal_post_like_user_ips', true );
	$likes    = get_post_meta( $post_id, 'neal_post_like', true ) ? get_post_meta( $post_id, 'neal_post_like', true ) : 0;
	$is_liked = is_array( $get_ips ) && in_array( $get_ip, $get_ips ) ? 'liked' : '';
?>
	<span class="post-like <?php echo esc_attr( $is_liked ); ?>" data-post-id="<?php echo esc_attr( $post_id ); ?>"><i class="fa fa-heart"></i><span><?php echo esc_html( $likes ); ?></span></span>
<?php
}

function neal_get_css_animations( $args = array() ) {

	$defaults = array(
		'name'			=> '',
		'id'			=> '',
		'an_in_val' 	=> '',
		'an_out_val'	=> '',
		'an_in_label'	=> '',
		'an_out_label'	=> '',
	);

	$args = wp_parse_args( $args, $defaults );

	// animations in
	$animations_in = array(
		'Bouncing Entrances' => array(
			'bounceIn',
			'bounceInDown',
			'bounceInLeft',
			'bounceInRight',
			'bounceInUp'
		),
		'Fading Entrances' => array(
			'fadeIn',
			'fadeInDown',
			'fadeInDownBig',
			'fadeInLeft',
			'fadeInLeftBig',
			'fadeInRight',
			'fadeInRightBig',
			'fadeInUp',
			'fadeInUpBig'
		),
		'Flippers' => array(
			'flip',
			'flipInX',
			'flipInY',
		),
		'Lightspeed' => array(
			'lightSpeedIn'
		),
		'Rotating Entrances' => array(
			'rotateIn',
			'rotateInDownLeft',
			'rotateInDownRight',
			'rotateInUpLeft',
			'rotateInUpRight',
		),
		'Sliding Entrances' => array(
			'slideInUp',
			'slideInDown',
			'slideInLeft',
			'slideInRight'
		),
		'Zoom Entrances' => array(
			'zoomIn',
			'zoomInDown',
			'zoomInLeft',
			'zoomInRight',
			'zoomInUp'
		)
	);

	// animations out
	$animations_out = array(
		'Bouncing Exits' => array(
			'bounceOut',
			'bounceOutDown',
			'bounceOutLeft',
			'bounceOutRight',
			'bounceOutUp'
		),
		'Fading Exits' => array(
			'fadeOut',
			'fadeOutDown',
			'fadeOutDownBig',
			'fadeOutLeft',
			'fadeOutLeftBig',
			'fadeOutRight',
			'fadeOutRightBig',
			'fadeOutUp',
			'fadeOutUpBig',
		),
		'Flippers' => array(
			'flip',
			'flipOutX',
			'flipOutY',
		),
		'Lightspeed' => array(
			'lightSpeedOut'
		),
		'Rotating Exits' => array(
			'rotateOut',
			'rotateOutDownLeft',
			'rotateOutDownRight',
			'rotateOutUpLeft',
			'rotateOutUpRight',
		),
		'Sliding Exits' => array(
			'slideOutUp',
			'slideOutDown',
			'slideOutLeft',
			'slideOutRight'
		),
		'Zoom Exits' => array(
			'zoomOut',
			'zoomOutDown',
			'zoomOutLeft',
			'zoomOutRight',
			'zoomOutUp'
		)
	);

	$animate_in = array();
	$animate_out = array();

	// animate in
	foreach ( $animations_in as $group => $animates ) {
		$animate_in[] = sprintf( '<optgroup label="%s">', $group );

		foreach ( $animates as $animate ) {
			if ( $animate == $args['an_in_val'] ) {
				$selected = 'selected';
			} else {
				$selected = '';
			}

			$animate_in[] = sprintf( '<option value="%s" %s>%s</option>', $animate, $selected, $animate );
		}

		$animate_in[] = sprintf( '</optgroup>' );
	}

	$output = sprintf(
			'<label>' .
			'%s' .
			'<select name="%s_animate_in" id="%s_animate_in">' .
		 	'%s' .
		 	'</select>' .
		 	'</label>' ,
		 	$args['an_in_label'],
		 	$args['name'],
		 	$args['id'],
			implode( '', $animate_in )
		);


	// animate out
	foreach ( $animations_out as $group => $animates ) {
		$animate_out[] = sprintf( '<optgroup label="%s">', $group );

		foreach ( $animates as $animate ) {
			if ( $animate == $args['an_out_val'] ) {
				$selected = 'selected';
			} else {
				$selected = '';
			}

			$animate_out[] = sprintf( '<option value="%s" %s>%s</option>', $animate, $selected, $animate );
		}

		$animate_out[] = sprintf( '</optgroup>' );
	}

	$output .= sprintf(
			'<label>' .
			'%s' .
			'<select name="%s_animate_out" id="%s_animate_out">' .
		 	'%s' .
		 	'</select>' .
		 	'<label>' ,
		 	$args['an_out_label'],
		 	$args['name'],
		 	$args['id'],
			implode( '', $animate_out )
		);

	return $output;
}

/**
 * Query WooCommerce activation
 */
function neal_is_woocommerce_activated() {
	return class_exists( 'woocommerce' ) ? true : false;
}