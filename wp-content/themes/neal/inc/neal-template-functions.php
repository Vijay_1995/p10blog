<?php
/**
 * Neal template functions.
 *
 * @package neal
 */

if ( ! function_exists( 'neal_contact_page_frontend' ) ) {
	/**
	 * Contact page front-end
	 *
	 * @since  1.0.0
	 */
	function neal_contact_page_frontend() {
		// get theme customizer data
		$contact = get_option( 'neal_contact' );
	?>
		<h1 class="contact-page-title"><?php the_title(); ?></h1>

		<div class="container">
		<div class="row">
		<div id="primary" class="content-area col-xs-10 centered">
				<div class="contact-wrap">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="row">
								<div class="contact-image col-xs-12">
									<img src="<?php echo esc_attr( neal_get_option( 'contact_info-short-image') ); ?> ">
								</div>

								<?php if ( $contact['form-label'] ) : ?>
									<div class="contact-form col-xs-12">
										<h3 class="contact-title"><?php echo esc_html( $contact['form-title'] ); ?></h3>

										<form action="<?php echo esc_url(get_permalink()); ?>" method="POST">
											<div class="row">
												<div class="input-1 col-sm-6 col-xs-12">
													<input type="text" name="con-p-name" placeholder="<?php esc_attr_e( 'Name *', 'neal' );?>" required="true">
												</div>
												<div class="input-2 col-sm-6 col-xs-12">
													<input type="email" name="con-p-email" placeholder="<?php esc_attr_e( 'Email *', 'neal' ); ?>" required="true">
												</div>
												<div class="input-3 col-sm-12 col-xs-12">
													<input type="text" name="con-p-subject" placeholder="<?php esc_attr_e( 'Subject', 'neal' ); ?>">
												</div>
												<div class="input-4 col-sm-12 col-xs-12">
													<textarea placeholder="<?php esc_attr_e( 'Message *', 'neal' ); ?>" style="height:220px" required="true" name="con-p-msg"></textarea>

													<input type="submit" value="<?php esc_attr_e( 'Send Message', 'neal' ); ?>" class="submit-btn button">
													<input type="hidden" id="con-p-submit" name="con-p-submit" value="true">
												</div>
											</div>

										</form>
									</div>
								<?php endif; ?>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="row">
								<?php if ( $contact['info-label'] ) : ?>
									<div class="contact-info col-xs-12">
										<?php 

											// short info
											if ( $contact['info-short-info'] ) {
												echo '<p>'.esc_html($contact['info-short-info']).'</p>';
											}

											// email
											if ( $contact['info-email'] ) {
												echo '<span class="info">';
													echo '<h3>'.esc_html__( 'E-mail', 'neal' ).'</h3>';
													echo '<span class="info-value">' . esc_html( $contact['info-email'] ) . '</span>';
												echo '</span>';
											}

											// phone
											if ( $contact['info-phone'] ) {
												echo '<span class="info">';
													echo '<h3>'.esc_html__( 'Phone', 'neal' ).'</h3>';
													echo '<span class="info-value">' . esc_html( $contact['info-phone'] ) . '</span>';
												echo '</span>';
											}

											// address
											if ( $contact['info-address'] ) {
												echo '<span class="info">';
													echo '<h3>'.esc_html__( 'Address', 'neal' ).'</h3>';
													echo '<span class="info-value">' . esc_html( $contact['info-address'] ) . '</span>';
												echo '</span>';
											}

											?>

											<?php if ( $contact['google-map-label'] ) : ?>
												<!-- GOOGLE MAP -->
												<div class="<?php echo (esc_attr($contact['google-map-layout']) == "boxed" ? "container" : "map" ); ?>">
													<div class="google-map" data-zoom="<?php echo esc_attr($contact['google-map-zoom']); ?>" data-map-type="<?php echo esc_attr($contact['google-map-type']); ?>" data-location="<?php echo esc_attr($contact['google-map-location']); ?>" data-title="<?php echo esc_attr($contact['google-map-martitle']); ?>" data-type-control="<?php echo esc_attr($contact['google-map-tcontrol']); ?>" data-nav="<?php echo esc_attr($contact['google-map-nav']); ?>" data-marker-icon="<?php echo esc_attr( $contact['google-map-marker-icon'] ); ?>"></div>
												</div>
												<!-- GOOGLE MAP END -->
										<?php endif; ?>

									</div>
								<?php endif; ?>
							</div>
							</div>
						</div>
				</div>


		</div> <!-- #primary -->
		</div>
		</div>

	<?php
	}
}

if ( ! function_exists( 'neal_display_comments' ) ) {
	/**
	 * Display comments
	 *
	 * @since  1.0.0
	 */
	function neal_display_comments() {

		// get theme customizer data
		$comments = get_option( 'neal_comments' );

		// If comments are open or we have at least one comment, load up the comment template.
		if ( $comments['blog-display-label'] ) {

			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template( '', true );
			endif;
		}
	}
}

if ( ! function_exists( 'neal_blog_single_subscribe' ) ) {
	/**
	 * Subscribe on blog single
	 *
	 * @since  1.0.0
	 */
	function neal_blog_single_subscribe() {

		if ( ! neal_get_option( 'blog_single_subscribe-box-label' ) ) {
			return;
		}

		// get post meta data
		$items   = neal_get_post_options_data();
		$classes = 'col-xs-10 centered';

		if ( $items['post_layouts'] == 2 || $items['post_layouts'] == 3 ) {
			$classes = 'col-xs-12';
		}

	?>
		<div class="container">
			<div class="row">
				<div class="single-subscribe-form <?php echo esc_attr( $classes ); ?>">
					<?php echo do_shortcode( '[jetpack_subscription_form title="'.esc_attr( neal_get_option( 'blog_single_subscribe-box-title' ) ).'" subscribe_text="'.esc_attr( neal_get_option( 'blog_single_subscribe-box-desc' ) ).'" subscribe_placeholder="'.esc_attr( neal_get_option( 'blog_single_subscribe-box-place' ) ).'" subscribe_button="'.esc_attr( neal_get_option( 'blog_single_subscribe-box-button' ) ).'" ]' ); ?>
				</div>
			</div>
		</div>

	<?php
	}
}

if ( ! function_exists( 'neal_html_tag_schema' ) ) {
	/**
	 * Schema type
	 *
	 * @return void
	 */
	function neal_html_tag_schema() {
		$schema = 'http://schema.org/';
		$type   = 'WebPage';

		if ( is_singular( 'post' ) ) {
			$type = 'Article';
		} elseif ( is_author() ) {
			$type = 'ProfilePage';
		} elseif ( is_search() ) {
			$type 	= 'SearchResultsPage';
		}

		echo 'itemscope="itemscope" itemtype="' . esc_attr( $schema ) . esc_attr( $type ) . '"';
	}
}

if ( ! function_exists( 'neal_header_layouts' ) ) {
	/**
	 * Display site header
	 *
	 * @return void
	 */
	function neal_header_layouts() {
		$header_layout = neal_get_option( 'header_general-header-layout' );
		$header_layout = $header_layout ? $header_layout : 1;
		get_template_part( 'template-parts/headers/layout', $header_layout );
	}
}

if ( ! function_exists( 'neal_footer_layouts' ) ) {
	/**
	 * Display site footer
	 *
	 * @return void
	 */
	function neal_footer_layouts() {
		$footer_layout = neal_get_option( 'footer_general-footer-layout' );
		$footer_layout = $footer_layout ? $footer_layout : 1;
		get_template_part( 'template-parts/footers/layout', $footer_layout );
	}
}

if ( ! function_exists( 'neal_footer_instagram' ) ) {
	/**
	 * Display the footer instagram photos
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_footer_instagram() {
		if ( ! neal_get_option( 'footer_instagram-label' ) && function_exists('neal_instagram') ) {
			return;
		}
	?>
		<div class="instagram-footer">
			<?php echo neal_instagram( 
						esc_html(neal_get_option( 'footer_instagram-username' )), 
					   	esc_html(neal_get_option( 'footer_instagram-number' )), 
						esc_html(neal_get_option( 'footer_instagram-carousel' )),
						esc_html(neal_get_option( 'footer_instagram-columns' )),
						esc_html(neal_get_option( 'footer_instagram-carousel-nav' )),
						esc_html(neal_get_option( 'footer_instagram-carousel-loop' )),
						'', 
						'small' ); ?>

			<h3 class="title<?php echo esc_attr(neal_get_option( 'footer_instagram-title-pos' )) && esc_attr(neal_get_option( 'footer_instagram-columns' )) == 5 ? " fixed" : ''; ?>">
				<?php if ( neal_get_option( 'footer_instagram-title-link' ) ) : ?>
					<a href="https://instagram.com/<?php echo esc_attr(neal_get_option( 'footer_instagram-username' )); ?>" target="_blank"><?php echo esc_html( neal_get_option( 'footer_instagram-title' ) ); ?></a>
				<?php else : ?>
					<span><?php echo esc_html( neal_get_option( 'footer_instagram-title' ) ); ?></span>
				<?php endif; ?>
			</h3>
		</div>	

	<?php
	}
}

if ( ! function_exists( 'neal_footer_widgets' ) ) {
	/**
	 * Display the footer widget regions
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_footer_widgets() {
		if ( is_active_sidebar( 'footer-widget-4' ) ) {
			$widget_columns = 4;
		} elseif ( is_active_sidebar( 'footer-widget-3' ) ) {
			$widget_columns = 3;
		} elseif ( is_active_sidebar( 'footer-widget-2' ) ) {
			$widget_columns = 2;
		} elseif ( is_active_sidebar( 'footer-widget-1' ) ) {
			$widget_columns = 1;
		} else {
			$widget_columns = 0;
		}

		if ( $widget_columns > 0 ) : ?>

			<div class="widget-area">
				<div class="container">
					<div class="row">

						<?php
							$i = 0;
							while ( $i < $widget_columns ) : $i++;
								if ( is_active_sidebar( 'footer-widget-' . $i ) ) : ?>

									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 widget-column">
										<?php
											dynamic_sidebar( 'footer-widget-' . intval( $i ) );
										?>
									</div>

								<?php endif;
							endwhile; ?>
					</div>
					<div class="clear"></div>
				</div>
			</div>

		<?php endif;
	}
}

if ( ! function_exists( 'neal_credit' ) ) {
	/**
	 * Display social icons & copyright
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_credit() {

		// get theme customizer data 
		$footer 	= get_option( 'neal_footer' );
		$copyright  = get_option( 'neal_socialcopy' );

		?>
		
		<?php 

		if ( neal_get_option( 'footer_general-footer-layout' ) == 2 ) {
			// social media
			neal_social_media();
		} 

		if ( neal_get_option( 'footer_general-footer-layout' ) != 3 ) : 

			if ( neal_get_option( 'footer_general-footer-layout' ) != 2 ) {
				// socials
				neal_social_icons();
			}
		?>
			<div class="footer-nav">
				<div class="container">
					<?php

					if ( has_nav_menu( 'footer-menu' ) ) {
						wp_nav_menu( array( 
							'theme_location'	=> 'footer-menu',
							'menu_class'	 	=> 'footer-menu'
						) );
					}
					?>
				</div>
			</div>
		<?php endif; ?>

		<div class="site-info">
			<div class="container">
			<?php
				// copyright
				 if ( $copyright['general-copy-label'] ) : ?>
					<p><?php 
					   		// allow elements & attributes in copyright
					    	echo wp_kses( $copyright['general-copy-text'], array(
										'a' => array(
											   	'href' => array(),
											    'title' => array()
											   ),
										'br' => array(),
										'em' => array(),
										'strong' => array(),
										'i' => array(
					   						'class'	=> array()
					   					),
								 	) );
						?>
					</p>
				<?php endif; ?>
			</div>
		</div><!-- .site-info -->
		<?php
	}
}

if ( ! function_exists( 'neal_skip_links' ) ) {
	/**
	 * Skip links
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function neal_skip_links() {
		?>
		<a class="skip-link screen-reader-text" href="#site-navigation"><?php esc_attr_e( 'Skip to navigation', 'neal' ); ?></a>
		<a class="skip-link screen-reader-text" href="#content"><?php esc_attr_e( 'Skip to content', 'neal' ); ?></a>
		<?php
	}
}

if ( ! function_exists( 'neal_page_header' ) ) {
	/**
	 * Display page header
	 *
	 * @since 1.0.0
	 */
	function neal_page_header() {

		if ( neal_has_page_header() ) {
		?>
			<div class="page-header">
				<div class="container">
					<?php
						if ( ! is_singular() ) {
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="taxonomy-description">', '</div>' );
						} elseif ( is_page() ) {
							printf( '<h1 class="page-title">%s</h1>', single_post_title( '', false ) );
						} else {
							printf( '<h1 class="page-title">%s</h1>', single_post_title( '', false ) );
						}
					?>

					<?php if ( neal_get_option( 'header_page-header-bread' ) ) : ?>
						<div class="page-breadcrumbs">
							<?php 
								neal_display_breadcrumbs();
							?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php
		}
	}
}

if ( ! function_exists( 'neal_header_shop_cart' ) ) {
	/**
	 * Header shop cart btn
	 *
	 * @since 1.0.0
	 */
	function neal_header_shop_cart() {
		if ( ! neal_is_woocommerce_activated() || ! neal_get_option( 'menu_shop-label' ) ) {
			return;
		}
		
		?>
		<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="shop-cart-btn">
			<span class="cart-value">0</span>
			<i class="fa <?php echo esc_attr( neal_get_option( 'menu_shop-icon' ) ); ?>"></i>
		</a>
		<?php
	}
}

if ( ! function_exists( 'neal_header_search_icon' ) ) {
	/**
	 * Header search icon
	 *
	 * @since 1.0.0
	 */
	function neal_header_search_icon() {
		if ( ! neal_get_option( 'menu_search-label' ) ) {
			return;
		}
		
		?>
		<a href="#" class="search-btn"><i class="fa <?php echo esc_attr( neal_get_option( 'menu_search-icon' ) ); ?>"></i></a>
		<?php
	}
}

if ( ! function_exists( 'neal_header_social_media' ) ) {
	/**
	 * Display header social media
	 *
	 * @since 1.0.0
	 */
	function neal_header_social_media() {

		if ( ! neal_get_option( 'header_social-media-label' ) || is_search() ) {
			return;
		}

		$post_id = get_the_ID();

		// shop pages
		if ( neal_is_woocommerce_activated() ) {
			if ( is_shop() ) {
				$post_id = wc_get_page_id('shop');
			} elseif ( is_cart() ) {
				$post_id = wc_get_page_id('cart');
			} elseif ( is_checkout() ) {
				$post_id = wc_get_page_id('checkout');
			} elseif ( is_account_page() ) {
				$post_id = wc_get_page_id('myaccount');
			}
		}

		// pages
		$is_pages = get_post_meta( $post_id, 'neal_hide_header_social_media', true );
		// single products
		$is_products = get_post_meta( $post_id, 'neal_product_options_header_social_media', true );

		if ( $is_pages || is_singular('product') && $is_products ) {
			return;
		} elseif ( is_singular('post') ) {
			// single posts
			$is_posts = get_post_meta( $post_id, 'neal_post_options_items', true );
			if ( isset( $is_posts['header_social_media'] ) && $is_posts['header_social_media'] ) {
				return;
			}
		}

		echo '<div class="header-social-media">';
			neal_social_media();
		echo '</div>';
	}
}

if ( ! function_exists( 'neal_page_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function neal_page_content() {
		?>
		<div class="entry-content" itemprop="mainContentOfPage">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'neal' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
		<?php
	}
}

if ( ! function_exists( 'neal_paging_nav' ) ) {
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function neal_paging_nav( $max_num = '', $paged = '' ) {

		// get data theme customizer
		$pag = get_option( 'neal_pagination' );

		$next_icon = '';
		$prev_icon = '';

		if ( $pag['navigation-icon-fa'] !== 'none' ) {
			$next_icon = '<i class="fa fa-' . esc_attr($pag['navigation-icon-fa']) . '-right"></i>';
			$prev_icon = '<i class="fa fa-' . esc_attr($pag['navigation-icon-fa']) . '-left"></i>';
		}

		// numeric pagination
		if ( $pag['navigation-pag-type'] == 'numeric' || $pag['navigation-pag-type'] == 'load-more' || $pag['navigation-pag-type'] == 'infinite-scroll' ) {

			$prev = sprintf( esc_html__( '%s Previous', 'neal' ), $prev_icon );
			$next = sprintf( esc_html__( 'Next %s', 'neal' ), $next_icon );

			if ( $pag['navigation-pag-type'] == 'load-more' ) {
				$next = '<span class="button-text">' . esc_html__( 'Load More', 'neal' ) . '</span>';
			}

			neal_numeric_pagination( $max_num, $paged, $prev, $next );

		} elseif ( $pag['navigation-pag-type'] == 'default' ) { // default text pagination

			$prev_label = sprintf( esc_html__( '%s Previous Page', 'neal' ), $prev_icon );
			$next_label = sprintf( esc_html__( 'Next Page %s', 'neal' ), $next_icon );

			$html = '<nav class="navigation default">';
				$html .= '<div class="default-prev-link">'. get_previous_posts_link( $prev_label ) .'</div>';
				$html .='<div class="default-next-link">'. get_next_posts_link( $next_label ) .'</div>';
			$html .= '</nav>';

			echo ''. $html;

		}

	}
}

if ( ! function_exists( 'neal_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function neal_post_nav( $n_text = '', $p_text = '' ) {

		// get theme customizer data
		$blog_single = get_option( 'neal_blog_single' );

		if ( ! $blog_single['post-navigation-label'] ) {
			return false;
		}

		if ( ! $n_text || ! $p_text ) {
			$n_text = esc_html__( 'Next Article', 'neal' );
			$p_text = esc_html__( 'Previous Article', 'neal' );
		}

		// navigation icon
		if ( $blog_single['post-navigation-icon-fa'] !== 'none' ) {
			$next_icon = '<i class="fa fa-'.esc_attr($blog_single['post-navigation-icon-fa']).'-right"></i>';
			$prev_icon = '<i class="fa fa-'.esc_attr($blog_single['post-navigation-icon-fa']).'-left"></i>';
		} else {
			$next_icon = '';
			$prev_icon = '';
		}

		// navigation title
		if ( $blog_single['post-navigation-title'] ) {
			$next_title = '<h3>%title '.$next_icon.'</h3>';
			$prev_title = '<h3>'.$prev_icon.' %title</h3>';
		} else {
			$next_title = '';
			$prev_title = '';
		}

		$args = array(
			'next_text' => '<span>'.$n_text.'</span>'.$next_title,
			'prev_text' => '<span>'.$p_text.'</span>'.$prev_title,
			);

		the_post_navigation( $args );
	}
}

if ( ! function_exists( 'neal_open_content_container' ) ) {
	/**
	 * Print the open tags of site content container
	 *
	 * @since  1.0.0
	 */
	function neal_open_content_container() {
		if ( is_page_template( 'template-homepage.php' ) 
			|| is_page_template( 'template-contactpage.php' ) 
			|| is_single() && ! is_singular('product') 
			|| is_page() && ! is_page_template( 'template-portfolio.php' ) ) {
			return;
		}

		echo '<div class="container">';

		echo '<div class="row">';
	}
}

if ( ! function_exists( 'neal_close_content_container' ) ) {
	/**
	 * Print the close tags of site content container
	 *
	 * @since  1.0.0
	 */
	function neal_close_content_container() {
		if ( is_page_template( 'template-homepage.php' ) 
			|| is_page_template( 'template-contactpage.php' ) 
			|| is_single() && ! is_singular('product') 
			|| is_page() && ! is_page_template( 'template-portfolio.php' ) ) {
			return;
		}

		echo '</div>';

		echo '</div>';
	}
}

if ( ! function_exists( 'neal_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function neal_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s" itemprop="datePublished">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s" itemprop="datePublished">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			_x( '%s', 'post date', 'neal' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo wp_kses( apply_filters( 'neal_single_post_posted_on_html', '<span class="posted-on">' . $posted_on . '</span>', $posted_on ), array(
			'span' => array(
				'class'  => array(),
			),
			'a'    => array(
				'href'  => array(),
				'title' => array(),
				'rel'   => array(),
			),
			'time' => array(
				'datetime' => array(),
				'itemprop' => array(),
				'class'    => array(),
			),
		) );
	}
}

if ( ! function_exists( 'neal_carousel_posts' ) ) {
	/**
	 * Display Carousel posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_carousel_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_carousel_posts_items', true );

		$defaults = array(
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 10,
			'car_columns'	=> 1,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 0,
			'read_more'		=> '',
			'width'			=> 'center-width',
			'full_height'	=> 0,
			'layouts'		=> 1,
			'content_position' => 'in-media',
			'nav'			=> 0,
			'dots'			=> 1,
			'loop'			=> 0,
			'autoplay'		=> 0,
			'animate'		=> 0,
			'animate_in'	=> '',
			'animate_out'	=> '',
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		$classes = '';
		$c_image = '';

		if ( $layouts != 3 ) {	
			// do not add class on layout 2
			if ( $layouts == 2 && $content_position != 'above' || $layouts == 1 ) {
				$classes .= $content_position;
			}
		}

		$classes .= ' layout-' . $layouts;

		if ( $width == 'full' || $width == 'boxed' ) {
			$classes .= ' full';
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);

	?>
		<div class="post-carousel" data-carousel-columns="<?php echo esc_attr( $car_columns ); ?>" data-carousel-nav="<?php echo esc_attr( $nav ); ?>" data-carousel-dots="<?php echo esc_attr( $dots ); ?>" data-carousel-loop="<?php echo esc_attr( $loop ); ?>" data-carousel-autoplay="<?php echo esc_attr( $autoplay ); ?>" data-carousel-animate-in="<?php echo esc_attr( $animate_in ); ?>" data-carousel-animate-out="<?php echo esc_attr( $animate_out ); ?>">

			<?php echo esc_html( $width ) != 'full' ? '<div class="container">' : ''; ?>
					<div class="post-carousel-inner owl-carousel <?php echo esc_attr( $classes ); ?>">

		<?php

			// post layouts
			$col_left = '';
			$col_right = '';
			if ( $layouts == 3 ) {
				$col_left = ' col-lg-6 col-md-6 col-sm-6 col-xs-12';
				$col_right = ' col-lg-6 col-md-6 col-sm-6 col-xs-12';
			}
		
			if ( $wp_query->have_posts() ) : 
				while ( $wp_query->have_posts() ) : $wp_query->the_post();

				// ful height
				if ( $full_height ) {
					$c_image = sprintf( 'style="background-image: url(%s)"', esc_url( get_the_post_thumbnail_url() ) );
				}

				$is_post_formats = ( get_post_format() == 'link' || get_post_format() == 'quote' );
		?>
					<div class="carousel-item"<?php printf( $c_image ); ?>>
						<?php echo esc_html( $layouts ) == 3 ? '<div class="row">' : ''; ?>
							
						<?php if ( $content_position == 'above' && ! $is_post_formats ) : ?>
							<div class="carousel-item-content <?php echo esc_attr( $col_left ); ?>">
								<div class="item-content">
									<?php
										// meta post categories
										if ( $meta_category ) {
											neal_post_categories();
										}

										// meta date
										if ( $meta_date ) {
											neal_post_date();
										}

										// meta author
										if ( $meta_author ) {
											neal_post_author();
										}
									?>
									<?php
										the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
									?>
									<p><?php neal_custom_post_content( 'p_excerpt', intval( $post_excerpt ) ); ?></p>
									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>

						<?php if ( ! $full_height ) : ?>
							<div class="post-image<?php echo esc_attr( $col_left ); ?>">
								<?php 
									neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-carousel-post', 'carousel_posts' ); ?>
							</div>
						<?php endif; ?>

						<?php if ( $content_position == 'below' || $content_position == 'in-media' && ! $is_post_formats ) : ?>
							<div class="carousel-item-content <?php echo esc_attr( $col_left ); ?>">
								<div class="item-content">
									<?php
										// meta post categories
										if ( $meta_category ) {
											neal_post_categories();
										}

										// meta date
										if ( $meta_date ) {
											neal_post_date();
										}

										// meta author
										if ( $meta_author ) {
											neal_post_author();
										}
									?>
									<?php
										the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
									?>

									<?php if ( $post_excerpt != 0 ) : ?>
										<p><?php neal_custom_post_content( 'p_excerpt', intval( $post_excerpt ) ); ?></p>
									<?php endif; ?>

									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>

						<?php echo esc_html( $layouts ) == 3 ? '</div>' : ''; ?>
						<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="carousel-link"></a>
					</div>
		<?php
				endwhile;
				wp_reset_postdata();
			endif;
		?>

		</div>
		<?php echo esc_html( $width ) != 'full' ? '</div>' : ''; ?>
	</div>

<?php
	}
}

if ( ! function_exists( 'neal_full_featured_post' ) ) {
	/**
	 * Display Full featured post
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_full_featured_post() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_full_featured_post_items', true );
		$defaults = array(
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 20,
			'read_more'		=> 'yes',
			'parallax'		=> 1,
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		$args = array(
			'post_type'			=> 'post',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> 1,
			'post__not_in' 		=> get_option( 'sticky_posts' )
		);

		// featured posts
			$args['meta_key'] = 'neal_post_options_featured';
			$args['meta_value'] = 'yes';

		$wp_query = new WP_Query( $args );

		if ( $wp_query->have_posts() ) :
			$wp_query->have_posts(); 
			$wp_query->the_post(); 

	?>
		<div class="featured-post" style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url() ); ?>);" data-parallax="<?php echo esc_attr( $parallax ); ?>">
			<div class="featured-post-content">
				<?php if ( get_post_format() != 'link' ) : ?>
					<div class="post-content">
						<?php if ( get_post_format() == 'video' ) : ?>
							<span class="post-icon post-icon-video"><i class="ion-play"></i></span>
						<?php endif; ?>

						<h1><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h1>
						<?php
							// meta post categories
							if ( $meta_category ) {
								neal_post_categories();
							}

							// meta date
							if ( $meta_date ) {
								neal_post_date();
							}

							// meta author
							if ( $meta_author ) {
								neal_post_author();
							}
						?>

						<p><?php neal_custom_post_content( 'p_excerpt', intval( $post_excerpt ) ); ?></p>
						
						<?php if ( $read_more ) : ?>
							<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
						<?php endif; ?>
					</div>
				<?php else:
					// quote & link format
					if ( get_post_format() == 'link' ) {
						$link_description 	= get_post_meta( get_the_ID(), 'neal_link_description', true );
						$link_title 		= get_post_meta( get_the_ID(), 'neal_link_title', true );
						$link_url 			= get_post_meta( get_the_ID(), 'neal_link_url', true );

				?>
					<div class="entry-link">
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="overlay-inner">
								<div class="overlay-content">
									<?php if ( esc_html( $link_description ) ) { ?>
										<p><?php echo esc_html( $link_description ); ?></p>
									<?php  } ?>
									<small><a href="<?php echo esc_url( $link_url ); ?>" target="_blank"><?php echo esc_html( $link_title ); ?></a></small>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php } else {
							$quote_content 	= get_post_meta( get_the_ID(), 'neal_quote_content', true );
							$quote_title 	= get_post_meta( get_the_ID(), 'neal_quote_title', true );
				?>
					<div class="entry-quote">
						<div class="overlay-wrap">
							<div class="overlay">
								<div class="overlay-inner">
									<div class="overlay-content">
										<h4><?php echo esc_html( $quote_content ); ?></h4>
										<small><?php echo esc_html( $quote_title ); ?></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php 
					}
				endif; ?>
				<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
			</div>
		<?php
			wp_reset_postdata();
			endif; 
		?>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_masonry_posts' ) ) {
	/**
	 * Display Masonry post
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_masonry_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_masonry_posts_items', true );

		if ( ! $items ) {
			return;
		}

		extract( $items );

		$post_layout = $layouts ? $layouts : 1;
		get_template_part( 'template-parts/masonry-posts/layout', $post_layout );
	}
}

if ( ! function_exists( 'neal_2_grid_posts' ) ) {
	/**
	 * Display 2 grid posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_2_grid_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_2_grid_posts_items', true );

		if ( ! $items ) {
			return;
		}

		$defaults = array(
			'title'			=> '',
			'title_fl'		=> 'yes',
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 2,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 20,
			'read_more'		=> 'yes',
			'load_more'		=> '',
			'hover_like'	=> 'yes',
			'display_adv'	=> '',
			'adv_img'		=> '',
			'adv_url'		=> '',
			'adv_pos'		=> 'bottom',
			'adv_js_code'	=> '',
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		// title first letter
		$title_fl 			= $title_fl ? substr( esc_html( $title ), 0, 1 ) : false;
		$first_lett_title   = '';
		if ( $title_fl ) {
			$first_lett_title = sprintf( ' data-title="%s"', $title_fl );
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);

	?>
		<div class="container">
			<div class="row">
			<?php
				if ( $wp_query->have_posts() ) :
					neal_get_homepage_sidebar(); 
			?>
					<div class="posts col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<?php if ( $display_adv && $adv_pos == 'top' ) : ?>
							<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
								<?php if ( $adv_url ) : ?>
									<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
										<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
									</a>
								<?php else:; ?>
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								<?php endif; ?>
							</div>
						<?php endif; ?>

						<?php if ( $title ) : ?>
							<h2 class="section-title"><span class="title"<?php printf( $first_lett_title ); ?>><?php echo esc_html( $title ); ?></span></h2>
						<?php  endif; ?>
						<div class="row">
					<?php
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
					?>

							<article <?php post_class('col-lg-6 col-md-6 col-sm-6 col-xs-12'); ?>>
								<div class="article-inner">

									<?php if ( has_post_thumbnail() ) :  ?>
										<!-- Post Media -->
										<div class="entry-media">
											<?php 
											neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-full-posts-image', '2_grid_posts' ); ?>
										</div>
										<!-- Post Media End -->
									<?php endif; ?>

									<!-- Post Header -->
									<div class="entry-header">
										<?php

											// category
											if ( $meta_category ) {
												neal_post_categories();
											}

											// date
											if ( $meta_date ) {
												neal_post_date();
											}

											// author
											if ( $meta_author ) {
												neal_post_author();
											}
											
											neal_post_title();
										?>
									</div>
									<!-- Post Header End -->
									<?php if ( $post_excerpt != 0 ) : ?>
										<div class="entry-content"><?php neal_custom_post_content( 'p_excerpt', esc_html( $post_excerpt ) ); ?></div>
									<?php endif; ?>

									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span>
										</a>
									<?php endif; ?>

									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="article-link"></a>
								</div>
							</article>

					<?php 
						endwhile;
						wp_reset_postdata();
					?>
					</div>

					<?php 
						// load more with ajax
						neal_homepage_load_more( '2_grid_posts', array(
							'load_more'		 => $load_more,
							'posts_per_page' => $amount,
							'max_num_pages'	 => $wp_query->max_num_pages,
							'sort_order'	 => $sort_order,
							'sort_by'		 => $sort_by,
							'category'		 => $category,
							'tag'			 => $tag,
							'post_ids'		 => $post_ids,
							'meta_category'	 => $meta_category,
							'meta_date'		 => $meta_date,
							'meta_author'	 => $meta_author,
							'post_excerpt'	 => $post_excerpt,
							'read_more'		 => $read_more
						) );
					?>

						<?php if ( $display_adv && $adv_pos == 'bottom' ) : ?>
							<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
								<?php if ( $adv_url ) : ?>
									<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
										<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
									</a>
								<?php else:; ?>
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					</div>

					<?php
						endif; ?>
						</div>
					</div>
	<?php
	}
}

if ( ! function_exists( 'neal_full_posts' ) ) {
	/**
	 * Display Full posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_full_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_full_posts_items', true );

		if ( ! $items ) {
			return;
		}

		$defaults = array(
			'title'			=> '',
			'title_fl'		=> 'yes',
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 3,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 20,
			'read_more'		=> 'yes',
			'load_more'		=> '',
			'hover_like'	=> 'yes',
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		// title first letter
		$title_fl 			= $title_fl ? substr( esc_html( $title ), 0, 1 ) : false;
		$first_lett_title   = '';
		if ( $title_fl ) {
			$first_lett_title = sprintf( ' data-title="%s"', $title_fl );
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);
	?>
		<div class="full-posts">
			<div class="container">
				<?php if ( $title ) : ?>
					<h2 class="section-title"><span class="title"<?php printf( $first_lett_title ); ?>><?php echo esc_html( $title ); ?></span></h2>
				<?php endif; ?>
				
				<?php
					$post_not_in = array();
					if ( $wp_query->have_posts() ) :
				?>

					<div class="posts">
						<div class="row">
					<?php
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
							$post_not_in[] = get_the_ID();
					?>

							<article <?php post_class('col-lg-4 col-md-4 col-sm-4 col-xs-12'); ?>>
								<div class="article-inner">

									<?php if ( has_post_thumbnail() ) :  ?>
										<!-- Post Media -->
										<div class="entry-media">
											<?php 
											neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-full-posts-image', 'full_posts' ); ?>
										</div>
										<!-- Post Media End -->
									<?php endif; ?>

									<!-- Post Header -->
									<div class="entry-header">
										<?php

											// category
											if ( $meta_category ) {
												neal_post_categories();
											}

											// date
											if ( $meta_date ) {
												neal_post_date();
											}

											// author
											if ( $meta_author ) {
												neal_post_author();
											}
											
											neal_post_title();
										?>
									</div>
									<!-- Post Header End -->
									<?php if ( $post_excerpt != 0 ) : ?>
										<div class="entry-content"><?php neal_custom_post_content( 'p_excerpt', esc_html( $post_excerpt ) ); ?></div>
									<?php endif; ?>

									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span>
										</a>
									<?php endif; ?>

									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="article-link"></a>
								</div>
							</article>

					<?php 
						endwhile;
					?>
					</div>

					<?php
						// load more with ajax
						neal_homepage_load_more( 'full_posts', array(
							'load_more'		 => $load_more,
							'posts_per_page' => $amount,
							'max_num_pages'	 => $wp_query->max_num_pages,
							'sort_order'	 => $sort_order,
							'sort_by'		 => $sort_by,
							'category'		 => $category,
							'tag'			 => $tag,
							'post_ids'		 => $post_ids,
							'post_not_in'	 => $post_not_in,
							'meta_category'	 => $meta_category,
							'meta_date'		 => $meta_date,
							'meta_author'	 => $meta_author,
							'post_excerpt'	 => $post_excerpt,
							'read_more'		 => $read_more
						) );

						wp_reset_postdata();

					?>
					</div>
					<?php
						endif; ?>
					</div>
				</div>
	<?php
	}
}

if ( ! function_exists( 'neal_list_posts' ) ) {
	/**
	 * Display List posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_list_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_list_posts_items', true );

		if ( ! $items ) {
			return;
		}

		$defaults = array(
			'title'			=> '',
			'title_fl'		=> 'yes',
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 3,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 20,
			'read_more'		=> 'yes',
			'load_more'		=> '',
			'hover_like'	=> 'yes',
			'display_adv'	=> '',
			'adv_img'		=> '',
			'adv_url'		=> '',
			'adv_pos'		=> 'bottom',
			'adv_js_code'	=> '',
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		// title first letter
		$title_fl 			= $title_fl ? substr( esc_html( $title ), 0, 1 ) : false;
		$first_lett_title   = '';
		if ( $title_fl ) {
			$first_lett_title = sprintf( ' data-title="%s"', $title_fl );
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);
	?>
		<div class="container">
			<div class="row">
				<?php neal_get_homepage_sidebar(); ?>
				<div class="list-posts col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<?php if ( $display_adv && $adv_pos == 'yop' ) : ?>
						<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
							<?php if ( $adv_url ) : ?>
								<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								</a>
							<?php else:; ?>
								<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
							<?php endif; ?>
						</div>
					<?php endif; ?>

					<?php if ( $title ) : ?>
						<h2 class="section-title"><span class="title"<?php print( $first_lett_title ); ?>><?php echo esc_html( $title ); ?></span></h2>
					<?php endif; ?>
					<?php
						if ( $wp_query->have_posts() ) :
					?>

					<div class="posts">
						<div class="row">
					<?php
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
					?>

							<article <?php post_class('col-lg-12 col-md-12 col-sm-12 col-xs-12'); ?>>
								<div class="article-inner">
								<div class="row">
								<div class="post-left col-lg-6 col-md-6 col-sm-6 col-xs-12">

									<?php if ( has_post_thumbnail() ) :  ?>
										<!-- Post Media -->
										<div class="entry-media">
											<?php 
											neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-posts-list-image', 'list_posts' ); ?>
										</div>
										<!-- Post Media End -->
									<?php endif; ?>
								</div>

								<div class="post-right col-lg-6 col-md-6 col-sm-6 col-xs-12">

									<!-- Post Header -->
									<div class="entry-header">
										<?php

											// category
											if ( $meta_category ) {
												neal_post_categories();
											}
											
											// date
											if ( $meta_date ) {
												neal_post_date();
											}
											
											// author
											if ( $meta_author ) {
												neal_post_author();
															
											}
	
											neal_post_title();
										?>
									</div>
									<!-- Post Header End -->
									<?php if ( $post_excerpt != 0 ) : ?>
										<div class="entry-content"><?php neal_custom_post_content( 'p_excerpt', esc_html( $post_excerpt ) ); ?></div>
									<?php endif; ?>

									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span>
										</a>
									<?php endif; ?>

								</div>

								</div>

									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="article-link"></a>
								</div>
							</article>

					<?php 
						endwhile;
					?>
						</div>

					<?php
						// load more with ajax
						neal_homepage_load_more( 'list_posts', array(
							'load_more'		 => $load_more,
							'posts_per_page' => $amount,
							'max_num_pages'	 => $wp_query->max_num_pages,
							'sort_order'	 => $sort_order,
							'sort_by'		 => $sort_by,
							'category'		 => $category,
							'tag'			 => $tag,
							'post_ids'		 => $post_ids,
							'meta_category'	 => $meta_category,
							'meta_date'		 => $meta_date,
							'meta_author'	 => $meta_author,
							'post_excerpt'	 => $post_excerpt,
							'read_more'		 => $read_more
						) );

						wp_reset_postdata();
					?>
					</div>
					<?php
						endif; ?>
					
					<?php if ( $display_adv && $adv_pos == 'bottom' ) : ?>
						<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
							<?php if ( $adv_url ) : ?>
								<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								</a>
							<?php else:; ?>
								<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>

			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_slider_posts' ) ) {
	/**
	 * Display Slider posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_slider_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_slider_posts_items', true );

		if ( ! $items ) {
			return;
		}

		$defaults = array(
			'title'			=> '',
			'title_fl'		=> '',
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 8,
			'car_columns'	=> 4,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 0,
			'read_more'		=> '',
			'nav'			=> 0,
			'dots'			=> 1,
			'loop'			=> 1,
			'autoplay'		=> 0,
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		// title first letter
		$title_fl 			= $title_fl ? substr( esc_html( $title ), 0, 1 ) : false;
		$first_lett_title   = '';
		if ( $title_fl ) {
			$first_lett_title = sprintf( ' data-title="%s"', $title_fl );
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);
	?>
		<div class="slider-posts" data-carousel-columns="<?php echo esc_attr( $car_columns ); ?>" data-carousel-nav="<?php echo esc_attr( $nav ); ?>" data-carousel-dots="<?php echo esc_attr( $dots ); ?>" data-carousel-loop="<?php echo esc_attr( $loop ); ?>" data-carousel-autoplay="<?php echo esc_attr( $autoplay ); ?>">
			<?php if ( $title ) : ?>
				<h2 class="section-title"><span class="title"<?php printf( $first_lett_title ); ?>><?php echo esc_html( $title ); ?></span></h2>
			<?php endif; ?>
			<div class="posts owl-carousel">
			<?php
				if ( $wp_query->have_posts() ) :
			?>
				<?php
					while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
				?>

							<div class="post-item">

									<?php if ( has_post_thumbnail() ) :  ?>
										<!-- Post Media -->
										<div class="entry-media">
											<?php 
											neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-slider-posts-image', 'slider_posts' ); ?>
										</div>
										<!-- Post Media End -->
									<?php endif; ?>

									<?php 
										$is_post_formats = ( get_post_format() == 'link' || get_post_format() == 'quote' );
										if ( ! $is_post_formats ) : ?>
										<!-- Post Header -->
										<div class="entry-header">
											<?php

												// category
												if ( $meta_category ) {
													neal_post_categories();
												}

												// date
												if ( $meta_date ) {
													neal_post_date();
												}

												// author
												if ( $meta_author ) {
													neal_post_author();
												}

												neal_post_title();
											?>
											<?php if ( $read_more ) : ?>
												<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
											<?php endif; ?>
										</div>
										<!-- Post Header End -->
									<?Php endif; ?>
									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
							</div>

					<?php 
						endwhile;
					?>

					<?php
						wp_reset_postdata();
						endif; ?>
					</div>
				</div>
	<?php
	}
}

if ( ! function_exists( 'neal_3_grid_posts' ) ) {
	/**
	 * Display 3 grid posts
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_3_grid_posts() {
		global $post;

		// get post meta data
		$items = get_post_meta( $post->ID, 'neal_3_grid_posts_items', true );

		if ( ! $items ) {
			return;
		}

		$defaults = array(
			'title'			=> '',
			'title_fl'		=> 'yes',
			'sort_by'		=> 'recent',
			'category'		=> '',
			'tag'			=> '',
			'amount'		=> 3,
			'sort_order'	=> 'desc',
			'post_ids'		=> '',
			'meta_category'	=> 'yes',
			'meta_date'		=> 'yes',
			'meta_author'	=> '',
			'post_excerpt'	=> 20,
			'read_more'		=> 'yes',
			'load_more'		=> '',
			'hover_like'	=> 'yes',
			'display_adv'	=> '',
			'adv_img'		=> '',
			'adv_url'		=> '',
			'adv_pos'		=> 'bottom',
			'adv_js_code'	=> '',
		);

		$items = wp_parse_args( $items, $defaults );
		extract( $items );

		// title first letter
		$title_fl 			= $title_fl ? substr( esc_html( $title ), 0, 1 ) : false;
		$first_lett_title   = '';
		if ( $title_fl ) {
			$first_lett_title = sprintf( ' data-title="%s"', $title_fl );
		}

		$wp_query = neal_homepage_post_query(
			$amount,
			$sort_order,
			$sort_by,
			$category,
			$tag,
			$post_ids
		);
	?>
			<div class="container">
				<div class="row">
					<?php
						if ( $wp_query->have_posts() ) :
					?>

					<?php neal_get_homepage_sidebar(); ?>

					<div class="posts col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<?php if ( $display_adv && $adv_pos == 'top' ) : ?>
							<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
								<?php if ( $adv_url ) : ?>
									<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
										<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
									</a>
								<?php else:; ?>
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								<?php endif; ?>
							</div>
						<?php endif; ?>

						<?php if ( $title ) : ?>
							<h2 class="section-title"><span class="title"<?php printf( $first_lett_title ); ?>><?php echo esc_html( $title ); ?></span></h2>
						<?php endif; ?>
						<div class="row">
					<?php
						while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
					?>

							<article <?php post_class('col-lg-4 col-md-4 col-sm-4 col-xs-12'); ?>>
								<div class="article-inner">
									<?php if ( has_post_thumbnail() ) :  ?>
										<!-- Post Media -->
										<div class="entry-media">
											<?php 
											neal_get_template_part( 'post-formats/content', get_post_format(), 'neal-full-posts-image', '3_grid_posts' ); ?>
										</div>
										<!-- Post Media End -->
									<?php endif; ?>

									<!-- Post Header -->
									<div class="entry-header">
										<?php

											// category
											if ( $meta_category ) {
												neal_post_categories();
											}

											// date
											if ( $meta_date ) {
												neal_post_date();
											}

											// author
											if ( $meta_author ) {
												neal_post_author();
											}
											
											neal_post_title();
										?>
									</div>
									<!-- Post Header End -->
									<?php if ( $post_excerpt != 0 ) : ?>
										<div class="entry-content"><?php neal_custom_post_content( 'p_excerpt', esc_html( $post_excerpt ) ); ?></div>
									<?php endif; ?>

									<?php if ( $read_more ) : ?>
										<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span>
										</a>
									<?php endif; ?>

									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="article-link"></a>
								</div>
							</article>

					<?php 
						endwhile;
					?>
					</div>

					<?php
						// load more with ajax
						neal_homepage_load_more( '3_grid_posts', array(
							'load_more'		 => $load_more,
							'posts_per_page' => $amount,
							'max_num_pages'	 => $wp_query->max_num_pages,
							'sort_order'	 => $sort_order,
							'sort_by'		 => $sort_by,
							'category'		 => $category,
							'tag'			 => $tag,
							'post_ids'		 => $post_ids,
							'meta_category'	 => $meta_category,
							'meta_date'		 => $meta_date,
							'meta_author'	 => $meta_author,
							'post_excerpt'	 => $post_excerpt,
							'read_more'		 => $read_more
						) );

						wp_reset_postdata();
					?>
						
						<?php if ( $display_adv && $adv_pos == 'bottom' ) : ?>
							<div class="post-inner-adv <?php echo esc_attr( $adv_pos ); ?>">
								<?php if ( $adv_url ) : ?>
									<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
										<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
									</a>
								<?php else:; ?>
									<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>

					<?php
						endif; ?>
						</div>
					</div>
	<?php
	}
}

if ( ! function_exists( 'neal_newsletter' ) ) {
	/**
	 * Display Newsletter
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_newsletter() {
		global $post;
		
		if ( ! shortcode_exists( 'jetpack_subscription_form' ) ) {
			return;
		}

		// get post meta data
		$title 		 = get_post_meta( $post->ID, 'neal_newsletter_title', true );
		$desc 		 = get_post_meta( $post->ID, 'neal_newsletter_desc', true );
		$placeholder = get_post_meta( $post->ID, 'neal_newsletter_placeholder', true );
		$button 	 = get_post_meta( $post->ID, 'neal_newsletter_button', true );
	?>
		<div class="homepage-newsletter">
			<div class="container">
				<div class="letter-form">
					<?php echo do_shortcode( '[jetpack_subscription_form title="'.esc_attr( $title ).'" subscribe_text="'.esc_attr( $desc ).'" subscribe_placeholder="'.esc_attr( $placeholder ).'" subscribe_button="'.esc_attr( $button ).'" ]' ); ?>
				</div>
			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_adv_block_1' ) ) {
	/**
	 * Display Adv Block
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_adv_block_1() {
		global $post;

		// get post meta data
		$adv_img	= get_post_meta( $post->ID, 'neal_adv_block_1_img', true );
		$adv_url	= get_post_meta( $post->ID, 'neal_adv_block_1_url', true );
		$js_code	= get_post_meta( $post->ID, 'neal_adv_block_1_js_code', true );
	?>
		<div class="container">
			<div class="full-adv col-xs-10 centered">
				<?php if ( $adv_url ) : ?>
					<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
						<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
					</a>
				<?php else: ?>
					<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>">
				<?php endif; ?>
			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_adv_block_2' ) ) {
	/**
	 * Display Adv Block 2
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_adv_block_2() {
		global $post;

		// get post meta data
		$adv_img	= get_post_meta( $post->ID, 'neal_adv_block_2_img', true );
		$adv_url	= get_post_meta( $post->ID, 'neal_adv_block_2_url', true );
		$js_code	= get_post_meta( $post->ID, 'neal_adv_block_2_js_code', true );
	?>
		<div class="container">
			<div class="full-adv col-xs-10 centered">
				<?php if ( $adv_url ) : ?>
					<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
						<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
					</a>
				<?php else: ?>
					<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>">
				<?php endif; ?>
			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_adv_block_3' ) ) {
	/**
	 * Display Adv Block 3
	 * Hooked into the `neal-homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function neal_adv_block_3() {
		global $post;

		// get post meta data
		$adv_img	= get_post_meta( $post->ID, 'neal_adv_block_3_img', true );
		$adv_url	= get_post_meta( $post->ID, 'neal_adv_block_3_url', true );
		$js_code	= get_post_meta( $post->ID, 'neal_adv_block_3_js_code', true );
	?>
		<div class="container">
			<div class="full-adv col-xs-10 centered">
				<?php if ( $adv_url ) : ?>
					<a href="<?php echo esc_url( $adv_url ); ?>" target="_blank">
						<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>" alt>
					</a>
				<?php else: ?>
					<img src="<?php echo wp_get_attachment_url( $adv_img ); ?>">
				<?php endif; ?>
			</div>
		</div>
	<?php
	}
}

if ( ! function_exists( 'neal_get_homepage_sidebar' ) ) {
	/**
	 * Display Sidebars on homepage
	 *
	 * @since 1.0.0
	 */
	function neal_get_homepage_sidebar() {
		static $sidebar_pos = 0;
		$sidebar_pos++;

		$is_sticky = neal_get_option( 'homepage_sidebar-' . $sidebar_pos . '-sticky' );
	?>
		<aside class="widget-area col-lg-3 col-md-3 col-sm-12 col-xs-12 sidebar-area-<?php echo esc_attr( $sidebar_pos ); ?>" role="complementary" data-sticky="<?php echo esc_attr( $is_sticky ); ?>">
			<?php dynamic_sidebar( 'homepage-widget-' . $sidebar_pos ); ?>
		</aside><!-- #secondary -->
	<?php
	}
}

if ( ! function_exists( 'neal_get_sidebar' ) ) {
	/**
	 * Display neal sidebar
	 *
	 * @uses get_sidebar()
	 * @since 1.0.0
	 */
	function neal_get_sidebar() {
		global $post;
		
		// get post meta data
		$items = neal_get_post_options_data();
		
		if ( neal_get_option('blog_sidebar-label') && ! is_single() || is_front_page() ) {
			get_sidebar();
		} elseif ( $items['sidebar'] && is_single() ) {
			get_sidebar();
		}
	}
}

if ( ! function_exists( 'neal_post_thumbnail' ) ) {
	/**
	 * Display post thumbnail
	 *
	 * @var $size thumbnail size. thumbnail|medium|large|full|$custom
	 * @uses has_post_thumbnail()
	 * @uses the_post_thumbnail
	 * @param string $size the post thumbnail size.
	 * @since 1.0.0
	 */
	function neal_post_thumbnail( $size = '' ) {
		if ( has_post_thumbnail() ) {
			$size = $size ? $size : '';
			the_post_thumbnail( $size, array( 'itemprop' => 'image' ) );
		}
	}
}

if ( ! function_exists( 'neal_author_box' ) ) {

	/**
	 * Display Author Box
	 *
	 * @since 1.0.0
	 */
	function neal_author_box() {

		// get data theme customizer
		$blog_single = get_option( 'neal_blog_single' );

		if ( ! $blog_single['author-box-label'] ) {
			return false;
		}

		// Get Author Data
		$author_id 			= get_the_author_meta( 'ID' );
		$author             = get_the_author();
		$author_description = get_the_author_meta( 'description' );
		$author_url         = get_author_posts_url( $author_id );
		$author_email		= get_the_author_meta( 'user_email' );
		$avatar_size 		= intval( $blog_single['author-box-img-size'] ); // get avatar size from theme customizer

		$ab_classes = 'col-sm-6 col-xs-12';
		// blog single layouts
		if ( $blog_single['general-post-layout'] == 2 || $blog_single['general-post-layout'] == 3 ) {
			$ab_classes = 'col-sm-8 col-xs-12';
		}

		// Only display if author has a description
		if ( $author_description ) : ?>

		    <div class="author-box">
		    	<div class="row">
			        <div class="author-info-inner <?php echo esc_attr( $ab_classes ); ?> centered">
			        	<div class="author-avatar">
			            	<a href="<?php echo esc_url( $author_url ); ?>" rel="author">
			                	<?php echo get_avatar( esc_html( $author_email ), $avatar_size ); ?>
			                </a>
			            </div><!-- .author-avatar -->
			            <div class="author-description">
			            	<h4 class="heading">
			            		<a href="<?php echo esc_url( $author_url ); ?>" rel="author">		<?php printf( esc_html__( '%s', 'neal' ), esc_html( $author ) ); ?>
			            		</a>
			            		</h4>
			                <p><?php echo wp_kses_post( $author_description ); ?></p>
			                <div class="author-socials">
			                	<?php
			                		// socials
			                		if ( get_the_author_meta( 'facebook', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'facebook', $author_id )).'" target="_blank"><i class="fa fa-facebook"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'twitter', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'twitter', $author_id )).'" target="_blank"><i class="fa fa-twitter"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'instagram', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'instagram', $author_id )).'" target="_blank"><i class="fa fa-instagram"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'googlep', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'googlep', $author_id )).'" target="_blank"><i class="fa fa-google-plus"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'youtube', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'youtube', $author_id )).'" target="_blank"><i class="fa fa-youtube"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'pinterest', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'pinterest', $author_id )).'" target="_blank"><i class="fa fa-pinterest"></i></a>';
			                		} 

			                		if ( get_the_author_meta( 'linkedin', $author_id ) ) {
			                			echo '<a href="'.esc_attr(get_the_author_meta( 'linkedin', $author_id )).'" target="_blank"><i class="fa fa-linkedin"></i></a>';
			                		}

			                	?>
			                </div>
			            </div><!-- .author-description -->
			        </div><!-- .author-info-inner -->
			    </div>
		    </div><!-- .author-info -->

	<?php 
		endif;
	}
}

if ( ! function_exists( 'neal_related_posts' ) ) {

	/**
	 * Display Related posts
	 *
	 * @since 1.0.0
	 */
	function neal_related_posts() {
		if ( ! neal_get_option( 'blog_single_related-posts-label' ) ) {
			return;
		}
		
		get_template_part( 'template-parts/related-posts' );
	}
}