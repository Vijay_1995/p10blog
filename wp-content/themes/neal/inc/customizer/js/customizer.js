jQuery(document).ready(function( $ ) {
    "use strict";


/*
***************************************************************
* #Body
***************************************************************
*/	

	/* ----------------- General Options ----------------- */

		// main layout width
		nealSetCustomize( 'body', 'layout-width', function ( to ) {
			$( '.container' ).css( 'max-width', to + 'px' );
		});

		// main layout align
		nealSetCustomize( 'body', 'layout-align', function ( to ) {
			if ( to !== 'center' ) {
				$( '.container' ).css( 'float', to );
			} else {
				$( '.container' ).css({
										'float' : 'none',
										'margin' : 'auto'
									});
			}
		});

	/* ----------------- Styling Options ----------------- */

		// default values
		var bodyLinkColor 		= neal_options.body['general-styl-link-hover-color'],
			bodyBgImg 			= neal_options.body['general-styl-child-bg-img'],
			bodyBgImgSize 		= neal_options.body['general-styl-child-bg-img-size'],
			bodyBgImgAttchmnt 	= neal_options.body['general-styl-child-bg-img-attchmnt'];

		// background color
		nealSetCustomize( 'body', 'general-styl-bg-color', function ( to ) {
			$('body').css( 'background-color', to );
		});

		// font color
		nealSetCustomize( 'body', 'general-styl-font-color', function ( to ) {
			$('body').css( 'color', to );
		});

		// link color
		nealSetCustomize( 'body', 'general-styl-link-color', function ( to ) {
			$('a').css( 'color', to );
			bodyLinkColor = to;
		});

		// link hover color
		nealSetCustomize( 'body', 'general-styl-link-hover-color', function ( to ) {
			$('a').on( 'hover', function () {
				$( this ).css( 'color', to );
			}, function () {
				$( this ).css( 'color', bodyLinkColor );
			});
		});

		// background image label
		nealSetCustomize( 'body', 'general-styl-child-bg-img-label', function ( to ) {
			if ( to ) {
				$('body').css({
						    	'background-image': 'url(' + bodyBgImg + ')',
								'background-size': bodyBgImgSize,
								'background-attachment': bodyBgImgAttchmnt,
				});
			} else {
				$('body').css({
								'background-image': '',
								'background-size': '',
								'background-attachment': '',
								});
			}
		});

		// background image
		nealSetCustomize( 'body', 'general-styl-child-bg-img', function ( to ) {
			$('body').css({
				'background-image': 'url(' + to + ')'
			});
			bodyBgImg = to;
		});

		// background size
		nealSetCustomize( 'body', 'general-styl-child-bg-img-size', function ( to ) {
			$('body').css( 'background-size', to );
			bodyBgImgSize = to;
		});

		// background attachment
		nealSetCustomize( 'body', 'general-styl-child-bg-img-attchmnt', function ( to ) {
			$('body').css( 'background-attachment', to );
			bodyBgImgAttchmnt = to;
		});

	/* ----------------- Font Options ----------------- */

		// font size
		nealSetCustomize( 'body', 'general-font-size', function ( to ) {
			$('body').css( 'font-size', to + 'px' );
		});

		// font weight
		nealSetCustomize( 'body', 'general-font-weight', function ( to ) {
			$('body').css( 'font-weight', to );
		});

		// letter-spacing
		nealSetCustomize( 'body', 'general-font-letter-spacing', function ( to ) {
			$('body').css( 'letter-spacing', to + 'px' );
		});

/*
***************************************************************
* #Header
***************************************************************
*/

	/* ----------------- Spacing Options ----------------- */

		// page-header padding top
		nealSetCustomize( 'header', 'page-header-spac-padding-top', function ( to ) {
			$('.page-header').css( 'padding-top', to + 'px' );
		});

		// page-header padding bottom
		nealSetCustomize( 'header', 'page-header-spac-padding-bottom', function ( to ) {
			$('.page-header').css( 'padding-bottom', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		// default values
		var headerBgImg 			= neal_options.header['general-header-styl-child-bg-img'],
			headerBgImgSize 		= neal_options.header['general-header-styl-child-bg-img-size'],
			headerBgImgAttchmnt 	= neal_options.header['general-header-styl-child-bg-img-attchmnt'],
			pageHeaderLinkColor;


		// general header background-color
		nealSetCustomize( 'header', 'general-header-styl-bg-color', function ( to ) {
			$('.site-header .header-area').css( 'background-color', to );
		});

		// general header background image label
		nealSetCustomize( 'header', 'general-header-styl-child-bg-img-label', function ( to ) {
			if ( to ) {
				$('.site-header .header-area').css({
					'background-image': 'url(' + headerBgImg + ')',
					'background-size': headerBgImgSize,
					'background-attachment': headerBgImgAttchmnt,
				});
			} else {
				$('.site-header .header-area').css({
					'background-image': '',
					'background-size': '',
					'background-attachment': '',
				});
			}
		});

		// general header background image
		nealSetCustomize( 'header', 'general-header-styl-child-bg-img', function ( to ) {
			$('.site-header .header-area').css({
				'background-image': 'url(' + to + ')'
			});
			headerBgImg = to;
		});

		// general header background size
		nealSetCustomize( 'header', 'general-header-styl-child-bg-img-size', function ( to ) {
			$('.site-header .header-area').css( 'background-size', to );
			headerBgImgSize = to;
		});

		// general header background attachment
		nealSetCustomize( 'header', 'general-header-styl-child-bg-img-attchmnt', function ( to ) {
			$('.site-header .header-area').css( 'background-attachment', to );
			headerBgImgAttchmnt = to;
		});

		// page-header background-color
		nealSetCustomize( 'header', 'page-header-styl-bg-color', function ( to ) {
			$('.page-header').css( 'background-color', to );
		});

		// page-header title color
		nealSetCustomize( 'header', 'page-header-styl-title-color', function ( to ) {
			$('.page-header .page-title').css( 'color', to );
		});

		// page-header font color
		nealSetCustomize( 'header', 'page-header-styl-font-color', function ( to ) {
			$('.page-header').css( 'color', to );
		});

		// page-header link color
		nealSetCustomize( 'header', 'page-header-styl-link-color', function ( to ) {
			$('.page-header a').css( 'color', to );
			pageHeaderLinkColor = to;
		});

		// page-header link hover color
		nealSetCustomize( 'header', 'page-header-styl-link-hover-color', function ( to ) {
			$( '.page-header a' ).on( 'hover', function () {
				$( this ).css( 'color', to );
			}, function () {
				$( this ).css( 'color', pageHeaderLinkColor );
			});
			
		});


/*
***************************************************************
* #Logo & Tagline
***************************************************************
*/

	/* ----------------- General Options ----------------- */

		// tagline
		nealSetCustomize( 'logo', 'tagline-align', function ( to ) {
			$('.site-branding .site-description').css( 'text-align', to );
		});

	/* ----------------- Spacing Options ----------------- */

		// logo img max-height
		nealSetCustomize( 'logo', 'img-spac-max-height', function ( to ) {
			$('.site-branding img').css( 'max-height', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		// logo text font color
		nealSetCustomize( 'logo', 'logo-styl-font-color', function ( to ) {
			$('.site-branding .site-title').css( 'color', to );
		});

		// tagline font color
		nealSetCustomize( 'logo', 'tagline-styl-font-color', function ( to ) {
			$('.site-branding .site-description').css( 'color', to );
		});

	/* ----------------- Font Options ----------------- */

		// logo text font-size
		nealSetCustomize( 'logo', 'logo-text-font-size', function ( to ) {
			$('.site-branding .site-title').css( 'font-size', to + 'px' );
		});

		// logo text font-weight
		nealSetCustomize( 'logo', 'logo-text-font-weight', function ( to ) {
			$('.site-branding .site-title').css( 'font-weight', to );
		});

		// logo text line-height
		nealSetCustomize( 'logo', 'logo-text-font-line-height', function ( to ) {
			$('.site-branding .site-title').css( 'line-height', to );
		});

		// logo text letter-spacing
		nealSetCustomize( 'logo', 'logo-text-font-letter-spacing', function ( to ) {
			$('.site-branding .site-title').css( 'letter-spacing', to + 'px' );
		});

		// logo text uppercase
		nealSetCustomize( 'logo', 'logo-text-font-uppercase', function ( to ) {
			if ( to ) {
				$('.site-branding .site-title').css( 'text-transform', 'uppercase' );
			} else {
				$('.site-branding .site-title').css( 'text-transform', 'uppercase' );
			}
		});

		// tagline font-size
		nealSetCustomize( 'logo', 'tagline-font-size', function ( to ) {
			$('.site-branding .site-description').css( 'font-size', to + 'px' );
		});

		// tagline font-weight
		nealSetCustomize( 'logo', 'tagline-font-weight', function ( to ) {
			$('.site-branding .site-description').css( 'font-weight', to );
		});

		// tagline letter-spacing
		nealSetCustomize( 'logo', 'tagline-font-letter-spacing', function ( to ) {
			$('.site-branding .site-description').css( 'letter-spacing', to + 'px' );
		});

/*
***************************************************************
* #Menus
***************************************************************
*/
	var searchIcon = neal_options.menu['search-icon'];

	/* ----------------- General Options ----------------- */

		// menu items align
		nealSetCustomize( 'menu', 'menu-items-align', function ( to ) {
			if ( to == 'left' ) {
				$( '.main-navigation' ).css({
					'margin-right'	: 'auto',
					'margin-left'	: 30 + 'px'
				});
			} else {
				$( '.main-navigation' ).css({
					'margin-right'	: '',
					'margin-left'	: 'auto'
				});
			}
		});

		// submenu items align
		nealSetCustomize( 'menu', 'submenu-items-align', function ( to ) {
			$('.nav-menu li > ul.sub-menu').css( to, 0 );
			$('.nav-menu li > ul.sub-menu li ul.sub-menu').css( to, '-100%' );
		});

		// search icon
		nealSetCustomize( 'menu', 'search-icon', function ( to ) {
			$('.search-panel i.icon').
			removeClass( searchIcon ).
			addClass( to );
			searchIcon = to;
		});

		// footer menu item align
		nealSetCustomize( 'menu', 'footer-menu-items-align', function ( to ) {
			$('.footer-nav ul.footer-menu').css( 'text-align', to );
		});

	/* ----------------- Spacing Options ----------------- */

		// menu items padding-top-bottom
		nealSetCustomize( 'menu', 'menu-items-spac-padding-top-bottom', function ( to ) {
			$('.nav-menu > li:not(.icon) > a').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// menu items padding-right-left
		nealSetCustomize( 'menu', 'menu-items-spac-padding-right-left', function ( to ) {
			$('.nav-menu > li:not(.icon) > a').css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

		// menu items margin-right
		nealSetCustomize( 'menu', 'menu-items-spac-margin-right', function ( to ) {
			$('.nav-menu > li > a').css( 'margin-right', to + 'px' );
		});

		// submenu width
		nealSetCustomize( 'menu', 'submenu-items-spac-width', function ( to ) {
			$('.nav-menu li > ul.sub-menu').css( 'width', to + 'px' );
		});

		// submenu items padding-top-bottom
		nealSetCustomize( 'menu', 'submenu-items-spac-padding-top-bottom', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// submenu items padding-right-left
		nealSetCustomize( 'menu', 'submenu-items-spac-padding-right-left', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

		// submenu margin-top
		nealSetCustomize( 'menu', 'submenu-items-spac-margin-top', function ( to ) {
			$('.nav-menu > li > ul.sub-menu').css( 'top', to + 'px' );
		});

		// footer menu padding-top-bottom
		nealSetCustomize( 'menu', 'footer-menu-items-spac-padding-top-bottom', function ( to ) {
			$('.footer-nav').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

	/* ----------------- Styling Options ----------------- */
		
		var menuLinkColor = neal_options.menu['menu-items-styl-link-color'],
			menuIconsColor = neal_options.menu['menu-items-styl-icons-color'],
			submenuLinkColor = neal_options.menu['submenu-items-styl-link-color'];
		
		// menu items link color
		nealSetCustomize( 'menu', 'menu-items-styl-link-color', function ( to ) {
			$('.nav-menu > li:not(.active, .icon) > a').css( 'color', to );
			menuLinkColor = to;
		});

		// menu items link hover color
		nealSetCustomize( 'menu', 'menu-items-styl-link-hover-color', function ( to ) {
			$('.nav-menu > li:not(.active) > a').on( 'hover', function () {
				$(this).css( 'color', to );
			}, function () {
				if ( $(this).closest('li').hasClass('icon') ) {
					$(this).css( 'color', menuIconsColor );
				} else {
					$(this).css( 'color', menuLinkColor );
				}
			})
		});

		// menu items link active color
		nealSetCustomize( 'menu', 'menu-items-styl-link-active-color', function ( to ) {
			$('.nav-menu > li.active a').css( 'color', to );
		});

		// menu items icons color
		nealSetCustomize( 'menu', 'menu-items-styl-icons-color', function ( to ) {
			$('.nav-menu > li.icon > a').css( 'color', to );
			menuIconsColor = to;
		});

		// highlight active item
		nealSetCustomize( 'menu', 'menu-items-styl-active-item', function ( to ) {
			if ( to ) {
				$('.nav-menu > li.deactive').removeClass('deactive').addClass('active');
			} else {
				$('.nav-menu > li.active').removeClass('active').addClass('deactive');
			}
		});

		// active border-bottom
		nealCssBorder( 'menu', 'menu-items-styl-child', '.nav-menu > li.active a', ['bottom'] );

		// submenu items background-color
		nealSetCustomize( 'menu', 'submenu-items-styl-bg-color', function ( to ) {
			$('.nav-menu li > ul.sub-menu').css( 'background-color', to );
		});

		// submenu items link color
		nealSetCustomize( 'menu', 'submenu-items-styl-link-color', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css( 'color', to );
			submenuLinkColor = to;
		});

		// submenu items link hover color
		nealSetCustomize( 'menu', 'submenu-items-styl-link-hover-color', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').on( 'hover', function () {
				$(this).css( 'color', to );
			}, function () {
				$(this).css( 'color', submenuLinkColor );
			});
		});

		// footer background-color
		nealSetCustomize( 'menu', 'footer-menu-styl-bg-color', function ( to ) {
			$('.footer-nav').css( 'background-color', to );
		});

		// footer menu items background-color
		nealSetCustomize( 'menu', 'footer-menu-items-styl-link-color', function ( to ) {
			$('.footer-nav ul.footer-menu li a').css( 'color', to );
		});
		

	/* ----------------- Font Options ----------------- */

		// menu-items font-size
		nealSetCustomize( 'menu', 'menu-items-font-size', function ( to ) {
			$('.nav-menu > li > a').css( 'font-size', to + 'px' );
		});

		// menu-items font-weight
		nealSetCustomize( 'menu', 'menu-items-font-weight', function ( to ) {
			$('.nav-menu > li > a').css( 'font-weight', to );
		});

		// menu-items letter-spacing
		nealSetCustomize( 'menu', 'menu-items-font-letter-spacing', function ( to ) {
			$('.nav-menu > li > a').css( 'letter-spacing', to + 'px' );
		});

		// menu-items icons size
		nealSetCustomize( 'menu', 'menu-items-font-icon-size', function ( to ) {
			$('.site-header .header-area .icon a .icon').css( 'font-size', to + 'px' );
		});

		// menu-items italic
		nealSetCustomize( 'menu', 'menu-items-font-italic', function ( to ) {
			if ( to ) {
				$('.nav-menu > li > a').css( 'font-style', 'italic' );
			} else {
				$('.nav-menu > li > a').css( 'font-style', 'normal' );
			}
		});

		// menu-items uppercase
		nealSetCustomize( 'menu', 'menu-items-font-uppercase', function ( to ) {
			if ( to ) {
				$('.nav-menu > li > a').css( 'text-transform', 'uppercase' );
			} else {
				$('.nav-menu > li > a').css( 'text-transform', 'none' );
			}
		});

		// submenu-items font-size
		nealSetCustomize( 'menu', 'submenu-items-font-size', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css( 'font-size', to + 'px' );
		});

		// submenu-items font-weight
		nealSetCustomize( 'menu', 'submenu-items-font-weight', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css( 'font-weight', to );
		});

		// submenu-items letter-spacing
		nealSetCustomize( 'menu', 'submenu-items-font-letter-spacing', function ( to ) {
			$('.nav-menu li ul.sub-menu li a').css( 'letter-spacing', to + 'px' );
		});

		// submenu-items italic
		nealSetCustomize( 'menu', 'submenu-items-font-italic', function ( to ) {
			if ( to ) {
				$('.nav-menu li ul.sub-menu li a').css( 'font-style', 'italic' );
			} else {
				$('.nav-menu li ul.sub-menu li a').css( 'font-style', 'normal' );
			}
		});

		// submenu-items uppercase
		nealSetCustomize( 'menu', 'submenu-items-font-uppercase', function ( to ) {
			if ( to ) {
				$('.nav-menu li ul.sub-menu li a').css( 'text-transform', 'uppercase' );
			} else {
				$('.nav-menu li ul.sub-menu li a').css( 'text-transform', 'none' );
			}
		});

		// mobile menu font-size
		nealSetCustomize( 'menu', 'mobile-menu-font-size', function ( to ) {
			$('.mobile-nav .menu > li a').css( 'font-size', to + 'px' );
		});

		// mobile-menu font-weight
		nealSetCustomize( 'menu', 'mobile-menu-font-weight', function ( to ) {
			$('.mobile-nav .menu > li a').css( 'font-weight', to );
		});

		// mobile-menu letter-spacing
		nealSetCustomize( 'menu', 'mobile-menu-font-letter-spacing', function ( to ) {
			$('.mobile-nav .menu > li a').css( 'letter-spacing', to + 'px' );
		});

		// mobile-menu italic
		nealSetCustomize( 'menu', 'mobile-menu-font-italic', function ( to ) {
			if ( to ) {
				$('.mobile-nav .menu > li a').css( 'font-style', 'italic' );
			} else {
				$('.mobile-nav .menu > li a').css( 'font-style', 'normal' );
			}
		});

		// mobile-menu uppercase
		nealSetCustomize( 'menu', 'mobile-menu-font-uppercase', function ( to ) {
			if ( to ) {
				$('.mobile-nav .menu > li a').css( 'text-transform', 'uppercase' );
			} else {
				$('.mobile-nav .menu > li a').css( 'text-transform', 'none' );
			}
		});

		// footer menu font-size
		nealSetCustomize( 'menu', 'footer-menu-items-font-size', function ( to ) {
			$('.footer-nav ul.footer-menu li a').css( 'font-size', to + 'px' );
		});

		// footer menu font-weight
		nealSetCustomize( 'menu', 'footer-menu-items-font-weight', function ( to ) {
			$('.footer-nav ul.footer-menu li a').css( 'font-weight', to );
		});

		// footer menuu italic
		nealSetCustomize( 'menu', 'footer-menu-items-font-italic', function ( to ) {
			if ( to ) {
				$('.footer-nav ul.footer-menu li a').css( 'font-style', 'italic' );
			} else {
				$('.footer-nav ul.footer-menu li a').css( 'font-style', 'normal' );
			}
		});

		// footer menu uppercase
		nealSetCustomize( 'menu', 'footer-menu-items-font-uppercase', function ( to ) {
			if ( to ) {
				$('.footer-nav ul.footer-menu li a').css( 'text-transform', 'uppercase' );
			} else {
				$('.footer-nav ul.footer-menu li a').css( 'text-transform', 'none' );
			}
		});


/*
***************************************************************
* #Footer
***************************************************************
*/

	/* ----------------- General Options ----------------- */

		// widget title align
		nealSetCustomize( 'footer', 'wtitle-align', function ( to ) {
			$('.site-footer .widget-area .widget .widget-title').css( 'text-align', to );
		});

		// payment icon size
		nealSetCustomize( 'footer', 'payment-icon-size', function ( to ) {
			$('.site-footer .site-info .payment-icons span').css( 'font-size', to + 'px' );
		});

		// payment align
		nealSetCustomize( 'footer', 'payment-align', function ( to ) {
			$('.site-footer .site-info .payment-icons').css( 'float', to );
		});

	/* ----------------- Spacing Options ----------------- */

		// general padding-top
		nealSetCustomize( 'footer', 'general-spac-padding-top', function ( to ) {
			$('.site-footer').css( 'padding-top', to + 'px' );
		});

		// general padding-bottom
		nealSetCustomize( 'footer', 'general-spac-padding-bottom', function ( to ) {
			$('.site-footer').css( 'padding-bottom', to + 'px' );
		});

		// widget title margin-bottom
		nealSetCustomize( 'footer', 'wtitle-spac-margin-bottom', function ( to ) {
			$('.site-footer .widget-area .widget .widget-title').css( 'margin-bottom', to + 'px' );
		});

		// payment margin-right
		nealSetCustomize( 'footer', 'payment-spac-margin-right', function ( to ) {
			$('.site-footer .site-info .payment-icons span').css( 'margin-right', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		// general background-color
		nealSetCustomize( 'footer', 'general-styl-bg-color', function ( to ) {
			$('.site-footer').css( 'background-color', to );
		});

		// widgets font color
		nealSetCustomize( 'footer', 'widgets-styl-font-color', function ( to ) {
			$('.site-footer .widget-area .widget .foot-contact span').css( 'color', to );
		});

		// widgets link color
		nealSetCustomize( 'footer', 'widgets-styl-link-color', function ( to ) {
			$('.site-footer .widget-area .widget ul li a').css( 'color', to );
		});	

		// payment color
		nealSetCustomize( 'footer', 'payment-styl-color', function ( to ) {
			$('.site-footer .site-info .payment-icons span').css( 'color', to );
		});

/*
***************************************************************
* #Sidebar
***************************************************************
*/

	/* ----------------- General Options ----------------- */

		// widget title align
		nealSetCustomize( 'sidebar', 'wtitle-align', function ( to ) {
			$('.site-content .widget .widget-title').css( 'text-align', to );
		});

	/* ----------------- Spacing Options ----------------- */

		// general padding-top-bottom
		nealSetCustomize( 'sidebar', 'general-spac-padding-top-bottom', function ( to ) {
			$('.site-content .widget-area .widget').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// general padding-right-left
		nealSetCustomize( 'sidebar', 'general-spac-padding-right-left', function ( to ) {
			$('.site-content .widget-area .widget').css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

		// general margin-bottom
		nealSetCustomize( 'sidebar', 'general-spac-margin-bottom', function ( to ) {
			$('.site-content .widget-area .widget').css( 'margin-bottom', to + 'px' );
		});

		// widget title margin-bottom
		nealSetCustomize( 'sidebar', 'wtitle-spac-margin-bottom', function ( to ) {
			$('.site-content .widget-area .widget .widget-title').css( 'margin-bottom', to + 'px' );
		});


	/* ----------------- Styling Options ----------------- */

		// general backgorund-color
		nealSetCustomize( 'sidebar', 'general-styl-bg-color', function ( to ) {
			$('.site-content .widget-area .widget').css( 'background-color', to );
		});

		// general border
		nealCssBorder_4( 'sidebar', 'general-styl-child', '.site-content .widget-area .widget' );

		// widget title color
		nealSetCustomize( 'sidebar', 'wtitle-styl-font-color', function ( to ) {
			$('.site-content .widget-area .widget .widget-title').css( 'color', to );
		});

	/* ----------------- Font Options ----------------- */

		// widget title font-size
		nealSetCustomize( 'sidebar', 'wtitle-font-size', function ( to ) {
			$('.site-content .widget-area .widget .widget-title').css( 'font-size', to + 'px' );
		});

		// widget title font-weight
		nealSetCustomize( 'sidebar', 'wtitle-font-weight', function ( to ) {
			$('.site-content .widget-area .widget .widget-title').css( 'font-weight', to );
		});

		// widget title italic
		nealSetCustomize( 'sidebar', 'wtitle-font-italic', function ( to ) {
			if ( to ) {
				$('.site-content .widget-area .widget .widget-title').css( 'font-style', 'italic' );
			} else {
				$('.site-content .widget-area .widget .widget-title').css( 'font-style', 'normal' );
			}
		});

		// widget title uppercase
		nealSetCustomize( 'sidebar', 'wtitle-font-uppercase', function ( to ) {
			if ( to ) {
				$('.site-content .widget-area .widget .widget-title').css( 'text-transform', 'uppercase' );
			} else {
				$('.site-content .widget-area .widget .widget-title').css( 'text-transform', 'none' );
			}
		});

/*
***************************************************************
* #Blog page
***************************************************************
*/
	
	var blogPosts = $( '.blog-posts');

	/* ----------------- General Options ----------------- */

		// categories align
		nealSetCustomize( 'blog', 'categories-align', function ( to ) {
			blogPosts.find('.entry-header .meta-categories' ).css( 'text-align', to );
		
		});

		// title align
		nealSetCustomize( 'blog', 'title-align', function ( to ) {
			blogPosts.find('.entry-title' ).css( 'text-align', to );
		
		});

		// author align
		nealSetCustomize( 'blog', 'author-align', function ( to ) {
			blogPosts.find('.entry-header .meta-author' ).css( 'text-align', to );
		
		});

		// date align
		nealSetCustomize( 'blog', 'date-align', function ( to ) {
			blogPosts.find('.entry-header .meta-date' ).css( 'text-align', to );
		
		});

		// description align
		nealSetCustomize( 'blog', 'description-align', function ( to ) {
			blogPosts.find('.entry-content' ).css( 'text-align', to );
		
		});

		// read-more align
		nealSetCustomize( 'blog', 'read-more-align', function ( to ) {
			blogPosts.find('.read-more-block' ).css( 'text-align', to );
		
		});

	/* ----------------- Spacing Options ----------------- */

		// media margin-bottom
		nealSetCustomize( 'blog', 'media-spac-margin-bottom', function ( to ) {
			blogPosts.find( '.entry-media' ).css( 'margin-bottom', to + 'px' );
		});

		// title margin-bottom
		nealSetCustomize( 'blog', 'title-spac-margin-bottom', function ( to ) {
			blogPosts.find( '.entry-title' ).css( 'margin-bottom', to + 'px' );
		});

		// meta-items margin-bottom
		nealSetCustomize( 'blog', 'meta-items-spac-margin-bottom', function ( to ) {
			blogPosts.find( '.meta-categories, .meta-author, .meta-date' ).css( 'margin-bottom', to + 'px' );
		});

		// description margin-bottom
		nealSetCustomize( 'blog', 'description-spac-margin-bottom', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'margin-bottom', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		var bEntryTitleColor = neal_options.blog['title-styl-font-color'],
			bEntryMetaLinkColor = neal_options.blog['meta-styl-link-color'],
			bReadMoreColor = neal_options.blog['read-more-styl-font-color'],
			bReadMoreBorderBottomSize	= neal_options.blog['read-more-styl-child-border-bottom-size'],
			bReadMoreBorderBottomStyle 	= neal_options.blog['read-more-styl-child-border-bottom-style'],
			bReadMoreBorderBottomColor 	= neal_options.blog['read-more-styl-child-border-bottom-color'];

		// title color
		nealSetCustomize( 'blog', 'title-styl-font-color', function ( to ) {
			blogPosts.find( '.entry-title a' ).css( 'color', to );
			bEntryTitleColor = to;
		});

		// title hover color
		nealSetCustomize( 'blog', 'title-styl-font-hover-color', function ( to ) {
			blogPosts.find( '.entry-title a' ).on( 'hover', function () {
				$( this ).css( 'color', to );
			}, function () {
				$( this ).css( 'color', bEntryTitleColor );
			});
		});


		// meta font color
		nealSetCustomize( 'blog', 'meta-styl-font-color', function ( to ) {
			blogPosts.find( '.meta-author, .meta-date' ).css( 'color', to );
		});

		// meta link color
		nealSetCustomize( 'blog', 'meta-styl-link-color', function ( to ) {
			blogPosts.find('.meta-categories a, .meta-author a' ).css( 'color', to );
			bEntryMetaLinkColor = to;
		});

		// meta link hover color
		nealSetCustomize( 'blog', 'meta-styl-link-hover-color', function ( to ) {
			blogPosts.find( '.meta-categories a, .meta-author a' ).on( 'hover', function () {
				$( this ).css( 'color', to );
			}, function () {
				$( this ).css( 'color', bEntryMetaLinkColor);
			});
		});

		// meta icon color
		nealSetCustomize( 'blog', 'meta-styl-icon-color', function ( to ) {
			blogPosts.find( '.meta-author i, .meta-date i' ).css( 'color', to );
		});

		// description font color
		nealSetCustomize( 'blog', 'desc-styl-font-color', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'color', to );
		});

		// read-more color
		nealSetCustomize( 'blog', 'read-more-styl-font-color', function ( to ) {
			blogPosts.find( 'a.read-more' ).css( 'color', to );
			bReadMoreColor = to;
		});

		// read-more hover color
		nealSetCustomize( 'blog', 'read-more-styl-font-hover-color', function ( to ) {
			blogPosts.find( 'a.read-more' ).on( 'hover', function () {
				$( this ).css( 'color', to );
			}, function () {
				$( this ).css( 'color', bReadMoreColor );
			});
		});

		// read-more border-bottom label
		nealCssBorder( 'blog', 'read-more-styl-child', '.blog-posts a.read-more', ['bottom'] );


	/* ----------------- Font Options ----------------- */

		// title font size
		nealSetCustomize( 'blog', 'title-font-size', function ( to ) {
			blogPosts.find( '.entry-title' ).css( 'font-size', to + 'px' );
		});

		// title font weight
		nealSetCustomize( 'blog', 'title-font-weight', function ( to ) {
			blogPosts.find( '.entry-title' ).css( 'font-weight', to );
		});

		// title line height
		nealSetCustomize( 'blog', 'title-font-line-height', function ( to ) {
			blogPosts.find( '.entry-title' ).css( 'line-height', to );
		});

		// title letter spacing
		nealSetCustomize( 'blog', 'title-font-letter-spacing', function ( to ) {
			blogPosts.find( '.entry-title' ).css( 'letter-spacing', to + 'px' );
		});

		// title italic
		nealSetCustomize( 'blog', 'title-font-italic', function ( to ) {
			if ( to ) {
				blogPosts.find( '.entry-title' ).css( 'font-style', 'italic' );
			} else {
				blogPosts.find( '.entry-title' ).css( 'font-style', 'normal' );
			}
		});

		// title uppercase
		nealSetCustomize( 'blog', 'title-font-uppercase', function ( to ) {
			if ( to ) {
				blogPosts.find( '.entry-title' ).css( 'text-transform', 'uppercase' );
			} else {
				blogPosts.find( '.entry-title' ).css( 'text-transform', 'none' );
			}
		});

		// meta font size
		nealSetCustomize( 'blog', 'meta-font-size', function ( to ) {
			blogPosts.find('.meta-date span.post-date, .meta-author span.post-author').css( 'font-size', to + 'px' );
		});

		// meta italic
		nealSetCustomize( 'blog', 'meta-font-italic', function ( to ) {
			if ( to ) {
				blogPosts.find( '.meta-date span.post-date, .meta-author span.post-author' ).css( 'font-style', 'italic' );
			} else {
				blogPosts.find( '.meta-date span.post-date, .meta-author span.post-author' ).css( 'font-style', 'normal' );
			}
		});

		// meta uppercase
		nealSetCustomize( 'blog', 'meta-font-uppercase', function ( to ) {
			if ( to ) {
				blogPosts.find( '.meta-date span.post-date, .meta-author span.post-author' ).css( 'text-transform', 'uppercase' );
			} else {
				blogPosts.find( '.meta-date span.post-date, .meta-author span.post-author' ).css( 'text-transform', 'none' );
			}
		});

		// categories font size
		nealSetCustomize( 'blog', 'categories-font-size', function ( to ) {
			blogPosts.find( '.meta-categories .post-category a' ).css( 'font-size', to + 'px' );
		});

		// categories font weight
		nealSetCustomize( 'blog', 'categories-font-weight', function ( to ) {
			blogPosts.find( '.meta-categories .post-category a' ).css( 'font-weight', to );
		});

		// categories letter spacing
		nealSetCustomize( 'blog', 'categories-font-letter-spacing', function ( to ) {
			blogPosts.find( '.meta-categories .post-category a' ).css( 'letter-spacing', to + 'px' );
		});

		// categories italic
		nealSetCustomize( 'blog', 'categories-font-italic', function ( to ) {
			if ( to ) {
				blogPosts.find( '.meta-categories .post-category a' ).css( 'font-style', 'italic' );
			} else {
				blogPosts.find( '.meta-categories .post-category a' ).css( 'font-style', 'normal' );
			}
		});

		// categories uppercase
		nealSetCustomize( 'blog', 'categories-font-uppercase', function ( to ) {
			if ( to ) {
				blogPosts.find( '.meta-categories .post-category a' ).css( 'text-transform', 'uppercase' );
			} else {
				blogPosts.find( '.meta-categories .post-category a' ).css( 'text-transform', 'none' );
			}
		});

		// description font size
		nealSetCustomize( 'blog', 'desc-font-size', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'font-size', to + 'px' );
		});

		// description font weight
		nealSetCustomize( 'blog', 'desc-font-weight', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'font-weight', to );
		});

		// description line height
		nealSetCustomize( 'blog', 'desc-font-line-height', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'line-height', to );
		});

		// description letter spacing
		nealSetCustomize( 'blog', 'desc-font-letter-spacing', function ( to ) {
			blogPosts.find( '.entry-content' ).css( 'letter-spacing', to + 'px' );
		});

		// description italic
		nealSetCustomize( 'blog', 'desc-font-italic', function ( to ) {
			if ( to ) {
				blogPosts.find( '.entry-content' ).css( 'font-style', 'italic' );
			} else {
				blogPosts.find( '.entry-content' ).css( 'font-style', 'normal' );
			}
		});

		// description uppercase
		nealSetCustomize( 'blog', 'desc-font-uppercase', function ( to ) {
			if ( to ) {
				blogPosts.find( '.entry-content' ).css( 'text-transform', 'uppercase' );
			} else {
				blogPosts.find( '.entry-content' ).css( 'text-transform', 'none' );
			}
		});

		// read-more font size
		nealSetCustomize( 'blog', 'read-more-font-size', function ( to ) {
			blogPosts.find( 'a.read-more' ).css( 'font-size', to + 'px' );
		});

		// read-more font weight
		nealSetCustomize( 'blog', 'read-more-font-weight', function ( to ) {
			blogPosts.find( 'a.read-more' ).css( 'font-weight', to );
		});

		// read-more italic
		nealSetCustomize( 'blog', 'read-more-font-italic', function ( to ) {
			if ( to ) {
				blogPosts.find( 'a.read-more' ).css( 'font-style', 'italic' );
			} else {
				blogPosts.find( 'a.read-more' ).css( 'font-style', 'normal' );
			}
		});

		// read-more uppercase
		nealSetCustomize( 'blog', 'read-more-font-uppercase', function ( to ) {
			if ( to ) {
				blogPosts.find( 'a.read-more' ).css( 'text-transform', 'uppercase' );
			} else {
				blogPosts.find( 'a.read-more' ).css( 'text-transform', 'none' );
			}
		});


/*
***************************************************************
* #Blog Single
***************************************************************
*/
	var singlePost = $( '.single-post');

	/* ----------------- General Options ----------------- */

		// title align
		nealSetCustomize( 'blog_single', 'title-align', function ( to ) {
			singlePost.find('.entry-title' ).css( 'text-align', to );
		
		});

		// breadcrumb align
		nealSetCustomize( 'blog_single', 'bread-align', function ( to ) {
			singlePost.find('.site-main .breadcrumbs' ).css( 'text-align', to );
		
		});

		// meta align
		nealSetCustomize( 'blog_single', 'meta-align', function ( to ) {
			singlePost.find('.entry-header .post-meta' ).css( 'text-align', to );
		
		});

	/* ----------------- Spacing Options ----------------- */

		// media margin-bottom
		nealSetCustomize( 'blog_single', 'media-spac-margin-bottom', function ( to ) {
			singlePost.find( '.entry-media' ).css( 'margin-bottom', to + 'px' );
		});

		// title margin-bottom
		nealSetCustomize( 'blog_single', 'title-spac-margin-bottom', function ( to ) {
			singlePost.find( '.entry-title' ).css( 'margin-bottom', to + 'px' );
		});

		// meta margin-bottom
		nealSetCustomize( 'blog_single', 'meta-spac-margin-bottom', function ( to ) {
			singlePost.find( '.entry-header .post-meta' ).css( 'margin-bottom', to + 'px' );
		});

	/* ----------------- Font Options ----------------- */

		// title font-size
		nealSetCustomize( 'blog_single', 'title-font-size', function ( to ) {
			singlePost.find( '.entry-title' ).css( 'font-size', to + 'px' );
		});

		// title font-weight
		nealSetCustomize( 'blog_single', 'title-font-weight', function ( to ) {
			singlePost.find( '.entry-title' ).css( 'font-weight', to );
		});

		// title line-height
		nealSetCustomize( 'blog_single', 'title-font-line-height', function ( to ) {
			singlePost.find( '.entry-title' ).css( 'line-height', to );
		});

		// title letter-spacing
		nealSetCustomize( 'blog_single', 'title-font-letter-spacing', function ( to ) {
			singlePost.find( '.entry-title' ).css( 'letter-spacing', to + 'px' );
		});

		// title uppercase
		nealSetCustomize( 'blog_single', 'title-font-uppercase', function ( to ) {
			if ( to ) {
				singlePost.find( '.entry-title' ).css( 'text-transform', 'uppercase' );
			} else {
				singlePost.find( '.entry-title' ).css( 'text-transform', 'none' );
			}
		});

		// content font-size
		nealSetCustomize( 'blog_single', 'content-font-size', function ( to ) {
			singlePost.find( '.entry-content' ).css( 'font-size', to + 'px' );
		});

		// content font-weight
		nealSetCustomize( 'blog_single', 'content-font-weight', function ( to ) {
			singlePost.find( '.entry-content' ).css( 'font-weight', to );
		});

		// content line-height
		nealSetCustomize( 'blog_single', 'content-font-line-height', function ( to ) {
			singlePost.find( '.entry-content' ).css( 'line-height', to );
		});

		// content letter-spacing
		nealSetCustomize( 'blog_single', 'content-font-letter-spacing', function ( to ) {
			singlePost.find( '.entry-content' ).css( 'letter-spacing', to + 'px' );
		});


/*
***************************************************************
* #Homepage
***************************************************************
*/
	
	/* ----------------- Spacing Options ----------------- */

	/* ----------------- Styling Options ----------------- */

		// about-me corner-radius
		nealSetCustomize( 'homepage', 'about-me-styl-radius', function ( to ) {
			$('.author-vcard .author-avatar img').css( 'border-radius', to + '%' );
		});

	/* ----------------- Font Options ----------------- */


/*
***************************************************************
* #Comments
***************************************************************
*/
	/* ----------------- Spacing Options ----------------- */

		// author-image margin-right
		nealSetCustomize( 'comments', 'author-img-spac-margin-right', function ( to ) {
			$('.comments-area .comment-author-img').css( 'margin-right', to + 'px' );
		});

		// comment-content padding-top-bottom
		nealSetCustomize( 'comments', 'comment-content-spac-padding-top-bottom', function ( to ) {
			$('.comments-area .comment-content').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// comment-content padding-right-left
		nealSetCustomize( 'comments', 'comment-content-spac-padding-right-left', function ( to ) {
			$('.comments-area .comment-content').css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

		// comment-content vertical guuter
		nealSetCustomize( 'comments', 'comment-content-spac-vertical-gutter', function ( to ) {
			$('.comments-area .comment-content').css( 'margin-bottom', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		// author-image corner-radius
		nealSetCustomize( 'comments', 'author-img-styl-radius', function ( to ) {
			$('.comments-area .comment-author-img img').css( 'border-radius', to + '%' );
		});

		// comment-content background-color
		nealSetCustomize( 'comments', 'comment-content-styl-bg-color', function ( to ) {
			$('.comments-area .comment-content').css( 'background-color', to );
		});

		// comment-content font color
		nealSetCustomize( 'comments', 'comment-content-styl-font-color', function ( to ) {
			$('.comments-area .comment-content p').css( 'color', to );
		});

/*
***************************************************************
* #Inputs
***************************************************************
*/	
	// selectors
	var generalInput = $('.input-text, input[type=text], input[type=email], input[type=url], input[type=password], input[type=search], textarea'),
		submitButton = $('.button:not(.order-btn), button, input[type="button"], input[type="reset"], input[type="submit"]');


	/* ----------------- Spacing Options ----------------- */

		// input padding-top-bottom
		nealSetCustomize( 'inputs', 'general-spac-padding-top-bottom', function ( to ) {
			generalInput.css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// input padding-right-left
		nealSetCustomize( 'inputs', 'general-spac-padding-right-left', function ( to ) {
			generalInput.css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

		// submit button padding-top-bottom
		nealSetCustomize( 'inputs', 'submit-button-spac-padding-top-bottom', function ( to ) {
			submitButton.css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px'
			});
		});

		// submit button padding-right-left
		nealSetCustomize( 'inputs', 'submit-button-spac-padding-right-left', function ( to ) {
			submitButton.css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px'
			});
		});

	/* ----------------- Styling Options ----------------- */

		var inputBgColor,
			inputFontColor,
			inputBorderRadius = neal_options.inputs['general-styl-child-radius'],
			buttonBgColor = neal_options.inputs['submit-button-styl-bg-color'],
			buttonFontColor = neal_options.inputs['submit-button-styl-font-color'],
			buttonBorderRadius = neal_options.inputs['submit-button-styl-child-radius'];

		// input font color
		nealSetCustomize( 'inputs', 'general-styl-font-color', function ( to ) {
			generalInput.css( 'color', to );
			inputFontColor = to;
		});

		// input font focus color
		nealSetCustomize( 'inputs', 'general-styl-font-focus-color', function ( to ) {
			generalInput.focus( function () {
				$(this).css( 'color', to );
			}, function () {
				$(this).css( 'color', inputFontColor );
			})
		});

		// input border focus color
		nealSetCustomize( 'inputs', 'general-styl-border-focus-color', function ( to ) {
			generalInput.focus( function () {
				$(this).css( 'border-color', to );
			}).focusout( function () {
				var color = neal_options.inputs['general-styl-child-border-color'];
				$(this).css( 'border-color', color );
			})
		});

		// input border
		nealCssBorder_4( 'inputs', 'general-styl-child', generalInput );

		// input border-radius label
		nealSetCustomize( 'inputs', 'general-styl-child-radius-label', function ( to ) {
			if ( to ) {
				generalInput.css( 'border-radius', inputBorderRadius + 'px' );
			} else {
				generalInput.css( 'border-radius', '' );
			}
		});

		// input border-radius
		nealSetCustomize( 'inputs', 'general-styl-child-radius', function ( to ) {
			generalInput.css( 'border-radius', to + 'px' );
			inputBorderRadius = to;
		});

		// submit button background-color
		nealSetCustomize( 'inputs', 'submit-button-styl-bg-color', function ( to ) {
			submitButton.css( 'background-color', to );
			buttonBgColor = to;
		});

		// submit button font color
		nealSetCustomize( 'inputs', 'submit-button-styl-font-color', function ( to ) {
			submitButton.css( 'color', to );
			buttonFontColor = to;
		});

		// submit button hover background-color
		nealSetCustomize( 'inputs', 'submit-button-styl-bg-hover-color', function ( to ) {
			submitButton.on( 'hover', function () {
				$(this).css( 'background-color', to );
			}, function () {
				$(this).css( 'background-color', buttonBgColor );
			});
		});

		// submit button hover font color
		nealSetCustomize( 'inputs', 'submit-button-styl-font-hover-color', function ( to ) {
			submitButton.on( 'hover', function () {
				$(this).css( 'color', to );
			}, function () {
				$(this).css( 'color', buttonFontColor );
			});
		});

		// submit button hover border color
		nealSetCustomize( 'inputs', 'submit-button-styl-border-hover-color', function ( to ) {
			submitButton.on( 'hover', function () {
				$(this).css( 'border-color', to );
			}, function () {
				var color = neal_options.inputs['submit-button-styl-child-border-color'];
				$(this).css( 'border-color', color );
			});
		});

		// submit button border
		nealCssBorder_4( 'inputs', 'submit-button-styl-child', submitButton );

		// submit button border-radius label
		nealSetCustomize( 'inputs', 'submit-button-styl-child-radius-label', function ( to ) {
			if ( to ) {
				submitButton.css( 'border-radius', buttonBorderRadius + 'px' );
			} else {
				submitButton.css( 'border-radius', '' );
			}
		});

		// submit button border-radius
		nealSetCustomize( 'inputs', 'submit-button-styl-child-radius', function ( to ) {
			submitButton.css( 'border-radius', to + 'px' );
			buttonBorderRadius = to;
		});


	/* ----------------- Font Options ----------------- */

		// input font-size
		nealSetCustomize( 'inputs', 'general-font-size', function ( to ) {
			generalInput.css( 'font-size', to + 'px' );
		});

		// input font-weight
		nealSetCustomize( 'inputs', 'general-font-weight', function ( to ) {
			generalInput.css( 'font-weight', to );
		});

		// input letter-spacing
		nealSetCustomize( 'inputs', 'general-font-letter-spacing', function ( to ) {
			generalInput.css( 'letter-spacing', to + 'px' );
		});

		// submit button font-size
		nealSetCustomize( 'inputs', 'submit-button-font-size', function ( to ) {
			submitButton.css( 'font-size', to + 'px' );
		});

		// submit button font-weight
		nealSetCustomize( 'inputs', 'submit-button-font-weight', function ( to ) {
			submitButton.css( 'font-weight', to );
		});

		// submit button letter-spacing
		nealSetCustomize( 'inputs', 'submit-button-font-letter-spacing', function ( to ) {
			submitButton.css( 'letter-spacing', to + 'px' );
		});

		// submit button uppercase
		nealSetCustomize( 'inputs', 'submit-button-font-uppercase', function ( to ) {
			if ( to ) {
				submitButton.css( 'text-transform', 'uppercase' );
			} else {
				submitButton.css( 'text-transform', 'none' );
			}
		});


/*
***************************************************************
* #Contact page
***************************************************************
*/	
	
	/* ----------------- Spacing Options ----------------- */

		// google-map height
		nealSetCustomize( 'contact', 'google-map-spac-height', function ( to ) {
			$('.google-map').css( 'height', to + 'px' );
		});

		// google-map margin-top-bottom
		nealSetCustomize( 'contact', 'google-map-spac-margin-top-bottom', function ( to ) {
			$('.google-map').css({
				'margin-top' : to + 'px',
				'margin-bottom' : to + 'px'
			});
		});

/*
***************************************************************
* #Pagination
***************************************************************
*/	
	/* ----------------- General Options ----------------- */

		// navigation align
		nealSetCustomize( 'pagination', 'navigation-align', function ( to ) {
			$('.navigation.pagination').css( 'text-align', to );
		});

	/* ----------------- Spacing Options ----------------- */

		// navigation padding-top-bottom
		nealSetCustomize( 'pagination', 'navigation-spac-padding-top-bottom', function ( to ) {
			$('ul.page-numbers li > a, ul.page-numbers li > span').css({
				'padding-top' : to + 'px',
				'padding-bottom' : to + 'px' 
			});
		});

		// navigation padding-right-left
		nealSetCustomize( 'pagination', 'navigation-spac-padding-right-left', function ( to ) {
			$('ul.page-numbers li > a, ul.page-numbers li > span').css({
				'padding-right' : to + 'px',
				'padding-left' : to + 'px' 
			});
		});

		// navigation margin-right
		nealSetCustomize( 'pagination', 'navigation-spac-margin-right', function ( to ) {
			$('ul.page-numbers > li').css( 'margin-right', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */

		var navBgColor = neal_options.pagination['navigation-styl-bg-color'],
			navLinkColor = neal_options.pagination['navigation-styl-link-color'];

		// navigation background-color
		nealSetCustomize( 'pagination', 'navigation-styl-bg-color', function ( to ) {
			$('ul.page-numbers li > a').css( 'background-color', to );
			navBgColor = to;
		});

		// navigation hover background-color
		nealSetCustomize( 'pagination', 'navigation-styl-bg-hover-color', function ( to ) {
			$('ul.page-numbers li > a').on( 'hover', function () {
				$(this).css( 'background-color', to );
			}, function () {
				$(this).css( 'background-color', navBgColor );
			})
		});

		// navigation active background-color
		nealSetCustomize( 'pagination', 'navigation-styl-bg-active-color', function ( to ) {
			$('ul.page-numbers li > span.current').css( 'background-color', to );
		});

		// navigation font color
		nealSetCustomize( 'pagination', 'navigation-styl-font-color', function ( to ) {
			$('ul.page-numbers li > span').css( 'color', to );
		});

		// navigation link color
		nealSetCustomize( 'pagination', 'navigation-styl-link-color', function ( to ) {
			$('ul.page-numbers li > a, .navigation.default a').css( 'color', to );
			navLinkColor = to;
		});

		// navigation link hover color
		nealSetCustomize( 'pagination', 'navigation-styl-link-hover-color', function ( to ) {
			$('ul.page-numbers li > a, .navigation.default a').on( 'hover', function () {
				$(this).css( 'color', to );
			}, function () {
				$(this).css( 'color', navLinkColor );
			});
		});

		// navigation font active color
		nealSetCustomize( 'pagination', 'navigation-styl-font-active-color', function ( to ) {
			$('ul.page-numbers li > span.current').css( 'color', to );
		});

	/* ----------------- Font Options ----------------- */

		// navigation font size
		nealSetCustomize( 'pagination', 'navigation-font-size', function ( to ) {
			$('ul.page-numbers li > a, ul.page-numbers li > span, .navigation.default a').css( 'font-size', to + 'px' );
		});

		// navigation font weight
		nealSetCustomize( 'pagination', 'navigation-font-weight', function ( to ) {
			$('ul.page-numbers li > a, ul.page-numbers li > span, .navigation.default a').css( 'font-weight', to );
		});

		// navigation uppercase
		nealSetCustomize( 'pagination', 'navigation-font-uppercase', function ( to ) {
			if ( to ) {
				$('.navigation.default a').css( 'text-transform', 'uppercase' );
			} else {
				$('.navigation.default a').css( 'text-transform', 'none' );
			}
		});

/*
***************************************************************
* #Typography
***************************************************************
*/	
	
	/* ----------------- Font Options ----------------- */

		// paragraph font-size
		nealSetCustomize( 'typography', 'paragraph-font-size', function ( to ) {
			$('p').css( 'font-size', to + 'px' );
		
		});

		// paragraph font-weight
		nealSetCustomize( 'typography', 'paragraph-font-weight', function ( to ) {
			$('p').css( 'font-weight', to );
		
		});

		// paragraph line-height
		nealSetCustomize( 'typography', 'paragraph-font-line-height', function ( to ) {
			$('p').css( 'line-height', to );
		
		});

		// paragraph letter-spacing
		nealSetCustomize( 'typography', 'paragraph-font-letter-spacing', function ( to ) {
			$('p').css( 'letter-spacing', to + 'px' );
		
		});

		// paragraph italic
		nealSetCustomize( 'typography', 'paragraph-font-italic', function ( to ) {
			if ( to ) {
				$('p').css( 'font-style', 'italic' );
			} else {
				$('p').css( 'font-style', 'normal' );
			}
		
		});

		// paragraph uppercase
		nealSetCustomize( 'typography', 'paragraph-font-uppercase', function ( to ) {
			if ( to ) {
				$('p').css( 'text-transform', 'uppercase' );
			} else {
				$('p').css( 'text-transform', 'none' );
			}
		
		});

		// h1 font-size
		nealSetCustomize( 'typography', 'h1-font-size', function ( to ) {
			$('h1').css( 'font-size', to + 'px' );
		
		});

		// h1 font-weight
		nealSetCustomize( 'typography', 'h1-font-weight', function ( to ) {
			$('h1').css( 'font-weight', to );
		
		});

		// h1 line-height
		nealSetCustomize( 'typography', 'h1-font-line-height', function ( to ) {
			$('h1').css( 'line-height', to );
		
		});

		// h1 letter-spacing
		nealSetCustomize( 'typography', 'h1-font-letter-spacing', function ( to ) {
			$('h1').css( 'letter-spacing', to + 'px' );
		
		});

		// h1 italic
		nealSetCustomize( 'typography', 'h1-font-italic', function ( to ) {
			if ( to ) {
				$('h1').css( 'font-style', 'italic' );
			} else {
				$('h1').css( 'font-style', 'normal' );
			}
		
		});

		// h1 uppercase
		nealSetCustomize( 'typography', 'h1-font-uppercase', function ( to ) {
			if ( to ) {
				$('h1').css( 'text-transform', 'uppercase' );
			} else {
				$('h1').css( 'text-transform', 'none' );
			}
		
		});

		// h2 font-size
		nealSetCustomize( 'typography', 'h2-font-size', function ( to ) {
			$('h2').css( 'font-size', to + 'px' );
		
		});

		// h2 font-weight
		nealSetCustomize( 'typography', 'h2-font-weight', function ( to ) {
			$('h2').css( 'font-weight', to );
		
		});

		// h2 line-height
		nealSetCustomize( 'typography', 'h2-font-line-height', function ( to ) {
			$('h2').css( 'line-height', to );
		
		});

		// h2 letter-spacing
		nealSetCustomize( 'typography', 'h2-font-letter-spacing', function ( to ) {
			$('h2').css( 'letter-spacing', to + 'px' );
		
		});

		// h2 italic
		nealSetCustomize( 'typography', 'h2-font-italic', function ( to ) {
			if ( to ) {
				$('h2').css( 'font-style', 'italic' );
			} else {
				$('h2').css( 'font-style', 'normal' );
			}
		
		});

		// h2 uppercase
		nealSetCustomize( 'typography', 'h2-font-uppercase', function ( to ) {
			if ( to ) {
				$('h2').css( 'text-transform', 'uppercase' );
			} else {
				$('h2').css( 'text-transform', 'none' );
			}
		
		});

		// h3 font-size
		nealSetCustomize( 'typography', 'h3-font-size', function ( to ) {
			$('h3').css( 'font-size', to + 'px' );
		
		});

		// h3 font-weight
		nealSetCustomize( 'typography', 'h3-font-weight', function ( to ) {
			$('h3').css( 'font-weight', to );
		
		});

		// h3 line-height
		nealSetCustomize( 'typography', 'h3-font-line-height', function ( to ) {
			$('h3').css( 'line-height', to );
		
		});

		// h3 letter-spacing
		nealSetCustomize( 'typography', 'h3-font-letter-spacing', function ( to ) {
			$('h3').css( 'letter-spacing', to + 'px' );
		
		});

		// h3 italic
		nealSetCustomize( 'typography', 'h3-font-italic', function ( to ) {
			if ( to ) {
				$('h3').css( 'font-style', 'italic' );
			} else {
				$('h3').css( 'font-style', 'normal' );
			}
		
		});

		// h3 uppercase
		nealSetCustomize( 'typography', 'h3-font-uppercase', function ( to ) {
			if ( to ) {
				$('h3').css( 'text-transform', 'uppercase' );
			} else {
				$('h3').css( 'text-transform', 'none' );
			}
		
		});

		// h4 font-size
		nealSetCustomize( 'typography', 'h4-font-size', function ( to ) {
			$('h4').css( 'font-size', to + 'px' );
		
		});

		// h4 font-weight
		nealSetCustomize( 'typography', 'h4-font-weight', function ( to ) {
			$('h4').css( 'font-weight', to );
		
		});

		// h4 line-height
		nealSetCustomize( 'typography', 'h4-font-line-height', function ( to ) {
			$('h4').css( 'line-height', to );
		
		});

		// h4 letter-spacing
		nealSetCustomize( 'typography', 'h4-font-letter-spacing', function ( to ) {
			$('h4').css( 'letter-spacing', to + 'px' );
		
		});

		// h4 italic
		nealSetCustomize( 'typography', 'h4-font-italic', function ( to ) {
			if ( to ) {
				$('h4').css( 'font-style', 'italic' );
			} else {
				$('h4').css( 'font-style', 'normal' );
			}
		
		});

		// h4 uppercase
		nealSetCustomize( 'typography', 'h4-font-uppercase', function ( to ) {
			if ( to ) {
				$('h4').css( 'text-transform', 'uppercase' );
			} else {
				$('h4').css( 'text-transform', 'none' );
			}
		
		});

		// h5 font-size
		nealSetCustomize( 'typography', 'h5-font-size', function ( to ) {
			$('h5').css( 'font-size', to + 'px' );
		
		});

		// h5 font-weight
		nealSetCustomize( 'typography', 'h5-font-weight', function ( to ) {
			$('h5').css( 'font-weight', to );
		
		});

		// h5 line-height
		nealSetCustomize( 'typography', 'h5-font-line-height', function ( to ) {
			$('h5').css( 'line-height', to );
		
		});

		// h5 letter-spacing
		nealSetCustomize( 'typography', 'h5-font-letter-spacing', function ( to ) {
			$('h5').css( 'letter-spacing', to + 'px' );
		
		});

		// h5 italic
		nealSetCustomize( 'typography', 'h5-font-italic', function ( to ) {
			if ( to ) {
				$('h5').css( 'font-style', 'italic' );
			} else {
				$('h5').css( 'font-style', 'normal' );
			}
		
		});

		// h5 uppercase
		nealSetCustomize( 'typography', 'h5-font-uppercase', function ( to ) {
			if ( to ) {
				$('h5').css( 'text-transform', 'uppercase' );
			} else {
				$('h5').css( 'text-transform', 'none' );
			}
		
		});

		// h6 font-size
		nealSetCustomize( 'typography', 'h6-font-size', function ( to ) {
			$('h6').css( 'font-size', to + 'px' );
		
		});

		// h6 font-weight
		nealSetCustomize( 'typography', 'h6-font-weight', function ( to ) {
			$('h6').css( 'font-weight', to );
		
		});

		// h6 line-height
		nealSetCustomize( 'typography', 'h6-font-line-height', function ( to ) {
			$('h6').css( 'line-height', to );
		
		});

		// h6 letter-spacing
		nealSetCustomize( 'typography', 'h6-font-letter-spacing', function ( to ) {
			$('h6').css( 'letter-spacing', to + 'px' );
		
		});

		// h6 italic
		nealSetCustomize( 'typography', 'h6-font-italic', function ( to ) {
			if ( to ) {
				$('h6').css( 'font-style', 'italic' );
			} else {
				$('h6').css( 'font-style', 'normal' );
			}
		
		});

		// h6 uppercase
		nealSetCustomize( 'typography', 'h6-font-uppercase', function ( to ) {
			if ( to ) {
				$('h6').css( 'text-transform', 'uppercase' );
			} else {
				$('h6').css( 'text-transform', 'none' );
			}
		
		});

/*
***************************************************************
* #Socials & Copyright
***************************************************************
*/
	
	/* ----------------- General Options ----------------- */

		// copyright align
		nealSetCustomize( 'socialcopy', 'general-copy-align', function ( to ) {
			// empty
		});

	/* ----------------- Spacing Options ----------------- */

		// socials width
		nealSetCustomize( 'socialcopy', 'social-spac-width', function ( to ) {
			$('.site-footer .social-icons a').css( 'width', to + 'px' );
		});

		// socials height
		nealSetCustomize( 'socialcopy', 'social-spac-height', function ( to ) {
			$('.site-footer .social-icons a').css( 'height', to + 'px' );
		});

		// socials margin-right
		nealSetCustomize( 'socialcopy', 'social-spac-margin-right', function ( to ) {
			$('.site-footer .social-icons a').css( 'margin-right', to + 'px' );
		});

	/* ----------------- Styling Options ----------------- */
		
		var socialBgColor = neal_options.socialcopy['socialcopy-styl-bg-color'],
			socialFontColor = neal_options.socialcopy['socialcopy-styl-font-color'],
			socialBorderRadius = neal_options.socialcopy['social-styl-child-radius'];

		// socials background-color
		nealSetCustomize( 'socialcopy', 'social-styl-bg-color', function ( to ) {
			$('.site-footer .social-icons a').css( 'background-color', to );
			socialBgColor = to;
		});

		// socials font color
		nealSetCustomize( 'socialcopy', 'social-styl-font-color', function ( to ) {
			$('.site-footer .social-icons a').css( 'color', to );
			socialFontColor = to;
		});

		// socials hover background-color
		nealSetCustomize( 'socialcopy', 'social-styl-bg-hover-color', function ( to ) {
			$('.site-footer .social-icons a').on( 'hover', function () {
				$(this).css( 'background-color', to );
			}, function () {
				$(this).css( 'background-color', socialBgColor );
			});
		});

		// socials hover color
		nealSetCustomize( 'socialcopy', 'social-styl-font-hover-color', function ( to ) {
			$('.site-footer .social-icons a').on( 'hover', function () {
				$(this).css( 'color', to );
			}, function () {
				$(this).css( 'color', socialFontColor );
			});
		});

		// socials radius label
		nealSetCustomize( 'socialcopy', 'social-styl-child-radius-label', function ( to ) {
			if ( to ) {
				$('.site-footer .social-icons a').css( 'border-radius', socialBorderRadius + '%' );
			} else {
				$('.site-footer .social-icons a').css( 'border-radius', '' );
			}
		});

		// socials radius
		nealSetCustomize( 'socialcopy', 'social-styl-child-radius', function ( to ) {
			$('.site-footer .social-icons a').css( 'border-radius', to + '%' );
			socialBorderRadius = to;
		});

		// copyright font color
		nealSetCustomize( 'socialcopy', 'copy-styl-font-color', function ( to ) {
			$('.site-footer .site-info p').css( 'color', to );
		});


	/* ----------------- Font Options ----------------- */

		// socials font size
		nealSetCustomize( 'socialcopy', 'social-font-size', function ( to ) {
			$('.site-footer .social-icons a').css( 'font-size', to + 'px' );
		});

		// socials line height
		nealSetCustomize( 'socialcopy', 'social-font-line-height', function ( to ) {
			$('.site-footer .social-icons a').css( 'line-height', to + 'px' );
		});

		// copyright font size
		nealSetCustomize( 'socialcopy', 'copy-font-size', function ( to ) {
			$('.site-footer .site-info p').css( 'font-size', to + 'px' );
		});


/*
***************************************************************
* Custom Functions
***************************************************************
*/

// css border function 4 example: border: 1px solid #444
function nealCssBorder_4( db, name, selector ) {

	var setBorderWidth = 'border-size',
		setBorderStyle = 'border-style',
		setBorderColor = 'border-color';

		// size
		nealSetCustomize( db, name + '-' + setBorderWidth, function ( value ) {
			// change border size value
			neal_options[db][ name + '-' + setBorderWidth ] = value;

			$( selector ).css( 'border-width', value + 'px' );
		});

		// style
		nealSetCustomize( db, name + '-' + setBorderStyle, function ( value ) {
			// change border style value
			neal_options[db][ name + '-' + setBorderStyle ] = value;

			$( selector ).css( 'border-style', value );
		});

		// color
		nealSetCustomize( db, name + '-' + setBorderColor, function ( value ) {
			// change border color value
			neal_options[db][ name + '-' + setBorderColor ] = value;

			$( selector ).css( 'border-color', value );
		});

		// css border object
		var border = {
			'border' : 0
		};

		// border label
		nealSetCustomize( db, name + '-border-label', function ( to ) {
			if ( to ) {

				var border_size = neal_options[db][ name + '-' + setBorderWidth ] + 'px',
					border_style = neal_options[db][ name + '-' + setBorderStyle ],
					border_color = neal_options[db][ name + '-' + setBorderColor ];

				border['border'] = border_size + ' ' + border_style + ' ' + border_color;
				$( selector ).css( border );
			} else {
				$( selector ).css( 'border', 'none' );
			}
		
		});
}

// css border function
function nealCssBorder( db, name, selector, set ) {

	var setCount = set.length;

	for ( var i = 0; i < setCount; i ++ ) {
		var setBorderWidth = 'border-' + set[i] + '-width',
			setBorderStyle = 'border-' + set[i] + '-style',
			setBorderColor = 'border-' + set[i] + '-color';

		// size
		nealSetCustomizeProp( db, name + '-' + 'border-' + set[i] + '-size', setBorderWidth, function ( prop, value ) {
			var propSplit = prop.split( '-' );

			// change border size value
			neal_options[db][name + '-border-' + propSplit[1] + '-size'] = value;

			$( selector ).css( prop, value + 'px' );
		});

		// style
		nealSetCustomizeProp( db, name + '-' + setBorderStyle, setBorderStyle, function ( prop, value ) {
			// change border style value
			neal_options[db][name + '-' + prop] = value;

			$( selector ).css( prop, value );
		});

		// color
		nealSetCustomizeProp( db, name + '-' + setBorderColor, setBorderColor, function ( prop, value ) {
			// change border color value
			neal_options[db][name + '-' + prop] = value;

			$( selector ).css( prop, value );
		});

	}

	// css border object
	var border = {};

	// border label
	nealSetCustomize( db, name + '-border-label', function ( to ) {
		if ( to ) {

			for ( var i = 0; i < setCount; i ++ ) {
				var border_size = neal_options[db][name + '-border-'+ set[i] +'-size'] + 'px',
					border_style = neal_options[db][name + '-border-'+ set[i] +'-style'],
					border_color = neal_options[db][name + '-border-'+ set[i] +'-color'];

					border['border-' + set[i] ] = border_size + ' ' + border_style + ' ' + border_color;
			}

			$( selector ).css( border );
		} else {
			$( selector ).css( 'border', 'none' );
		}
		
	});
}

function nealSetCustomizeProp( db, name, prop, loadFunc ) {
	wp.customize( 'neal_'+ db +'['+ name +']', function( value ) {
		value.bind( function( getValue ) {
			// callback function
			loadFunc( prop, getValue );
		} );
	} );
}

function nealSetCustomize( db, name, loadFunc ) {
	var elName = 'neal_'+ db +'['+ name +']';
	wp.customize( 'neal_'+ db +'['+ name +']', function( value ) {
		value.bind( function( getValue ) {

			// callback function
			loadFunc( getValue );

		} );

	} );
}

});