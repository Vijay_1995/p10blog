jQuery(document).ready(function( $ ) {
    "use strict";

    // header layouts
    $('body').on( 'change', '#customize-control-neal_header-general-header-layout select', function() {
        var headerLayout = $( this ).val();

        if ( headerLayout == 1 ) {
            // left menu-items align
            wp.customize.value('neal_menu[left-menu-items-align]')('left');

             // right menu-items align
            wp.customize.value('neal_menu[right-menu-items-align]')('right');
        }

        if ( headerLayout == 2 ) {

            $('#menu_menu-items-spac-margin-right .custom-slider-value span').text(10);
            wp.customize.value('neal_menu[menu-items-spac-margin-right]')(10);

            // left menu-items align
            wp.customize.value('neal_menu[left-menu-items-align]')('right');

             // right menu-items align
            wp.customize.value('neal_menu[right-menu-items-align]')('left');
        }

        if ( headerLayout == 3 || headerLayout == 4 ) {
            // menu-items align
            wp.customize.value('neal_menu[menu-items-align]')('center');   
        }

        if ( headerLayout == 6 ) {
            // sticky header
            wp.customize.value('neal_header[general-header-sticky]')(false);
        } else {
            // sticky header
            wp.customize.value('neal_header[general-header-sticky]')(true);
        }

        if ( headerLayout == 5 ) {
            // Distance From parent
            $('#menu_submenu-items-spac-margin-top .custom-slider-value span').text(70);
            wp.customize.value('neal_menu[submenu-items-spac-margin-top]')(70);

            // menu-items align
            wp.customize.value('neal_menu[menu-items-align]')('left');  
        }
        
    });

    // footer layouts
    $('body').on( 'change', '#customize-control-neal_footer-general-footer-layout select', function() {
        var footerLayout = $( this ).val();

        if ( footerLayout == 1 ) {
            // general padding-top
            $('#footer_general-spac-padding-top .custom-slider-value span').text(0);
            wp.customize.value('neal_footer[general-spac-padding-top]')(0);
            // carousel nav
            wp.customize.value('neal_footer[instagram-carousel-nav]')(true);
        }

        if ( footerLayout == 2 ) {
            // general padding-top
            $('#footer_general-spac-padding-top .custom-slider-value span').text(0);
            wp.customize.value('neal_footer[general-spac-padding-top]')(0);
            // carousel nav
            wp.customize.value('neal_footer[instagram-carousel-nav]')(false);
        }

        if ( footerLayout == 3 ) {
            // general padding-top
            $('#footer_general-spac-padding-top .custom-slider-value span').text(80);
            wp.customize.value('neal_footer[general-spac-padding-top]')(80);
        }

        if ( footerLayout == 4 ) {
            // general padding-top
            $('#footer_general-spac-padding-top .custom-slider-value span').text(0);
            wp.customize.value('neal_footer[general-spac-padding-top]')(0);
            wp.customize.value('neal_footer[general-footer-fixed]')(false); // turn of fixed header
        }
        
    });

    // Reset button
    var headerAction = $('#customize-header-actions'),
        resetButton = $('<input type="submit" name="neal-customize-reset" id="neal-customize-reset" class="button-secondary button">')
        .attr( 'value', nealCustomizerCustom.reset );

    resetButton.on('click', function ( event ) {
        event.preventDefault();

        var data = {
            wp_customize: 'on',
            action: 'neal_customizer_reset',
            _wpnonce: nealCustomizerCustom.nonce.reset
        };

        var conf = confirm('Are you sure?');

        if ( ! conf ) {
            return;
        }

        resetButton.attr('disabled', 'disabled');

        $.post( ajaxurl, data, function (e) {
            wp.customize.state( 'saved' ).set( true );
            location.reload();
        });
    });

    headerAction.append( resetButton );

    // custom select box
    $('select.neal-fonts-list').select2();

    // custom slider
    $('.custom-slider').on( "input", function () {
        var index = $( this ).index( '.custom-slider' );

        $('.custom-slider-value span').eq( index ).html( $( this ).val() );
    });

    // custom label
	function nealCustomLabel( id, input, main, child = false ) {

        var liId     = '#customize-control-neal_' + id + '-label',
	        label    = $( liId ),
            checkbox = label.find('input'),
            mainEl   = '#sub-accordion-section-section_' + main,
            element  = $( mainEl + ' li[id*="customize-control-neal_' + id + '"]' );


        if ( child ) {
            label.addClass( 'neal-custom-child-label' );
        }

		label.addClass( 'neal-custom-label' ); // add class to label
		if ( input ) {
            checkbox.change( function () {
                // checkbox true
                if ( $(this).is(':checked') ) {

                    if ( !child ) {
                        element.not('.neal-nocheck').stop().show(); // all selector not checkbox show
                    } else {
                        element.stop().show();
                        element.not( liId ).removeClass('neal-nocheck');
                    }

                    label.addClass( 'neal-custom-label-active' ); // add active class to label
                } else {
                    element.not( liId ).stop().hide();

                    if ( child ) {
                        element.not( liId ).addClass('neal-nocheck');
                    }
                    label.removeClass( 'neal-custom-label-active' ); // add active class to label
                }
            });

            if ( checkbox.is(':checked')  ) {
                
                if ( !child ) {
                    element.not('.neal-nocheck').stop().show();
                } else {
                    element.not('.neal-closed').stop().show();
                    element.not( liId ).removeClass('neal-nocheck');
                }
        
                label.addClass( 'neal-custom-label-active' ); // add active class to label
            } else {

                element.not( liId ).stop().hide();
                
                if ( !child ) {
                    element.not( liId ).addClass('neal-closed');
                } else {
                    element.not( liId ).addClass('neal-nocheck');
                }

                label.removeClass( 'neal-custom-label-active' ); // add active class to label
            }

		} else {

			label.addClass( 'neal-custom-label-active' ); // add active class to label
		}

	}

/*
***************************************************************
* #Body
***************************************************************
*/
	
	/* ----------------- General Options ----------------- */

       // main layout label
	   nealCustomLabel( 'body-main-layout', false, 'body_general' );
     

    /* ----------------- Styling Options ----------------- */

        // general label
        nealCustomLabel( 'body-general-styl', false, 'body_styling' );

        // general background-image child-label
        nealCustomLabel( 'body-general-styl-child-bg-img', true, 'body_styling', true );

    /* ----------------- Font Options ----------------- */

        // general label
        nealCustomLabel( 'body-general-font', false, 'body_font' );


/*
***************************************************************
* #Header
***************************************************************
*/
    /* ----------------- Spacing Options ----------------- */

        // general header label
        nealCustomLabel( 'header-general-header', false, 'header_general' );

        // page-header label
        nealCustomLabel( 'header-page-header', false, 'header_general' );

        // social media label
        nealCustomLabel( 'header-social-media', false, 'header_general' );

    /* ----------------- Spacing Options ----------------- */

        // general header label
        nealCustomLabel( 'header-general-header-spac', false, 'header_spacing' );

        // sticky header label
        nealCustomLabel( 'header-sticky-header-spac', false, 'header_spacing' );

        // page-header label
        nealCustomLabel( 'header-page-header-spac', false, 'header_spacing' );

    /* ----------------- Styling Options ----------------- */

        // general header label
        nealCustomLabel( 'header-general-header-styl', false, 'header_styling' );

        // header backround-image child-label
        nealCustomLabel( 'header-general-header-styl-child-bg-img', true, 'header_styling', true );

        // sticky header label
        nealCustomLabel( 'header-sticky-header-styl', false, 'header_styling' );

        // page-header label
        nealCustomLabel( 'header-page-header-styl', false, 'header_styling' );


/*
***************************************************************
* #Logo & Tagline
***************************************************************
*/
    
    /* ----------------- General Options ----------------- */

        // logo image label
        nealCustomLabel( 'logo-img', true, 'logo_general' );

        // logo text label
        nealCustomLabel( 'logo-text', true, 'logo_general' );

        // logo tagline label
        nealCustomLabel( 'logo-tagline', true, 'logo_general' );

    /* ----------------- Spacing Options ----------------- */

        // logo image label
        nealCustomLabel( 'logo-img-spac', false, 'logo_spacing' );

    /* ----------------- Styling Options ----------------- */

        // logo text label
        nealCustomLabel( 'logo-logo-text-styl', false, 'logo_styling' );

        // tagline label
        nealCustomLabel( 'logo-tagline-styl', false, 'logo_styling' );

    /* ----------------- Font Options ----------------- */

        // logo text label
        nealCustomLabel( 'logo-logo-text-font', false, 'logo_font' );

        // tagline label
        nealCustomLabel( 'logo-tagline-font', false, 'logo_font' );



/*
***************************************************************
* #Menu
***************************************************************
*/
    
    /* ----------------- General Options ----------------- */

        // menu items label
        nealCustomLabel( 'menu-menu-items', false, 'menu_general' );

        // left menu items label
        nealCustomLabel( 'menu-left-menu-items', false, 'menu_general' );

        // right menu items label
        nealCustomLabel( 'menu-right-menu-items', false, 'menu_general' );

        // sub-menu items label
        nealCustomLabel( 'menu-submenu-items', false, 'menu_general' );

        // shop icon label
        nealCustomLabel( 'menu-shop', true, 'menu_general' );

        // search icon label
        nealCustomLabel( 'menu-search', true, 'menu_general' );

        // hamburger menu label
        nealCustomLabel( 'menu-ham-menu', true, 'menu_general' );

        // footer menu items label
        nealCustomLabel( 'menu-footer-menu-items', false, 'menu_general' );

    /* ----------------- Spacing Options ----------------- */

        // menu items label
        nealCustomLabel( 'menu-menu-items-spac', false, 'menu_spacing' );

        // submenu items label
        nealCustomLabel( 'menu-submenu-items-spac', false, 'menu_spacing' );

        // footer menu items label
        nealCustomLabel( 'menu-footer-menu-items-spac', false, 'menu_spacing' );

    /* ----------------- Styling Options ----------------- */

        // menu items label
        nealCustomLabel( 'menu-menu-items-styl', false, 'menu_styling' );

        // submenu items label
        nealCustomLabel( 'menu-submenu-items-styl', false, 'menu_styling' );

        // general border child-label
        nealCustomLabel( 'menu-submenu-items-styl-child-border', true, 'menu_styling', true );

        // footer menu label
        nealCustomLabel( 'menu-footer-menu-styl', false, 'menu_styling' );


    /* ----------------- Font Options ----------------- */

        // menu items label
        nealCustomLabel( 'menu-menu-items-font', false, 'menu_font' );

        // submenu items label
        nealCustomLabel( 'menu-submenu-items-font', false, 'menu_font' );

        // mobile menu label
        nealCustomLabel( 'menu-mobile-menu-font', false, 'menu_font' );

        // footer menu items label
        nealCustomLabel( 'menu-footer-menu-items-font', false, 'menu_font' );


/*
***************************************************************
* #Footer
***************************************************************
*/
    
    /* ----------------- General Options ----------------- */

        // general footer label
        nealCustomLabel( 'footer-general-footer', false, 'footer_general' );

        // widget title label
        nealCustomLabel( 'footer-wtitle', false, 'footer_general' );

        // instagram label
        nealCustomLabel( 'footer-instagram', true, 'footer_general' );

    /* ----------------- Spacing Options ----------------- */

        // general label
        nealCustomLabel( 'footer-general-spac', false, 'footer_spacing' );

        // widget title label
        nealCustomLabel( 'footer-wtitle-spac', false, 'footer_spacing' );

        // widget title label
        nealCustomLabel( 'footer-instagram-spac', false, 'footer_spacing' );

    /* ----------------- Styling Options ----------------- */

        // general label
        nealCustomLabel( 'footer-general-styl', false, 'footer_styling' );

        // widgets label
        nealCustomLabel( 'footer-widgets-styl', false, 'footer_styling' );


/*
***************************************************************
* #Sidebar
***************************************************************
*/
    
    /* ----------------- General Options ----------------- */

        // align label
        nealCustomLabel( 'sidebar-sidebar-align', false, 'sidebar_general' );

        // widget title label
        nealCustomLabel( 'sidebar-wtitle', false, 'sidebar_general' );

    /* ----------------- Spacing Options ----------------- */

        // general label
        nealCustomLabel( 'sidebar-general-spac', false, 'sidebar_spacing' );

        // widget title label
        nealCustomLabel( 'sidebar-wtitle-spac', false, 'sidebar_spacing' );

    /* ----------------- Styling Options ----------------- */

        // general label
        nealCustomLabel( 'sidebar-general-styl', false, 'sidebar_styling' );

        // general border child-label
        nealCustomLabel( 'sidebar-general-styl-child-border', true, 'sidebar_styling', true );

        // widget title label
        nealCustomLabel( 'sidebar-wtitle-styl', false, 'sidebar_styling' );

        // about me label
        nealCustomLabel( 'sidebar-about-me-styl', false, 'sidebar_styling' );

    /* ----------------- Font Options ----------------- */

        // widget title label
        nealCustomLabel( 'sidebar-wtitle-font', false, 'sidebar_font' );

        // categories image label
        nealCustomLabel( 'sidebar-categories-image-font', false, 'sidebar_font' );


/*
***************************************************************
* #Product
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // star rating label
        nealCustomLabel( 'product-star-rating', false, 'product_general' );

        // sale label
        nealCustomLabel( 'product-sale', false, 'product_general' );

        // title label
        nealCustomLabel( 'product-title', false, 'product_general' );

        // prices label
        nealCustomLabel( 'product-prices', false, 'product_general' );


    /* ----------------- Spacing Options ----------------- */

        // star rating label
        nealCustomLabel( 'product-star-rating-spac', false, 'product_spacing' );

        // title label
        nealCustomLabel( 'product-title-spac', false, 'product_spacing' );

        // prices label
        nealCustomLabel( 'product-prices-spac', false, 'product_spacing' );


    /* ----------------- Styling Options ----------------- */

        // star rating label
        nealCustomLabel( 'product-star-rating-styl', false, 'product_styling' );

        // sale label
        nealCustomLabel( 'product-sale-styl', false, 'product_styling' );

        // add to cart label
        nealCustomLabel( 'product-add-to-cart-styl', false, 'product_styling' );

        // title label
        nealCustomLabel( 'product-title-styl', false, 'product_styling' );

        // special price label
        nealCustomLabel( 'product-special-price-styl', false, 'product_styling' );

        // old price label
        nealCustomLabel( 'product-old-price-styl', false, 'product_styling' );

    /* ----------------- Font Options ----------------- */

        // star rating label
        nealCustomLabel( 'product-star-rating-font', false, 'product_font' );

        // sale label
        nealCustomLabel( 'product-sale-font', false, 'product_font' );

        // title label
        nealCustomLabel( 'product-title-font', false, 'product_font' );

        // special price label
        nealCustomLabel( 'product-special-price-font', false, 'product_font' );

        // old price label
        nealCustomLabel( 'product-old-price-font', false, 'product_font' );


/*
***************************************************************
* #Blog page
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // layout label
        nealCustomLabel( 'blog-layout', false, 'blog_general' );

        // sidebar label
        nealCustomLabel( 'blog-sidebar', true, 'blog_general' );

        // categories label
        nealCustomLabel( 'blog-categories', true, 'blog_general' );

        // title label
        nealCustomLabel( 'blog-title', true, 'blog_general' );

        // author label
        nealCustomLabel( 'blog-author', true, 'blog_general' );

        // date label
        nealCustomLabel( 'blog-date', true, 'blog_general' );

        // description label
        nealCustomLabel( 'blog-description', true, 'blog_general' );

        // read-more label
        nealCustomLabel( 'blog-read-more', true, 'blog_general' );

    /* ----------------- Spacing Options ----------------- */

        // media label
        nealCustomLabel( 'blog-media-spac', false, 'blog_spacing' );

        // title label
        nealCustomLabel( 'blog-title-spac', false, 'blog_spacing' );

        // meta items label
        nealCustomLabel( 'blog-meta-items-spac', false, 'blog_spacing' );

        // description label
        nealCustomLabel( 'blog-description-spac', false, 'blog_spacing' );

    /* ----------------- Styling Options ----------------- */

        // title label
        nealCustomLabel( 'blog-title-styl', false, 'blog_styling' );

        // meta label
        nealCustomLabel( 'blog-meta-styl', false, 'blog_styling' );

        // categories label
        nealCustomLabel( 'blog-categories-styl', false, 'blog_styling' );

        // description label
        nealCustomLabel( 'blog-desc-styl', false, 'blog_styling' );

    /* ----------------- Font Options ----------------- */

        // title label
        nealCustomLabel( 'blog-title-font', false, 'blog_font' );

        // meta label
        nealCustomLabel( 'blog-meta-font', false, 'blog_font' );

        // categories label
        nealCustomLabel( 'blog-categories-font', false, 'blog_font' );

        // description label
        nealCustomLabel( 'blog-desc-font', false, 'blog_font' );


/*
***************************************************************
* #Blog single
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // general post label
        nealCustomLabel( 'blog_single-general-post', false, 'blog_single_general' );

        // sidebar label
        nealCustomLabel( 'blog_single-sidebar', true, 'blog_single_general' );

        // featured img label
        nealCustomLabel( 'blog_single-featured-img', true, 'blog_single_general' );

        // title label
        nealCustomLabel( 'blog_single-title', false, 'blog_single_general' );

        // breadcrumb label
        nealCustomLabel( 'blog_single-bread', true, 'blog_single_general' );

        // meta label
        nealCustomLabel( 'blog_single-meta', true, 'blog_single_general' );

        // sharing label
        nealCustomLabel( 'blog_single-sharing', true, 'blog_single_general' );

        // subscribe box label
        nealCustomLabel( 'blog_single-subscribe-box', true, 'blog_single_general' );

        // author box label
        nealCustomLabel( 'blog_single-author-box', true, 'blog_single_general' );

        // related posts label
        nealCustomLabel( 'blog_single-related-posts', true, 'blog_single_general' );

        // post navigation label
        nealCustomLabel( 'blog_single-post-navigation', true, 'blog_single_general' );

    /* ----------------- Spacing Options ----------------- */

        // media label
        nealCustomLabel( 'blog_single-media-spac', false, 'blog_single_spacing' );

        // title label
        nealCustomLabel( 'blog_single-title-spac', false, 'blog_single_spacing' );

        // breadcrumb label
        nealCustomLabel( 'blog_single-bread-spac', false, 'blog_single_spacing' );

        // meta label
        nealCustomLabel( 'blog_single-meta-spac', false, 'blog_single_spacing' );

        // meta label
        nealCustomLabel( 'blog_single-caption-image-spac', false, 'blog_single_spacing' );


    /* ----------------- Font Options ----------------- */

        // title label
        nealCustomLabel( 'blog_single-title-font', false, 'blog_single_font' );

        // content label
        nealCustomLabel( 'blog_single-content-font', false, 'blog_single_font' );


/*
***************************************************************
* #Shop page
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // layout label
        nealCustomLabel( 'shop-layout', false, 'shop_general' );

        // sidebar label
        nealCustomLabel( 'shop-sidebar', true, 'shop_general' );

        // sidebar label
        nealCustomLabel( 'shop-title', false, 'shop_general' );

        // toolbar label
        nealCustomLabel( 'shop-tbar', true, 'shop_general' );

        // ordering label
        nealCustomLabel( 'shop-ordering', true, 'shop_general' );

        // result text label
        nealCustomLabel( 'shop-result-t', true, 'shop_general' );

    /* ----------------- Spacing Options ----------------- */

        // toolbar label
        nealCustomLabel( 'shop-tbar-spac', false, 'shop_spacing' );

        // result text label
        nealCustomLabel( 'shop-result-t-spac', false, 'shop_spacing' );

        // ordering label
        nealCustomLabel( 'shop-ordering-spac', false, 'shop_spacing' );

    /* ----------------- Styling Options ----------------- */

        // toolbar label
        nealCustomLabel( 'shop-tbar-styl', false, 'shop_styling' );

        // result text label
        nealCustomLabel( 'shop-result-t-styl', false, 'shop_styling' );

        // ordering label
        nealCustomLabel( 'shop-ordering-styl', false, 'shop_styling' );

        // filter slider label
        nealCustomLabel( 'shop-filter-sld-styl', false, 'shop_styling' );

    /* ----------------- Font Options ----------------- */

        // result text label
        nealCustomLabel( 'shop-result-t-font', false, 'shop_font' );

        // ordering label
        nealCustomLabel( 'shop-ordering-font', false, 'shop_font' );


/*
***************************************************************
* #Shop single
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // sidebar label
        nealCustomLabel( 'shop_single-sidebar', true, 'shop_single_general' );

        // product images label
        nealCustomLabel( 'shop_single-images', false, 'shop_single_general' );

        // product thumbs label
        nealCustomLabel( 'shop_single-thumbs', false, 'shop_single_general' );

        // summary content label
        nealCustomLabel( 'shop_single-summary-content', false, 'shop_single_general' );

        // tabs label
        nealCustomLabel( 'shop_single-tabs', false, 'shop_single_general' );

        // social sharing label
        nealCustomLabel( 'shop_single-social-sharing', false, 'shop_single_general' );

    /* ----------------- Spacing Options ----------------- */

        // title label
        nealCustomLabel( 'shop_single-title-spac', false, 'shop_single_spacing' );

        // product rating label
        nealCustomLabel( 'shop_single-product-rating-spac', false, 'shop_single_spacing' );

        // product prices label
        nealCustomLabel( 'shop_single-product-prices-spac', false, 'shop_single_spacing' );

        // product meta label
        nealCustomLabel( 'shop_single-product-meta-spac', false, 'shop_single_spacing' );

        // tabs tab label
        nealCustomLabel( 'shop_single-tabs-tab-spac', false, 'shop_single_spacing' );

    /* ----------------- Styling Options ----------------- */

        // title label
        nealCustomLabel( 'shop_single-title-styl', false, 'shop_single_styling' );

        // product prices label
        nealCustomLabel( 'shop_single-product-prices-styl', false, 'shop_single_styling' );

        // tabs tab label
        nealCustomLabel( 'shop_single-tabs-tab-styl', false, 'shop_single_styling' );

        // tabs tab active border-bottom child-label
        nealCustomLabel( 'shop_single-tabs-tab-styl-child-border', true, 'shop_single_styling', true )

    /* ----------------- Font Options ----------------- */

        // title label
        nealCustomLabel( 'shop_single-title-font', false, 'shop_single_font' );

        // product prices label
        nealCustomLabel( 'shop_single-product-prices-font', false, 'shop_single_font' );

        // tabs tab label
        nealCustomLabel( 'shop_single-tabs-tab-font', false, 'shop_single_font' );


/*
***************************************************************
* #Homepage
***************************************************************
*/
    /* ----------------- General Options ----------------- */

        // sidebar 1 label
        nealCustomLabel( 'homepage-sidebar-1', false, 'homepage_general' );

        // sidebar 2 label
        nealCustomLabel( 'homepage-sidebar-2', false, 'homepage_general' );

        // sidebar 3 label
        nealCustomLabel( 'homepage-sidebar-3', false, 'homepage_general' );

    /* ----------------- Spacing Options ----------------- */

        // newsletter label
        nealCustomLabel( 'homepage-newsletter-spac', false, 'homepage_spacing' );

    /* ----------------- Styling Options ----------------- */

        // general label
        nealCustomLabel( 'homepage-general-styl', false, 'homepage_styling' );

        // full posts label
        nealCustomLabel( 'homepage-full-posts-styl', false, 'homepage_styling' );

        // newsletter label
        nealCustomLabel( 'homepage-newsletter-styl', false, 'homepage_styling' );

    /* ----------------- Font Options ----------------- */

        // general label
        nealCustomLabel( 'homepage-general-font', false, 'homepage_font' );

        // carousel posts label
        nealCustomLabel( 'homepage-carousel-posts-font', false, 'homepage_font' );

        // full featured post label
        nealCustomLabel( 'homepage-full-featured-post-font', false, 'homepage_font' );

        // masonry posts label
        nealCustomLabel( 'homepage-masonry-posts-font', false, 'homepage_font' );

        // 2 grid posts label
        nealCustomLabel( 'homepage-2-grid-posts-font', false, 'homepage_font' );

        // full posts label
        nealCustomLabel( 'homepage-full-posts-font', false, 'homepage_font' );

        // list posts label
        nealCustomLabel( 'homepage-list-posts-font', false, 'homepage_font' );

        // slider posts label
        nealCustomLabel( 'homepage-slider-posts-font', false, 'homepage_font' );

        // 3 grid posts label
        nealCustomLabel( 'homepage-3-grid-posts-font', false, 'homepage_font' );

/*
***************************************************************
* #Comments
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // display comments on blog label
        nealCustomLabel( 'comments-blog-display', true, 'comments_general' );

        // comment type label
        nealCustomLabel( 'comments-comment-type', false, 'comments_general' );

        // comment texts label
        nealCustomLabel( 'comments-comment-texts', true, 'comments_general' );

        // author image label
        nealCustomLabel( 'comments-author-img', false, 'comments_general' );

    /* ----------------- Spacing Options ----------------- */

        // author image label
        nealCustomLabel( 'comments-author-img-spac', false, 'comments_spacing' );

        // comment content label
        nealCustomLabel( 'comments-comment-content-spac', false, 'comments_spacing' );

    /* ----------------- Styling Options ----------------- */

        // author image label
        nealCustomLabel( 'comments-author-img-styl', false, 'comments_styling' );

        // comment content label
        nealCustomLabel( 'comments-comment-content-styl', false, 'comments_styling' );


/*
***************************************************************
* #Inputs
***************************************************************
*/

    /* ----------------- Spacing Options ----------------- */

        // general label
        nealCustomLabel( 'inputs-general-spac', false, 'inputs_spacing' );

        // submit button label
        nealCustomLabel( 'inputs-submit-button-spac', false, 'inputs_spacing' );

    /* ----------------- Styling Options ----------------- */

        // general label
        nealCustomLabel( 'inputs-general-styl', false, 'inputs_styling' );

        // general border child-label
        nealCustomLabel( 'inputs-general-styl-child-border', true, 'inputs_styling', true );

        // general border-radius child-label
        nealCustomLabel( 'inputs-general-styl-child-radius', true, 'inputs_styling', true );

        // submit button label
        nealCustomLabel( 'inputs-submit-button-styl', false, 'inputs_styling' );

        // submit button border child-label
        nealCustomLabel( 'inputs-submit-button-styl-child-border', true, 'inputs_styling', true );

        // submit button border-radius child-label
        nealCustomLabel( 'inputs-submit-button-styl-child-radius', true, 'inputs_styling', true );

    /* ----------------- Font Options ----------------- */

        // general label
        nealCustomLabel( 'inputs-general-font', false, 'inputs_font' );

        // submit button label
        nealCustomLabel( 'inputs-submit-button-font', false, 'inputs_font' );

/*
***************************************************************
* #Contact page
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // contact form label
        nealCustomLabel( 'contact-form', true, 'contact_general' );

        // contact info label
        nealCustomLabel( 'contact-info', true, 'contact_general' );

        // google map label
        nealCustomLabel( 'contact-google-map', true, 'contact_general' );

    /* ----------------- Spacing Options ----------------- */

        // google map label
        nealCustomLabel( 'contact-google-map-spac', false, 'contact_spacing' );


/*
***************************************************************
* #Pagination
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // navigation label
        nealCustomLabel( 'pagination-navigation', false, 'pagination_general' );

    /* ----------------- Spacing Options ----------------- */

        // navigation label
        nealCustomLabel( 'pagination-navigation-spac', false, 'pagination_spacing' );

    /* ----------------- Styling Options ----------------- */

        // navigation label
        nealCustomLabel( 'pagination-navigation-styl', false, 'pagination_styling' );

    /* ----------------- Font Options ----------------- */

        // navigation label
        nealCustomLabel( 'pagination-navigation-font', false, 'pagination_font' );


/*
***************************************************************
* #Typography
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // subset label
        nealCustomLabel( 'typography-subset', true, 'typography_general' );

    /* ----------------- Font Options ----------------- */

        // paragraph label
        nealCustomLabel( 'typography-paragraph-font', false, 'typography_font' );

        // h1 label
        nealCustomLabel( 'typography-h1-font', false, 'typography_font' );

        // h2 label
        nealCustomLabel( 'typography-h2-font', false, 'typography_font' );

        // h3 label
        nealCustomLabel( 'typography-h3-font', false, 'typography_font' );

        // h4 label
        nealCustomLabel( 'typography-h4-font', false, 'typography_font' );

        // h5 label
        nealCustomLabel( 'typography-h5-font', false, 'typography_font' );

        // h6 label
        nealCustomLabel( 'typography-h6-font', false, 'typography_font' );


/*
***************************************************************
* #Socials & Copyright
***************************************************************
*/

    /* ----------------- General Options ----------------- */

        // socials label
        nealCustomLabel( 'socialcopy-general-social', true, 'socialcopy_general' );

        // copyright label
        nealCustomLabel( 'socialcopy-general-copy', true, 'socialcopy_general' );

    /* ----------------- Spacing Options ----------------- */

        // socials label
        nealCustomLabel( 'socialcopy-social-spac', false, 'socialcopy_spacing' );

    /* ----------------- Styling Options ----------------- */

        // socials label
        nealCustomLabel( 'socialcopy-social-styl', false, 'socialcopy_styling' );

        // socials border-radius child-label
        nealCustomLabel( 'socialcopy-social-styl-child-radius', true, 'socialcopy_styling', true );

        // copyright label
        nealCustomLabel( 'socialcopy-copy-styl', false, 'socialcopy_styling' );

    /* ----------------- Font Options ----------------- */

        // socials label
        nealCustomLabel( 'socialcopy-social-font', false, 'socialcopy_font' );

        // copyright label
        nealCustomLabel( 'socialcopy-copy-font', false, 'socialcopy_font' );

});