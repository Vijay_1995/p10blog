<?php
/**
 * Class to create a Customizer control for displaying information
 *
 * @author   TheSpan
 * @package  neal
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	
	/**
	* Custom Controls
	*/

	// custom checkbox label
	class Neal_Custom_Label_Radio_Control extends WP_Customize_Control { 

		/**
		 * Render the content on the theme customizer page
		 */
		public function render_content() {
			?>
			<span class="customize-control-title custom-control-label">
				<label class="switch"><input type="checkbox" value="<?php echo esc_attr( $this->value()); ?>"
					<?php echo esc_attr( $this->value()) ? 'checked' : null; ?>
					<?php printf( $this->get_link() ); ?>>
					<div class="neal-slider round"></div>
				</label>
				<p><?php echo esc_html( $this->label ); ?></p>
			</span>
		<?php
		}

	}

	// custom label
	class Neal_Custom_Label_Control extends WP_Customize_Control { 

		/**
		 * Render the content on the theme customizer page
		 */
		public function render_content() {
			?>
			<span class="customize-control-title custom-control-label">
				<p><?php echo esc_html( $this->label ); ?></p>
			</span>
		<?php
		}

	}

    // Custom Slider Control
    class Neal_Custom_Slider_Control extends WP_Customize_Control {
		public $type = 'slider';
		public $r_id = '';

        public function render_content(){

        	// remove unnecessary characters
	    	$this->r_id = str_replace( array( 'neal_', ']' ), '', $this->id );
	    	$this->r_id = str_replace( '[', '_', $this->r_id );

            ?>
            <div class="custom-slider-main" id="<?php echo esc_attr($this->r_id); ?>">
	            <label>
	                <span class="customize-control-title"><?php echo esc_html($this->label); ?>
	                	<span class="custom-slider-value"><span><?php echo esc_attr( $this->value()); ?></span><strong></strong></span>
	                </span>
	                <input class="custom-slider" type="range" <?php echo printf( $this->get_link() ); ?> value="<?php echo esc_attr( $this->value()); ?>" <?php echo esc_attr( $this->input_attrs()); ?>" />
	                <div id="<?php echo esc_attr($this->r_id); ?>"></div>
	            </label>
	         </div>
            <?php
        }
    }

    // Google Fonts
	class Neal_Google_Fonts_Control extends WP_Customize_Control {
	    public $type = 'gfonts';

	    public function __construct( $manager, $id, $args = array(), $options = array() ) {
        	parent::__construct( $manager, $id, $args );
   		}
	 
	    public function render_content() {

	    	// remove unnecessary characters
	    	$id = str_replace( array( 'neal_', ']' ), '', $this->id );
	    	$id = str_replace( '[', '_', $id );

	    	$html  = '<label>';
	    		$html .= '<span class="customize-control-title">'. esc_html( $this->label ) .'</span>';
	    		$html .= neal_google_fonts_dropdown( $id, $this->value(), $this->get_link() );
	    	$html .= '</label>';
	        
	        echo ''. $html;

	    }
	}

	// Custom textarea
	class Neal_Textarea_Control extends WP_Customize_Control {
	    public $type = 'textarea';
	 
	    public function render_content() {

	    	$html  = '<label>';
	    		$html .= '<span class="customize-control-title">'. esc_html( $this->label ) .'</span>';
	    		$html .= '<textarea rows="6" '. $this->get_link() .'>'. esc_textarea( $this->value() ) .'</textarea>';
	    	$html .= '</label>';
	        
	        echo ''. $html;
	    }
	}