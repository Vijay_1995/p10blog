<?php
/**
 * Neal Theme Customizer Class
 *
 * @author   TheSpan
 * @package  neal
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Customizer' ) ) :

	/**
	 * The Neal Customizer class
	 */
	class Neal_Customizer {

		/**
		 * Setup class.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
			add_action( 'customize_preview_init',				array( $this, 'customizer_live_preview' ), 10 );
			add_action( 'customize_register',					array( $this, 'register_customizer' ), 20 );
			add_action( 'customize_controls_enqueue_scripts', 	array( $this, 'customizer_enqueue_scripts' ) );

			add_filter( 'body_class',                      		array( $this, 'layout_class' ) );
		}


		/**
		 * Add postMessage support for site title and description for the Theme Customizer along with several other settings.
		 *
		 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
		 * @since  1.0.0
		 */

		public function register_customizer( $wp_customize ) {

			// remove native sections
			$wp_customize->remove_section( 'themes' );
			$wp_customize->remove_section( 'colors' );
			$wp_customize->remove_section( 'static_front_page' );
			$wp_customize->remove_section( 'background_image' );


			// remove native controls
			$wp_customize->remove_control("custom_logo");

			// Custom Controls
			require_once get_template_directory() . '/inc/customizer/neal-customizer-controls.php';
			
			// sanitize label
			function neal_sanitize_label( $input ) {
				return wp_filter_nohtml_kses( $input );
			}

			// sanitize number inputs / sliders
			function neal_sanitize_number( $input ) {
				return floatval( $input );
			}

			// sanitize string
			function neal_sanitize_string( $input ) {
				return wp_kses( $input, array(
					   	'strong' => array(),
					   	'i' => array(
					   		'class'	=> array()
					   		),
						'em' => array(),
						'br' => array(),
						'a'	 => array(
							    	'href'   => array(),
									'title'  => array(),
									'target' => array()
								)
						) );
			}

			// sanitize checkbox
			function neal_sanitize_checkbox( $input ) {
				return ( ( isset( $input ) && true == $input ) ? true : false );
			}

			// sanitize radio
			function neal_sanitize_radio( $input, $setting ) {
				$input = sanitize_key( $input );
			    $choices = $setting->manager->get_control( $setting->id )->choices;

			    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
			}

			// sanitize select
			function neal_sanitize_select( $input, $setting ) {
				$input = wp_filter_nohtml_kses( $input );
			    
			    return ( isset( $input ) ? $input : $setting->default );
			}

			/**
			* Adding all panels
			*/

			// body
			$wp_customize->add_panel( 'panel_body', array(
				'title'		=> esc_html__( 'Body', 'neal' ),
				'priority'	=> 1
			) );

			// header
			$wp_customize->add_panel( 'panel_header', array(
				'title'		=> esc_html__( 'Header', 'neal' ),
				'priority'  => 2
			) );

			// logo & tagline
			$wp_customize->add_panel( 'panel_logo', array(
				'title'		=> esc_html__( 'Logo & Tagline', 'neal' ),
				'priority'  => 3
			) );

			// menu
			$wp_customize->add_panel( 'panel_menu', array(
				'title'		=> esc_html__( 'Menus', 'neal' ),
				'priority'  => 4
			) );

			// footer
			$wp_customize->add_panel( 'panel_footer', array(
				'title'		=> esc_html__( 'Footer', 'neal' ),
				'priority'  => 5
			) );

			// sidebar
			$wp_customize->add_panel( 'panel_sidebar', array(
				'title'		=> esc_html__( 'Sidebar', 'neal' ),
				'priority'  => 6
			) );

			// product
			$wp_customize->add_panel( 'panel_product', array(
				'title'		=> esc_html__( 'Product', 'neal' ),
				'priority'  => 7
			) );

			// blog
			$wp_customize->add_panel( 'panel_blog', array(
				'title'     => esc_html__( 'Blog page', 'neal' ),
				'priority'	=> 8
			) );

			// blog single
			$wp_customize->add_panel( 'panel_blog_single', array(
				'title'     => esc_html__( 'Blog single', 'neal' ),
				'priority'	=> 9
			) );

			// shop
			$wp_customize->add_panel( 'panel_shop', array(
				'title'		=> esc_html__( 'Shop page', 'neal' ),
				'priority'  => 10
			) );

			// shop single
			$wp_customize->add_panel( 'panel_shop_single', array(
				'title'     => esc_html__( 'Shop single', 'neal' ),
				'priority'  => 11
			) );

			// homepage
			$wp_customize->add_panel( 'panel_homepage', array(
				'title'		=> esc_html__( 'Homepage', 'neal' ),
				'priority'  => 12
			) );

			// comments
			$wp_customize->add_panel( 'panel_comments', array(
				'title'     => esc_html__( 'Comments', 'neal' ),
				'priority'  => 13
			) );

			// inputs
			$wp_customize->add_panel( 'panel_inputs', array(
				'title'     => esc_html__( 'Inputs', 'neal' ),
				'priority'  => 14
			) );

			// contact
			$wp_customize->add_panel( 'panel_contact', array(
				'title'     => esc_html__( 'Contact page', 'neal' ),
				'priority'	=> 15
			) );

			// pagination
			$wp_customize->add_panel( 'panel_pagination', array(
				'title'		=> esc_html__( 'Pagination', 'neal' ),
				'priority'  => 16
			) );

			// typography
			$wp_customize->add_panel( 'panel_typography', array(
				'title'		=> esc_html__( 'Typography', 'neal' ),
				'priority'  => 17
			) );

			// socialcopy
			$wp_customize->add_panel( 'panel_socialcopy', array(
				'title'		=> esc_html__( 'Socials & Copyright', 'neal' ),
				'priority'  => 18
			) );


			/**
			* Adding all sections
			*/

			// body general options section
			$wp_customize->add_section( 'section_body_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_body',
				'priority'  => 1
			) );

			// body spacing options section
			$wp_customize->add_section( 'section_body_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_body',
				'priority'  => 2
			) );

			// body styling options section
			$wp_customize->add_section( 'section_body_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_body',
				'priority'  => 3
			) );

			// body font options section
			$wp_customize->add_section( 'section_body_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_body',
				'priority'  => 4
			) );

			// header general options section
			$wp_customize->add_section( 'section_header_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_header',
				'priority'  => 1
			) );

			// header spacing options section
			$wp_customize->add_section( 'section_header_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_header',
				'priority'  => 2
			) );

			// header styling options section
			$wp_customize->add_section( 'section_header_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_header',
				'priority'  => 3
			) );

			// header font options section
			$wp_customize->add_section( 'section_header_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_header',
				'priority'  => 4
			) );

			// logo general options section
			$wp_customize->add_section( 'section_logo_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_logo',
				'priority'  => 1
			) );

			// logo spacing options section
			$wp_customize->add_section( 'section_logo_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_logo',
				'priority'  => 2
			) );

			// logo styling options section
			$wp_customize->add_section( 'section_logo_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_logo',
				'priority'  => 3
			) );

			// logo font options section
			$wp_customize->add_section( 'section_logo_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_logo',
				'priority'  => 4
			) );

			// menu general options section
			$wp_customize->add_section( 'section_menu_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_menu',
				'priority'  => 1
			) );

			// menu spacing options section
			$wp_customize->add_section( 'section_menu_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_menu',
				'priority'  => 2
			) );

			// menu styling options section
			$wp_customize->add_section( 'section_menu_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_menu',
				'priority'	=> 3
			) );

			// menu font options section
			$wp_customize->add_section( 'section_menu_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_menu',
				'priority'	=> 4
			) );

			// footer general options section
			$wp_customize->add_section( 'section_footer_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_footer',
				'priority'  => 1
			) );

			// footer spacing options section
			$wp_customize->add_section( 'section_footer_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_footer',
				'priority'  => 2
			) );

			// footer styling options section
			$wp_customize->add_section( 'section_footer_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_footer',
				'priority'  => 3
			) );

			// footer font options section
			$wp_customize->add_section( 'section_footer_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_footer',
				'priority'  => 4
			) );

			// sidebar general options section
			$wp_customize->add_section( 'section_sidebar_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_sidebar',
				'priority'  => 1
			) );

			// sidebar spacing options section
			$wp_customize->add_section( 'section_sidebar_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_sidebar',
				'priority'  => 2
			) );

			// sidebar styling options section
			$wp_customize->add_section( 'section_sidebar_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_sidebar',
				'priority'  => 3
			) );

			// sidebar font options section
			$wp_customize->add_section( 'section_sidebar_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_sidebar',
				'priority'  => 4
			) );

			// product general options section
			$wp_customize->add_section( 'section_product_general' , array(
				'title'		=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_product',
				'priority'  => 1
			) );

			// product spacing options section
			$wp_customize->add_section( 'section_product_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_product',
				'priority'  => 2
			) );

			// product styling options section
			$wp_customize->add_section( 'section_product_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_product',
				'priority'	=> 3
			) );

			// product font options section
			$wp_customize->add_section( 'section_product_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_product',
				'priority'  => 4
			) );

			// blog general options section
			$wp_customize->add_section( 'section_blog_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_blog',
				'priority'	=> 1
			) );

			// blog spacing options section
			$wp_customize->add_section( 'section_blog_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_blog',
				'priority'	=> 2
			) );

			// blog styling options section
			$wp_customize->add_section( 'section_blog_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_blog',
				'priority'	=> 3
			) );

			// blog font options section
			$wp_customize->add_section( 'section_blog_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_blog',
				'priority'  => 4
			) );

			// blog single general options section
			$wp_customize->add_section( 'section_blog_single_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_blog_single',
				'priority'	=> 1
			) );

			// blog single spacing options section
			$wp_customize->add_section( 'section_blog_single_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_blog_single',
				'priority'	=> 2
			) );

			// blog single styling options section
			$wp_customize->add_section( 'section_blog_single_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel'		=> 'panel_blog_single',
				'priority'  => 3
			) );

			// blog single font options section
			$wp_customize->add_section( 'section_blog_single_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_blog_single',
				'priority'	=> 4
			) );

			// shop general options section
			$wp_customize->add_section( 'section_shop_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_shop',
				'priority'	=> 1
			) );

			// shop spacing options section
			$wp_customize->add_section( 'section_shop_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_shop',
				'priority'	=> 2
			) );

			// shop styling options section
			$wp_customize->add_section( 'section_shop_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_shop',
				'priority'	=> 3
			) );

			// shop font options section
			$wp_customize->add_section( 'section_shop_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_shop',
				'priority'	=> 4
			) );

			// shop single general options section
			$wp_customize->add_section( 'section_shop_single_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_shop_single',
				'priority'	=> 1
			) );

			// shop single spacing options section
			$wp_customize->add_section( 'section_shop_single_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_shop_single',
				'priority'	=> 2
			) );

			// shop single styling options section
			$wp_customize->add_section( 'section_shop_single_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_shop_single',
				'priority'	=> 3
			) );

			// shop single font options section
			$wp_customize->add_section( 'section_shop_single_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_shop_single',
				'priority'	=> 4
			) );

			// homepage general options section
			$wp_customize->add_section( 'section_homepage_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_homepage',
				'priority'	=> 1
			) );

			// homepage spacing options section
			$wp_customize->add_section( 'section_homepage_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_homepage',
				'priority'	=> 2
			) );

			// homepage styling options section
			$wp_customize->add_section( 'section_homepage_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_homepage',
				'priority'	=> 3
			) );

			// homepage font options section
			$wp_customize->add_section( 'section_homepage_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_homepage',
				'priority'	=> 4
			) );

			// comments general options section
			$wp_customize->add_section( 'section_comments_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_comments',
				'priority'	=> 1
			) );

			// comments spacing options section
			$wp_customize->add_section( 'section_comments_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_comments',
				'priority'	=> 2
			) );

			// comments styling options section
			$wp_customize->add_section( 'section_comments_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_comments',
				'priority'  => 3
			) );

			// comments font options section
			$wp_customize->add_section( 'section_comments_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_comments',
				'priority'	=> 4
			) );

			// inputs general options section
			$wp_customize->add_section( 'section_inputs_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_inputs',
				'priority'	=> 1
			) );

			// inputs spacing options section
			$wp_customize->add_section( 'section_inputs_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_inputs',
				'priority'	=> 2
			) );

			// inputs styling options section
			$wp_customize->add_section( 'section_inputs_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_inputs',
				'priority'	=> 3
			) );

			// inputs font options section
			$wp_customize->add_section( 'section_inputs_font' , array(
				'title'		=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_inputs',
				'priority'  => 4
			) );

			// contact general options section
			$wp_customize->add_section( 'section_contact_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_contact',
				'priority'	=> 1
			) );

			// contact spacing options section
			$wp_customize->add_section( 'section_contact_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_contact',
				'priority'	=> 2
			) );

			// contact styling options section
			$wp_customize->add_section( 'section_contact_styling' , array(
				'title'		=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_contact',
				'priority'  => 3
			) );

			// contact font options section
			$wp_customize->add_section( 'section_contact_font' , array(
				'title' => esc_html__( 'Font Options', 'neal' ),
				'panel' => 'panel_contact',
				'priority'       => 4
			) );

			// pagination general options section
			$wp_customize->add_section( 'section_pagination_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_pagination',
				'priority'	=> 1
			) );

			// pagination spacing options section
			$wp_customize->add_section( 'section_pagination_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_pagination',
				'priority'	=> 2
			) );

			// pagination styling options section
			$wp_customize->add_section( 'section_pagination_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_pagination',
				'priority'	=> 3
			) );

			// pagination font options section
			$wp_customize->add_section( 'section_pagination_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_pagination',
				'priority'	=> 4
			) );

			// typography general options section
			$wp_customize->add_section( 'section_typography_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_typography',
				'priority'	=> 1
			) );

			// typography spacing options section
			$wp_customize->add_section( 'section_typography_spacing' , array(
				'title' 	=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_typography',
				'priority'	=> 2
			) );

			// typography styling options section
			$wp_customize->add_section( 'section_typography_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_typography',
				'priority'	=> 3
			) );

			// typography font options section
			$wp_customize->add_section( 'section_typography_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_typography',
				'priority'	=> 4
			) );

			// socialcopy general options section
			$wp_customize->add_section( 'section_socialcopy_general' , array(
				'title' 	=> esc_html__( 'General Options', 'neal' ),
				'panel' 	=> 'panel_socialcopy',
				'priority'	=> 1
			) );

			// socialcopy spacing options section
			$wp_customize->add_section( 'section_socialcopy_spacing' , array(
				'title'		=> esc_html__( 'Spacing Options', 'neal' ),
				'panel' 	=> 'panel_socialcopy',
				'priority'  => 2
			) );

			// socialcopy styling options section
			$wp_customize->add_section( 'section_socialcopy_styling' , array(
				'title' 	=> esc_html__( 'Styling Options', 'neal' ),
				'panel' 	=> 'panel_socialcopy',
				'priority'	=> 3
			) );

			// socialcopy font options section
			$wp_customize->add_section( 'section_socialcopy_font' , array(
				'title' 	=> esc_html__( 'Font Options', 'neal' ),
				'panel' 	=> 'panel_socialcopy',
				'priority'	=> 4
			) );


			/**
			* Adding Selective refresh
			*/

			// if selective refresh is available.
   	 		if ( isset( $wp_customize->selective_refresh ) ) {


   	 			// logo img shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_logo[img]', array(
				    	'selector'	=> '.logo',
				    	'render_callback' => 'neal_customize_partial',
				) );

				// logo text shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_logo[text]', array(
				    	'selector'	=> '.site-title a',
				    	'render_callback' => array( $this, 'get_logo_text' ),
				) );

   	 			// tagline text shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_logo[tagline]', array(
				    	'selector' => '.site-description',
				    	'render_callback' => array( $this, 'get_logo_tagline' ),
				) );

				// footer social icons shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_socialcopy[general-social-label]', array(
				    	'selector'	=> '.site-footer .social-icons',
				    	'render_callback' => 'neal_customize_partial',
				) );

				// footer copyright shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_socialcopy[general-copy-text]', array(
				    	'selector'	=> '.site-footer .site-info p',
				    	'render_callback' => array( $this, 'get_copyright_text' ),
				) );

				// pagination shortcut
				$wp_customize->selective_refresh->add_partial( 'neal_pagination[navigation-label]', array(
				    	'selector'	=> '.pagination',
				    	'render_callback' => 'neal_customize_partial',
				) );
   	 		}


			/*
			***************************************************************
			* #Body
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// layout label
				$wp_customize->add_setting( 'neal_body[main-layout-label]', array(
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_body[main-layout-label]', array(
							'label'    => esc_html__( 'Layout', 'neal' ),
							'section'  => 'section_body_general',
							'type'	   => 'label',
							'priority' => 1
						)
				) );

				// layout max-width
				$wp_customize->add_setting('neal_body[layout-max-width]', array(
					'default' 	=> 1160,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_body[layout-max-width]', array(
							'settings'		=> 'neal_body[layout-max-width]',
					        'label' 		=> esc_html__( 'Max-Width', 'neal' ),
					        'section' 		=> 'section_body_general',
					        'input_attrs' 	=> array(
								                'min' => 800,
								                'max' => 2000
								               ),
					        'priority' 		=> 2
					    )
				) );

				// layout width
				$wp_customize->add_setting('neal_body[layout-width]', array(
					'default' 	=> 'full',
					'type'	  	=> 'option',
					'transport' => 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_body[layout-width]', array(
					'settings'	=> 'neal_body[layout-width]',
					'label' 	=> esc_html__( 'Layout Width', 'neal' ),
					'section' 	=> 'section_body_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'full'		=> esc_html__( 'Full', 'neal' ),
			                		'boxed' 	=> esc_html__( 'Boxed', 'neal' ),
			            			),
					'priority' => 3
				) );

				// layout align
				$wp_customize->add_setting('neal_body[layout-align]', array(
					'default' 	=> 'center',
					'type'	  	=> 'option',
					'transport' => 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_body[layout-align]', array(
					'settings'	=> 'neal_body[layout-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_body_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
			                		'center' 	=> esc_html__( 'Center', 'neal' ),
			                		'right' 	=> esc_html__( 'Right', 'neal' )
			            			),
					'priority' => 4
				) );

			/* -----------------{ Styling Options }----------------- */

				// general
				$wp_customize->add_setting( 'neal_body[general-styl-label]', array(
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_body[general-styl-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_body_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_body[general-styl-bg-color]', array(
						'default'	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-bg-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Background Color', 'neal' ),
							'settings'	=> 'neal_body[general-styl-bg-color]',
							'priority'	=> 2
						) 
				) );

				// background color 2
				$wp_customize->add_setting('neal_body[general-styl-bg-color-2]', array(
						'default'	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-bg-color-2]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Background Color 2', 'neal' ),
							'settings'	=> 'neal_body[general-styl-bg-color-2]',
							'priority'	=> 3
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_body[general-styl-font-color]', array(
						'default'	=> '#000',
						'type'		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-font-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Text Color', 'neal' ),
							'settings'  => 'neal_body[general-styl-font-color]',
							'priority' 	=> 4
						) 
				) );

				// link color
				$wp_customize->add_setting('neal_body[general-styl-link-color]', array(
						'default' 	=> '#000',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[link-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Link Color', 'neal' ),
							'settings'  => 'neal_body[general-styl-link-color]',
							'priority'	=> 5
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_body[general-styl-link-hover-color]', array(
						'default' 	=> '#c0392b',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-link-hover-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Link Hover Color', 'neal' ),
							'settings'  => 'neal_body[general-styl-link-hover-color]',
							'priority'	=> 6
						) 
				) );

				// ::selection background color
				$wp_customize->add_setting('neal_body[general-styl-selection-bg-color]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-selection-bg-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Selection Bg color', 'neal' ),
							'settings'  => 'neal_body[general-styl-selection-bg-color]',
							'priority'	=> 7
						) 	
				) );

				// ::selection font color
				$wp_customize->add_setting('neal_body[general-styl-selection-font-color]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_body[general-styl-selection-font-color]', array(
							'section'	=> 'section_body_styling',
							'label'    	=> esc_html__( 'Selection Font color', 'neal' ),
							'settings'  => 'neal_body[general-styl-selection-font-color]',
							'priority' 	=> 8
						) 	
				) );

				// background image label
				$wp_customize->add_setting( 'neal_body[general-styl-child-bg-img-label]', array(
						'default' 	=> false,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_body[general-styl-child-bg-img-label]', array(
							'label'    => esc_html__( 'Background Image', 'neal' ),
							'section'  => 'section_body_styling',
							'priority' => 9
						)
				) );

				// background image
				$wp_customize->add_setting('neal_body[general-styl-child-bg-img]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'esc_url_raw'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_body[general-styl-child-bg-img]', array(
							'section'	=> 'section_body_styling',
							'settings'  => 'neal_body[general-styl-child-bg-img]',
							'priority'	=> 10
						) 
				) );

				// background size
				$wp_customize->add_setting('neal_body[general-styl-child-bg-img-size]', array(
						'default' 	=> 'cover',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control('neal_body[general-styl-child-bg-img-size]', array(
						'settings'	=> 'neal_body[general-styl-child-bg-img-size]',
						'label' 	=> esc_html__( 'Background Size', 'neal' ),
						'section' 	=> 'section_body_styling',
						'type' 		=> 'radio',
						'choices' 	=> array(
								   		'pattern'	=> esc_html__( 'Pattern', 'neal' ),
										'cover' 	=> esc_html__( 'Cover', 'neal' )
								  		),
						'priority'	=> 11
				) );

				// background attachment
				$wp_customize->add_setting('neal_body[general-styl-child-bg-img-attchmnt]', array(
						'default' 	=> 'fixed',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control('neal_body[general-styl-child-bg-img-attchmnt]', array(
						'settings'	=> 'neal_body[general-styl-child-bg-img-attchmnt]',
					    'label' 	=> esc_html__( 'Background Attachment', 'neal' ),
					   	'section' 	=> 'section_body_styling',
					    'type' 		=> 'radio',
					    'choices' 	=> array(
									   	'scroll'	=> esc_html__( 'Scroll', 'neal' ),
									    'fixed' 	=> esc_html__( 'Fixed', 'neal')
									    ),
					    'priority'	=> 12
				) );	


			/* -----------------{ Font Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_body[general-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_body[general-font-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_body_font',
							'priority' => 1
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_body[general-font-family]',	array(
							'default' 	=> 'Montserrat',
							'type' 		=> 'option',
							'transport'	=> 'refresh',
							'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_body[general-font-family]', array(
							'section'  => 'section_body_font',
							'label'    => esc_html__( 'Family', 'neal' ),
							'priority' => 2
						)
				) );

				// font size
				$wp_customize->add_setting('neal_body[general-font-size]', array(
						'default'	=> 16,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_body[general-font-size]', array(
							'settings'		=> 'neal_body[general-font-size]',
							'label'    		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_body_font',
					        'input_attrs' 	=> array(
								               	'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 3
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_body[general-font-weight]', array(
						'default'	=> 400,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_body[general-font-weight]',array(
							'settings'		=> 'neal_body[general-font-weight]',
							'label'    		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_body_font',
					        'input_attrs' 	=> array(
								               	'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 4
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_body[general-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_body[general-font-letter-spacing]', array(
							'settings'		=> 'neal_body[general-font-letter-spacing]',
							'label'    		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_body_font',
					        'input_attrs' 	=> array(
								               	'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 5
					    )
				) );

			/*
			***************************************************************
			* #Header
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// general header label
				$wp_customize->add_setting( 'neal_header[general-header-label]', array(
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[general-header-label]', array(
							'label'    => esc_html__( 'General Header', 'neal' ),
							'section'  => 'section_header_general',
							'priority' => 1
						)
				) );

				// header layouts
				$wp_customize->add_setting( 'neal_header[general-header-layout]', array(
					'default' 	=> 1,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control( 'neal_header[general-header-layout]', array(
					'settings'	=> 'neal_header[general-header-layout]',
					'label' 	=> esc_html__( 'Header Layouts', 'neal' ),
					'section' 	=> 'section_header_general',
					'type' 		=> 'select',
					'choices' 	=> array(
								   	1 	=> esc_html__( 'Layout 1', 'neal' ),
									2 	=> esc_html__( 'Layout 2', 'neal' ),
									3 	=> esc_html__( 'Layout 3', 'neal' ),
									4 	=> esc_html__( 'Layout 4', 'neal' ),
									5 	=> esc_html__( 'Layout 5', 'neal' ),
									6 	=> esc_html__( 'Layout 6', 'neal' ),
								   ),
					'priority'	=> 2
				) );

				// sticky header
				$wp_customize->add_setting( 'neal_header[general-header-sticky]', array(
					'default' 		=> true,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_header[general-header-sticky]', array(
					'label'		=> esc_html__( 'Sticky Header', 'neal' ),
					'section'   => 'section_header_general',
					'settings'  => 'neal_header[general-header-sticky]',
					'type'		=> 'checkbox',
					'priority' 	=> 3
				) );

				// page-header label
				$wp_customize->add_setting( 'neal_header[page-header-label]', array(
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[page-header-label]', array(
							'label'    => esc_html__( 'Page Header', 'neal' ),
							'section'  => 'section_header_general',
							'priority' => 4
						)
				) );

				// breadcrumbs
				$wp_customize->add_setting( 'neal_header[page-header-bread]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_header[page-header-bread]', array(
					'label'		=> esc_html__( 'Breadcrumbs', 'neal' ),
					'section'   => 'section_header_general',
					'settings'  => 'neal_header[page-header-bread]',
					'type'		=> 'checkbox',
					'priority' 	=> 5
				) );

				// social media label
				$wp_customize->add_setting( 'neal_header[social-media-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_header[social-media-label]', array(
							'label'		=> esc_html__( 'Social Media', 'neal' ),
							'section'  	=> 'section_header_general',
							'priority'	=> 6
						)
				) );

			/* -----------------{ Spacing Options }----------------- */

				// general header label
				$wp_customize->add_setting( 'neal_header[general-header-spac-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[general-header-spac-label]', array(
							'label'    => esc_html__( 'General Header', 'neal' ),
							'section'  => 'section_header_spacing',
							'priority' => 1
						)
				) );

				// height
				$wp_customize->add_setting( 'neal_header[general-header-spac-height]', array(
					'default' 	=> 120,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_header[general-header-spac-height]', array(
							'settings'		=> 'neal_header[general-header-spac-height]',
					        'label' 		=> esc_html__( 'Height', 'neal' ),
					        'section' 		=> 'section_header_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 600
								               ),
					        'priority'      => 2
					    )
				) );

				// sticky header label
				$wp_customize->add_setting( 'neal_header[sticky-header-spac-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[sticky-header-spac-label]', array(
							'label'    => esc_html__( 'Sticky Header', 'neal' ),
							'section'  => 'section_header_spacing',
							'priority' => 3
						)
				) );

				// height
				$wp_customize->add_setting( 'neal_header[sticky-header-spac-height]', array(
					'default' 	=> 80,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_header[sticky-header-spac-height]', array(
							'settings'		=> 'neal_header[sticky-header-spac-height]',
					        'label' 		=> esc_html__( 'Height', 'neal' ),
					        'section' 		=> 'section_header_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 600
								               ),
					        'priority'      => 4
					    )
				) );

				// page-header label
				$wp_customize->add_setting( 'neal_header[page-header-spac-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[page-header-spac-label]', array(
							'label'    => esc_html__( 'Page Header', 'neal' ),
							'section'  => 'section_header_spacing',
							'priority' => 5
						)
				) );

				// padding top
				$wp_customize->add_setting('neal_header[page-header-spac-padding-top]', array(
					'default' 	=> 60,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_header[page-header-spac-padding-top]', array(
							'settings'		=> 'neal_header[page-header-spac-padding-top]',
					        'label' 		=> esc_html__( 'Padding Top', 'neal' ),
					        'section' 		=> 'section_header_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'      => 6
					    )
				) );

				// padding bottom
				$wp_customize->add_setting('neal_header[page-header-spac-padding-bottom]',array(
					'default' 	=> 0,
					'type'	 	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_header[page-header-spac-padding-bottom]', array(
							'settings'		=> 'neal_header[page-header-spac-padding-bottom]',
					        'label' 		=> esc_html__( 'Padding Bottom', 'neal' ),
					        'section' 		=> 'section_header_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 7
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// general header label
				$wp_customize->add_setting( 'neal_header[general-header-styl-label]', array(
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[general-header-styl-label]', array(
							'label'    => esc_html__( 'General Header', 'neal' ),
							'section'  => 'section_header_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_header[general-header-styl-bg-color]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[general-header-styl-bg-color]', array(
							'section'	=> 'section_header_styling',
							'label'    	=> esc_html__( 'Background Color', 'neal' ),
							'settings'  => 'neal_header[general-header-styl-bg-color]',
							'priority'	=> 2
						) 
				) );

				// background color 2
				$wp_customize->add_setting('neal_header[general-header-styl-bg-color-2]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[general-header-styl-bg-color-2]', array(
							'section'	=> 'section_header_styling',
							'label'    	=> esc_html__( 'Background Color 2', 'neal' ),
							'settings'  => 'neal_header[general-header-styl-bg-color-2]',
							'priority'	=> 3
						) 
				) );

				// background image label
				$wp_customize->add_setting( 'neal_header[general-header-styl-child-bg-img-label]', array( 
						'default' 	=> false,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_header[general-header-styl-child-bg-img-label]', array(
							'label'    => esc_html__( 'Background Image', 'neal' ),
							'section'  => 'section_header_styling',
							'priority' => 4
						)
				) );

				// background image
				$wp_customize->add_setting('neal_header[general-header-styl-child-bg-img]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'esc_url_raw',
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_header[general-header-styl-child-bg-img]', array(
							'section'	=> 'section_header_styling',
							'settings'  => 'neal_header[general-header-styl-child-bg-img]',
							'priority' 	=> 5
						) 
				) );

				// background size
				$wp_customize->add_setting('neal_header[general-header-styl-child-bg-img-size]', array(
						'default' 	=> 'cover',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control('neal_header[general-header-styl-child-bg-img-size]', array(
						'settings'	=> 'neal_header[general-header-styl-child-bg-img-size]',
						'label' 	=> esc_html__( 'Background Size', 'neal' ),
						'section' 	=> 'section_header_styling',
						'type' 		=> 'radio',
						'choices' 	=> array(
									   	'pattern' 	=> esc_html__( 'Pattern', 'neal' ),
										'cover' 	=> esc_html__( 'Cover', 'neal' )
									   ),
						'priority'	=> 6
				) );

				// background attachment
				$wp_customize->add_setting('neal_header[general-header-styl-child-bg-img-attchmnt]', array(
						'default' 	=> 'fixed',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control('neal_header[general-header-styl-child-bg-img-attchmnt]', array(
						'settings'	=> 'neal_header[general-header-styl-child-bg-img-attchmnt]',
					    'label' 	=> esc_html__( 'Background Attachment', 'neal' ),
					    'section' 	=> 'section_header_styling',
					    'type' 		=> 'radio',
					    'choices' 	=> array(
									   	'scroll'	=> esc_html__( 'Scroll', 'neal' ),
									    'fixed' 	=> esc_html__( 'Fixed', 'neal' )
									    ),
					    'priority'	=> 7
				) );

				if ( neal_get_option( 'header_general-header-sticky' ) ) {

					// sticky header label
					$wp_customize->add_setting( 'neal_header[sticky-header-styl-label]', array(
					    'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_header[sticky-header-styl-label]', array(
								'label'    => esc_html__( 'Sticky Header', 'neal' ),
								'section'  => 'section_header_styling',
								'priority' => 8
							)
					) );

					// background color
					$wp_customize->add_setting( 'neal_header[sticky-header-styl-bg-color]', array(
						'default' 	=> '#000',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
					) );

					$wp_customize->add_control( 
						new WP_Customize_Color_Control( $wp_customize, 'neal_header[sticky-header-styl-bg-color]', array(
								'section'	=> 'section_header_styling',
								'label'    	=> esc_html__( 'Background Color', 'neal' ),
								'settings'  => 'neal_header[sticky-header-styl-bg-color]',
								'priority'	=> 9
							) 
					) );

					// link color
					$wp_customize->add_setting( 'neal_header[sticky-header-styl-link-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
					) );

					$wp_customize->add_control( 
						new WP_Customize_Color_Control( $wp_customize, 'neal_header[sticky-header-styl-link-color]', array(
								'section'	=> 'section_header_styling',
								'label'    	=> esc_html__( 'Link Color', 'neal' ),
								'settings'  => 'neal_header[sticky-header-styl-link-color]',
								'priority'	=> 10
							) 
					) );

					// link hover color
					$wp_customize->add_setting( 'neal_header[sticky-header-styl-link-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
					) );

					$wp_customize->add_control( 
						new WP_Customize_Color_Control( $wp_customize, 'neal_header[sticky-header-styl-link-hover-color]', array(
								'section'	=> 'section_header_styling',
								'label'    	=> esc_html__( 'Link Hover Color', 'neal' ),
								'settings'  => 'neal_header[sticky-header-styl-link-hover-color]',
								'priority'	=> 11
							) 
					) );
				}

				// page-header label
				$wp_customize->add_setting( 'neal_header[page-header-styl-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_header[page-header-styl-label]', array(
							'label'    => esc_html__( 'Page Header', 'neal' ),
							'section'  => 'section_header_styling',
							'priority' => 12
						)
				) );

				// background color
				$wp_customize->add_setting('neal_header[page-header-styl-bg-color]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[page-header-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_header_styling',
							'settings'  => 'neal_header[page-header-styl-bg-color]',
							'priority'	=> 13
						) 
				) );

				// title color
				$wp_customize->add_setting('neal_header[page-header-styl-title-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[page-header-styl-title-color]', array(
							'label'     => esc_html__( 'Title Color', 'neal' ),
							'section'   => 'section_header_styling',
							'settings'  => 'neal_header[page-header-styl-title-color]',
							'priority'	=> 14
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_header[page-header-styl-font-color]', array(
					'default' 	=> '#999',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[page-header-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_header_styling',
							'settings'  => 'neal_header[page-header-styl-font-color]',
							'priority' 	=> 15
						) 
				) );

				// link color
				$wp_customize->add_setting('neal_header[page-header-styl-link-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[page-header-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'   => 'section_header_styling',
							'settings'  => 'neal_header[page-header-styl-link-color]',
							'priority'	=> 16
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_header[page-header-styl-link-hover-color]', array(
						'default' 	=> '#c0392b',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_header[page-header-styl-link-hover-color]', array(
							'label'		=> esc_html__( 'Link Hover Color', 'neal' ),
							'section'   => 'section_header_styling',
							'settings'  => 'neal_header[page-header-styl-link-hover-color]',
							'priority'	=> 17
						) 
				) );


			/*
			***************************************************************
			* #Logo & Tagline
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// logo image label
				$wp_customize->add_setting( 'neal_logo[img-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_logo[img-label]', array(
							'label'		=> esc_html__( 'Logo Image', 'neal' ),
							'section'  	=> 'section_logo_general',
							'priority'	=> 1
						)
				) );

				// logo image
				$wp_customize->add_setting('neal_logo[img]', array(
					'default' 	=> get_template_directory_uri() .'/images/logo-dark.png',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'esc_url_raw'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_logo[img]', array(
							'section'   => 'section_logo_general',
							'label' 	=> esc_html__( 'Logo Image', 'neal' ),
							'settings'  => 'neal_logo[img]',
							'priority' 	=> 2
						) 
				) );

				// logo image 2
				$wp_customize->add_setting('neal_logo[img-2]', array(
					'default' 	=> get_template_directory_uri() .'/images/logo-light.png',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'esc_url_raw'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_logo[img-2]', array(
							'section'   => 'section_logo_general',
							'label' 	=> esc_html__( 'Logo Image 2', 'neal' ),
							'settings'  => 'neal_logo[img-2]',
							'priority' 	=> 3
						) 
				) );

				// logo text label
				$wp_customize->add_setting( 'neal_logo[text-label]', array(
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_logo[text-label]', array(
							'label'		=> esc_html__( 'Text Logo', 'neal' ),
							'section'  	=> 'section_logo_general',
							'priority'	=> 4
						)
				) );

				// logo text blogname
				$wp_customize->add_setting('neal_logo[text]', array(
					'default' 	=> get_option( 'blogname' ),
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_string'
				) );

				$wp_customize->add_control('neal_logo[text]', array(
					'settings'	=> 'neal_logo[text]',
					'section' 	=> 'section_logo_general',
					'type' 		=> 'text',
					'priority' 	=> 5
				) );

				// tagline label
				$wp_customize->add_setting( 'neal_logo[tagline-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_logo[tagline-label]', array(
							'label'		=> esc_html__( 'Tagline', 'neal' ),
							'section'  	=> 'section_logo_general',
							'priority' 	=> 6
						)
				) );

				// tagline
				$wp_customize->add_setting('neal_logo[tagline]', array(
					'default' 	=> get_option( 'blogdescription' ),
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_string'
				) );

				$wp_customize->add_control('neal_logo[tagline]', array(
					'settings'	=> 'neal_logo[tagline]',
					'section' 	=> 'section_logo_general',
					'type' 		=> 'text',
					'priority' 	=> 7
				) );

				// logo tagline (align)
				$wp_customize->add_setting('neal_logo[tagline-align]', array(
					'default' 	=> 'right',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_logo[tagline-align]', array(
					'settings'	=> 'neal_logo[tagline-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_logo_general',
					'type' 		=> 'select',
					'choices'   => array(
			                		'left'		=> esc_html__( 'Left', 'neal' ),
			                		'center'  	=> esc_html__( 'Center', 'neal' ),
			                		'right' 	=> esc_html__( 'Right', 'neal' )
			            			),
					'priority'	=> 8
				) );

			/* -----------------{ Spacing Options }----------------- */

				// logo image label
				$wp_customize->add_setting( 'neal_logo[img-spac-label]', array( 
				    'sanitize_callback' => 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_logo[img-spac-label]', array(
							'label'		=> esc_html__( 'Logo Image', 'neal' ),
							'section'  	=> 'section_logo_spacing',
							'priority'	=> 1
						)
				) );

				// logo max-height
				$wp_customize->add_setting( 'neal_logo[img-spac-max-height]', array(
					'default' 	=> 40,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[img-spac-max-height]', array(
							'settings'		=> 'neal_logo[img-spac-max-height]',
					        'label' 		=> esc_html__( 'Max-Height', 'neal' ),
					        'section' 		=> 'section_logo_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 2
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// logo text label
				$wp_customize->add_setting( 'neal_logo[logo-text-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_logo[logo-text-styl-label]', array(
							'label'		=> esc_html__( 'Text Logo', 'neal' ),
							'section'  	=> 'section_logo_styling',
							'priority' 	=> 1
						)
				) );

				// font color
				$wp_customize->add_setting('neal_logo[logo-text-styl-font-color]', array(
					'default'	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_logo[logo-text-styl-font-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_logo_styling',
							'settings'  => 'neal_logo[logo-text-styl-font-color]',
							'priority'	=> 2
						) 
				) );

				// font color 2
				$wp_customize->add_setting('neal_logo[logo-text-styl-font-color-2]', array(
					'default'	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_logo[logo-text-styl-font-color-2]', array(
							'label'		=> esc_html__( 'Color 2', 'neal' ),
							'section'   => 'section_logo_styling',
							'settings'  => 'neal_logo[logo-text-styl-font-color-2]',
							'priority'	=> 3
						) 
				) );

				// tagline label
				$wp_customize->add_setting( 'neal_logo[tagline-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_logo[tagline-styl-label]', array(
							'label'    => esc_html__( 'Tagline', 'neal' ),
							'section'  => 'section_logo_styling',
							'priority' => 4
						)
				) );

				// font color
				$wp_customize->add_setting('neal_logo[tagline-styl-font-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_logo[tagline-styl-font-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_logo_styling',
							'settings'  => 'neal_logo[tagline-styl-font-color]',
							'priority'	=> 5
						) 
				) );

				// font color 2
				$wp_customize->add_setting('neal_logo[tagline-styl-font-color-2]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_logo[tagline-styl-font-color-2]', array(
							'label'		=> esc_html__( 'Color 2', 'neal' ),
							'section'   => 'section_logo_styling',
							'settings'  => 'neal_logo[tagline-styl-font-color-2]',
							'priority'	=> 6
						) 
				) );


			/* -----------------{ Font Options }----------------- */

				// logo text label
				$wp_customize->add_setting( 'neal_logo[logo-text-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_logo[logo-text-font-label]', array(
							'label'		=> esc_html__( 'Text Logo', 'neal' ),
							'section'  	=> 'section_logo_font',
							'priority' 	=> 1
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_logo[logo-text-font-family]', array(
					'default' 	=> 'Montserrat',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_logo[logo-text-font-family]', array(
							'label'		=> esc_html__( 'Family', 'neal' ),
							'section'  	=> 'section_logo_font',
							'priority' 	=> 2
						)
				) );

				// font size
				$wp_customize->add_setting('neal_logo[logo-text-font-size]', array(
					'default' 	=> 28,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[logo-text-font-size]', array(
							'settings'		=> 'neal_logo[logo-text-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								               	'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 3
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_logo[logo-text-font-weight]', array(
					'default' 	=> 600,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[logo-text-font-weight]', array(
							'settings'		=> 'neal_logo[logo-text-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 4
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_logo[logo-text-font-line-height]', array(
						'default' 	=> 1,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[logo-text-font-line-height]', array(
							'settings'		=> 'neal_logo[logo-text-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 5
					   	)
				) );

				// letter spacing
				$wp_customize->add_setting('neal_logo[logo-text-font-letter-spacing]', array(
						'default' 	=> 2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[logo-text-font-letter-spacing]', array(
							'settings'		=> 'neal_logo[logo-text-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								               	'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 6
					   	)
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_logo[logo-text-font-uppercase]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_logo[logo-text-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_logo_font',
						'settings' 	=> 'neal_logo[logo-text-font-uppercase]',
						'type'		=> 'checkbox',
						'priority'	=> 7
				) );

				// tagline label
				$wp_customize->add_setting( 'neal_logo[tagline-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_logo[tagline-font-label]', array(
							'label'		=> esc_html__( 'Tagline', 'neal' ),
							'section'  	=> 'section_logo_font',
							'priority' 	=> 8
						)
				) );

				// font size
				$wp_customize->add_setting('neal_logo[tagline-font-size]', array(
					'default' 	=> 14,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[tagline-font-size]', array(
							'settings'		=> 'neal_logo[tagline-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								              ),
					        'priority'		=> 9
					   	)
				) );

				// font weight
				$wp_customize->add_setting('neal_logo[tagline-font-weight]', array(
					'default' 	=> 500,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[tagline-font-weight]', array(
							'settings'		=> 'neal_logo[tagline-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 10
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_logo[tagline-font-letter-spacing]', array(
						'default' 	=> 0.5,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_logo[tagline-font-letter-spacing]', array(
							'settings'		=> 'neal_logo[tagline-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_logo_font',
					        'input_attrs' 	=> array(
								             	'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 11
					    )
				) );

			/*
			***************************************************************
			* #Menu
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				if ( neal_get_option( 'header_general-header-layout' ) == 1 || neal_get_option( 'header_general-header-layout' ) == 2 ) {

					// left menu items label
					$wp_customize->add_setting( 'neal_menu[left-menu-items-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[left-menu-items-label]', array(
								'label'		=> esc_html__( 'Left Menu Items', 'neal' ),
								'section'  	=> 'section_menu_general',
								'priority'	=> 1
							)
					) );

					// left menu align
					$wp_customize->add_setting( 'neal_menu[left-menu-items-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
					) );

					$wp_customize->add_control('neal_menu[left-menu-items-align]', array(
						'settings'	=> 'neal_menu[left-menu-items-align]',
						'label' 	=> esc_html__( 'Align', 'neal' ),
						'section' 	=> 'section_menu_general',
						'type' 		=> 'select',
						'choices'   => array(
				                	   		'left'	 => esc_html__( 'Left', 'neal' ),
				                	   		'center' => esc_html__( 'Center', 'neal' ),
											'right'	 => esc_html__( 'Right', 'neal' ),
				            		   	   ),
						'priority'	=> 2
					) );

					// right menu items label
					$wp_customize->add_setting( 'neal_menu[right-menu-items-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[right-menu-items-label]', array(
								'label'		=> esc_html__( 'Right Menu Items', 'neal' ),
								'section'  	=> 'section_menu_general',
								'priority'	=> 3
							)
					) );

					// right menu align
					$wp_customize->add_setting( 'neal_menu[right-menu-items-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
					) );

					$wp_customize->add_control('neal_menu[right-menu-items-align]', array(
						'settings'	=> 'neal_menu[right-menu-items-align]',
						'label' 	=> esc_html__( 'Align', 'neal' ),
						'section' 	=> 'section_menu_general',
						'type' 		=> 'select',
						'choices'   => array(
				                	   		'left'	 => esc_html__( 'Left', 'neal' ),
				                	   		'center' => esc_html__( 'Center', 'neal' ),
											'right'	 => esc_html__( 'Right', 'neal' ),
				            		   	   ),
						'priority'	=> 4
					) );
				} else {
					// menu items label
					$wp_customize->add_setting( 'neal_menu[menu-items-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[menu-items-label]', array(
								'label'		=> esc_html__( 'Menu Items', 'neal' ),
								'section'  	=> 'section_menu_general',
								'priority'	=> 5
							)
					) );

					// menu items align
					$wp_customize->add_setting('neal_menu[menu-items-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
					) );

					$wp_customize->add_control('neal_menu[menu-items-align]', array(
						'settings'	=> 'neal_menu[menu-items-align]',
						'label' 	=> esc_html__( 'Align', 'neal' ),
						'section' 	=> 'section_menu_general',
						'type' 		=> 'select',
						'choices'   => array(
				                	   		'left'	 => esc_html__( 'Left', 'neal' ),
				                	   		'center' => esc_html__( 'Center', 'neal' ),
											'right'	 => esc_html__( 'Right', 'neal' ),
				            		   	   ),
						'priority'	=> 6
					) );
				}

				// submenu items label
				$wp_customize->add_setting( 'neal_menu[submenu-items-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[submenu-items-label]', array(
							'label'		=> esc_html__( 'Submenu Items', 'neal' ),
							'section'  	=> 'section_menu_general',
							'priority'	=> 7
						)
				) );

				// submenu items align
				$wp_customize->add_setting('neal_menu[submenu-items-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_menu[submenu-items-align]', array(
					'settings'	=> 'neal_menu[submenu-items-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_menu_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'	=> esc_html__( 'Left', 'neal' ),
								 	'right' => esc_html__( 'Right', 'neal' ),
			            		  ),
					'priority'	=> 8
				) );

				// shop icon label
				$wp_customize->add_setting( 'neal_menu[shop-label]', array(
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_menu[shop-label]', array(
							'label'		=> esc_html__( 'Shop Icon', 'neal' ),
							'section'  	=> 'section_menu_general',
							'priority' 	=> 9
						)
				) );

				// shop icon
				$wp_customize->add_setting( 'neal_menu[shop-icon]', array(
					'default' 	=> 'ion-search',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_menu[shop-icon]', array(
					'settings'	=> 'neal_menu[shop-icon]',
					'label' 	=> esc_html__( 'Select Icon', 'neal' ),
					'section' 	=> 'section_menu_general',
					'type' 		=> 'select',
					'choices'   => array(
					        	   	'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
			                		'ion-bag'		=> '&#xf110;',
									'ion-ios-cart' 	=> '&#xf3f8;',
									'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
									'fa-shopping-bag' => '&#xf290;',
									'fa-shopping-basket' => '&#xf291;'
			            		   ),
					'priority'	=> 10
				) );

				// search icon label
				$wp_customize->add_setting( 'neal_menu[search-label]', array(
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_menu[search-label]', array(
							'label'		=> esc_html__( 'Search Icon', 'neal' ),
							'section'  	=> 'section_menu_general',
							'priority' 	=> 11
						)
				) );

				// search icon
				$wp_customize->add_setting( 'neal_menu[search-icon]', array(
					'default' 	=> 'ion-search',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_menu[search-icon]', array(
					'settings'	=> 'neal_menu[search-icon]',
					'label' 	=> esc_html__( 'Select Icon', 'neal' ),
					'section' 	=> 'section_menu_general',
					'type' 		=> 'select',
					'choices'   => array(
					        	   	'ion-icons'			=> esc_html__( '---Ion Icons---', 'neal' ),
			                		'ion-search' 		=> '&#xf21f;',
			                		'ion-ios-search' 	=> '&#xf4a5;',
			                		'ion-ios-search-strong' => '&#xf4a4;',
			                		'ion-android-search' => '&#xf2f5;',
			                		'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                		'fa-search'		=> '&#xf002;',
			                		'fa-search-plus' => '&#xf00e;',

			            		  ),
					'priority'	=> 12
				) );

				// hamburger menu label
				$wp_customize->add_setting( 'neal_menu[ham-menu-label]', array(
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_menu[ham-menu-label]', array(
							'label'		=> esc_html__( 'Hamburger Menu', 'neal' ),
							'section'  	=> 'section_menu_general',
							'priority' 	=> 13
						)
				) );

				// footer menu
				if ( neal_get_option( 'footer_general-footer-layout') == 1 ) {

					// footer-menu items label
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[footer-menu-items-label]', array(
								'label'		=> esc_html__( 'Footer Menu Items', 'neal' ),
								'section'  	=> 'section_menu_general',
								'priority'	=> 14
							)
					) );

					// footer-menu items align
					$wp_customize->add_setting('neal_menu[footer-menu-items-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
					) );

					$wp_customize->add_control('neal_menu[footer-menu-items-align]', array(
						'settings'	=> 'neal_menu[footer-menu-items-align]',
						'label' 	=> esc_html__( 'Align', 'neal' ),
						'section' 	=> 'section_menu_general',
						'type' 		=> 'select',
						'choices'   => array(
				                	   		'left'		=> esc_html__( 'Left', 'neal' ),
				                	   		'center'	=> esc_html__( 'Center', 'neal' ),
											'right'		=> esc_html__( 'Right', 'neal' ),
				            		   	   ),
						'priority'	=> 15
					) );
				}

			/* -----------------{ Spacing Options }----------------- */

				// menu items label
				$wp_customize->add_setting( 'neal_menu[menu-items-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[menu-items-spac-label]', array(
							'label'		=> esc_html__( 'Menu Items', 'neal' ),
							'section'  	=> 'section_menu_spacing',
							'priority' 	=> 1
						)
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_menu[menu-items-spac-padding-top-bottom]', array(
						'default' 	=> 8,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-spac-padding-top-bottom]', array(
							'settings'		=> 'neal_menu[menu-items-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 2
					    )
				) );

				// padding right-left
				$wp_customize->add_setting('neal_menu[menu-items-spac-padding-right-left]', array(
						'default' 	=> 5,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-spac-padding-right-left]', array(
							'settings'		=> 'neal_menu[menu-items-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 3
					    )
				) );

				// margin right
				$wp_customize->add_setting('neal_menu[menu-items-spac-margin-right]', array(
						'default' 	=> 20,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-spac-margin-right]', array(
							'settings'		=> 'neal_menu[menu-items-spac-margin-right]',
					        'label'			=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 4
					    )
				) );

				// submenu items label
				$wp_customize->add_setting( 'neal_menu[submenu-items-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[submenu-items-spac-label]', array(
							'label'		=> esc_html__( 'Submenu Items', 'neal' ),
							'section' 	=> 'section_menu_spacing',
							'priority' 	=> 5
						)
				) );

				// width
				$wp_customize->add_setting('neal_menu[submenu-items-spac-width]', array(
					'default' 	=> 250,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-spac-width]', array(
							'settings'		=> 'neal_menu[submenu-items-spac-width]',
					        'label' 		=> esc_html__( 'Width', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 100,
								                'max' => 350
								               ),
					        'priority'		=> 6
					    )
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_menu[submenu-items-spac-padding-top-bottom]', array(
						'default' 	=> 12,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-spac-padding-top]', array(
							'settings'		=> 'neal_menu[submenu-items-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 7
					   	)
				) );

				// padding right-left
				$wp_customize->add_setting('neal_menu[submenu-items-spac-padding-right-left]', array(
						'default' 	=> 25,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-spac-padding-right-left]', array(
							'settings'		=> 'neal_menu[submenu-items-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 8
					    )
				) );

				// margin top
				$wp_customize->add_setting('neal_menu[submenu-items-spac-margin-top]', array(
						'default' 	=> 60,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-spac-margin-top]', array(
							'settings'		=> 'neal_menu[submenu-items-spac-margin-top]',
					        'label' 		=> esc_html__( 'Distance From parent', 'neal' ),
					        'section' 		=> 'section_menu_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 9
					   	)
				) );

				// footer menu
				if ( neal_get_option( 'footer_general-footer-layout') == 1 ) {

					// footer menu items label
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[footer-menu-items-spac-label]', array(
								'label'		=> esc_html__( 'Footer Menu Items', 'neal' ),
								'section' 	=> 'section_menu_spacing',
								'priority' 	=> 10
							)
					) );

					// margin right
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-spac-margin-right]', array(
							'default' 	=> 20,
							'type'	  	=> 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback' => 'neal_sanitize_number'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[footer-menu-items-spac-margin-right]', array(
								'settings'		=> 'neal_menu[footer-menu-items-spac-margin-right]',
						        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
						        'section' 		=> 'section_menu_spacing',
						        'input_attrs' 	=> array(
									                'min' => 0,
									                'max' => 280
									               ),
						        'priority'		=> 11
						   	)
					) );
				}


			/* -----------------{ Styling Options }----------------- */

				// menu items label
				$wp_customize->add_setting( 'neal_menu[menu-items-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[menu-items-styl-label]', array(
							'label'    => esc_html__( 'Menu Items', 'neal' ),
							'section'  => 'section_menu_styling',
							'priority' => 1
						)
				) );

				// link color
				$wp_customize->add_setting('neal_menu[menu-items-styl-link-color]', array(
						'default' 	=> '#000',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[menu-items-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'	=> 'section_menu_styling',
							'settings'  => 'neal_menu[menu-items-styl-link-color]',
							'priority'	=> 2
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_menu[menu-items-styl-link-hover-color]', array(
						'default' 	=> '#c0392b',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[menu-items-styl-link-hover-color]', array(
							'label'		=> esc_html__( 'Link Hover Color', 'neal' ),
							'section'	=> 'section_menu_styling',
							'settings'	=> 'neal_menu[menu-items-styl-link-hover-color]',
							'priority'	=> 3
						) 
				) );

				// link active color
				$wp_customize->add_setting('neal_menu[menu-items-styl-link-active-color]', array(
						'default' 	=> '#c0392b',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[menu-items-styl-link-active-color]', array(
							'label'		=> esc_html__( 'Link Active Color', 'neal' ),
							'section'	=> 'section_menu_styling',
							'settings'	=> 'neal_menu[menu-items-styl-link-active-color]',
							'priority'	=> 4
						) 
				) );

				// icons color
				$wp_customize->add_setting('neal_menu[menu-items-styl-icons-color]', array(
						'default' 	=> '#000',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[menu-items-styl-icons-color]', array(
							'label'		=> esc_html__( 'Icons Color', 'neal' ),
							'section'   => 'section_menu_styling',
							'settings'  => 'neal_menu[menu-items-styl-icons-color]',
							'priority'	=> 5
						) 
				) );

				// highlight active item		
				$wp_customize->add_setting( 'neal_menu[menu-items-styl-active-item]', array(
						'default' 		=> true,
						'type' 			=> 'option',
						'transport'		=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[menu-items-styl-active-item]', array(
						'label'    => esc_html__( 'Highlight Active Item', 'neal' ),
						'section'           => 'section_menu_styling',
						'settings'          => 'neal_menu[menu-items-styl-active-item]',
						'type'				=> 'checkbox',
						'priority' => 6
				) );

				// submenu items label
				$wp_customize->add_setting( 'neal_menu[submenu-items-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[submenu-items-styl-label]', array(
							'label'		=> esc_html__( 'Submenu Items', 'neal' ),
							'section'	=> 'section_menu_styling',
							'priority'	=> 7
						)
				) );

				// background color
				$wp_customize->add_setting('neal_menu[submenu-items-styl-bg-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[submenu-items-styl-bg-color]', array(
							'label'      => esc_html__( 'Background Color', 'neal' ),
							'section'    => 'section_menu_styling',
							'settings'   => 'neal_menu[submenu-items-styl-bg-color]',
									'priority' => 8
								) ) 
				);

				// link color
				$wp_customize->add_setting('neal_menu[submenu-items-styl-link-color]', array(
						'default' 	=> '#000',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[submenu-items-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'	=> 'section_menu_styling',
							'settings'  => 'neal_menu[submenu-items-styl-link-color]',
							'priority'	=> 9
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_menu[submenu-items-styl-link-hover-color]', array(
						'default' 	=> '#c0392b',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[submenu-items-styl-link-hover-color]', array(
							'label'      => esc_html__( 'Link Hover Color', 'neal' ),
							'section'    => 'section_menu_styling',
							'settings'   => 'neal_menu[submenu-items-styl-link-hover-color]',
							'priority' => 10
						) 
				) );

				// shadow		
				$wp_customize->add_setting( 'neal_menu[submenu-items-styl-shadow]', array(
					'default' 		=> true,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[submenu-items-styl-shadow]', array(
					'label'		=> esc_html__( 'Shadow', 'neal' ),
					'section'   => 'section_menu_styling',
					'settings'  => 'neal_menu[submenu-items-styl-shadow]',
					'type'		=> 'checkbox',
					'priority'	=> 11
				) );

				// border label
				$wp_customize->add_setting( 'neal_menu[submenu-items-styl-child-border-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_menu[submenu-items-styl-child-border-label]', array(
							'label'    => esc_html__( 'Border', 'neal' ),
							'section'  => 'section_menu_styling',
							'priority' => 12
						)
				) );

				// border size
				$wp_customize->add_setting('neal_menu[submenu-items-styl-child-border-size]', array(
						'default' 	=> 4,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-styl-child-border-size]',array(
							'settings'		=> 'neal_menu[submenu-items-styl-child-border-size]',
					        'label' 		=> esc_html__( 'Size', 'neal' ),
					        'section'		=> 'section_menu_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 15
								               ),
					        'priority' 		=> 13
					    )
				) );

				// border style
				$wp_customize->add_setting('neal_menu[submenu-items-styl-child-border-style]', array(
						'default' 	=> 'solid',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_menu[submenu-items-styl-child-border-style]', array(
						'label'		=> esc_html__( 'Style', 'neal' ),
						'section'	=> 'section_menu_styling',
						'settings'  => 'neal_menu[submenu-items-styl-child-border-style]',
						'type'		=> 'select',
						'choices' 	=> array(
									   	'solid'		=> esc_html__( 'Solid', 'neal' ),
										'dotted'	=> esc_html__( 'Dotted', 'neal' ),
										'dashed'	=> esc_html__( 'Dashed', 'neal' ),
										'double'	=> esc_html__( 'Double', 'neal' ),
										'groove' 	=> esc_html__( 'Groove', 'neal' ),
										),
						'priority'	=> 14
				) );

				// border color
				$wp_customize->add_setting( 'neal_menu[submenu-items-styl-child-border-color]', array(
						'default' 	=> '#eee',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_menu[submenu-items-styl-child-border-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'	=> 'section_menu_styling',
							'settings'	=> 'neal_menu[submenu-items-styl-child-border-color]',
							'priority'	=> 15
						) 
				) );

				// footer menu
				if ( neal_get_option( 'footer_general-footer-layout') == 1 ) {

					// footer menu label
					$wp_customize->add_setting( 'neal_menu[footer-menu-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[footer-menu-styl-label]', array(
								'label'    => esc_html__( 'Footer Menu', 'neal' ),
								'section'  => 'section_menu_styling',
								'priority' => 16
							)
					) );

					// background color
					$wp_customize->add_setting('neal_menu[footer-menu-styl-bg-color]', array(
						'default' 	=> '#f5f5f5',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
					) );

					$wp_customize->add_control( 
						new WP_Customize_Color_Control( $wp_customize, 'neal_menu[footer-menu-styl-bg-color]', array(
								'label'     => esc_html__( 'Background Color', 'neal' ),
								'section'   => 'section_menu_styling',
								'settings'  => 'neal_menu[footer-menu-styl-bg-color]',
								'priority'	=> 17
						) ) 
					);

					// footer menu items color
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-styl-link-color]', array(
							'default' 	=> '#000',
							'type'	  	=> 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback' => 'sanitize_hex_color'
					) );

					$wp_customize->add_control( 
						new WP_Customize_Color_Control( $wp_customize, 'neal_menu[footer-menu-items-styl-link-color]', array(
								'label'     => esc_html__( 'Items Color', 'neal' ),
								'section'   => 'section_menu_styling',
								'settings'  => 'neal_menu[footer-menu-items-styl-link-color]',
								'priority'	=> 18
						) ) 
					);

				}


			/* -----------------{ Font Options }----------------- */

				// menu items label
				$wp_customize->add_setting( 'neal_menu[menu-items-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[menu-items-font-label]', array(
							'label'    => esc_html__( 'Menu Items', 'neal' ),
							'section'  => 'section_menu_font',
							'priority' => 1
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_menu[menu-items-font-family]', array(
					'default' 	=> 'Montserrat',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_menu[menu-items-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_menu_font',
							'priority' => 2
						)
				) );

				// font size
				$wp_customize->add_setting('neal_menu[menu-items-font-size]', array(
					'default' 	=> 14,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-font-size]', array(
							'settings'		=> 'neal_menu[menu-items-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								               	'min' => 10,
								                'max' => 50
								               ),
					        'priority'		=> 3
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_menu[menu-items-font-weight]', array(
					'default' 	=> 500,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-font-weight]', array(
							'settings'		=> 'neal_menu[menu-items-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								             	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 4
					       )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_menu[menu-items-font-letter-spacing]', array(
						'default' 	=> 1,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-font-letter-spacing]', array(
							'settings'		=> 'neal_menu[menu-items-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								               	'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 5
					    )
				) );

				// icon size
				$wp_customize->add_setting('neal_menu[menu-items-font-icon-size]', array(
						'default' 	=> 19,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[menu-items-font-icon-size]', array(
							'settings'		=> 'neal_menu[menu-items-font-icon-size]',
					        'label' 		=> esc_html__( 'Icons Size', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								                'min' => 8,
								                'max' => 80
								               ),
					        'priority'		=> 6
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_menu[menu-items-font-italic]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[menu-items-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_menu_font',
					'settings'  => 'neal_menu[menu-items-font-italic]',
					'type'		=> 'checkbox',
					'priority'	=> 7
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_menu[menu-items-font-uppercase]', array(
						'default' 	=> true,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[menu-items-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_menu_font',
						'settings'  => 'neal_menu[menu-items-font-uppercase]',
						'type'		=> 'checkbox',
						'priority'	=> 8
				) );

				// submenu items label
				$wp_customize->add_setting( 'neal_menu[submenu-items-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[submenu-items-font-label]', array(
							'label'    => esc_html__( 'Submenu Items', 'neal' ),
							'section'  => 'section_menu_font',
							'priority' => 9
						)
				) );

				// font size
				$wp_customize->add_setting('neal_menu[submenu-items-font-size]', array(
					'default' 	=> 14,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-font-size]', array(
							'settings'		=> 'neal_menu[submenu-items-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority'	=> 10
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_menu[submenu-items-font-weight]', array(
					  	'default' 	=> 400,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-font-weight]', array(
							'settings'		=> 'neal_menu[submenu-items-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								             	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 11
					       )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_menu[submenu-items-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[submenu-items-font-letter-spacing]', array(
							'settings'		=> 'neal_menu[submenu-items-font-letter-spacing]',
					        'label'			=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 12
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_menu[submenu-items-font-italic]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[submenu-items-font-italic]', array(
						'label'		=> esc_html__( 'Italic', 'neal' ),
						'section'   => 'section_menu_font',
						'settings' 	=> 'neal_menu[submenu-items-font-italic]',
						'type'		=> 'checkbox',
						'priority'	=> 13
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_menu[submenu-items-font-uppercase]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[submenu-items-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_menu_font',
						'settings'  => 'neal_menu[submenu-items-font-uppercase]',
						'type'		=> 'checkbox',
						'priority'	=> 14
				) );

				// mobile menu label
				$wp_customize->add_setting( 'neal_menu[mobile-menu-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[mobile-menu-font-label]', array(
							'label'    => esc_html__( 'Mobile Menu', 'neal' ),
							'section'  => 'section_menu_font',
							'priority' => 15
						)
				) );

				// font size
				$wp_customize->add_setting('neal_menu[mobile-menu-font-size]', array(
					'default' 	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[mobile-menu-font-size]', array(
							'settings'		=> 'neal_menu[mobile-menu-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority'		=> 16
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_menu[mobile-menu-font-weight]', array(
					'default' 	=> 400,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[mobile-menu-font-weight]', array(
							'settings'		=> 'neal_menu[mobile-menu-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								             	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 17
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_menu[mobile-menu-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 
						'neal_menu[mobile-menu-font-letter-spacing]', array(
							'settings'		=> 'neal_menu[mobile-menu-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_menu_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 18
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_menu[mobile-menu-font-italic]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[mobile-menu-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_menu_font',
					'settings'  => 'neal_menu[mobile-menu-font-italic]',
					'type'		=> 'checkbox',
					'priority'	=> 19
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_menu[mobile-menu-font-uppercase]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_menu[mobile-menu-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_menu_font',
						'settings'  => 'neal_menu[mobile-menu-font-uppercase]',
						'type'		=> 'checkbox',
						'priority'	=> 20
				) );

				// footer menu
				if ( neal_get_option( 'footer_general-footer-layout' ) == 3 ) {
					
					// menu items label
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
					) );

					$wp_customize->add_control(
						new Neal_Custom_Label_Control( $wp_customize, 'neal_menu[footer-menu-items-font-label]', array(
								'label'    => esc_html__( 'Footer Menu Items', 'neal' ),
								'section'  => 'section_menu_font',
								'priority' => 21
							)
					) );

					// font size
					$wp_customize->add_setting('neal_menu[footer-menu-items-font-size]', array(
						'default' 	=> 14,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
					) );
					
					$wp_customize->add_control(
						new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[footer-menu-items-font-size]', array(
								'settings'		=> 'neal_menu[footer-menu-items-font-size]',
						        'label' 		=> esc_html__( 'Font Size', 'neal' ),
						        'section' 		=> 'section_menu_font',
						        'input_attrs' 	=> array(
									               	'min' => 10,
									                'max' => 50
									               ),
						        'priority'		=> 23
						    )
					) );

					// font weight
					$wp_customize->add_setting('neal_menu[footer-menu-items-font-weight]', array(
						'default' 	=> 500,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
					) );
					
					$wp_customize->add_control(
						new Neal_Custom_Slider_Control( $wp_customize, 'neal_menu[footer-menu-items-font-weight]', array(
								'settings'		=> 'neal_menu[footer-menu-items-font-weight]',
						        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
						        'section' 		=> 'section_menu_font',
						        'input_attrs' 	=> array(
									             	'min'	=> 100,
									                'max' 	=> 900,
									                'step'	=> 100
									            ),
						        'priority' => 24
						       )
					) );

					// italic
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-font-italic]', array(
							'default' 	=> false,
							'type' 		=> 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback' => 'neal_sanitize_checkbox'
					) );

					$wp_customize->add_control( 'neal_menu[footer-menu-items-font-italic]', array(
							'label'		=> esc_html__( 'Italic', 'neal' ),
							'section'   => 'section_menu_font',
							'settings'  => 'neal_menu[footer-menu-items-font-italic]',
							'type'		=> 'checkbox',
							'priority'	=> 25
					) );

					// uppercase
					$wp_customize->add_setting( 'neal_menu[footer-menu-items-font-uppercase]', array(
							'default' 	=> true,
							'type' 		=> 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback' => 'neal_sanitize_checkbox'
					) );

					$wp_customize->add_control( 'neal_menu[footer-menu-items-font-uppercase]', array(
							'label'		=> esc_html__( 'Uppercase', 'neal' ),
							'section'   => 'section_menu_font',
							'settings'  => 'neal_menu[footer-menu-items-font-uppercase]',
							'type'		=> 'checkbox',
							'priority'	=> 26
					) );
				}

			/*
			***************************************************************
			* #Footer
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// general footer label
				$wp_customize->add_setting( 'neal_footer[general-footer-label]', array(
				    	'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[general-footer-label]', array(
							'label'    => esc_html__( 'General Footer', 'neal' ),
							'section'  => 'section_footer_general',
							'priority' => 1
						)
				) );

				// footer layouts
				$wp_customize->add_setting( 'neal_footer[general-footer-layout]', array(
						'default' 	=> 1,
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control( 'neal_footer[general-footer-layout]', array(
					'settings'	=> 'neal_footer[general-footer-layout]',
					'label' 	=> esc_html__( 'Footer Layouts', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'select',
					'choices' 	=> array(
								   	1 	=> esc_html__( 'Layout 1', 'neal' ),
									2 	=> esc_html__( 'Layout 2', 'neal' ),
									3 	=> esc_html__( 'Layout 3', 'neal' ),
									4 	=> esc_html__( 'Layout 4', 'neal' ),
								   ),
					'priority'	=> 2
				) );

				// fixed footer
				$wp_customize->add_setting( 'neal_footer[general-footer-fixed]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_footer[general-footer-fixed]', array(
					'label'		=> esc_html__( 'Fixed Footer', 'neal' ),
					'section'   => 'section_footer_general',
					'type'		=> 'checkbox',
					'priority'	=> 3
				) );

				// widget title label
				$wp_customize->add_setting( 'neal_footer[wtitle-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[wtitle-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_footer_general',
							'priority' => 4
						)
				) );

				// widget title align
				$wp_customize->add_setting('neal_footer[wtitle-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_footer[wtitle-align]', array(
					'settings'	=> 'neal_footer[wtitle-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' ),
			            		   ),
					'priority' => 5
				) );

				// instagram label
				$wp_customize->add_setting( 'neal_footer[instagram-label]', array(
				    'default' 	=> true,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_footer[instagram-label]', array(
							'label'    => esc_html__( 'Instagram', 'neal' ),
							'section'  => 'section_footer_general',
							'priority' => 6
						)
				) );

				// layout
				$wp_customize->add_setting( 'neal_footer[instagram-layout]', array(
					'default' 	=> 'boxed',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-layout]', array(
					'settings'	=> 'neal_footer[instagram-layout]',
					'label' 	=> esc_html__( 'Layout', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'full'	=> esc_html__( 'Full-Width', 'neal' ),
									'boxed' => esc_html__( 'Boxed', 'neal' ),
			            		   ),
					'priority' => 7
				) );

				// instagram title
				$wp_customize->add_setting( 'neal_footer[instagram-title]', array(
					'default' 	=> 'Follow@Neal',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_string'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-title]', array(
					'settings'	=> 'neal_footer[instagram-title]',
					'label' 	=> esc_html__( 'Title', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'text',
					'priority'	=> 8
				) );

				// instagram title link
				$wp_customize->add_setting( 'neal_footer[instagram-title-link]', array(
					'default' 	=> true,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-title-link]', array(
					'settings'	=> 'neal_footer[instagram-title-link]',
					'label' 	=> esc_html__( 'Title Link', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'checkbox',
					'priority'	=> 9
				) );

				// instagram title position
				$wp_customize->add_setting( 'neal_footer[instagram-title-pos]', array(
					'default' 	=> false,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-title-pos]', array(
					'settings'	=> 'neal_footer[instagram-title-pos]',
					'label' 	=> esc_html__( 'Show Title on Images', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'checkbox',
					'priority'	=> 10
				) );

				// instagram username
				$wp_customize->add_setting( 'neal_footer[instagram-username]', array(
					'default' 	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_string'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-username]', array(
					'settings'	=> 'neal_footer[instagram-username]',
					'label' 	=> esc_html__( 'Username', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'text',
					'priority'	=> 11
				) );

				// instagram number
				$wp_customize->add_setting( 'neal_footer[instagram-number]', array(
					'default' 	=> 10,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-number]', array(
					'settings'	=> 'neal_footer[instagram-number]',
					'label' 	=> esc_html__( 'Number', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'number',
					'priority'	=> 12
				) );

				// instagram columns
				$wp_customize->add_setting( 'neal_footer[instagram-columns]', array(
					'default' 	=> 4,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-columns]', array(
					'settings'	=> 'neal_footer[instagram-columns]',
					'label' 	=> esc_html__( 'Columns', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'number',
					'priority'	=> 13
				) );

				// instagram carousel
				$wp_customize->add_setting( 'neal_footer[instagram-carousel]', array(
					'default' 	=> true,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-carousel]', array(
					'settings'	=> 'neal_footer[instagram-carousel]',
					'label' 	=> esc_html__( 'Carousel', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'checkbox',
					'priority'	=> 14
				) );

				// instagram carousel nav
				$wp_customize->add_setting( 'neal_footer[instagram-carousel-nav]', array(
					'default' 	=> true,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-carousel-nav]', array(
					'settings'	=> 'neal_footer[instagram-carousel-nav]',
					'label' 	=> esc_html__( 'Carousel Nav', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'checkbox',
					'priority'	=> 15
				) );

				// instagram carousel loop
				$wp_customize->add_setting( 'neal_footer[instagram-carousel-loop]', array(
					'default' 	=> false,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control( 'neal_footer[instagram-carousel-loop]', array(
					'settings'	=> 'neal_footer[instagram-carousel-loop]',
					'label' 	=> esc_html__( 'Carousel Loop', 'neal' ),
					'section' 	=> 'section_footer_general',
					'type' 		=> 'checkbox',
					'priority'	=> 16
				) );

			/* -----------------{ Spacing Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_footer[general-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[general-spac-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_footer_spacing',
							'priority' => 1
						)
				) );

				// padding-top
				$wp_customize->add_setting('neal_footer[general-spac-padding-top]', array(
						'default' 	=> 50,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_footer[general-spac-padding-top]', array(
							'settings'		=> 'neal_footer[general-spac-padding-top]',
					        'label'			=> esc_html__( 'Padding Top', 'neal' ),
					        'section'		=> 'section_footer_spacing',
					        'input_attrs'	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 2
					    )
				) );

				// padding-bottom
				$wp_customize->add_setting('neal_footer[general-spac-padding-bottom]', array(
						'default' 	=> 50,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_footer[general-spac-padding-bottom]', array(
							'settings'		=> 'neal_footer[general-spac-padding-bottom]',
					        'label'			=> esc_html__( 'Padding Bottom', 'neal' ),
					        'section'		=> 'section_footer_spacing',
					        'input_attrs'	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 3
					    )
				) );

				// widget title label
				$wp_customize->add_setting( 'neal_footer[wtitle-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[wtitle-spac-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_footer_spacing',
							'priority' => 4
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_footer[wtitle-spac-margin-bottom]', array(
						'default' 	=> 20,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_footer[wtitle-spac-margin-bottom]', array(
							'settings'		=> 'neal_footer[wtitle-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_footer_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 5
					     )
				) );

				// instagram label
				$wp_customize->add_setting( 'neal_footer[instagram-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[instagram-spac-label]', array(
							'label'    => esc_html__( 'Instagram', 'neal' ),
							'section'  => 'section_footer_spacing',
							'priority' => 6
						)
				) );

				// margin right
				$wp_customize->add_setting( 'neal_footer[instagram-spac-item-padding-right-left]', array(
						'default' 	=> 10,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_footer[instagram-spac-item-padding-right-left]', array(
							'settings'		=> 'neal_footer[instagram-spac-item-padding-right-left]',
					        'label' 		=> esc_html__( 'Item Padding-Right-Left', 'neal' ),
					        'section'		=> 'section_footer_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 7
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_footer[general-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[general-styl-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_footer_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_footer[general-styl-bg-color]', array(
					'default' 	=> '#f5f5f5',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_footer[general-styl-bg-color]', array(
							'section'   => 'section_footer_styling',
							'label'    	=> esc_html__( 'Background Color', 'neal' ),
							'settings'  => 'neal_footer[general-styl-bg-color]',
							'priority'	=> 2
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_footer[general-styl-font-color]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_footer[general-styl-font-color]', array(
							'section'   => 'section_footer_styling',
							'label'    	=> esc_html__( 'Font Color', 'neal' ),
							'settings'  => 'neal_footer[general-styl-font-color]',
							'priority'	=> 3
						) 
				) );


				// widgets label
				$wp_customize->add_setting( 'neal_footer[widgets-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_footer[widgets-styl-label]', array(
							'label'    => esc_html__( 'Widgets', 'neal' ),
							'section'  => 'section_footer_styling',
							'priority' => 4
						)
				) );

				// font color
				$wp_customize->add_setting('neal_footer[widgets-styl-font-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_footer[widgets-styl-font-color]', array(
							'label'		=> esc_html__( 'Text Color', 'neal' ),
							'section'   => 'section_footer_styling',
							'settings'  => 'neal_footer[widgets-styl-font-color]',
							'priority' 	=> 5
						)
				) );

				// link color
				$wp_customize->add_setting('neal_footer[widgets-styl-link-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_footer[widgets-styl-link-color]', array(
							'label'     => esc_html__( 'Link Color', 'neal' ),
							'section'   => 'section_footer_styling',
							'settings'  => 'neal_footer[widgets-styl-link-color]',
							'priority'	=> 6
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_footer[widgets-styl-link-hover-color]', array(
					'default' 	=> '#999',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_footer[widgets-styl-link-hover-color]', array(
							'label'     => esc_html__( 'Link Hover Color', 'neal' ),
							'section'   => 'section_footer_styling',
							'settings'  => 'neal_footer[widgets-styl-link-hover-color]',
							'priority'	=> 7
						) 
				) );


			/*
			***************************************************************
			* #Sidebar
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// widget title
				$wp_customize->add_setting( 'neal_sidebar[wtitle-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[wtitle-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_sidebar_general',
							'priority' => 1
						)
				) );

				// widget title align
				$wp_customize->add_setting('neal_sidebar[wtitle-align]', array(
						'default' 	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_sidebar[wtitle-align]', array(		
					'label'		=> esc_html__( 'Align', 'neal' ),
					'settings'	=> 'neal_sidebar[wtitle-align]',
					'section' 	=> 'section_sidebar_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
			                		'center' 	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' ),
			            		   ),
					'priority'	=> 2
				) );

			/* -----------------{ Spacing Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_sidebar[general-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[general-spac-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_sidebar_spacing',
							'priority' => 1
						)
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_sidebar[general-spac-padding-top-bottom]', 
					array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[general-spac-padding-top-bottom]', array(
							'settings'		=> 'neal_sidebar[general-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_sidebar_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 2
					    )
				) );

				// padding right-left
				$wp_customize->add_setting('neal_sidebar[general-spac-padding-right-left]', 
					array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[general-spac-padding-right-left]', array(
							'settings'		=> 'neal_sidebar[general-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_sidebar_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 3
					       )
				) );

				// margin bottom
				$wp_customize->add_setting('neal_sidebar[general-spac-margin-bottom]', 
					array(
							'default' => 40,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[general-spac-margin-bottom]', array(
						   	'settings'		=> 'neal_sidebar[general-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_sidebar_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 4
					   	)
				) );

				// widget title label
				$wp_customize->add_setting( 'neal_sidebar[wtitle-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[wtitle-spac-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_sidebar_spacing',
							'priority' => 5
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_sidebar[wtitle-spac-margin-bottom]', array(
					'default'	=> 30,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[wtitle-spac-margin-bottom]', array(
							'settings'		=> 'neal_sidebar[wtitle-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_sidebar_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 6
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_sidebar[general-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[general-styl-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_sidebar_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_sidebar[general-styl-bg-color]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_sidebar[general-styl-bg-color]', array(
							'section'	=> 'section_sidebar_styling',
							'label'    	=> esc_html__( 'Background Color', 'neal' ),
							'settings'  => 'neal_sidebar[general-styl-bg-color]',
							'priority' 	=> 2
						) 
				) );

				// border label
				$wp_customize->add_setting( 'neal_sidebar[general-styl-child-border-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'postMessage',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_sidebar[general-styl-child-border-label]', array(
							'label'    => esc_html__( 'Border', 'neal' ),
							'section'  => 'section_sidebar_styling',
							'priority' => 3
						)
				) );

				// border size
				$wp_customize->add_setting('neal_sidebar[general-styl-child-border-size]', array(
					'default'	=> 0,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[general-styl-child-border-size]', array(
							'settings'		=> 'neal_sidebar[general-styl-child-border-size]',
					        'label' 		=> esc_html__( 'Size', 'neal' ),
					        'section' 		=> 'section_sidebar_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 15
								               ),
					        'priority' 		=> 4
					    )
				) );

				// border style
				$wp_customize->add_setting('neal_sidebar[general-styl-child-border-style]', array(
						'default' 	=> 'solid',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_sidebar[general-styl-child-border-style]', array(
					'label'		=> esc_html__( 'Style', 'neal' ),
					'section'   => 'section_sidebar_styling',
					'settings'  => 'neal_sidebar[general-styl-child-border-style]',
					'type' 		=> 'select',
					'choices' 	=> array(
								   	'solid'		=> esc_html__( 'Solid', 'neal' ),
									'dotted' 	=> esc_html__( 'Dotted', 'neal' ),
									'dashed' 	=> esc_html__( 'Dashed', 'neal' ),
									'double' 	=> esc_html__( 'Double', 'neal' ),
									'groove' 	=> esc_html__( 'Groove', 'neal' ),
								   ),
					'priority'	=> 5
				) );

				// border color
				$wp_customize->add_setting('neal_sidebar[general-styl-child-border-color]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_sidebar[general-styl-child-border-color]', array(
							'label'      => esc_html__( 'Color', 'neal' ),
							'section'    => 'section_sidebar_styling',
							'settings'   => 'neal_sidebar[general-styl-child-border-color]',
							'priority' => 6
						) 
				) );

				// widget title label
				$wp_customize->add_setting( 'neal_sidebar[wtitle-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[wtitle-styl-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_sidebar_styling',
							'priority' => 7
						)
				) );

				// font color
				$wp_customize->add_setting('neal_sidebar[wtitle-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_sidebar[wtitle-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_sidebar_styling',
							'settings'  => 'neal_sidebar[wtitle-styl-font-color]',
							'priority' 	=> 8
						) 
				) );

				// about me widget label
				$wp_customize->add_setting( 'neal_sidebar[about-me-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[about-me-styl-label]', array(
							'label'    => esc_html__( 'About Me Widget', 'neal' ),
							'section'  => 'section_sidebar_styling',
							'priority' => 9
						)
				) );

				// radius
				$wp_customize->add_setting( 'neal_sidebar[about-me-styl-radius]', array(
					'default' 	=> 50,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[about-me-styl-radius]', array(
							'settings'		=> 'neal_sidebar[about-me-styl-radius]',
					        'label' 		=> esc_html__( 'Border Radius', 'neal' ),
					        'section' 		=> 'section_sidebar_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 50
								               ),
					        'priority'		=> 10
					    )
				) );

			/* -----------------{ Font Options }----------------- */

				// widget title label
				$wp_customize->add_setting( 'neal_sidebar[wtitle-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[wtitle-font-label]', array(
							'label'    => esc_html__( 'Widget Title', 'neal' ),
							'section'  => 'section_sidebar_font',
							'priority' => 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_sidebar[wtitle-font-size]', array(
						'default'	=> 18,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[wtitle-font-size]', array(
							'settings'		=> 'neal_sidebar[wtitle-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_sidebar_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority'		=> 2
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_sidebar[wtitle-font-weight]', array(
					'default'	=> 500,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[wtitle-font-weight]', array(
							'settings'		=> 'neal_sidebar[wtitle-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_sidebar_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 3
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_sidebar[wtitle-font-italic]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_sidebar[wtitle-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'	=> 'section_sidebar_font',
					'settings'	=> 'neal_sidebar[wtitle-font-italic]',
					'type'		=> 'checkbox',
					'priority'	=> 4
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_sidebar[wtitle-font-uppercase]', array(
					'default'	=> true,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_sidebar[wtitle-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_sidebar_font',
					'settings'  => 'neal_sidebar[wtitle-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 5
				) );

				// categories image label
				$wp_customize->add_setting( 'neal_sidebar[categories-image-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_sidebar[categories-image-font-label]', array(
							'label'    => esc_html__( 'Categories Image', 'neal' ),
							'section'  => 'section_sidebar_font',
							'priority' => 6
						)
				) );

				// font size
				$wp_customize->add_setting('neal_sidebar[categories-image-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[categories-image-font-size]', array(
							'settings'		=> 'neal_sidebar[categories-image-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_sidebar_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 100
								               ),
					        'priority'		=> 7
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_sidebar[categories-image-font-weight]', array(
					'default'	=> 600,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_sidebar[categories-image-font-weight]', array(
							'settings'		=> 'neal_sidebar[categories-image-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_sidebar_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 8
					    )
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_sidebar[categories-image-font-uppercase]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_sidebar[categories-image-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_sidebar_font',
					'settings'  => 'neal_sidebar[categories-image-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 9
				) );


			/*
			***************************************************************
			* #Product
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// star rating label
				$wp_customize->add_setting( 'neal_product[star-rating-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[star-rating-label]', array(
							'label'    => esc_html__( 'Star Rating', 'neal' ),
							'section'  => 'section_product_general',
							'priority' => 1
						)
				) );

				// star rating position
				$wp_customize->add_setting( 'neal_product[star-rating-pos]', array(
					'default' 	=> 1,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_product[star-rating-pos]', array(	
					'label'		=> esc_html__( 'Position', 'neal' ),
					'settings' 	=> 'neal_product[star-rating-pos]',
					'section' 	=> 'section_product_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	1	=> esc_html__( 'Top', 'neal' ),
									2	=> esc_html__( 'Middle', 'neal' ),
									3	=> esc_html__( 'Bottom', 'neal' )
			            		   ),
					'priority'	=> 2
				) );

				// star rating align
				$wp_customize->add_setting( 'neal_product[star-rating-align]', array(
					'default' 	=> 'center',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_product[star-rating-align]', array(	
					'label'		=> esc_html__( 'Align', 'neal' ),
					'settings' 	=> 'neal_product[star-rating-align]',
					'section' 	=> 'section_product_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'  	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' )
			            			),
					'priority'	=> 3
				) );

				// sale label
				$wp_customize->add_setting( 'neal_product[sale-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[sale-label]', array(
							'label'    => esc_html__( 'Sale', 'neal' ),
							'section'  => 'section_product_general',
							'priority' => 4
						)
				) );

				// sale type
				$wp_customize->add_setting( 'neal_product[sale-type]', array(
					'default' 	=> 'off',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_product[sale-type]', array(	
					'label'		=> esc_html__( 'Type', 'neal' ),
					'settings' 	=> 'neal_product[sale-type]',
					'section' 	=> 'section_product_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'sale'	=> esc_html__( 'Sale', 'neal' ),
			                		'off' 	=> esc_html__( 'Number % off', 'neal' ),
			            		    ),
					'priority'	=> 5
				) );

				// title label
				$wp_customize->add_setting( 'neal_product[title-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[title-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_product_general',
							'priority' => 6
						)
				) );

				// title position
				$wp_customize->add_setting( 'neal_product[title-pos]', array(
					'default' 	=> 3,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_product[title-pos]', array(	
					'label'		=> esc_html__( 'Position', 'neal' ),
					'settings' 	=> 'neal_product[title-pos]',
					'section' 	=> 'section_product_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	1	=> esc_html__( 'Top', 'neal' ),
									2  	=> esc_html__( 'Middle', 'neal' ),
									3  	=> esc_html__( 'Bottom', 'neal' )
			            					),
					'priority'	=> 7
				) );

				// title align
				$wp_customize->add_setting( 'neal_product[title-align]', array(
					'default' 	=> 'center',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_product[title-align]', array(	
							'label'    => esc_html__( 'Align', 'neal' ),
							'settings' => 'neal_product[title-align]',
					        'section' => 'section_product_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' )
			            					),
					        'priority' => 8
					       )
				);

				// prices label
				$wp_customize->add_setting( 'neal_product[prices-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[prices-label]', array(
							'label'    => esc_html__( 'Prices', 'neal' ),
							'section'  => 'section_product_general',
							'priority' => 9
						)
				));

				// prices position
				$wp_customize->add_setting('neal_product[prices-pos]', array(
					'default' 	=> 3,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_product[prices-pos]', array(	
							'label'    => esc_html__( 'Position', 'neal' ),
							'settings' => 'neal_product[prices-pos]',
					        'section' => 'section_product_general',
					        'type' => 'select',
					        'choices'    => array(
			                					1	=> esc_html__( 'Top', 'neal' ),
												2  	=> esc_html__( 'Middle', 'neal' ),
												3  	=> esc_html__( 'Bottom', 'neal' )
			            					),
					        'priority' => 10
					       )
				);

				// prices align
				$wp_customize->add_setting('neal_product[prices-align]', array(
					   	'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_product[prices-align]', array(	
							'label'    => esc_html__( 'Align', 'neal' ),
							'settings' => 'neal_product[prices-align]',
					        'section' => 'section_product_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' )
			            					),
					        'priority' => 11
					       )
				);

			/* -----------------{ Spacing Options }----------------- */

				// star rating label
				$wp_customize->add_setting( 'neal_product[star-rating-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[star-rating-spac-label]', 
						array(
							'label'    => esc_html__( 'Star Rating', 'neal' ),
							'section'  => 'section_product_spacing',
							'priority' => 1
						)
				));

				// margin bottom
				$wp_customize->add_setting('neal_product[star-rating-spac-margin-bottom]', 
					array(
							'default' => 15,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_product[star-rating-spac-margin-bottom]',
						array(
							'settings' => 'neal_product[star-rating-spac-margin-bottom]',
					        'label' => esc_html__( 'Margin Bottom', 'neal' ),
					        'section' => 'section_product_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 2
					       )
				));

				// margin right
				$wp_customize->add_setting('neal_product[star-rating-spac-margin-right]', 
					array(
							'default' => 2,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_product[star-rating-spac-margin-right]',
						array(
							'settings' => 'neal_product[star-rating-spac-margin-right]',
					        'label' => esc_html__( 'Star Margin Right', 'neal' ),
					        'section' => 'section_product_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 3
					       )
				));

				// title label
				$wp_customize->add_setting( 'neal_product[title-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[title-spac-label]', 
						array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_product_spacing',
							'priority' => 4
						)
				));

				// margin bottom
				$wp_customize->add_setting('neal_product[title-spac-margin-bottom]', 
					array(
							'default' => 15,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_product[title-spac-margin-bottom]',
						array(
							'settings' => 'neal_product[title-spac-margin-bottom]',
					        'label' => esc_html__( 'Margin Bottom', 'neal' ),
					        'section' => 'section_product_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 5
					       )
				));

				// prices label
				$wp_customize->add_setting( 'neal_product[prices-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[prices-spac-label]', 
						array(
							'label'    => esc_html__( 'Prices', 'neal' ),
							'section'  => 'section_product_spacing',
							'priority' => 6
						)
				));

				// margin bottom
				$wp_customize->add_setting('neal_product[prices-spac-margin-bottom]', 
					array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_product[prices-spac-margin-bottom]',
						array(
							'settings' => 'neal_product[prices-spac-margin-bottom]',
					        'label' => esc_html__( 'Margin Bottom', 'neal' ),
					        'section' => 'section_product_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 7
					       )
				));

			/* -----------------{ Styling Options }----------------- */

				// star rating label
				$wp_customize->add_setting( 'neal_product[star-rating-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[star-rating-styl-label]', 
						array(
							'label'    => esc_html__( 'Star Rating', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 1
						)
				));

				// star color
				$wp_customize->add_setting('neal_product[star-rating-styl-star-color]', array(
						'default' 	=> '#C39F76',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[star-rating-styl-star-color]', 
								array(
									'label'		 => esc_html__( 'Star Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[star-rating-styl-star-color]',
									'priority' => 2
								) ) 
				);

				// star color 2
				$wp_customize->add_setting('neal_product[star-rating-styl-star-color-2]', array(
						'default' 	=> '#d4d4d4',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[star-rating-styl-star-color-2]', 
								array(
									'label'		 => esc_html__( 'Star Color 2', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[star-rating-styl-star-color-2]',
									'priority' => 3
								) ) 
				);

				// sale label
				$wp_customize->add_setting( 'neal_product[sale-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[sale-styl-label]', 
						array(
							'label'    => esc_html__( 'Sale', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 4
						)
				));

				// font color
				$wp_customize->add_setting('neal_product[sale-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[sale-styl-font-color]', 
								array(
									'label'    => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[sale-styl-font-color]',
									'priority' => 5
								) ) 
				);

				// add to cart label
				$wp_customize->add_setting( 'neal_product[add-to-cart-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[add-to-cart-styl-label]', 
						array(
							'label'    => esc_html__( 'Add to Cart', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 6
						)
				) );

				// font color
				$wp_customize->add_setting('neal_product[add-to-cart-styl-font-color]', array(
						'default' 	=> '#666666',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[add-to-cart-styl-font-color]', 
								array(
									'label'      => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[add-to-cart-styl-font-color]',
									'priority' => 7
								) ) 
				);

				// font hover color
				$wp_customize->add_setting('neal_product[add-to-cart-styl-font-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[add-to-cart-styl-font-hover-color]', 
								array(
									'label'      => esc_html__( 'Hover Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[add-to-cart-styl-font-hover-color]',
									'priority' => 8
								) ) 
				);

				// title label
				$wp_customize->add_setting( 'neal_product[title-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[title-styl-label]', 
						array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 9
						)
				));

				// color
				$wp_customize->add_setting('neal_product[title-styl-font-color]', array(
						'default' 	=> '#666666',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[title-styl-font-color]', 
								array(
									'label'		 => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[title-styl-font-color]',
									'priority' => 10
								) ) 
				);

				// hover color
				$wp_customize->add_setting('neal_product[title-styl-font-hover-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[title-styl-font-hover-color]', 
								array(
									'label'		 => esc_html__( 'Hover Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[title-styl-font-hover-color]',
									'priority' => 11
								) ) 
				);

				// special price label
				$wp_customize->add_setting( 'neal_product[special-price-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[special-price-styl-label]', 
						array(
							'label'    => esc_html__( 'Special price', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 12
						)
				));

				// color
				$wp_customize->add_setting('neal_product[special-price-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[special-price-styl-font-color]', 
								array(
									'label'		 => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[special-price-styl-font-color]',
									'priority' => 13
								) ) 
				);

				// old price label
				$wp_customize->add_setting( 'neal_product[old-price-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[old-price-styl-label]', 
						array(
							'label'    => esc_html__( 'Old price', 'neal' ),
							'section'  => 'section_product_styling',
							'priority' => 14
						)
				));

				// color
				$wp_customize->add_setting('neal_product[old-price-styl-font-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_product[old-price-styl-font-color]', 
								array(
									'label'		 => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_product_styling',
									'settings'   => 'neal_product[old-price-styl-font-color]',
									'priority' => 15
								) ) 
				);

			/* -----------------{ Font Options }----------------- */

				// star rating label
				$wp_customize->add_setting( 'neal_product[star-rating-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_product[star-rating-font-label]', 
						array(
							'label'    => esc_html__( 'Star Rating', 'neal' ),
							'section'  => 'section_product_font',
							'priority' => 1
						)
				));

				// font size
				$wp_customize->add_setting('neal_product[star-rating-font-size]', 
					array(
						'default' => 13,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[star-rating-font-size]', array(
							'settings' => 'neal_product[star-rating-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' => 8,
								                'max' => 80
								            ),
					        'priority' => 2
					    )
				) );

				// sale label
				$wp_customize->add_setting( 'neal_product[sale-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[sale-font-label]', array(
							'label'    => esc_html__( 'Sale', 'neal' ),
							'section'  => 'section_product_font',
							'priority' => 3
						)
				) );

				// font size
				$wp_customize->add_setting('neal_product[sale-font-size]', 
					array(
						'default' => 13,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[sale-font-size]', array(
							'settings' => 'neal_product[sale-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 5
					       )
				) );
				
				// title label
				$wp_customize->add_setting( 'neal_product[title-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[title-font-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_product_font',
							'priority' => 6
						)
				) );

				// font size
				$wp_customize->add_setting('neal_product[title-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[title-font-size]', array(
							'settings' => 'neal_product[title-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 7
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_product[title-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[title-font-weight]', array(
							'settings' => 'neal_product[title-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								               	'step'	=> 100
								            ),
					        'priority' => 8
					       )
				) );

				// italic
				$wp_customize->add_setting( 'neal_product[title-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_product[title-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_product_font',
					'settings'          => 'neal_product[title-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 9
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_product[title-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_product[title-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_product_font',
					'settings'          => 'neal_product[title-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 10
				) );

				// special price label
				$wp_customize->add_setting( 'neal_product[special-price-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[special-price-font-label]', array(
							'label'    => esc_html__( 'Special price', 'neal' ),
							'section'  => 'section_product_font',
							'priority' => 11
						)
				) );

				// font size
				$wp_customize->add_setting('neal_product[special-price-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[special-price-font-size]', array(
							'settings' => 'neal_product[special-price-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 12
					       )
				) );

				// old price label
				$wp_customize->add_setting( 'neal_product[old-price-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_product[old-price-font-label]', array(
							'label'    => esc_html__( 'Old price', 'neal' ),
							'section'  => 'section_product_font',
							'priority' => 13
						)
				) );

				// font size
				$wp_customize->add_setting('neal_product[old-price-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_product[old-price-font-size]', array(
							'settings' => 'neal_product[old-price-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_product_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 14
					       )
				) );

				// line through
				$wp_customize->add_setting( 'neal_product[old-price-font-line-through]', array(
					'default' 		=> true,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_product[old-price-font-line-through]', array(
					'label'    => esc_html__( 'Line Through', 'neal' ),
					'section'           => 'section_product_font',
					'settings'          => 'neal_product[old-price-font-line-through]',
					'type'				=> 'checkbox',
					'priority' => 15
				) );


			/*
			***************************************************************
			* #Blog Page
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// layout label
				$wp_customize->add_setting( 'neal_blog[layout-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[layout-label]', array(
							'label'		=> esc_html__( 'Layout', 'neal' ),
							'section'  	=> 'section_blog_general',
							'priority' 	=> 1
						)
				) );

				// layout (columns)
				$wp_customize->add_setting('neal_blog[layout-columns]', array(
					'default'	=> 3,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[layout-columns]', array(
					'settings'	=> 'neal_blog[layout-columns]',
					'label' 	=> esc_html__( 'Columns Rate', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'classic'  	=> esc_html__( 'Classic', 'neal' ),
									'list'  	=> esc_html__( 'List', 'neal' ),
									2  			=> esc_html__( '2 Columns', 'neal' ),
									3  			=> esc_html__( '3 Columns', 'neal' ),
									4  			=> esc_html__( '4 Columns', 'neal' ),
			            		   ),
					'priority' 	=> 2
				) );

				// sidebar label
				$wp_customize->add_setting( 'neal_blog[sidebar-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox',
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[sidebar-label]', array(
							'label'    => esc_html__( 'Sidebar', 'neal' ),
							'section'  => 'section_blog_general',
							'priority' => 3
						)
				) );

				// sidebar align
				$wp_customize->add_setting('neal_blog[sidebar-align]', array(
					'default'	=> 'sidebar-right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[sidebar-align]', array(
					'settings'	=> 'neal_blog[sidebar-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'sidebar-right'	=> esc_html__( 'Right', 'neal' ),
									'sidebar-left'  => esc_html__( 'Left', 'neal' ),
			            		   ),
					'priority' => 4
				) );

				// sticky
				$wp_customize->add_setting( 'neal_blog[sidebar-sticky]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[sidebar-sticky]', array(
					'label'		=> esc_html__( 'Sticky', 'neal' ),
					'section'   => 'section_blog_general',
					'type'		=> 'checkbox',
					'priority'	=> 5
				) );

				// categories label
				$wp_customize->add_setting( 'neal_blog[categories-label]', array( 
						'default'	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[categories-label]', array(
							'label'		=> esc_html__( 'Categories', 'neal' ),
							'section'  	=> 'section_blog_general',
							'priority' 	=> 6
						)
				) );

				// categories align
				$wp_customize->add_setting('neal_blog[categories-align]', array(
						'default'	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[categories-align]', array(
						'settings'	=> 'neal_blog[categories-align]',
						'label' 	=> esc_html__( 'Align', 'neal' ),
						'section' 	=> 'section_blog_general',
						'type' 		=> 'select',
						'choices'   => array(
			                	   		'left'		=> esc_html__( 'Left', 'neal' ),
										'center'	=> esc_html__( 'Center', 'neal' ),
										'right'  	=> esc_html__( 'Right', 'neal' ),
			            		      ),
					'priority'		=> 7
				) );

				// date label
				$wp_customize->add_setting( 'neal_blog[date-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[date-label]', array(
							'label'    => esc_html__( 'Date', 'neal' ),
							'section'  => 'section_blog_general',
							'priority' => 8
						)
				) );

				// date align
				$wp_customize->add_setting('neal_blog[date-align]', array(
					'default'	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[date-align]', array(
							'settings' => 'neal_blog[date-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_blog_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 9
					       )
				);

				// date icon
				$wp_customize->add_setting('neal_blog[date-icon]', array(
					'default' 	=> 'none',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[date-icon]', array(
							'settings' => 'neal_blog[date-icon]',
					        'label' => esc_html__( 'Select Icon', 'neal' ),
					        'section' => 'section_blog_general',
					        'type' => 'select',
					        'choices'    => array(
					        					'none'			=> '&#xf05e;',
					        					'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
			                					'ion-clock'		=> '&#xf26e;',
			                					'ion-ios-clock' => '&#xf403;',
			                					'ion-ios-clock-outline' => '&#xf402;',
			                					'ion-ios-time' => '&#xf4bf;',
			                					'ion-ios-time-outline' => '&#xf4be;',
			                					'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-clock-o'	=> '&#xf017;'

			            					),
					        'priority' => 10
					       )
				);

				// author label
				$wp_customize->add_setting( 'neal_blog[author-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[author-label]', array(
							'label'		=> esc_html__( 'Author', 'neal' ),
							'section'  	=> 'section_blog_general',
							'priority' 	=> 11
						)
				) );

				$wp_customize->add_control('neal_blog[author-pos]', array(
					'settings' 	=> 'neal_blog[author-pos]',
					'label' 	=> esc_html__( 'Position', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                			'below'	=> esc_html__( 'Below Media', 'neal' ),
										'above'	=> esc_html__( 'Above Media', 'neal' ),
			            		   ),
					'priority'	=> 12
				) );

				// author align
				$wp_customize->add_setting('neal_blog[author-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[author-align]', array(
					'settings'	=> 'neal_blog[author-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' ),
			            		   ),
					'priority'	=> 13
				) );

				// author icon
				$wp_customize->add_setting('neal_blog[author-icon]', array(
					'default' 	=> 'none',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[author-icon]', array(
					'settings'	=> 'neal_blog[author-icon]',
					'label' 	=> esc_html__( 'Select Icon', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
					        	   	'none'			=> '&#xf05e;',
					        		'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
			                		'ion-person'	=> '&#xf213;',
			                		'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                		'fa-user'		=> '&#xf007;'

			            			),
					'priority'	=> 14
				) );

				// title label
				$wp_customize->add_setting( 'neal_blog[title-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[title-label]', array(
							'label'		=> esc_html__( 'Title', 'neal' ),
							'section'  	=> 'section_blog_general',
							'priority' 	=> 15
						)
				) );

				// title align
				$wp_customize->add_setting('neal_blog[title-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[title-align]', array(
					'settings'	=> 'neal_blog[title-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' ),
			            		   ),
					'priority'	=> 16
				) );

				// description label
				$wp_customize->add_setting( 'neal_blog[description-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog[description-label]', array(
							'label'    => esc_html__( 'Description', 'neal' ),
							'section'  => 'section_blog_general',
							'priority' => 17
						)
				) );

				// description (display as)
				$wp_customize->add_setting('neal_blog[description-display_as]', array(
							'default' 	=> 'p_excerpt',
							'type'	  	=> 'option',
							'transport'	=> 'refresh',
							'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[description-display_as]', array(
							'settings' => 'neal_blog[description-display_as]',
					        'label' => esc_html__( 'Display As', 'neal' ),
					        'section' => 'section_blog_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'p_excerpt' => esc_html__( 'Post Excerpt', 'neal' ),
												'p_content'  => esc_html__( 'Post Content', 'neal' ),
			            					),
					        'priority' => 18
					       )
				);

				// description (excerpt length)
				$wp_customize->add_setting('neal_blog[description-excerpt_length]', array(
						'default'	=> 20,
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control('neal_blog[description-excerpt_length]', array(
							'settings' => 'neal_blog[description-excerpt_length]',
					        'label' => esc_html__( 'Excerpt Length', 'neal' ),
					        'section' => 'section_blog_general',
					        'type' => 'text',
					        'priority' => 19
					       )
				);

				// description (align)
				$wp_customize->add_setting('neal_blog[description-align]', array(
						'default'	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[description-align]', array(
							'settings' => 'neal_blog[description-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_blog_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 20
					       )
				);

				// read-more label
				$wp_customize->add_setting( 'neal_blog[read-more-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 
						'neal_blog[read-more-label]', 
						array(
							'label'    => esc_html__( 'Read More', 'neal' ),
							'section'  => 'section_blog_general',
							'priority' => 21
						)
				) );

				// read-more align
				$wp_customize->add_setting('neal_blog[read-more-align]', array(
					'default'	=> 'center',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog[read-more-align]', array(
					'settings'	=> 'neal_blog[read-more-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_blog_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left' => esc_html__( 'Left', 'neal' ),
									'center'  => esc_html__( 'Center', 'neal' ),
									'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					'priority' => 22
				) );

			/* -----------------{ Spacing Options }----------------- */

				// media label
				$wp_customize->add_setting( 'neal_blog[media-spac-label]', array( 
				    	'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[media-spac-label]', array(
							'label'    => esc_html__( 'Media', 'neal' ),
							'section'  => 'section_blog_spacing',
							'priority' => 1
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog[media-spac-margin-bottom]', array(
						'default'	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog[media-spac-margin-bottom]', array(
							'settings'		=> 'neal_blog[media-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 80
								             ),
					        'priority' 		=> 2
					       )
				) );

				// title label
				$wp_customize->add_setting( 'neal_blog[title-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[title-spac-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_blog_spacing',
							'priority' => 3
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog[title-spac-margin-bottom]', array(
					   	'default'	=> 5,
						'type'		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog[title-spac-margin-bottom]', array(
							'settings'		=> 'neal_blog[title-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 80
								               ),
					        'priority'		=> 4
					       )
				) );

				// meta items label
				$wp_customize->add_setting( 'neal_blog[meta-items-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[meta-items-spac-label]', array(
							'label'    => esc_html__( 'Meta Items', 'neal' ),
							'section'  => 'section_blog_spacing',
							'priority' => 5
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog[meta-items-spac-margin-bottom]', array(
							'default' => 10,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog[meta-items-spac-margin-bottom]',
						array(
							'settings' => 'neal_blog[meta-items-spac-margin-bottom]',
					        'label' => esc_html__( 'Margin Bottom', 'neal' ),
					        'section' => 'section_blog_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 6
					       )
				));

				// description label
				$wp_customize->add_setting( 'neal_blog[description-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[description-spac-label]', array(
							'label'    => esc_html__( 'Description', 'neal' ),
							'section'  => 'section_blog_spacing',
							'priority' => 7
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog[description-spac-margin-bottom]', 
					array(
							'default' => 15,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog[description-spac-margin-bottom]', array(
							'settings' => 'neal_blog[description-spac-margin-bottom]',
					        'label' => esc_html__( 'Margin Bottom', 'neal' ),
					        'section' => 'section_blog_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 70
								            ),
					        'priority' => 8
					       )
				) );


			/* -----------------{ Styling Options }----------------- */
				
				// title label
				$wp_customize->add_setting( 'neal_blog[title-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[title-styl-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_blog_styling',
							'priority' => 1
						)
				) );

				// font color
				$wp_customize->add_setting('neal_blog[title-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[title-styl-font-color]', array(
							'label'     => esc_html__( 'Color', 'neal' ),
							'section'   => 'section_blog_styling',
							'settings'	=> 'neal_blog[title-styl-font-color]',
							'priority'	=> 2
						) 
				) );

				// font hover color
				$wp_customize->add_setting('neal_blog[title-styl-font-hover-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[title-styl-font-hover-color]', array(
							'label'		=> esc_html__( 'Hover Color', 'neal' ),
							'section'	=> 'section_blog_styling',
							'settings'  => 'neal_blog[title-styl-font-hover-color]',
							'priority'	=> 3
						) 
				) );

				// meta label
				$wp_customize->add_setting( 'neal_blog[meta-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[meta-styl-label]', array(
							'label'    => esc_html__( 'Meta', 'neal' ),
							'section'  => 'section_blog_styling',
							'priority' => 4
						)
				) );

				// font color
				$wp_customize->add_setting('neal_blog[meta-styl-font-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[meta-styl-font-color]', array(
									'label'      => esc_html__( 'Font Color', 'neal' ),
									'section'    => 'section_blog_styling',
									'settings'   => 'neal_blog[meta-styl-font-color]',
									'priority' => 5
						) 
				) );

				// link color
				$wp_customize->add_setting('neal_blog[meta-styl-link-color]', array(
						'default' 	=> '#C39F76',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[meta-styl-link-color]', array(
									'label'      => esc_html__( 'Link Color', 'neal' ),
									'section'    => 'section_blog_styling',
									'settings'   => 'neal_blog[meta-styl-link-color]',
									'priority' => 6
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_blog[meta-styl-link-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[meta-styl-link-hover-color]', array(
									'label'      => esc_html__( 'Link Hover Color', 'neal' ),
									'section'    => 'section_blog_styling',
									'settings'   => 'neal_blog[meta-styl-link-hover-color]',
									'priority' => 7
						) 
				) );

				// icon color
				$wp_customize->add_setting('neal_blog[meta-styl-icon-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[meta-styl-icon-color]', array(
									'label'      => esc_html__( 'Icon Color', 'neal' ),
									'section'    => 'section_blog_styling',
									'settings'   => 'neal_blog[meta-styl-icon-color]',
									'priority' => 8
						) 
				) );

				// categories label
				$wp_customize->add_setting( 'neal_blog[categories-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[categories-styl-label]', array(
							'label'    => esc_html__( 'Categories', 'neal' ),
							'section'  => 'section_blog_styling',
							'priority' => 9
						)
				) );

				// font color
				$wp_customize->add_setting( 'neal_blog[categories-styl-font-color]', array(
					'default' 	=> '#fff',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[categories-styl-font-color]', array(
							'label'      => esc_html__( 'Font Color', 'neal' ),
							'section'    => 'section_blog_styling',
							'settings'   => 'neal_blog[categories-styl-font-color]',
							'priority'   => 10
						) 
				) );

				// font hover color
				$wp_customize->add_setting( 'neal_blog[categories-styl-font-hover-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[categories-styl-font-hover-color]', array(
							'label'      => esc_html__( 'Font Hover Color', 'neal' ),
							'section'    => 'section_blog_styling',
							'settings'   => 'neal_blog[categories-styl-font-hover-color]',
							'priority'   => 11
						) 
				) );

				// background color
				$wp_customize->add_setting( 'neal_blog[categories-styl-bg-color]', array(
					'default' 	=> '#22bb66',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[categories-styl-bg-color]', array(
							'label'      => esc_html__( 'Background Color', 'neal' ),
							'section'    => 'section_blog_styling',
							'settings'   => 'neal_blog[categories-styl-bg-color]',
							'priority'   => 12
						) 
				) );

				// background hover color
				$wp_customize->add_setting( 'neal_blog[categories-styl-bg-hover-color]', array(
					'default' 	=> '#22bb66',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[categories-styl-bg-hover-color]', array(
							'label'      => esc_html__( 'Background Hover Color', 'neal' ),
							'section'    => 'section_blog_styling',
							'settings'   => 'neal_blog[categories-styl-bg-hover-color]',
							'priority'   => 13
						) 
				) );

				// description label
				$wp_customize->add_setting( 'neal_blog[desc-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[desc-styl-label]', array(
							'label'    => esc_html__( 'Description', 'neal' ),
							'section'  => 'section_blog_styling',
							'priority' => 14
						)
				) );

				// font color
				$wp_customize->add_setting('neal_blog[desc-styl-font-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_blog[desc-styl-font-color-color]', array(
									'label'      => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_blog_styling',
									'settings'   => 'neal_blog[desc-styl-font-color]',
									'priority' => 15
						) 
				) );

			
			/* -----------------{ Font Options }----------------- */

				// title label
				$wp_customize->add_setting( 'neal_blog[title-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog[title-font-label]', array(
							'label'		=> esc_html__( 'Title', 'neal' ),
							'section'  	=> 'section_blog_font',
							'priority' 	=> 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_blog[title-font-size]', array(
					'default'	=> 24,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog[title-font-size]', array(
							'settings'		=> 'neal_blog[title-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_blog_font',
					        'input_attrs' 	=> array(
								               	'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 2
					       )
				) );

				// font weight
				$wp_customize->add_setting('neal_blog[title-font-weight]', 
					array(
						'default' => 500,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[title-font-weight]',
						array(
							'settings' => 'neal_blog[title-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 3
					       )
				));

				// line height
				$wp_customize->add_setting('neal_blog[title-font-line-height]', 
					array(
						'default' => 1.3,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[title-font-line-height]',
						array(
							'settings' => 'neal_blog[title-font-line-height]',
					        'label' => esc_html__( 'Line Height', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								             	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 4
					       )
				));

				// letter spacing
				$wp_customize->add_setting('neal_blog[title-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[title-font-letter-spacing]',
						array(
							'settings' => 'neal_blog[title-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 5
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_blog[title-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[title-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[title-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 6
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_blog[title-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[title-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[title-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 7
				) );

				// meta label
				$wp_customize->add_setting( 'neal_blog[meta-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog[meta-font-label]', 
						array(
							'label'    => esc_html__( 'Meta', 'neal' ),
							'section'  => 'section_blog_font',
							'priority' => 8
						)
				));

				// font size
				$wp_customize->add_setting('neal_blog[meta-font-size]', 
					array(
						'default' => 15,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[meta-font-size]',
						array(
							'settings' => 'neal_blog[meta-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 9
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_blog[meta-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[meta-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[meta-font-italic]',
					'type'			=> 'checkbox',
					'priority' => 10
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_blog[meta-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[meta-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[meta-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 11
				) );

				// categories label
				$wp_customize->add_setting( 'neal_blog[categories-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog[categories-font-label]', 
						array(
							'label'    => esc_html__( 'Categories', 'neal' ),
							'section'  => 'section_blog_font',
							'priority' => 12
						)
				));

				// font size
				$wp_customize->add_setting('neal_blog[categories-font-size]', 
					array(
						'default' => 14,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[categories-font-size]',
						array(
							'settings' => 'neal_blog[categories-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 13
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_blog[categories-font-weight]', 
					array(
						'default' => 600,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[categories-font-weight]',
						array(
							'settings' => 'neal_blog[categories-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 14
					       )
				));

				// letter spacing
				$wp_customize->add_setting('neal_blog[categories-font-letter-spacing]', 
					array(
						'default' => 0.5,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[categories-font-letter-spacing]',
						array(
							'settings' => 'neal_blog[categories-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 15
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_blog[categories-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[categories-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[categories-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 16
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_blog[categories-font-uppercase]', array(
					'default' 		=> true,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[categories-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[categories-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 17
				) );


				// description label
				$wp_customize->add_setting( 'neal_blog[desc-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog[desc-font-label]', 
						array(
							'label'    => esc_html__( 'Description', 'neal' ),
							'section'  => 'section_blog_font',
							'priority' => 18
						)
				));

				// font size
				$wp_customize->add_setting('neal_blog[desc-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[desc-font-size]',
						array(
							'settings' => 'neal_blog[desc-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 19
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_blog[desc-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[desc-font-weight]',
						array(
							'settings' => 'neal_blog[desc-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 20
					       )
				));

				// line height
				$wp_customize->add_setting('neal_blog[desc-font-line-height]', 
					array(
						'default' => 1.8,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[desc-font-line-height]',
						array(
							'settings' => 'neal_blog[desc-font-line-height]',
					        'label' => esc_html__( 'Line Height', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 21
					       )
				));

				// letter spacing
				$wp_customize->add_setting('neal_blog[desc-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog[desc-font-letter-spacing]',
						array(
							'settings' => 'neal_blog[desc-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_blog_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 22
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_blog[desc-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[desc-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[desc-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 23
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_blog[desc-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog[desc-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_blog_font',
					'settings'          => 'neal_blog[desc-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 24
				) );


			/*
			***************************************************************
			* #Blog Single
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// general header label
				$wp_customize->add_setting( 'neal_blog_single[general-post-label]', array(
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[general-post-label]', array(
							'label'    => esc_html__( 'General Post', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 1
						)
				) );

				// header layouts
				$wp_customize->add_setting( 'neal_blog_single[general-post-layout]', array(
					'default' 	=> 1,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_radio'
				) );

				$wp_customize->add_control( 'neal_blog_single[general-post-layout]', array(
					'settings'	=> 'neal_blog_single[general-post-layout]',
					'label' 	=> esc_html__( 'Post Layouts', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'select',
					'choices' 	=> array(
								   	1 	=> esc_html__( 'Layout 1', 'neal' ),
									2 	=> esc_html__( 'Layout 2', 'neal' ),
									3 	=> esc_html__( 'Layout 3', 'neal' ),
									4 	=> esc_html__( 'Layout 4', 'neal' ),
									5 	=> esc_html__( 'Layout 5', 'neal' ),
									6 	=> esc_html__( 'Layout 6', 'neal' ),
								   ),
					'priority'	=> 2
				) );

				// parallax image
				$wp_customize->add_setting( 'neal_blog_single[general-post-parallax]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[general-post-parallax]', array(
					'label'		=> esc_html__( 'Parallax Image', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 3
				) );

				// rotate caption text
				$wp_customize->add_setting( 'neal_blog_single[general-post-rotate-cap-text]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[general-post-rotate-cap-text]', array(
					'label'		=> esc_html__( 'Rotate Caption Text', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 4
				) );

				// sidebar label
				$wp_customize->add_setting( 'neal_blog_single[sidebar-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_blog_single[sidebar-label]', 
						array(
							'label'    => esc_html__( 'Sidebar', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 5
						)
				));

				// sidebar align
				$wp_customize->add_setting('neal_blog_single[sidebar-align]', array(
						'default'	=> 'sidebar-right',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog_single[sidebar-align]', array(
							'settings' => 'neal_blog_single[sidebar-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'sidebar-right' => esc_html__( 'Right', 'neal' ),
												'sidebar-left'  => esc_html__( 'Left', 'neal' ),
			            					),
					        'priority' => 6
					       )
				);

				// sticky
				$wp_customize->add_setting( 'neal_blog_single[sidebar-sticky]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sidebar-sticky]', array(
					'label'		=> esc_html__( 'Sticky', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 7
				) );

				// featured img label
				$wp_customize->add_setting( 'neal_blog_single[featured-img-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog_single[featured-img-label]', array(
							'label'    => esc_html__( 'Featured Image', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 8
						)
				) );

				// title label
				$wp_customize->add_setting( 'neal_blog_single[title-label]', array( 
				    	'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog_single[title-label]', 
						array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 9
						)
				));

				// title position
				$wp_customize->add_setting('neal_blog_single[title-pos]', array(
						'default' 	=> 'above',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog_single[title-pos]', array(
							'settings' => 'neal_blog_single[title-pos]',
					        'label' => esc_html__( 'Position', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'below'		=> esc_html__( 'Below Media', 'neal' ),
												'above'  	=> esc_html__( 'Above Media', 'neal' ),
												'in-media' 	=> esc_html__( 'In Media', 'neal' ),
			            					),
					        'priority' => 10
					       )
				);

				// title align
				$wp_customize->add_setting('neal_blog_single[title-align]', array(
						'default'	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog_single[title-align]', array(
							'settings' => 'neal_blog_single[title-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 11
					       )
				);

				// breadcrumb label
				$wp_customize->add_setting( 'neal_blog_single[bread-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_blog_single[bread-label]', 
						array(
							'label'    => esc_html__( 'Breadcrumb', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 12
						)
				));

				// breadcrumb position
				$wp_customize->add_setting('neal_blog_single[bread-pos]', array(
						'default' 	=> 'above',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );
				$wp_customize->add_control('neal_blog_single[bread-pos]', array(
							'settings' => 'neal_blog_single[bread-pos]',
					        'label' => esc_html__( 'Position', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'below'		=> esc_html__( 'Below Media', 'neal' ),
												'above'  	=> esc_html__( 'Above Media', 'neal' ),
												'in-media' 	=> esc_html__( 'In Media', 'neal' ),
			            					),
					        'priority' => 13
					       )
				);

				// breadcrumb align
				$wp_customize->add_setting( 'neal_blog_single[bread-align]', array(
					'default' => 'center',
					'type'	  => 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_blog_single[bread-align]', array(
					'settings'	=> 'neal_blog_single[bread-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'  	=> esc_html__( 'Center', 'neal' ),
									'right'  	=> esc_html__( 'Right', 'neal' ),
			            		  ),
					'priority'	=> 14
				) );

				// meta
				$wp_customize->add_setting( 'neal_blog_single[meta-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog_single[meta-label]', array(
							'label'    => esc_html__( 'Meta', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 15
						)
				) );

				// meta position
				$wp_customize->add_setting('neal_blog_single[meta-pos]', array(
					'default' 	=> 'below',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );
				$wp_customize->add_control('neal_blog_single[meta-pos]', array(
							'settings' => 'neal_blog_single[meta-pos]',
					        'label' => esc_html__( 'Position', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'below'		=> esc_html__( 'Below Media', 'neal' ),
												'above'  	=> esc_html__( 'Above Media', 'neal' ),
												'in-media' 	=> esc_html__( 'In Media', 'neal' ),
			            					),
					        'priority' => 16
					       )
				);

				// category
				$wp_customize->add_setting( 'neal_blog_single[meta-category]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[meta-category]', array(
					'label'		=> esc_html__( 'Category', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 17
				) );

				// date
				$wp_customize->add_setting( 'neal_blog_single[meta-date]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[meta-date]', array(
					'label'		=> esc_html__( 'Date', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 18
				) );

				// author
				$wp_customize->add_setting( 'neal_blog_single[meta-author]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[meta-author]', array(
					'label'		=> esc_html__( 'Author', 'neal' ),
					'section'   => 'section_blog_single_general',
					'type'		=> 'checkbox',
					'priority'	=> 19
				) );

				// meta align
				$wp_customize->add_setting('neal_blog_single[meta-align]', array(
						'default' => 'center',
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog_single[meta-align]', array(
							'settings' => 'neal_blog_single[meta-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'center'  => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 20
					       )
				);

				// sharing
				$wp_customize->add_setting( 'neal_blog_single[sharing-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog_single[sharing-label]', array(
							'label'    => esc_html__( 'Social Sharing', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 21
						)
				) );

				// facebook
				$wp_customize->add_setting( 'neal_blog_single[sharing-fb]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sharing-fb]', array(
					'label'		=> esc_html__( 'Facebook', 'neal' ),
					'section'   => 'section_blog_single_general',
					'settings'  => 'neal_blog_single[sharing-fb]',
					'type'		=> 'checkbox',
					'priority'	=> 22
				) );

				// twitter
				$wp_customize->add_setting( 'neal_blog_single[sharing-tw]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sharing-tw]', array(
					'label'		=> esc_html__( 'Twitter', 'neal' ),
					'section'   => 'section_blog_single_general',
					'settings'  => 'neal_blog_single[sharing-tw]',
					'type'		=> 'checkbox',
					'priority'	=> 23
				) );

				// google plus
				$wp_customize->add_setting( 'neal_blog_single[sharing-google-p]', array(
						'default' 	=> true,
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sharing-google-p]', array(
						'label'		=> esc_html__( 'Google Plus', 'neal' ),
						'section'   => 'section_blog_single_general',
						'settings'  => 'neal_blog_single[sharing-google-p]',
						'type'		=> 'checkbox',
						'priority'	=> 24
				) );

				// linkedin
				$wp_customize->add_setting( 'neal_blog_single[sharing-linke]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sharing-linke]', array(
					'label'		=> esc_html__( 'Linkedin', 'neal' ),
					'section'   => 'section_blog_single_general',
					'settings'  => 'neal_blog_single[sharing-linke]',
					'type'		=> 'checkbox',
					'priority'	=> 25
				) );

				// pinterest
				$wp_customize->add_setting( 'neal_blog_single[sharing-pint]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[sharing-pint]', array(
					'label'		=> esc_html__( 'Pinterest', 'neal' ),
					'section'   => 'section_blog_single_general',
					'settings'  => 'neal_blog_single[sharing-pint]',
					'type'		=> 'checkbox',
					'priority'	=> 26
				) );

				// subscribe box
				$wp_customize->add_setting( 'neal_blog_single[subscribe-box-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog_single[subscribe-box-label]', array(
							'label'    => esc_html__( 'Subscribe Box', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 27
						)
				) );

				// title
				$wp_customize->add_setting( 'neal_blog_single[subscribe-box-title]', array(
					'default'	=> esc_html__('Never miss a story!', 'neal' ),
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );
				
				$wp_customize->add_control( 'neal_blog_single[subscribe-box-title]', array(
					'settings' 	=> 'neal_blog_single[subscribe-box-title]',
					'label' 	=> esc_html__( 'Title', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'text',
					'priority' 	=> 28
				) );

				// description
				$wp_customize->add_setting( 'neal_blog_single[subscribe-box-desc]', array(
					'default'	=> esc_html__('Sign up for our newsletter and get the best stories delivered to your inbox.', 'neal' ),
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );
				
				$wp_customize->add_control( 'neal_blog_single[subscribe-box-desc]', array(
					'settings' 	=> 'neal_blog_single[subscribe-box-desc]',
					'label' 	=> esc_html__( 'Description', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'text',
					'priority' 	=> 29
				) );

				// subscribe placeholder
				$wp_customize->add_setting( 'neal_blog_single[subscribe-box-place]', array(
					'default'	=> esc_html__('Email Address', 'neal' ),
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );
				
				$wp_customize->add_control( 'neal_blog_single[subscribe-box-place]', array(
					'settings' 	=> 'neal_blog_single[subscribe-box-place]',
					'label' 	=> esc_html__( 'Subscribe Placeholder', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'text',
					'priority' 	=> 30
				) );

				// subscribe button
				$wp_customize->add_setting( 'neal_blog_single[subscribe-box-button]', array(
					'default'	=> esc_html__('Sign Me Up', 'neal' ),
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );
				
				$wp_customize->add_control( 'neal_blog_single[subscribe-box-button]', array(
					'settings' 	=> 'neal_blog_single[subscribe-box-button]',
					'label' 	=> esc_html__( 'Subscribe Button', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'text',
					'priority' 	=> 31
				) );

				// author box
				$wp_customize->add_setting( 'neal_blog_single[author-box-label]', array( 
					   	'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_blog_single[author-box-label]', 
						array(
							'label'    => esc_html__( 'Author Box', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 32
						)
				));

				// author box image size
				$wp_customize->add_setting('neal_blog_single[author-box-img-size]', array(
						'default' 	=> '120',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_blog_single[author-box-img-size]', array(
							'settings' => 'neal_blog_single[author-box-img-size]',
					        'label' => esc_html__( 'Avatar Size', 'neal' ),
					        'section' => 'section_blog_single_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'55' => esc_html__( 'Small', 'neal' ),
												'70'  => esc_html__( 'Medium', 'neal' ),
												'120'  => esc_html__( 'Large', 'neal' ),

			            					),
					        'priority' => 33
					       )
				);

				// related posts
				$wp_customize->add_setting( 'neal_blog_single[related-posts-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_blog_single[related-posts-label]', array(
							'label'    => esc_html__( 'Related Posts', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 34
						)
				) );

				// title
				$wp_customize->add_setting( 'neal_blog_single[related-posts-title]', array(
					'default'	=> esc_html__('You Might Also Like', 'neal' ),
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );
				
				$wp_customize->add_control( 'neal_blog_single[related-posts-title]', array(
					'settings' 	=> 'neal_blog_single[related-posts-title]',
					'label' 	=> esc_html__( 'Title', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'text',
					'priority' 	=> 35
				) );

				// position
				$wp_customize->add_setting( 'neal_blog_single[related-posts-pos]', array(
					'default' 	=> 'below-author-box',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_blog_single[related-posts-pos]', array(
					'settings'	=> 'neal_blog_single[related-posts-pos]',
					'label' 	=> esc_html__( 'Position', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'select',
					'choices'   => array(
									'below-author-box' => esc_html__( 'Below Author Box', 'neal' ),
								   	'below-comments'   => esc_html__( 'Below Comments', 'neal' ),
								   ),
					'priority' => 36
				) );

				// post navigation
				$wp_customize->add_setting( 'neal_blog_single[post-navigation-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				   	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_blog_single[post-navigation-label]', 
						array(
							'label'    => esc_html__( 'Post navigation', 'neal' ),
							'section'  => 'section_blog_single_general',
							'priority' => 37
						)
				));

				// post navigation title
				$wp_customize->add_setting( 'neal_blog_single[post-navigation-title]', array(
					'default'	=> true,
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[post-navigation-title]', array(
					'settings'	=> 'neal_blog_single[post-navigation-title]',
					'label' 	=> esc_html__( 'Title', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'checkbox',
					'priority' 	=> 38
				) );

				// post navigation icon
				$wp_customize->add_setting( 'neal_blog_single[post-navigation-icon-fa]', array(
					'default' 	=> 'long-arrow',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_blog_single[post-navigation-icon-fa]', array(
					'settings'	=> 'neal_blog_single[post-navigation-icon-fa]',
					'label' 	=> esc_html__( 'Select Icon', 'neal' ),
					'section' 	=> 'section_blog_single_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'none'			 => '&#xf05e;',
			                		'angle'			 => '&#xf104; &#xf105;',
			                		'angle-double'	 => '&#xf100; &#xf101;',
			                		'chevron'		 => '&#xf053; &#xf054;',
			                		'long-arrow'	 => '&#xf177; &#xf178;',
			                		'chevron-circle' => '&#xf137; &#xf138;',
			                		'arrow-circle'   => '&#xf0a8; &#xf0a9;',
			                		'arrow-circle-o' => '&#xf190; &#xf18e;',
			                	    ),
					'priority' => 39
				) );

			/* -----------------{ Spacing Options }----------------- */

				// media
				$wp_customize->add_setting( 'neal_blog_single[media-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[media-spac-label]', array(
							'label'    => esc_html__( 'Media', 'neal' ),
							'section'  => 'section_blog_single_spacing',
							'priority' => 1
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog_single[media-spac-margin-bottom]', array(
						'default' 	=> 30,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[media-spac-margin-bottom]', array(
							'settings' 		=> 'neal_blog_single[media-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' => 2
					       )
				) );

				// title
				$wp_customize->add_setting( 'neal_blog_single[title-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[title-spac-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_blog_single_spacing',
							'priority' => 3
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog_single[title-spac-margin-bottom]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[title-spac-margin-bottom]', array(
							'settings' 		=> 'neal_blog_single[title-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' => 4
					       )
				) );

				// breadcrumb
				$wp_customize->add_setting( 'neal_blog_single[bread-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[bread-spac-label]', array(
							'label'    => esc_html__( 'Breadcrumb', 'neal' ),
							'section'  => 'section_blog_single_spacing',
							'priority' => 5
						)
				) );

				// margin bottom
				$wp_customize->add_setting( 'neal_blog_single[bread-spac-margin-bottom]', array(
					'default' 	=> 0,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[bread-spac-margin-bottom]', array(
							'settings' 		=> 'neal_blog_single[bread-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' => 6
					       )
				) );

				// meta
				$wp_customize->add_setting( 'neal_blog_single[meta-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[meta-spac-label]', array(
							'label'    => esc_html__( 'Meta', 'neal' ),
							'section'  => 'section_blog_single_spacing',
							'priority' => 7
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_blog_single[meta-spac-margin-bottom]', array(
						'default' 	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[meta-spac-margin-bottom]', array(
							'settings' 		=> 'neal_blog_single[meta-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' => 8
					       )
				) );

				// caption image
				$wp_customize->add_setting( 'neal_blog_single[caption-image-spac-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_blog_single[caption-image-spac-label]', array(
							'label'    => esc_html__( 'Caption Image', 'neal' ),
							'section'  => 'section_blog_single_spacing',
							'priority' => 9
						)
				) );

				// caption image margin left
				$wp_customize->add_setting('neal_blog_single[caption-image-spac-margin-left]', array(
						'default' 	=> -80,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[caption-image-spac-margin-left]', array(
							'settings' 		=> 'neal_blog_single[caption-image-spac-margin-left]',
					        'label' 		=> esc_html__( 'Margin Left', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => -280,
								                'max' => 280
								               ),
					        'priority' => 10
					       )
				) );

				// caption image margin right
				$wp_customize->add_setting('neal_blog_single[caption-image-spac-margin-right]', array(
						'default' 	=> -80,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_blog_single[caption-image-spac-margin-right]', array(
							'settings' 		=> 'neal_blog_single[caption-image-spac-margin-right]',
					        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_blog_single_spacing',
					        'input_attrs' 	=> array(
								                'min' => -280,
								                'max' => 280
								               ),
					        'priority' => 11
					       )
				) );

			/* -----------------{ Font Options }----------------- */

				// title label
				$wp_customize->add_setting( 'neal_blog_single[title-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog_single[title-font-label]', 
						array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_blog_single_font',
							'priority' => 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_blog_single[title-font-size]', 
					array(
						'default' => 50,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[title-font-size]',
						array(
							'settings' => 'neal_blog_single[title-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 2
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_blog_single[title-font-weight]', 
					array(
						'default' => 600,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[title-font-weight]',
						array(
							'settings' => 'neal_blog_single[title-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 3
					       )
				));

				// line height
				$wp_customize->add_setting('neal_blog_single[title-font-line-height]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[title-font-line-height]',
						array(
							'settings' => 'neal_blog_single[title-font-line-height]',
					        'label' => esc_html__( 'Line Height', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								             	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 4
					       )
				));

				// letter-spacing
				$wp_customize->add_setting('neal_blog_single[title-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[title-font-letter-spacing]',
						array(
							'settings' => 'neal_blog_single[title-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 5
					       )
				));

				// uppercase
				$wp_customize->add_setting( 'neal_blog_single[title-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_blog_single[title-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_blog_single_font',
					'settings'          => 'neal_blog_single[title-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 6
				) );

				// content label
				$wp_customize->add_setting( 'neal_blog_single[content-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_blog_single[content-font-label]', 
						array(
							'label'    => esc_html__( 'Content', 'neal' ),
							'section'  => 'section_blog_single_font',
							'priority' => 7
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_blog_single[content-font-family]', array(
						'default' 	=> 'Open+Sans',
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_select'
					)
				);
				$wp_customize->add_control(
							new Neal_Google_Fonts_Control( 
								$wp_customize, 
								'neal_blog_single[content-font-family]', 
								array(
									'label'    => esc_html__( 'Font Family', 'neal' ),
									'section'  => 'section_blog_single_font',
									'priority' => 8
									)
				));

				// font size
				$wp_customize->add_setting('neal_blog_single[content-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[content-font-size]',
						array(
							'settings' => 'neal_blog_single[content-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 9
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_blog_single[content-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[content-font-weight]',
						array(
							'settings' => 'neal_blog_single[content-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 10
					       )
				));

				// line height
				$wp_customize->add_setting('neal_blog_single[content-font-line-height]', 
					array(
						'default' => 1.8,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[content-font-line-height]',
						array(
							'settings' => 'neal_blog_single[content-font-line-height]',
					        'label' => esc_html__( 'Line Height', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 11
					       )
				));

				// letter-spacing
				$wp_customize->add_setting('neal_blog_single[content-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_blog_single[content-font-letter-spacing]',
						array(
							'settings' => 'neal_blog_single[content-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_blog_single_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 12
					       )
				));


			/*
			***************************************************************
			* #Shop Page
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// layout label
				$wp_customize->add_setting( 'neal_shop[layout-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[layout-label]', 
						array(
							'label'    => esc_html__( 'Layout', 'neal' ),
							'section'  => 'section_shop_general',
							'priority' => 1
						)
				));

				// layout (columns)
				$wp_customize->add_setting('neal_shop[layout-columns]', array(
						'default' 	=> 3,
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[layout-columns]', array(
							'settings' => 'neal_shop[layout-columns]',
					        'label' => esc_html__( 'Columns Rate', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
												2  => esc_html__( '2 Columns', 'neal' ),
												3  => esc_html__( '3 Columns', 'neal' ),
												4  => esc_html__( '4 Columns', 'neal' ),
			            					),
					        'priority' => 2
					       )
				);

				// sidebar label
				$wp_customize->add_setting( 'neal_shop[sidebar-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_shop[sidebar-label]', 
						array(
							'label'    => esc_html__( 'Sidebar', 'neal' ),
							'section'  => 'section_shop_general',
							'priority' => 3
						)
				));

				// sidebar align
				$wp_customize->add_setting('neal_shop[sidebar-align]', array(
					'default' 	=> 'sidebar-right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[sidebar-align]', array(
							'settings' => 'neal_shop[sidebar-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'sidebar-left' => esc_html__( 'Left', 'neal' ),
												'sidebar-right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 4
					       )
				);

				// toolbar label
				$wp_customize->add_setting( 'neal_shop[tbar-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_shop[tbar-label]', 
						array(
							'label'    => esc_html__( 'Toolbar(Grid/List View)', 'neal' ),
							'section'  => 'section_shop_general',
							'priority' => 5
						)
				));

				// toolbar align
				$wp_customize->add_setting('neal_shop[tbar-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[tbar-align]', array(
							'settings' => 'neal_shop[tbar-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 6
					       )
				);

				// grid icon
				$wp_customize->add_setting('neal_shop[tbar-grid-icon]', array(
						'default' 	=> 'th-large',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[tbar-grid-icon]', array(
							'settings' => 'neal_shop[tbar-grid-icon]',
					        'label' => esc_html__( 'Grid Icon', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
												'fawe-icons' => esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-th-large' => '&#xf009;',
			                					'fa-th' => '&#xf00a;'
			            					),
					        'priority' => 7
					       )
				);

				// list icon
				$wp_customize->add_setting('neal_shop[tbar-list-icon]', array(
						'default' 	=> 'th-list',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[tbar-list-icon]', array(
							'settings' => 'neal_shop[tbar-list-icon]',
					        'label' => esc_html__( 'List Icon', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
												'fawe-icons' => esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-list' => '&#xf03a;',
			                					'fa-th-list' => '&#xf00b;'
			            					),
					        'priority' => 8
					       )
				);

				// icon size
				$wp_customize->add_setting('neal_shop[tbar-icon-size]', 
					array(
						'default' => 21,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[tbar-icon-size]',
						array(
							'settings' => 'neal_shop[tbar-icon-size]',
					        'label' => esc_html__( 'Icon Size', 'neal' ),
					        'section' => 'section_shop_general',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 30
								            ),
					        'priority' => 9
					       )
				));

				// result text label
				$wp_customize->add_setting( 'neal_shop[result-t-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_shop[result-t-label]', 
						array(
							'label'    => esc_html__( 'Result Text', 'neal' ),
							'section'  => 'section_shop_general',
							'priority' => 10
						)
				));

				// result text align
				$wp_customize->add_setting('neal_shop[result-t-align]', array(
						'default' 	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[result-t-align]', array(
							'settings' => 'neal_shop[result-t-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
			                					'center' => esc_html__( 'Center', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 11
					       )
				);

				// ordering label
				$wp_customize->add_setting( 'neal_shop[ordering-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_shop[ordering-label]', 
						array(
							'label'    => esc_html__( 'Ordering', 'neal' ),
							'section'  => 'section_shop_general',
							'priority' => 12
						)
				));

				// ordering align
				$wp_customize->add_setting('neal_shop[ordering-align]', array(
						'default' 	=> 'right',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop[ordering-align]', array(
							'settings' => 'neal_shop[ordering-align]',
					        'label' => esc_html__( 'Align', 'neal' ),
					        'section' => 'section_shop_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'left' => esc_html__( 'Left', 'neal' ),
												'right'  => esc_html__( 'Right', 'neal' ),
			            					),
					        'priority' => 13
					       )
				);

			/* -----------------{ Spacing Options }----------------- */

				// toolbar label
				$wp_customize->add_setting( 'neal_shop[tbar-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[tbar-spac-label]', 
						array(
							'label'    => esc_html__( 'Toolbar(Grid/List View)', 'neal' ),
							'section'  => 'section_shop_spacing',
							'priority' => 1
						)
				));

				// margin left
				$wp_customize->add_setting('neal_shop[tbar-spac-margin-left]', array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[tbar-spac-margin-left]',
						array(
							'settings' => 'neal_shop[tbar-spac-margin-left]',
					        'label' => esc_html__( 'Margin Left', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 2
					       )
				));

				// margin right
				$wp_customize->add_setting('neal_shop[tbar-spac-margin-right]', array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[tbar-spac-margin-right]',
						array(
							'settings' => 'neal_shop[tbar-spac-margin-right]',
					        'label' => esc_html__( 'Margin Right', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 3
					       )
				));		

				// result text label
				$wp_customize->add_setting( 'neal_shop[result-t-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[result-t-spac-label]', 
						array(
							'label'    => esc_html__( 'Result Text', 'neal' ),
							'section'  => 'section_shop_spacing',
							'priority' => 4
						)
				));

				// margin left
				$wp_customize->add_setting('neal_shop[result-t-spac-margin-left]', array(
							'default' => 30,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[result-t-spac-margin-left]',
						array(
							'settings' => 'neal_shop[result-t-spac-margin-left]',
					        'label' => esc_html__( 'Margin Left', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 5
					       )
				));

				// margin right
				$wp_customize->add_setting('neal_shop[result-t-spac-margin-right]', array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[result-t-spac-margin-right]',
						array(
							'settings' => 'neal_shop[result-t-spac-margin-right]',
					        'label' => esc_html__( 'Margin Right', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 6
					       )
				));

				// ordering label
				$wp_customize->add_setting( 'neal_shop[ordering-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[ordering-spac-label]', 
						array(
							'label'    => esc_html__( 'Ordering', 'neal' ),
							'section'  => 'section_shop_spacing',
							'priority' => 7
						)
				));

				// margin left
				$wp_customize->add_setting('neal_shop[ordering-spac-margin-left]', array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[ordering-spac-margin-left]',
						array(
							'settings' => 'neal_shop[ordering-spac-margin-left]',
					        'label' => esc_html__( 'Margin Left', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 8
					       )
				));

				// margin right
				$wp_customize->add_setting('neal_shop[ordering-spac-margin-right]', array(
							'default' => 0,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[ordering-spac-margin-right]',
						array(
							'settings' => 'neal_shop[ordering-spac-margin-right]',
					        'label' => esc_html__( 'Margin Right', 'neal' ),
					        'section' => 'section_shop_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 280
								            ),
					        'priority' => 9
					       )
				));

			/* -----------------{ Styling Options }----------------- */

				// toolbar label
				$wp_customize->add_setting( 'neal_shop[tbar-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[tbar-styl-label]', 
						array(
							'label'    => esc_html__( 'Toolbar(Grid/List View)', 'neal' ),
							'section'  => 'section_shop_styling',
							'priority' => 1
						)
				));

				// link color
				$wp_customize->add_setting('neal_shop[tbar-styl-link-color]', array(
						'default' 	=> '#999',
						'type'	 	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[tbar-styl-link-color]', 
								array(
									'label'    => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[tbar-styl-link-color]',
									'priority' => 3
								) ) 
				);

				// link active color
				$wp_customize->add_setting('neal_shop[tbar-styl-link-active-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[tbar-styl-link-active-color]', 
								array(
									'label'    => esc_html__( 'Active Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[tbar-styl-link-active-color]',
									'priority' => 4
								) ) 
				);

				// link hover color
				$wp_customize->add_setting('neal_shop[tbar-styl-link-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[tbar-styl-link-hover-color]', 
								array(
									'label'    => esc_html__( 'Hover Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[tbar-styl-link-hover-color]',
									'priority' => 5
								) ) 
				);

				// result text label
				$wp_customize->add_setting( 'neal_shop[result-t-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[result-t-styl-label]', 
						array(
							'label'    => esc_html__( 'Result Text', 'neal' ),
							'section'  => 'section_shop_styling',
							'priority' => 6
						)
				));

				// font color
				$wp_customize->add_setting('neal_shop[result-t-styl-font-color]', array(
					   	'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[result-t-styl-font-color]', 
								array(
									'label'      => esc_html__( 'Font Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[result-t-styl-font-color]',
									'priority' => 7
								) ) 
				);

				// ordering label
				$wp_customize->add_setting( 'neal_shop[ordering-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[ordering-styl-label]', 
						array(
							'label'    => esc_html__( 'Ordering', 'neal' ),
							'section'  => 'section_shop_styling',
							'priority' => 8
						)
				));

				// color
				$wp_customize->add_setting('neal_shop[ordering-styl-color]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[ordering-styl-color]', 
								array(
									'label'      => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[ordering-styl-color]',
									'priority' => 9
								) ) 
				);

				// hover color
				$wp_customize->add_setting('neal_shop[ordering-styl-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[ordering-styl-hover-color]', 
								array(
									'label'      => esc_html__( 'Hover Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[ordering-styl-hover-color]',
									'priority' => 10
								) ) 
				);

				// filter slider label
				$wp_customize->add_setting( 'neal_shop[filter-sld-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[filter-sld-styl-label]', 
						array(
							'label'    => esc_html__( 'Filter Slider', 'neal' ),
							'section'  => 'section_shop_styling',
							'priority' => 11
						)
				));

				// color
				$wp_customize->add_setting('neal_shop[filter-sld-styl-color]', array(
						'default' 	=> '#9E9E9E',
						'type'	 	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[filter-sld-styl-color]', 
								array(
									'label'      => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[filter-sld-styl-color]',
									'priority' => 12
								) ) 
				);

				// color 2
				$wp_customize->add_setting('neal_shop[filter-sld-styl-color-2]', array(
						'default' 	=> '#eee',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_shop[filter-sld-styl-color-2]', 
								array(
									'label'      => esc_html__( 'Color 2', 'neal' ),
									'section'    => 'section_shop_styling',
									'settings'   => 'neal_shop[filter-sld-styl-color-2]',
									'priority' => 13
								) ) 
				);

			/* -----------------{ Font Options }----------------- */

				// result text label
				$wp_customize->add_setting( 'neal_shop[result-t-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[result-t-font-label]', 
						array(
							'label'    => esc_html__( 'Result Text', 'neal' ),
							'section'  => 'section_shop_font',
							'priority' => 1
						)
				));

				// font size
				$wp_customize->add_setting('neal_shop[result-t-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[result-t-font-size]',
						array(
							'settings' => 'neal_shop[result-t-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 2
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_shop[result-t-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[result-t-font-weight]',
						array(
							'settings' => 'neal_shop[result-t-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								                'min'	=> 100,
								                'max' 	=> 900,
								                'stpe'	=> 100
								            ),
					        'priority' => 3
					       )
				));

				// letter spacing
				$wp_customize->add_setting('neal_shop[result-t-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[result-t-font-letter-spacing]',
						array(
							'settings' => 'neal_shop[result-t-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 4
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_shop[result-t-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop[result-t-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_shop_font',
					'settings'          => 'neal_shop[result-t-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 5
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_shop[result-t-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop[result-t-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_shop_font',
					'settings'          => 'neal_shop[result-t-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 6
				) );

				// ordering label
				$wp_customize->add_setting( 'neal_shop[ordering-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_shop[ordering-font-label]', 
						array(
							'label'    => esc_html__( 'Ordering', 'neal' ),
							'section'  => 'section_shop_font',
							'priority' => 7
						)
				));

				// font size
				$wp_customize->add_setting('neal_shop[ordering-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[ordering-font-size]',
						array(
							'settings' => 'neal_shop[ordering-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 8
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_shop[ordering-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[ordering-font-weight]',
						array(
							'settings' => 'neal_shop[ordering-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								             	'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 9
					       )
				));

				// letter spacing
				$wp_customize->add_setting('neal_shop[ordering-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_shop[ordering-font-letter-spacing]',
						array(
							'settings' => 'neal_shop[ordering-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_shop_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 10
					       )
				));

				// italic
				$wp_customize->add_setting( 'neal_shop[ordering-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop[ordering-font-italic]', array(
					'label'    => esc_html__( 'Italic', 'neal' ),
					'section'           => 'section_shop_font',
					'settings'          => 'neal_shop[ordering-font-italic]',
					'type'				=> 'checkbox',
					'priority' => 11
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_shop[ordering-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop[ordering-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_shop_font',
					'settings'          => 'neal_shop[ordering-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 12
				) );


			/*
			***************************************************************
			* #Shop Single
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// sidebar label
				$wp_customize->add_setting( 'neal_shop_single[sidebar-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_shop_single[sidebar-label]', array(
							'label'    => esc_html__( 'Sidebar', 'neal' ),
							'section'  => 'section_shop_single_general',
							'priority' => 1
						)
				) );

				// sidebar align
				$wp_customize->add_setting( 'neal_shop_single[sidebar-align]', array(
					'default'	=> 'sidebar-right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_shop_single[sidebar-align]', array(
					'settings'	=> 'neal_shop_single[sidebar-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_shop_single_general',
					'type' 		=> 'select',
					'choices'   => array(
			                			'sidebar-right' => esc_html__( 'Right', 'neal' ),
										'sidebar-left'  => esc_html__( 'Left', 'neal' ),
			            		   ),
					'priority' => 2
				) );

				// product images label
				$wp_customize->add_setting( 'neal_shop_single[images-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[images-label]', 
						array(
							'label'    => esc_html__( 'Product Images', 'neal' ),
							'section'  => 'section_shop_single_general',
							'priority' => 3
						)
				) );

				// product images align
				$wp_customize->add_setting('neal_shop_single[images-align]', array(
						'default' 	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[images-align]', array(
						'settings'	=> 'neal_shop_single[images-align]',
					    'label' 	=> esc_html__( 'Align', 'neal' ),
					    'section' 	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'left' => esc_html__( 'Left', 'neal' ),
										'right'  => esc_html__( 'Right', 'neal' ),
			            				),
					    'priority' => 4
				) );

				// product thumbnails label
				$wp_customize->add_setting( 'neal_shop_single[thumbs-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[thumbs-label]', array(
							'label'		=> esc_html__( 'Product Thumbnails', 'neal' ),
							'section'  	=> 'section_shop_single_general',
							'priority' 	=> 5
						)
				) );

				// product thumbnail position
				$wp_customize->add_setting('neal_shop_single[thumbs-pos]', array(
						'default' 	=> 'horizontal',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[thumbs-pos]', array(
						'settings'	=> 'neal_shop_single[thumbs-pos]',
					    'label' 	=> esc_html__( 'Position', 'neal' ),
					    'section' 	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'vertical'		=> esc_html__( 'Vertical', 'neal' ),
										'horizontal'  	=> esc_html__( 'Horizontal', 'neal' ),
			            			    ),
					    'priority'	=> 6
				) );

				// product thumbnail align
				$wp_customize->add_setting('neal_shop_single[thumbs-align]', array(
						'default' 	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[thumbs-align]', array(
						'settings'	=> 'neal_shop_single[thumbs-align]',
					    'label' 	=> esc_html__( 'Align', 'neal' ),
					    'section' 	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'left'	=> esc_html__( 'Left', 'neal' ),
										'right'	=> esc_html__( 'Right', 'neal' ),
			            				),
					    'priority'	=> 7
				) );

				// summary content
				$wp_customize->add_setting( 'neal_shop_single[summary-content-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[summary-content-label]', array(
							'label'    => esc_html__( 'Summary content', 'neal' ),
							'section'  => 'section_shop_single_general',
							'priority' => 8
						)
				) );

				// summary content align
				$wp_customize->add_setting('neal_shop_single[summary-content-align]', array(
						'default' 	=> 'center',
						'type'	 	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[summary-content-align]', array(
						'settings'	=> 'neal_shop_single[summary-content-align]',
					    'label'		=> esc_html__( 'Align', 'neal' ),
					    'section'	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'left'		=> esc_html__( 'Left', 'neal' ),
										'center'  	=> esc_html__( 'Center', 'neal' ),
										'right'  	=> esc_html__( 'Right', 'neal' ),
			            			   ),
					    'priority'	=> 9
				) );

				// social sharing
				$wp_customize->add_setting( 'neal_shop_single[social-sharing-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[social-sharing-label]', array(
							'label'    => esc_html__( 'Social Sharing', 'neal' ),
							'section'  => 'section_shop_single_general',
							'priority' => 10
						)
				) );

				// facebook
				$wp_customize->add_setting( 'neal_shop_single[sharing-fb]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[sharing-fb]', array(
					'label'		=> esc_html__( 'Facebook', 'neal' ),
					'section'   => 'section_shop_single_general',
					'settings'  => 'neal_shop_single[sharing-fb]',
					'type'		=> 'checkbox',
					'priority'	=> 11
				) );

				// twitter
				$wp_customize->add_setting( 'neal_shop_single[sharing-tw]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[sharing-tw]', array(
					'label'		=> esc_html__( 'Twitter', 'neal' ),
					'section'   => 'section_shop_single_general',
					'settings'  => 'neal_shop_single[sharing-tw]',
					'type'		=> 'checkbox',
					'priority'	=> 12
				) );

				// google plus
				$wp_customize->add_setting( 'neal_shop_single[sharing-google-p]', array(
						'default' 	=> true,
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[sharing-google-p]', array(
						'label'		=> esc_html__( 'Google Plus', 'neal' ),
						'section'   => 'section_shop_single_general',
						'settings'  => 'neal_shop_single[sharing-google-p]',
						'type'		=> 'checkbox',
						'priority'	=> 13
				) );

				// linkedin
				$wp_customize->add_setting( 'neal_shop_single[sharing-linke]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[sharing-linke]', array(
					'label'		=> esc_html__( 'Linkedin', 'neal' ),
					'section'   => 'section_shop_single_general',
					'settings'  => 'neal_shop_single[sharing-linke]',
					'type'		=> 'checkbox',
					'priority'	=> 14
				) );

				// pinterest
				$wp_customize->add_setting( 'neal_shop_single[sharing-pint]', array(
					'default' 	=> true,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[sharing-pint]', array(
					'label'		=> esc_html__( 'Pinterest', 'neal' ),
					'section'   => 'section_shop_single_general',
					'settings'  => 'neal_shop_single[sharing-pint]',
					'type'		=> 'checkbox',
					'priority'	=> 15
				) );

				// social sharing align
				$wp_customize->add_setting('neal_shop_single[social-sharing-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[social-sharing-align]', array(
						'settings'	=> 'neal_shop_single[social-sharing-align]',
					    'label' 	=> esc_html__( 'Align', 'neal' ),
					    'section' 	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'left'		=> esc_html__( 'Left', 'neal' ),
										'center'  	=> esc_html__( 'Center', 'neal' ),
										'right'  	=> esc_html__( 'Right', 'neal' ),
			            			   ),
					    'priority'	=> 16
				) );

				// tabs label
				$wp_customize->add_setting( 'neal_shop_single[tabs-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[tabs-label]', array(
							'label'    => esc_html__( 'Tabs', 'neal' ),
							'section'  => 'section_shop_single_general',
							'priority' => 17
						)
				) );

				// tabs align
				$wp_customize->add_setting('neal_shop_single[tabs-align]', array(
						'default' 	=> 'center',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[tabs-align]', array(
						'settings'	=> 'neal_shop_single[tabs-align]',
					    'label' 	=> esc_html__( 'Align', 'neal' ),
					    'section' 	=> 'section_shop_single_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'left'		=> esc_html__( 'Left', 'neal' ),
										'center'  	=> esc_html__( 'Center', 'neal' ),
										'right'  	=> esc_html__( 'Right', 'neal' ),
			            			   ),
					    'priority'	=> 18
				) );


			/* -----------------{ Spacing Options }----------------- */

				// title
				$wp_customize->add_setting( 'neal_shop_single[title-spac-label]', array(
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[title-spac-label]', array(
							'label'		=> esc_html__( 'Title', 'neal' ),
							'section'  	=> 'section_shop_single_spacing',
							'priority' 	=> 1
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_shop_single[title-spac-margin-bottom]', array(
						'default'	=> 5,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[title-spac-margin-bottom]', array(
							'settings'		=> 'neal_shop_single[title-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 2
					    )
				) );

				// product rating
				$wp_customize->add_setting( 'neal_shop_single[product-rating-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[product-rating-spac-label]', array(
							'label'		=> esc_html__( 'Product Rating', 'neal' ),
							'section'  	=> 'section_shop_single_spacing',
							'priority' 	=> 3
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_shop_single[product-rating-spac-margin-bottom]', array(
					'default'	=> 15,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[product-rating-spac-margin-bottom]', array(
							'settings'		=> 'neal_shop_single[product-rating-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 4
					    )
				) );

				// product prices
				$wp_customize->add_setting( 'neal_shop_single[product-prices-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[product-prices-spac-label]', array(
							'label'		=> esc_html__( 'Product Prices', 'neal' ),
							'section'  	=> 'section_shop_single_spacing',
							'priority' 	=> 5
						)
				) );

				// margin bottom
				$wp_customize->add_setting('neal_shop_single[product-prices-spac-margin-bottom]', array(
					'default'	=> 25,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[product-prices-spac-margin-bottom]', array(
							'settings'		=> 'neal_shop_single[product-prices-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 6
					    )
				) );

				// product meta
				$wp_customize->add_setting( 'neal_shop_single[product-meta-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[product-meta-spac-label]', array(
							'label'		=> esc_html__( 'Product Meta', 'neal' ),
							'section'  	=> 'section_shop_single_spacing',
							'priority' 	=> 7
						)
				) );

				// margin top
				$wp_customize->add_setting('neal_shop_single[product-meta-spac-margin-top]', array(
					'default'	=> 25,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[product-meta-spac-margin-top]', array(
							'settings'		=> 'neal_shop_single[product-meta-spac-margin-top]',
					        'label' 		=> esc_html__( 'Margin Top', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 8
					    )
				) );

				// tabs tab
				$wp_customize->add_setting( 'neal_shop_single[tabs-tab-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[tabs-tab-spac-label]', array(
							'label'		=> esc_html__( 'Tabs Tab', 'neal' ),
							'section'  	=> 'section_shop_single_spacing',
							'priority' 	=> 9
						)
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_shop_single[tabs-tab-spac-padding-top-bottom]', array(
						'default'	=> 16,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-spac-padding-top-bottom]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 10
					    )
				) );

				// padding right-left
				$wp_customize->add_setting('neal_shop_single[tabs-tab-spac-padding-right-left]', array(
						'default'	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-spac-padding-right-left]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 11
					    )
				) );

				// margin-right
				$wp_customize->add_setting('neal_shop_single[tabs-tab-spac-margin-right]', array(
						'default'	=> 30,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-spac-margin-right]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-spac-margin-right]',
					        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_shop_single_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								            ),
					        'priority' 		=> 12
					    )
				) );
				

			/* -----------------{ Styling Options }----------------- */

				// title
				$wp_customize->add_setting( 'neal_shop_single[title-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[title-styl-label]', array(
							'label'		=> esc_html__( 'Title', 'neal' ),
							'section'  	=> 'section_shop_single_styling',
							'priority' 	=> 1
						)
				) );

				// font color
				$wp_customize->add_setting('neal_shop_single[title-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[title-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[title-styl-font-color]',
							'priority' 	=> 2
						) 
				) );

				// product prices
				$wp_customize->add_setting( 'neal_shop_single[product-prices-styl-label]', array(
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[product-prices-styl-label]', array(
							'label'		=> esc_html__( 'Product Prices', 'neal' ),
							'section'  	=> 'section_shop_single_styling',
							'priority' 	=> 3
						)
				) );

				// color
				$wp_customize->add_setting('neal_shop_single[product-prices-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[product-prices-styl-font-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[product-prices-styl-font-color]',
							'priority' 	=> 4
						) 
				) );

				// color 2
				$wp_customize->add_setting('neal_shop_single[product-prices-styl-font-color-2]', array(
						'default' 	=> '#999',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[product-prices-styl-font-color-2]', array(
							'label'		=> esc_html__( 'Color 2', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[product-prices-styl-font-color-2]',
							'priority' 	=> 5
						) 
				) );

				// tabs tab
				$wp_customize->add_setting( 'neal_shop_single[tabs-tab-styl-label]', array(
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-label]', array(
							'label'		=> esc_html__( 'Tabs Tab', 'neal' ),
							'section'  	=> 'section_shop_single_styling',
							'priority' 	=> 6
						)
				) );

				// link color
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-link-color]', array(
						'default' 	=> '#A8A7A8',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[tabs-tab-styl-link-color]',
							'priority' 	=> 7
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-link-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-link-hover-color]', array(
							'label'		=> esc_html__( 'Link Hover Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[tabs-tab-styl-link-hover-color]',
							'priority' 	=> 8
						) 
				) );

				// link active color
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-link-active-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-link-active-color]', array(
							'label'		=> esc_html__( 'Link Active Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[tabs-tab-styl-link-active-color]',
							'priority' 	=> 9
						) 
				) );

				// active border-bottom label
				$wp_customize->add_setting( 'neal_shop_single[tabs-tab-styl-child-border-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-child-border-label]', array(
							'label'    => esc_html__( 'Active Border Bottom', 'neal' ),
							'section'  => 'section_shop_single_styling',
							'priority' => 10
						)
				) );

				// border bottom size
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-child-border-bottom-size]', array(
						'default' 	=> 2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-child-border-bottom-size]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-styl-child-border-bottom-size]',
					        'label' 		=> esc_html__( 'Size', 'neal' ),
					        'section' 		=> 'section_shop_single_styling',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 15
								               ),
					        'priority'		=> 11
					    )
				) );

				// border bottom style
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-child-border-bottom-style]', array(
						'default' 	=> 'solid',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_shop_single[tabs-tab-styl-child-border-bottom-style]', array(
						'label'      => esc_html__( 'Style', 'neal' ),
						'section'    => 'section_shop_single_styling',
						'settings'   => 'neal_shop_single[tabs-tab-styl-child-border-bottom-style]',
						'type' 		=> 'select',
						'choices' 	=> array(
									   	'solid'		=> esc_html__( 'Solid', 'neal' ),
										'dotted'	=> esc_html__( 'Dotted', 'neal' ),
										'dashed' 	=> esc_html__( 'Dashed', 'neal' ),
										'double' 	=> esc_html__( 'Double', 'neal' ),
										'groove' 	=> esc_html__( 'Groove', 'neal' ),
									   ),
						'priority'	=> 12
				) );

				// border bottom color
				$wp_customize->add_setting('neal_shop_single[tabs-tab-styl-child-border-bottom-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_shop_single[tabs-tab-styl-child-border-bottom-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_shop_single_styling',
							'settings'  => 'neal_shop_single[tabs-tab-styl-child-border-bottom-color]',
							'priority' 	=> 13
						) 
					) );


			/* -----------------{ Font Options }----------------- */

				// title
				$wp_customize->add_setting( 'neal_shop_single[title-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[title-font-label]', array(
							'label'    => esc_html__( 'Title', 'neal' ),
							'section'  => 'section_shop_single_font',
							'priority' => 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_shop_single[title-font-size]', array(
					   	'default' => 33,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[title-font-size]', array(
							'settings'		=> 'neal_shop_single[title-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								                ),
					        'priority' 		=> 2
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_shop_single[title-font-weight]', array(
						'default' 	=> 600,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[title-font-weight]', array(
							'settings'		=> 'neal_shop_single[title-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 3
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_shop_single[title-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[title-font-line-height]', array(
							'settings'		=> 'neal_shop_single[title-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 4
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_shop_single[title-font-letter-spacing]', array(
						'default' 	=> 1.5,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[title-font-letter-spacing]', array(
							'settings'		=> 'neal_shop_single[title-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority'		=> 5
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_shop_single[title-font-italic]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[title-font-italic]', array(
						'label'		=> esc_html__( 'Italic', 'neal' ),
						'section'   => 'section_shop_single_font',
						'settings'  => 'neal_shop_single[title-font-italic]',
						'type'		=> 'checkbox',
						'priority' 	=> 6
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_shop_single[title-font-uppercase]', array(
						'default' 		=> false,
						'type' 			=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[title-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_shop_single_font',
						'settings'  => 'neal_shop_single[title-font-uppercase]',
						'type'		=> 'checkbox',
						'priority' 	=> 7
				) );

				// underline
				$wp_customize->add_setting( 'neal_shop_single[title-font-underline]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[title-font-underline]', array(
						'label'		=> esc_html__( 'Underline', 'neal' ),
						'section'   => 'section_shop_single_font',
						'settings'  => 'neal_shop_single[title-font-underline]',
						'type'		=> 'checkbox',
						'priority' 	=> 8
				) );


				// product prices
				$wp_customize->add_setting( 'neal_shop_single[product-prices-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[product-prices-font-label]', array(
							'label'    => esc_html__( 'Porduct Prices', 'neal' ),
							'section'  => 'section_shop_single_font',
							'priority' => 9
						)
				) );

				// font size
				$wp_customize->add_setting('neal_shop_single[product-prices-font-size]', array(
						'default' 	=> 18,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[product-prices-font-size]', array(
							'settings'		=> 'neal_shop_single[product-prices-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority'		=> 10
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_shop_single[product-prices-font-weight]', array(
						'default' 	=> 600,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[product-prices-font-weight]', array(
							'settings'		=> 'neal_shop_single[product-prices-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								               	'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 11
					    )
				) );

				// tabs tab
				$wp_customize->add_setting( 'neal_shop_single[tabs-tab-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_shop_single[tabs-tab-font-label]', array(
							'label'    => esc_html__( 'Tabs Tab', 'neal' ),
							'section'  => 'section_shop_single_font',
							'priority' => 12
						)
				) );

				// font size
				$wp_customize->add_setting('neal_shop_single[tabs-tab-font-size]', array(
						'default' 	=> 17,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-font-size]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								                ),
					        'priority'		=> 13
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_shop_single[tabs-tab-font-weight]', array(
						'default' 	=> 400,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_shop_single[tabs-tab-font-weight]', array(
							'settings'		=> 'neal_shop_single[tabs-tab-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_shop_single_font',
					        'input_attrs' 	=> array(
								             	'min'	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 14
					    )
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_shop_single[tabs-tab-font-uppercase]', array(
						'default' 	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_shop_single[tabs-tab-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_shop_single_font',
						'settings'  => 'neal_shop_single[tabs-tab-font-uppercase]',
						'type'		=> 'checkbox',
						'priority' 	=> 15
				) );


			/*
			***************************************************************
			* #Homepage
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// sidebar 1 label
				$wp_customize->add_setting( 'neal_homepage[sidebar-1-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[sidebar-1-label]', array(
							'label'    => esc_html__( 'Sidebar 1', 'neal' ),
							'section'  => 'section_homepage_general',
							'priority' => 1
						)
				) );

				// align
				$wp_customize->add_setting( 'neal_homepage[sidebar-1-align]', array(
					'default'	=> 'right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-1-align]', array(
					'settings'	=> 'neal_homepage[sidebar-1-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_homepage_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'right'	=> esc_html__( 'Right', 'neal' ),
									'left'  => esc_html__( 'Left', 'neal' ),
			            		   ),
					'priority' => 2
				) );

				// sticky
				$wp_customize->add_setting( 'neal_homepage[sidebar-1-sticky]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-1-sticky]', array(
					'label'		=> esc_html__( 'Sticky', 'neal' ),
					'section'   => 'section_homepage_general',
					'type'		=> 'checkbox',
					'priority'	=> 3
				) );

				// sidebar 2 label
				$wp_customize->add_setting( 'neal_homepage[sidebar-2-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[sidebar-2-label]', array(
							'label'    => esc_html__( 'Sidebar 2', 'neal' ),
							'section'  => 'section_homepage_general',
							'priority' => 4
						)
				) );

				// align
				$wp_customize->add_setting( 'neal_homepage[sidebar-2-align]', array(
					'default'	=> 'right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-2-align]', array(
					'settings'	=> 'neal_homepage[sidebar-2-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_homepage_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'right'	=> esc_html__( 'Right', 'neal' ),
									'left'  => esc_html__( 'Left', 'neal' ),
			            		   ),
					'priority' => 5
				) );

				// sticky
				$wp_customize->add_setting( 'neal_homepage[sidebar-2-sticky]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-2-sticky]', array(
					'label'		=> esc_html__( 'Sticky', 'neal' ),
					'section'   => 'section_homepage_general',
					'type'		=> 'checkbox',
					'priority'	=> 6
				) );

				// sidebar 3 label
				$wp_customize->add_setting( 'neal_homepage[sidebar-3-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[sidebar-3-label]', array(
							'label'    => esc_html__( 'Sidebar 3', 'neal' ),
							'section'  => 'section_homepage_general',
							'priority' => 7
						)
				) );

				// align
				$wp_customize->add_setting( 'neal_homepage[sidebar-3-align]', array(
					'default'	=> 'right',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-3-align]', array(
					'settings'	=> 'neal_homepage[sidebar-3-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_homepage_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'right'	=> esc_html__( 'Right', 'neal' ),
									'left'  => esc_html__( 'Left', 'neal' ),
			            		   ),
					'priority' => 8
				) );

				// sticky
				$wp_customize->add_setting( 'neal_homepage[sidebar-3-sticky]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_homepage[sidebar-3-sticky]', array(
					'label'		=> esc_html__( 'Sticky', 'neal' ),
					'section'   => 'section_homepage_general',
					'type'		=> 'checkbox',
					'priority'	=> 9
				) );

			/* -----------------{ Spacing Options }----------------- */

				// newsletter label
				$wp_customize->add_setting( 'neal_homepage[newsletter-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[newsletter-spac-label]', array(
							'label'    => esc_html__( 'Newsletter', 'neal' ),
							'section'  => 'section_homepage_spacing',
							'priority' => 1
						)
				) );

				// margin top
				$wp_customize->add_setting( 'neal_homepage[newsletter-spac-margin-top]', array(
					'default'	=> 0,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[newsletter-spac-margin-top]', array(
							'settings'		=> 'neal_homepage[newsletter-spac-margin-top]',
					        'label' 		=> esc_html__( 'Margin Top', 'neal' ),
					        'section' 		=> 'section_homepage_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 2
					    )
				) );

				// margin bottom
				$wp_customize->add_setting( 'neal_homepage[newsletter-spac-margin-bottom]', array(
					'default'	=> 0,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[newsletter-spac-margin-bottom]', array(
							'settings'		=> 'neal_homepage[newsletter-spac-margin-bottom]',
					        'label' 		=> esc_html__( 'Margin Bottom', 'neal' ),
					        'section' 		=> 'section_homepage_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 3
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_homepage[general-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[general-styl-label]', array(
							'label'		=> esc_html__( 'General', 'neal' ),
							'section'  	=> 'section_homepage_styling',
							'priority' 	=> 1
						)
				) );

				// title first letter
				$wp_customize->add_setting( 'neal_homepage[general-styl-tfl-color]', array(
					'default' 	=> '#eaeaea',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[general-styl-tfl-color]', array(
							'label'		=> esc_html__( 'Title First Letter Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[general-styl-tfl-color]',
							'priority' 	=> 2
						) 
				) );

				// full posts label
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[full-posts-styl-label]', array(
							'label'		=> esc_html__( 'Full Posts', 'neal' ),
							'section'  	=> 'section_homepage_styling',
							'priority' 	=> 3
						)
				) );

				// background color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-bg-color]', array(
					'default' 	=> '#E7F2F8',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-bg-color]',
							'priority' 	=> 4
						) 
				) );

				// title first letter
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-tfl-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-tfl-color]', array(
							'label'		=> esc_html__( 'Title First Letter Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-tfl-color]',
							'priority' 	=> 5
						) 
				) );

				// font color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-font-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-font-color]',
							'priority' 	=> 6
						) 
				) );

				// post title color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-post-title-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-post-title-color]', array(
							'label'		=> esc_html__( 'Post Title Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-post-title-color]',
							'priority' 	=> 7
						) 
				) );

				// post title hover color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-post-title-hover-color]', array(
					'default' 	=> '#9a9a9a',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-post-title-hover-color]', array(
							'label'		=> esc_html__( 'Post Title Hover Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-post-title-hover-color]',
							'priority' 	=> 8
						) 
				) );

				// read-more color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-read-more-color]', array(
					'default' 	=> '#9a9a9a',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-read-more-color]', array(
							'label'		=> esc_html__( 'Read More Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-read-more-color]',
							'priority' 	=> 9
						) 
				) );

				// read-more hover color
				$wp_customize->add_setting( 'neal_homepage[full-posts-styl-read-more-hover-color]', array(
					'default' 	=> '#9a9a9a',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[full-posts-styl-read-more-hover-color]', array(
							'label'		=> esc_html__( 'Read More Hover Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[full-posts-styl-read-more-hover-color]',
							'priority' 	=> 10
						) 
				) );

				// newsletter label
				$wp_customize->add_setting( 'neal_homepage[newsletter-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[newsletter-styl-label]', array(
							'label'		=> esc_html__( 'Newsletter', 'neal' ),
							'section'  	=> 'section_homepage_styling',
							'priority' 	=> 11
						)
				) );

				// background color
				$wp_customize->add_setting( 'neal_homepage[newsletter-styl-bg-color]', array(
					'default' 	=> '#E7F2F8',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[newsletter-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[newsletter-styl-bg-color]',
							'priority' 	=> 12
						) 
				) );

				// title color
				$wp_customize->add_setting( 'neal_homepage[newsletter-styl-title-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[newsletter-styl-title-color]', array(
							'label'		=> esc_html__( 'Title Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[newsletter-styl-title-color]',
							'priority' 	=> 13
						) 
				) );

				// description color
				$wp_customize->add_setting( 'neal_homepage[newsletter-styl-desc-color]', array(
					'default' 	=> '#000',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_homepage[newsletter-styl-desc-color]', array(
							'label'		=> esc_html__( 'Description Color', 'neal' ),
							'section'   => 'section_homepage_styling',
							'settings'  => 'neal_homepage[newsletter-styl-desc-color]',
							'priority' 	=> 14
						) 
				) );

			/* -----------------{ Font Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_homepage[general-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[general-font-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 1
						)
				));

				// section title font size
				$wp_customize->add_setting( 'neal_homepage[general-section-title-font-size]', array(
					'default'	=> 40,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[general-section-title-font-size]', array(
							'settings'		=> 'neal_homepage[general-section-title-font-size]',
					        'label' 		=> esc_html__( 'Section Title F-Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 250
								           	 	),
					        'priority' 		=> 2
					)
				) );

				// title first letter font size
				$wp_customize->add_setting( 'neal_homepage[general-tfl-font-size]', array(
					'default'	=> 120,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[general-tfl-font-size]', array(
							'settings'		=> 'neal_homepage[general-tfl-font-size]',
					        'label' 		=> esc_html__( 'Title First Letter Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 250
								           	 	),
					        'priority' 		=> 3
					)
				) );

				// carousel posts label
				$wp_customize->add_setting( 'neal_homepage[carousel-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[carousel-posts-font-label]', array(
							'label'    => esc_html__( 'Carousel Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 4
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[carousel-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[carousel-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[carousel-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 5
					)
				) );

				// full featured post label
				$wp_customize->add_setting( 'neal_homepage[full-featured-post-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[full-featured-post-font-label]', array(
							'label'    => esc_html__( 'Full Featured Post', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 6
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[full-featured-post-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[full-featured-post-title-font-size]', array(
							'settings'		=> 'neal_homepage[full-featured-post-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 7
					)
				) );

				// masonry posts label
				$wp_customize->add_setting( 'neal_homepage[masonry-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[masonry-posts-font-label]', array(
							'label'    => esc_html__( 'Masonry Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 8
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[masonry-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[masonry-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[masonry-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 9
					)
				) );

				// second title font size
				$wp_customize->add_setting( 'neal_homepage[masonry-posts-second-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[masonry-posts-second-title-font-size]', array(
							'settings'		=> 'neal_homepage[masonry-posts-second-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 10
					)
				) );

				// 2 grid posts label
				$wp_customize->add_setting( 'neal_homepage[2-grid-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[2-grid-posts-font-label]', array(
							'label'    => esc_html__( '2 Grid Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 11
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[2-grid-posts-title-font-size]', array(
					'default'	=> 30,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[2-grid-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[2-grid-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 12
					)
				) );

				// full posts label
				$wp_customize->add_setting( 'neal_homepage[full-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[full-posts-font-label]', array(
							'label'    => esc_html__( 'Full Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 13
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[full-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[full-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[full-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 14
					)
				) );

				// list posts label
				$wp_customize->add_setting( 'neal_homepage[list-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[list-posts-font-label]', array(
							'label'    => esc_html__( 'List Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 15
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[list-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[list-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[list-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 16
					)
				) );

				// slider posts label
				$wp_customize->add_setting( 'neal_homepage[slider-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[slider-posts-font-label]', array(
							'label'    => esc_html__( 'Slider Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 17
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[slider-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[slider-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[slider-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 18
					)
				) );

				// 3 grid posts label
				$wp_customize->add_setting( 'neal_homepage[3-grid-posts-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_homepage[3-grid-posts-font-label]', array(
							'label'    => esc_html__( '3 Grid Posts', 'neal' ),
							'section'  => 'section_homepage_font',
							'priority' => 19
						)
				));

				// title font size
				$wp_customize->add_setting( 'neal_homepage[3-grid-posts-title-font-size]', array(
					'default'	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_homepage[3-grid-posts-title-font-size]', array(
							'settings'		=> 'neal_homepage[3-grid-posts-title-font-size]',
					        'label' 		=> esc_html__( 'Title Font Size', 'neal' ),
					        'section' 		=> 'section_homepage_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								           	 	),
					        'priority' 		=> 20
					)
				) );


			/*
			***************************************************************
			* #Comments
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// display on blog
				$wp_customize->add_setting( 'neal_comments[blog-display-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_comments[blog-display-label]', array(
							'label'    => esc_html__( 'Display on Blog', 'neal' ),
							'section'  => 'section_comments_general',
							'priority' => 1
						)
				) );

				// comment type
				$wp_customize->add_setting( 'neal_comments[comment-type-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[comment-type-label]', array(
							'label'    => esc_html__( 'Comment Type', 'neal' ),
							'section'  => 'section_comments_general',
							'priority' => 2
						)
				) );

				// comment type
				$wp_customize->add_setting( 'neal_comments[comment-type]', array(
					'default' 	=> '70',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control( 'neal_comments[comment-type]', array(
					'settings'	=> 'neal_comments[comment-type]',
					'section' 	=> 'section_comments_general',
					'type' 		=> 'select',
					'choices'   => array(
			                	   	'default'	=> esc_html__( 'Default', 'neal' ),
									'disqus'  	=> esc_html__( 'Disqus', 'neal' ),
									'facebook'  => esc_html__( 'Facebook', 'neal' ),

			            			   ),
					'priority'	=> 3
				) );

				// disqus shortname
				$wp_customize->add_setting( 'neal_comments[comment-type-disqus-shortname]', array(
					'default'	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );

				$wp_customize->add_control( 'neal_comments[comment-type-disqus-shortname]', array(
					'settings'	=> 'neal_comments[comment-type-disqus-shortname]',
					'label' 	=> esc_html__( 'Disqus Shortname', 'neal' ),
					'section' 	=> 'section_comments_general',
					'type' 		=> 'text',
					'priority' 	=> 4
				) );

				// facebook appid
				$wp_customize->add_setting( 'neal_comments[comment-type-facebook-appid]', array(
					'default'	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_string'
				) );

				$wp_customize->add_control( 'neal_comments[comment-type-facebook-appid]', array(
					'settings'	=> 'neal_comments[comment-type-facebook-appid]',
					'label' 	=> esc_html__( 'Facebook AppID', 'neal' ),
					'section' 	=> 'section_comments_general',
					'type' 		=> 'text',
					'priority' 	=> 5
				) );

				// comment texts
				$wp_customize->add_setting( 'neal_comments[comment-texts-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_comments[comment-texts-label]', array(
							'label'    => esc_html__( 'Comment Texts', 'neal' ),
							'section'  => 'section_comments_general',
							'priority' => 6
						)
				) );

				// moderation text
				$wp_customize->add_setting('neal_comments[comment-texts-moderation]', array(
						'default'	=> esc_html__( 'Your comment is awaiting moderation!', 'neal' ),
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_string'
				) );

				$wp_customize->add_control('neal_comments[comment-texts-moderation]', array(
						'settings'	=> 'neal_comments[comment-texts-moderation]',
					    'label' 	=> esc_html__( 'Awaiting Moderation Text', 'neal' ),
					    'section' 	=> 'section_comments_general',
					    'type' 		=> 'text',
					    'priority' 	=> 7
				) );

				// closed text
				$wp_customize->add_setting('neal_comments[comment-texts-closed]', array(
					   	'default'	=> esc_html__('Hey! comments are closed.', 'neal' ),
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_string'
				) );

				$wp_customize->add_control('neal_comments[comment-texts-closed]', array(
					   	'settings' 	=> 'neal_comments[comment-texts-closed]',
					    'label' 	=> esc_html__( 'Closed Comments Text', 'neal' ),
					    'section' 	=> 'section_comments_general',
					    'type' 		=> 'text',
					    'priority' 	=> 8
				) );

				// author image
				$wp_customize->add_setting( 'neal_comments[author-img-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[author-img-label]', array(
							'label'    => esc_html__( 'Author Image', 'neal' ),
							'section'  => 'section_comments_general',
							'priority' => 9
						)
				) );

				// author image size
				$wp_customize->add_setting('neal_comments[author-img-size]', array(
					'default' 	=> 'default',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_comments[author-img-size]', array(
						'settings'	=> 'neal_comments[author-img-size]',
					    'label' 	=> esc_html__( 'Avatar Size', 'neal' ),
					    'section' 	=> 'section_comments_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'40'	=> esc_html__( 'Small', 'neal' ),
										'55'  	=> esc_html__( 'Medium', 'neal' ),
										'70'  	=> esc_html__( 'Large', 'neal' ),
										'100'  	=> esc_html__( 'Extra Large', 'neal' ),

			            			   ),
					    'priority'	=> 10
				) );

			/* -----------------{ Spacing Options }----------------- */

				// author image label
				$wp_customize->add_setting( 'neal_comments[author-img-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[author-img-spac-label]', array(
							'label'    => esc_html__( 'Author Image', 'neal' ),
							'section'  => 'section_comments_spacing',
							'priority' => 1
						)
				) );

				// margin right
				$wp_customize->add_setting('neal_comments[author-img-spac-margin-right]', array(
						'default'	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_comments[author-img-spac-margin-right]', array(
							'settings'		=> 'neal_comments[author-img-spac-margin-right]',
					        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_comments_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 2
					    )
				) );

				// comment content label
				$wp_customize->add_setting( 'neal_comments[comment-content-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[comment-content-spac-label]', array(
							'label'    => esc_html__( 'Comment Content', 'neal' ),
							'section'  => 'section_comments_spacing',
							'priority' => 3
						)
				) );

				// vertical gutter
				$wp_customize->add_setting('neal_comments[comment-content-spac-vertical-gutter]', array(
						'default'	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_comments[comment-content-spac-vertical-gutter]',array(
							'settings'		=> 'neal_comments[comment-content-spac-vertical-gutter]',
					        'label' 		=> 'Vertical Gutter',
					        'section' 		=> 'section_comments_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 70
								                ),
					        'priority'		=> 4
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// author image label
				$wp_customize->add_setting( 'neal_comments[author-img-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[author-img-styl-label]', array(
							'label'    => esc_html__( 'Author Image', 'neal' ),
							'section'  => 'section_comments_styling',
							'priority' => 1
						)
				) );

				// radius
				$wp_customize->add_setting('neal_comments[author-img-styl-radius]', array(
						'default' 	=> 14,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 
						'neal_comments[author-img-styl-radius]', array(
							'settings'		=> 'neal_comments[author-img-styl-radius]',
					        'label' 		=> esc_html__( 'Corner Radius', 'neal' ),
					        'section' 		=> 'section_comments_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 50
								               ),
					        'priority'		=> 2
					    )
				) );

				// comment content label
				$wp_customize->add_setting( 'neal_comments[comment-content-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_comments[comment-content-styl-label]', array(
							'label'    => esc_html__( 'Comment Content', 'neal' ),
							'section'  => 'section_comments_styling',
							'priority' => 3
						)
				) );

				// background color
				$wp_customize->add_setting('neal_comments[comment-content-styl-bg-color]', array(
						'default' 	=> '#edeff1',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_comments[comment-content-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_comments_styling',
							'settings'  => 'neal_comments[comment-content-styl-bg-color]',
							'priority' 	=> 4
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_comments[comment-content-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_comments[comment-content-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_comments_styling',
							'settings'  => 'neal_comments[comment-content-styl-font-color]',
							'priority' 	=> 5
						) 
				) );

			/*
			***************************************************************
			* #Inputs
			***************************************************************
			*/	

			/* -----------------{ Spacing Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_inputs[general-spac-label]', array( 
				 	'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_inputs[general-spac-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_inputs_spacing',
							'priority' => 1
						)
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_inputs[general-spac-padding-top-bottom]', array(
						'default'	=> 10,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[general-spac-padding-top-bottom]', array(
							'settings'		=> 'neal_inputs[general-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_inputs_spacing',
					        'input_attrs' 	=> array(
								               	'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 2
					    )
				) );

				// padding right-left
				$wp_customize->add_setting('neal_inputs[general-spac-padding-right-left]', array(
						'default'	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[general-spac-padding-right-left]', array(
							'settings'		=> 'neal_inputs[general-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_inputs_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority' 		=> 3
					    )
				) );

				// submit button label
				$wp_customize->add_setting( 'neal_inputs[submit-button-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_inputs[submit-button-spac-label]', array(
							'label'    => esc_html__( 'Submit Button', 'neal' ),
							'section'  => 'section_inputs_spacing',
							'priority' => 4
						)
				) );

				// padding top-bottom
				$wp_customize->add_setting('neal_inputs[submit-button-spac-padding-top-bottom]', array(
						'default' => 11,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[submit-button-spac-padding-top-bottom]', array(
							'settings'		=> 'neal_inputs[submit-button-spac-padding-top-bottom]',
					        'label' 		=> esc_html__( 'Padding Top-Bottom', 'neal' ),
					        'section' 		=> 'section_inputs_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 5
					    )
				) );

				// padding right-left
				$wp_customize->add_setting('neal_inputs[submit-button-spac-padding-right-left]', array(
						'default'	=> 18,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[submit-button-spac-padding-right-left]', array(
							'settings'		=> 'neal_inputs[submit-button-spac-padding-right-left]',
					        'label' 		=> esc_html__( 'Padding Right-Left', 'neal' ),
					        'section' 		=> 'section_inputs_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 280
								               ),
					        'priority'		=> 6
					    )
				) );


			/* -----------------{ Styling Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_inputs[general-styl-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_inputs[general-styl-label]', array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 1
						)
				) );

				// font color
				$wp_customize->add_setting('neal_inputs[general-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[general-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[general-styl-font-color]',
							'priority' 	=> 2
						) 
				) );

				// font focus color
				$wp_customize->add_setting('neal_inputs[general-styl-font-focus-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[general-styl-font-focus-color]', array(
							'label'		=> esc_html__( 'Font Focus Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[general-styl-font-focus-color]',
							'priority'	=> 3
						) 
				) );

				// border focus color
				$wp_customize->add_setting('neal_inputs[general-styl-border-focus-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[general-styl-border-focus-color]', array(
							'label'		=> esc_html__( 'Border Focus Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[general-styl-border-focus-color]',
							'priority'	=> 4
						) 
				) );

				// border-bottom label
				$wp_customize->add_setting( 'neal_inputs[general-styl-child-border-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_inputs[general-styl-child-border-label]', array(
							'label'    => esc_html__( 'Border Bottom', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 5
						)
				) );

				// border-bottom size
				$wp_customize->add_setting('neal_inputs[general-styl-child-border-bottom-size]', array(
						'default'	=> 1,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[general-styl-child-border-bottom-size]', array(
							'settings'		=> 'neal_inputs[general-styl-child-border-bottom-size]',
					        'label' 		=> esc_html__( 'Size', 'neal' ),
					        'section' 		=> 'section_inputs_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 15
								              ),
					        'priority'		=> 6
					    )
				) );

				// border style
				$wp_customize->add_setting('neal_inputs[general-styl-child-border-bottom-style]', array(
						'default' 	=> 'solid',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_inputs[general-styl-child-border-bottom-style]', array(
						'label'		=> esc_html__( 'Style', 'neal' ),
						'section'   => 'section_inputs_styling',
						'settings'  => 'neal_inputs[general-styl-child-border-bottom-style]',
						'type' 		=> 'select',
						'choices' 	=> array(
									   	'solid'		=> esc_html__( 'Solid', 'neal' ),
										'dotted' 	=> esc_html__( 'Dotted', 'neal' ),
										'dashed' 	=> esc_html__( 'Dashed', 'neal' ),
										'double' 	=> esc_html__( 'Double', 'neal' ),
										'groove' 	=> esc_html__( 'Groove', 'neal' ),
									   ),
						'priority'	=> 7
				) );

				// border top color
				$wp_customize->add_setting('neal_inputs[general-styl-child-border-bottom-color]', array(
						'default' 	=> '#d9d9d9',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[general-styl-child-border-bottom-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[general-styl-child-border-bottom-color]',
							'priority' 	=> 8
						) 
				) );


				// radius label
				$wp_customize->add_setting( 'neal_inputs[general-styl-child-radius-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'postMessage',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_inputs[general-styl-child-radius-label]', array(
							'label'    => esc_html__( 'Corner Radius', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 9
						)
				) );

				// radius
				$wp_customize->add_setting('neal_inputs[general-styl-child-radius]', array(
						'default'	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[general-styl-child-radius]', array(
							'settings'		=> 'neal_inputs[general-styl-child-radius]',
					        'label' 		=> esc_html__( 'Radius', 'neal' ),
					        'section' 		=> 'section_inputs_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 30
								               ),
					        'priority'		=> 10
					     )
				) );

				// submit button label
				$wp_customize->add_setting( 'neal_inputs[submit-button-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_inputs[submit-button-styl-label]', array(
							'label'    => esc_html__( 'Submit Button', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 11
						)
				) );

				// background color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-bg-color]', array(
						'default' 	=> '#eee',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[submit-button-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[submit-button-styl-bg-color]',
							'priority' 	=> 12
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[submit-button-styl-font-color]', array(
							'label'		=> esc_html__( 'Font Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[submit-button-styl-font-color]',
							'priority'	=> 13
						) 
				) );

				// background hover color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-bg-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[submit-button-styl-bg-hover-color]', array(
							'label'		=> esc_html__( 'Background Hover Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[submit-button-styl-bg-hover-color]',
							'priority'	=> 14
						) 
				) );

				// font hover color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-font-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[submit-button-styl-font-hover-color]', array(
							'label'		=> esc_html__( 'Font Hover Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[submit-button-styl-font-hover-color]',
							'priority' 	=> 15
					) 
				) );

				// border hover color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-border-hover-color]', array(
						'default' 	=> '#eee',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_inputs[submit-button-styl-border-hover-color]', array(
							'label'		=> esc_html__( 'Border Hover Color', 'neal' ),
							'section'   => 'section_inputs_styling',
							'settings'  => 'neal_inputs[submit-button-styl-border-hover-color]',
							'priority' 	=> 16
						) 
				) );

				// border label
				$wp_customize->add_setting( 'neal_inputs[submit-button-styl-child-border-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'postMessage',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_inputs[submit-button-styl-child-border-label]', array(
							'label'    => esc_html__( 'Border', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 17
						)
				) );

				// border size
				$wp_customize->add_setting('neal_inputs[submit-button-styl-child-border-size]', array(
						'default'	=> 1,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_inputs[submit-button-styl-child-border-size]',array(
							'settings'		=> 'neal_inputs[submit-button-styl-child-border-size]',
					        'label' 		=> esc_html__( 'Size', 'neal' ),
					        'section' 		=> 'section_inputs_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 15
								               ),
					        'priority'		=> 18
					    )
				) );

				// border top style
				$wp_customize->add_setting('neal_inputs[submit-button-styl-child-border-style]', array(
						'default' 	=> 'solid',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_inputs[submit-button-styl-child-border-style]', array(
						'label'		=> esc_html__( 'Style', 'neal' ),
						'section'   => 'section_inputs_styling',
						'settings'  => 'neal_inputs[submit-button-styl-child-border-style]',
						'type' 		=> 'select',
						'choices' 	=> array(
									   	'solid'		=> esc_html__( 'Solid', 'neal' ),
										'dotted' 	=> esc_html__( 'Dotted', 'neal' ),
										'dashed' 	=> esc_html__( 'Dashed', 'neal' ),
										'double' 	=> esc_html__( 'Double', 'neal' ),
										'groove' 	=> esc_html__( 'Groove', 'neal' ),
										),
						'priority'	=> 19
				) );

				// border color
				$wp_customize->add_setting('neal_inputs[submit-button-styl-child-border-color]', array(
						'default' 	=> '',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
							new WP_Customize_Color_Control( 
								$wp_customize, 
								'neal_inputs[submit-button-styl-child-border-color]', 
								array(
									'label'      => esc_html__( 'Color', 'neal' ),
									'section'    => 'section_inputs_styling',
									'settings'   => 'neal_inputs[submit-button-styl-child-border-color]',
									'priority' => 20
								) ) 
				);

				// radius label
				$wp_customize->add_setting( 'neal_inputs[submit-button-styl-child-radius-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'postMessage',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_inputs[submit-button-styl-child-radius-label]', 
						array(
							'label'    => esc_html__( 'Corner Radius', 'neal' ),
							'section'  => 'section_inputs_styling',
							'priority' => 21
						)
				));

				// radius
				$wp_customize->add_setting('neal_inputs[submit-button-styl-child-radius]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[submit-button-styl-child-radius]',
						array(
							'settings' => 'neal_inputs[submit-button-styl-child-radius]',
					        'label' => esc_html__( 'Radius', 'neal' ),
					        'section' => 'section_inputs_styling',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 30
								            ),
					        'priority' => 22
					       )
				));

			/* -----------------{ Font Options }----------------- */

				// general label
				$wp_customize->add_setting( 'neal_inputs[general-font-label]', array( 
				    'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_inputs[general-font-label]', 
						array(
							'label'    => esc_html__( 'General', 'neal' ),
							'section'  => 'section_inputs_font',
							'priority' => 1
						)
				));

				// font size
				$wp_customize->add_setting('neal_inputs[general-font-size]', 
					array(
						'default' => 16,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[general-font-size]',
						array(
							'settings' => 'neal_inputs[general-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 2
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_inputs[general-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[general-font-weight]',
						array(
							'settings' => 'neal_inputs[general-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 3
					       )
				));

				// letter-spacing
				$wp_customize->add_setting('neal_inputs[general-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[general-font-letter-spacing]',
						array(
							'settings' => 'neal_inputs[general-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 4
					       )
				));

				// submit button label
				$wp_customize->add_setting( 'neal_inputs[submit-button-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_inputs[submit-button-font-label]', 
						array(
							'label'    => esc_html__( 'Submit Button', 'neal' ),
							'section'  => 'section_inputs_font',
							'priority' => 5
						)
				));

				// font size
				$wp_customize->add_setting('neal_inputs[submit-button-font-size]', 
					array(
						'default' => 15,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[submit-button-font-size]',
						array(
							'settings' => 'neal_inputs[submit-button-font-size]',
					        'label' => esc_html__( 'Font Size', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min' => 10,
								                'max' => 50
								            ),
					        'priority' => 6
					       )
				));

				// font weight
				$wp_customize->add_setting('neal_inputs[submit-button-font-weight]', 
					array(
						'default' => 400,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[submit-button-font-weight]',
						array(
							'settings' => 'neal_inputs[submit-button-font-weight]',
					        'label' => esc_html__( 'Font Weight', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								            ),
					        'priority' => 7
					       )
				));

				// letter-spacing
				$wp_customize->add_setting('neal_inputs[submit-button-font-letter-spacing]', 
					array(
						'default' => 0,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_inputs[submit-button-font-letter-spacing]',
						array(
							'settings' => 'neal_inputs[submit-button-font-letter-spacing]',
					        'label' => esc_html__( 'Letter Spacing', 'neal' ),
					        'section' => 'section_inputs_font',
					        'input_attrs' => array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 8
					       )
				));

				// uppercase
				$wp_customize->add_setting( 'neal_inputs[submit-button-font-uppercase]', array(
					'default' 		=> true,
					'type' 			=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_inputs[submit-button-font-uppercase]', array(
					'label'    => esc_html__( 'Uppercase', 'neal' ),
					'section'           => 'section_inputs_font',
					'settings'          => 'neal_inputs[submit-button-font-uppercase]',
					'type'				=> 'checkbox',
					'priority' => 9
				) );


			/*
			***************************************************************
			* #Contact Page
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// form label
				$wp_customize->add_setting( 'neal_contact[form-label]', array( 
					'default'	=> true,
					'type'		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_contact[form-label]', 
						array(
							'label'    => esc_html__( 'Contact Form', 'neal' ),
							'section'  => 'section_contact_general',
							'priority' => 1
						)
				));

				// form title
				$wp_customize->add_setting('neal_contact[form-title]', array(
							'default' => 'Write To Us',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[form-title]', array(
							'settings' => 'neal_contact[form-title]',
					        'label' => esc_html__( 'Title', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 2
					       )
				);

				// form reciever email
				$wp_customize->add_setting('neal_contact[form-reciever-email]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'is_email'
							));
				$wp_customize->add_control('neal_contact[form-reciever-email]', array(
							'settings' => 'neal_contact[form-reciever-email]',
					        'label' => esc_html__( 'Reciever Email', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 3
					       )
				);

				// info label
				$wp_customize->add_setting( 'neal_contact[info-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_contact[info-label]', 
						array(
							'label'    => esc_html__( 'Contact Info', 'neal' ),
							'section'  => 'section_contact_general',
							'priority' => 4
						)
				));

				// info title
				$wp_customize->add_setting('neal_contact[info-title]', array(
							'default' => 'Our Info',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));

				$wp_customize->add_control('neal_contact[info-title]', array(
							'settings' => 'neal_contact[info-title]',
					        'label' => esc_html__( 'Title', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 5
					       )
				);

				// info address
				$wp_customize->add_setting('neal_contact[info-address]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[info-address]', array(
							'settings' => 'neal_contact[info-address]',
					        'label' => esc_html__( 'Address', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 6
					       )
				);

				// address icon
				$wp_customize->add_setting('neal_contact[info-address-icon]', array(
						'default' 	=> 'fa-map-marker',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[info-address-icon]', array(
							'settings' => 'neal_contact[info-address-icon]',
					        'label' => esc_html__( 'Select Icon', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
					        					'none' => '&#xf05e;',
					        					'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
					        					'ion-navigate' => '&#xf2a3;',
			                					'ion-map' => '&#xf203;',
												'ion-ios-location' => '&#xf456;',
												'ion-ios-location-outline' => '&#xf455;',
												'ion-ios-navigate' => '&#xf46e;',
												'ion-ios-navigate-outline' => '&#xf46d;',
												'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-location-arrow'	=> '&#xf124;',
			                					'fa-map-marker'		=> '&#xf041;',

			            					),
					        'priority' => 7
					       )
				);

				// info phone
				$wp_customize->add_setting('neal_contact[info-phone]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[info-phone]', array(
							'settings' => 'neal_contact[info-phone]',
					        'label' => esc_html__( 'Phone', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 8
					       )
				);

				// phone icon
				$wp_customize->add_setting('neal_contact[info-phone-icon]', array(
						'default' 	=> 'fa-phone',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[info-phone-icon]', array(
							'settings' => 'neal_contact[info-phone-icon]',
					        'label' => esc_html__( 'Select Icon', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
					        					'none' => '&#xf05e;',
					        					'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
					        					'ion-iphone' => '&#xf1fa;',
			                					'ion-ios-telephone' => '&#xf4b9;',
												'ion-ios-telephone-outline' => '&#xf4b8;',
												'ion-android-call' => '&#xf2d2;',
												'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-phone' => '&#xf095;',
			                					'fa-phone-square' => '&#xf098;',
			                					'fa-volume-control-phone' => '&#xf2a0;',
			                					'fa-mobile' => '&#xf10b'
			            					),
					        'priority' => 9
					       )
				);

				// info email
				$wp_customize->add_setting('neal_contact[info-email]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[info-email]', array(
							'settings' => 'neal_contact[info-email]',
					        'label' => esc_html__( 'Email', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 10
					       )
				);

				// email icon
				$wp_customize->add_setting('neal_contact[info-email-icon]', array(
						'default' 	=> 'fa-envelope',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[info-email-icon]', array(
							'settings' => 'neal_contact[info-email-icon]',
					        'label' => esc_html__( 'Select Icon', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
					        					'none' => '&#xf05e;',
					        					'ion-icons'		=> esc_html__( '---Ion Icons---', 'neal' ),
					        					'ion-email' => '&#xf132;',
			                					'ion-ios-email' => '&#xf423;',
												'ion-ios-email-outline' => '&#xf422;',
												'fawe-icons'	=> esc_html__( '---Font Awesome Icons---', 'neal' ),
			                					'fa-envelope-o' => '&#xf003;',
			                					'fa-envelope' => '&#xf0e0;'
			            					),
					        'priority' => 11
					       )
				);

				// info short info
				$wp_customize->add_setting('neal_contact[info-short-info]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'wp_filter_nohtml_kses'
							));

				$wp_customize->add_control(
					new Neal_Textarea_Control( $wp_customize, 'neal_contact[info-short-info]', array(
							'label'		=> esc_html__( 'Short Info', 'neal' ),
							'section' 	=> 'section_contact_general',
							'priority' 	=> 12
						)
				) );

				// info image
				$wp_customize->add_setting( 'neal_contact[info-short-image]', array(
					'default' 	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'esc_url_raw'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_contact[info-short-image]', array(
							'label'		=> esc_html__( 'Image', 'neal' ),
							'section'	=> 'section_contact_general',
							'settings'  => 'neal_contact[info-short-image]',
							'priority'	=> 13
						) 
				) );


				// google map label
				$wp_customize->add_setting( 'neal_contact[google-map-label]', array( 
					'default' 	=> true,
				    'type' 		=> 'option',
				    'transport'	=> 'refresh',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control(
						$wp_customize, 
						'neal_contact[google-map-label]', 
						array(
							'label'    => esc_html__( 'Google Map', 'neal' ),
							'section'  => 'section_contact_general',
							'priority' => 14
						)
				));

				// google map layout
				$wp_customize->add_setting('neal_contact[google-map-layout]', array(
						'default' 	=> 'full',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[google-map-layout]', array(
							'settings' => 'neal_contact[google-map-layout]',
					        'label' => esc_html__( 'Layout Width', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'full' => esc_html__( 'Full-Width', 'neal' ),
												'boxed'  => esc_html__( 'Boxed', 'neal' ),
			            					),
					        'priority' => 15
					       )
				);

				// google map position
				$wp_customize->add_setting('neal_contact[google-map-pos]', array(
						'default' 	=> 'top',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[google-map-pos]', array(
							'settings' => 'neal_contact[google-map-pos]',
					        'label' => esc_html__( 'Position', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'top' => esc_html__( 'Top', 'neal' ),
												'bottom'  => esc_html__( 'Bottom', 'neal' ),
			            					),
					        'priority' => 16
					       )
				);

				// google map api key
				$wp_customize->add_setting('neal_contact[google-map-api_key]', array(
							'default' => '',
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[google-map-api_key]', array(
							'settings' => 'neal_contact[google-map-api_key]',
					        'label' => esc_html__( 'Google Map Api Key', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 17
					       )
				);

				// google map location
				$wp_customize->add_setting('neal_contact[google-map-location]', array(
							'default' => 'so173sq',
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'neal_sanitize_string'
							));
				$wp_customize->add_control('neal_contact[google-map-location]', array(
							'settings' => 'neal_contact[google-map-location]',
					        'label' => esc_html__( 'Location', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'text',
					        'priority' => 18
					       )
				);

				// google map type
				$wp_customize->add_setting('neal_contact[google-map-type]', array(
							'default' => 'ROADMAP',
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'neal_sanitize_radio'
							));
				$wp_customize->add_control('neal_contact[google-map-type]', array(
							'settings' => 'neal_contact[google-map-type]',
					        'label' => esc_html__( 'Type', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'radio',
					        'choices'    => array(
			                					'ROADMAP' => esc_html__( 'Roadmap', 'neal' ),
												'SATELLITE'  => esc_html__( 'Satellite', 'neal' ),
			            					),
					        'priority' => 19
					       )
				);

				// google map zoom
				$wp_customize->add_setting('neal_contact[google-map-zoom]', array(
						'default' 	=> 3,
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_contact[google-map-zoom]', array(
							'settings' => 'neal_contact[google-map-zoom]',
					        'label' => esc_html__( 'Zoom Level', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'select',
					        'choices'    => array(
			                					'2' => esc_html__( '2 x', 'neal' ),
												'3'  => esc_html__( '3 x', 'neal' ),
												'4'  => esc_html__( '4 x', 'neal' ),
												'5'  => esc_html__( '5 x', 'neal' ),
												'6'  => esc_html__( '6 x', 'neal' ),
												'7'  => esc_html__( '7 x', 'neal' ),
												'8'  => esc_html__( '8 x', 'neal' ),
												'9'  => esc_html__( '9 x', 'neal' ),
												'10'  => esc_html__( '10 x', 'neal' ),
												'11'  => esc_html__( '11 x', 'neal' ),
												'12'  => esc_html__( '12 x', 'neal' ),
												'13'  => esc_html__( '13 x', 'neal' ),
												'14'  => esc_html__( '14 x', 'neal' ),
												'15'  => esc_html__( '15 x', 'neal' ),
												'16'  => esc_html__( '16 x', 'neal' ),
												'17'  => esc_html__( '17 x', 'neal' ),
												'18'  => esc_html__( '18 x', 'neal' ),
												'19'  => esc_html__( '19 x', 'neal' ),
												'20'  => esc_html__( '20 x', 'neal' ),
			            					),
					        'priority' => 20
					       )
				);

				// google map typecontrol
				$wp_customize->add_setting('neal_contact[google-map-tcontrol]', array(
							'default' => true,
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'neal_sanitize_checkbox'
							));
				$wp_customize->add_control('neal_contact[google-map-tcontrol]', array(
							'settings' => 'neal_contact[google-map-tcontrol]',
					        'label' => esc_html__( 'Type Control', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'checkbox',
					        'priority' => 21
					       )
				);

				// google map navigation
				$wp_customize->add_setting('neal_contact[google-map-nav]', array(
							'default' => true,
							'type'	  => 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'neal_sanitize_checkbox'
							));
				$wp_customize->add_control('neal_contact[google-map-nav]', array(
							'settings' => 'neal_contact[google-map-nav]',
					        'label' => esc_html__( 'Navigation & Scaling', 'neal' ),
					        'section' => 'section_contact_general',
					        'type' => 'checkbox',
					        'priority' => 22
					       )
				);

				// google map marker icon
				$wp_customize->add_setting( 'neal_contact[google-map-marker-icon]', array(
					'default' 	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'esc_url_raw'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Image_Control( $wp_customize, 'neal_contact[google-map-marker-icon]', array(
							'label'		=> esc_html__( 'Marker Icon', 'neal' ),
							'section'	=> 'section_contact_general',
							'settings'  => 'neal_contact[google-map-marker-icon]',
							'priority'	=> 23
						) 
				) );

			/* -----------------{ Spacing Options }----------------- */

				// google map label
				$wp_customize->add_setting( 'neal_contact[google-map-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control(
						$wp_customize, 
						'neal_contact[google-map-spac-label]', 
						array(
							'label'    => esc_html__( 'Google Map', 'neal' ),
							'section'  => 'section_contact_spacing',
							'priority' => 1
						)
				));

				// height
				$wp_customize->add_setting('neal_contact[google-map-spac-height]', array(
							'default' => 400,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_contact[google-map-spac-height]',
						array(
							'settings' => 'neal_contact[google-map-spac-height]',
					        'label' => esc_html__( 'Height', 'neal' ),
					        'section' => 'section_contact_spacing',
					        'input_attrs' => array(
								                'min' => 150,
								                'max' => 900
								            ),
					        'priority' => 2
					       )
				));

				// margin top-bottom
				$wp_customize->add_setting('neal_contact[google-map-spac-margin-top-bottom]', array(
							'default' => 30,
							'type'	  => 'option',
							'transport'	=> 'postMessage',
							'sanitize_callback'	=> 'neal_sanitize_number'
							));
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control(
						$wp_customize, 
						'neal_contact[google-map-spac-margin-top-bottom]',
						array(
							'settings' => 'neal_contact[google-map-spac-margin-top-bottom]',
					        'label' => esc_html__( 'Margin Top-Bottom', 'neal' ),
					        'section' => 'section_contact_spacing',
					        'input_attrs' => array(
								                'min' => 0,
								                'max' => 80
								            ),
					        'priority' => 3
					       )
				));


			/*
			***************************************************************
			* #Pagination
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// navigation label
				$wp_customize->add_setting( 'neal_pagination[navigation-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_pagination[navigation-label]', array(
							'label'    => esc_html__( 'Navigation', 'neal' ),
							'section'  => 'section_pagination_general',
							'priority' => 1
						)
				) );

				// pagination type
				$wp_customize->add_setting('neal_pagination[navigation-pag-type]', array(
						'default' 	=> 'numeric',
						'type'	  	=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_pagination[navigation-pag-type]', array(
						'settings'	=> 'neal_pagination[navigation-pag-type]',
						'label' 	=> esc_html__( 'Pagination Type:', 'neal' ),
					    'section' 	=> 'section_pagination_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                		   	'numeric'			=> esc_html__( 'Numeric (Prev/Next Links)', 'neal' ),
			                			'default' 			=> esc_html__( 'Default (Only Previous/Next Links)', 'neal' ),
			                			'load-more'			=> esc_html__( 'Load More', 'neal' ),
					    				'infinite-scroll' 	=> esc_html__( 'Infinite Scroll', 'neal' )
					    				),
					    'priority'	=> 2
				) );

				// navigation align
				$wp_customize->add_setting('neal_pagination[navigation-align]', array(
					'default' 	=> 'center',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_pagination[navigation-align]', array(
					'settings'	=> 'neal_pagination[navigation-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_pagination_general',
					'type' 		=> 'select',
					'choices' 	=> array(
								   	'left'		=> esc_html__( 'Left', 'neal' ),
									'center'	=> esc_html__( 'Center', 'neal' ),
									'right' 	=> esc_html__( 'Right', 'neal' ),
								   ),
					'priority'	=> 3
				) );

				// navigation icon
				$wp_customize->add_setting('neal_pagination[navigation-icon-fa]', array(
					'default' 	=> 'none',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_pagination[navigation-icon-fa]', array(
					'settings'	=> 'neal_pagination[navigation-icon-fa]',
					'label' 	=> esc_html__( 'Select Icon', 'neal' ),
					'section' 	=> 'section_pagination_general',
					'type' 		=> 'select',
					'choices'   => array(
					    				'none'			=> '&#xf05e;',
			                		   	'angle'			=> '&#xf104; &#xf105;',
			                			'angle-double'	=> '&#xf100; &#xf101;',
			                			'chevron'		=> '&#xf053; &#xf054;',
			                			'long-arrow'	=> '&#xf177; &#xf178;',
			                			'chevron-circle'=> '&#xf137; &#xf138;',
			                			'arrow-circle'  => '&#xf0a8; &#xf0a9;',
			                			'arrow-circle-o'=> '&#xf190; &#xf18e;'
			            		   ),
					'priority'	=> 4
				) );

			/* -----------------{ Spacing Options }----------------- */

				// navigation label
				$wp_customize->add_setting( 'neal_pagination[navigation-spac-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_pagination[navigation-spac-label]', array(
							'label'    => esc_html__( 'Navigation', 'neal' ),
							'section'  => 'section_pagination_spacing',
							'priority' => 1
						)
				) );

				// width
				$wp_customize->add_setting('neal_pagination[navigation-spac-width]', array(
						'default' 	=> 8,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 
						'neal_pagination[navigation-spac-width]', array(
							'settings'		=> 'neal_pagination[navigation-spac-width]',
					        'label' 		=> esc_html__( 'Width', 'neal' ),
					        'section' 		=> 'section_pagination_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 80
								               ),
					        'priority' 		=> 2
					    )
				) );

				// height
				$wp_customize->add_setting('neal_pagination[navigation-spac-height]', array(
						'default' 	=> 18,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 
						'neal_pagination[navigation-spac-height]', array(
							'settings'		=> 'neal_pagination[navigation-spac-height]',
					        'label' 		=> esc_html__( 'Height', 'neal' ),
					        'section' 		=> 'section_pagination_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 80
								               ),
					        'priority' 		=> 3
					    )
				) );

				// margin right
				$wp_customize->add_setting('neal_pagination[navigation-spac-margin-right]', array(
						'default' 	=> 8,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_pagination[navigation-spac-margin-right]', array(
							'settings'		=> 'neal_pagination[navigation-spac-margin-right]',
					        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_pagination_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 50
								               ),
					        'priority' 		=> 4
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// navigation label
				$wp_customize->add_setting( 'neal_pagination[navigation-styl-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_pagination[navigation-styl-label]', array(
							'label'    => esc_html__( 'Navigation', 'neal' ),
							'section'  => 'section_pagination_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_pagination[navigation-styl-bg-color]', array(
						'default' 	=> '#e5e5e5',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-bg-color]',
							'priority'  => 2
						) 
				) );

				// background hover color
				$wp_customize->add_setting('neal_pagination[navigation-styl-bg-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-bg-hover-color]', array(
							'label'		=> esc_html__( 'Background Hover Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-bg-hover-color]',
							'priority' 	=> 3
						) 
				) );

				// background active color
				$wp_customize->add_setting('neal_pagination[navigation-styl-bg-active-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-bg-active-color]', array(
							'label'		=> esc_html__( 'Background Active Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-bg-active-color]',
							'priority' 	=> 4
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_pagination[navigation-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-font-color]', array(
							'label'		=> esc_html__( 'Text Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-font-color]',
							'priority' 	=> 5
						) 
				) );

				// link color
				$wp_customize->add_setting('neal_pagination[navigation-styl-link-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-link-color]',
							'priority' 	=> 6
						) 
				) );

				// link hover color
				$wp_customize->add_setting('neal_pagination[navigation-styl-link-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-link-hover-color]', array(
							'label'		=> esc_html__( 'Link Hover Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-link-hover-color]',
							'priority' 	=> 7
						) 
				) );

				// font active color
				$wp_customize->add_setting('neal_pagination[navigation-styl-font-active-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_pagination[navigation-styl-font-active-color]', array(
							'label'		=> esc_html__( 'Active Color', 'neal' ),
							'section'   => 'section_pagination_styling',
							'settings'  => 'neal_pagination[navigation-styl-font-active-color]',
							'priority' 	=> 8
						) 
				) );

				// radius
				$wp_customize->add_setting('neal_pagination[navigation-styl-radius]', array(
						'default' 	=> 50,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 
						'neal_pagination[navigation-styl-radius]', array(
							'settings'		=> 'neal_pagination[navigation-styl-radius]',
					        'label' 		=> esc_html__( 'Corner Radius', 'neal' ),
					        'section' 		=> 'section_pagination_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 50
								               ),
					        'priority'		=> 9
					    )
				) );


			/* -----------------{ Font Options }----------------- */

				// navigation label
				$wp_customize->add_setting( 'neal_pagination[navigation-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_pagination[navigation-font-label]', array(
							'label'    => esc_html__( 'Navigation', 'neal' ),
							'section'  => 'section_pagination_font',
							'priority' => 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_pagination[navigation-font-size]', 
					array(
						'default' => 15,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_pagination[navigation-font-size]', array(
							'settings'		=> 'neal_pagination[navigation-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_pagination_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								              ),
					        'priority' 		=> 2
					   	)
				) );

				// font weight
				$wp_customize->add_setting('neal_pagination[navigation-font-weight]', 
					array(
						'default' => 500,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_pagination[navigation-font-weight]', array(
							'settings'		=> 'neal_pagination[navigation-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_pagination_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority'		=> 3
					   	)
				) );

				// font size
				$wp_customize->add_setting('neal_pagination[navigation-font-line-height]', 
					array(
						'default' => 15,
						'type'	  => 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
					)
				);
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_pagination[navigation-font-line-height]', array(
							'settings'		=> 'neal_pagination[navigation-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_pagination_font',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 80
								              ),
					        'priority' 		=> 4
					   	)
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_pagination[navigation-font-uppercase]', array(
						'default'	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_pagination[navigation-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_pagination_font',
						'settings'  => 'neal_pagination[navigation-font-uppercase]',
						'type'		=> 'checkbox',
						'priority'	=> 5
				) );


			/*
			***************************************************************
			* #Typography
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// subset label
				$wp_customize->add_setting( 'neal_typography[subset-label]', array( 
					'default' 	=> false,
				    'type' 		=> 'option',
				    'transport'	=> 'postMessage',
				    'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_typography[subset-label]', array(
							'label'    => esc_html__( 'Google Font Subsets', 'neal' ),
							'section'  => 'section_typography_general',
							'priority' => 1
						)
				) );

				// subset latin
				$wp_customize->add_setting( 'neal_typography[subset-latin]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-latin]', array(
					'label'		=> esc_html__( 'Latin', 'neal' ),
					'section'   => 'section_typography_general',
					'settings'  => 'neal_typography[subset-latin]',
					'type'		=> 'checkbox',
					'priority'	=> 2
				) );

				// subset cyrillic
				$wp_customize->add_setting( 'neal_typography[subset-cyrillic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-cyrillic]', array(
					'label'		=> esc_html__( 'Cyrillic', 'neal' ),
					'section'   => 'section_typography_general',
					'settings'  => 'neal_typography[subset-cyrillic]',
					'type'		=> 'checkbox',
					'priority'	=> 3
				) );

				// subset greek
				$wp_customize->add_setting( 'neal_typography[subset-greek]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-greek]', array(
					'label'		=> esc_html__( 'Greek', 'neal' ),
					'section'   => 'section_typography_general',
					'settings'  => 'neal_typography[subset-greek]',
					'type'		=> 'checkbox',
					'priority' 	=> 4
				) );

				// subset vietnamese
				$wp_customize->add_setting( 'neal_typography[subset-vietnamese]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-vietnamese]', array(
					'label'    	=> esc_html__( 'Vietnamese', 'neal' ),
					'section'	=> 'section_typography_general',
					'settings'  => 'neal_typography[subset-vietnamese]',
					'type'		=> 'checkbox',
					'priority' 	=> 5
				) );

				// subset arabic
				$wp_customize->add_setting( 'neal_typography[subset-arabic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-arabic]', array(
					'label'		=> esc_html__( 'Arabic', 'neal' ),
					'section'   => 'section_typography_general',
					'settings'  => 'neal_typography[subset-arabic]',
					'type'		=> 'checkbox',
					'priority' 	=> 6
				) );

				// subset hindi
				$wp_customize->add_setting( 'neal_typography[subset-hindi]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[subset-hindi]', array(
					'label'		=> esc_html__( 'Hindi', 'neal' ),
					'section'   => 'section_typography_general',
					'settings'  => 'neal_typography[subset-hindi]',
					'type'		=> 'checkbox',
					'priority' 	=> 7
				) );

			/* -----------------{ Font Options }----------------- */

				// paragraph label
				$wp_customize->add_setting( 'neal_typography[paragraph-font-label]', array( 
				    	'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 
						'neal_typography[paragraph-font-label]', array(
							'label'    => esc_html__( 'Paragraph', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 1
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[paragraph-font-family]', array(
						'default'	=> 'Open+Sans',
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[paragraph-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 2
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[paragraph-font-size]', array(
						'default' 	=> 16,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[paragraph-font-size]', array(
							'settings'		=> 'neal_typography[paragraph-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 3
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[paragraph-font-weight]', array(
						'default' 		=> 400,
						'type'	  		=> 'option',
						'transport'		=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[paragraph-font-weight]', array(
							'settings'		=> 'neal_typography[paragraph-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' => 4
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[paragraph-font-line-height]', array(
						'default' 	=> 1.8,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[paragraph-font-line-height]', array(
							'settings'		=> 'neal_typography[paragraph-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 5
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[paragraph-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[paragraph-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[paragraph-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 6
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[paragraph-font-italic]', array(
						'default'	=> false,
						'type' 		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[paragraph-font-italic]', array(
					   	'label'		=> esc_html__( 'Italic', 'neal' ),
						'section'   => 'section_typography_font',
						'settings'  => 'neal_typography[paragraph-font-italic]',
						'type'		=> 'checkbox',
						'priority' 	=> 7
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[paragraph-font-uppercase]', array(
						'default' 		=> false,
						'type' 			=> 'option',
						'transport'		=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[paragraph-font-uppercase]', array(
						'label'		=> esc_html__( 'Uppercase', 'neal' ),
						'section'   => 'section_typography_font',
						'settings'  => 'neal_typography[paragraph-font-uppercase]',
						'type'		=> 'checkbox',
						'priority' 	=> 8
				) );

				// h1 label
				$wp_customize->add_setting( 'neal_typography[h1-font-label]', array( 
						'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h1-font-label]', array(
							'label'    => esc_html__( 'H1', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 9
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h1-font-family]', array(
						'default' 	=> 'Lato',
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h1-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 10
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h1-font-size]', 
					array(
						'default' 	=> 40,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h1-font-size]',array(
							'settings'		=> 'neal_typography[h1-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 11
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h1-font-weight]', array(
						'default' 	=> 600,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h1-font-weight]', array(
							'settings'		=> 'neal_typography[h1-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 12
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h1-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h1-font-line-height]', array(
							'settings'		=> 'neal_typography[h1-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 13
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h1-font-letter-spacing]', array(
						'default' 	=> 1.5,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h1-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h1-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 14
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h1-font-italic]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h1-font-italic]', array(
					'label'    	=> esc_html__( 'Italic', 'neal' ),
					'section'	=> 'section_typography_font',
					'settings'  => 'neal_typography[h1-font-italic]',
					'type'		=> 'checkbox',
					'priority'	=> 15
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h1-font-uppercase]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h1-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h1-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 16
				) );

				// h2 label
				$wp_customize->add_setting( 'neal_typography[h2-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h2-font-label]', array(
							'label'    => esc_html__( 'H2', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 17
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h2-font-family]', array(
					'default' 	=> 'Lato',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h2-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 18
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h2-font-size]', array(
					'default' 	=> 28,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h2-font-size]', array(
							'settings'		=> 'neal_typography[h2-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 19
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h2-font-weight]', array(
					'default' 	=> 600,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h2-font-weight]', array(
							'settings'		=> 'neal_typography[h2-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step' 	=> 100
								               ),
					        'priority' 		=> 20
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h2-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h2-font-line-height]', array(
							'settings'		=> 'neal_typography[h2-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 21
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h2-font-letter-spacing]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h2-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h2-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 22
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h2-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h2-font-italic]', array(
					'label'    	=> esc_html__( 'Italic', 'neal' ),
					'section'	=> 'section_typography_font',
					'settings'  => 'neal_typography[h2-font-italic]',
					'type'		=> 'checkbox',
					'priority' 	=> 23
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h2-font-uppercase]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h2-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h2-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 24
				) );

				// h3 label
				$wp_customize->add_setting( 'neal_typography[h3-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h3-font-label]', array(
							'label'    => esc_html__( 'H3', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 25
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h3-font-family]', array(
					'default' 	=> 'Lato',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h3-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 26
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h3-font-size]', array(
					'default' 	=> 20,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h3-font-size]', array(
							'settings'		=> 'neal_typography[h3-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 27
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h3-font-weight]', array(
					'default' 	=> 600,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h3-font-weight]', array(
							'settings'		=> 'neal_typography[h3-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 28
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h3-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h3-font-line-height]', array(
							'settings'		=> 'neal_typography[h3-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 29
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h3-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h3-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h3-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 30
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h3-font-italic]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h3-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h3-font-italic]',
					'type'		=> 'checkbox',
					'priority' 	=> 31
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h3-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h3-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h3-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 32
				) );

				// h4 label
				$wp_customize->add_setting( 'neal_typography[h4-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h4-font-label]', array(
							'label'    => esc_html__( 'H4', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 33
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h4-font-family]', array(
					  	'default' 	=> 'Lato',
						'type' 		=> 'option',
						'transport'	=> 'refresh',
						'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h4-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 34
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h4-font-size]', array(
					'default' 	=> 17,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h4-font-size]', array(
							'settings'		=> 'neal_typography[h4-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 35
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h4-font-weight]', array(
					'default' 	=> 600,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h4-font-weight]', array(
							'settings'		=> 'neal_typography[h4-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 36
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h4-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h4-font-line-height]', array(
							'settings'		=> 'neal_typography[h4-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 37
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h4-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h4-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h4-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 38
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h4-font-italic]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h4-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h4-font-italic]',
					'type'		=> 'checkbox',
					'priority' 	=> 39
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h4-font-uppercase]', array(
					'default' 		=> false,
					'type' 			=> 'option',
					'transport'		=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h4-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h4-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 40
				) );

				// h5 label
				$wp_customize->add_setting( 'neal_typography[h5-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h5-font-label]', array(
							'label'    => esc_html__( 'H5', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 41
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h5-font-family]', array(
					'default' 	=> 'Lato',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h5-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 42
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h5-font-size]', array(
					'default' 	=> 16,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h5-font-size]', array(
							'settings'		=> 'neal_typography[h5-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 43
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h5-font-weight]', array(
					'default' 	=> 400,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h5-font-weight]', array(
							'settings'		=> 'neal_typography[h5-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 44
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h5-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h5-font-line-height]', array(
							'settings'		=> 'neal_typography[h5-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								            ),
					        'priority' => 45
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h5-font-letter-spacing]', array(
						'default' 	=> false,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h5-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h5-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 46
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h5-font-italic]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h5-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h5-font-italic]',
					'type'		=> 'checkbox',
					'priority' 	=> 47
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h5-font-uppercase]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h5-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h5-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 48
				) );

				// h6 label
				$wp_customize->add_setting( 'neal_typography[h6-font-label]', array( 
					'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_typography[h6-font-label]', array(
							'label'    => esc_html__( 'H6', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 49
						)
				) );

				// font family
				$wp_customize->add_setting( 'neal_typography[h6-font-family]', array(
					'default' 	=> 'Lato',
					'type' 		=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_select'
				) );
				
				$wp_customize->add_control(
					new Neal_Google_Fonts_Control( $wp_customize, 'neal_typography[h6-font-family]', array(
							'label'    => esc_html__( 'Family', 'neal' ),
							'section'  => 'section_typography_font',
							'priority' => 50
						)
				) );

				// font size
				$wp_customize->add_setting('neal_typography[h6-font-size]', 
					array(
						'default' 	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h6-font-size]', array(
							'settings'		=> 'neal_typography[h6-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 51
					    )
				) );

				// font weight
				$wp_customize->add_setting('neal_typography[h6-font-weight]', array(
					'default' 	=> 400,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h6-font-weight]', array(
							'settings'		=> 'neal_typography[h6-font-weight]',
					        'label' 		=> esc_html__( 'Font Weight', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								               	'min' 	=> 100,
								                'max' 	=> 900,
								                'step'	=> 100
								               ),
					        'priority' 		=> 52
					    )
				) );

				// line height
				$wp_customize->add_setting('neal_typography[h6-font-line-height]', array(
						'default' 	=> 1.2,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h6-font-line-height]', array(
							'settings'		=> 'neal_typography[h6-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> 0,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 53
					    )
				) );

				// letter spacing
				$wp_customize->add_setting('neal_typography[h6-font-letter-spacing]', array(
						'default' 	=> 0,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_typography[h6-font-letter-spacing]', array(
							'settings'		=> 'neal_typography[h6-font-letter-spacing]',
					        'label' 		=> esc_html__( 'Letter Spacing', 'neal' ),
					        'section' 		=> 'section_typography_font',
					        'input_attrs' 	=> array(
								                'min'	=> -10,
								                'max' 	=> 10,
								                'step'	=> 0.1
								               ),
					        'priority' 		=> 54
					    )
				) );

				// italic
				$wp_customize->add_setting( 'neal_typography[h6-font-italic]', array(
					'default' 	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h6-font-italic]', array(
					'label'		=> esc_html__( 'Italic', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h6-font-italic]',
					'type'		=> 'checkbox',
					'priority' 	=> 55
				) );

				// uppercase
				$wp_customize->add_setting( 'neal_typography[h6-font-uppercase]', array(
					'default'	=> false,
					'type' 		=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control( 'neal_typography[h6-font-uppercase]', array(
					'label'		=> esc_html__( 'Uppercase', 'neal' ),
					'section'   => 'section_typography_font',
					'settings'  => 'neal_typography[h6-font-uppercase]',
					'type'		=> 'checkbox',
					'priority' 	=> 56
				) );


			/*
			***************************************************************
			* #Socials & Copyright
			***************************************************************
			*/

			/* -----------------{ General Options }----------------- */

				// social label
				$wp_customize->add_setting( 'neal_socialcopy[general-social-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				    	'transport'	=> 'refresh',
				    	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_socialcopy[general-social-label]', array(
							'label'    => esc_html__( 'Socials', 'neal' ),
							'section'  => 'section_socialcopy_general',
							'priority' => 1
						)
				) );

				$s_url = array(
							'url1'	=> 'facebook',
							'url2' 	=> 'instagram',
							'url3' 	=> 'twitter',
							'url4' 	=> 'google-plus',
							'url5' 	=> 'pinterest',
							'url6' 	=> 'linkedin',
							'url7' 	=> 'skype',
							'url8' 	=> 'youtube',
							'url9' 	=> 'vk',
							'url10' => 'github'
							);
				$i_num = 1;
				$priority = 2;
				foreach ( $s_url as $url => $icon ) {
					
					// social url
					$wp_customize->add_setting('neal_socialcopy[general-social-'.$url.']', array(
							'default'	=> '',
							'type'	  	=> 'option',
							'transport'	=> 'refresh',
							'sanitize_callback'	=> 'esc_url_raw'
					) );

					$wp_customize->add_control('neal_socialcopy[general-social-'.$url.']', array(
							'settings'	=> 'neal_socialcopy[general-social-'.$url.']',
						    'label' 	=> esc_html__( 'Social URL', 'neal' ),
						    'section' 	=> 'section_socialcopy_general',
						    'type' 		=> 'text',
						    'priority' 	=> $priority,
					) );

					// social icon 1
					$wp_customize->add_setting('neal_socialcopy[general-social-icon'.$i_num.']', array(
							'default'	=> '',
							'type'	  	=> 'option',
							'transport'	=> 'refresh',
							'sanitize_callback' => 'neal_sanitize_select'
					) );

					$wp_customize->add_control('neal_socialcopy[general-social-icon'.$i_num.']', array(
							'settings'	=> 'neal_socialcopy[general-social-icon'.$i_num.']',
						    'label' 	=> esc_html__( 'Icon', 'neal' ),
						    'section' 	=> 'section_socialcopy_general',
						    'type' 		=> 'select',
						    'choices' 	=> array(
										   	'facebook'		=> '&#xf09a;',
										    'instagram'		=> '&#xf16d;',
										    'twitter' 		=> '&#xf099;',
										    'google-plus'	=> '&#xf0d5;',
										    'pinterest' 	=> '&#xf0d2;',
										    'linkedin'		=> '&#xf0e1;',
										    'skype'			=> '&#xf17e;',
										    'youtube'		=> '&#xf167;',
										    'vk'			=> '&#xf189;',
										    'github'		=> '&#xf09b;',
										    'soundcloud' 	=> '&#xf1be;',
										    'behance'		=> '&#xf1b4;',
										   ),
						    'priority'	=> $priority++,
					) );
					
					$i_num++;
					$priority++;
				}

				// social active icon
				$wp_customize->add_setting('neal_socialcopy[general-social-active-icon]', array(
					'default'	=> '',
					'type'	  	=> 'option',
					'transport'	=> 'refresh',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control('neal_socialcopy[general-social-active-icon]', array(
					'settings'	=> 'neal_socialcopy[general-social-active-icon]',
					'label' 	=> esc_html__( 'Active Icon', 'neal' ),
					'section' 	=> 'section_socialcopy_general',
					'type' 		=> 'number',
					'priority' 	=> 22,
				) );

				// social align
				$wp_customize->add_setting('neal_socialcopy[general-social-align]', array(
						'default' 	=> 'left',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_socialcopy[general-social-align]', array(
						'settings'	=> 'neal_socialcopy[general-social-align]',
					    'label' 	=> esc_html__( 'Align', 'neal' ),
					    'section' 	=> 'section_socialcopy_general',
					    'type' 		=> 'select',
					    'choices'   => array(
			                	       	'left' 		=> esc_html__( 'Left', 'neal' ),
			                			'center'	=> esc_html__( 'Center', 'neal' ),
										'right'  	=> esc_html__( 'Right', 'neal' ),
			            					),
					    'priority' 	=> 23
				) );

				// copyright label
				$wp_customize->add_setting( 'neal_socialcopy[general-copy-label]', array( 
						'default' 	=> true,
				    	'type' 		=> 'option',
				   	 	'transport'	=> 'refresh',
				   	 	'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_socialcopy[general-copy-label]', array(
							'label'    => esc_html__( 'Copyright', 'neal' ),
							'section'  => 'section_socialcopy_general',
							'priority' => 24
						)
				) );

				// copyright textarea
				$wp_customize->add_setting('neal_socialcopy[general-copy-text]', array(
						'default'	=> 'Copyright 2017 Powered by WordPress and neal',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_string'
				) );

				$wp_customize->add_control(
					new Neal_Textarea_Control( $wp_customize, 'neal_socialcopy[general-copy-text]', array(
							'label'		=> esc_html__( 'Text', 'neal' ),
							'section' 	=> 'section_socialcopy_general',
							'priority' 	=> 25
						)
				) );

				// copyright align
				$wp_customize->add_setting('neal_socialcopy[general-copy-align]', array(
					'default' 	=> 'left',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'neal_sanitize_select'
				) );

				$wp_customize->add_control('neal_socialcopy[general-copy-align]', array(
					'settings'	=> 'neal_socialcopy[general-copy-align]',
					'label' 	=> esc_html__( 'Align', 'neal' ),
					'section' 	=> 'section_socialcopy_general',
					'type' 		=> 'select',
					'choices' 	=> array(
								  	'left'		=> 'Left',
									'center'	=> 'Center',
									'right' 	=> 'Right'
								   ),
					'priority' 	=> 26
				) );

			/* -----------------{ Spacing Options }----------------- */

				// socials
				$wp_customize->add_setting( 'neal_socialcopy[social-spac-label]', array( 'sanitize_callback'	=> 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_socialcopy[social-spac-label]', array(
							'label'    => esc_html__( 'Socials', 'neal' ),
							'section'  => 'section_socialcopy_spacing',
							'priority' => 1
						)
				) );

				// socials width
				$wp_customize->add_setting('neal_socialcopy[social-spac-width]', array(
					'default' 	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-spac-width]', array(
							'settings'		=> 'neal_socialcopy[social-spac-width]',
					        'label' 		=> esc_html__( 'Width', 'neal' ),
					        'section' 		=> 'section_socialcopy_spacing',
					        'input_attrs' 	=> array(
								                'min' => 30,
								                'max' => 120
								               ),
					        'priority' 		=> 2
					    )
				) );

				// socials height
				$wp_customize->add_setting('neal_socialcopy[social-spac-height]', array(
					'default' 	=> 35,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-spac-height]', array(
							'settings'		=> 'neal_socialcopy[social-spac-height]',
					        'label' 		=> esc_html__( 'Height', 'neal' ),
					        'section' 		=> 'section_socialcopy_spacing',
					        'input_attrs' 	=> array(
								                'min' => 30,
								                'max' => 120
								               ),
					        'priority' 		=> 3
					    )
				) );

				// margin right
				$wp_customize->add_setting('neal_socialcopy[social-spac-margin-right]', array(
						'default' 	=> 15,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-spac-margin-right]', array(
							'settings'		=> 'neal_socialcopy[social-spac-margin-right]',
					        'label' 		=> esc_html__( 'Margin Right', 'neal' ),
					        'section' 		=> 'section_socialcopy_spacing',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 70
								                ),
					        'priority' 		=> 4
					    )
				) );

			/* -----------------{ Styling Options }----------------- */

				// socials
				$wp_customize->add_setting( 'neal_socialcopy[social-styl-label]', array( 'sanitize_callback' => 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_socialcopy[social-styl-label]', array(
							'label'    => esc_html__( 'Socials', 'neal' ),
							'section'  => 'section_socialcopy_styling',
							'priority' => 1
						)
				) );

				// background color
				$wp_customize->add_setting('neal_socialcopy[social-styl-bg-color]', array(
						'default' 	=> '#f5f5f5',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[social-styl-bg-color]', array(
							'label'		=> esc_html__( 'Background Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[social-styl-bg-color]',
							'priority' 	=> 2
						) 
				) );

				// font color
				$wp_customize->add_setting('neal_socialcopy[social-styl-font-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[social-styl-font-color]', array(
							'label'		=> esc_html__( 'Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[social-styl-font-color]',
							'priority' 	=> 3
						) 
				) );

				// background hover color
				$wp_customize->add_setting('neal_socialcopy[social-styl-bg-hover-color]', array(
						'default' 	=> '#474747',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[social-styl-bg-hover-color]', array(
							'label'		=> esc_html__( 'Background Hover Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[social-styl-bg-hover-color]',
							'priority' 	=> 4
						) 
				) );

				// font hover color
				$wp_customize->add_setting('neal_socialcopy[social-styl-font-hover-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[social-styl-font-hover-color]', array(
							'label'		=> esc_html__( 'Hover Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[social-styl-font-hover-color]',
							'priority' 	=> 5
						) 
				) );

				// radius label
				$wp_customize->add_setting( 'neal_socialcopy[social-styl-child-radius-label]', array( 
						'default' 	=> true,
						'type'		=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_checkbox'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Radio_Control( $wp_customize, 'neal_socialcopy[social-styl-child-radius-label]', array(
							'label'    => esc_html__( 'Corner Radius', 'neal' ),
							'section'  => 'section_socialcopy_styling',
							'priority' => 6
						)
				) );

				// radius
				$wp_customize->add_setting('neal_socialcopy[social-styl-child-radius]', array(
						'default' 		=> 50,
						'type'	  		=> 'option',
						'transport'		=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-styl-child-radius]', array(
							'settings'		=> 'neal_socialcopy[social-styl-child-radius]',
					        'label' 		=> esc_html__( 'Radius', 'neal' ),
					        'section' 		=> 'section_socialcopy_styling',
					        'input_attrs' 	=> array(
								                'min' => 0,
								                'max' => 50
								               ),
					        'priority' 		=> 7
					    )
				) );

				// copyright
				$wp_customize->add_setting( 'neal_socialcopy[copy-styl-label]', array( 
					'sanitize_callback' => 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_socialcopy[copy-styl-label]', array(
							'label'    => esc_html__( 'Copyright', 'neal' ),
							'section'  => 'section_socialcopy_styling',
							'priority' => 8
						)
				) );

				// font color
				$wp_customize->add_setting('neal_socialcopy[copy-styl-font-color]', array(
						'default' 	=> '#fff',
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[copy-styl-font-color]', array(
							'label'		=> esc_html__( 'Text Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[copy-styl-font-color]',
							'priority' 	=> 9
						) 
				) );

				// link color
				$wp_customize->add_setting('neal_socialcopy[copy-styl-link-color]', array(
					'default' 	=> '#a9aaac',
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback' => 'sanitize_hex_color'
				) );

				$wp_customize->add_control( 
					new WP_Customize_Color_Control( $wp_customize, 'neal_socialcopy[copy-styl-link-color]', array(
							'label'		=> esc_html__( 'Link Color', 'neal' ),
							'section'   => 'section_socialcopy_styling',
							'settings'  => 'neal_socialcopy[copy-styl-link-color]',
							'priority' 	=> 10
						) 
				) );

			/* -----------------{ Font Options }----------------- */

				// socials label
				$wp_customize->add_setting( 'neal_socialcopy[social-font-label]', array( 'sanitize_callback' => 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_socialcopy[social-font-label]', array(
							'label'    => esc_html__( 'Socials', 'neal' ),
							'section'  => 'section_socialcopy_font',
							'priority' => 1
						)
				) );

				// font size
				$wp_customize->add_setting('neal_socialcopy[social-font-size]', array(
					'default' 	=> 16,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-font-size]', array(
							'settings'		=> 'neal_socialcopy[social-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_socialcopy_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 2
					    )
				) );


				// line height
				$wp_customize->add_setting('neal_socialcopy[social-font-line-height]', array(
					   	'default' 	=> 35,
						'type'	  	=> 'option',
						'transport'	=> 'postMessage',
						'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[social-font-line-height]', array(
							'settings'		=> 'neal_socialcopy[social-font-line-height]',
					        'label' 		=> esc_html__( 'Line Height', 'neal' ),
					        'section' 		=> 'section_socialcopy_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 80
								               ),
					        'priority' 		=> 3
					    )
				) );

				// copy label
				$wp_customize->add_setting( 'neal_socialcopy[copy-font-label]', array( 
					'sanitize_callback' => 'neal_sanitize_label'
				) );

				$wp_customize->add_control(
					new Neal_Custom_Label_Control( $wp_customize, 'neal_socialcopy[copy-font-label]', array(
							'label'    => esc_html__( 'Copyright', 'neal' ),
							'section'  => 'section_socialcopy_font',
							'priority' => 4
						)
				) );

				// font size
				$wp_customize->add_setting('neal_socialcopy[copy-font-size]', array(
					'default' 	=> 15,
					'type'	  	=> 'option',
					'transport'	=> 'postMessage',
					'sanitize_callback'	=> 'neal_sanitize_number'
				) );
				
				$wp_customize->add_control(
					new Neal_Custom_Slider_Control( $wp_customize, 'neal_socialcopy[copy-font-size]', array(
							'settings'		=> 'neal_socialcopy[copy-font-size]',
					        'label' 		=> esc_html__( 'Font Size', 'neal' ),
					        'section' 		=> 'section_socialcopy_font',
					        'input_attrs' 	=> array(
								                'min' => 10,
								                'max' => 50
								               ),
					        'priority' 		=> 5
					    )
				) );

		}

		/**
		 * Enqueue Scripts & Styles
		 *
		 * @since  1.0.0
		 */
		public function customizer_enqueue_scripts() {

			// send home url to javascript
			$neal_home_url = array( esc_url(get_stylesheet_directory_uri()) );

			// register styles
			wp_register_style( 'fontawesome', get_template_directory_uri() .'/css/font-awesome.min.css' );

			wp_register_style( 'ionicons', get_template_directory_uri() . '/css/ionicons.min.css' );

			wp_register_style( 'select2', get_template_directory_uri() . '/inc/customizer/css/select2.min.css' );

			wp_register_style( 'neal-customizer-custom-ui', get_template_directory_uri() . '/inc/customizer/css/custom-ui.css' );

			// enqueue script
			wp_enqueue_script( 'select2', get_template_directory_uri() . '/inc/customizer/js/select2.min.js', array(), null, true );

			wp_enqueue_script( 'neal-customizer-custom-ui', get_template_directory_uri() . '/inc/customizer/js/custom-ui.js', array( 'customize-controls', 'jquery' ), null, true );

			wp_localize_script( 'neal-customizer-custom-ui', 'nealCustomizerCustom', array(
				'reset'   => esc_html__( 'Reset', 'neal' ),
				'nonce'   => array(
					'reset' => wp_create_nonce( 'neal-customizer-reset' ),
				)
			) );

			// enqueue styles
			wp_enqueue_style( 'fontawesome' );
			wp_enqueue_style( 'ionicons' );
			wp_enqueue_style( 'select2' );
			wp_enqueue_style( 'neal-customizer-custom-ui' );

		}

		/**
		 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
		 *
		 * @since  1.0.0
		 */
		public function customizer_live_preview() {

			// send theme customizer data to javascript
			$options = array(
				'body'			=> get_option( 'neal_body' ),
				'header'		=> get_option( 'neal_header' ),
				'logo'			=> get_option( 'neal_logo' ),
				'menu'			=> get_option( 'neal_menu' ),
				'footer'		=> get_option( 'neal_footer' ),
				'sidebar'		=> get_option( 'neal_sidebar' ),
				'blog'			=> get_option( 'neal_blog' ),
				'homepage'		=> get_option( 'neal_homepage' ),
				'inputs'		=> get_option( 'neal_inputs' ),
				'pagination'	=> get_option( 'neal_pagination' ),
				'socialcopy'	=> get_option( 'neal_socialcopy' )
				);

			// register
			wp_enqueue_script( 'neal-customizer', get_template_directory_uri() . '/inc/customizer/js/customizer.js', array( 'customize-preview' ), '1.0', true );
			wp_localize_script( 'neal-customizer', 'neal_options', $options );
		}

		/**
		 * Layout classes
		 * Adds 'right-sidebar' and 'left-sidebar' classes to the body tag
		 *
		 * @param  array $classes current body classes.
		 * @return string[] modified body classes
		 * @since  1.0.0
		 */
		public function layout_class( $classes ) {
			global $post;

			// blog page
			if ( neal_get_option('blog_sidebar-label') ) {
				neal_is_blog_pages() ? $classes[] = neal_get_option('blog_sidebar-align') : '';
			}

			// blog single
			if ( isset( $post ) ) {
				$items = neal_get_post_options_data();

				if ( $items['sidebar'] ) {
					is_single() ? $classes[] = $items['sidebar_align'] : '';
				}
			}

			// shop page
			if ( neal_get_option('shop_sidebar-label') ) {
				neal_is_shop_pages() ? $classes[] = neal_get_option('shop_sidebar-align') : '';
			}

			// shop single
			if ( neal_get_option('shop_single_sidebar-label') ) {
				is_singular('product') ? $classes[] = neal_get_option('shop_single_sidebar-align') : '';
			}

			return $classes;
		}

		/**
		 * Get logo text
		 */
		public function get_logo_text() {
			return neal_get_option('logo_text');
		}

		/**
		 * Get logo tagline
		 */
		public function get_logo_tagline() {
			return neal_get_option('logo_tagline');
		}

		/**
		 * Get Copyright text
		 */
		public function get_copyright_text() {
			return neal_get_option('socialcopy_general-copy-text');
		}

}

endif;

return new Neal_Customizer();