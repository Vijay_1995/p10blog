<?php
/**
 * Neal Theme Customizer default options Class
 *
 * @author   TheSpan
 * @package  neal
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Customizer_Defaults' ) ) :

	/**
	 * The Neal Customizer Defaults class
	 */

	class Neal_Customizer_Defaults {

		/**
		 * Setup class.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			add_option( 'neal_activated', false );

			if ( ! get_option('neal_activated') ) {
				add_action( 'after_switch_theme',	array( $this, 'setup_options' ) );
			}

			add_action( 'wp_ajax_neal_customizer_reset', array( $this, 'ajax_customizer_reset' ) );
		}

		/**
		 * The Cutomizer default values
		 *
		 * @since  1.0.0
		 */
		public function setup_options() {

			// Body options
			$body = array(
				'layout-max-width'	=> 1360,
				'layout-width'		=> 'full',
				'layout-align'		=> 'center',

				'general-styl-bg-color'		=> '#fff',
				'general-styl-bg-color-2'	=> '#fff',
				'general-styl-font-color'	=> '#000',
				'general-styl-link-color'	=> '#000',
				'general-styl-link-hover-color'	=> '#9a9a9a',
				'general-styl-selection-bg-color'	=> '#e7f2f8',
				'general-styl-selection-font-color'	=> '#000',
				'general-styl-child-bg-img-label'		=> false,
				'general-styl-child-bg-img'				=> '',
				'general-styl-child-bg-img-size'		=> 'cover',
				'general-styl-child-bg-img-attchmnt'	=> 'fixed',

				'general-font-family'			=> 'Open+Sans',
				'general-font-size'				=> 16,
				'general-font-weight'			=> 400,
				'general-font-letter-spacing'	=> 0

				);

			// Header options
			$header = array(
				'general-header-layout'		=> 3,
				'general-header-sticky'		=> true,
				'page-header-bread'			=> false,
				'social-media-label'		=> false,

				'general-header-spac-height'		=> 120,
				'sticky-header-spac-height'			=> 80,
				'page-header-spac-padding-top'		=> 0,
				'page-header-spac-padding-bottom'	=> 0,

				'general-header-styl-bg-color'				=> '#fff',
				'general-header-styl-bg-color-2'			=> '#000',
				'general-header-styl-child-bg-img-label'	=> false,
				'general-header-styl-child-bg-img'			=> '',
				'general-header-styl-child-bg-img-size'		=> 'cover',
				'general-header-styl-child-bg-img-attchmnt'	=> 'fixed',
				'sticky-header-styl-bg-color'				=> '#000',
				'sticky-header-styl-link-color'				=> '#fff',
				'sticky-header-styl-link-hover-color' 		=> '#fff',
				'page-header-styl-bg-color'					=> '#fff',
				'page-header-styl-title-color'				=> '#000',
				'page-header-styl-font-color'				=> '#999',
				'page-header-styl-link-color'				=> '#000',
				'page-header-styl-link-hover-color'			=> '#9a9a9a'

				);

			// Logo & Tagline options
			$logo = array(
				'img-label'			=> true,
				'img' 				=> get_template_directory_uri() .'/images/logo-dark.png',
				'img-2' 			=> get_template_directory_uri() .'/images/logo-light.png',
				'text-label'		=> false,
				'text'				=> get_option( 'blogname' ),
				'tagline-label'		=> false,
				'tagline'			=> get_option( 'blogdescription' ),
				'tagline-align'		=> 'left',

				'img-spac-max-height'			=> 33,

				'logo-text-styl-font-color'		=> '#000',
				'logo-text-styl-font-color-2'	=> '#fff',
				'tagline-styl-font-color'		=> '#000',
				'tagline-styl-font-color-2'		=> '#fff',

				'logo-text-font-family'			=> 'Playfair+Display',
				'logo-text-font-size'			=> 38,
				'logo-text-font-weight'			=> 900,
				'logo-text-font-line-height'	=> 1,
				'logo-text-font-letter-spacing'	=> 2,
				'logo-text-font-uppercase'		=> false,
				'tagline-font-size'				=> 11,
				'tagline-font-weight'			=> 400,
				'tagline-font-letter-spacing'	=> 0

				);

			// Menus options
			$menu = array(
				'left-menu-items-align'	=> 'right',
				'right-menu-items-align'=> 'left',
				'menu-items-align'		=> 'center',
				'submenu-items-align'	=> 'left',
				'shop-label'			=> false,
				'shop-icon'				=> 'ion-bag',
				'search-label'				=> true,
				'search-icon'				=> 'ion-search',
				'ham-menu-label'			=> false,
				'footer-menu-items-align'	=> 'center',

				'menu-items-spac-padding-top-bottom' 	=> 0,
				'menu-items-spac-padding-right-left'	=> 0,
				'menu-items-spac-margin-right'			=> 10,
				'submenu-items-spac-width'				=> 250,
				'submenu-items-spac-padding-top-bottom'	=> 8,
				'submenu-items-spac-padding-right-left'	=> 25,
				'submenu-items-spac-margin-top'			=> 79,
				'footer-menu-items-spac-padding-top-bottom' => 40,
				'footer-menu-items-spac-margin-right'		=> 20,

				'menu-items-styl-link-color'			=> '#000',
				'menu-items-styl-link-hover-color'		=> '#000',
				'menu-items-styl-link-active-color'		=> '#000',
				'menu-items-styl-icons-color'			=> '#000',
				'menu-items-styl-active-item'			=> true,
				'submenu-items-styl-bg-color'			=> '#fff',
				'submenu-items-styl-link-color'			=> '#000',
				'submenu-items-styl-link-hover-color'	=> '#000',
				'submenu-items-styl-shadow'				=> true,
				'submenu-items-styl-child-border-label'	=> false,
				'submenu-items-styl-child-border-size'	=> 4,
				'submenu-items-styl-child-border-style'	=> 'solid',
				'submenu-items-styl-child-border-color'	=> '#eee',
				'footer-menu-styl-bg-color'				=> '#000',
				'footer-menu-items-styl-link-color'		=> '#fff',

				'menu-items-font-family'	=> 'Open+Sans',
				'menu-items-font-size'		=> 14,
				'menu-items-font-weight'	=> 600,
				'menu-items-font-letter-spacing'	=> 0,
				'menu-items-font-icon-size'	=> 19,
				'menu-items-font-italic'	=> false,
				'menu-items-font-uppercase'	=> true,
				'submenu-items-font-size'	=> 14,
				'submenu-items-font-weight'	=> 600,
				'submenu-items-font-letter-spacing'	=> 0,
				'submenu-items-font-italic'		=> false,
				'submenu-items-font-uppercase'	=> true,
				'mobile-menu-font-size'		=> 35,
				'mobile-menu-font-weight'	=> 400,
				'mobile-menu-font-letter-spacing'	=> 0,
				'mobile-menu-font-italic'		=> false,
				'mobile-menu-font-uppercase'	=> false,
				'footer-menu-items-font-size'	=> 14,
				'footer-menu-items-font-weight' => 500,
				'footer-menu-items-font-italic'	=> false,
				'footer-menu-items-font-uppercase' => true

				);

			// Footer options
			$footer = array(
				'general-footer-layout'	=> 3,
				'general-footer-fixed'	=> false,
				'wtitle-align'			=> 'left',
				'instagram-label'		=> false,
				'instagram-layout'		=> 'boxed',
				'instagram-title'		=> '',
				'instagram-title-link'	=> true,
				'instagram-title-pos'	=> false,
				'instagram-username'	=> '',
				'instagram-number'		=> 10,
				'instagram-columns'		=> 5,
				'instagram-carousel'	=> true,
				'instagram-carousel-nav'	=> false,
				'instagram-carousel-loop'	=> false,

				'general-spac-padding-top'		=> 80,
				'general-spac-padding-bottom'	=> 80,
				'wtitle-spac-margin-bottom'		=> 20,
				'instagram-spac-item-padding-right-left' => 0,

				'general-styl-bg-color'			=> '#000',
				'general-styl-font-color'		=> '#fff',
				'widgets-styl-font-color'		=> '#fff',
				'widgets-styl-link-color'		=> '#9a9a9a',
				'widgets-styl-link-hover-color' => '#fff'

				);

			// Sidebar options
			$sidebar = array(
				'wtitle-align'	=> 'left',

				'general-spac-padding-top-bottom'	=> 0,
				'general-spac-padding-right-left'	=> 0,
				'general-spac-margin-bottom'		=> 40,
				'wtitle-spac-margin-bottom'			=> 20,

				'general-styl-bg-color'				=> '#fff',
				'general-styl-child-border-label'	=> false,
				'general-styl-child-border-size'	=> 0,
				'general-styl-child-border-style'	=> 'solid',
				'general-styl-child-border-color'	=> '',
				'wtitle-styl-font-color'			=> '#000',
				'about-me-styl-radius'				=> 50,

				'wtitle-font-size'					=> 24,
				'wtitle-font-weight'				=> 600,
				'wtitle-font-italic'				=> false,
				'wtitle-font-uppercase'				=> false,
				'categories-image-font-size'		=> 35,
				'categories-image-font-weight'		=> 600,
				'categories-image-font-uppercase'	=> false,

				);

			// Product options
			$product = array(
				'star-rating-pos'		=> 1,
				'star-rating-align'		=> 'center',
				'sale-type'				=> 'off',
				'title-pos'				=> 2,
				'title-align'			=> 'center',
				'prices-pos'			=> 3,
				'prices-align'			=> 'center',

				'star-rating-spac-margin-bottom'	=> 15,
				'star-rating-spac-margin-right'		=> 2,
				'title-spac-margin-bottom'			=> 15,
				'prices-spac-margin-bottom'			=> 0,

				'star-rating-styl-star-color'	=> '#dd4b39',
				'star-rating-styl-star-color-2'	=> '#d4d4d4',
				'sale-styl-font-color'			=> '#000',
				'add-to-cart-styl-font-color'	=> '#000',
				'add-to-cart-styl-font-hover-color'	=> '#000',
				'title-styl-font-color'				=> '#000',
				'title-styl-font-hover-color'		=> '#9a9a9a',
				'special-price-styl-font-color'		=> '#000',
				'old-price-styl-font-color'			=> '#9a9a9a',

				'star-rating-font-size'			=> 13,
				'sale-font-size'				=> 13,
				'title-font-size'				=> 16,
				'title-font-weight'				=> 600,
				'title-font-italic'				=> false,
				'title-font-uppercase'			=> false,
				'special-price-font-size'		=> 16,
				'old-price-font-size'			=> 17,
				'old-price-font-line-through'	=> true
				
				);

			// Blog page options
			$blog = array(
				'layout-columns'		=> 1,
				'sidebar-label'			=> true,
				'sidebar-align'			=> 'sidebar-right',
				'sidebar-sticky'		=> false,
				'categories-label'		=> true,
				'categories-align'		=> 'left',
				'date-label'			=> true,
				'date-align'			=> 'left',
				'date-icon'				=> 'none',
				'author-label'			=> false,
				'author-align'			=> 'left',
				'author-icon'			=> 'none',
				'title-label'			=> true,
				'title-align'			=> 'left',
				'description-label'		=> true,
				'description-display_as'		=> 'p_excerpt',
				'description-excerpt_length'	=> 20,
				'description-align'		=> 'left',
				'read-more-label'		=> true,
				'read-more-align'		=> 'left',

				'media-spac-margin-bottom'			=> 15,
				'title-spac-margin-bottom'			=> 15,
				'meta-items-spac-margin-bottom'		=> 10,
				'description-spac-margin-bottom'	=> 15,

				'title-styl-font-color'			=> '#000',
				'title-styl-font-hover-color'	=> '#9a9a9a',
				'meta-styl-font-color'			=> '#999',
				'meta-styl-link-color'			=> '#000',
				'meta-styl-link-hover-color'	=> '#9a9a9a',
				'meta-styl-icon-color'			=> '#000',
				'categories-styl-font-color'		=> '#fff',
				'categories-styl-font-hover-color'	=> '#000',
				'categories-styl-bg-color'			=> '#22bb66',
				'categories-styl-bg-hover-color'	=> '#22bb66',
				'desc-styl-font-color'			=> '#000',

				'title-font-size'			=> 28,
				'title-font-weight'			=> 600,
				'title-font-line-height'	=> 1.3,
				'title-font-letter-spacing' => 0,
				'title-font-italic'			=> false,
				'title-font-uppercase'		=> false,
				'meta-font-size'			=> 15,
				'meta-font-italic'			=> false,
				'meta-font-uppercase'		=> false,
				'categories-font-size'		=> 14,
				'categories-font-weight'	=> 600,
				'categories-font-letter-spacing'	=> 0.5,
				'categories-font-italic'	=> false,
				'categories-font-uppercase'	=> true,
				'desc-font-size'			=> 16,
				'desc-font-weight'			=> 400,
				'desc-font-line-height'		=> 1.7,
				'desc-font-letter-spacing'	=> 0,
				'desc-font-italic'			=> false,
				'desc-font-uppercase'		=> false,

				);

			// Blog single options
			$blog_single = array(
				'general-post-layout'			=> 1,
				'general-post-parallax' 		=> false,
				'general-post-rotate-cap-text' 	=> false,
				'sidebar-label'			=> false,
				'sidebar-align'			=> 'sidebar-right',
				'sidebar-sticky'		=> false,
				'featured-img-label'	=> true,
				'title-pos'				=> 'below',
				'title-align'			=> 'left',
				'bread-label'			=> false,
				'bread-pos'				=> 'below',
				'bread-align'			=> 'left',
				'meta-label'			=> true,
				'meta-pos'				=> 'below',
				'meta-category'			=> true,
				'meta-date'				=> true,
				'meta-author'			=> true,
				'meta-align'			=> 'left',
				'sharing-label'			=> false,
				'sharing-fb'			=> true,
				'sharing-tw'			=> true,
				'sharing-google-p'		=> true,
				'sharing-linke'			=> true,
				'sharing-pint'			=> true,
				'subscribe-box-label'	=> false,
				'subscribe-box-title'	=> 'Never miss a story!',
				'subscribe-box-desc'	=> 'Sign up for our newsletter and get the best stories delivered to your inbox.',
				'subscribe-box-place'	=> 'Email Address',
				'subscribe-box-button'  => 'Sign Me Up',
				'author-box-label'		=> false,
				'author-box-img-size' 	=> 120,
				'related-posts-label'	=> false,
				'related-posts-title'	=> 'You Might Also Like',
				'related-posts-pos'		=> 'below-author-box',
				'post-navigation-label' => true,
				'post-navigation-title'	=> true,
				'post-navigation-icon-fa' => 'long-arrow',

				'media-spac-margin-bottom'	=> 30,
				'title-spac-margin-bottom'	=> 30,
				'bread-spac-margin-bottom'	=> 0,
				'meta-spac-margin-bottom'	=> 0,
				'caption-image-spac-margin-left'  => 0,
				'caption-image-spac-margin-right' => 0,

				'title-font-size'		=> 50,
				'title-font-weight'		=> 600,
				'title-font-line-height'	=> 1.2,
				'title-font-letter-spacing'	=> 0,
				'title-font-uppercase'	=> false,
				'content-font-family'	=> 'Open+Sans',
				'content-font-size'		=> 16,
				'content-font-weight'	=> 400,
				'content-font-line-height'		=> 1.8,
				'content-font-letter-spacing'	=> 0,

				);

			// Shop options
			$shop = array(
				'layout-columns'		=> 3,
				'sidebar-label'			=> true,
				'sidebar-align'			=> 'sidebar-right',
				'tbar-label'			=> true,
				'tbar-align'			=> 'left',
				'tbar-grid-icon'		=> 'fa-th-large',
				'tbar-list-icon'		=> 'fa-th-list',
				'tbar-icon-size'		=> 21,
				'result-t-label'		=> true,
				'result-t-align'		=> 'center',
				'ordering-label'		=> true,
				'ordering-align'		=> 'right',

				'tbar-spac-margin-left'		=> 0,
				'tbar-spac-margin-right'	=> 15,
				'result-t-spac-margin-left'		=> 30,
				'result-t-spac-margin-right'	=> 0,
				'ordering-spac-margin-left'		=> 0,
				'ordering-spac-margin-right'	=> 0,

				'tbar-styl-link-color'			=> '#999',
				'tbar-styl-link-active-color'	=> '#000',
				'tbar-styl-link-hover-color'	=> '#000',
				'result-t-styl-font-color'	=> '#999',
				'ordering-styl-color'		=> '#999',
				'ordering-styl-hover-color'	=> '#000',
				'filter-sld-styl-color'		=> '#000',
				'filter-sld-styl-color-2'	=> '#eee',

				'result-t-font-size'		=> 16,
				'result-t-font-weight'		=> 400,
				'result-t-font-letter-spacing'	=> 0,
				'result-t-font-italic'		=> false,
				'result-t-font-uppercase'	=> false,
				'ordering-font-size'		=> 16,
				'ordering-font-weight'		=> 400,
				'ordering-font-letter-spacing'	=> 0,
				'ordering-font-italic'		=> false,
				'ordering-font-uppercase'	=> false

				);

			// Shop single options
			$shop_single = array(
				'sidebar-label'			=> false,
				'sidebar-align'			=> 'sidebar-right',
				'images-align'			=> 'left',
				'thumbs-pos'			=> 'vertical',
				'thumbs-align'			=> 'right',
				'summary-content-align'	=> 'left',
				'social-sharing-align'	=> 'left',
				'sharing-fb'			=> false,
				'sharing-tw'			=> false,
				'sharing-google-p'		=> false,
				'sharing-linke'			=> false,
				'sharing-pint'			=> false,
				'tabs-align'			=> 'left',

				'title-spac-margin-bottom'		=> 15,
				'product-rating-spac-margin-bottom'	=> 15,
				'product-prices-spac-margin-bottom'	=> 25,
				'product-meta-spac-margin-top'	=> 25,
				'tabs-tab-spac-padding-top-bottom'	=> 8,
				'tabs-tab-spac-padding-right-left'	=> 0,
				'tabs-tab-spac-margin-right'		=> 25,

				'title-styl-font-color'				=> '#000',
				'product-prices-styl-font-color'	=> '#000',
				'product-prices-styl-font-color-2'	=> '#999',
				'tabs-tab-styl-link-color'			=> '#9a9a9a',
				'tabs-tab-styl-link-hover-color'	=> '#000',
				'tabs-tab-styl-link-active-color'	=> '#000',
				'tabs-tab-styl-child-border-label'			=> false,
				'tabs-tab-styl-child-border-bottom-size'	=> 3,
				'tabs-tab-styl-child-border-bottom-style'	=> 'solid',
				'tabs-tab-styl-child-border-bottom-color'	=> '#000',

				'title-font-size'			=> 33,
				'title-font-weight'			=> 600,
				'title-font-line-height'	=> 1.2,
				'title-font-letter-spacing'	=> 1.5,
				'title-font-italic'			=> false,
				'title-font-uppercase'		=> false,
				'title-font-underline'		=> false,
				'product-prices-font-size'	=> 18,
				'product-prices-font-weight' => 600,
				'tabs-tab-font-size'		=> 18,
				'tabs-tab-font-weight'		=> 400,
				'tabs-tab-font-uppercase'	=> false,

				);

			// Homepage options
			$homepage = array(
				'sidebar-1-align'	=> 'right',
				'sidebar-1-sticky'	=> false,
				'sidebar-2-align'	=> 'right',
				'sidebar-2-sticky'	=> false,
				'sidebar-3-align'	=> 'right',
				'sidebar-3-sticky'	=> false,

				'newsletter-spac-margin-top'	=> 0,
				'newsletter-spac-margin-bottom'	=> 0,

				'general-styl-tfl-color'			 	 => '#eaeaea',
				'full-posts-styl-bg-color'				 => '#e7f2f8',
				'full-posts-styl-tfl-color'			 	 => '#c8d1d6',
				'full-posts-styl-font-color' 			 => '#000',
				'full-posts-styl-post-title-color'		 => '#000',
				'full-posts-styl-post-title-hover-color' => '#9a9a9a',
				'full-posts-styl-read-more-color'	     => '#000',
				'full-posts-styl-read-more-hover-color'	 => '#000',
				'newsletter-styl-bg-color'				 => '#e7f2f8',
				'newsletter-styl-title-color'			 => '#000',
				'newsletter-styl-desc-color'			 => '#000',

				'general-section-title-font-size'		=> 40,
				'general-tfl-font-size'					=> 120,
				'carousel-posts-title-font-size'		=> 35,
				'full-featured-post-title-font-size' 	=> 50,
				'masonry-posts-title-font-size'			=> 30,
				'masonry-posts-second-title-font-size'	=> 20,
				'2-grid-posts-title-font-size'			=> 30,
				'full-posts-title-font-size'			=> 28,
				'list-posts-title-font-size'			=> 28,
				'slider-posts-title-font-size'			=> 22,
				'3-grid-posts-title-font-size'			=> 24,

			);

			// Comments options
			$comments = array(
				'blog-display-label'			=> true,
				'comment-type'					=> 'default',
				'comment-type-disqus-shortname'	=> '',
				'comment-type-facebook-appid'	=> '',
				'comments-texts-label'			=> true,
				'comment-texts-moderation' 		=> 'Your comment is awaiting moderation!',
				'comment-texts-closed'			=> 'Hey! comments are closed.',
				'comments-title-align'			=> 'center',
				'author-img-size'				=> '100',

				'author-img-spac-margin-right'				=> 30,
				'comment-content-spac-vertical-gutter'		=> 15,

				'author-img-styl-radius'			=> 50,
				'comment-content-styl-bg-color'		=> '#f5f5f5',
				'comment-content-styl-font-color'	=> '#000'

				);

			// Inputs options
			$inputs = array(
				'general-spac-padding-top-bottom'	=> 10,
				'general-spac-padding-right-left'	=> 15,
				'submit-button-spac-padding-top-bottom'	=> 12,
				'submit-button-spac-padding-right-left'	=> 27,

				'general-styl-font-color'		=> '#000',
				'general-styl-font-focus-color'	=> '#000',
				'general-styl-border-focus-color' => '#000',
				'general-styl-child-border-label'			=> true,
				'general-styl-child-border-bottom-size'		=> 3,
				'general-styl-child-border-bottom-style'	=> 'solid',
				'general-styl-child-border-bottom-color'	=> '#d9d9d9',
				'general-styl-child-radius-label'		=> false,
				'general-styl-child-radius'				=> 0,
				'submit-button-styl-bg-color'			=> '#22bb66',
				'submit-button-styl-font-color'			=> '#fff',
				'submit-button-styl-bg-hover-color'		=> '#000',
				'submit-button-styl-font-hover-color'	=> '#fff',
				'submit-button-styl-border-hover-color'	=> '#d9d9d9',
				'submit-button-styl-child-border-label'		=> false,
				'submit-button-styl-child-border-size'		=> 1,
				'submit-button-styl-child-border-style'		=> 'solid',
				'submit-button-styl-child-border-color'		=> 'transparent',
				'submit-button-styl-child-radius-label'			=> false,
				'submit-button-styl-child-radius'				=> 0,

				'general-font-size'						=> 16,
				'general-font-weight'					=> 600,
				'general-font-letter-spacing'			=> 0,
				'submit-button-font-size'				=> 13,
				'submit-button-font-weight'				=> 700,
				'submit-button-font-letter-sapcing'		=> 0,
				'submit-button-font-uppercase'			=> true,

				);

			// Contact page options
			$contact = array(
				'form-label'			=> true,
				'form-title'			=> 'Write To Us',
				'form-reciever-email'	=> '',
				'info-label'			=> true,
				'info-title'			=> 'Our Info',
				'info-address'			=> 'Jordania 86 BGT-245, Hallstatt, Upper Austria, Austria',
				'info-address-icon'		=> 'fa-map-marker',
				'info-phone'			=> '+995 557-562-776',
				'info-phone-icon'		=> 'fa-phone',
				'info-email'			=> 'neal@google.com',
				'info-email-icon'		=> 'fa-envelope',
				'info-short-info'		=> 'Jujubes marzipan jelly tart cupcake liquorice icing gummies. Croissant bear claw fruitcake bonbon wafer. Marshmallow tootsie roll marzipan cake sesame snaps liquorice chupa chups.',
				'contact_info-short-image'	=> '',
				'google-map-label'		=> true,
				'google-map-layout'		=> 'full',
				'google-map-pos'		=> 'top',
				'google-map-api_key'	=> 'AIzaSyAU1V6fY6OcyN7Fd33Llm5wDHwU3jr42fU',
				'google-map-location'	=> '',
				'google-map-type'		=> 'ROADMAP',
				'google-map-zoom'		=> 8,
				'google-map-tcontrol'	=> true,
				'google-map-nav'		=> true,
				'google-map-marker-icon' => '',

				'google-map-spac-height'			=> 400,
				'google-map-spac-margin-top-bottom'	=> 30

				);

			// Pagination options
			$pagination = array(
				'navigation-pag-type'	=> 'numeric',
				'navigation-align'		=> 'right',
				'navigation-icon-fa'	=> 'long-arrow',

				'navigation-spac-width'			=> 0,
				'navigation-spac-height'		=> 0,
				'navigation-spac-margin-right'	=> 8,

				'navigation-styl-bg-color'			=> '#fff',
				'navigation-styl-bg-hover-color'	=> '#fff',
				'navigation-styl-bg-active-color'	=> '#fff',
				'navigation-styl-font-color'		=> '#000',
				'navigation-styl-link-color'		=> '#999',
				'navigation-styl-link-hover-color'	=> '#000',
				'navigation-styl-font-active-color'	=> '#000',
				'navigation-styl-radius'			=> 50,

				'navigation-font-size'			=> 50,
				'navigation-font-weight'		=> 700,
				'navigation-font-line-height'	=> 30,
				'navigation-font-uppercase'		=> false

				);

			// Typography options
			$typography = array(
				'subset-label'			=> false,
				'subset-latin'			=> false,
				'subset-cyrillic'		=> false,
				'subset-greek'			=> false,
				'subset-vietnamese'		=> false,
				'subset-arabic'			=> false,
				'subset-hindi'			=> false,

				'paragraph-font-family' 		=> 'Open+Sans',
				'paragraph-font-size'			=> 16,
				'paragraph-font-weight' 		=> 400,
				'paragraph-font-line-height'	=> 1.8,
				'paragraph-font-letter-spacing'	=> 0,
				'paragraph-font-italic'			=> false,
				'paragraph-font-uppercase'		=> false,
				'h1-font-family' 				=> 'Playfair+Display',
				'h1-font-size'					=> 40,
				'h1-font-weight' 				=> 600,
				'h1-font-line-height'			=> 1.2,
				'h1-font-letter-spacing'		=> 0,
				'h1-font-italic'				=> false,
				'h1-font-uppercase'				=> false,
				'h2-font-family' 				=> 'Playfair+Display',
				'h2-font-weight' 				=> 600,
				'h2-font-size'					=> 28,
				'h2-font-line-height'			=> 1.2,
				'h2-font-letter-spacing'		=> 0,
				'h2-font-italic'				=> false,
				'h2-font-uppercase'				=> false,
				'h3-font-family' 				=> 'Playfair+Display',
				'h3-font-size'					=> 20,
				'h3-font-weight' 				=> 600,
				'h3-font-line-height'			=> 1.2,
				'h3-font-letter-spacing'		=> 0,
				'h3-font-italic'				=> false,
				'h3-font-uppercase'				=> false,
				'h4-font-family' 				=> 'Playfair+Display',
				'h4-font-size'					=> 17,
				'h4-font-weight' 				=> 600,
				'h4-font-line-height'			=> 1.2,
				'h4-font-letter-spacing'		=> 0,
				'h4-font-italic'				=> false,
				'h4-font-uppercase'				=> false,
				'h5-font-family' 				=> 'Playfair+Display',
				'h5-font-size'					=> 16,
				'h5-font-weight' 				=> 400,
				'h5-font-line-height'			=> 1.2,
				'h5-font-letter-spacing'		=> 0,
				'h5-font-italic'				=> false,
				'h5-font-uppercase'				=> false,
				'h6-font-family' 				=> 'Playfair+Display',
				'h6-font-size'					=> 15,
				'h6-font-weight' 				=> 400,
				'h6-font-line-height'			=> 1.2,
				'h6-font-letter-spacing'		=> 0,
				'h6-font-italic'				=> false,
				'h6-font-uppercase'				=> false

				);

			// Socials & Copyright options
			$socialcopy = array(
				'general-social-label'	=> false,
				'general-social-url1' 	=> 'facebook',
				'general-social-icon1'	=> 'facebook',
				'general-social-url2' 	=> 'instagram',
				'general-social-icon2'	=> 'instagram',
				'general-social-url3' 	=> 'twitter',
				'general-social-icon3'	=> 'twitter',
				'general-social-url4' 	=> 'google-plus',
				'general-social-icon4'	=> 'google-plus',
				'general-social-url5' 	=> 'pinterest',
				'general-social-icon5'	=> 'pinterest',
				'general-social-url6' 	=> '',
				'general-social-icon6'	=> '',
				'general-social-url7' 	=> '',
				'general-social-icon7'	=> '',
				'general-social-url8' 	=> '',
				'general-social-icon8'	=> '',
				'general-social-url9' 	=> '',
				'general-social-icon9'	=> '',
				'general-social-url10' 	=> '',
				'general-social-icon10'	=> '',
				'general-social-active-icon' => 3,
				'general-social-align'	=> 'left',
				'general-copy-label'	=> true,
				'general-copy-text'		=> '2018 &copy; Handcrafted with love<i class="fa fa-heart"></i>by <a href="#">TheSpan</a>',
				'general-copy-align'	=> 'left',
				
				'social-spac-width'			=> 40,
				'social-spac-height'		=> 40,
				'social-spac-margin-right'	=> 20,

				'social-styl-bg-color'			=> '#000',
				'social-styl-font-color'		=> '#fff',
				'social-styl-bg-hover-color'	=> '#fff',
				'social-styl-font-hover-color'	=> '#000',
				'social-styl-child-radius-label' => true,
				'social-styl-child-radius'		=> 10,
				'copy-styl-font-color'			=> '#9a9a9a',
				'copy-styl-link-color'			=> '#fff',

				'social-font-size'			=> 20,
				'social-font-line-height'	=> 40,
				'copy-font-size'			=> 16
			);

			update_option( 'neal_body', $body ); // Body
			update_option( 'neal_header', $header ); // Header
			update_option( 'neal_logo', $logo ); // Logo & Tagline
			update_option( 'neal_menu', $menu ); // Menu
			update_option( 'neal_footer', $footer ); // Footer
			update_option( 'neal_sidebar', $sidebar ); // Sidebar
			update_option( 'neal_product', $product ); // Product
			update_option( 'neal_blog', $blog ); // Blog page
			update_option( 'neal_blog_single', $blog_single ); // Blog single
			update_option( 'neal_shop', $shop ); // Shop page
			update_option( 'neal_shop_single', $shop_single ); // Shop single
			update_option( 'neal_homepage', $homepage ); // Homepage
			update_option( 'neal_comments', $comments ); // Comments
			update_option( 'neal_inputs', $inputs ); // Inputs
			update_option( 'neal_contact', $contact ); // Contact page
			update_option( 'neal_pagination', $pagination ); // Pagination
			update_option( 'neal_typography', $typography ); // Typography
			update_option( 'neal_socialcopy', $socialcopy ); // Social & Copyright

		    update_option( 'neal_activated', true ); // Theme activated
		}

		/**
		 * The Cutomizer reset values
		 *
		 * @since  1.0.0
		 */
		public function ajax_customizer_reset() {

			if ( ! check_ajax_referer( 'neal-customizer-reset', 'nonce', false ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
			}

			if ( ! is_customize_preview() ) {
				wp_send_json_error( esc_html__( 'Not Preview', 'neal' ) );
			}

			$this->setup_options();

			wp_send_json_success();
		}
	}

endif;

return new Neal_Customizer_Defaults();