<?php

// Dynamic Css

function neal_dynamic_style() {
	// Get Theme Customizer options
	$body 			= get_option( 'neal_body' );
	$header 		= get_option( 'neal_header' );
	$logo			= get_option( 'neal_logo' );
	$menu 			= get_option( 'neal_menu' );
	$footer 		= get_option( 'neal_footer' );
	$sidebar 		= get_option( 'neal_sidebar' );
	$product 		= get_option( 'neal_product' );
	$blog 			= get_option( 'neal_blog' );
	$blog_single 	= get_option( 'neal_blog_single' );
	$shop 			= get_option( 'neal_shop' );
	$shop_single 	= get_option( 'neal_shop_single' );
	$homepage 		= get_option( 'neal_homepage' );
	$comments 		= get_option( 'neal_comments' );
	$inputs			= get_option( 'neal_inputs' );
	$contact 		= get_option( 'neal_contact' );
	$pagination 	= get_option( 'neal_pagination' );
	$typography 	= get_option( 'neal_typography' );
	$socialcopy 	= get_option( 'neal_socialcopy' );

	/* General */
	
	$css = 'a {
				color: '.$body['general-styl-link-color'].';
			}';

	$css .= 'a:hover {
				color: '.$body['general-styl-link-hover-color'].';
			}';


	$css .= '::-moz-selection { /* Code for Firefox */
    			background: '.$body['general-styl-selection-bg-color'].';
			}

			::selection {
    			background: '.$body['general-styl-selection-bg-color'].';
			}';

	$css .= '::-moz-selection { /* Code for Firefox */
    			
			}
			::selection {
    			color: '.$body['general-styl-selection-font-color'].';
			}';


	// mobile nav button
	$css .= '.mobile-nav-btn span {
				background-color: '.$body['general-styl-font-color'].';
	        }';

	$css .= '.footer-nav ul.footer-menu li a:hover {
				color: '.$body['general-styl-link-hover-color'].'; 
			}';

	// blog single layouts
	if ( neal_get_option( 'blog_single_general-post-layout' ) == 2 ||
	     neal_get_option( 'blog_single_general-post-layout' ) == 3 ) {
		$css .= '.post-navigation {
					padding-right: 0;
					padding-left: 0;
		        }';
	}
			

/*
***************************************************************
* #Body
***************************************************************
*/

	/* ----------------- General Options ----------------- */
		
		// main layout max-width
		$css .= '.container {
					max-width: '.$body['layout-max-width'].'px;
				}';

		// main layout width
		if ( $body['layout-width'] == 'boxed' ) {
			$css .= '.full-site-content {
						max-width: '.$body['layout-max-width'].'px;
						margin: auto;
					}';

			// sticky header
			$css .= '.site-header .header-area.sticky {
						width: '.$body['layout-max-width'].'px;
						margin: auto;
						left: 0;
						right: 0;
			        }';

			$css .= '@media (max-width: '.($body['layout-max-width'] + 18).'px) {
				     	.site-header .header-area.sticky {
				     		width: 100%;
				     	}
				    }';
		}

		// main layout align
		if ( $body['layout-align'] !== 'center' ) {
			$css .= '.container {
						float: '.$body['layout-align'].';
					}';
		} else {
			$css .= '.container {
						margin: auto;
					}';
		}

	/* ----------------- Styling Options ----------------- */

		// background color
		$css .= 'body {
					background-color: '.$body['general-styl-bg-color'].';
				}';

		// background color 2
		if ( $body['layout-width'] == 'boxed' ) {
			$css .= '.full-site-content {
						background-color: '.$body['general-styl-bg-color-2'].';
					}';
		}

		// font color
		$css .= 'body {
					color: '.$body['general-styl-font-color'].';
				}';

		// background image
		if ( $body['general-styl-child-bg-img-label'] ) {
			$css .= 'body {
						background-image: url(" '.$body['general-styl-child-bg-img'].' ");
						background-size: '.$body['general-styl-child-bg-img-size'].';
						background-attachment: '.$body['general-styl-child-bg-img-attchmnt'].';
					}';
		}


	/* ----------------- Font Options ----------------- */

		// general font
		$css .= neal_css_fonts( 'body',
	 			$body,
	 			'general-font'
	 			);

/*
***************************************************************
* #Header
***************************************************************
*/
	/* ----------------- Spacing Options ----------------- */

		// general header height
		$css .= '.site-header, 
		         .site-header .header-inner,
		         .mobile-nav .mobile-nav-header {
		         	height: '.$header['general-header-spac-height'].'px;	
		         }';

		// sitcky header height
		$css .= '.site-header .header-area.sticky .header-inner,
		         .sticky-header .mobile-nav .mobile-nav-header {
		         	height: '.$header['sticky-header-spac-height'].'px;	
		         }';

		// mobile nav
		$css .= '.mobile-nav .menu-scroll-container {
					top: '.$header['general-header-spac-height'].'px;
				 }

		         .sticky-header .mobile-nav .menu-scroll-container {
					top: '.$header['sticky-header-spac-height'].'px;
				}';

		// page-header padding
		$css .= neal_css_spacing( '.page-header', 
				'padding', 
				$header,
				'page-header-spac'
				);

	/* ----------------- Styling Options ----------------- */

		// general header background-color
		$css .= '.site-header {
					background-color: '.$header['general-header-styl-bg-color'].';
				}';

		// general header background-image
		if ( $header['general-header-styl-child-bg-img-label'] ) {
			$css .= '.site-header .header-area {
						background-image: url(" '.$header['general-header-styl-child-bg-img'].' ");
						background-size: '.$header['general-header-styl-child-bg-img-size'].';
						background-attachment: '.$header['general-header-styl-child-bg-img-attchmnt'].';
					}';
		}

		// sticky header background-color
		$css .= '.site-header .header-area.sticky {
					background-color: '.$header['sticky-header-styl-bg-color'].';
				}';

		// sticky header link color
		$css .= '.site-header .header-area.sticky .nav-menu > li > a,
				 .header-area.sticky .header-icon .search-btn,
				 .header-area.sticky .header-icon .shop-cart-btn,
				 .header-area.sticky .header-icon .social-icons a {
					color: '.$header['sticky-header-styl-link-color'].';
				}';

		// sticky header link bottom line color
		$css .= '.site-header .header-area.sticky .nav-menu > li > a:before {
					background-color: '.$header['sticky-header-styl-link-color'].';
				}';

		// sticky header link hover color
		$css .= '.site-header .header-area.sticky .nav-menu > li:hover > a {
					color: '.$header['sticky-header-styl-link-hover-color'].';
				}';

		// sticky header link bottom line hover color
		$css .= '.site-header .header-area.sticky .nav-menu > li:hover > a:before {
					background-color: '.$header['sticky-header-styl-link-hover-color'].';
				}';

		// page-header bg-color color
		$css .= '.page-header {
					background-color: '.$header['page-header-styl-bg-color'].'
				}';

		// page-header title color
		$css .= '.page-header .page-title {
					color: '.$header['page-header-styl-title-color'].'
				}';

		// page-header title span color
		$css .= '.page-header .page-title span:first-child {
					color: '.$header['page-header-styl-font-color'].'
				}';

		// page-header font color
		$css .= '.page-header {
					color: '.$header['page-header-styl-font-color'].'
				}';

		// page-header link color
		$css .= '.page-header a {
					color: '.$header['page-header-styl-link-color'].'
				}';

		// page-header link hover color
		$css .= '.page-header a:hover {
					color: '.$header['page-header-styl-link-hover-color'].'
				}';




/*
***************************************************************
* #Logo & Tagline
***************************************************************
*/
	
	/* ----------------- General Options ----------------- */

		// tagline align
		if ( $logo['tagline-label'] ) {
			$css .= '.site-branding .site-description {
						text-align: '.$logo['tagline-align'].'
					}';
		}

	/* ----------------- Styling Options ----------------- */

		// logo text font color
		$css .= '.site-branding .site-title a {
					color: '.$logo['logo-text-styl-font-color'].';
				}';

		// logo text font color on sticky header
		$css .= '.site-header .header-area.sticky .site-branding .site-title a {
					color: '.$logo['logo-text-styl-font-color-2'].';

				}';

		// tagline font color
		$css .= '.site-branding .site-description {
					color: '.$logo['tagline-styl-font-color'].';
				}';

		// tagline font color on sticky header
		$css .= '.site-header .header-area.sticky .site-branding .site-description {
					color: '.$logo['tagline-styl-font-color-2'].';
				}';

	/* ----------------- Font Options ----------------- */

		// logo text
		$css .= neal_css_fonts( '.site-branding .site-title',
	 			$logo,
	 			'logo-text-font'
	 			);

		if ( $logo['logo-text-font-uppercase'] ) {
			$css .= '.site-branding .site-title {
						text-transform: uppercase;
					}';
		}

		// tagline
		$css .= neal_css_fonts( '.site-branding .site-description',
	 			$logo,
	 			'tagline-font'
	 			);


/*
***************************************************************
* #Menus
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// left menu items align
		$css .= '.left-navigation {
					text-align: '.$menu['left-menu-items-align'].';
				}';

		// right menu items align
		$css .= '.right-navigation {
					text-align: '.$menu['right-menu-items-align'].';
				}';

		// menu items align
		$css .= '.main-navigation {
					text-align: '.$menu['menu-items-align'].';
				}';

		if ( $menu['left-menu-items-align'] == 'center' || $menu['right-menu-items-align'] == 'center' || $menu['menu-items-align'] == 'center' ) {
			// submenu-items text-align
			$css .= '.nav-menu li > ul.sub-menu {
						text-align: left;
					}';
		}

		// submenu-items align
		$css .= '.nav-menu li > ul.sub-menu {
					'.$menu['submenu-items-align'].': 0;
				}';

		// hamburger menu
		if ( ! $menu['ham-menu-label'] ) {
			$css .= '.header-icon .mobile-nav-btn {
						display: none;
					}';
		}

		// footer menu items align
		$css .= '.footer-nav ul.footer-menu {
						text-align: '.$menu['footer-menu-items-align'].';
					}';


	/* ----------------- Spacing Options ----------------- */

		// menu-items padding
		$css .= neal_css_spacing_2( '.nav-menu > li > a', 
				'padding', 
				$menu,
				'menu-items-spac'
				);

		// menu-items margin-right
		$css .= '.nav-menu > li > a {
					margin-right: '.$menu['menu-items-spac-margin-right'].'px;
				}';

		// submenu-items width
		$css .= '.nav-menu li > ul.sub-menu {
					width: '.$menu['submenu-items-spac-width'].'px;
				}';

		// submenu-items padding
		$css .= neal_css_spacing_2( '.nav-menu li ul.sub-menu li a', 
				'padding', 
				$menu,
				'submenu-items-spac'
				);

		// Distance From parent sub-menu
		$css .= '.nav-menu li > ul.sub-menu,
				 .mega-content, 
				 .is-mega-menu .mega-dropdown-submenu {
					top: '.$menu['submenu-items-spac-margin-top'].'px;
				}';

		// footer menu items margin
		$css .= '.footer-nav ul.footer-menu li a {
					margin-right: '.$menu['footer-menu-items-spac-margin-right'].'px;
				}';

	/* ----------------- Styling Options ----------------- */

		// menu-items link color
		$css .= '.nav-menu > li > a {
					color: '.$menu['menu-items-styl-link-color'].';
				}';

		// menu-items link hover color
		$css .= '.nav-menu > li:not(.active):hover > a {
					color: '.$menu['menu-items-styl-link-hover-color'].';
				}';

		// menu-items link active color
		$css .= '.nav-menu > li.active > a {
					color: '.$menu['menu-items-styl-link-active-color'].';
				}';


		// menu-items link bottom line color
		$css .= '.nav-menu > li > a:before {
					background-color: '.$menu['menu-items-styl-link-color'].';
				}';

		// menu-items link bottom line hover color
		$css .= '.nav-menu > li:not(.active):hover > a:before {
					background-color: '.$menu['menu-items-styl-link-hover-color'].';
				}';

		// menu-items link bottom line active color
		$css .= '.nav-menu > li.active > a:before {
					background-color: '.$menu['menu-items-styl-link-active-color'].';
				}';

		// menu-items icons color
		$css .= '.header-icon .search-btn,
				 .header-icon .shop-cart-btn {
					color: '.$menu['menu-items-styl-icons-color'].';
				}';

		// menu-items icon background-color
		$css .= '.header-icon .hamburger .hamburger-inner,
				 .header-icon .hamburger .hamburger-inner:after, 
				 .header-icon .hamburger .hamburger-inner:before {
				 	background-color: '.$menu['menu-items-styl-icons-color'].';
				 }';

		// submenu-items background-color
		$css .= '.nav-menu li > ul.sub-menu,
				 .mega-content, 
				 .is-mega-menu .mega-dropdown-submenu {
					background-color: '.$menu['submenu-items-styl-bg-color'].';
				}';

		// submenu-items link color
		$css .= '.nav-menu li ul.sub-menu li a {
					color: '.$menu['submenu-items-styl-link-color'].';
				}';

		// submenu-items link hover color
		$css .= '.nav-menu li ul.sub-menu li:hover > a {
					color: '.$menu['submenu-items-styl-link-hover-color'].';
				}';

		// submenu-items shadow
		if ( $menu['submenu-items-styl-shadow'] ) {
			$css .= '.nav-menu li > ul.sub-menu {
					    -webkit-box-shadow: 0 6px 11px 0 rgba(0, 0, 0, 0.06);
					    -moz-box-shadow: 0 6px 11px 0 rgba(0, 0, 0, 0.06);
					    box-shadow: 0 6px 11px 0 rgba(0, 0, 0, 0.06);
					}';
		}

		// submenu border
		$css .= neal_css_border_4( '.nav-menu li ul.sub-menu', 
			$menu,
			'submenu-items-styl-child'
			);

		// footer menu background-color
		$css .= '.footer-nav {
					background-color: '.$menu['footer-menu-styl-bg-color'].';
				}';

		// footer menu items color
		$css .= '.footer-nav ul.footer-menu li a {
					color: '.$menu['footer-menu-items-styl-link-color'].';
				}';

	/* ----------------- Font Options ----------------- */

		// menu-items
		$css .= neal_css_fonts( '.nav-menu > li > a',
	 			$menu,
	 			'menu-items-font'
	 			);

		// menu-items icons size
		$css .= '.site-header .header-area .icon a .icon {
					font-size: '.$menu['menu-items-font-icon-size'].'px;
				}';

		// menu-items italic
		if ( $menu['menu-items-font-italic'] ) {
			$css .= '.nav-menu > li > a {
						font-style:italic;
					}';
		}

		// menu-items uppercase
		if ( $menu['menu-items-font-uppercase'] ) {
			$css .= '.nav-menu > li > a {
						text-transform: uppercase;
					}';
		}

		// submenu-items
		$css .= neal_css_fonts( '.nav-menu li ul.sub-menu li a',
	 			$menu,
	 			'submenu-items-font'
	 			);

		// submenu-items italic
		if ( $menu['submenu-items-font-italic'] ) {
			$css .= '.nav-menu li ul.sub-menu li a {
						font-style:italic;
					}';
		}

		// submenu-items uppercase
		if ( $menu['submenu-items-font-uppercase'] ) {
			$css .= '.nav-menu li ul.sub-menu li a {
						text-transform: uppercase;
					}';
		}

		// mobile menu
		$css .= neal_css_fonts( '.mobile-nav .menu > li a',
	 			$menu,
	 			'mobile-menu-font'
	 			);

		// mobile menu italic
		if ( $menu['mobile-menu-font-italic'] ) {
			$css .= '.mobile-nav .menu> li a {
						font-style:italic;
					}';
		}

		// mobile menu uppercase
		if ( $menu['mobile-menu-font-uppercase'] ) {
			$css .= '.mobile-nav .menu > li a {
						text-transform: uppercase;
					}';
		}

		// footer menu
		$css .= neal_css_fonts( '.footer-nav ul.footer-menu li a',
	 			$menu,
	 			'footer-menu-items-font'
	 			);

		// footer menu italic
		if ( $menu['footer-menu-items-font-italic'] ) {
			$css .= '.footer-nav ul.footer-menu li a {
						font-style:italic;
					}';
		}

		// footer menu uppercase
		if ( $menu['footer-menu-items-font-uppercase'] ) {
			$css .= '.footer-nav ul.footer-menu li a {
						text-transform: uppercase;
					}';
		}
		


/*
***************************************************************
* #Footer
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// widget title align
		$css .= '.site-footer .widget-area .widget .widget-title {
					text-align: '.$footer['wtitle-align'].'
				}';

		// instagram layout
		if ( $footer['instagram-layout'] == 'boxed' ) {
			$css .= '.instagram-footer {
				    	padding-right: 30px;
				    	padding-left: 30px;
				    	margin-top: -100px;
					}';

			$css .= '.site-footer:not(.footer-3) {
						margin-top: 160px;
					}';
					
			if ( $blog_single['post-navigation-label'] ) {
				$css .= '.single .site-footer:not(.footer-3) {
							margin-top: 100px;
						}';
			}

		} else {
			if ( $blog_single['post-navigation-label'] ) {
				$css .= '.single .site-footer {
							margin-top: 0;
						}';
			}

			// title
			if ( ! $footer['instagram-title-pos'] ) {
				$css .= '.instagram-footer .title {
							margin-top: 30px;
						}';
			}

			$css .= '.instagram-footer .title a,
					 .instagram-footer .title span {
						color: '.$footer['general-styl-font-color'].';
					}';
		}

		// fixed footer
		if ( $footer['general-footer-fixed'] ) {

    		$css .= '.site-header, .site-content {
    					position: relative;
    					z-index: 100;
    				}';

    		$css .= '.site-header {
						z-index: 110;
    				}';

			$css .= '.site-content {
						padding-bottom: 60px !important;
						background: '.$body['general-styl-bg-color'].';
					}';

			$css .= '.site-footer {
						position: fixed;
					    bottom: 0;
					    left: 0;
					    z-index: 99;
					    display: inline-block;
					    width: 100%;
					    vertical-align: middle;
					    box-sizing: border-box;
					}';

			// boxed layout
			if ( neal_get_option( 'body_layout-width') == 'boxed' ) {
				$css .= '.site-footer {
							width: '.$body['layout-max-width'].'px;
							margin: auto;
							right: 0;
				        }';
			}
		}

	/* ----------------- Spacing Options ----------------- */

		// general padding
		$css .= neal_css_spacing( '.site-footer', 
				'padding', 
				$footer,
				'general-spac'
				);

		// widget-title margin-bottom
		$css .= '.site-footer .widget-area .widget .widget-title {
					margin-bottom: '.$footer['wtitle-spac-margin-bottom'].'px;
				}';

		// instagram item padding-right-left
		$css .= '.instagram-footer .insta {
					padding-right: '.$footer['instagram-spac-item-padding-right-left'].'px;
					padding-left: '.$footer['instagram-spac-item-padding-right-left'].'px;
				}';

	/* ----------------- Styling Options ----------------- */

		// general background-color
		$css .= '.site-footer {
					background-color: '.$footer['general-styl-bg-color'].';
				}';

		// general font color
		$css .= '.instagram-footer .owl-nav,
		 		 .instagram-footer .title,
		 		 .instagram-footer .title a,
				 .site-footer .widget ul li,
				 .site-footer .select2-container--default .select2-selection--single .select2-selection__rendered,
				 .site-footer .widget-title .rsswidget,
				 .site-footer .search-form .search-field,
				 .site-footer .widget .tagcloud a,
				 .site-footer #wp-calendar caption,
				 .site-footer #wp-calendar thead th,
				 .site-footer #wp-calendar a {
					color: '.$footer['general-styl-font-color'].';
				}';

		// widgets font color
		$css .= '.site-footer .widget-area .widget .widget-title {
					color: '.$footer['widgets-styl-font-color'].';
				}';

		// widgets link color
		$css .= '.site-footer .widget-area .widget ul li a {
					color: '.$footer['widgets-styl-link-color'].';
				}';

		// widgets link hover color
		$css .= '.site-footer .widget-area .widget ul li a:hover {
					color: '.$footer['widgets-styl-link-hover-color'].';
				}';

/*
***************************************************************
* #Sidebar
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// widget title
		$css .= '.site-content .widget .widget-title {
					text-align: '.$sidebar['wtitle-align'].';
				}';

	/* ----------------- Spacing Options ----------------- */

		// general padding
		$css .= neal_css_spacing_2( '.site-content .widget-area .widget', 
				'padding', 
				$sidebar,
				'general-spac'
				);

		// general margin-bottom
		$css .= '.site-content .widget-area .widget {
					margin-bottom: '.$sidebar['general-spac-margin-bottom'].'px;
				}';

		// widget title margin-bottom
		$css .= '.site-content .widget-area .widget .widget-title {
					margin-bottom: '.$sidebar['wtitle-spac-margin-bottom'].'px;
				}';


	/* ----------------- Styling Options ----------------- */

		// general background-color
		$css .= '.site-content .widget-area .widget {
					background-color: '.$sidebar['general-styl-bg-color'].';
				}';

		// general border
		$css .= neal_css_border_4( '.site-content .widget-area .widget', 
			$sidebar,
			'general-styl-child'
			);

		// widget title font color
		$css .= '.site-content .widget-area .widget .widget-title {
					color: '.$sidebar['wtitle-styl-font-color'].';
				}';

		// about me widget border-radius
		$css .= '.widget_aboutme .about-me img {
					border-radius: '.$sidebar['about-me-styl-radius'].'%;
				}';

	/* ----------------- Font Options ----------------- */


		// widget-title
		$css .= neal_css_fonts( '.site-content .widget-area .widget .widget-title',
	 			$sidebar,
	 			'wtitle-font'
	 			);

		// widget title italic
		if ( $sidebar['wtitle-font-italic'] ) {
			$css .= '.widget-area .widget .widget-title {
						font-style: italic;
				}';
		}

		// widget title uppercase
		if ( $sidebar['wtitle-font-uppercase'] ) {
			$css .= '.site-content .widget-area .widget .widget-title {
						text-transform: uppercase;
				}';
		}

		// categories image
		$css .= neal_css_fonts( '.widget_categoriesimage .category-title h3',
	 			$sidebar,
	 			'categories-image-font'
	 			);

		// categories image uppercase
		if ( $sidebar['categories-image-font-uppercase'] ) {
			$css .= '.widget_categoriesimage .category-title h3 {
						text-transform: uppercase;
				}';
		}

/*
***************************************************************
* #Product
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// star rating align
		$css .= '.products .product-item .product-meta .ratings {
					text-align: '.$product['star-rating-align'].';
				}';

		// title align
		$css .= '.products .product-item .product-meta h4 {
					text-align: '.$product['title-align'].';
				}';

		// prices align
		$css .= '.products .product-item .price-info {
					text-align: '.$product['prices-align'].';
				}';
	

	/* ----------------- Spacing Options ----------------- */

		// star-rating margin-bottom
		$css .= '.products .product-item .product-meta .ratings {
					margin-bottom: '.$product['star-rating-spac-margin-bottom'].'px;
				}';

		// star-rating-star margin-right
		$css .= '.products .product-item .product-meta .star-rating {
					letter-spacing: '.$product['star-rating-spac-margin-right'].'px;
				}';


		// title margin-bottom
		$css .= '.products .product-item .product-meta h4 {
					margin-bottom: '.$product['title-spac-margin-bottom'].'px;
				}';

		// prices margin-bottom
		$css .= '.products .product-item .price-info {
					margin-bottom: '.$product['prices-spac-margin-bottom'].'px;
				}';

	/* ----------------- Styling Options ----------------- */

		// star-rating-star color
		$css .= '.products .product-item .product-meta .star-rating span:before {
					color: '.$product['star-rating-styl-star-color'].';
				}';

		// star-rating-star color 2
		$css .= '.products .product-item .product-meta .star-rating {
					color: '.$product['star-rating-styl-star-color-2'].';
				}';

		// sale font color
		$css .= '.products .product-item .on-sale, 
				 .woopost-single-images .on-sale {
					color: '.$product['sale-styl-font-color'].';
				}';

		// add-to-cart font color
		$css .= '.woocommerce:not(.shop-view-list) .products .product-item .product-meta .button,
		         .woocommerce:not(.shop-view-list) .products .product-item .product-meta .added_to_cart {
					color: '.$product['add-to-cart-styl-font-color'].';
				}';

		// add-to-cart font hover color
		$css .= '.woocommerce:not(.shop-view-list) .products .product-item .product-meta .button:hover,
		         .woocommerce:not(.shop-view-list) .products .product-item .product-meta .added_to_cart:hover  {
					color: '.$product['add-to-cart-styl-font-hover-color'].';
				}';

		// title color
		$css .= '.products .product-item .product-meta h4 a,
				 .woocommerce.shop-view-list .products .product-item h4 a {
					color: '.$product['title-styl-font-color'].'
				}';

		// title hover color
		$css .= '.products .product-item .product-meta h4 a:hover,
		         .woocommerce.shop-view-list .products .product-item h4 a:hover {
					color: '.$product['title-styl-font-hover-color'].'
				}';

		// special-price color
		$css .= '.products .product-item .price-info .price,
				 .products .product-item .price-info .price ins {
					color: '.$product['special-price-styl-font-color'].'
				}';

		// old-price color
		$css .= '.products .product-item .price-info .price del {
					color: '.$product['old-price-styl-font-color'].'
				}';


	/* ----------------- Font Options ----------------- */

		// star-rating
		$css .= neal_css_fonts( '.products .product-item .product-meta .star-rating',
	 			$product,
	 			'star-rating-font'
	 			);

		// sale
		$css .= neal_css_fonts( '.products .product-item .on-sale',
	 			$product,
	 			'sale-font'
	 			);

		// title
		$css .= neal_css_fonts( '.products .product-item .product-meta h4',
	 			$product,
	 			'title-font'
	 			);

		// title italic
		if ( $product['title-font-italic'] ) {
			$css .= '.products .product-item .product-meta h4 {
						font-style: italic;
					}';
		}

		// title uppercase
		if ( $product['title-font-uppercase'] ) {
			$css .= '.products .product-item .product-meta h4 {
						text-transform: uppercase;
					}';
		}

		// special-price
		$css .= neal_css_fonts( 
				'.products .product-item .price-info .amount',
	 			$product,
	 			'special-price-font'
	 			);

		// old-price
		$css .= neal_css_fonts( '.products .product-item .price-info del .amount',
	 			$product,
	 			'old-price-font'
	 			);

		// old-price line-through
		if ( $product['old-price-font-line-through'] ) {
			$css .= '.products .product-item .price-info .price del {
						text-decoration: line-through;
					}';
		}

/*
***************************************************************
* #Blog page
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// categories align
		$css .= '.blog-posts .entry-header .meta-categories {
					text-align: '.$blog['categories-align'].';
				}';

		// date align
		$css .= '.blog-posts .entry-header .meta-date {
					text-align: '.$blog['date-align'].';
				}';

		// author align
		$css .= '.blog-posts .entry-header .meta-author {
					text-align: '.$blog['author-align'].';
				}';

		// title align
		$css .= '.blog-posts .entry-title {
					text-align: '.$blog['title-align'].';
				}';

		// description align
		$css .= '.blog-posts .entry-content {
					text-align: '.$blog['description-align'].';
				}';

		// read-more align
		$css .= '.blog-posts .read-more-block {
					text-align: '.$blog['read-more-align'].';
				}';

		// post date & categories
		if ( $blog['date-align'] == 'center' && $blog['categories-align'] == 'center' ) { 
			// post date & categories
			$css .= '.post-date-category {
						text-align: center;
					}';
		} elseif ( $blog['date-align'] == 'left' && $blog['categories-align'] == 'left' ) {
			// post date & categories
			$css .= '.post-date-category {
						text-align: left;
					}';
		} elseif ( $blog['date-align'] == 'right' && $blog['categories-align'] == 'right' ) {
			// post date & categories
			$css .= '.post-date-category {
						text-align: right;
					}';
		}


	/* ----------------- Spacing Options ----------------- */

		// media margin-bottom
		$css .= neal_css_spacing( '.blog-posts .entry-media', 
				'margin', 
				$blog,
				'media-spac'
				);

		if ( neal_get_option( 'blog_layout-columns' ) == 'list' ) {
			$css .= '.blog-posts .entry-media {
						margin-bottom: 0;
					}';
		}

		// title margin
		$css .= neal_css_spacing( '.blog-posts .entry-title', 
				'margin', 
				$blog,
				'title-spac'
				);

		// meta-items margin-bottom
		$css .= neal_css_spacing( '.blog-posts .meta-categories .post-category a,
								   .blog-posts .meta-date,
								   .blog-posts .meta-author',
				'margin', 
				$blog,
				'meta-items-spac'
				);

		// description margin-bottom
		$css .= neal_css_spacing( '.blog-posts .entry-content', 
				'margin', 
				$blog,
				'description-spac'
				);


	/* ----------------- Styling Options ----------------- */

		// title font color
		$css .= '.blog-posts .entry-title a {
					color: '.$blog['title-styl-font-color'].';
				}';

		// title font hover color
		$css .= '.blog-posts article .article-inner:hover .entry-title a {
					color: '.$blog['title-styl-font-hover-color'].';
				}';

		// meta font color
		$css .= '.meta-categories,
				 .meta-author,
	             .meta-date {
					color: '.$blog['meta-styl-font-color'].';
				}';

		// meta link color
		$css .= '.meta-categories a,
				 .blog-posts .meta-author a {
					color: '.$blog['meta-styl-link-color'].';
				}';

		// meta link hover color
		$css .= '.meta-categories a:hover,
				 .blog-posts .meta-author a:hover {
					color: '.$blog['meta-styl-link-hover-color'].';
				}';

		// meta icon color
		$css .= '.meta-author i,
				 .meta-date i {
					color: '.$blog['meta-styl-link-hover-color'].';
				}';

		// categories font color
		$css .= '.meta-categories .post-category a {
					color: '.$blog['categories-styl-font-color'].';
				}';

		// categories font hover color
		$css .= '.meta-categories .post-category a:hover {
					color: '.$blog['categories-styl-font-hover-color'].';
				}';

		// categories background color
		$css .= '.meta-categories .post-category a {
					background-color: '.$blog['categories-styl-bg-color'].';
				}';

		// categories background hover color
		$css .= '.meta-categories .post-category a:hover {
					background-color: '.$blog['categories-styl-bg-hover-color'].';
				}';

		// description font color
		$css .= '.blog-posts .entry-content {
					color: '.$blog['desc-styl-font-color'].';
				}';

	/* ----------------- Font Options ----------------- */

		// title
		$css .= neal_css_fonts( '.blog-posts .entry-title',
	 			$blog,
	 			'title-font'
	 			);

		// title italic
		if ( $blog['title-font-italic'] ) {
			$css .= '.blog-posts .entry-title {
						font-style: italic;
					}';
		}

		// title uppercase
		if ( $blog['title-font-uppercase'] ) {
			$css .= '.blog-posts .entry-title {
						text-transform: uppercase;
					}';
		}

		// meta
		$css .= neal_css_fonts( '.meta-date span.post-date, 
									.meta-author span.post-author',
	 			$blog,
	 			'meta-font'
	 			);
		
		// meta italic
		if ( $blog['title-font-italic'] ) {
			$css .= '.meta-date span.post-date, 
					 .meta-author span.post-author {
						font-style: italic;
					}';
		}

		// meta uppercase
		if ( $blog['title-font-uppercase'] ) {
			$css .= '.meta-date span.post-date, 
					 .meta-author span.post-author{
						text-transform: uppercase;
					}';
		}

		// categories
		$css .= neal_css_fonts( '.meta-categories .post-category a',
	 			$blog,
	 			'categories-font'
	 			);

		// categories italic
		if ( $blog['categories-font-italic'] ) {
			$css .= '.meta-categories .post-category a {
						font-style: italic;
					}';
		}

		// categories uppercase
		if ( $blog['categories-font-uppercase'] ) {
			$css .= '.meta-categories .post-category a {
						text-transform: uppercase;
					}';
		}

		// description
		$css .= neal_css_fonts( '.blog-posts .entry-content',
	 			$blog,
	 			'desc-font'
	 			);

		// description italic
		if ( $blog['desc-font-italic'] ) {
			$css .= '.blog-posts .entry-content {
						font-style: italic;
					}';
		}

		// description uppercase
		if ( $blog['desc-font-uppercase'] ) {
			$css .= '.blog-posts .entry-content {
						text-transform: uppercase;
					}';
		}

/*
***************************************************************
* #Blog single
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// title align
		$css .= '.single .entry-title {
					text-align: '.$blog_single['title-align'].';
				}';

		// breadcrumb align
		$css .= '.single .site-main .breadcrumbs {
					text-align: '.$blog_single['bread-align'].';
				}';

		// post meta align
		$css .= '.single .entry-header .post-meta {
					text-align: '.$blog_single['meta-align'].';
				}';

		// rotate caption text
		if ( $blog_single['general-post-rotate-cap-text'] ) {
			$css .= '@media (min-width: 768px) {
				         body:not(.sidebar-right) .wp-caption.alignright .wp-caption-text,
				         body:not(.sidebar-left) .wp-caption.alignleft .wp-caption-text {
				         	margin-top: 0;
						    position: absolute;
						    transform-origin: right bottom;
						    transform: rotate(-90deg) translateX(100%);
						    white-space: nowrap;
						    bottom: 5px;
				         }

				         body:not(.sidebar-right) .wp-caption.alignleft .wp-caption-text,
				         body:not(.sidebar-left) .wp-caption.alignleft .wp-caption-text {
		    				margin-right: 15px;
		    				right: 100%;
		    		   	 }

		    		   	 body:not(.sidebar-right) .wp-caption.alignright .wp-caption-text,
		    		   	 body:not(.sidebar-left) .wp-caption.alignright .wp-caption-text {
		    		   	 	right: -39px;
		    		   	 }
		    		}';
	   	}

	   	// tags
	   	if ( ! $blog_single['sharing-label'] ) {
	   		$css .= '.tags-links {
	   					width: 100%;
	   				}';
	   	}


	/* ----------------- Spacing Options ----------------- */

		// media margin
		$css .= neal_css_spacing( '.single .entry-media,
			                       .single .post-full-media,
			                       .single .post-boxed-media', 
				'margin', 
				$blog_single,
				'media-spac'
				);

		// title margin
		$css .= neal_css_spacing( '.single .entry-title', 
				'margin', 
				$blog_single,
				'title-spac'
				);

		// breadcrumb margin
		$css .= neal_css_spacing( '.single .breadcrumbs', 
				'margin', 
				$blog_single,
				'bread-spac'
				);

		// meta margin
		$css .= neal_css_spacing( '.single .entry-header .post-meta', 
				'margin', 
				$blog_single,
				'meta-spac'
				);

		// caption image margin-left
		$css .= '.wp-caption.alignleft {
					margin-left: '.$blog_single['caption-image-spac-margin-left'].'px;
				}';

		// caption image margin-right
		$css .= '.wp-caption.alignright {
					margin-right: '.$blog_single['caption-image-spac-margin-right'].'px;
				}';


		if ( $blog_single['related-posts-pos'] == 'below-comments' ) {
			$css .= '.related-posts {
						padding-top: 30px;
					}';
		}

	/* ----------------- Font Options ----------------- */

		// title
		$css .= neal_css_fonts( '.single .entry-title',
	 			$blog_single,
	 			'title-font'
	 			);

		// title uppercase
		if ( $blog_single['title-font-uppercase'] ) {
			$css .= '.single.entry-title {
						text-transform: uppercase;
					}';
		}

		// content
		$css .= neal_css_fonts( '.single .entry-content',
	 			$blog_single,
	 			'content-font'
	 			);

		// content dropcap
		$family = str_replace( '+', ' ', $typography['h1-font-family'] );
		$css .= '.entry-content .dropcap {
					font-family: "'.$family.'", Arial, "Helvetica Neue", Helvetica, sans-serif;
				}';


/*
***************************************************************
* #Shop page
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// toolbar align
		$css .= '.tool-bar {
					float: '.$shop['tbar-align'].';
				}';

		// toolbar icon size
		$css .= '.tool-bar .mode-view li a {
					font-size: '.$shop['tbar-icon-size'].'px;
				}';

		// result text align
		$css .= '.container-header {
					text-align: '.$shop['result-t-align'].';
				}';

		// ordering align
		$css .= '.woocommerce form.woocommerce-ordering {
					float: '.$shop['ordering-align'].';
				}';


	/* ----------------- Spacing Options ----------------- */

		// toolbar margin
		$css .= neal_css_spacing( '.tool-bar .mode-view li a', 
				'margin', 
				$shop,
				'tbar-spac'
				);

		// result text margin
		$css .= neal_css_spacing( '.woocommerce-result-count', 
				'margin', 
				$shop,
				'result-t-spac'
				);

		// ordering margin
		$css .= neal_css_spacing( '.woocommerce form.woocommerce-ordering', 
				'margin', 
				$shop,
				'ordering-spac'
				);


	/* ----------------- Styling Options ----------------- */

		// toolbar link color
		$css .= '.tool-bar .mode-view li a {
					color: '.$shop['tbar-styl-link-color'].'
				}';

		// toolbar link active color
		$css .= '.tool-bar .mode-view li a.active {
					color: '.$shop['tbar-styl-link-active-color'].'
				}';

		// toolbar link hover color
		$css .= '.tool-bar .mode-view li a:hover {
					color: '.$shop['tbar-styl-link-hover-color'].'
				}';

		// result text font color
		$css .= '.woocommerce-result-count {
					color: '.$shop['result-t-styl-font-color'].'
				}';

		// ordering color
		$css .= '.woocommerce form.woocommerce-ordering select {
					color: '.$shop['ordering-styl-color'].'
				}';

		// filter-slider color
		$css .= '.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
				 .woocommerce .widget_price_filter .ui-slider .ui-slider-handle {
				 	border-color: '.$shop['filter-sld-styl-color'].';
					background-color: '.$shop['filter-sld-styl-color'].';
				}';

		// filter-slider color 2
		$css .= '.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content {
					background-color: '.$shop['filter-sld-styl-color-2'].';
				}';

	/* ----------------- Font Options ----------------- */

		// result text
		$css .= neal_css_fonts( '.woocommerce-result-count',
	 			$shop,
	 			'result-t-font'
	 			);

		// result text italic
		if ( $shop['result-t-font-italic'] ) {
			$css .= '.woocommerce-result-count {
						font-style: italic;
					}';
		}

		// result text uppercase
		if ( $shop['result-t-font-uppercase'] ) {
			$css .= '.woocommerce-result-count {
						text-transform: uppercase;
					}';
		}

		// ordering
		$css .= neal_css_fonts( '.woocommerce form.woocommerce-ordering select',
	 			$shop,
	 			'ordering-font'
	 			);

		// ordering italic
		if ( $shop['ordering-font-italic'] ) {
			$css .= '.woocommerce form.woocommerce-ordering select {
						font-style: italic;
					}';
		}

		// ordering uppercase
		if ( $shop['ordering-font-uppercase'] ) {
			$css .= '.woocommerce form.woocommerce-ordering select {
						text-transform: uppercase;
					}';
		}

/*
***************************************************************
* #Shop single
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// product images align
		$css .= '.woopost-single-images-wrap {
				 	float: '.$shop_single['images-align'].';
				}';

		if ( $shop_single['images-align'] == 'left' ) {

			$css .= '.woopost-single-summary-wrap {
						float: right;
					}';
		} else {

			$css .= '.woopost-single-summary-wrap {
						float: left;
					}';
		}

		// product thumbnail align
		$css .= '.woocommerce .vertical-carousel {
						float: '.$shop_single['thumbs-align'].';
					}';

		// summary content align
		$css .= '.woopost-single-summary .summary,
				 .woopost-single-summary .entry-summary .entry-title {
					text-align: '.$shop_single['summary-content-align'].';
				}';

		//social sharing align
		$css .= '.single .social-share {
					text-align: '.$shop_single['social-sharing-align'].';
				}';

		// tabs align
		$css .= '.woocommerce-tabs ul.tabs {
					text-align: '.$shop_single['tabs-align'].';
				}';

	/* ----------------- Spacing Options ----------------- */

		// title margin-bottom
		$css .= neal_css_spacing( '.woopost-single-summary .entry-summary .entry-title', 
				'margin', 
				$shop_single,
				'title-spac'
				);

		// product rating margin-bottom
		$css .= neal_css_spacing( '.woopost-single-summary .woocommerce-product-rating', 
				'margin', 
				$shop_single,
				'product-rating-spac'
				);

		// product prices margin-bottom
		$css .= neal_css_spacing( '.woopost-single-summary .price-box', 
				'margin', 
				$shop_single,
				'product-prices-spac'
				);

		// product meta margin-top
		$css .= neal_css_spacing( '.single-product .product .product_meta', 
				'margin', 
				$shop_single,
				'product-meta-spac'
				);

		// tabs tab padding-top-bottom
		$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li a {
					padding-top: '.$shop_single['tabs-tab-spac-padding-top-bottom'].'px;
					padding-bottom: '.$shop_single['tabs-tab-spac-padding-top-bottom'].'px;
				}';

		// tabs tab padding-right-left
		$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li a {
					padding-right: '.$shop_single['tabs-tab-spac-padding-right-left'].'px;
					padding-left: '.$shop_single['tabs-tab-spac-padding-right-left'].'px;
				}';

	/* ----------------- Styling Options ----------------- */

		// title font color
		$css .= '.woopost-single-summary .entry-summary .entry-title {
					color: '.$shop_single['title-styl-font-color'].'
				}';

		// product prices font color
		$css .= '.single-product p.price .price, 
		         .single-product p.price .price ins {
					color: '.$shop_single['product-prices-styl-font-color'].'
				}';

		// product prices font color 2
		$css .= '.single-product p.price .price del {
					color: '.$shop_single['product-prices-styl-font-color-2'].'
				}';

		// tabs tab link color
		$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li a {
					color: '.$shop_single['tabs-tab-styl-link-color'].'
				}';

		// tabs tab link hover color
		$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover {
					color: '.$shop_single['tabs-tab-styl-link-hover-color'].'
				}';

		// tabs tab link active color
		$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li.active a {
					color: '.$shop_single['tabs-tab-styl-link-active-color'].'
				}';

		// tabs tab active border-bottom
		$css .= neal_css_border( '.woocommerce div.product .woocommerce-tabs ul.tabs li.active a',
				$shop_single,
				'tabs-tab-styl-child'
				);


	/* ----------------- Font Options ----------------- */

		// title
		$css .= neal_css_fonts( '.woopost-single-summary .entry-summary .entry-title',
	 			$shop_single,
	 			'title-font'
	 			);

		// title italic
		if ( $shop_single['title-font-italic'] ) {
			$css .= '.woopost-single-summary .entry-summary .entry-title {
						font-style: italic;
					}';
		}

		// title uppercase
		if ( $shop_single['title-font-uppercase'] ) {
			$css .= '.woopost-single-summary .entry-summary .entry-title {
						text-transform: uppercase;
					}';
		}

		// title underline
		if ( $shop_single['title-font-underline'] ) {
			$css .= '.woopost-single-summary .entry-summary .entry-title {
						text-decoration: underline;
					}';
		}

		// product prices
		$css .= neal_css_fonts( '.entry-summary .price',
	 			$shop_single,
	 			'product-prices-font'
	 			);

		// tabs tab
		$css .= neal_css_fonts( '.woocommerce div.product .woocommerce-tabs ul.tabs li a',
	 			$shop_single,
	 			'tabs-tab-font'
	 			);

		// tabs tab uppercase
		if ( $shop_single['tabs-tab-font-uppercase'] ) {
			$css .= '.woocommerce div.product .woocommerce-tabs ul.tabs li a {
						text-transform: uppercase;
					}';
		}


/*
***************************************************************
* #Homepage
***************************************************************
*/
	
	/* ----------------- General Options ----------------- */

		if ( is_page_template( 'template-homepage.php' ) || is_singular('post') ) {
			global $post;
				
			// get post meta data
			$carousel_posts = get_post_meta( $post->ID, 'neal_carousel_posts_checkbox', true );
			$items 			= get_post_meta( $post->ID, 'neal_carousel_posts_items', true );
			$post_items 	= neal_get_post_options_data();
			$post_layout 	= $post_items['post_layouts'];
			$layouts		= '';

			if ( $carousel_posts && $items ) {
				extract( $items );
			}
				
			// carousel layouts
			if ( $layouts == 2 || is_singular('post') && $post_layout == 6 ) {
				$css .= '.site-header {
							background: '.$header['general-header-styl-bg-color-2'].';
						}';

				$css .= '.nav-menu > li > a,
						 .header-icon .social-icons a,
						 .header-icon .search-btn,
						 .header-icon .shop-cart-btn {
							color:#fff !important;
						}';

				$css .= '.site-header .hamburger-inner, 
				         .site-header .hamburger-inner:after, 
				         .site-header .hamburger-inner:before,
				         .nav-menu > li > a:before {
				         	background-color: #fff !important;
				         }';

				$css .= '.header-social-media .socials-wrap {
							top: 480px;
						}';

				// logos
				$css .= '.site-branding .logo .logo-1 {
							display: none;
						}';

				$css .= '.site-branding .logo .logo-2 {
							display: inline-block;
						}';

				// logo text font color
				$css .= '.site-branding .site-title a {
							color: '.$logo['logo-text-styl-font-color-2'].';
						}';

				// tagline font color
				$css .= '.site-branding .site-description {
							color: '.$logo['tagline-styl-font-color-2'].';
						}';

				if ( is_page_template( 'template-homepage.php' ) || has_post_thumbnail() ) {
					$css .= '.site-content:before {
								display: block;
								height: 180px;
								background: '.$header['general-header-styl-bg-color-2'].';
							}';
				}

				$css .= '.post-carousel {
							margin-top: -180px;
						}';

				if ( is_page_template( 'template-homepage.php' ) && $items['content_position'] == 'above' ) {
					$css .= '.post-carousel .carousel-item,
					         .post-carousel .carousel-item h2 a,
					         .post-carousel .carousel-item .meta-date,
					         .post-carousel .carousel-item .meta-author a {
								color: #fff;
							}';

					$css .= '.post-carousel .carousel-item .meta-author a {
								border-bottom-color: #fff;
							}';
				}
			}
		}

		// sidebars on homepage
		$css .= '.site-content .sidebar-area-1 {
					float: '.$homepage['sidebar-1-align'].';
				}';

		$css .= '.site-content .sidebar-area-2 {
					float: '.$homepage['sidebar-2-align'].';
				}';

		$css .= '.site-content .sidebar-area-3 {
					float: '.$homepage['sidebar-3-align'].';
				}';

	/* ----------------- Spacing Options ----------------- */

		// newsletter margin
		$css .= neal_css_spacing( '.homepage-newsletter', 
				'margin', 
				$homepage,
				'newsletter-spac'
				);

	/* ----------------- Styling Options ----------------- */

		// general title first letter color
		$css .= '.section-title .title:before {
					color: '.$homepage['general-styl-tfl-color'].';
				}';

		// posts title hover color
		$css .= '.posts article .article-inner:hover .entry-title a,
				 .related-posts .post-inner:hover h4 a {
					color: '.$body['general-styl-link-hover-color'].';
				}';

		// full-posts background-color
		$css .= '.full-posts {
					background-color: '.$homepage['full-posts-styl-bg-color'].';
				}';

		// full-posts title first letter color
		$css .= '.full-posts .section-title .title:before {
					color: '.$homepage['full-posts-styl-tfl-color'].';
				}';

		// full-posts font-color
		$css .= '.full-posts {
					color: '.$homepage['full-posts-styl-font-color'].';
				}';

		// full-posts post title color
		$css .= '.full-posts .posts .entry-header .entry-title a {
					color: '.$homepage['full-posts-styl-post-title-color'].';
				}';

		// full-posts post title hover color
		$css .= '.full-posts .posts article .article-inner:hover .entry-title a {
					color: '.$homepage['full-posts-styl-post-title-hover-color'].';
				}';

		// full-posts read-more color
		$css .= '.full-posts a.read-more {
					color: '.$homepage['full-posts-styl-read-more-color'].';
				}';

		// full-posts read-more hover color
		$css .= '.full-posts a.read-more:hover {
					color: '.$homepage['full-posts-styl-read-more-hover-color'].';
				}';

		// newsletter background-color
		$css .= '.homepage-newsletter {
					background-color: '.$homepage['newsletter-styl-bg-color'].';
				}';

		// newsletter title color
		$css .= '.homepage-newsletter .widgettitle,
		         .homepage-newsletter input[type="email"],
		         .homepage-newsletter input[type="email"]:focus {
					color: '.$homepage['newsletter-styl-title-color'].';
				}';

		// newsletter description color
		$css .= '.homepage-newsletter #subscribe-text {
					color: '.$homepage['newsletter-styl-desc-color'].';
				}';

	/* ----------------- Font Options ----------------- */

		// general section title
		$css .= neal_css_fonts( '.section-title',
	 			$homepage,
	 			'general-section-title-font'
	 			);

		// general title first letter
		$css .= neal_css_fonts( '.section-title .title:before',
	 			$homepage,
	 			'general-tfl-font'
	 			);

		// carousel posts title
		$css .= neal_css_fonts( '.post-carousel .carousel-item .carousel-item-content h2',
	 			$homepage,
	 			'carousel-posts-title-font'
	 			);

		// full featured post title
		$css .= neal_css_fonts( '.featured-post .post-content h1',
	 			$homepage,
	 			'full-featured-post-title-font'
	 			);

		// masonry posts title
		$css .= neal_css_fonts( '.masonry-posts .post-item-inner .post-item .post-content .content .entry-title',
	 			$homepage,
	 			'masonry-posts-title-font'
	 			);

		// masonry posts second title
		$css .= neal_css_fonts( '.masonry-posts.layout-2 .post-item-inner .second .post-content
								 .content .entry-title, 
								 .masonry-posts.layout-3 .post-item-inner 
								 .second .post-content .content .entry-title,
								 .masonry-posts.layout-6 .post-item-inner .third .post-content .content .entry-title,
								 .masonry-posts.layout-7 .post-item-inner .second .post-content .content .entry-title',
	 			$homepage,
	 			'masonry-posts-second-title-font'
	 			);

		// 2 grid posts title
		$css .= neal_css_fonts( '.posts .col-lg-6 .entry-header .entry-title',
	 			$homepage,
	 			'2-grid-posts-title-font'
	 			);

		// list posts title
		$css .= neal_css_fonts( '.list-posts .posts .entry-header .entry-title',
	 			$homepage,
	 			'list-posts-title-font'
	 			);

		// slider posts title
		$css .= neal_css_fonts( '.slider-posts .post-item .entry-header .entry-title',
	 			$homepage,
	 			'slider-posts-title-font'
	 			);

		// 3 grid posts title
		$css .= neal_css_fonts( '.posts .col-lg-4 .entry-header .entry-title',
	 			$homepage,
	 			'3-grid-posts-title-font'
	 			);

		// full posts title
		$css .= neal_css_fonts( '.full-posts .posts .entry-header .entry-title',
	 			$homepage,
	 			'full-posts-title-font'
	 			);



/*
***************************************************************
* #Comments
***************************************************************
*/

	/* ----------------- Spacing Options ----------------- */

		// author-image margin
		$css .= '.comments-area .comment-author-img {
					margin-right: '.$comments['author-img-spac-margin-right'].'px;
				}';

		// comment content vetical gutter
		$css .= '.comments-area .comment-content {
					margin-bottom: '.$comments['comment-content-spac-vertical-gutter'].'px;
				}';


	/* ----------------- Styling Options ----------------- */
		
		// author-image corner radius
		$css .= '.comments-area .comment-author-img img {
					border-radius: '.$comments['author-img-styl-radius'].'%;
				}';

		// comment-content background-color
		$css .= '.post-comments {
					background-color: '.$comments['comment-content-styl-bg-color'].';
				}';

		// comment-content font color
		$css .= '.comments-area .comment-content p {
					color: '.$comments['comment-content-styl-font-color'].';
				}';


/*
***************************************************************
* #Inputs
***************************************************************
*/
	
	// selectors
	$general_input = '.input-text, input[type=text], input[type=email], input[type=url], input[type=password], input[type=search], textarea';
	$general_input_focus = '.input-text:focus, input[type=text]:focus, input[type=email]:focus, input[type=url]:focus, input[type=password]:focus, input[type=search]:focus, textarea:focus';

	$submit_button = '.button, button, input[type="button"], input[type="reset"], input[type="submit"]';

	$submit_button_hover = '.button:not( :disabled ):hover, input[type="submit"]:not( :disabled ):hover';


	/* ----------------- Spacing Options ----------------- */

		// input padding
		$css .= neal_css_spacing_2( $general_input, 
				'padding', 
				$inputs,
				'general-spac'
				);

		// submit-button padding
		$css .= neal_css_spacing_2( $submit_button, 
				'padding', 
				$inputs,
				'submit-button-spac'
				);

	/* ----------------- Styling Options ----------------- */

		// input font color
		$css .= $general_input .' {
					color: '.$inputs['general-styl-font-color'].';
				}';

		// input font focus color
		$css .= $general_input_focus .' {
					color: '.$inputs['general-styl-font-focus-color'].';
				}';

		// input border focus color
		$css .= $general_input_focus .' {
					border-color: '.$inputs['general-styl-border-focus-color'].';
				}';

		// input border
		$css .= neal_css_border( $general_input,
				$inputs,
				'general-styl-child'
				);

		// input corner radius
		if ( $inputs['general-styl-child-radius-label'] ) {
			$css .= $general_input .'{
						border-radius: '.$inputs['general-styl-child-radius'].'px;
					}';
		}

		// submit-button background-color
		$css .= $submit_button .' {
					background-color: '.$inputs['submit-button-styl-bg-color'].';
				}';

		// submit-button font color
		$css .= $submit_button .' {
					color: '.$inputs['submit-button-styl-font-color'].';
				}';

		// submit-button hover background-color
		$css .= $submit_button_hover .' {
					background-color: '.$inputs['submit-button-styl-bg-hover-color'].';
				}';

		// submit-button font hover color
		$css .= $submit_button_hover .' {
					color: '.$inputs['submit-button-styl-font-hover-color'].';
				}';

		// submit-button border hover color
		$css .= $submit_button_hover .' {
					border-color: '.$inputs['submit-button-styl-border-hover-color'].';
				}';

		// submit-button border
		$css .= neal_css_border_4( $submit_button,
				$inputs,
				'submit-button-styl-child'
				);

		// submit-button corner radius
		if ( $inputs['submit-button-styl-child-radius-label'] ) {
			$css .= $submit_button .' {
						border-radius: '.$inputs['submit-button-styl-child-radius'].'px;
					}';
		}

	/* ----------------- Font Options ----------------- */

		// input
		$css .= neal_css_fonts( $general_input,
	 			$inputs,
	 			'general-font'
	 			);

		// submit button
		$css .= neal_css_fonts( $submit_button,
	 			$inputs,
	 			'submit-button-font'
	 			);

		// submit button uppercase
		if ( $inputs['submit-button-font-uppercase'] ) {
			$css .= $submit_button .' {
										text-transform: uppercase;
									}';
		}



/*
***************************************************************
* #Contact page
***************************************************************
*/
	/* ----------------- General Options ----------------- */
	/* ----------------- Spacing Options ----------------- */

		// google-map height
		$css .= '.google-map {
					height: '.$contact['google-map-spac-height'].'px;	
				}';

		// google-map margin
		$css .= neal_css_spacing_2( '.google-map', 
				'margin', 
				$contact,
				'google-map-spac'
				);


/*
***************************************************************
* #Pagination
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// navigation align
		$css .= '.navigation.pagination {
					text-align: '.$pagination['navigation-align'].';
				}';

	/* ----------------- Spacing Options ----------------- */

		// navigation width & height
		if ( $pagination['navigation-spac-width'] != 0 || 
			$pagination['navigation-spac-height'] != 0 ) {
			$css .='.navigation a.page-numbers:not(.prev):not(.next), 
					 .navigation span.page-numbers {
					 	width: '.$pagination['navigation-spac-width'].'px;
					 	height: '.$pagination['navigation-spac-height'].'px;
					 }';
		}

		// navigation margin-right
		$css .= '.navigation a.page-numbers:not(.next), 
				 .navigation span.page-numbers {
					margin-right: '.$pagination['navigation-spac-margin-right'].'px;	
				}';

	/* ----------------- Styling Options ----------------- */

		// navigation background-color
		$css .= '.navigation a.page-numbers:not(.prev):not(.next), 
				 .navigation span.page-numbers:not(.dots) {
				 	background-color: '.$pagination['navigation-styl-bg-color'].';
				}';

		// navigation hover background-color
		$css .= '.navigation a.page-numbers:not(.prev):not(.next):hover {
				 	background-color: '.$pagination['navigation-styl-bg-hover-color'].';
				}';

		// navigation active background-color
		$css .= '.navigation span.page-numbers.current {
				 	background-color: '.$pagination['navigation-styl-bg-active-color'].';
				}';

		// navigation font color
		$css .= '.navigation span.page-numbers {
				 	color: '.$pagination['navigation-styl-font-color'].';
				}';

		// navigation link color
		$css .= '.navigation a.page-numbers:not(.prev):not(.next),
				.navigation.default a {
				 	color: '.$pagination['navigation-styl-link-color'].';
				}';

		// navigation link hover color
		$css .= '.navigation a.page-numbers:not(.prev):not(.next):hover,
				.navigation.default a:hover {
				 	color: '.$pagination['navigation-styl-link-hover-color'].';
				}';

		// navigation font active color
		$css .= '.navigation span.page-numbers.current {
				 	color: '.$pagination['navigation-styl-font-active-color'].';
				}';

		// navigation border-radius
		$css .= '.navigation a.page-numbers:not(.prev):not(.next) {
				 	border-radius: '.$pagination['navigation-styl-radius'].'%;
				}';

	/* ----------------- Font Options ----------------- */

		// navigation
		$css .= neal_css_fonts( '.navigation a.page-numbers:not(.prev):not(.next), 
									.navigation span.page-numbers, 
									.navigation.default a',
	 			$pagination,
	 			'navigation-font',
	 			false
	 			);

		// navigation uppercase
		if ( $pagination['navigation-font-uppercase'] ) {
			$css .= '.navigation.default a {
					 	text-transform: uppercase;
					 }';
		}

/*
***************************************************************
* #Typography
***************************************************************
*/

	/* ----------------- Font Options ----------------- */

		// paragraph
		$css .= neal_css_fonts( 'p',
	 			$typography,
	 			'paragraph-font'
	 			);	

		// paragraph font-italic
		if ( $typography['paragraph-font-italic'] ) {
			$css .= 'p {
						font-style: italic;
					}';
		}

		// paragraph uppercase
		if ( $typography['paragraph-font-uppercase'] ) {
			$css .= 'p {
						text-transform: uppercase;
					}';
		}

		// heading (h1)
		$css .= neal_css_fonts( 'h1',
	 			$typography,
	 			'h1-font'
	 			);

		// heading (h1) font-italic
		if ( $typography['h1-font-italic'] ) {
			$css .= 'h1 {
						font-style: italic;
					}';
		}

		// heading (h1) uppercase
		if ( $typography['h1-font-uppercase'] ) {
			$css .= 'h1 {
						text-transform: uppercase;
					}';
		}

		// heading (h2)
		$css .= neal_css_fonts( 'h2',
	 			$typography,
	 			'h2-font'
	 			);

		// heading (h2) font-italic
		if ( $typography['h2-font-italic'] ) {
			$css .= 'h2 {
						font-style: italic;
					}';
		}

		// heading (h2) uppercase
		if ( $typography['h2-font-uppercase'] ) {
			$css .= 'h2 {
						text-transform: uppercase;
					}';
		}

		// heading (h3)
		$css .= neal_css_fonts( 'h3',
	 			$typography,
	 			'h3-font'
	 			);

		// heading (h3) font-italic
		if ( $typography['h3-font-italic'] ) {
			$css .= 'h3 {
						font-style: italic;
					}';
		}

		// heading (h3) uppercase
		if ( $typography['h3-font-uppercase'] ) {
			$css .= 'h3 {
						text-transform: uppercase;
					}';
		}

		// heading (h4)
		$css .= neal_css_fonts( 'h4',
	 			$typography,
	 			'h4-font'
	 			);

		// heading (h4) font-italic
		if ( $typography['h4-font-italic'] ) {
			$css .= 'h4 {
						font-style: italic;
					}';
		}

		// heading (h4) uppercase
		if ( $typography['h4-font-uppercase'] ) {
			$css .= 'h4 {
						text-transform: uppercase;
					}';
		}

		// heading (h5)
		$css .= neal_css_fonts( 'h5',
	 			$typography,
	 			'h5-font'
	 			);

		// heading (h5) font-italic
		if ( $typography['h5-font-italic'] ) {
			$css .= 'h5 {
						font-style: italic;
					}';
		}

		// heading (h5) uppercase
		if ( $typography['h5-font-uppercase'] ) {
			$css .= 'h5 {
						text-transform: uppercase;
					}';
		}

		// heading (h6)
		$css .= neal_css_fonts( 'h6',
	 			$typography,
	 			'h6-font'
	 			);

		// heading (h6) font-italic
		if ( $typography['h6-font-italic'] ) {
			$css .= 'h6 {
						font-style: italic;
					}';
		}

		// heading (h6) uppercase
		if ( $typography['h6-font-uppercase'] ) {
			$css .= 'h6 {
						text-transform: uppercase;
					}';
		}

/*
***************************************************************
* #Socials & Copyright
***************************************************************
*/
	/* ----------------- General Options ----------------- */

		// copyright align
		$css .= '.main-footer .footer-bottom p {
					text-align: '.$socialcopy['general-copy-align'].';
				}';

	/* ----------------- Spacing Options ----------------- */

		// socials  margin-right
		$css .= '.site-footer .social-icons a {
					width: '.$socialcopy['social-spac-width'].'px;
					height: '.$socialcopy['social-spac-height'].'px;
					margin-right: '.$socialcopy['social-spac-margin-right'].'px;
				}';

	/* ----------------- Styling Options ----------------- */

		// socials bg color
		$css .= '.site-footer .social-icons a {
					background-color: '.$socialcopy['social-styl-bg-color'].';
				}';

		// socials font color
		$css .= '.site-footer .social-icons a,
				 .site-footer .socials-wrap .list-socials li a {
					color: '.$socialcopy['social-styl-font-color'].';
				}';

		// socials hover font color
		$css .= '.site-footer .socials-wrap .list-socials li a:hover {
					color: '.$socialcopy['social-styl-font-hover-color'].';
				 }';

		// socials hover bg & font color
		$css .= '.site-footer .social-icons a:hover {
					color: '.$socialcopy['social-styl-font-hover-color'].';
					background-color: '.$socialcopy['social-styl-bg-hover-color'].';
				}';

		if ( $socialcopy['general-social-active-icon'] > 0 ) {
			$css .= '.site-footer .social-icons a:nth-child('.$socialcopy['general-social-active-icon'].') {
					color: '.$socialcopy['social-styl-font-hover-color'].';
					background-color: '.$socialcopy['social-styl-bg-hover-color'].';
				}';

			// socials active icon hover
			$css .= '.site-footer .social-icons a:nth-child('.$socialcopy['general-social-active-icon'].'):hover {
					color: '.$socialcopy['social-styl-font-color'].';
					background: none;
				}';
		}

		// socials border-radius
		$css .= '.site-footer .social-icons a {
					border-radius: '.$socialcopy['social-styl-child-radius'].'%;
				}';

		// copyright font color
		$css .= '.site-footer .site-info p {
					color: '.$socialcopy['copy-styl-font-color'].';
				}';

		// copyright link color
		$css .= '.site-footer .site-info p a {
					color: '.$socialcopy['copy-styl-link-color'].';
				}';

	/* ----------------- Font Options ----------------- */

		// socials
		$css .= neal_css_fonts( '.site-footer .social-icons a',
	 			$socialcopy,
	 			'social-font',
	 			false
	 			);

		// copyright
		$css .= neal_css_fonts( '.site-footer .site-info p',
	 			$socialcopy,
	 			'copy-font'
	 			);

	// Theme variation settings

	$css .= '.neal-quote .author-name span::before {
				background-color: '.$body['general-styl-font-color'].';
			}';

	$css .= '.neal-tabs .tabs-menu li a {
				color: '.$body['general-styl-link-color'].';
			}';

	$css .= '.neal-tabs .tabs-content {
				color: '.$body['general-styl-bg-color'].';
				background-color: '.$body['general-styl-font-color'].';
			}';

	// CSS minify
	$css = str_replace( array( "\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css ); 

	wp_enqueue_style( 'neal-dynamic-custom-style', get_template_directory_uri() . '/inc/customizer/css/dynamic-style.css' );
    
    wp_add_inline_style( 'neal-dynamic-custom-style', $css );
}

add_action( 'wp_enqueue_scripts', 'neal_dynamic_style' );

// css font options inside font-size,font-weight,line-height,letter-spacing
function neal_css_fonts( $selector, $theme_data, $theme_control = false, $unitless = true ) {
	$theme_control = ( $theme_control ) ? $theme_control .'-' : '';
	$id = $theme_control;

	// if label is set on Theme Customizer
	if ( array_key_exists( $theme_control .'label', $theme_data ) ) {
		$label = $theme_data[ $theme_control.'label' ];
	} else {
		$label = true;
	}

	// the label open
	if ( $label ) {

		// css variables
		$font_family = '';
		$font_size = '';
		$font_weight = '';
		$line_height = '';
		$letter_spacing = '';

		if ( array_key_exists( $id.'family' , $theme_data ) ) {
			$family = str_replace( '+', ' ', $theme_data[ $id .'family' ] );
			$font_family  = 'font-family: "'.$family.'", Arial, "Helvetica Neue", Helvetica, sans-serif';
		}
		if ( array_key_exists( $id.'size' , $theme_data ) ) {
			$font_size  = 'font-size: '.$theme_data[ $id .'size' ].'px';
		}
		if ( array_key_exists( $id.'weight' , $theme_data ) ) {
			$font_weight  = 'font-weight: '.$theme_data[ $id .'weight' ];
		}
		if ( array_key_exists( $id.'line-height' , $theme_data ) ) {
			$line_height  = 'line-height: '.$theme_data[ $id .'line-height' ] . ( $unitless ? '' : 'px' );
		}
		if ( array_key_exists( $id.'letter-spacing' , $theme_data ) ) {
			$letter_spacing  = 'letter-spacing: '.$theme_data[ $id .'letter-spacing' ].'px';
		}

		// css parse
		$parse = $selector .' {
					'. $font_family .';
					'. $font_size .';
					'. $font_weight .';
					'. $line_height .';
					'. $letter_spacing .';
				}';

		return $parse;
	}
}

// css border 4
function neal_css_border_4( $selector, $theme_data, $theme_control = false ) {
	$theme_control = ( $theme_control ) ? $theme_control .'-' : '';
	$id = $theme_control;

	// if label is set on Theme Customizer
	if ( array_key_exists( $theme_control .'border-label', $theme_data ) ) {
		$label = $theme_data[ $theme_control.'border-label' ];
	} else {
		$label = true;
	}
	
	// the label open
	if ( $label ) {

		// css variables
		$b_size = '';
		$b_style = '';
		$b_color = '';

		// border size
		if ( array_key_exists( $id.'border-size' , $theme_data ) ) {
			$b_size  = $theme_data[ $id .'border-size' ];
		}
		// border style
		if ( array_key_exists( $id.'border-style' , $theme_data ) ) {
			$b_style = $theme_data[ $id .'border-style' ];
		}
		// border color
		if ( array_key_exists( $id.'border-color' , $theme_data ) ) {
			$b_color = $theme_data[ $id .'border-color' ];
		}
		
		// css parse
		$parse = $selector .' { ';

			if ( $b_size && $b_style && $b_color ) {
				$parse .= 'border:'. $b_size.'px '.$b_style.' '.$b_color.';';
			}

		$parse .= '}';

		return $parse;
	}
}

// css border
function neal_css_border( $selector, $theme_data, $theme_control = false ) {
	$theme_control = ( $theme_control ) ? $theme_control .'-' : '';
	$id = $theme_control;

	// if label is set on Theme Customizer
	if ( array_key_exists( $theme_control .'border-label', $theme_data ) ) {
		$label = $theme_data[ $theme_control.'border-label' ];
	} else {
		$label = true;
	}
	
	// the label open
	if ( $label ) {

		// css variables
		$top_size = '';
		$right_size = '';
		$bottom_size = '';
		$left_size = '';

		// example:top-bottom exists on theme data
		if ( array_key_exists( $id.'border-top-size' , $theme_data ) ) {
			$top_size  = $theme_data[ $id .'border-top-size' ];
			$top_style = $theme_data[ $id .'border-top-style' ];
			$top_color = $theme_data[ $id .'border-top-color' ];
		}
		// example:right-left exists on theme data
		if ( array_key_exists( $id.'border-right-size' , $theme_data ) ) {
			$right_size  = $theme_data[ $id .'border-right-size' ];
			$right_style = $theme_data[ $id .'border-right-style' ];
			$right_color = $theme_data[ $id .'border-right-color' ];
		}
		// example:right-left exists on theme data
		if ( array_key_exists( $id.'border-bottom-size' , $theme_data ) ) {
			$bottom_size  = $theme_data[ $id .'border-bottom-size' ];
			$bottom_style = $theme_data[ $id .'border-bottom-style' ];
			$bottom_color = $theme_data[ $id .'border-bottom-color' ];
		}
		// example:right-left exists on theme data
		if ( array_key_exists( $id.'border-left-size' , $theme_data ) ) {
			$left_size  = $theme_data[ $id .'border-left-size' ];
			$left_style = $theme_data[ $id .'border-left-style' ];
			$left_color = $theme_data[ $id .'border-left-color' ];
		}
		
		// css parse
		$parse = $selector .' { ';
			if ( $top_size ) {
				$parse .= 'border-top:'. $top_size.'px '.$top_style.' '.$top_color.';';
			}
			if ( $right_size ) {
				$parse .= 'border-right:'.$right_size.'px '.$right_style.' '.$right_color.';';
			}
			if( $bottom_size ) {
				$parse .= 'border-bottom:' .$bottom_size.'px '.$bottom_style.' '.$bottom_color.';';
			}
			if( $left_size ) {
				$parse .= 'border-left:' .$left_size.'px '.$left_style.' '.$left_color.';';
			}

		$parse .= '}';

		return $parse;
	}
}

// css spacing 2: margin, padding example: margin: 10px 15px
function neal_css_spacing_2( $selector, $type, $theme_data, $theme_control = false ) {

	$theme_control_set = ( $theme_control ) ? $theme_control  : $type;
	$theme_control = ( $theme_control ) ? $theme_control .'-' : '';
	$id = $theme_control .$type;

	// if label is set on Theme Customizer
	if ( array_key_exists( $theme_control_set .'-label', $theme_data ) ) {
		$label = $theme_data[ $theme_control_set.'-label' ];
	} else {
		$label = true;
	}
	
	// the label open
	if ( $label ) {

		// css variables
		$top = '';
		$bottom = '';
		$right = '';
		$left = '';

		// example:top-bottom exists on theme data
		if ( array_key_exists( $id.'-top-bottom' , $theme_data ) ) {
			$top = $type .'-top: ' .$theme_data[ $id.'-top-bottom' ] .'px';
			$bottom = $type .'-bottom: ' .$theme_data[ $id.'-top-bottom' ] .'px';
		}
		// example:right-left exists on theme data
		if ( array_key_exists( $id.'-right-left' , $theme_data ) ) {
			$right = $type .'-right: ' .$theme_data[ $id.'-right-left' ] .'px';
			$left = $type .'-left: ' .$theme_data[ $id.'-right-left' ] .'px';
		}
		
		// css parse
		$parse = $selector .' {
					'. $top .';
					'. $right .';
					'. $bottom .';
					'. $left .';
				}';

		return $parse;
	}
}

// css spacing: margin, padding
function neal_css_spacing( $selector, $type, $theme_data, $theme_control = false ) {

	$theme_control_set = ( $theme_control ) ? $theme_control  : $type;
	$theme_control = ( $theme_control ) ? $theme_control .'-' : '';
	$id = $theme_control .$type;

	// if label is set on Theme Customizer
	if ( array_key_exists( $theme_control_set .'-label', $theme_data ) ) {
		$label = $theme_data[ $theme_control_set.'-label' ];
	} else {
		$label = true;
	}
	
	// the label open
	if ( $label ) {

		// css variables
		$top = '';
		$right = '';
		$bottom = '';
		$left = '';

		// margin-top exists on theme data
		if ( array_key_exists( $id.'-top' , $theme_data ) ) {
			$top = $type .'-top: ' .$theme_data[ $id.'-top' ] .'px';
		}
		// margin-right exists on theme data
		if ( array_key_exists( $id.'-right' , $theme_data ) ) {
			$right = $type .'-right: ' .$theme_data[ $id.'-right' ] .'px';
		}
		// margin-bottom exists on theme data
		if ( array_key_exists( $id.'-bottom' , $theme_data ) ) {
			$bottom = $type .'-bottom: ' .$theme_data[ $id.'-bottom' ] .'px';
		}
		// margin-left exists on theme data
		if ( array_key_exists( $id.'-left' , $theme_data ) ) {
			$left = $type .'-left: ' .$theme_data[ $id.'-left' ] .'px';
		}
		
		// css parse
		$parse = $selector .' {
					'. $top .';
					'. $right .';
					'. $bottom .';
					'. $left .';
				}';

		return $parse;
	}
}