<?php

$data 			= neal_masonry_posts_data( 1, 2 );
$items			= $data['items'];
$post_query 	= $data['query'];
$post_query2 	= $data['query2'];

if ( $post_query->have_posts() ) :

?>

<div class="masonry-posts layout-1">
	<?php echo esc_html( $items['width'] ) ? '<div class="container">' : ''; ?>
	
	<div class="post-item-inner col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
			<div class="post-item col-xs-12" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
				<div class="post-content">
					<?php if ( get_post_format() == 'gallery' ) : ?>
						<span class="post-icon post-icon-gallery"><i class="fa fa-camera"></i></span>
					<?php endif; ?>

					<?php if ( get_post_format() == 'video' ) : ?>
						<span class="post-icon post-icon-video"><i class="ion-play"></i></span>
					<?php endif; ?>

					<?php if ( get_post_format() == 'audio' ) : ?>
						<span class="post-icon post-icon-audio"><i class="ion-music-note"></i></span>
					<?php endif; ?>

					<?php 
						$is_post_formats = ( get_post_format() == 'link' || get_post_format() == 'quote' );
						if ( ! $is_post_formats ) : ?>
						<div class="content">
							<!-- Post Header -->
							<div class="entry-header">
							<?php
								// category
								if ( $items['meta_category'] ) {
									neal_post_categories();
								}

								// date
								if ( $items['meta_date'] ) {
									neal_post_date();
								}

								// author
								if ( $items['meta_author'] ) {
									neal_post_author();
								}
									
								// title		
								neal_post_title();
							?>
							</div>
							<!-- Post Header End -->
							<?php if ( $items['read_more'] ) : ?>
								<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
							<?php endif; ?>
						</div>
					<?php else:
					// quote & link format
				if ( get_post_format() == 'link' ) {
					$link_description 	= get_post_meta( get_the_ID(), 'neal_link_description', true );
					$link_title 		= get_post_meta( get_the_ID(), 'neal_link_title', true );
					$link_url 			= get_post_meta( get_the_ID(), 'neal_link_url', true );

				?>
					<div class="entry-link">
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="overlay-inner">
								<div class="overlay-content">
									<?php if ( esc_html( $link_description ) ) { ?>
										<p><?php echo esc_html( $link_description ); ?></p>
									<?php  } ?>
									<small><a href="<?php echo esc_url( $link_url ); ?>" target="_blank"><?php echo esc_html( $link_title ); ?></a></small>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php } else {
							$quote_content 	= get_post_meta( get_the_ID(), 'neal_quote_content', true );
							$quote_title 	= get_post_meta( get_the_ID(), 'neal_quote_title', true );
				?>
				<div class="entry-quote">
						<div class="overlay-wrap">
							<div class="overlay">
								<div class="overlay-inner">
									<div class="overlay-content">
										<h4><?php echo esc_html( $quote_content ); ?></h4>
										<small><?php echo esc_html( $quote_title ); ?></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php 
					}
				endif; ?>
				</div>

				<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
			</div>
		<?php endwhile; ?>
	</div>
	<div class="post-item-inner col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<?php while ( $post_query2->have_posts() ) : $post_query2->the_post(); ?>
			<div class="post-item second col-xs-12" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
				<div class="post-content">
					<?php if ( get_post_format() == 'gallery' ) : ?>
						<span class="post-icon post-icon-gallery"><i class="fa fa-camera"></i></span>
					<?php endif; ?>

					<?php if ( get_post_format() == 'video' ) : ?>
						<span class="post-icon post-icon-video"><i class="ion-play"></i></span>
					<?php endif; ?>

					<?php if ( get_post_format() == 'audio' ) : ?>
						<span class="post-icon post-icon-audio"><i class="ion-music-note"></i></span>
					<?php endif; ?>

					<div class="content">
						<!-- Post Header -->
						<div class="entry-header">
						<?php
							// category
							if ( $items['meta_category'] ) {
								neal_post_categories();
							}

							// date
							if ( $items['meta_date'] ) {
								neal_post_date();
							}

							// author
							if ( $items['meta_author'] ) {
								neal_post_author();
							}
								
							// title		
							neal_post_title();
						?>
						</div>
						<!-- Post Header End -->
						<?php if ( $items['read_more'] ) : ?>
							<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
						<?php endif; ?>
					</div>
				</div>

				<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
			</div>
		<?php endwhile; ?>
	</div>

	<?php echo esc_html( $items['width'] ) ? '</div>' : ''; ?>
</div>

	<?php
		wp_reset_postdata();
endif; ?>