<?php

$data 			= neal_masonry_posts_data( 1, 3 );
$items			= $data['items'];
$post_query 	= $data['query'];
$post_query2 	= $data['query2'];

if ( $post_query->have_posts() ) :

?>

<div class="masonry-posts layout-6">
	<?php echo esc_html( $items['width'] ) ? '<div class="container">' : ''; ?>
	
	<div class="post-item-inner col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
			<div class="post-item col-xs-12" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
				<div class="post-content">
					<?php if ( get_post_format() == 'video' ) : ?>
						<span class="post-icon-video"><i class="ion-play"></i></span>
					<?php endif; ?>

					<?php if ( get_post_format() == 'gallery' ) : ?>
						<span class="post-icon-gallery"><i class="ion-images"></i></span>
					<?php endif; ?>
						
					<div class="content">
						<!-- Post Header -->
						<div class="entry-header">
						<?php 
							// category
							if ( $items['meta_category'] ) {
								neal_post_categories();
							}

							// date
							if (  $items['meta_date'] ) {
								neal_post_date();
							}

							// author
							if (  $items['meta_author'] ) {
								neal_post_author();
							}
								
							// title		
							neal_post_title();
							?>
							</div>
							<!-- Post Header End -->
							<?php if ( $items['read_more'] ) : ?>
								<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
							<?php endif; ?>
						</div>
					</div>

					<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
				</div>
		<?php endwhile; ?>
	</div>
	<div class="post-item-inner col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<?php 
			$count = 1;
			while ( $post_query2->have_posts() ) : $post_query2->the_post(); ?>
				<?php if ( $count >= 2 ) : ?>
					<div class="post-item second third col-lg-6 col-md-6 col-sm-6 col-xs-12" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
						<div class="post-content">
							<?php if ( get_post_format() == 'video' ) : ?>
								<span class="post-icon-video"><i class="ion-play"></i></span>
							<?php endif; ?>

							<?php if ( get_post_format() == 'gallery' ) : ?>
								<span class="post-icon-gallery"><i class="ion-images"></i></span>
							<?php endif; ?>
							<div class="content">
								<!-- Post Header -->
								<div class="entry-header">
								<?php

									// category
									if (  $items['meta_category'] ) {
										neal_post_categories();
									}

									// date
									if (  $items['meta_date'] ) {
										neal_post_date();
									}

									// author
									if (  $items['meta_author'] ) {
										neal_post_author();
									}
								
									// title		
									neal_post_title();
								?>
								</div>
								<!-- Post Header End -->
								<?php if ( $items['read_more'] ) : ?>
									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
								<?php endif; ?>
							</div>
						</div>

						<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
					</div>
				<?php else : ?>
					<div class="post-item second col-xs-12" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
						<div class="post-content">
							<?php if ( get_post_format() == 'video' ) : ?>
								<span class="post-icon-video"><i class="ion-play"></i></span>
							<?php endif; ?>

							<?php if ( get_post_format() == 'gallery' ) : ?>
								<span class="post-icon-gallery"><i class="ion-images"></i></span>
							<?php endif; ?>
							<div class="content">
								<!-- Post Header -->
								<div class="entry-header">
								<?php

									// category
									if (  $items['meta_category'] ) {
										neal_post_categories();
									}

									// date
									if (  $items['meta_date'] ) {
										neal_post_date();
									}

									// author
									if (  $items['meta_author'] ) {
										neal_post_author();
									}
									
									// title		
									neal_post_title();
								?>
								</div>
								<!-- Post Header End -->
								<?php if ( $items['read_more'] ) : ?>
									<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="read-more"><span><?php esc_html_e( 'Read More', 'neal' ); ?></span></a>
								<?php endif; ?>
							</div>
						</div>

						<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
					</div>
				<?php endif; ?>
			<?php 
			$count++;
			endwhile; ?>
	</div>

	<?php echo esc_html(  $items['width'] ) ? '</div>' : ''; ?>

</div>
	<?php
		wp_reset_postdata();
endif; ?>