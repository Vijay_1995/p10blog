<?php
/**
 * Template part for displaying related posts
 *
 * @package neal
 */


$args = array(
	'posts_per_page'		 => 3,
	'ignore_stikcy_posts'	 => 1,
	'tax_query' => array(
        'relation' => 'OR',
        array(
            'taxonomy' 	=> 'category',
            'field' 	=> 'ID',
            'terms' 	=> wp_get_post_categories( get_the_ID() ),
            'include_children' => false 
        ),
        array(
            'taxonomy' 	=> 'post_tag',
            'field' 	=> 'ID',
            'terms' 	=> wp_get_post_tags( get_the_ID(), array( 'fields' => 'ids' ) ),
        )
     ),
	'post__not_in'			 => array( get_the_ID() ),
	'no_found_rows'			 => true,
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false
);

$query = new WP_Query( $args );

if ( ! $query->have_posts() ) {
	return;
}

// get post meta data
$items   	= neal_get_post_options_data();
$rp_classes = 'col-xs-10 centered';

if ( $items['post_layouts'] == 2 || $items['post_layouts'] == 3 ) {
	$rp_classes = 'col-xs-12';
}

?>

<div class="container">
	<div class="row">
		<div class="related-posts <?php echo esc_attr( $rp_classes ); ?>">
				<h3 class="title"><?php echo esc_html( neal_get_option( 'blog_single_related-posts-title' ) ); ?></h3>

				<div class="row">
					<?php while( $query->have_posts() ) : $query->the_post(); ?>
						<div class="post col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="post-inner">
								<a href="<?php echo esc_url( get_the_permalink() ); ?>">
									<?php neal_post_thumbnail( 'neal-full-posts-image' ); ?>
								</a>
								<?php neal_post_date(); ?>
								<h4><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php the_title(); ?></a></h4>

								<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="post-link"></a>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
		</div>
	</div>
</div>

<?php
wp_reset_postdata();
