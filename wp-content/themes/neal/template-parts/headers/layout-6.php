<div class="container">
	<div class="row">
		<div class="site-branding">
			<?php neal_header_logo(); ?>
		</div>
		<div class="header-inner">
			<nav id="site-navigation" class="main-navigation col-xs-12" role="navigation">
				<div class="main-nav">
					<?php
						if ( has_nav_menu( 'primary' ) ) {
							$menu_args = array(
								'theme_location'	=> 'primary',
								'menu_class'	 	=> 'nav-menu',
							);

							// mega menu
							if ( class_exists( 'Neal_Mega_Menu_Walker' ) ) {
								$menu_args['walker'] = new Neal_Mega_Menu_Walker();
							}

							wp_nav_menu( $menu_args );
						}
					?>
				</div>
				
				<div class="header-icon">
					<?php 
						// shop icon
						neal_header_shop_cart(); 

						// search icon
						neal_header_search_icon();
					?>
					<div class="hamburger hamburger--3dx js-hamburger mobile-nav-btn mobile-nav-open">
					  <div class="hamburger-box">
					    <div class="hamburger-inner"></div>
					  </div>
					</div>
				</div>
			</nav>

		</div>
	</div>

	<div class="search-wrap">
		<a href="#" class="search-close"><i class="ion-android-close"></i></a>
			<?php get_search_form(); ?>
	</div>

</div>