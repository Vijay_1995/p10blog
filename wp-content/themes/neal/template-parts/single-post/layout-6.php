<?php
	// get theme customizer data
	$blog_single = get_option( 'neal_blog_single' );

	// get post meta data
	$items = neal_get_post_options_data();

	$content_classes = '';
	$is_sidebar      = false;
	$sticky_sidebar  = '';

	if ( $items['sidebar'] ) {
		$content_classes = ' col-lg-9 col-md-9 col-sm-12 col-xs-12';
		$is_sidebar = true;

		// sticky sidebar
		if ( $items['sidebar_sticky'] ) {
			$sticky_sidebar = ' sticky-sidebar';
		}
	}
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main<?php echo esc_attr( $sticky_sidebar ); ?>" role="main">
		<div class="container">
			<div class="row">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); 
				do_action( 'neal_single_post_before' );
		?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('col-lg-10 col-md-10 col-sm-10 col-xs-12 centered'); ?>>
					<?php if ( has_post_thumbnail() ) :  ?>
						<!-- Post Media -->
						<div class="entry-media">
							<?php get_template_part( 'post-formats/content-single', get_post_format() ); ?>
						</div>
						<!-- Post Media End -->
					<?php endif; ?>

					<!-- Post Header -->
					<div class="entry-header">
						<div class="post-meta">
							<?php
								// post meta
								if ( $items['meta'] && $items['meta_pos'] == 'below' ) {
									
									// post categories
									if ( $items['meta_category'] ) {
										neal_post_categories();
									}

									// post date
									if ( $items['meta_date'] ) {
										neal_post_date();
									}

									// post author
									if ( $items['meta_author'] ) {
										neal_post_author();
									}
								}

							?>
						</div>

						<?php

							// post title
							if ( $items['title_pos'] == 'below' ) {
								echo '<h1 class="entry-title" itemprop="name headline">'. get_the_title() .'</h1>';
							}

							// breadcrumbs
							if ( $blog_single['bread-label'] && $blog_single['bread-pos'] == 'below' ) {
								neal_breadcrumbs();
							}
						?>

					</div>
					<!-- Post Header End -->

					<?php echo esc_html( $is_sidebar ) ? '<div class="main-content row">' : ''; ?>

					<!-- Post Content -->
					<div class="entry-content<?php echo esc_attr( $content_classes ); ?>" itemprop="articleBody">
						<?php 
							the_content();
							
							// post content pagination
							wp_link_pages( array(
										   	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'neal' ),
											'after'  => '</div>',
											'link_before'      => '<span>',
											'link_after'       => '</span>'
								) );
						?>
					</div>

					<?php do_action( 'neal_sidebar' ); ?>

					<div class="clear"></div>

					<?php echo esc_html( $is_sidebar ) ? '</div>' : ''; ?>

					<!-- Post Content End -->

					<!-- Post Footer -->
					<footer class="entry-footer">
						<?php
							// social share icons
							neal_sharing_icon_links( true );

							// tags list
							neal_post_tags();
						?>
					</footer>
					<!-- Post Footer End -->

				</article><!-- #post-## -->

			</div>
		</div>
		<?php
				/**
				 * Functions hooked into neal_single_post_after action
				 *
				 * @hooked neal_blog_single_subscribe - 10
				 * @hooked neal_author_box			  - 20
				 * @hooked neal_related_posts		  - 30
				 * @hooked neal_display_comments	  - 40
				 * @hooked neal_post_nav			  - 50
				 */
				do_action( 'neal_single_post_after' );
			
			endwhile;
						?>
	</main><!-- #main -->
</div><!-- #primary -->