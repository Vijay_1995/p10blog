<?php
	// get theme customizer data
	$blog_single = get_option( 'neal_blog_single' );

	// get post meta data
	$items = neal_get_post_options_data();

	$content_classes = '';
	$is_sidebar      = false;
	$sticky_sidebar  = '';
	$parallax_image  = '';

	if ( $items['sidebar'] ) {
		$content_classes = ' col-lg-9 col-md-9 col-sm-12 col-xs-12';
		$is_sidebar = true;

		// sticky sidebar
		if ( $items['sidebar_sticky'] ) {
			$sticky_sidebar = ' sticky-sidebar';
		}
	}

	// parallax image
	if ( $items['parallax_image'] ) {
		$parallax_image = ' parallax-image';
	}
?>
<?php if ( has_post_thumbnail() ) : ?>
	<!-- Post Media -->
	<div class="post-full-media<?php echo esc_attr( $parallax_image ); ?>" style="background-image:url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">

		<?php
			$is_post_formats = ( get_post_format() == 'link' || get_post_format() == 'quote' );
			if ( ( $items['title_pos']       == 'in-media' ||
				 $blog_single['bread-label'] && $blog_single['bread-pos'] == 'in-media' ||
			     $items['meta_pos']        == 'in-media' ) && ! $is_post_formats ) {
				echo '<div class="media-content">';
				echo '<div class="content-inner">';
				echo '<div class="entry-header">';

				// breadcrumbs
				if ( $blog_single['bread-label'] && $blog_single['bread-pos'] == 'in-media' ) {
					neal_breadcrumbs();
				}
				
				// post title
				if ( $items['title_pos'] == 'in-media' ) {
					echo '<h1 class="entry-title" itemprop="name headline">'. get_the_title() .'</h1>';
				}

				// post meta
				if ( $items['meta'] && $items['meta_pos'] == 'in-media' ) {

					echo '<div class="post-meta">';
									
						// post categories
						if ( $items['meta_category'] ) {
							neal_post_categories();
						}

						// post date
						if ( $items['meta_date'] ) {
							neal_post_date();
						}

						// post author
						if ( $items['meta_author'] ) {
							neal_post_author();
						}

					echo '</div>';
				}
				echo '</div>';
				echo '</div>';
				echo '</div>';
			} else {
				// quote & link format
				if ( get_post_format() == 'link' ) {
					$link_description 	= get_post_meta( get_the_ID(), 'neal_link_description', true );
					$link_title 		= get_post_meta( get_the_ID(), 'neal_link_title', true );
					$link_url 			= get_post_meta( get_the_ID(), 'neal_link_url', true );

			?>
				<div class="entry-link">
				<div class="overlay-wrap">
					<div class="overlay">
						<div class="overlay-inner">
							<div class="overlay-content">
								<?php if ( esc_html( $link_description ) ) { ?>
									<p><?php echo esc_html( $link_description ); ?></p>
								<?php  } ?>
								<small><a href="<?php echo esc_url( $link_url ); ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i><?php echo esc_html( $link_title ); ?></a></small>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php } else {
					$quote_content 	= get_post_meta( get_the_ID(), 'neal_quote_content', true );
					$quote_title 	= get_post_meta( get_the_ID(), 'neal_quote_title', true );
		?>
			<div class="entry-quote">
					<div class="overlay-wrap">
						<div class="overlay">
							<div class="overlay-inner">
								<div class="overlay-content">
									<h4><?php echo esc_html( $quote_content ); ?></h4>
									<small><?php echo esc_html( $quote_title ); ?></small>
								</div>
							</div>
						</div>
					</div>
				</div>
		<?php }

		}
		?>
	</div>
	<!-- Post Media End -->
<?php endif; ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main<?php echo esc_attr( $sticky_sidebar ); ?>" role="main">
		<div class="container">
			<div class="row">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); 
				do_action( 'neal_single_post_before' );
		?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('col-lg-10 col-md-10 col-sm-10 col-xs-12 centered'); ?>>
					<!-- Post Header -->
					<div class="entry-header">
						<div class="post-meta">
							<?php
								// post meta
								if ( $items['meta'] && ! has_post_thumbnail() || 
									 $items['meta'] && $is_post_formats ) {

									// post categories
									if ( $items['meta_category'] ) {
										neal_post_categories();
									}

									// post date
									if ( $items['meta_date'] ) {
										neal_post_date();
									}

									// post author
									if ( $items['meta_author'] ) {
										neal_post_author();
									}
								}

							?>
						</div>

						<?php
							// post title
							if ( ! has_post_thumbnail() || $is_post_formats ) {
								echo '<h1 class="entry-title" itemprop="name headline">'. get_the_title() .'</h1>';
							}

						?>

						<?php
							// breadcrumbs
							if ( $blog_single['bread-label'] && ! has_post_thumbnail() ||
							     $blog_single['bread-label'] && $is_post_formats ) {
								neal_breadcrumbs();
							}
						?>
					</div>
					<!-- Post Header End -->

					<?php echo esc_html( $is_sidebar ) ? '<div class="main-content row">' : ''; ?>

					<!-- Post Content -->
					<div class="entry-content<?php echo esc_attr( $content_classes ); ?>" itemprop="articleBody">
						<?php if ( get_post_format() == 'gallery' || get_post_format() == 'video' || get_post_format() == 'audio' ) : ?>
							<?php get_template_part( 'post-formats/content-single', get_post_format() ); ?>
						<?php endif; ?>

						<?php 
							the_content();
							
							// post content pagination
							wp_link_pages( array(
										   	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'neal' ),
											'after'  => '</div>',
											'link_before'      => '<span>',
											'link_after'       => '</span>'
								) );
						?>
					</div>

					<?php do_action( 'neal_sidebar' ); ?>

					<div class="clear"></div>

					<?php echo esc_html( $is_sidebar ) ? '</div>' : ''; ?>

					<!-- Post Content End -->

					<!-- Post Footer -->
					<footer class="entry-footer">
						<?php
							// social share icons
							neal_sharing_icon_links( true );

							// tags list
							neal_post_tags();
						?>
					</footer>
					<!-- Post Footer End -->

				</article><!-- #post-## -->

			</div>
		</div>
		<?php
				/**
				 * Functions hooked into neal_single_post_after action
				 *
				 * @hooked neal_blog_single_subscribe - 10
				 * @hooked neal_author_box			  - 20
				 * @hooked neal_related_posts		  - 30
				 * @hooked neal_display_comments	  - 40
				 * @hooked neal_post_nav			  - 50
				 */
				do_action( 'neal_single_post_after' );
			endwhile;
	
		?>
	</main><!-- #main -->
</div><!-- #primary -->