jQuery( document ).ready( function( $ ) {
	"use strict";

	var demoData,
		demoDataWpNonce;

	// open modal
	$('.neal-demo-import-open-modal').on( 'click', function () {
		$('.neal-demo-import-modal-bg,.neal-demo-import-modal').addClass('open');
		demoData 		= $( this ).closest('.neal-demo-box').find( 'input[name="neal-demo"]' ).val();
		demoDataWpNonce = $( this ).closest('.neal-demo-box').find( 'input[name="_wpnonce"]' ).val();
	});

	$('.neal-demo-import-modal-bg').on( 'click', function () {
		$( this ).removeClass('open');
		$('.neal-demo-import-modal').removeClass('open');
	});

	// one click demo import
    $('.neal-demo-import').on( 'click', function( e ) {
    	e.preventDefault();

    	// hide modal
    	$('.neal-demo-import-modal-bg,.neal-demo-import-modal').removeClass('open');

        var $btnIndex = $( this ).index( '.neal-demo-import' ),
        	$progress = $( '#neal-demo-import-progress' ),
        	$log = $( '#neal-demo-import-log' ),
        	$importer = $( '#neal-demo-list #neal-demo-importer' ).eq( $btnIndex ),
        	steps = [ 'content', 'widgets', 'customizer', 'sliders' ],
        	selects = [];


        $('.neal-demo-import-modal .list-data-select input').each( function () {
        	if ( $( this ).is(':checked') ) {
        		selects.push( $( this ).val() );
        	}
        });

        var nealImport = {

			download: function ( type ) {
				nealImport.log( 'Downloading ' + type + ' file' );

				$.get(
					ajaxurl,
					{
						action: 'neal_download_file',
						type: type,
						demo: demoData,
						_wpnonce: demoDataWpNonce
					},
					function( response ) {
						if ( response.success ) {
							nealImport.import( type );
						} else {
							nealImport.log( response.data );

							if ( steps.length ) {
								nealImport.download( steps.shift() );
							} else {
								nealImport.configTheme();
							}
						}
					}
				).fail( function() {
					nealImport.log( 'Failed' );
				} );
			},

			import: function ( type ) {
				nealImport.log( 'Importing ' + type );

				var data = {
						action: 'neal_import',
						type: type,
						data: selects,
						_wpnonce: demoDataWpNonce
					};
				var url = ajaxurl + '?' + $.param( data );
				var evtSource = new EventSource( url );

				evtSource.onmessage = function ( message ) {
					var data = JSON.parse( message.data );

					switch ( data.action ) {
						case 'updateTotal':
							console.log( data.delta );
							break;

						case 'updateDelta':
							console.log(data.delta);
							break;

						case 'complete':
							evtSource.close();
							nealImport.log( type + ' has been imported successfully!' );

							if ( steps.length ) {
								nealImport.download( steps.shift() );
							} else {
								nealImport.configTheme();
							}

							break;
					}
				};

				evtSource.addEventListener( 'log', function ( message ) {
					var data = JSON.parse( message.data );
					nealImport.log( data.message );
				});
			},

			configTheme: function () {
				$.get(
					ajaxurl,
					{
						action: 'neal_config_theme',
						demo: demoData,
						_wpnonce: demoDataWpNonce
					},
					function( response ) {
						if ( response.success ) {
							nealImport.generateImages();
						}

						nealImport.log( response.data );
					}
				).fail( function() {
					nealImport.log( 'Failed' );
				} );
			},

			generateImages: function () {
				$.get(
					ajaxurl,
					{
						action: 'neal_get_images',
						_wpnonce: demoDataWpNonce
					},
					function( response ) {
						if ( ! response.success ) {
							nealImport.log( response.data );
							nealImport.log( 'Import completed!' );
							$progress.find( '.spinner' ).hide();
							return;
						} else {
							var ids = response.data;

							if ( ! ids.length ) {
								nealImport.log( 'Import completed!' );
								$progress.find( '.spinner' ).hide();
							}

							nealImport.log( 'Starting generate ' + ids.length + ' images' );

							nealImport.generateSingleImage( ids );
						}
					}
				);
			},

			generateSingleImage: function ( ids ) {
				if ( ! ids.length ) {
					nealImport.log( 'Import completed!' );
					$progress.find( '.spinner' ).hide();
					$progress.removeClass('warning notice-warning').addClass('updated');
					
					return;
				}

				var id = ids.shift();

				$.get(
					ajaxurl,
					{
						action: 'neal_generate_image',
						id: id,
						_wpnonce: demoDataWpNonce
					},
					function( response ) {
						nealImport.log( response.data + ' (' + ids.length + ' images left)' );

						nealImport.generateSingleImage( ids );
					}
				);
			},

			log: function( message ) {
				$progress.find( '.text' ).text( message );
				$log.prepend( '<p>' + message + '</p>' );
			}
		};

		nealImport.download( steps.shift() );

		$('.neal-demo-container').remove();
    	$progress.show();
    	$('.neal-info').show();

	});

} );
