<?php
/**
 * Plugin Name: Neal Demo Importer
 * Plugin URI: https://www.thespan.net/wordpress-themes/neal
 * Description: One click demo importer
 * Author: TheSpan
 * Author URI: https://themeforest.net/user/thespan
 * Version: 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Demo_Importer' ) ) :

	/**
	 * The Main demo importer class.
	 */
	class Neal_Demo_Importer {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 *
		 * @var array
		 */
		public $data;

		public function __construct() {

			// include demo packages
			require_once plugin_dir_path( __FILE__ ) . '/neal-demo-packages.php';

			add_action( 'admin_menu', array( $this, 'register_menu' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );

			add_action( 'wp_ajax_neal_download_file', array( $this, 'ajax_download_file' ) );
			add_action( 'wp_ajax_neal_import', array( $this, 'ajax_import' ) );
			add_action( 'wp_ajax_neal_config_theme', array( $this, 'ajax_config_theme' ) );
			add_action( 'wp_ajax_neal_get_images', array( $this, 'ajax_get_images' ) );
			add_action( 'wp_ajax_neal_generate_image', array( $this, 'ajax_generate_image' ) );

			$this->data = apply_filters( 'neal_demo_packages', array() );
		}

		/**
		 * Register menu
		 *
		 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/admin_menu
		 */
		public function register_menu() {
			if ( empty( $this->data ) ) {
				return;
			}

			add_theme_page(
				esc_html__( 'Import Demo Data', 'neal' ),
				esc_html__( 'Import Demo Data', 'neal' ),
				'edit_theme_options',
				'import-demo-data',
				array( $this, 'import_demo_page' )
			);
		}

		/**
		 * Load scripts
		 */
		public function scripts( $hook ) {
			if ( 'appearance_page_import-demo-data' != $hook ) {
				return;
			}

			// style
	        wp_register_style( 'neal-demo-import', plugins_url( 'css/neal-importer.css', __FILE__ ), array(), '1.0.0' );
	        wp_enqueue_style( 'neal-demo-import' );

	        // script
	        wp_register_script( 'neal-demo-import', plugins_url( 'js/neal-importer.js', __FILE__ ), array( 'jquery' ), true );
	        wp_enqueue_script( 'neal-demo-import' );
		}

		/**
		 * Import demo frontend page
		 */
		public function import_demo_page() {
			?>

			<div class="wrap">
				<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

				<div class="notice notice-warning is-dismissible">
					<p><?php _e( 'Before you begin, make sure all the required plugins are activated.', 'neal' ) ?></p>
					<button type="button" class="notice-dismiss"><span class="screen-reader-text"><?php _e( 'Dismiss this notice.', 'neal' ) ?></span></button>
				</div>

				<p class="neal-info"><?php esc_html_e( 'This process may take a few minutes, please continue to wait.', 'neal' ) ?></p>

				<div class="neal-demo-container">

					<h2><?php esc_html_e( 'With this demo you can quickly create your website with just one click.', 'neal' ); ?></h2>

					<p class="neal-import-demo-feature"><?php _e( 'The demo includes: ', 'neal' ); ?></p>

            		<ul class="neal-demo-import-list">
                		<li>
                			<p><?php esc_html_e( 'The demo data will be overwritten with your existing data and there will be no data loss.', 'neal'); ?></p>
                		</li>

               			<li>
               				<p><?php esc_html_e( 'Posts, products, pages, comments, images, menus, custom fields, categories, tags and widgets will all be imported.', 'neal' );?>
               				</p>
               			</li>
                		<li>
                			<?php if ( count( $this->data ) > 1 ) { ?>
                				<p><?php esc_html_e( 'Please select the demo you want and click on the "Import" button and wait a few minutes.', 'neal' ); ?>
                			<?php } else { ?>
                				<p><?php esc_html_e( 'Please wait a few minutes by clicking on the "Import Demo Data" button.', 'neal' ); ?>
                			<?php } ?>
                			</p>
                		</li>
            		</ul>

            		<hr><br/>

					<?php

						if ( count( $this->data ) > 1 ) {

							foreach ( $this->data as $demo => $data ) : ?>

								<div class="neal-demo-box" id="neal-demo-list">
									<div id="neal-demo-importer">
										<input type="hidden" name="neal-demo" value="<?php echo esc_attr( $demo ) ?>">
										<?php wp_nonce_field( 'neal_demo_import' ) ?>
									</div>

									<div class="neal-demo-image">
										<a href="<?php echo esc_url( $data['preview_url'] ) ?>" target="_blank">
											<img src="<?php echo esc_url( $data['preview'] ) ?>">
										</a>
									</div>

									<div class="neal-demo-box-tools">
										<div class="neal-demo-install-progress"></div>
										<div class="neal-demo-install-actions">
											<input type="button" class="button button-primary neal-demo-import-open-modal" value="<?php esc_html_e( 'Import', 'neal' ) ?>">
											<h2 class="neal-demo-title"><?php echo $data['name'] ?></h2>
											<div class="clear"></div>
										</div>
									</div>
								</div>

							<?php endforeach; 

						} else {
					?>
							<div id="neal-demo-list">
								<div id="neal-demo-importer">
									<input type="hidden" name="neal-demo" value="0">
									<?php wp_nonce_field( 'neal_demo_import' ) ?>

									<input type="button" value="<?php esc_html_e( 'Import Demo Data', 'neal' ); ?>" class="button button-primary button-hero neal-demo-import-open-modal">
								</div>
							</div>

						<?php } ?>
				</div>

				<div id="neal-demo-import-progress" class="notice warning notice-warning">
					<p>
						<span class="spinner is-active"></span>
						<span class="text"><?php esc_html_e( 'Starting...', 'neal' ) ?></span>
					</p>
				</div>
				<div id="neal-demo-import-log" class="neal-demo-log"></div>

				<div class="neal-demo-import-modal">
					<h2><?php esc_html_e( 'Are you sure?', 'neal' ); ?></h2>
					<p>
						<?php esc_html_e( 'The demo data will be overwritten with your existing data and there will be no data loss.Posts, products, pages, comments, images, menus, custom fields, categories, tags and widgets will all be imported. Please select the demo you want and click on the "Import" button and wait a few minutes.', 'neal' ); ?>
					</p>
					<div class="list-data-select">
						<input type="checkbox" id="neal-di-data-customizer" value="customizer" checked><label for="neal-di-data-customizer"><?php esc_html_e( 'Customizer', 'neal' ); ?></label>
						<input type="checkbox" id="neal-di-data-widgets" value="widgets" checked><label for="neal-di-data-widgets"><?php esc_html_e( 'Widgets', 'neal' ); ?></label>
						<input type="checkbox" id="neal-di-data-contents" value="contents" checked><label for="neal-di-data-contents"><?php esc_html_e( 'Contents', 'neal' ); ?></label>
					</div>
					<footer>
						<a href="#" class="neal-demo-import"><?php esc_html_e( 'Yes, I\'m sure', 'neal' ) ?></a>
					</footer>
				</div>
				<div class="neal-demo-import-modal-bg"></div>

			</div>

			<?php
		}


		/**
		 * Import content
		 * Import posts, pages, menus, custom post types
		 *
		 * @param  string $file The exported file's name
		 */
		public function import_content( $file ) {
			// Disable import of authors.
			add_filter( 'wxr_importer.pre_process.user', '__return_false' );

			// Disables generation of multiple image sizes (thumbnails) in the content import step.
			add_filter( 'intermediate_image_sizes_advanced', '__return_null' );

			if ( ! file_exists( $file ) ) {
				return false;
			}

			// Import content.
			if ( ! class_exists( 'Neal_Demo_Content_Importer' ) ) {
				require_once plugin_dir_path( __FILE__ ) . '/includes/class-content-importer.php';
			}

			$importer = new Neal_Demo_Content_Importer( array(
				'fetch_attachments' => true
			) );

			if ( ! class_exists( 'WP_Importer_Logger' ) ) {
				require_once plugin_dir_path( __FILE__ ) . '/includes/class-logger.php';
			}

			if ( ! class_exists( 'WP_Importer_Logger_ServerSentEvents' ) ) {
				require_once plugin_dir_path( __FILE__ ) . '/includes/class-logger-serversentevents.php';
			}

			$logger = new WP_Importer_Logger_ServerSentEvents();
			$err = $importer->set_logger( $logger );

			return $importer->import( $file );
		}


		/**
		 * Import widgets
		 *
		 * @param  string $file The exported file's name
		 */
		function import_widgets( $file ) {
			if ( ! file_exists( $file ) ) {
				return false;
			}

			if ( ! class_exists( 'Neal_Demo_Widgets_Importer') ) {
				require_once plugin_dir_path( __FILE__ ) . '/includes/widgets-importer.php';
			}

			$data = json_decode( file_get_contents( $file ) );

			$importer = new Neal_Demo_Widgets_Importer();
			$importer->import( $data );

			return true;
		}

		/**
		 * Import theme options
		 *
		 * @param  string $file The exported file's name
		 */
		public function import_customizer( $file ) {
			if ( ! file_exists( $file ) ) {
				return false;
			}

			if( ! class_exists( 'Neal_Demo_Customizer_Importer' ) ) {
				require_once plugin_dir_path( __FILE__ ) . '/includes/customizer-importer.php';
			}

			// Disables generation of multiple image sizes (thumbnails) in the content import step.
			add_filter( 'intermediate_image_sizes_advanced', '__return_null' );

			$import = new Neal_Demo_Customizer_Importer();
			$import->download_images = true;
			$import->import( $file );

			return true;
		}

		/**
		 * Import exported revolution sliders
		 */
		public function import_sliders() {
			if ( ! class_exists( 'RevSlider' ) ) {
				return new WP_Error( 'plugin-not-installed', esc_html__( 'Revolution Slider plugin is not installed', 'neal' ) );
			}

			$path = $this->get_demo_path();
			$sliders_zip = $path . '/sliders.zip';

			if ( empty( $sliders_zip ) ) {
				return new WP_Error( 'import-sliders', esc_html__( 'There is no slider to import', 'neal' ) );
			}

			$unzipfile = unzip_file( $sliders_zip, $path );

			if ( is_wp_error( $unzipfile ) ) {
				define('FS_METHOD', 'direct'); //lets try direct.

				WP_Filesystem();

				$unzipfile = unzip_file( $sliders_zip, $path );
				@unlink( $sliders_zip );
			}

			$files = scandir( $path . '/sliders/' );

			if ( empty( $files ) ) {
				return new WP_Error( 'import-sliders', esc_html__( 'There is no slider to import', 'neal' ) );
			}

			$slider = new RevSlider();

			$this->emit_sse_message( array(
				'action' => 'updateTotal',
				'type'   => 'slider',
				'delta'  => count( $files ) - 2,
			));

			foreach ( $files as $file ) {
				if ( $file == '.' || $file == '..' ) {
					continue;
				}
				$file = $path . '/sliders/' . $file;

				$response = $slider->importSliderFromPost( true, true, $file );

				$this->emit_sse_message( array(
					'action' => 'updateDelta',
					'type'   => 'slider',
					'delta'  => $response,
				));

				unlink( $file );
			}
		}

		/**
		 * Import menu locations
		 *
		 * @param  string $file The exported file's name
		 */
		public function setup_menus( $demo ) {
			$demo = $this->data[$demo];

			if ( ! isset( $demo['menus'] ) ) {
				return;
			}

			$data = $demo['menus'];
			$menus = wp_get_nav_menus();
			$locations = array();

			foreach ( $data as $key => $val ) {
				foreach ( $menus as $menu ) {
					if ( $val && $menu->slug == $val ) {
						$locations[$key] = absint( $menu->term_id );
					}
				}
			}

			set_theme_mod( 'nav_menu_locations', $locations );
		}

		/**
		 * Setup pages
		 */
		public function setup_pages( $demo ) {
			$demo = $this->data[$demo];

			if ( ! isset( $demo['pages'] ) ) {
				return;
			}

			// Front Page
			if ( isset( $demo['pages']['front_page'] ) ) {
				$home = get_page_by_title( $demo['pages']['front_page'] );

				if ( $home ) {
					update_option( 'show_on_front', 'page' );
					update_option( 'page_on_front', $home->ID );
				}
			}

			// Blog Page
			if ( isset( $demo['pages']['blog'] ) ) {
				$blog = get_page_by_title( $demo['pages']['blog'] );

				if ( $blog ) {
					update_option( 'page_for_posts', $blog->ID );
				}
			}

			// WooCommerce Pages
			if ( class_exists( 'WooCommerce' ) ) {
				// Shop page
				if ( isset( $demo['pages']['shop'] ) && ! get_option( 'woocommerce_shop_page_id' ) ) {
					$shop = get_page_by_title( $demo['pages']['shop'] );

					if ( $shop ) {
						update_option( 'woocommerce_shop_page_id', $shop->ID );
					}
				}

				// Cart page
				if ( isset( $demo['pages']['cart'] ) && ! get_option( 'woocommerce_cart_page_id' ) ) {
					$cart = get_page_by_title( $demo['pages']['cart'] );

					if ( $cart ) {
						update_option( 'woocommerce_cart_page_id', $cart->ID );
					}
				}

				// Checkout page
				if ( isset( $demo['pages']['checkout'] ) && ! get_option( 'woocommerce_checkout_page_id' ) ) {
					$checkout = get_page_by_title( $demo['pages']['checkout'] );

					if ( $checkout ) {
						update_option( 'woocommerce_checkout_page_id', $checkout->ID );
					}
				}

				// Myaccount page
				if ( isset( $demo['pages']['my_account'] ) && ! get_option( 'woocommerce_myaccount_page_id' ) ) {
					$account = get_page_by_title( $demo['pages']['my_account'] );

					if ( $account ) {
						update_option( 'woocommerce_myaccount_page_id', $account->ID );
					}
				}
			}

			flush_rewrite_rules();
		}

		/**
		 * Update options
		 *
		 * @param  int $demo
		 */
		public function update_options( $demo ) {
			$demo = $this->data[$demo];

			if ( empty( $demo['options'] ) ) {
				return;
			}

			foreach ( $demo['options'] as $option => $value) {
				update_option( $option, $value );
			}
		}

		/**
		 * Generate image sizes
		 * @param  int $id
		 */
		public function generate_image( $id ) {
			$fullsizepath = get_attached_file( $id );

			if ( false === $fullsizepath || ! file_exists( $fullsizepath ) ) {
				return false;
			}

			$metadata = wp_generate_attachment_metadata( $id, $fullsizepath );

			if ( ! $metadata || is_wp_error( $metadata ) ) {
				return false;
			}

			// If this fails, then it just means that nothing was changed (old value == new value)
			wp_update_attachment_metadata( $id, $metadata );

			return true;
		}

		/**
		 * Ajax function to import demo content
		 */
		public function ajax_import() {
			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'neal_demo_import' ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
				exit;
			}

			// Turn off PHP output compression
			$previous = error_reporting( error_reporting() ^ E_WARNING );
			ini_set( 'output_buffering', 'off' );
			ini_set( 'zlib.output_compression', false );
			error_reporting( $previous );

			if ( $GLOBALS['is_nginx'] ) {
				// Setting this header instructs Nginx to disable fastcgi_buffering
				// and disable gzip for this request.
				header( 'X-Accel-Buffering: no' );
				header( 'Content-Encoding: none' );
			}

			// Start the event stream.
			header( 'Content-Type: text/event-stream' );

			set_time_limit( 0 );

			// Ensure we're not buffered.
			wp_ob_end_flush_all();
			flush();

			$type = $_GET['type'];
			$data = $_GET['data'];
			$dir = $this->get_demo_path();

			if ( $type == 'content' && in_array( 'contents', $data ) ) {
				$file = $dir . '/demo-content.xml';
				$result = $this->import_content( $file );
			} elseif ( $type == 'widgets' && in_array( 'widgets', $data ) ) {
				$file = $dir . '/widgets.wie';
				$result = $this->import_widgets( $file );
			} elseif ( $type == 'customizer' && in_array( 'customizer', $data ) ) {
				$file = $dir . '/customizer.dat';
				$result = $this->import_customizer( $file );
			} elseif ( $type == 'sliders' && in_array( 'sliders', $data ) ) {
				$result = $this->import_sliders();
			}

			// Let the browser know we're done.
			$complete = array(
				'action' => 'complete',
				'error' => false,
			);

			if ( is_wp_error( $result ) ) {
				$complete['error'] = $result->get_error_message();
			}

			@unlink( $file );

			$this->emit_sse_message( $complete );
			exit;
		}

		/**
		 * Ajax function to download file
		 */
		public function ajax_download_file() {
			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'neal_demo_import' ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
				exit;
			}

			$demo = $_GET['demo'];
			$demo = $this->data[$demo];

			$type = $_GET['type'];

			if ( ! isset( $demo[$type] ) ) {
				wp_send_json_error( esc_html__( 'This demo dose not need', 'neal' ) . " $type" );
				exit;
			}

			if ( empty( $demo[$type] ) ) {
				wp_send_json_error( esc_html__( 'File does not exists', 'neal' ) );
				exit;
			}

			@set_time_limit(0);

			$file = $this->download_file( $demo[$type] );

			if ( is_wp_error( $file ) ) {
				wp_send_json_error( $file->get_error_message() );
				exit;
			}

			wp_send_json_success();
		}

		/**
		 * Ajax function to setup front page and blog page
		 */
		public function ajax_config_theme() {
			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'neal_demo_import' ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
				exit;
			}

			$demo = $_GET['demo'];

			// Setup pages
			$this->setup_pages( $demo );

			// Setup menu locations
			$this->setup_menus( $demo );

			// Update options
			$this->update_options( $demo );

			wp_send_json_success( esc_html__( 'Finish setting up front page and blog page.', 'neal' ) );
		}


		/**
		 * Ajax function to get all image ids
		 */
		public function ajax_get_images() {
			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'neal_demo_import' ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
				exit;
			}

			global $wpdb;

			$images = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image/%' ORDER BY ID DESC" );

			$ids = array();

			if ( $images ) {
				foreach ( $images as $image ) {
					$ids[] = $image->ID;
				}
			}

			wp_send_json_success( $ids );
		}

		/**
		 * Ajax function to generate a single image
		 */
		public function ajax_generate_image() {
			if ( ! wp_verify_nonce( $_GET['_wpnonce'], 'neal_demo_import' ) ) {
				wp_send_json_error( esc_html__( 'Verifing failed', 'neal' ) );
				exit;
			}

			$id = absint( $_REQUEST['id'] );

			@set_time_limit( 900 ); // 5 minutes per image should be PLENTY

			$result = $this->generate_image( $id );

			if ( ! $result ) {
				wp_send_json_error( esc_html__( 'Failed to generate image ID:', 'neal' ) . " $id" );
			} else {
				wp_send_json_success( sprintf( esc_html__( 'Generated image ID %s successfully', 'neal' ), $id ) );
			}
		}

		/**
		 * Download file from URL
		 *
		 * @param  staring $file_url
		 * @return string  Downloaded file path
		 */
		protected function download_file( $file_url ) {
			$filename = basename( $file_url );
			$path = $this->get_demo_path();
			$file = $path . '/' . $filename;

			wp_mkdir_p( $path );

			$ifp = @fopen( $file, 'wb' );

			if ( ! $ifp ) {
				return new WP_Error( 'import_file_error', sprintf( __( 'Could not write file %s' ), $file ) );
			}

			@fwrite( $ifp, 0 );
			fclose( $ifp );
			clearstatcache();

			// Set correct file permissions
			$stat = @stat( dirname( $file ) );
			$perms = $stat['mode'] & 0007777;
			$perms = $perms & 0000666;
			@chmod( $file, $perms );
			clearstatcache();

			$response = wp_remote_get( $file_url, array(
				'stream' => true,
				'filename' => $file,
				'timeout' => 500,
			) );

			// request failed
			if ( is_wp_error( $response ) ) {
				@unlink( $file );
				return $response;
			}

			$code = (int) wp_remote_retrieve_response_code( $response );

			// make sure the fetch was successful
			if ( $code !== 200 ) {
				@unlink( $file );

				return new WP_Error(
					'import_file_error',
					sprintf(
						esc_html__( 'Remote server returned %1$d %2$s for %3$s', 'neal' ),
						$code,
						get_status_header_desc( $code ),
						$url
					)
				);
			}

			if ( 0 == filesize( $file ) ) {
				@unlink( $file );
				return new WP_Error( 'import_file_error', esc_html__( 'Zero size file downloaded', 'neal' ) );
			}


			return $file;
		}

		/**
		 * Emit a Server-Sent Events message.
		 *
		 * @param mixed $data Data to be JSON-encoded and sent in the message.
		 */
		protected function emit_sse_message( $data ) {
			echo "event: message\n";
			echo 'data: ' . wp_json_encode( $data ) . "\n\n";

			// Extra padding.
			echo ':' . str_repeat( ' ', 2048 ) . "\n\n";

			flush();
		}

		/**
		 * Get the path to demo directory
		 *
		 * @return string
		 */
		private function get_demo_path() {
			$upload_dir = wp_upload_dir();
			$dir = path_join( $upload_dir['basedir'], 'demo_files' );

			return $dir;
		}
		
	} //end Neal_Demo_Importer class

endif;

return new Neal_Demo_Importer();