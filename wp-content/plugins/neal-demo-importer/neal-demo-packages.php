<?php
/**
 * Demo content settings
 */

function neal_demo_setup() {
	$packages = array(
		array(
			'name'       	=> 'Home 1',
			'preview'    	=> plugins_url( 'data/home-1/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/',
			'customizer' 	=> plugins_url( 'data/home-1/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-1/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-1/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary'	  => 'Left menu',
				'secondary'   => 'Right menu',
				'mobile-menu' => 'Mobile menu',
				'footer-menu' => 'Footer menu'
			)
		),
		array(
			'name'       	=> 'Home 2',
			'preview'    	=> plugins_url( 'data/home-2/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/home-2/',
			'customizer' 	=> plugins_url( 'data/home-2/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-2/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-2/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary'		=> 'Main menu',
				'mobile-menu'	=> 'Mobile menu'
			)
		),
		array(
			'name'       	=> 'Home 3',
			'preview'    	=> plugins_url( 'data/home-3/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/home-3/',
			'customizer' 	=> plugins_url( 'data/home-3/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-3/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-3/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary'	  => 'Left menu',
				'secondary'   => 'Right menu',
				'footer-menu' => 'Footer menu'
			)
		),
		array(
			'name'       	=> 'Home 4',
			'preview'    	=> plugins_url( 'data/home-4/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/home-4/',
			'customizer' 	=> plugins_url( 'data/home-4/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-4/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-4/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary' => 'Main menu',
			)
		),
		array(
			'name'       	=> 'Home 5',
			'preview'    	=> plugins_url( 'data/home-5/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/home-5/',
			'customizer' 	=> plugins_url( 'data/home-5/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-5/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-5/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary' => 'Main menu',
			)
		),
		array(
			'name'       	=> 'Home 6',
			'preview'    	=> plugins_url( 'data/home-6/preview.jpg', __FILE__ ),
			'preview_url'   => 'http://neal.thespan.net/home-6/',
			'customizer' 	=> plugins_url( 'data/home-6/customizer.dat', __FILE__ ),
			'content'    	=> plugins_url( 'data/home-6/demo-content.xml', __FILE__ ),
			'widgets'    	=> plugins_url( 'data/home-6/widgets.wie', __FILE__ ),
			'pages'      	=> array(
				'front_page' => 'Home'
			),
			'menus'			=> array(
				'primary'	=> 'Main menu',
			)
		),
	);
	
	return $packages;
}

add_filter( 'neal_demo_packages', 'neal_demo_setup' );