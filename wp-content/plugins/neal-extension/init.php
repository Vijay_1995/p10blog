<?php
/**
 * Plugin Name: Neal Extension
 * Plugin URI: https://www.thespan.net/wordpress-themes/neal
 * Description: 
 * Author: TheSpan
 * Author URI: https://themeforest.net/user/thespan
 * Version: 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! defined( 'NEAL_EXT_DIR' ) ) {
	define( 'NEAL_EXT_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'NEAL_EXT_URL' ) ) {
	define( 'NEAL_EXT_URL', plugin_dir_url( __FILE__ ) );
}

/*
***************************************************************
* #Initialize all the things.
***************************************************************
*/

// Main Class
require_once( NEAL_EXT_DIR . '/inc/class-neal-ext.php');

// Shortcodes Class
require_once( NEAL_EXT_DIR . '/inc/class-neal-ext-shortcodes.php');

// Shortcode Items
require_once( NEAL_EXT_DIR . '/inc/shortcodes/shortcode-items.php');

// Shortcode Components
require_once( NEAL_EXT_DIR . '/inc/shortcodes/shortcode-components.php');

// Functions
require_once( NEAL_EXT_DIR . '/inc/neal-ext-functions.php');

// Custom Fonts
require_once( NEAL_EXT_DIR . '/inc/custom-fonts/neal-ext-custom-fonts.php');

// Mega Menu
require_once( NEAL_EXT_DIR . '/inc/mega-menu/class-neal-mega-menu.php');
require_once( NEAL_EXT_DIR . '/inc/mega-menu/class-neal-mega-menu-walker.php');

// Portfolio
require_once( NEAL_EXT_DIR . '/inc/portfolio/class-neal-ext-portfolio.php');