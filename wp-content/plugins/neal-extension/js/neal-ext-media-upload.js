jQuery(document).ready(function( $ ) {
	"use strict";

	// category images upload
	$('body').on( 'click', '.neal-cat-image-upload', function ( event ) {
		var btnThis = $( this );
		var bannerImg = btnThis.next( '.neal-cat-thumb-id');
		
		// Prevents default behaviour
		event.preventDefault();

        var mediaFrame = wp.media({
	        title: nealUploader.mediaTitle,
	        multiple: false,
	        library: {
	            type: 'image'
	        },
	        button: {
	            text: nealUploader.mediaButton
	        }
	    });

	    mediaFrame.on('select', function() {
	    	var selection = mediaFrame.state().get( 'selection' );

	    	selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					var attachmentImage = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

					$('#neal-cat-thumbnail img').attr( 'src', attachmentImage );
				}

				bannerImg.val( attachment.id );
			});
	    });

	    // open popup
        mediaFrame.open();
	});

	// category images remove
	$('body').on( 'click', '.neal-cat-image-remove', function ( event ) {
		$( this ).prev().prev( '.banner-image-prew' ).find('img').remove();
		$( this ).next( 'input#martivi-banner-img' ).val('');
	});

});