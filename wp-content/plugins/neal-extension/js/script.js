jQuery(function( $ ) {
	"use strict";

	// define main variables
	var $window   = $( window ),
    	$document = $( document ),
    	$body 	  = $('body');


    $('.neal-shortcode-insert').on( 'click', function() {
    	var html,
    		url = $( this ).data('url');

      $.post( ajaxurl, {
        'action': 'neal_shortcode_list_items',
      }, function( items ) {
        $('.neal-list-shortcodes').html( items );

      });
    	// modal dom
    	html = `
    		<div class="neal-scode-modal-bg"></div>

    		<div class="neal-scode-modal">
    			<div class="neal-scode-content">
    				<a href="#" class="m-scode-mo-close">×</a>
    				<div class="neal-list-shortcodes">
    				</div>

    				<div class="neal-shortcodes-settings">
                        <div class="neal-sc-settings-header">
        					<a href="#" class="list-back"><i class="dashicons dashicons-arrow-left-alt"></i> Back</a>
        					<h2><span></span>Settings</h2>
                        </div>
    					<div class="settings-item">
    					</div>
                        <div class="neal-shortcode-loading">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                        </div>
    				</div>
    			</div>
    		</div>
    	`;

    	if ( ! $body.hasClass('is-neal-modal') ) {
    		$body.append( html );

    		$body.addClass('is-neal-modal');
    	}

    	$('.neal-scode-modal-bg, .neal-scode-modal').fadeIn();

    	return false;
    });

    $body.on( 'click', '.m-scode-mo-close', function() {
    	$('.neal-scode-modal-bg, .neal-scode-modal').fadeOut();
    	return false;
    });

    $body.on( 'click', '.neal-scode-modal-bg', function() {
        $('.neal-scode-modal-bg, .neal-scode-modal').fadeOut();
        return false;
    });

    $body.on( 'click', '.neal-list-shortcodes span', function(e) {
    	$('.neal-list-shortcodes').hide();
    	$('.neal-shortcodes-settings').show();
    });

    $body.on('click', '.neal-shortcodes-settings .list-back', function(e) {
    	$('.neal-list-shortcodes').show();
    	$('.neal-shortcodes-settings').hide();
    	$('.neal-shortcodes-settings .settings-item').empty();

    	return false;
    });

    $body.on('click', '.neal-list-shortcodes span', function(e) {
    	var short = $( this ).data('shortcode');
        $('.neal-shortcode-loading').show();
        $('.neal-sc-settings-header h2 span').html( short );

          $.post( ajaxurl, {
            'action': 'neal_shortcode_items',
            'data': short,
          }, function( item ) {

            $('.neal-shortcodes-settings .settings-item').empty().html( item );
            $('.neal-shortcode-loading').hide();

            $('.neal-shortcode-colorpic').wpColorPicker();

            tinymce.remove('#neal-recipe-ingredients,#neal-recipe-directions,#neal-recipe-notes');

            var wp_editor_e = 'neal-recipe-ingredients,neal-recipe-directions,neal-recipe-notes';
            tinymce.init( {
                    mode : "exact",
                    elements : wp_editor_e,
                    theme: "modern",
                    skin: "lightgray",
                    menubar : false,
                    statusbar : true,
                    toolbar: 
                        "bold,italic,underline,strikethrough,bullist,numlist,link,unlink"
                    ,
                    plugins : "paste,lists,wordpress,wpeditimage,wpgallery,wplink,wpdialogs",
                    
                } );

            if ( tinymce.get( wp_editor_e ) ) {
                tinymce.get( wp_editor_e ).getContent();
            }

            // custom slider
            $('.neal-shortcode-slider').on( 'input', function () {
                $( this ).next('.neal-shortcode-slider-value').html( $( this ).val() );
            });
        
        });

    });

    // Insert shortcode
	$body.on('click', '.neal-insert-shortcode', function(e) {
		var short = $('.settings-item #neal-shortcode').val();
		// Prepare data
		var shortcode = parse( short );

		e.preventDefault();

        tinymce.get("content").execCommand( 'mceInsertContent', false, shortcode );

        neal_hide_modal();
        return false;
	});

    function neal_hide_modal() {
        $('.neal-scode-modal-bg, .neal-scode-modal').fadeOut( function() {
            $('.neal-list-shortcodes').show();
            $('.neal-shortcodes-settings').hide();
            $('.neal-shortcodes-settings .settings-item').empty();
        });
    }

	function parse(short) {
		// prepare data
		var result = new String(''),
			mce_selection = (typeof tinymce !== 'undefined' && tinymce.get("content") != null && tinymce.get('content').hasOwnProperty('selection')) ? tinymce.get('content').selection.getContent({
			 format: "text"
			}) : '';

        var childShort = '';

		// open shortcode
		result += '[' + short;

        $('.settings-item .neal-shortcode-attr-container:not(.neal-child-shortcode) .neal-shortcode-attr').each( function () {
			var $this = $(this),
				value = '';

            if ( tinymce.get( $this.attr('id') ) ) {
                 if ( tinymce.get( $this.attr('id') ).getContent() ) {
                    var shortcode = 'neal_ext_' + $this.data('ch-shortcode');
                    childShort += '[';
                    childShort += shortcode;
                    childShort += ']';
                    childShort += tinymce.get( $this.attr('id') ).getContent();
                    childShort += '[/' + shortcode + ']';
                 }
            }

    			// selects
    			if ($this.is('select') ) value = $this.find('option:selected').val();
    			// other fields
    			else value = $this.val();
    			// check that value is not empty
    			if (value == null) value = '';
    			else if (typeof value === 'array') value = value.join(',');
    			// add attribute
    			if (value !== '') result += ' ' + $(this).attr('name') + '="' + $(this).val().toString().replace(/"/gi, "'") + '"';

        });

        $('.settings-item .neal-shortcode-child').each( function () {
            var shortcode = 'neal_ext_' + $(this).data('ch-shortcode');
                    childShort += '[';
                    childShort += shortcode;

            $('.neal-shortcode-attr', this ).each( function () {
            var $this = $(this),
                value = '';

                // selects
                if ($this.is('select') ) value = $this.find('option:selected').val();
                // other fields
                else value = $this.val();
                // check that value is not empty
                if (value == null) value = '';
                else if (typeof value === 'array') value = value.join(',');
                // add attribute
                if (value !== '') childShort += ' ' + $(this).attr('name') + '="' + $(this).val().toString().replace(/"/gi, "'") + '"';

            });

            childShort += ']';

            childShort += $( '.neal-shortcode-content', this ).val();

            childShort += '[/' + shortcode + ']';
        });

		result += ']';
		result += mce_selection;

        var shortcodeContentVal = $('.settings-item > .neal-shortcode-attr-container > .neal-shortcode-content').val();
        if ( shortcodeContentVal ) {
            result += shortcodeContentVal;
        }

        result += childShort;

		// wrap shortcode if content presented
		result += '[/' + short + ']';
		// return result
		return result;
	}

    $body.on( 'click', '.neal-shortcode-add-item', function () {
        $('.settings-item .neal-setting-items').append( $( this ).data('shortcode-item-html') );
        return false;
    });

    // add images to slider
    $body.on( 'click', '.neal-shortcode-add-slider-image', function ( e ) {
        // prevents default behaviour
        e.preventDefault();

        var sliderImagesIds = $('.neal-shortcode-slider-images-ids');

        var mediaFrame = wp.media({
            title: '',
            multiple: true,
            library: {
                type: 'image'
            },
            button: {
                text: 'Add Images'
            }
        });

        mediaFrame.on('select', function() {
            var selection = mediaFrame.state().get( 'selection' ),
                sliderImagesIdV  = sliderImagesIds.val();

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();

                if ( attachment.id ) {
                    var attachmentImage = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
                    sliderImagesIdV     = sliderImagesIdV ? sliderImagesIdV + ',' + attachment.id : attachment.id;
                    
                    // load image
                    $('.neal-shortcode-slider-images')
                    .append( '<li class="image" data-attachment_id="' + attachment.id + '">' +
                            '<img src="'+ attachmentImage +'" />' +
                            '<a href="#" class="neal-shortcode-slider-img-delete" title="">X</a>' +
                        '</li>');


                }

                // set id
                sliderImagesIds.val( sliderImagesIdV );
            });

        });

        // open popup
        mediaFrame.open();

        return false;
    });

    // remove slider image
    $body.on( 'click', '.neal-shortcode-slider-img-delete', function () {
        $( this ).closest( 'li.image' ).remove();

        var sliderImagesIdV;

        $('.neal-shortcode-slider-images').find( 'li.image' ).css( 'cursor', 'default' ).each( function() {
            var imagesId = $( this ).attr( 'data-attachment_id' );

            sliderImagesIdV = sliderImagesIdV ? sliderImagesIdV + ',' + imagesId : imagesId;
        });

        $('.neal-shortcode-slider-images-ids').val( sliderImagesIdV );

        return false;
    });

    // show categories & tags on widgets
    $('.neal-widget-posts-source').change( function () {
        var form = $( this ).closest('.widget'),
            val  = $( this ).val();
        form.find('.neal-widget-posts-source-category,.neal-widget-posts-source-tag').hide();
        form.find('.neal-widget-posts-source-' + val ).show();
    });

    // show posts carousel on widgets
    $('.show-posts-carousel').click( function () {
        var form = $( this ).closest('.widget');
        form.find('.widget-posts-carousel-settings').toggleClass('open');
    });
    
    
});