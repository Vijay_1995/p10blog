jQuery(function( $ ) {
	"use strict";

	// define main variables
	var $window   = $( window ),
    	$document = $( document ),
    	$body 	  = $('body');

    // Tabs
    $('.neal-tabs').each( function () {
        var tabThis = $( this ),
            activeTab = $( this ).data('active') - 1;
        
        tabThis.find('.tabs-menu li a').eq( activeTab ).addClass('active');
        
        tabThis.find('.tabs-menu li a').on( 'click', function () {
        	var index = $( this ).index('.neal-tabs .tabs-menu li a');

        	if ( ! $( this ).hasClass('active') ) {
    	    	tabThis.find('.tabs-menu li a').removeClass('active');
    	    	$( this ).addClass('active');

    	    	tabThis.find('.tabs-content .tab-content').hide().
    	    	eq( index ).fadeIn();
    	    }

        	return false;
        });
    });

    // Spoilers
    $('.neal-spoiler').each( function () {
        var sThis = $( this );
        
        sThis.find('.spoiler-title').on( 'click', function () {
            var index = $( this ).index('.spoiler-title');

                sThis.find('.spoiler-content').
                eq( index ).stop().slideToggle( 200 );

            return false;
        });
    });

    // Accordions
    $('.neal-accordion').each( function () {
        var aThis = $( this );
        
        aThis.find('ul li h3').on( 'click', function () {
            var index = $( this ).index('.neal-accordion ul li h3');

            if ( ! $( this ).hasClass('active') ) {
                aThis.find('ul li h3').removeClass('active');
                $( this ).addClass('active');

                aThis.find('ul li .accordion-content').stop().slideUp( 200 ).
                eq( index ).slideDown( 300 );
            } else {
                aThis.find('ul li .accordion-content').
                eq( index ).stop().slideUp( 200 );
                $( this ).removeClass('active');
            }

            return false;
        });
    });

    // Sliders
    var nSlider = $('.neal-slider');
    nSlider.each( function () {
        var sThis       = $( this ),
            nSliderCol  = sThis.data('columns'),
            nSliderLoop = sThis.data('loop'),
            nSliderNav  = sThis.data('nav');

        sThis.waitForImages( function() {
            
            sThis.find('.slider-item a').magnificPopup({
                type: 'image',
                gallery:{
                    enabled: true
                },
            });

            sThis.owlCarousel({
                items: nSliderCol,
                loop: nSliderLoop,
                nav: nSliderNav,
                autoHeight: true,
                dots: true,
                navText: [ '<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>' ]
            });
        });
    });

    // Tooltips
    var nTooltip = $('.neal-tooltip');
    nTooltip.each( function () {
        var nTooltipBeh = $( this ).data('behavior'),
            nTooltipSet = {};

        if ( nTooltipBeh == 'always' ) {
            nTooltipSet.showArrow = true;
        }

        $( this ).tipso( nTooltipSet );
    });

});