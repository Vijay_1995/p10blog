<?php
/**
 * Neal Extension Shortcodes Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Ext_Shortcodes' ) ) :

	/**
	 * The Shortcodes class
	 */
	class Neal_Ext_Shortcodes {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */

		protected $tabs_content;

		public function __construct() {
			$shortcodes = array(
				'dropcap',
				'tabs',
				'tab',
				'spoiler',
				'accordion',
				'accordion_item',
				'divider',
				'recipe',
				'recipe_ingredients',
				'recipe_directions',
				'recipe_notes',
				'slider',
				'gmap',
				'youtube',
				'button',
				'quote',
				'row',
				'row_column',
				'box',
				'tooltip',
				'highlight',
			);

			foreach ( $shortcodes as $shortcode ) {
				add_shortcode( 'neal_ext_' . $shortcode, array( $this, $shortcode ) );
			}
		}

		/**
		 * dropcap
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function dropcap( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'font-size'  	=> '',
					'font-weight'	=> '',
					'extra-class'	=> '',
				), $atts
			);

			return sprintf(
				'<span class="dropcap %s" style="font-size:%spx;font-weight:%s">' .
				'%s' .
				'</span>',
				$atts['extra-class'],
				$atts['font-size'],
				$atts['font-weight'],
				$content
			);
		}

		/**
		 * tabs
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function tabs( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'         => '',
					'active'		=> '',
					'vertical'		=> '',
					'extra-class'	=> '',
				), $atts
			);


			$output = array();

			$output[] = do_shortcode( $content );

			$v_class = '';
			if ( $atts['vertical'] == 'yes' ) {
				$v_class = 'vertical';
			}

			return sprintf(
				'<div class="neal-tabs %s %s" data-active="%s">' .
				'<ul class="tabs-menu">' .
				'%s' .
				'</ul>' .
				'<div class="tabs-content">' . 
				'%s' . 
				'</div>' .
				'</div>',
				$atts['extra-class'],
				$v_class,
				$atts['active'],
				implode( '', $output ),
				$this->tabs_content
			);
		}

		/**
		 * tab
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function tab( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'          => '',
				), $atts
			);


			$output = array();

			$output[] = sprintf( '<li><a href="#">%s</a></li>', $atts['title'] );

			$this->tabs_content .= '<div class="tab-content">' . $content . '</div>';

			return sprintf(
				'%s' ,
				implode( '', $output )
			);
		}

		/**
		 * spoiler
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function spoiler( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'  		=> '',
					'extra-class'	=> '',
				), $atts
			);

			return sprintf(
				'<div class="neal-spoiler %s">' .
				'<div class="spoiler-title">' .
				'%s' .
				'</div>' .
				'<div class="spoiler-content">' .
				'%s' .
				'</div>' .
				'</div>',
				$atts['extra-class'],
				$atts['title'],
				$content
			);
		}

		/**
		 * accordion
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function accordion( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'         => '',
					'extra-class'	=> '',

				), $atts
			);


			$output = array();

			$output[] = do_shortcode( $content );

			return sprintf(
				'<div class="neal-accordion %s">' .
				'<ul>' .
				'%s' .
				'</ul>' .
				'</div>',
				$atts['extra-class'],
				implode( '', $output )
			);
		}

		/**
		 * accordion_item
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function accordion_item( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'	=> '',
				), $atts
			);


			$output = array();
			$accordion_content = '';

			$accordion_content .= '<div class="accordion-content">' . $content . '</div>';

			$output[] = sprintf( '<li><h3>%s</h3>%s</li>', $atts['title'], $accordion_content );

			return sprintf(
				'%s' ,
				implode( '', $output )
			);
		}

		/**
		 * divider
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function divider( $atts ) {
			$atts = shortcode_atts(
				array(
					'color'  		=> '',
					'size'			=> '',
					'margin'		=> '',
					'extra-class'	=> '',
				), $atts
			);

			return sprintf(
				'<div class="neal-divider %s" style="background-color:%s;height:%spx;margin:%spx 0;">' .
				'</div>',
				$atts['extra-class'],
				$atts['color'],
				$atts['size'],
				$atts['margin']
			);
		}

		/**
		 * recipe
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function recipe( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'  		=> '',
					'description'	=> '',
					'servings'		=> '',
					'time'			=> '',
					'difficulty' 	=> '',
				), $atts
			);

			$output = array();

			$s_output = array();

			$output[] = sprintf( '<h3 class="recipe-title">%s</h3>', $atts['title'] );

			$recipe_meta = '<li class="recipe-servings"><strong>' . esc_html__( 'Servings:', 'neal' ) . '</strong>' . $atts['servings'] . '</li>';
			$recipe_meta .= '<li class="recipe-time"><strong>' . esc_html__( 'Time:', 'neal' ) . '</strong>' . $atts['time'] . '</li>';
			$recipe_meta .= '<li class="recipe-difficulty"><strong>' . esc_html__( 'Difficulty:', 'neal' ) . '</strong>' . $atts['difficulty'] . '</li>';

			$output[] = sprintf( '<ul class="recipe-meta">%s</ul>', $recipe_meta );
			
			$s_output[] = do_shortcode( $content );

			return sprintf(
				'<div class="neal-recipe">' .
				'%s' .
				'<div class="recipe-content">' .
				'<p>' .
				'%s' .
				'</p>' .
				'%s' .
				'</div>' .
				'</div>',
				implode( '', $output ),
				$atts['description'],
				implode( '', $s_output )
			);
		}

		/**
		 * recipe_ingredients
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function recipe_ingredients( $atts, $content ) {

			$output = array();

			// title
			$output[] = sprintf( '<h4 class="ingredients-title">%s</h4>', esc_html__( 'Ingredients', 'neal' ) );

			$output[] = $content;

			return sprintf(
				'<div class="recipe-ingredients">' .
				'%s' .
				'</div>',
				implode( '', $output )
			);
		}

		/**
		 * recipe_directions
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function recipe_directions( $atts, $content ) {

			$output = array();

			// title
			$output[] = sprintf( '<h4 class="directions-title">%s</h4>', esc_html__( 'Directions', 'neal' ) );

			$output[] = $content;

			return sprintf(
				'<div class="recipe-directions">' .
				'%s' .
				'</div>',
				implode( '', $output )
			);
		}

		/**
		 * recipe_notes
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function recipe_notes( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'  		=> '',
				), $atts
			);

			$output = array();

			$output[] = $content;

			return sprintf(
				'<div class="recipe-notes">' .
				'%s' .
				'</div>',
				implode( '', $output )
			);
		}

		/**
		 * slider
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function slider( $atts ) {
			$atts = shortcode_atts(
				array(
					'images-ids'	=> '',
					'images-size'	=> '',
					'columns'		=> '',
					'loop'			=> '',
					'nav'			=> '',
					'extra-class'	=> '',
				), $atts
			);

			$output = array();

			// slider items
			if ( isset( $atts['images-ids'] ) ) {
				$ids = explode( ',', $atts['images-ids'] );

				foreach ( $ids as $id ) {
					$image 		= wp_get_attachment_image( esc_html( $id ), $atts['images-size'] );
					$image_link = wp_get_attachment_url( $id );
					$output[] = sprintf( '<div class="slider-item"><a href="%s">%s</a></div>', $image_link, $image );
				}
			}

			return sprintf(
				'<div class="neal-slider owl-carousel %s" data-columns="%s" data-loop="%s" data-nav="%s">' .
				'%s' .
				'</div>' ,
				$atts['extra-class'],
				$atts['columns'],
				$atts['loop'],
				$atts['nav'],
				implode( '', $output )
			);
		}

		/**
		 * gmap
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function gmap( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'width'			=> '',
					'height'		=> '',
					'marker'		=> '',
					'extra-class'	=> '',
				), $atts
			);

			return sprintf(
				'<div class="neal-gmap %s">' .
				'<iframe width="%s" height="%s" src="//maps.google.com/maps?q=%s&amp;output=embed"></iframe>' .
				'</div>' ,
				$atts['extra-class'],
				$atts['width'],
				$atts['height'],
				$atts['marker']
			);
		}

		/**
		 * youtube
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function youtube( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'url'			=> '',
					'width'			=> '',
					'height'		=> '',
					'autoplay'		=> '',
					'extra-class'	=> '',
				), $atts
			);

			$id = ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $atts['url'], $match ) ) ? $match[1] : false;

			$autoplay = ( $atts['autoplay'] ) ? '?autoplay=1' : '';

			return sprintf(
				'<div class="neal-youtube %s">' .
				'<iframe width="%s" height="%s" src="https://www.youtube.com/embed/%s%s" frameborder="0" allowfullscreen="true"></iframe>' .
				'</div>' ,
				$atts['extra-class'],
				$atts['width'],
				$atts['height'],
				$id,
				$autoplay
			);
		}

		/**
		 * button
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function button( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'link'				=> '',
					'target'			=> '',
					'background-color'	=> '',
					'text-color'		=> '',
					'size'				=> '',
					'extra-class'		=> '',
				), $atts
			);

			$button = '<a';
				if ( isset( $atts['link'] ) ) {
					$button .= ' href="'. $atts['link'] . '"';
				}

				if ( $atts['target'] == 'new' ) {
					$button .= 'target="_blank"';
				}

			$button .= 'class="neal-button '. $atts['extra-class'] .'" style="background-color:'. $atts['background-color'] .'; color:'. $atts['text-color'] .'; font-size:'. $atts['size'] .'px">';

				$button .= $content;

			$button .= '</a>';

			return sprintf(
				'%s' ,
				$button
			);
		}

		/**
		 * quote
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function quote( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'author-name'	=> '',
					'extra-class'	=> '',
				), $atts
			);

			$output = array();

			if ( isset( $atts['author-name'] ) ) {
				$output[] = sprintf( '<span class="author-name"><span>%s</span></span>', $atts['author-name'] );
			}

			return sprintf(
				'<div class="neal-quote %s">' .
				'%s' .
				'%s' .
				'</div>' ,
				$atts['extra-class'],
				$content,
				implode( '', $output )
			);
		}

		/**
		 * row
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function row( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'size'			=> '',
					'extra-class'	=> '',
				), $atts
			);


			$output = array();

			$output[] = do_shortcode( $content );

			return sprintf(
				'<div class="neal-row column-%s %s">' .
				'%s' . 
				'</div>' ,
				$atts['size'],
				$atts['extra-class'],
				implode( '', $output )
			);
		}

		/**
		 * row_column
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function row_column( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'          => '',
				), $atts
			);


			$output = array();

			return sprintf(
				'<div class="neal-row-column">' .
				'%s' .
				'</div>' ,
				$content
			);
		}

		/**
		 * box
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function box( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'				=> '',
					'color'				=> '',
					'title-text-color'	=> '',
					'border-radius'		=> '',
				), $atts
			);

			return sprintf(
				'<div class="neal-box" style="border-color:%s;border-radius:%spx">' .
				'<span class="neal-box-title" style="background-color:%s;color:%s">' .
				'%s' .
				'</span>' .
				'<div class="neal-box-content">' .
				'%s' .
				'</div>'.
				'</div>',
				$atts['color'],
				$atts['border-radius'],
				$atts['color'],
				$atts['title-text-color'],
				$atts['title'],
				$content
			);
		}

		/**
		 * tooltip
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function tooltip( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'title'  			=> '',
					'position'			=> '',
					'behavior'			=> '',
					'color'				=> '',
					'background-color'	=> '',
					'extra-class'		=> '',
				), $atts
			);

			return sprintf(
				'<span class="neal-tooltip %s" data-tipso="%s" data-tipso-position="%s" data-behavior="%s" data-tipso-color="%s" data-tipso-background="%s">' .
				'%s' .
				'</span>' ,
				$atts['extra-class'],
				$atts['title'],
				$atts['position'],
				$atts['behavior'],
				$atts['color'],
				$atts['background-color'],
				$content
			);
		}

		/**
		 * highlight
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		 */
		function highlight( $atts, $content ) {
			$atts = shortcode_atts(
				array(
					'background-color'  => '',
					'color'				=> '',
					'extra-class'		=> '',
				), $atts
			);

			return sprintf(
				'<span class="neal-highlight %s" style="background-color:%s; color:%s">' .
				'%s' .
				'</span>' ,
				$atts['extra-class'],
				$atts['background-color'],
				$atts['color'],
				$content
			);
		}

	}

endif;

return new Neal_Ext_Shortcodes;