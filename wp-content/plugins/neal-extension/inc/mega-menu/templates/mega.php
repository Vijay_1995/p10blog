<div id="neal-panel-mega" class="neal-panel-mega neal-panel">
	<p class="mr-neal-panel-box">
		<label>
			<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'mega', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.mega ) { print( 'checked="checked"' ); } %> >
			<?php esc_html_e( 'Mega Menu', 'neal' ) ?>
		</label>
	</p>

	<hr>

	<p class="neal-panel-box-large">
		<label>
			<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'mega_full_width', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.mega_full_width ) { print( 'checked="checked"' ); } %> >
			<?php esc_html_e( 'Mega Full Width', 'neal' ) ?>
		</label>
	</p>

	<p class="neal-panel-box-large">
		<label>
			<?php esc_html_e( 'Mega Width', 'neal' ) ?><br>
			<input type="text" name="<%= nealMegaMenu.getFieldName( 'mega_width', data['menu-item-db-id'] ) %>" placeholder="100%" value="<%= megaData.mega_width %>">
		</label>
	</p>

	<div id="neal-mega-content" class="neal-mega-content">
		<%
		var items = _.filter( children, function( item ) {
		return item.subDepth == 0;
		} );
		%>
		<% _.each( items, function( item, index ) { %>

		<div class="neal-submenu-column" data-width="<%= item.megaData.width %>">
			<ul>
				<li class="menu-item menu-item-depth-<%= item.subDepth %>">
					<% if ( item.megaData.icon ) { %>
					<i class="<%= item.megaData.icon %>"></i>
					<% } %>
					<%= item.data['menu-item-title'] %>
					<% if ( item.subDepth == 0 ) { %>
					<span class="neal-column-handle neal-resizable-e"><i class="dashicons dashicons-arrow-left-alt2"></i></span>
					<span class="neal-column-handle neal-resizable-w"><i class="dashicons dashicons-arrow-right-alt2"></i></span>
					<input type="hidden" name="<%= nealMegaMenu.getFieldName( 'width', item.data['menu-item-db-id'] ) %>" value="<%= item.megaData.width %>" class="menu-item-width">
					<% } %>
				</li>
			</ul>
		</div>
		
		<% } ) %>
	</div>
</div>
