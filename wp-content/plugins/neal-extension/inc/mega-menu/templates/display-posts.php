<% var itemId = data['menu-item-db-id']; %>
<% if ( megaData.display_posts.tab_posts ) { var tabClass = 'open' } %>
<div id="neal-panel-display_posts" class="neal-panel-display_posts neal-panel">
	<div class="neal-form">

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.hide', itemId ) %>"><?php esc_html_e( 'Display Posts:', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<label>
					<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'display_posts.hide', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.hide', itemId ) %>" value="1" <% if ( megaData.display_posts.hide ) { print( 'checked' ) } %>><?php esc_html_e( 'Show / Hide', 'neal' ); ?></label>
			</div>
		</div>

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.amount', itemId ) %>"><?php esc_html_e( 'Posts Amount', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<input type="number" name="<%= nealMegaMenu.getFieldName( 'display_posts.amount', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.amount', itemId ) %>" value="<%= megaData.display_posts.amount %>">
			</div>
		</div>

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.sort_order', itemId ) %>"><?php esc_html_e( 'Sort Order', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<select name="<%= nealMegaMenu.getFieldName( 'display_posts.sort_order', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.amount', itemId ) %>">
					<option value="asc" <% if ( 'asc' == megaData.display_posts.sort_order ) { print( 'selected="selected"' ) } %>><?php esc_html_e( 'Ascending (A > Z)', 'neal' ); ?></option>
					<option value="desc" <% if ( 'desc' == megaData.display_posts.sort_order ) { print( 'selected="selected"' ) } %>><?php esc_html_e( 'Descending (Z > A)', 'neal' ); ?></option>
					<option value="random" <% if ( 'random' == megaData.display_posts.sort_order ) { print( 'selected="selected"' ) } %>><?php esc_html_e( 'Random', 'neal' ); ?></option>
				</select>
			</div>
		</div>

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.post_ids', itemId ) %>"><?php esc_html_e( 'Post IDs:', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<input type="text" name="<%= nealMegaMenu.getFieldName( 'display_posts.post_ids', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.post_ids', itemId ) %>" value="<%= megaData.display_posts.post_ids %>">
				<p class="form-description"><?php esc_html_e( 'If you have a post that you particularly want to bring up among the first ones, you can add it by typing the post’s ID (Example: 55,22,33)', 'neal' ); ?></p>
			</div>
		</div>

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label><?php esc_html_e( 'Post Meta', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<label style="margin-right:20px;"><input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'display_posts.meta_category', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.meta_category', itemId ) %>" value="1" <% if ( megaData.display_posts.meta_category ) { print( 'checked' ) } %>><?php esc_html_e( 'Category', 'neal' );?></label>
				<label><input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'display_posts.meta_date', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.meta_date', itemId ) %>" value="1" <% if ( megaData.display_posts.meta_date ) { print( 'checked' ) } %>><?php esc_html_e( 'Date', 'neal' );?></label>
			</div>
		</div>

		<div class="neal-form-field">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.tab_posts', itemId ) %>"><?php esc_html_e( 'Tab Posts:', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<input type="checkbox" class="show-tab-categories" name="<%= nealMegaMenu.getFieldName( 'display_posts.tab_posts', itemId ) %>" id="<%= nealMegaMenu.getFieldName( 'display_posts.tab_posts', itemId ) %>" value="1" <% if ( megaData.display_posts.tab_posts ) { print( 'checked' ) } %>>
			</div>
		</div>

		<div class="neal-form-field tab-posts-categories <%= tabClass %>">
			<div class="neal-form-left-col">
				<label for="<%= nealMegaMenu.getFieldName( 'display_posts.tab_categories', itemId ) %>"><?php esc_html_e( 'Select Categories:', 'neal' ) ?></label>
			</div>

			<div class="neal-form-right-col">
				<select multiple="multiple" data-tab-cats="<%= megaData.display_posts.tab_categories %>" name="<%= nealMegaMenu.getFieldName( 'display_posts.tab_categories', itemId ) %>[]" id="<%= nealMegaMenu.getFieldName( 'display_posts.tab_categories', itemId ) %>">
					<?php echo neal_get_categories(); ?>
				</select>
			</div>
		</div>
	</div>
</div>