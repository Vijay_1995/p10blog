<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'General', 'neal' ) ?>" data-panel="general"><?php esc_html_e( 'General', 'neal' ) ?></a>

<% if ( depth == 0 ) { %>
	<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Mega Menu Settings', 'neal' ) ?>" data-panel="mega"><?php esc_html_e( 'Mega Menu Settings', 'neal' ) ?></a>
	<% if ( isCategory ) { %>
		<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Display Posts', 'neal' ) ?>" data-panel="display_posts"><?php esc_html_e( 'Display Posts', 'neal' ) ?></a>
	<% } %>
	<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Mega Menu Background', 'neal' ) ?>" data-panel="background"><?php esc_html_e( 'Background', 'neal' ) ?></a>
	<div class="separator"></div>
<% } %>