<?php
global $wp_widget_factory;
?>
<div id="neal-panel-general" class="neal-panel-general neal-panel">
	<% if ( depth != 0 ) { %>
		<div class="neal-panel-box">
			<p>
				<label>
					<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'hideText', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.hideText ) { print( 'checked="checked"' ); } %> >
					<?php esc_html_e( 'Hide Text', 'neal' ) ?>
				</label>
			</p>
		</div>
	<% } %>

	<div class="neal-panel-box">
		<p>
			<label>
				<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'hot', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.hot ) { print( 'checked="checked"' ); } %> >
				<?php esc_html_e( 'Hot', 'neal' ) ?>
			</label>
		</p>

		<p>
			<label>
				<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'new', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.new ) { print( 'checked="checked"' ); } %> >
				<?php esc_html_e( 'New', 'neal' ) ?>
			</label>
		</p>

		<p>
			<label>
				<input type="checkbox" name="<%= nealMegaMenu.getFieldName( 'trending', data['menu-item-db-id'] ) %>" value="1" <% if ( megaData.trending ) { print( 'checked="checked"' ); } %> >
				<?php esc_html_e( 'Trending', 'neal' ) ?>
			</label>
		</p>
	</div>
</div>