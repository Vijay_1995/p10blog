<?php
/**
 * Init Mega Menu Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Mega_Menu' ) ) :

	class Neal_Mega_Menu {
		/**
		 * Setup class.
		 *
		 * @since 1.0
		*/
		public function __construct() {

			$this->load();
			$this->init();

			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'edit_nav_menu_walker' ) );

		}

		/**
		 * Load files
		 */
		private function load() {
			include NEAL_EXT_DIR . '/inc/mega-menu/class-neal-menu-edit.php';
		}

		/**
		 * Initialize
		 */
		private function init() {
			if ( is_admin() ) {
				new Neal_Mega_Menu_Edit();
			}
		}

		/**
		 * Change the default nav menu walker
		 *
		 * @return string
		 */
		public function edit_nav_menu_walker() {
			return 'Neal_Mega_Menu_Walker_Edit';
		}

		/**
		 * Display Posts
		 *
		 * @param array  $atts
		 * @param string $content
		 *
		 * @return string
		*/
		public static function mega_display_posts( $arg, $tab_posts = false ) {

			$defaults = array(
				'amount'		=> 4,
				'sort_order'	=> 'desc',
				'post_ids'		=> '',
				'meta_category'	=> 'yes',
				'meta_date'		=> 'yes',
				'cat_id'		=> '',
				'tab_categories' => array(),
			);

			$arg = wp_parse_args( $arg, $defaults );

			$cols = 12 / $arg['amount'];

			$output = array();

			if ( ! $tab_posts ) {

				$args = array(
					'post_type'			=> 'post',
					'post_status' 		=> 'publish',
					'posts_per_page' 	=> intval( $arg['amount'] ),
					'order'   			=> esc_html( $arg['sort_order']),
					'post__not_in' 		=> get_option( 'sticky_posts' ),
					'cat'				=> array( $arg['cat_id'] )
				);

				// post ids
				if ( $arg['post_ids'] ) {
					$ids = explode( ',', esc_html( $arg['post_ids'] ) );
					$args['post__in'] = $ids;
				}

				$wp_query = new WP_Query( $args ); 
				
				if ( $wp_query->have_posts() ) : 
					while ( $wp_query->have_posts() ) : $wp_query->the_post();

						$output[] = sprintf( '<div class="mega-post col-xs-%s"><div class="post-inner">', $cols );
							$output[] = sprintf( '<div class="entry-media"><a href="%s">%s</a></div>', get_the_permalink(),get_the_post_thumbnail( get_the_ID(), 'neal-mega-posts-image' ) );

							// meta category
							if ( $arg['meta_category'] ) {
								$output[] = sprintf( '%s', neal_get_post_categories() );
							}

							// meta date
							if ( $arg['meta_date'] ) {
								$output[] = sprintf( '%s', neal_get_post_date() );
							}

							$output[] = sprintf( '<h3><a href="%s">%s</a></h3>', get_the_permalink(), get_the_title() );
							$output[] = sprintf( '<a href="%s" class="post-link"></a>', esc_url( get_the_permalink() ) );
						$output[] = sprintf( '</div></div>' );

					endwhile;
					wp_reset_postdata();
				endif;

			} else {

				$categories = array_map( 'intval', $arg['tab_categories'] );

				$args = array(
					'post_type'			=> 'post',
					'post_status' 		=> 'publish',
					'posts_per_page' 	=> intval( $arg['amount'] ),
					'order'   			=> esc_html( $arg['sort_order']),
					'post__not_in' 		=> get_option( 'sticky_posts' ),
				);

				$tabs_tab = '';
				$tabs_content = '';

				foreach ( $categories as $id ) {
					$tabs_tab .= '<li>' .get_cat_name( $id ). '</li>';

					$args['cat'] = $id;
					$wp_query = new WP_Query( $args );

					$tabs_content .= '<div class="tabs-post-content row">';

					if ( $wp_query->have_posts() ) : 
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							$tabs_content .= '<div class="mega-post col-xs-'.$cols.'">';
								$tabs_content .= '<div class="post-inner">';

									$tabs_content .= '<div class="entry-media"><a href="'. get_the_permalink() .'">';
										$tabs_content .= get_the_post_thumbnail( get_the_ID(), 'neal-mega-posts-image' );
									$tabs_content .= '</a></div>';

									// meta category
									if ( $arg['meta_category'] ) {
										$tabs_content .= neal_get_post_categories();
									}

									// meta date
									if ( $arg['meta_category'] ) {
										$tabs_content .= neal_get_post_date();
									}

									$tabs_content .= '<h3><a href="'. get_the_permalink() .'">'. get_the_title() .'</a></h3>';
									$tabs_content .= '<a href="'. esc_url( get_the_permalink() ) .'" class="post-link"></a>';
								$tabs_content .= '</div>';
							$tabs_content .= '</div>';
						endwhile;
						wp_reset_postdata();
					endif;

					$tabs_content .= '</div>';
				}

				$output[] = sprintf( '<div class="post-tabs row">' );
					$output[] = sprintf( '<ul class="post-tab col-xs-2">%s</ul>', $tabs_tab );
					$output[] = sprintf( '<div class="post-content col-xs-10">%s</div>', $tabs_content );
				$output[] = sprintf( '</div>' );


			}

			return sprintf(
				'<div class="mega-posts">' .
				'%s' .
				'</div>' ,
				implode( '', $output )
			);
		}
	}

endif;

new Neal_Mega_Menu;