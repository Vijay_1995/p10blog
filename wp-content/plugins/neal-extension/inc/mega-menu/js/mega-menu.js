var nealMegaMenu;

jQuery(function( $ ) {
	"use strict";

	var api;

	api = nealMegaMenu = {
		init: function () {
			api.$body = $('body');
			api.$modal = $('#neal-settings');
			api.itemData = {};
			api.templates = {
				menus     : _.template($('#neal-tmpl-menus').html()),
				title     : _.template($('#neal-tmpl-title').html()),
				mega      : _.template($('#neal-tmpl-mega').html()),
				display_posts: _.template($('#neal-tmpl-display-posts').html()),
				background: _.template($('#neal-tmpl-background').html()),
				general   : _.template($('#neal-tmpl-general').html())
			};

			api.frame = wp.media({
				library: {
					type: 'image'
				}
			});

			this.initActions();

		},

		initActions: function () {
			api.$body
				.on('click', '.opensettings', this.openModal)
				.on('click', '.neal-modal-backdrop, .neal-modal-close, .neal-button-cancel', this.closeModal);

			api.$modal
				.on('click', '.neal-menu a', this.switchPanel)
				.on('click', '.neal-column-handle', this.resizeMegaColumn)
				.on('click', '.neal-button-save', this.saveChanges);
		},

		openModal: function () {
			api.getItemData(this);

			api.$modal.show();
			api.$body.addClass('modal-open');
			api.render();

			return false;
		},

		closeModal: function () {
			api.$modal.hide().find('.neal-content').html('');
			api.$body.removeClass('modal-open');
			return false;
		},

		switchPanel: function (e) {
			e.preventDefault();

			var $el = $(this),
				panel = $el.data('panel');

			$el.addClass('active').siblings('.active').removeClass('active');
			api.openSettings(panel);
		},

		render: function () {
			// Render menu
			api.$modal.find('.neal-frame-menu .neal-menu').html(api.templates.menus(api.itemData));

			var $activeMenu = api.$modal.find('.neal-menu a.active');

			// Render content
			this.openSettings($activeMenu.data('panel'));
		},

		openSettings: function (panel) {
			var $content = api.$modal.find('.neal-frame-content .neal-content'),
				$panel = $content.children('#neal-panel-' + panel);

			if ($panel.length) {
				$panel.addClass('active').siblings().removeClass('active');
			} else {
				$content.append(api.templates[panel](api.itemData));
				$content.children('#neal-panel-' + panel).addClass('active').siblings().removeClass('active');

				if ('mega' == panel) {
					api.initMegaColumns();
				}
				if ('background' == panel) {
					api.initBackgroundFields();
				}

				if ( 'display_posts' == panel ) {
					api.initDisplayPosts();
				}
			}

			// Render title
			var title = api.$modal.find('.neal-frame-menu .neal-menu a[data-panel=' + panel + ']').data('title');
			api.$modal.find('.neal-frame-title').html(api.templates.title({title: title}));
		},

		resizeMegaColumn: function (e) {
			e.preventDefault();

			var steps = ['25.00%', '33.33%', '50.00%', '66.66%', '75.00%', '100.00%'],
				$el = $(this),
				$column = $el.closest('.neal-submenu-column'),
				width = $column.data('width'),
				current = _.indexOf(steps, width),
				next;

			if (-1 === current) {
				return;
			}

			if ($el.hasClass('neal-resizable-w')) {
				next = current == steps.length ? current : current + 1;
			} else {
				next = current == 0 ? current : current - 1;
			}

			$column[0].style.width = steps[next];
			$column.data('width', steps[next]);
			$column.find('.menu-item-depth-0 .menu-item-width').val(steps[next]);
		},

		initMegaColumns: function () {
			var $columns = api.$modal.find('#neal-panel-mega .neal-submenu-column'),
				defaultWidth = '25.00%';

			if (!$columns.length) {
				return;
			}

			// Support maximum 4 columns
			if ($columns.length < 4) {
				defaultWidth = String(( 100 / $columns.length ).toFixed(2)) + '%';
			}

			_.each($columns, function (column) {
				var width = column.dataset.width;

				width = width || defaultWidth;

				column.style.width = width;
				column.dataset.width = width;
				$(column).find('.menu-item-depth-0 .menu-item-width').val(width);
			});
		},

		initDisplayPosts: function () {
			var tabCatIds = api.$modal.find('.tab-posts-categories select').data('tab-cats').split(',');

			api.$modal.find('.tab-posts-categories select option').each( function () {
				if ( tabCatIds.indexOf( $( this ).val() ) != -1 ) {
					$( this ).attr( 'selected', 'selected' );
				}
			});

			api.$modal.find('.show-tab-categories').each( function () {
				$( this ).change( function () {
					var closest = $( this ).closest('.neal-form');
					closest.find('.tab-posts-categories').toggleClass('open');
				});
			});
					
		},

		initBackgroundFields: function () {
			api.$modal.find('.background-color-picker').wpColorPicker();

			// Background image
			api.$modal.on('click', '.background-image .upload-button', function (e) {
				e.preventDefault();

				var $el = $(this);

				// Remove all attached 'select' event
				api.frame.off('select');

				// Update inputs when select image
				api.frame.on('select', function () {
					// Update input value for single image selection
					var url = api.frame.state().get('selection').first().toJSON().url;

					$el.siblings('.background-image-preview').html('<img src="' + url + '">');
					$el.siblings('input').val(url);
					$el.siblings('.remove-button').removeClass('hidden');
				});

				api.frame.open();
			}).on('click', '.background-image .remove-button', function (e) {
				e.preventDefault();

				var $el = $(this);

				$el.siblings('.background-image-preview').html('');
				$el.siblings('input').val('');
				$el.addClass('hidden');
			});

			// Background position
			api.$modal.on('change', '.background-position select', function () {
				var $el = $(this);

				if ('custom' == $el.val()) {
					$el.next('input').removeClass('hidden');
				} else {
					$el.next('input').addClass('hidden');
				}
			});
		},

		getItemData: function (menuItem) {
			var $menuItem = $(menuItem).closest('li.menu-item'),
				$menuData = $menuItem.find('.neal-data'),
				children = $menuItem.childMenuItems();

			api.itemData = {
				depth          : $menuItem.menuItemDepth(),
				isCategory	   : $menuItem.hasClass('menu-item-category') ? true : false,
				megaData       : {
					mega         : $menuData.data('mega'),
					mega_full_width   : $menuData.data('mega_full_width'),
					mega_width   : $menuData.data('mega_width'),
					width        : $menuData.data('width'),
					display_posts: $menuData.data('display_posts'),
					background   : $menuData.data('background'),
					hideText     : $menuData.data('hide-text'),
					hot          : $menuData.data('hot'),
					new          : $menuData.data('new'),
					trending     : $menuData.data('trending'),
					disableLink  : $menuData.data('disable-link'),
				},
				data           : $menuItem.getItemData(),
				children       : [],
				originalElement: $menuItem.get(0)
			};

			if (!_.isEmpty(children)) {
				_.each(children, function (item) {
					var $item = $(item),
						$itemData = $item.find('.neal-data'),
						depth = $item.menuItemDepth();

					api.itemData.children.push({
						depth          : depth,
						subDepth       : depth - api.itemData.depth - 1,
						data           : $item.getItemData(),
						megaData       : {
							mega         : $itemData.data('mega'),
							mega_full_width   : $itemData.data('mega_full_width'),
							mega_width   : $itemData.data('mega_width'),
							width        : $itemData.data('width'),
							display_posts: $itemData.data('display_posts'),
							background   : $itemData.data('background'),
							hideText     : $itemData.data('hide-text'),
							hot          : $itemData.data('hot'),
							new          : $itemData.data('new'),
							trending     : $itemData.data('trending'),
							disableLink  : $itemData.data('disable-link'),
						},
						originalElement: item
					});
				});
			}

		},

		setItemData: function (item, data, depth) {
			if (!_.has(data, 'mega')) {
				data.mega = false;
			}


			if (depth == 0) {
				if (!_.has(data, 'hideText')) {
					data.hideText = false;
				}

				if (!_.has(data, 'hot')) {
					data.hot = false;
				}

				if (!_.has(data, 'trending')) {
					data.trending = false;
				}

				if (!_.has(data, 'new')) {
					data.new = false;
				}
			}

			var $dataHolder = $(item).find('.neal-data');

			if (_.has(data, 'content')) {
				$dataHolder.html(data.content);
				delete data.content;
			}

			$dataHolder.data(data);

		},

		getFieldName: function (name, id) {
			name = name.split('.');
			name = '[' + name.join('][') + ']';

			return 'menu-item[' + id + ']' + name;
		},

		saveChanges: function () {
			var data = api.$modal.find('.neal-content :input').serialize(),
				$spinner = api.$modal.find('.neal-toolbar .spinner');

			$spinner.addClass('is-active');

			$.post(ajaxurl, {
				action: 'neal_save_menu_item_data',
				data  : data
			}, function (res) {
				if (!res.success) {
					return;
				}


				var data = res.data['menu-item'];

				// Update parent menu item
				if (_.has(data, api.itemData.data['menu-item-db-id'])) {
					api.setItemData(api.itemData.originalElement, data[api.itemData.data['menu-item-db-id']], 0);
				}

				_.each(api.itemData.children, function (menuItem) {
					if (!_.has(data, menuItem.data['menu-item-db-id'])) {
						return;
					}

					api.setItemData(menuItem.originalElement, data[menuItem.data['menu-item-db-id']], 1);
				});

				$spinner.removeClass('is-active');
				api.closeModal();
			});
		}
	};

	$(function () {
		nealMegaMenu.init();
	});
});