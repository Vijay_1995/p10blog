<?php
/**
 * Neal Extension Main Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Ext_Main' ) ) :

	/**
	 * The Main class
	 */
	class Neal_Ext_Main {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 */
		public function __construct() {
			add_action( 'admin_enqueue_scripts',    array( $this, 'scripts' ),       		10 );
			add_action( 'wp_enqueue_scripts',       array( $this, 'frontend_scripts' ),     10 );

			// add custom meta field to categories taxonomy
			add_action( 'category_add_form_fields', 	array( $this, 'taxonomy_add_new_meta_field' ), 10, 2 );
			add_action( 'genres_add_form_fields', 		array( $this, 'taxonomy_add_new_meta_field' ), 10, 2 );

			add_action( 'category_edit_form_fields', 	array( $this, 'taxonomy_edit_meta_field' ), 10, 2 );

			add_action( 'edited_category',				array( $this, 'save_taxonomy_custom_meta' ), 10, 2 );  
			add_action( 'create_category', 				array( $this, 'save_taxonomy_custom_meta' ), 10, 2 );

			add_action( 'create_term-thumbnail-wrap', 	array( $this, 'save_taxonomy_custom_meta' ) );
			add_action( 'edited_term-thumbnail-wrap', 	array( $this, 'save_taxonomy_custom_meta' ) );

			// list custom field on categories taxonomy
			add_filter('manage_edit-category_columns' , 	array( $this, 'cat_image_taxonomy_columns' ) );

			add_filter( 'manage_category_custom_column', 	array( $this, 'cat_image_taxonomy_columns_content' ), 10, 3 );

			// add button to media buttons
			add_action( 'media_buttons', 			array( $this, 'shortcode_insert_button' ) );

			// user profile
			add_action( 'show_user_profile',		array( $this, 'extra_user_profile_fields' ) );
			add_action( 'edit_user_profile', 		array( $this, 'extra_user_profile_fields' ) );

			add_action( 'personal_options_update', 	array( $this, 'save_extra_user_profile_fields' ) );
			add_action( 'edit_user_profile_update', array( $this, 'save_extra_user_profile_fields' ) );


		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since  1.0.0
		 */
		public function scripts() {

			/**
			 * Styles
			 */
			wp_register_style( 'neal-ext-admin-style', NEAL_EXT_URL . 'css/admin.css', array(), '' );

			wp_enqueue_style( 'neal-ext-admin-style' );

			/**
			 * Scripts
			 */

			// add wp_media
			wp_enqueue_media();

			// Add the color picker css file       
        	wp_enqueue_style( 'wp-color-picker' ); 

			wp_register_script( 'neal-ext-media-upload-js', NEAL_EXT_URL . 'js/neal-ext-media-upload.js', array(), false, true );
			wp_enqueue_script( 'neal-ext-media-upload-js' );


			// custom text labels in Wp Media Uploader - ready for translation
			$neal_localize_uploader = array(
				'mediaTitle' 	=> esc_html__( 'Add Media', 'neal' ),
				'mediaButton' 	=> esc_html__( 'Add Media', 'neal' )
			);

			wp_localize_script( 'neal-ext-media-upload-js', 'nealUploader', $neal_localize_uploader );

			wp_register_script( 'neal-ext-script', NEAL_EXT_URL . 'js/script.js', array(
					'jquery',
					'wp-color-picker'
			), '', true );

			wp_enqueue_script( 'neal-ext-script' );

			// tinymce editor
			wp_enqueue_script( 'tinymce_js', includes_url( 'js/tinymce/' ) . 'wp-tinymce.php', array( 'jquery' ), false, true );
		}

		/**
		 * Enqueue scripts and styles(Frontend)
		 *
		 * @since  1.0.0
		 */
		public function frontend_scripts() {

			/**
			 * Styles
			 */

			wp_register_style( 'tipso-style', NEAL_EXT_URL . 'css/tipso.min.css', array(), '' );

			wp_enqueue_style( 'tipso-style' );

			wp_register_style( 'neal-ext-frontend-style', NEAL_EXT_URL . 'css/frontend.css', array(), '' );

			wp_enqueue_style( 'neal-ext-frontend-style' );

			// tooltip
			wp_register_script( 'tipso-js', NEAL_EXT_URL . 'js/tipso.min.js', array(), false, true );

			wp_register_script( 'neal-ext-shortcodes-frontend', NEAL_EXT_URL . 'js/neal-shortcodes-frontend.js', array(
					'jquery',
					'tipso-js'
			), '', true );

			wp_enqueue_script( 'neal-ext-shortcodes-frontend' );
		}

		/**
		 * Add custom meta field to categories taxonomy
		 *
		 * @since  1.0.0
		 */
		public function taxonomy_add_new_meta_field() {
			// this will add the custom meta field to the add new term page
			?>
			<div class="form-field">
				<label for="featured-category"><?php esc_html_e( 'Featured Category', 'neal' ); ?></label><input type="checkbox" id="featured-category" name="neal_featured_category">
				<p class="description"><?php esc_html_e( 'You can choose this box if you want to featured the category.', 'neal' ); ?></p>
			</div>

			<div class="form-field">
				<label for="neal_cat_thumbnail_i"><?php esc_html_e( 'Thumbnail', 'neal' ); ?></label>
				<div id="neal-cat-thumbnail" style="float: left; margin-right: 10px;"><img src="" width="60px" height="60px"></div>
				<div>
					<button type="button" class="neal-cat-image-upload button"><?php esc_html_e( 'Upload/Add image', 'neal'); ?></button>
					<input type="hidden" id="neal_cat_thumbnail_id" name="neal_cat_thumbnail_id" class="neal-cat-thumb-id">
				</div>
				<div class="clear"></div>
			</div>
		<?php
		}

		// Edit term page
		public function taxonomy_edit_meta_field( $term ) {
		 
			// put the term ID into a variable
			$t_id = $term->term_id;
		 
			// retrieve the existing value(s) for this meta field. This returns an array
			$term_meta = get_option( "taxonomy_$t_id" );

			$featured = isset( $term_meta['neal_featured_category'] ) ? esc_attr( $term_meta['neal_featured_category'] ) : '';

			$checked_featured = ( ( isset ( $featured ) ) ? checked( $featured, 'on', false ) : '' );

			$image_id = isset( $term_meta['neal_cat_thumbnail_id'] ) ? esc_attr( $term_meta['neal_cat_thumbnail_id'] ) : '';

			$get_image = wp_get_attachment_image_src( $image_id );

			?>

			<tr class="form-field">
			<th scope="row" valign="top"><label for="featured-category"><?php esc_html_e( 'Featured Category', 'neal' ); ?></label></th>
				<td>
					<input type="checkbox" id="featured-category" name="neal_featured_category" value="on" <?php echo $checked_featured; ?>>
					<p class="description"><?php esc_html_e( 'You can choose this box if you want to featured the category.', 'neal' ); ?></p>
				</td>
			</tr>

			<tr class="form-field">
			<th scope="row" valign="top"><label for="neal_cat_thumbnail_id"><?php esc_html_e( 'Thumbnail', 'neal' ); ?></label></th>
				<td>
					<div id="neal-cat-thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo $get_image[0]; ?>" width="60px" height="60px"></div>

					<button type="button" class="neal-cat-image-upload button"><?php esc_html_e( 'Upload/Add image', 'neal'); ?></button>
					<input type="hidden" id="neal_cat_thumbnail_id" name="neal_cat_thumbnail_id" class="neal-cat-thumb-id" value="<?php echo esc_attr( $image_id ); ?>">
				</td>
			</tr>
		<?php
		}

		// Save extra taxonomy fields callback function.
		public function save_taxonomy_custom_meta( $term_id ) {
			$t_id = $term_id;
			$term_meta = get_option( "taxonomy_$t_id" );

			// thumb
			if ( isset( $_POST['neal_cat_thumbnail_id'] ) ) {
				$id = $_POST['neal_cat_thumbnail_id'];
				$term_meta['neal_cat_thumbnail_id'] = $id;
				update_option( "taxonomy_$t_id", $term_meta );
			}
			
			// featured
			if ( isset( $_POST['neal_featured_category'] ) ) {
				$id = $_POST['neal_featured_category'];
				$term_meta['neal_featured_category'] = $id;
				update_option( "taxonomy_$t_id", $term_meta );
			} else {
				$term_meta['neal_featured_category'] = '';
				update_option( "taxonomy_$t_id", $term_meta );
			}

		}

		public function cat_image_taxonomy_columns( $columns ) {
			$columns['thumb'] = esc_html__( 'Image', 'neal' );

			return $columns;
		}

		public function cat_image_taxonomy_columns_content( $content, $column_name, $term_id ) {
		    if ( 'thumb' == $column_name ) {
		    	$term_meta = get_option( "taxonomy_$term_id" );
		    	$img_id = isset( $term_meta['neal_cat_thumbnail_id'] ) ? $term_meta['neal_cat_thumbnail_id'] : '';
		        $content = wp_get_attachment_image( $img_id );
		    }

			return $content;
		}

		public function shortcode_insert_button() {
		    echo '<a href="#" class="neal-shortcode-insert button" data-url="' . NEAL_EXT_URL . '">' . esc_html__( 'Neal Shortcodes', 'neal') . '</a>';
		}

		public function extra_user_profile_fields( $user ) { ?>
			<h3><?php esc_html_e( 'Social Media', 'neal' ); ?></h3>

			<table class="form-table">
				<tr>
					<th><label for="facebook"><?php esc_html_e( 'Facebook' ); ?></label></th>
					<td>
						<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="twitter"><?php esc_html_e( 'Twitter' ); ?></label></th>
					<td>
						<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="instagram"><?php esc_html_e( 'Instagram' ); ?></label></th>
					<td>
						<input type="text" name="instagram" id="instagram" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="googlep"><?php esc_html_e( 'Google Plus' ); ?></label></th>
					<td>
						<input type="text" name="googlep" id="googlep" value="<?php echo esc_attr( get_the_author_meta( 'googlep', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="youtube"><?php esc_html_e( 'YouTube' ); ?></label></th>
					<td>
						<input type="text" name="youtube" id="postalcode" value="<?php echo esc_attr( get_the_author_meta( 'youtube', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="pinterest"><?php esc_html_e( 'Pinterest' ); ?></label></th>
					<td>
						<input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr( get_the_author_meta( 'pinterest', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

				<tr>
					<th><label for="linkedin"><?php esc_html_e( 'Linkedin' ); ?></label></th>
					<td>
						<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" />
					</td>
				</tr>

			</table>
		<?php }

		public function save_extra_user_profile_fields( $user_id ) {

			if ( ! current_user_can( 'edit_user', $user_id ) ) { 
				return false; 
			}

			update_user_meta( $user_id, 'facebook',  esc_url_raw( $_POST['facebook'] ) );
			update_user_meta( $user_id, 'twitter', 	 esc_url_raw( $_POST['twitter'] ) );
			update_user_meta( $user_id, 'instagram', esc_url_raw( $_POST['instagram'] ) );
			update_user_meta( $user_id, 'googlep',   esc_url_raw( $_POST['googlep'] ) );
			update_user_meta( $user_id, 'youtube',   esc_url_raw( $_POST['youtube'] ) );
			update_user_meta( $user_id, 'pinterest', esc_url_raw( $_POST['pinterest'] ) );
			update_user_meta( $user_id, 'linkedin',  esc_url_raw( $_POST['linkedin'] ) );
		}
	}

endif;

return new Neal_Ext_Main();