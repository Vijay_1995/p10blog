jQuery( document ).ready( function( $ ) {
	"use strict";

	$('body').on( 'click', '.neal-custom-font-add-file', function ( event ) {
		var field = $( this ).data('field');
		$('.downloadable_files tbody').append( field );

		return false;
	});

	$('body').on( 'change', 'input.upload-custom-font', function(){
		var file = $( this ),
			uploadDir = $('.downloadable_files').data('upload-dir'),
			uploadFile = uploadDir + "/custom-fonts/" + file[0].files[0].name,
			index = file.index('input.uploadImage'),
			fileName = file[0].files[0].name.split('.');

		$('.neal-cf-font-name').eq( index ).val( fileName[0] );
		$('.neal-cf-file-url').eq( index ).val( uploadFile );
	});

	$('body').on( 'click', '.upload_file_button', function ( e ) {
		var btnThis = $( this ),
			index = btnThis.index('.downloadable_files tbody .upload_file_button');

		$('.upload-custom-font').eq( index ).click();
		
		return false;
	});

	$('body').on( 'click', '.neal-custom-font-delete', function ( event ) {
		var index = $( this ).index('.neal-custom-font-delete'),
			file_url = $('.neal-cf-file-url').eq( index ).val(),
			val = $('.neal-cf-delete').val();

		$('.neal-cf-delete').val( val != 'false' ? val + ',' + file_url :file_url );
		$( this ).closest('.downloadable_files tbody tr').remove();
		return false;
	});

});