<?php
/**
 * Neal Extension Custom fonts.
 *
 * @package neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Custom_Fonts' ) ) :

	/**
	 * The Main demo importer class.
	 */
	class Neal_Custom_Fonts {

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 *
		 * @var array
		 */

		public function __construct() {

			if ( ! get_option( 'neal-custom-fonts' ) ) {
				add_option( 'neal-custom-fonts', '' );
			}

			add_action( 'admin_menu', array( $this, 'register_menu' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );

			$this->custom_fonts_upload();
		}

		/**
		 * Register menu
		 *
		 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/admin_menu
		 */
		public function register_menu() {

			add_theme_page(
				esc_html__( 'Custom Fonts', 'neal' ),
				esc_html__( 'Custom Fonts', 'neal' ),
				'edit_theme_options',
				'neal-custom-fonts',
				array( $this, 'custom_fonts' )
			);
		}

		/**
		 * Load scripts
		 */
		public function scripts( $hook ) {
			if ( 'appearance_page_neal-custom-fonts' != $hook ) {
				return;
			}

	        // script
	        wp_register_script( 'neal-ext-custom-fonts', NEAL_EXT_URL . 'inc/custom-fonts/js/neal-ext-custom-fonts.js', array(
					'jquery',
			), '', true );
	        wp_enqueue_script( 'neal-ext-custom-fonts' );
		}

		/**
		 * Custom fonts frontend page
		 */
		public function custom_fonts() {
			$wp_upload_dir = wp_upload_dir();
		?>
			<div class="wrap">
				<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
				<div class="form-field downloadable_files" data-upload-dir="<?php echo $wp_upload_dir['baseurl']; ?>">
					<form action="<?php echo esc_url( get_permalink() ); ?>" method="post" enctype="multipart/form-data">
					<table class="widefat">
						<thead>
							<tr>
								<th><?php esc_html_e( 'Name', 'neal' ); ?></th>
								<th><?php esc_html_e( 'File URL', 'neal' ); ?></th>
								<th colspan="2"><?php esc_html_e( 'Accepted Formats', 'neal' ); ?></th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php if ( ! get_option( 'neal-custom-fonts' ) ) : ?>
							<tr>
								<td class="file_name">
									<input type="text" class="input_text neal-cf-font-name" placeholder="<?php esc_attr_e( 'Font name', 'neal' ); ?>" name="neal-cf-names[]" value="">
								</td>
								<td class="file_url">
									<input type="text" class="input_text neal-cf-file-url" placeholder="http://" name="neal-cf-urls[]" value="">
								</td>
								<td class="file_formats">
									<p><strong>.ttf, .otf</strong> <?php esc_html_e( 'and', 'neal' ); ?> <strong>.woff</strong></p>
								</td>
								<td class="file_url_choose" width="1%">
									<a href="#" class="button upload_file_button" data-choose="<?php esc_attr_e( 'Choose file', 'neal' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', 'neal' ); ?>"><?php esc_html_e( 'Choose file', 'neal' ); ?></a>
									<input class="upload-custom-font" type="file" name="font_file[]" accept=".ttf,.otf,.woff" style="display: none;">
								</td>
								<td width="1%"><a href="#" class="neal-custom-font-delete"><?php esc_html_e( 'Delete', 'neal' ); ?></a></td>
							</tr>
							<?php else: ?>
							<?php foreach ( get_option( 'neal-custom-fonts' ) as $font ) : ?>
							<tr>
								<td class="file_name">
									<input type="text" class="input_text neal-cf-font-name" placeholder="<?php esc_attr_e( 'Font name', 'neal' ); ?>" name="neal-cf-names[]" value="<?php echo $font['name']; ?>">
								</td>
								<td class="file_url">
									<input type="text" class="input_text neal-cf-file-url" placeholder="http://" name="neal-cf-urls[]" value="<?php echo $font['url']; ?>">
								</td>
								<td class="file_formats">
									<p><strong>.ttf, .otf</strong> <?php esc_html_e( 'and', 'neal' ); ?><strong>.woff</strong></p>
								</td>
								<td class="file_url_choose" width="1%">
									<a href="#" class="button upload_file_button" data-choose="<?php esc_attr_e( 'Choose file', 'neal' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', 'neal' ); ?>"><?php esc_html_e( 'Choose file', 'neal' ); ?></a>
									<input class="upload-custom-font" type="file" name="font_file[]" accept=".ttf,.otf,.woff" style="display: none;">
								</td>
								<td width="1%"><a href="#" class="neal-custom-font-delete"><?php esc_html_e( 'Delete', 'neal' ); ?></a></td>
							</tr>

							<?php 
							endforeach;
							endif; ?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="5"><a href="#" data-field='<tr><td class="file_name"><input type="text" class="input_text neal-cf-font-name" placeholder="<?php esc_attr_e('Font name','neal');?>" name="neal-cf-names[]" value="">
								</td>
								<td class="file_url">
									<input type="text" class="input_text neal-cf-file-url" placeholder="http://" name="neal-cf-urls[]" value="">
								</td>
								<td class="file_formats">
									<p><strong>.ttf, .otf</strong> <?php esc_html_e('and','neal'); ?> <strong>.woff</strong></p>
								</td>
								<td class="file_url_choose" width="1%">
									<a href="#" class="button upload_file_button" data-choose="<?php esc_attr_e('Choose file','neal');?>" data-update="<?php esc_attr_e('Insert file URL','neal'); ?>"><?php esc_html_e('Choose file','neal');?></a>
									<input class="upload-custom-font" type="file" name="font_file[]" accept=".ttf,.otf,.woff" style="display: none;">
								</td>
								<td width="1%"><a href="#" class="neal-custom-font-delete"><?php esc_html_e('Delete','neal');?></a></td>
							</tr>' class="button neal-custom-font-add-file"><?php esc_html_e( 'Add File', 'neal' ); ?></a>
								<input type="hidden" id="neal-cf-submit" name="neal-cf-submit" value="true">
								<input type="hidden" class="neal-cf-delete" name="neal-cf-delete" value="false">
								<button class="button button-primary"><?php esc_html_e( 'Save Fonts', 'neal' ); ?></button>
							</th>

							</tr>
						</tfoot>
					</table>
					</form>
			</div>
		</div>
		<?php
		}

		public function custom_fonts_upload() {

			if ( isset( $_POST['neal-cf-submit'] ) ) {
				$fonts = array();

				if ( isset( $_FILES['font_file'] ) ) {
					$files = $_FILES['font_file'];
					$errors = array();

					$upload_dir = wp_upload_dir();
					$dir = path_join( $upload_dir['basedir'], 'custom-fonts' );

					wp_mkdir_p( $dir );

					$upload_overrides = array( 'ttf', 'otf', 'woff' );

					foreach ( $files['name'] as $key => $value ) {
						if ( $files['name'][ $key ] ) {
						    $file = array(
						      'name'     => $files['name'][ $key ],
						      'type'     => $files['type'][ $key ],
						      'tmp_name' => $files['tmp_name'][ $key ],
						      'error'    => $files['error'][ $key ],
						      'size'     => $files['size'][ $key ]
						    );

						   	$file_expl = explode( '.', $file['name'] );
						    $file_ext = strtolower( end( $file_expl ) );


					    	if ( in_array( $file_ext, $upload_overrides ) === false ) {
					        	$errors[] = esc_html__( 'Extension not allowed, please choose a ttf, otf, woff files.', 'neal' );
					      	}

					      	if ( $file['size'] > 4097152 ) {
					        	$errors[] = esc_html__( 'File size must be exactly 4 MB', 'neal' );
					     	}

					      	if ( empty( $errors ) == true ) {
					      		$movefile = @move_uploaded_file( $file['tmp_name'], $dir . '/' . $file['name'] );
					      		echo '<div class="notice notice-success is-dismissible">';
									echo '<p>' . esc_html__( 'Font successfully uploaded', 'neal' ) . '</p>';
								echo '</div>';
					      	} else {
					      		if ( isset( $errors[0] ) ) {
						      		echo '<div class="notice notice-warning is-dismissible">';
										echo '<p>' . $errors[0] . '</p>';
									echo '</div>';
								} 

								if ( isset( $errors[1] ) ) {
									echo '<div class="notice notice-warning is-dismissible">';
										echo '<p>' . $errors[1] . '</p>';
									echo '</div>';
								}
					      	}
					  	}
					}

				}

	
				if ( isset( $_POST['neal-cf-names'] ) ) {
					$font_names = $_POST['neal-cf-names'];
					$font_urls 	= $_POST['neal-cf-urls'];

					foreach ( $font_names as $key => $value ) {
						$font = trim( $font_names[ $key ] );
						if ( ! empty( $font ) ) {
							$fonts[] = array(
								'name'	=> wp_filter_nohtml_kses( $font_names[ $key ] ),
								'url'	=> wp_filter_nohtml_kses( $font_urls[ $key ] )
							);
						}
					}
				}

				update_option( 'neal-custom-fonts', $fonts );


			}

			if ( isset( $_POST['neal-cf-delete'] ) ) {
				$file_delete = explode( ',', $_POST['neal-cf-delete'] );
				$upload_dir = wp_upload_dir();
				$dir = path_join( $upload_dir['basedir'], 'custom-fonts' );

				foreach ( $file_delete as $file ) {
					if ( $file_delete ) {
						$file = basename( $file );
						@unlink( $dir . '/' . $file );
					}
				}
			}
		}

	} //end Neal_Custom_Fonts class

endif;

return new Neal_Custom_Fonts;