<?php
/**
 * Neal Extension functions.
 *
 * @package neal-extension
 */

// based on https://gist.github.com/cosmocatalano/4544576
function neal_instagram( $username, $numbers, $carousel = false, $carousel_columns = 4, $carousel_nav = false, $carousel_loop = false, $extra_classes = '', $image_size = 'thumbnail' ) {

		$username = strtolower( $username );
		$username = str_replace( '@', '', $username );

		if ( false === ( $instagram = get_transient( 'neal-instagram-'.sanitize_title_with_dashes( $username ) ) ) ) {

			$remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );

			if ( is_wp_error( $remote ) ) {
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'neal' ) );
			}

			if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'neal' ) );
			}

			$shards = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], TRUE );

			if ( ! $insta_array ) {
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'neal' ) );
			}

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
			} else {
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'neal' ) );
			}

			if ( ! is_array( $images ) ) {
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'neal' ) );
			}

			$instagram = array();

			foreach ( $images as $image ) {

				if ( $image['is_video'] == true ) {
					$type = 'video';
				} else {
					$type = 'image';
				}

				$caption = esc_html__( 'Instagram Image', 'neal' );
				if ( ! empty( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ) {
					$caption = wp_kses( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'], array() );
				}

				$instagram[] = array(
					'description' => $caption,
					'link'        => trailingslashit( '//instagram.com/p/' . $image['node']['shortcode'] ),
					'time'        => $image['node']['taken_at_timestamp'],
					'comments'    => $image['node']['edge_media_to_comment']['count'],
					'likes'       => $image['node']['edge_liked_by']['count'],
					'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ),
					'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ),
					'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][4]['src'] ),
					'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
					'type'        => $type,
				);
			}

			// do not set an empty transient - should help catch private or empty accounts
			if ( ! empty( $instagram ) ) {
				$instagram = base64_encode( serialize( $instagram ) );
				set_transient( 'neal-instagram-'.sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
			}
		}

		if ( ! empty( $instagram ) ) {

			$instagram = unserialize( base64_decode( $instagram ) );

			$carousel_c = '';
			$carousel_column = '';
			$carousel_navi = '';
			$carousel_loopp = '';
			$cont = '';
			$col_classes = '';

			if ( $carousel ) {
				$carousel_c = 'owl-carousel';
				$carousel_column = 'data-carousel-columns="'.$carousel_columns.'"';
				$carousel_navi = 'data-carousel-nav="'.$carousel_nav.'"';
				$carousel_loopp = 'data-carousel-loop="'.$carousel_loop.'"';
			} else {
				// boxed layout
				if ( neal_get_option( 'footer_instagram-layout' ) == 'boxed' ) {
					$cont = 'container';
					$carousel_c = 'row ';
				}
				
				$col = ( 12 / $numbers );
				$col_classes = 'col-lg-' . intval( $col );
			}

			$output   = array();
			$output[] = '<div class="instagram-list '.$cont.'" '.$carousel_column.' '.$carousel_navi.' '.$carousel_loopp.'>';
			$output[] = '<div class="list '.$carousel_c.''.$extra_classes.'">';

			$count = 0;

			foreach ( $instagram as $item ) {
				$image_link = $item[ $image_size ];
				$image_url  = $item['link'];

				$image_html  = '';
				
				$image_html = sprintf( '<img src="%s" alt="%s">', esc_url( $image_link ), esc_attr( '' ) );

				$output[] = '<div class="insta '.$col_classes.'">' . '<a class="insta-item" href="' . esc_url( $image_url ) . '" target="_blank">' . $image_html . '<span><i class="fa fa-instagram"></i></span></a>' . '</div>' . "\n";


				$count ++;
				$numbers = intval( $numbers );
				if ( $numbers > 0 ) {
					if ( $count == $numbers ) {
						break;
					}
				}
			}

			$output[] = '</div></div>';

			return implode( ' ', $output );

		} else {

			return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'neal' ) );

		}
}

if ( ! function_exists( 'neal_contact_page_backend' ) ) {
	/**
	 * Contact page back-end
	 *
	 * @since  1.0.0
	 */
	function neal_contact_page_backend() {
		// get theme customizer data
		$contact = get_option( 'neal_contact' );


		if ( isset( $_POST['con-p-submit'] ) ) {

			$name 	 		= sanitize_text_field( $_POST['con-p-name'] );
			$email 	 		= sanitize_email( $_POST['con-p-email'] );
			$subject 		= sanitize_text_field( $_POST['con-p-subject'] );
			$message 		= esc_textarea( $_POST['con-p-msg'] );

			if ( isset( $contact['form-reciever-email'] ) ) {
				$reciever_email = esc_html( $contact['form-reciever-email'] );
			} else {
				$reciever_email = get_option('admin_email');
			}

			// email content
			$body  = esc_html__( 'Name: ', 'neal' ) . $name ."\n\n";
			$body .= esc_html__( 'Email: ', 'neal' ) . $email ."\n\n";
			$body .= esc_html__( 'Subject: ', 'neal' ) . $subject ."\n\n";
			$body .= esc_html__( 'Message: ', 'neal' ) ."\n\n";
			$body .= $message;


			// email headers
			$headers = esc_html__( 'From ', 'neal' ) . $name .' <'. $email .'>' ."\r\n";

			$send_it = wp_mail( $reciever_email, $subject, $body, $headers );

			if ( $send_it ) {
				echo '<p class="result-message">'.esc_html__( 'Your request has been sent', 'neal' ).'</p>';
			} else {
				echo '<p class="result-message">'.esc_html__( 'Request not sent!', 'neal' ).'</p>';
			}
		}
	}
}