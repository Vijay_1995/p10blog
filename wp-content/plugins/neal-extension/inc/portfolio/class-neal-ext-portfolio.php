<?php
/**
 * Neal Extension Portfolio.
 *
 * @package neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Portfolio' ) ) :

	/**
	 * The portfolio class.
	 */
	class Neal_Portfolio {

		private $option = 'neal-portfolio';

		/**
		 * Setup class.
		 *
		 * @since 1.0
		 *
		 * @var array
		 */

		public function __construct() {
			// Add an option to enable the CPT
			add_action( 'admin_init', array( $this, 'settings_api_init' ) );

			if ( get_option( $this->option ) ) {
				add_action( 'init', array( $this, 'create_posttype' ) );
				add_action( 'init', array( $this, 'create_taxonomy' ) );
			}

			// load more pagination
			add_action( 'wp_ajax_neal_portfolio_load_more',		array( $this, 'portfolio_load_more_ajax' ) );
			add_action( 'wp_ajax_nopriv_neal_portfolio_load_more',	array( $this, 'portfolio_load_more_ajax' ) );
		}

		public function create_posttype() {
			register_post_type( $this->option,
				array(
					'description' => esc_html__( 'Portfolio Items', 'neal' ),
					'labels'	  => array(
						'name'			=> esc_html__( 'Portfolio', 'neal' ),
						'singular'		=> esc_html__( 'Portfolio Item', 'neal' ),
						'all_items'    	=> esc_html__( 'All Items', 'neal' ),
						'add_new_item'	=> esc_html__( 'Add New Item', 'neal' )
					),
					'public'		=> true,
					'menu_position' => 20, 
					'menu_icon'     => 'dashicons-portfolio',
					'has_archive'	=> false,
					'rewrite'		=> array( 
						'slug' => 'portfolio' 
					),
					'supports'		=> array(
						'title',
						'editor',
						'thumbnail',
						'author',
					)
				)
			);
		}

		public function create_taxonomy() {
			// portfolio categories
			register_taxonomy(
				'neal-portfolio-category',
				$this->option,
				array(
					'labels' => array(
						'name'				=> esc_html__( 'Portfolio Categories', 'neal' ),
						'singular_name'		=> esc_html__( 'Portfolio Category', 'neal' ),
						'search_items'		=> esc_html__( 'Search Categories', 'neal' ),
						'popular_items'		=> esc_html__( 'Popular Categories', 'neal' ),
						'all_items'			=> esc_html__( 'All Categories', 'neal' ),
						'parent_item'		=> esc_html__( 'Parent Category', 'neal' ),
						'parent_item_colon' => esc_html__( 'Parent Category:', 'neal' ),
						'edit_item'			=> esc_html__( 'Edit Category', 'neal' ),
						'update_item' 		=> esc_html__( 'Update Category', 'neal' ),
						'add_new_item'		=> esc_html__( 'Add New Category', 'neal' ),
						'new_item_name'		=> esc_html__( 'New Category Name', 'neal' ),
						'menu_name'			=> esc_html__( 'Portfolio Categories', 'neal' )
					),
					'public' 			=> true,
					'show_in_nav_menus' => true,
					'show_admin_column' => true,
					'show_tagcloud'		=> true,
					'hierarchical' 		=> true,
					'rewrite' 			=> array( 'slug' => 'portfolio-category' )
				)
			);
		}

		/**
		 * Add a checkbox field in 'Settings' > 'Writing'
		 * for enabling CPT functionality.
		 */
		public function settings_api_init() {
			add_settings_section(
				'neal_portfolio_section',
				'<span id="portfolio-options">' . esc_html__( 'Portfolio', 'neal' ) . '</span>',
				array( $this, 'writing_section_html' ),
				'writing'
			);

			add_settings_field(
				$this->option,
				'<span class="portfolio-options">' . esc_html__( 'Portfolio Projects', 'neal' ) . '</span>',
				array( $this, 'enable_field_html' ),
				'writing',
				'neal_portfolio_section'
			);

			register_setting(
				'writing',
				$this->option,
				'intval'
			);

			// Check if CPT is enabled
			if ( get_option( $this->option ) ) {
				
				// Reading settings
				add_settings_section(
					'sober_portfolio_section',
					'<span id="portfolio-options">' . esc_html__( 'Portfolio', 'neal' ) . '</span>',
					array( $this, 'reading_section_html' ),
					'reading'
				);

				add_settings_field(
					$this->option . '_posts_per_page',
					'<label for="portfolio_items_per_page">' . esc_html__( 'Portfolio items show at most', 'neal' ) . '</label>',
					array( $this, 'per_page_field_html' ),
					'reading',
					'sober_portfolio_section'
				);

				register_setting(
					'reading',
					$this->option . '_posts_per_page',
					'intval'
				);
			}
		}

		/**
		 * Add writing setting section
		 */
		public function writing_section_html() {
			?>
			<p>
				<?php esc_html_e( 'Use these settings to display custom types of content on your site', 'neal' ); ?>
			</p>
			<?php
		}

		/**
		 * Add reading setting section
		 */
		public function reading_section_html() {
			?>
			<p>
				<?php esc_html_e( 'Use these settings to control custom post type content', 'neal' ); ?>
			</p>
			<?php
		}

		/**
		 * HTML code to display a checkbox true/false option
		 * for the Portfolio CPT setting.
		 */
		public function enable_field_html() {
			?>

			<label for="<?php echo esc_attr( $this->option ); ?>">
				<input name="<?php echo esc_attr( $this->option ); ?>"
					   id="<?php echo esc_attr( $this->option ); ?>" <?php checked( get_option( $this->option ), true ); ?>
					   type="checkbox" value="1" />
				<?php esc_html_e( 'Enable Portfolio Projects for this site.', 'neal' ); ?>
			</label>

			<?php
		}

		/**
		 * HTML code to display a input of option for portfolio items per page
		 */
		public function per_page_field_html() {
			$name = $this->option . '_posts_per_page';
			?>

			<label for="portfolio_posts_per_page">
				<input name="<?php echo esc_attr( $name ) ?>" id="portfolio_items_per_page" type="number" step="1" min="1"
					   value="<?php echo esc_attr( get_option( $name, '9' ) ) ?>" class="small-text" />
				<?php _ex( 'items', 'Portfolio items per page', 'neal' ) ?>
			</label>

			<?php
		}

		public function portfolio_load_more_ajax() {
			$portfolio_amount 	= intval( get_option('neal-portfolio_posts_per_page') );
			$post_category 	 	= $_POST['postCat'];
			$post_offset 		= $portfolio_amount * $_POST['nextPage'];

			$output = '';

			$args = array(
				'post_type'			=> 'neal-portfolio',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> $portfolio_amount,
				'offset'			=> $post_offset
			);

			// category
			if ( isset( $_POST['postCat'] ) ) {
				$args['neal-portfolio-category'] = $post_category;
			}

			$query = new WP_Query( $args );

			if ( $query->have_posts() ) : 
				while ( $query->have_posts() ) : $query->the_post();
					$post_classes = implode( ' ', get_post_class('portfolio-item col-lg-4') );

					$output .= '<div id="post-'.get_the_ID().'" class="'.$post_classes.'">';
						
						// entry media
						if ( has_post_thumbnail() ) :
							$output .= '<div class="entry-media">';
								$output .= '<div class="entry-image">';
									$output .= '<a href="'.esc_url( get_the_permalink() ).'">';
										ob_start();
										neal_post_thumbnail( 'neal-portfolio-item-image' );
										$output .= ob_get_clean();
										$output .= '<span class="portfolio-link"><i class="ion-ios-plus"></i></span>';
									$output .= '</a>';
								$output .= '</div>';
							$output .= '</div>';
						endif;
						
						$output .= '<h1 class="entry-title">';
							$output .= '<a href="'.esc_url( get_the_permalink() ).'">';
								$output .= get_the_title();
							$output .= '</a>';
						$output .= '</h1>';

						$output .= neal_get_portfolio_categories();

					$output .= '</div>';

					endwhile;
					wp_reset_postdata();

				endif;

			$html_temp = array(
				'html' 	=> $output
			);

			wp_send_json( $html_temp );
			wp_die();
		}

	}

endif;

return new Neal_Portfolio;