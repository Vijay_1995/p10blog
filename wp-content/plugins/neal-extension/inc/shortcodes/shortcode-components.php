<?php
/**
 * Shortcode components.
 *
 * @package neal
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Neal_Ext_Shortcode_Components' ) ) :

	/**
	 * The Shortcodes Components class
	 */
	
	class Neal_Ext_Shortcode_Components {

		public $html_components = '';

		public function __construct( $shortcode ) {
			// load component
			call_user_func( array( $this, $shortcode ) );
			
		}

		/* Shortcode items components */

		public function dropcap() {

			// size
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Font Size', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'font-size',
								'min'	=> 5,
								'max'	=> 150,
								'value'	=> 75,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 75,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// weight
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Font Weight', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'font-weight',
								'min'	=> 100,
								'max'	=> 900,
								'step'	=> 100,
								'value'	=> 700,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 700,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function tabs() {

			// tabs
			$this->add_child_item(
					// title
					array(
						'title' 	=> esc_html__( 'Title', 'neal' ),
						'element'	=> 'input',
						'attr'		=> array(
							'type'	=> 'text',
							'name'	=> 'title',
							'class'	=> 'neal-shortcode-attr'

						),
						'shortcode'	=> 'tab'
					),
					// content
					array(
						'title' 	=> esc_html__( 'Content', 'neal' ),
						'element'	=> 'textarea',
						'attr'		=> array(
							'rows'	=> 8,
							'class'	=> 'neal-shortcode-content'

						)
					)
			);

			// active tab
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Active Tab', 'neal' ),
					'description'	=> esc_html__( 'Select which tab is open by default', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'number',
						'name'	=> 'active',
						'value'	=> 1,
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// vertical
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Vertical', 'neal' ),
					'description'	=> esc_html__( 'Show tabs vertically', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'vertical',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Yes', 'neal' ),
							'attr'		=> array(
								'value'	=> 'yes'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'No', 'neal' ),
							'attr'		=> array(
								'value'		=> 'no',
								'selected'	=> 'selected'
							)
						)
					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// add item 'add_button'
			$this->add_child_item_addbutton(
				array(
					'title'		=> esc_html__( 'Add Tab', 'neal' ),
					'shortcode'	=> 'tab'
				)
			);

		}

		public function spoiler() {

			// title
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Title', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'title',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// content
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Content', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'class'	=> 'neal-shortcode-content'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function accordion() {
			
			// accordion
			$this->add_child_item(
					// title
					array(
						'title' 	=> esc_html__( 'Title', 'neal' ),
						'element'	=> 'input',
						'attr'		=> array(
							'type'	=> 'text',
							'name'	=> 'title',
							'class'	=> 'neal-shortcode-attr'

						),
						'shortcode'	=> 'accordion_item'
					),
					// content
					array(
						'title' 	=> esc_html__( 'Content', 'neal' ),
						'element'	=> 'textarea',
						'attr'		=> array(
							'rows'	=> 8,
							'class'	=> 'neal-shortcode-content'

						),
						'shortcode'	=> 'accordion_item'
					)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// add item 'add_button'
			$this->add_child_item_addbutton(
				array(
					'title'		=> esc_html__( 'Add Accordion', 'neal' ),
					'shortcode'	=> 'accordion_item'
				)
			);

		}

		public function divider() {
			
			// color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'color',
						'value'	=> '#000',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// size
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Size', 'neal' ),
					'description'	=> esc_html__( 'Height of the divider (in pixels)', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'size',
								'min'	=> 0,
								'max'	=> 50,
								'value'	=> 3,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 3,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// margin
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Margin', 'neal' ),
					'description'	=> esc_html__( 'Adjust the top and bottom margins of this divider (in pixels)', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'margin',
								'min'	=> 0,
								'max'	=> 100,
								'value'	=> 10,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 10,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

		}

		public function recipe() {

			// title
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Title', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'title',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// description
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Description', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'name'	=> 'description',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// servings
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Servings', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'servings',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// time
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Time', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'time',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// difficulty
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Difficulty', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'difficulty',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements' => array(
						array(
							'element' => 'option',
							'value'	  => esc_html__( 'Easy', 'neal' )
						),
						array(
							'element' => 'option',
							'value'	  => esc_html__( 'Medium', 'neal' )
						),
						array(
							'element' => 'option',
							'value'	  => esc_html__( 'Hard', 'neal' )
						)
					)
				)
			);

			// ingredients
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Ingredients', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'id'	=> 'neal-recipe-ingredients',
						'class'	=> 'neal-shortcode-attr',
						'data-ch-shortcode' => 'recipe_ingredients',


					)
				)
			);

			// directions
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Directions', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'id'	=> 'neal-recipe-directions',
						'class'	=> 'neal-shortcode-attr',
						'data-ch-shortcode' => 'recipe_directions',


					)
				)
			);

			// notes
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Notes', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'id'	=> 'neal-recipe-notes',
						'class'	=> 'neal-shortcode-attr',
						'data-ch-shortcode' => 'recipe_notes',

					)
				)
			);
		}

		public function slider() {

			// add images
			$this->add_item(
				array( 
					'description'	=> esc_html__( 'Click the button above and select images. You can select multimple images with Ctrl (Cmd) key', 'neal' ),
					'element'	=> 'a',
					'value' 	=> esc_html__( 'Add Images', 'neal' ),
					'attr'		=> array(
						'class'	=> 'button button-primary neal-shortcode-add-slider-image'

					)
				)
			);

			// images list
			$this->add_item(
				array( 
					'description'	=> esc_html__( 'List Images', 'neal' ),
					'element'		=> 'ul',
					'attr'			=> array(
						'class'	=> 'neal-shortcode-slider-images'

					),
					'child_elements'	=> array(
						array( 
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'hidden',
								'name'	=> 'images-ids',
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider-images-ids'

							)
						)
					)
				)
			);

			// images size
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Images Size', 'neal' ),
					'description'	=> esc_html__( 'Slider Images size', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'images-size',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Small', 'neal' ),
							'attr'		=> array(
								'value'	=> 'thumbnail'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Medium', 'neal' ),
							'attr'		=> array(
								'value'	=> 'medium'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Large', 'neal' ),
							'attr'		=> array(
								'value'	=> 'full'
							)
						)
					)
				)
			);

			// slider columns
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Slider Columns', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'number',
						'name'	=> 'columns',
						'value'	=> 3,
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// slider loop
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Slider Loop', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'loop',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Yes', 'neal' ),
							'attr'		=> array(
								'value'	=> 1
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'No', 'neal' ),
							'attr'		=> array(
								'value'	=> 0
							)
						)
					)
				)
			);

			// slider navigation
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Slider Navigation', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'nav',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Yes', 'neal' ),
							'attr'		=> array(
								'value'	=> 1
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'No', 'neal' ),
							'attr'		=> array(
								'value'		=> 0,
								'selected'	=> 'selected'
							)
						)
					)
				)
			);
			
			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function gmap() {

			// width
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Width', 'neal' ),
					'description'	=> esc_html__( 'Map width', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'width',
								'min'	=> 0,
								'max'	=> 2000,
								'value'	=> 300,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 300,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// height
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Height', 'neal' ),
					'description'	=> esc_html__( 'Map height', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'height',
								'min'	=> 0,
								'max'	=> 2000,
								'value'	=> 300,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 300,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// marker
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Marker', 'neal' ),
					'description'	=> esc_html__( 'Address for the marker. You can type it in any language', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'marker',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function youtube() {

			// url
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Url', 'neal' ),
					'description'	=> esc_html__( 'Url of YouTube page with video. Ex: http://youtube.com/watch?v=XXXXXX', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'url',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// width
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Width', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'width',
								'min'	=> 0,
								'max'	=> 2000,
								'value'	=> 400,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 400,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// height
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Height', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'height',
								'min'	=> 0,
								'max'	=> 2000,
								'value'	=> 400,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 400,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// autoplay
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Autoplay', 'neal' ),
					'description'	=> esc_html__( 'Play video automatically when page is loaded', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'autoplay',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Yes', 'neal' ),
							'attr'		=> array(
								'value'	=> 1
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'No', 'neal' ),
							'attr'		=> array(
								'value'		=> 0,
								'selected'	=> 'selected'
							)
						)
					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function button() {

			// link
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Link', 'neal' ),
					'description'	=> esc_html__( 'Button link', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'link',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// target
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Target', 'neal' ),
					'description'	=> esc_html__( 'Button link target', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'target',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Same tab', 'neal' ),
							'attr'		=> array(
								'value'	=> 'same'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'New tab', 'neal' ),
							'attr'		=> array(
								'value'	=> 'new'
							)
						)
					)
				)
			);

			// background-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Background Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'background-color',
						'value'	=> '#000',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// text-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Text Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'text-color',
						'value'	=> '#000',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// size
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Size', 'neal' ),
					'description'	=> esc_html__( 'Button font size', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'size',
								'min'	=> 0,
								'max'	=> 50,
								'value'	=> 3,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 3,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// button content
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Content', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'class'	=> 'neal-shortcode-content'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function quote() {

			// author name
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Author Name', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'author-name',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
			
			// content
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Content', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'class'	=> 'neal-shortcode-content'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function row() {

			// row
			$this->add_child_item(
					// content
					array(
						'title' 	=> esc_html__( 'Content', 'neal' ),
						'element'	=> 'textarea',
						'attr'		=> array(
							'rows'	=> 8,
							'class'	=> 'neal-shortcode-content'

						),
						'shortcode'	=> 'row_column'
					)
			);

			// row size
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Row Size', 'neal' ),
					'description'	=> esc_html__( 'Show row count', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'number',
						'name'	=> 'size',
						'min'	=> 1,
						'max'	=> 4,
						'value'	=> 1,
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// add item 'add_button'
			$this->add_child_item_addbutton(
				array(
					'title'		=> esc_html__( 'Add Row', 'neal' ),
					'shortcode'	=> 'row_column'
				)
			);
		}

		public function box() {

			// title
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Title', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'title',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// content
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Content', 'neal' ),
					'element'	=> 'textarea',
					'attr'		=> array(
						'rows'	=> 8,
						'class'	=> 'neal-shortcode-content'

					)
				)
			);

			// color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'color',
						'value'	=> '#000',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// title text-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Title Text Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'title-text-color',
						'value'	=> '#fff',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// size
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Border Radius', 'neal' ),
					'element'	=> 'div',
					'attr'		=> array(
						'class'	=> 'neal-shortcode-slider-inner'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'input',
							'attr'		=> array(
								'type'	=> 'range',
								'name'	=> 'border-radius',
								'min'	=> 0,
								'max'	=> 50,
								'value'	=> 3,
								'class'	=> 'neal-shortcode-attr neal-shortcode-slider'
							)
						),
						array(
							'element'	=> 'span',
							'value'		=> 3,
							'attr'		=> array(
								'class'	=> 'neal-shortcode-slider-value'
							)
						)
					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

		}

		public function tooltip() {

			// tooltip title
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Tooltip Title', 'neal' ),
					'description'	=> esc_html__( 'Enter title for tooltip window. Leave this field empty to hide the title', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'title',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

			// position
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Position', 'neal' ),
					'description'	=> esc_html__( 'Tooltip position', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'position',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Top', 'neal' ),
							'attr'		=> array(
								'value'	=> 'top'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Bottom', 'neal' ),
							'attr'		=> array(
								'value'	=> 'bottom'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Left', 'neal' ),
							'attr'		=> array(
								'value'	=> 'left'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Right', 'neal' ),
							'attr'		=> array(
								'value'	=> 'right'
							)
						)
					)
				)
			);

			// behavior
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Behavior', 'neal' ),
					'description'	=> esc_html__( 'Select tooltip behavior', 'neal' ),
					'element'	=> 'select',
					'attr'		=> array(
						'name'	=> 'behavior',
						'class'	=> 'neal-shortcode-attr'

					),
					'child_elements'	=> array(
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Show and hide on mouse hover', 'neal' ),
							'attr'		=> array(
								'value'	=> 'hover'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Show and hide by mouse click', 'neal' ),
							'attr'		=> array(
								'value'	=> 'click'
							)
						),
						array(
							'element'	=> 'option',
							'value'		=> esc_html__( 'Always visible', 'neal' ),
							'attr'		=> array(
								'value'	=> 'always'
							)
						)
					)
				)
			);

			// color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Font Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'color',
						'value'	=> '#fff',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// background-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Background Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'background-color',
						'value'	=> '#000',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);
		}

		public function highlight() {

			// background-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Background Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'background-color',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// text-color
			$this->add_item(
				array( 
					'title' 	=> esc_html__( 'Text Color', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'color',
						'class'	=> 'neal-shortcode-attr neal-shortcode-colorpic'

					)
				)
			);

			// extra class
			$this->add_item(
				array( 
					'title' 		=> esc_html__( 'Class', 'neal' ),
					'description'	=> esc_html__( 'Extra CSS class', 'neal' ),
					'element'	=> 'input',
					'attr'		=> array(
						'type'	=> 'text',
						'name'	=> 'extra-class',
						'class'	=> 'neal-shortcode-attr'

					)
				)
			);

		}

		function add_item( $args ) {
			$args = wp_parse_args( $args, array(
				'title'			=> '',
				'description'	=> '',
				'element'		=> '',
				'value'			=> '',
				'attr'			=> array(),
				'child_elements'	=> array(),
			) );

			$output = array();
			$child_elements = array();
			$attr = '';

			// child elements
			if ( isset( $args['child_elements'] ) ) {

				foreach ( $args['child_elements'] as $key ) {
					$child_attr = '';

					// attributes
					if ( isset( $key['attr'] ) ) {
						foreach ( $key['attr'] as $att => $att_value ) {
							$child_attr .= $att . '="' . $att_value .'"';
						}
					}

					if ( $key['element'] == 'input' ) {
						$child_elements[] = sprintf( '<%s %s>', $key['element'], $child_attr);
					} else {
						$child_elements[] = sprintf( '<%s %s>%s</%s>', $key['element'], $child_attr, $key['value'], $key['element'] );
					}
				}
			}

			// attributes
			if ( isset( $args['attr'] ) ) {
				foreach ( $args['attr'] as $key => $value ) {
					$attr .= $key . '="' . $value .'"';
				}
			}

			// title
			if ( isset( $args['title'] ) ) {
				$output[] = sprintf( '<label>%s</label>', $args['title'] );
			}

			if ( $args['element'] == 'input' ) {
				$output[] = sprintf( '<%s %s>', $args['element'], $attr );
			} else {
				if ( ! empty( $child_elements ) ) {
					$output[] = sprintf( '<%s %s>%s</%s>', $args['element'], $attr, implode( '', $child_elements ), $args['element'] );
				} else {
					if ( isset( $args['value'] ) ) {
						$output[] = sprintf( '<%s %s>%s</%s>', $args['element'], $attr, $args['value'], $args['element'] );
					} else {
						$output[] = sprintf( '<%s %s></%s>', $args['element'], $attr, $args['element'] );
					}
				}
			}

			// description
			if ( isset( $args['description'] ) ) {
				$output[] = sprintf( '<span class="neal-shortcode-description">%s</span>', $args['description'] );
			}

			echo sprintf(
				 	'<div class="neal-shortcode-attr-container">' .
					'%s' .
					'</div>',
					implode( '', $output )
			);
		}

		function add_child_item() {

			$html = '';

			$arg_count = func_num_args();
			$arg_list = func_get_args();

			for ( $i = 0; $i < $arg_count; $i++ ) {
				$args = wp_parse_args( $arg_list[ $i ], array(
					'title'			=> '',
					'description'	=> '',
					'element'		=> '',
					'attr'			=> array(),
					'child_elements'	=> array(),
					'shortcode'		=> '',
				) );

				$output = array();
				$child_elements = array();
				$attr = '';


				// child elements
				if ( isset( $args['child_elements'] ) ) {

					foreach ( $args['child_elements'] as $key ) {
						$child_attr = '';

						// attributes
						if ( isset( $key['attr'] ) ) {
							foreach ( $key['attr'] as $att => $att_value ) {
								$child_attr .= $att . '="' . $att_value .'"';
							}
						}

						if ( $key['element'] == 'input' ) {
							$child_elements[] = sprintf( '<%s %s>', $key['element'], $child_attr);
						} else {
							$child_elements[] = sprintf( '<%s %s>%s</%s>', $key['element'], $child_attr, $key['value'], $key['element'] );
						}
					}
				}

				// attributes
				if ( isset( $args['attr'] ) ) {
					foreach ( $args['attr'] as $key => $value ) {
						$attr .= $key . '="' . $value .'"';
					}
				}

				// title
				if ( isset( $args['title'] ) ) {
					$output[] = sprintf( '<label>%s</label>', $args['title'] );
				}

				if ( $args['element'] == 'input' ) {
					$output[] = sprintf( '<%s %s>', $args['element'], $attr );
				} else {
					if ( isset( $child_elements ) ) {
						$output[] = sprintf( '<%s %s>%s</%s>', $args['element'], $attr, implode( '', $child_elements ), $args['element'] );
					} else {
						$output[] = sprintf( '<%s %s></%s>', $args['element'], $attr, $args['element'] );
					}
				}

				// description
				if ( isset( $args['description'] ) ) {
					$output[] = sprintf( '<span class="neal-shortcode-description">%s</span>', $args['description'] );
				}

				$html .= sprintf(
					'<div class="neal-shortcode-attr-container neal-child-shortcode">' .
					'%s' .
					'</div>' ,
					implode( '', $output )
				);

			}

			$this->html_components = $html;

			echo sprintf(
				 	'<div class="neal-setting-items">' .
					'<div class="neal-shortcode-child" data-ch-shortcode="%s">' .
					'%s' .
					'</div>' .
					'</div>',
					$arguments[0]['shortcode'],
					$html
			);
		}

		function add_child_item_addbutton( $args ) {

			$args = wp_parse_args( $args, array(
				'title'		=> '',
				'shortcode'	=> '',
				
			) );

			echo sprintf(
					'<a href="#" class="button neal-shortcode-add-item" data-shortcode-item-html="<div class=\'neal-shortcode-child\' data-ch-shortcode=\'%s\'>%s</div>">' .
					'%s' .
					'</a>',
					$args['shortcode'],
					str_replace( '"', '\'', $this->html_components ),
					$args['title'],
					implode( '', $output )
			);
		}
	}

endif;