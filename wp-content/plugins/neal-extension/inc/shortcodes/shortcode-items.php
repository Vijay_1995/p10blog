<?php
/**
 * Neal Extension Shortcodes Class
 *
 * @author   TheSpan
 * @since    1.0.0
 * @package  neal-extension
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once ABSPATH . 'wp-includes/pluggable.php';

if ( ! class_exists( 'Neal_Ext_Shortcode_Items' ) ) :

	/**
	 * The Shortcodes class
	 */
	
	class Neal_Ext_Shortcode_Items {

		public function __construct() {

			add_action( 'wp_ajax_neal_shortcode_list_items', array( $this, 'ajax_list_items' ) );
			add_action( 'wp_ajax_neal_shortcode_items', array( $this, 'ajax_items' ) );

		}

		public function add_item( $name, $icon, $title, $description = '' ) {
			$html = '<span data-shortcode="' .$name. '">
    				 	<img src="' .NEAL_EXT_URL .'/images/shortcodes/'. $icon. '">
    					<div class="neal-list-shortcodes-content">
    						<h3>'.$title.'</h3>
    						<p>'.$description.'</p>
    					</div>
    				</span>';

    		echo $html;
		}
		
		public function ajax_list_items() {
			// dropcap
			$this->add_item( 'dropcap', 'dropcap.png', 'Dropcap', esc_html__( 'First uppercase letter', 'neal' ) );

			// tabs
			$this->add_item( 'tabs', 'tabs.png', 'Tabs', esc_html__( 'Partial content', 'neal' ) );

			// spoiler
			$this->add_item( 'spoiler', 'spoiler.png', 'Spoiler', esc_html__( 'Spoiler content', 'neal' ) );

			// accordion
			$this->add_item( 'accordion', 'accordion.png', 'Accordion', esc_html__( 'Partial content', 'neal' ) );

			// divider
			$this->add_item( 'divider', 'divider.png', 'Divider', esc_html__( 'Content separator', 'neal' ) );

			// recipe
			$this->add_item( 'recipe', 'recipe.png', 'Recipe', esc_html__( 'Custom food content', 'neal' ) );

			// slider
			$this->add_item( 'slider', 'slider.png', 'Slider', esc_html__( 'List images in slider', 'neal' ) );

			// gmap
			$this->add_item( 'gmap', 'gmap.png', 'Gmap', esc_html__( 'Show your address', 'neal' ) );

			// youtube
			$this->add_item( 'youtube', 'youtube.png', 'Youtube', esc_html__( 'Show YouTube videos', 'neal' ) );

			// button
			$this->add_item( 'button', 'button.png', 'Button', esc_html__( 'Create Custom buttons', 'neal' ) );

			// quote
			$this->add_item( 'quote', 'quote.png', 'Quote', esc_html__( 'Special content', 'neal' ) );

			// row
			$this->add_item( 'row', 'row.png', 'Row', esc_html__( 'Row content', 'neal' ) );

			// box
			$this->add_item( 'box', 'box.png', 'Box', esc_html__( 'Show content in box', 'neal' ) );

			// tooltip
			$this->add_item( 'tooltip', 'tooltip.png', 'Tooltip', esc_html__( 'Show content in tooltip', 'neal' ) );

			// hightlight
			$this->add_item( 'highlight', 'highlight.png', 'Highlight', esc_html__( 'Scribed texts', 'neal' ) );

			wp_die();
		}

		public function items( $item ) {
			ob_start();
			new Neal_Ext_Shortcode_Components( $item );
			return ob_get_clean();;
		}

		public function ajax_items() {
			if ( isset( $_POST['data'] ) ) {
				$item = $_POST['data'];

				$output = get_transient( 'neal_shortcode_items' . sanitize_text_field( $item ) );

				if ( $output ) {
					echo $output;
				} else {
				
					$return = $this->items( sanitize_text_field( $item ) );
					$return .= '<a href="#" class="button button-primary neal-insert-shortcode">'.esc_html__( 'Insert', 'neal' ).'</a>';

					$return .= '<input type="hidden" id="neal-shortcode" value="neal_ext_'.esc_attr($item).'">';

					set_transient( 'neal_shortcode_items' . sanitize_text_field( $item ), $return, 2 * DAY_IN_SECONDS );
					echo $return;
				}

				exit;
			}
		}
	}

endif;

return new Neal_Ext_Shortcode_Items;